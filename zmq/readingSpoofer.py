import zmq
import random
import sys
import time
import SerialMessage_pb2 as SerialMessage

port = "5555"

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.connect("tcp://127.0.0.1:%s" % port)

counter = 0

controllers = [SerialMessage.ControlMessage.RUDDER, SerialMessage.ControlMessage.AILERON, SerialMessage.ControlMessage.THROTTLE]
time.sleep(1)


while True:
    obj = SerialMessage.ControlMessage()
    obj.TargetComponent = SerialMessage.ControlMessage.FLIGHT_COMPUTER
    obj.SourceComponent = controllers[int(counter) % 3]
    obj.position = counter
    socket.send(obj.SerializeToString())
    print "Sent Message value %s" % (counter)
    counter = (counter + 1.0) % 100.0
    time.sleep(0.1)
