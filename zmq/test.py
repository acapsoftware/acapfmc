#
#   Weather update client
#   Connects SUB socket to tcp://localhost:5556
#   Collects weather updates and finds avg temp in zipcode
#

import sys
import zmq
import SerialMessage_pb2

#  Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://127.0.0.1:5560")

# NEEDED
socket.setsockopt_string(zmq.SUBSCRIBE, "".decode('ascii'))



while(True):
    string = socket.recv()
    print "test"
    obj = SerialMessage_pb2.ControlMessage()
    obj.ParseFromString(string)
    print obj.component
    print obj.positionSet
