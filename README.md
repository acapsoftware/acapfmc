# Getting Started

## Build Instructions

To build the project, you will require the following

- Java8 SDK (Available on the oracle site)
- Gradle
- Git

### First Build

Once you have the required dependencies, we can build the code base.

```
git clone git@bitbucket.org:acapsoftware/acapfmc.git
cd acapfmc
gradle install (if this fails, run it again)
gradle build
gradle run
```

When you run the program, you should see a splash window showing the loading progress.

Once it loads, you'll see the OpenGL world on the left, and the control panel interface on the right. 

### Setting Up Hotswap

If you're working on a routine and want to debug without having to restart the FMC, you can set up hotswap to allow changes to be reloaded immediately. 

`gradle hotswapsetup` 

This command will download and run an installer to add the dcevm modified JVM. Select your java sdk and install as an alternate JVM. Do not replace the current one. Once installed, you can run the program in hotswap mode with

`gradle run -Photswap`

Any changes made will only be seen if they are re-run. For example, one time executions are not going to be seen when using a hot swapper. If you are working on a looped task, then changes will be seen upon save.

## Overview of FMC 

The program starts out by initializing the GLWorld, which is the 3d view seen on the left side of the FMC. The first object created by the FMC is the UserAircraft. The UserAircraft singleton is used throughout the FMC to control devices, manage sensor readings, and manage the mode control. The GLWorld class then loads draw loops tasks found in the **acap.world.plugin** folder and adds them to the draw loop. The draw loop tasks are used to add content to the OpenGL world.  Once the drawing tasks are loaded, the core logic threads are started. These include the control scheduler, message handler, spatial sensors, GPS, audio cue handler, and state machine. Once started, all of these threads handle their own logic and the world is drawing logic is all handled in the GLWorld.

### Control Scheduler

The Control Scheduler thread is in charge of going through the task queue and running each group of tasks given. These tasks are created through the **Tasks** tab by clicking on the + button. They can also be added programmatically.

### Message Handler

The message handler, otherwise known as the SubscribeThread, is in charge of interpreting received messages. The messages are received through the locally running ZMQ instance. By default, this ZMQ instance is running on localhost bound to port 5560. 

Messages are handled based on their type. The Message handler loads handlers from the **acap.fmc.procedure.messages** package. Each handler implements the *IMessageHandler* class. The corresponding message handler for an incoming message stores the received data and runs the action defined in the message handler. For example, the ControlMessageHandler interprets all sensor messages from the aircraft and sends the readings to the proper device specified in the message. 

### Spatial Sensors/GPS

Currently, we use phidget USB sensors to get GPS readings, pitch, and roll. The spatial sensor threads take data from these sensors and put them through a Kalman filter. After filtering the sensor data, the refined data is stored in the UserAircraft class.

### Audio Cues (Aural Handler)

The Aural handler manages audio cues that need to be played during any phase of flight. The thread loads all classes found in the **fmc.procedure.aural** package. These classes implement a common *IAudioCue* interface. The aural handler thread then checks the condition of each loaded cue and will play the corresponding audio file. 

### State Machine

The state machine is responsible for loading routines related to each phase of flight. These routines are user defined and can be found in **acap.fmc.state.fsm.routines**. At the transition of each state, the state machine thread checks the classes found in the routines package and kicks off any IRoutine that returns a relevant state that matches the next state. The routine is then executed either one time, forever (until the FMC shuts down), or until a specific state is reached. Example routines can be found in the routines package. 

###### Optional Threads

If the *ENABLE_JOYSTICK* flag in **fmc.Constants** is set to true, the flight computer will accept joystick input and act as a fly-by-wire. If the *READ_DATA_FROM_SIM* flag is set to true in **fmc.Constants**, the control sensor data will be retrieved from a locally running MSFlightSim 2009 instance. (The FMC needs to be run in 32-bit java in order to use this feature). 

## Contributing to the FMC

If you'd like to contribute code to the FMC, check out the [wiki](https://bitbucket.org/acapsoftware/acapfmc/wiki/Home) for information on how to implement routines, audio cues, tasks, plugins, messages handlers, and any other custom additions.