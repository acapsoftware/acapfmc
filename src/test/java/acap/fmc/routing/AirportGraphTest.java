/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.routing;

import static org.junit.Assert.*;

import acap.world.model.Airport;
import java.util.ArrayList;
import java.util.Arrays;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import org.junit.Before;
import org.junit.Test;


public class AirportGraphTest {

  @Test
  public void getRunwayByHeading1() throws Exception {

  }

  AirportGraph g = new AirportGraph("KBOS");

  @Before
  public void setUp() throws Exception {}

  @Test
  public void getNodeNearLonLat() throws Exception {}

  @Test
  public void findPathToTaxiWay() throws Exception {}

  @Test
  public void getRunwayByHeading() throws Exception {
    Node[] test = g.getRunwayByHeading("22L");
    Node[] test2 = g.getRunwayByHeading("15R");

    ArrayList<String> nodesVisited = new ArrayList<>();
    Arrays.stream(test).forEach(n -> nodesVisited.add(n.getAttribute(AirportGraph.TAXIWAY).toString()));

    ArrayList<String> nodesVisited2 = new ArrayList<>();
    Arrays.stream(test2).forEach(n -> nodesVisited2.add(n.getAttribute(AirportGraph.TAXIWAY).toString()));

    assertTrue(nodesVisited.contains("04R/22L") && nodesVisited.contains("15L/33R") && nodesVisited.contains("09/27"));
    assertTrue(nodesVisited2.contains("15R/33L"));


  }

  @Test
  public void findPathToTaxiWay_WITHOUT_USING_OTHER_TAXIWAYS() throws Exception {

    Node test = g.getNodeNearLonLat(-72.03807067871094, 42.09977340698242);
    Path p = g.findPathToTaxiWay(test, "K", e -> !e.getAttribute("type").equals("TAXI"));
    ArrayList<String> nodesVisited = new ArrayList<>();

    p.getNodeSet().forEach(n -> nodesVisited.add(n.getAttribute(AirportGraph.TAXIWAY).toString()));

    assertTrue(nodesVisited.contains("K"));
  }

  @Test
  public void findPathToTaxiWay_SHORTESTROUTE() throws Exception {
    Node test = g.getNodeNearLonLat(-71.028293, 42.373740);
    Path p = g.findPathToTaxiWay(test, "A");
    ArrayList<String> nodesVisited = new ArrayList<>();

    p.getNodeSet().forEach(n -> nodesVisited.add(n.getAttribute(AirportGraph.NAME).toString()));
    nodesVisited.forEach(System.out::println);

    test.setAttribute("ui.class", "important");

    assertTrue(nodesVisited.contains("A"));
  }

  @Test
  public void parsePathFromString_FromParkingSpace() throws Exception {
    Path p = new Path();

    ArrayList<String> taxiwaysUsed = new ArrayList<>();

    Node test = g.getNodeNearLonLat(-71.0176773071289, 42.35686492919922);

    p = g.parsePathFromString("K K3 E K F 15R G D D1 09", test);

    p.getEdgeSet()
        .forEach(
            (Edge e) -> {
              taxiwaysUsed.add(e.getAttribute("taxipath"));
            });

    Arrays.stream("K K3 E K F 15R/33L G D D1".split(" "))
        .forEach(s -> assertTrue(taxiwaysUsed.contains(s)));
  }

  @Test
  public void getTaxiEdges() throws Exception {}
}
