package acap.fmc.routing.air;

import static org.junit.Assert.*;

import acap.fmc.UserAircraft;
import acap.fmc.routing.Waypoint;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by erikn on 4/17/2017.
 */
public class AirRouteTest {

  @Test
  public void getLandingWaypoints() throws Exception {

  }

  @Test
  public void calculateRoute() throws Exception {
    ArrayList<Waypoint> correct_route_to_KORH_KGDM = new ArrayList<>();

    Waypoint start = new Waypoint(-71.87067413330078,42.266326904296875, 1013);

    correct_route_to_KORH_KGDM.add(new Waypoint(-71.87265014648438,42.26642608642578, 1013)); // 0
    correct_route_to_KORH_KGDM.add(new Waypoint(-71.9366683959961,42.26960372924805, 2008)); // 1
    correct_route_to_KORH_KGDM.add(new Waypoint(-71.95623016357422,42.339012145996094, 3000)); // 2
    correct_route_to_KORH_KGDM.add(new Waypoint(-71.97239685058594,42.521278381347656, 3000)); // 3
    correct_route_to_KORH_KGDM.add(new Waypoint(-71.97457885742188,42.54592514038086, 1955)); // 4
    correct_route_to_KORH_KGDM.add(new Waypoint(-71.99418640136719,42.554222106933594, 1955)); // 5
    correct_route_to_KORH_KGDM.add(new Waypoint(-72.00302124023438,42.57830047607422, 1955)); // 6
    correct_route_to_KORH_KGDM.add(new Waypoint(-72.02485656738281,42.57395553588867, 1955)); // 7
    correct_route_to_KORH_KGDM.add(new Waypoint(-72.01642608642578,42.550987243652344, 955)); // 8

//    ArrayList<Waypoint> KORH_KGDM = RoutingHelper.generateRoute(
//        start,
//        "29",
//        "18",
//        "KORH KGDM",
//        3000,
//        700,
//        60);

    AirRoute route = new AirRoute("KORH KGDM");
    route.setStartingWaypoint(start);
    route.setTakeOffRunway("29");
    route.setLandingRunway("18");
    route.setCruiseSpeed(60);
    UserAircraft.getInstance().setCruiseSpeed(60);
    UserAircraft.getInstance().setCruiseAlt(3000);

    Assert.assertArrayEquals(correct_route_to_KORH_KGDM.toArray(),route.calculateRoute().toArray());
  }

}