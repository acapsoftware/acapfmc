package acap.fmc;

import static org.junit.Assert.*;

import acap.utilities.Audio;
import org.junit.Test;


public class AudioTest {

  @Test
  public void playAudio() throws Exception {
    new Thread(Audio.getInstance()).start();
    Audio.getInstance().playAudio("data/10.wav",1);
    Audio.getInstance().playAudio("data/20.wav",1);
    Audio.getInstance().playAudio("data/30.wav",1);
    Audio.getInstance().playAudio("data/40.wav",1);
    Audio.getInstance().playAudio("data/50.wav",1);
    Audio.getInstance().playAudio(Audio.AURAL_WARNING_TEST_A,0);
    Thread.sleep(10000);
    assertEquals(Audio.getInstance().getNumPlayed(), 6);
  }

}