package acap.fmc;

import static org.junit.Assert.*;

import acap.utilities.Audio;
import acap.utilities.TTS;
import org.junit.Test;

public class TTSTest {

  @Test
  public void speak() throws Exception {
      new Thread(Audio.getInstance()).start();
      int currentPlayed = Audio.getInstance().getNumPlayed();
      TTS t = new TTS();
      t.speak("This is a test. ");
      Thread.sleep(1000);

      assertEquals(currentPlayed + 1, Audio.getInstance().getNumPlayed());
  }

}