/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package metar.core;

import static org.junit.Assert.assertEquals;

import acap.world.utilities.NavDB;
import acap.world.model.Runway;
import metar.core.xml.METAR;
import metar.core.xml.SkyCondition;
import org.junit.Test;

public class METARAPITest {
  @Test
  public void grabMETARLastHour() throws Exception {
    API test = new API();
    METAR b = test.grabLatestMETAR("KORH");
    for (SkyCondition a : b.getSkyCondition()) {
      System.out.println(a.getSkyCover());
    }
    assertEquals("KORH", b.getStationId());
  }

  @Test
  public void grabMETARNearTest() throws Exception {
    API test = new API();
    METAR b = test.grabNearestMETAR(-104.75f, 39.72f, 10);
    for (SkyCondition a : b.getSkyCondition()) {
      System.out.println(a.getSkyCover());
    }
    assertEquals("KBKF", b.getStationId());
  }

  @Test
  public void getDensityAltitude() throws Exception {

    API test = new API();
    METAR b = test.grabLatestMETAR("KORH");

    System.out.println(test.calculateDensityAltitude(b) + " ft");
  }

  @Test
  public void runwayCheck() throws Exception {
    NavDB d = NavDB.getInstance();
    d.getAirport("KORH");
    API test = new API();
    METAR b = test.grabLatestMETAR("KORH");

    Runway r = test.pickTakeOffRunway(b, d.getAirport("KORH"), 0f, 0f, 0f);
    System.out.println("WIND HEADING: " + b.getWindDirDegrees());
    System.out.println(r.getHeading());
    System.out.println(r.getPrimaryNumber());
    System.out.println(r.getPrimaryDesignator());
    System.out.println(r.getSecondaryNumber());
  }
}
