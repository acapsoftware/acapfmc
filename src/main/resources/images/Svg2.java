

import java.awt.*;
import java.awt.geom.*;
import static java.awt.Color.*;
import static java.awt.MultipleGradientPaint.CycleMethod.*;
import static java.awt.MultipleGradientPaint.ColorSpaceType.*;

/**
 * This class has been automatically generated using
 * <a href="http://ebourg.github.io/flamingo-svg-transcoder/">Flamingo SVG transcoder</a>.
 */
public class Svg2 {

    /**
     * Paints the transcoded SVG image on the specified processing context. You
     * can install a custom transformation on the processing context to scale the
     * image.
     * 
     * @param g Graphics context.
     */
    public static void paint(Graphics2D g) {
        Shape shape = null;
        
        float origAlpha = 1.0f;
        Composite origComposite = g.getComposite();
        if (origComposite instanceof AlphaComposite) {
            AlphaComposite origAlphaComposite = (AlphaComposite)origComposite;
            if (origAlphaComposite.getRule() == AlphaComposite.SRC_OVER) {
                origAlpha = origAlphaComposite.getAlpha();
            }
        }
        
        java.util.LinkedList<AffineTransform> transformations = new java.util.LinkedList<AffineTransform>();
        

        // 

        // _0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, 540));

        // _0_0

        // _0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(960.0, -536.071);
        ((GeneralPath) shape).curveTo(960.0, -538.24, 958.24, -540.0, 956.071, -540.0);
        ((GeneralPath) shape).lineTo(3.929, -540.0);
        ((GeneralPath) shape).curveTo(1.76, -540.0, 0.0, -538.24, 0.0, -536.071);
        ((GeneralPath) shape).lineTo(0.0, 537.5);
        ((GeneralPath) shape).curveTo(0.0, 539.668, 1.76, 541.429, 3.929, 541.429);
        ((GeneralPath) shape).lineTo(956.071, 541.429);
        ((GeneralPath) shape).curveTo(958.24, 541.429, 960.0, 539.668, 960.0, 537.5);
        ((GeneralPath) shape).lineTo(960.0, -536.071);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00001C));
        g.fill(shape);

        // _0_0_1
        shape = new Rectangle2D.Double(353.14898681640625, -538.989990234375, 175.42300415039062, 63.2760009765625);
        g.setPaint(new Color(0x001A6D));
        g.fill(shape);
        g.setPaint(new Color(0x0075FF));
        g.setStroke(new BasicStroke(1.5f, 0, 0, 4));
        g.draw(shape);

        // _0_0_2
        shape = new Rectangle2D.Double(177.00999450683594, -538.989990234375, 175.42300415039062, 63.2760009765625);
        g.fill(shape);
        g.draw(shape);

        // _0_0_3
        shape = new Rectangle2D.Double(0.36500000953674316, -538.989990234375, 175.42300415039062, 63.2760009765625);
        g.setPaint(new Color(0x001A6D));
        g.fill(shape);
        g.setPaint(new Color(0x0075FF));
        g.draw(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 206.429f, -494.286f));

        // _0_0_4

        // _0_0_4_0

        // _0_0_4_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.625, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -15.047);
        ((GeneralPath) shape).lineTo(17.648, -15.047);
        ((GeneralPath) shape).lineTo(17.648, -11.672);
        ((GeneralPath) shape).lineTo(5.625, -11.672);
        ((GeneralPath) shape).lineTo(5.625, 0.352);
        ((GeneralPath) shape).lineTo(2.074, 0.352);
        ((GeneralPath) shape).lineTo(2.074, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -23.695);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_4_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(45.932, -25.84);
        ((GeneralPath) shape).lineTo(28.002, 1.037);
        ((GeneralPath) shape).lineTo(25.102, -0.879);
        ((GeneralPath) shape).lineTo(43.031, -27.773);
        ((GeneralPath) shape).lineTo(45.932, -25.84);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_4_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(66.357, -19.863);
        ((GeneralPath) shape).lineTo(62.525, -23.695);
        ((GeneralPath) shape).lineTo(56.057, -23.695);
        ((GeneralPath) shape).lineTo(53.051, -20.689);
        ((GeneralPath) shape).lineTo(53.051, -6.029);
        ((GeneralPath) shape).lineTo(56.057, -3.023);
        ((GeneralPath) shape).lineTo(62.525, -3.023);
        ((GeneralPath) shape).lineTo(66.357, -6.855);
        ((GeneralPath) shape).lineTo(68.818, -4.395);
        ((GeneralPath) shape).lineTo(64.072, 0.352);
        ((GeneralPath) shape).lineTo(54.51, 0.352);
        ((GeneralPath) shape).lineTo(49.5, -4.658);
        ((GeneralPath) shape).lineTo(49.5, -22.061);
        ((GeneralPath) shape).lineTo(54.51, -27.07);
        ((GeneralPath) shape).lineTo(64.072, -27.07);
        ((GeneralPath) shape).lineTo(68.818, -22.324);
        ((GeneralPath) shape).lineTo(66.357, -19.863);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_4_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(81.018, 0.352);
        ((GeneralPath) shape).lineTo(81.018, -23.695);
        ((GeneralPath) shape).lineTo(73.002, -23.695);
        ((GeneralPath) shape).lineTo(73.002, -27.07);
        ((GeneralPath) shape).lineTo(92.584, -27.07);
        ((GeneralPath) shape).lineTo(92.584, -23.695);
        ((GeneralPath) shape).lineTo(84.568, -23.695);
        ((GeneralPath) shape).lineTo(84.568, 0.352);
        ((GeneralPath) shape).lineTo(81.018, 0.352);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_4_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(116.227, -3.023);
        ((GeneralPath) shape).lineTo(116.227, 0.352);
        ((GeneralPath) shape).lineTo(96.645, 0.352);
        ((GeneralPath) shape).lineTo(96.645, -27.07);
        ((GeneralPath) shape).lineTo(100.195, -27.07);
        ((GeneralPath) shape).lineTo(100.195, -3.023);
        ((GeneralPath) shape).lineTo(116.227, -3.023);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_4
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 392.429f, -494.286f));

        // _0_0_5

        // _0_0_5_0

        // _0_0_5_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.625, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -15.047);
        ((GeneralPath) shape).lineTo(17.648, -15.047);
        ((GeneralPath) shape).lineTo(17.648, -11.672);
        ((GeneralPath) shape).lineTo(5.625, -11.672);
        ((GeneralPath) shape).lineTo(5.625, -3.023);
        ((GeneralPath) shape).lineTo(21.656, -3.023);
        ((GeneralPath) shape).lineTo(21.656, 0.352);
        ((GeneralPath) shape).lineTo(2.074, 0.352);
        ((GeneralPath) shape).lineTo(2.074, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -23.695);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_5_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(45.299, -3.023);
        ((GeneralPath) shape).lineTo(45.299, 0.352);
        ((GeneralPath) shape).lineTo(25.717, 0.352);
        ((GeneralPath) shape).lineTo(25.717, -27.07);
        ((GeneralPath) shape).lineTo(29.268, -27.07);
        ((GeneralPath) shape).lineTo(29.268, -3.023);
        ((GeneralPath) shape).lineTo(45.299, -3.023);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_5_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(52.91, -23.695);
        ((GeneralPath) shape).lineTo(52.91, -15.047);
        ((GeneralPath) shape).lineTo(64.934, -15.047);
        ((GeneralPath) shape).lineTo(64.934, -11.672);
        ((GeneralPath) shape).lineTo(52.91, -11.672);
        ((GeneralPath) shape).lineTo(52.91, -3.023);
        ((GeneralPath) shape).lineTo(68.941, -3.023);
        ((GeneralPath) shape).lineTo(68.941, 0.352);
        ((GeneralPath) shape).lineTo(49.359, 0.352);
        ((GeneralPath) shape).lineTo(49.359, -27.07);
        ((GeneralPath) shape).lineTo(68.941, -27.07);
        ((GeneralPath) shape).lineTo(68.941, -23.695);
        ((GeneralPath) shape).lineTo(52.91, -23.695);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_5_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(90.0, -19.863);
        ((GeneralPath) shape).lineTo(86.168, -23.695);
        ((GeneralPath) shape).lineTo(79.699, -23.695);
        ((GeneralPath) shape).lineTo(76.693, -20.689);
        ((GeneralPath) shape).lineTo(76.693, -6.029);
        ((GeneralPath) shape).lineTo(79.699, -3.023);
        ((GeneralPath) shape).lineTo(86.168, -3.023);
        ((GeneralPath) shape).lineTo(90.0, -6.855);
        ((GeneralPath) shape).lineTo(92.461, -4.395);
        ((GeneralPath) shape).lineTo(87.715, 0.352);
        ((GeneralPath) shape).lineTo(78.152, 0.352);
        ((GeneralPath) shape).lineTo(73.143, -4.658);
        ((GeneralPath) shape).lineTo(73.143, -22.061);
        ((GeneralPath) shape).lineTo(78.152, -27.07);
        ((GeneralPath) shape).lineTo(87.715, -27.07);
        ((GeneralPath) shape).lineTo(92.461, -22.324);
        ((GeneralPath) shape).lineTo(90.0, -19.863);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_5

        // _0_0_6
        shape = new Rectangle2D.Double(529.218017578125, -538.989990234375, 175.42300415039062, 63.2760009765625);
        g.setPaint(new Color(0x001A6D));
        g.fill(shape);
        g.setPaint(new Color(0x0075FF));
        g.draw(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 544, -494.286f));

        // _0_0_7

        // _0_0_7_0

        // _0_0_7_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.625, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -15.047);
        ((GeneralPath) shape).lineTo(17.648, -15.047);
        ((GeneralPath) shape).lineTo(17.648, -11.672);
        ((GeneralPath) shape).lineTo(5.625, -11.672);
        ((GeneralPath) shape).lineTo(5.625, -3.023);
        ((GeneralPath) shape).lineTo(21.656, -3.023);
        ((GeneralPath) shape).lineTo(21.656, 0.352);
        ((GeneralPath) shape).lineTo(2.074, 0.352);
        ((GeneralPath) shape).lineTo(2.074, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -23.695);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_7_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(45.299, 0.352);
        ((GeneralPath) shape).lineTo(42.539, 0.352);
        ((GeneralPath) shape).lineTo(29.268, -19.564);
        ((GeneralPath) shape).lineTo(29.268, 0.352);
        ((GeneralPath) shape).lineTo(25.717, 0.352);
        ((GeneralPath) shape).lineTo(25.717, -27.07);
        ((GeneralPath) shape).lineTo(28.477, -27.07);
        ((GeneralPath) shape).lineTo(41.748, -7.154);
        ((GeneralPath) shape).lineTo(41.748, -27.07);
        ((GeneralPath) shape).lineTo(45.299, -27.07);
        ((GeneralPath) shape).lineTo(45.299, 0.352);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_7_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(66.217, -19.863);
        ((GeneralPath) shape).lineTo(62.385, -23.695);
        ((GeneralPath) shape).lineTo(55.916, -23.695);
        ((GeneralPath) shape).lineTo(52.91, -20.689);
        ((GeneralPath) shape).lineTo(52.91, -6.029);
        ((GeneralPath) shape).lineTo(55.916, -3.023);
        ((GeneralPath) shape).lineTo(62.385, -3.023);
        ((GeneralPath) shape).lineTo(65.391, -6.029);
        ((GeneralPath) shape).lineTo(65.391, -11.672);
        ((GeneralPath) shape).lineTo(57.375, -11.672);
        ((GeneralPath) shape).lineTo(57.375, -15.047);
        ((GeneralPath) shape).lineTo(68.941, -15.047);
        ((GeneralPath) shape).lineTo(68.941, -4.658);
        ((GeneralPath) shape).lineTo(63.932, 0.352);
        ((GeneralPath) shape).lineTo(54.369, 0.352);
        ((GeneralPath) shape).lineTo(49.359, -4.658);
        ((GeneralPath) shape).lineTo(49.359, -22.061);
        ((GeneralPath) shape).lineTo(54.369, -27.07);
        ((GeneralPath) shape).lineTo(63.932, -27.07);
        ((GeneralPath) shape).lineTo(68.678, -22.324);
        ((GeneralPath) shape).lineTo(66.217, -19.863);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_7_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(80.982, -24.258);
        ((GeneralPath) shape).lineTo(75.059, -24.258);
        ((GeneralPath) shape).lineTo(75.059, -27.07);
        ((GeneralPath) shape).lineTo(90.51, -27.07);
        ((GeneralPath) shape).lineTo(90.51, -24.258);
        ((GeneralPath) shape).lineTo(84.533, -24.258);
        ((GeneralPath) shape).lineTo(84.533, -2.707);
        ((GeneralPath) shape).lineTo(90.51, -2.707);
        ((GeneralPath) shape).lineTo(90.51, 0.352);
        ((GeneralPath) shape).lineTo(75.059, 0.352);
        ((GeneralPath) shape).lineTo(75.059, -2.707);
        ((GeneralPath) shape).lineTo(80.982, -2.707);
        ((GeneralPath) shape).lineTo(80.982, -24.258);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_7_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(116.227, 0.352);
        ((GeneralPath) shape).lineTo(113.467, 0.352);
        ((GeneralPath) shape).lineTo(100.195, -19.564);
        ((GeneralPath) shape).lineTo(100.195, 0.352);
        ((GeneralPath) shape).lineTo(96.645, 0.352);
        ((GeneralPath) shape).lineTo(96.645, -27.07);
        ((GeneralPath) shape).lineTo(99.404, -27.07);
        ((GeneralPath) shape).lineTo(112.676, -7.154);
        ((GeneralPath) shape).lineTo(112.676, -27.07);
        ((GeneralPath) shape).lineTo(116.227, -27.07);
        ((GeneralPath) shape).lineTo(116.227, 0.352);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_7_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(123.838, -23.695);
        ((GeneralPath) shape).lineTo(123.838, -15.047);
        ((GeneralPath) shape).lineTo(135.861, -15.047);
        ((GeneralPath) shape).lineTo(135.861, -11.672);
        ((GeneralPath) shape).lineTo(123.838, -11.672);
        ((GeneralPath) shape).lineTo(123.838, -3.023);
        ((GeneralPath) shape).lineTo(139.869, -3.023);
        ((GeneralPath) shape).lineTo(139.869, 0.352);
        ((GeneralPath) shape).lineTo(120.287, 0.352);
        ((GeneralPath) shape).lineTo(120.287, -27.07);
        ((GeneralPath) shape).lineTo(139.869, -27.07);
        ((GeneralPath) shape).lineTo(139.869, -23.695);
        ((GeneralPath) shape).lineTo(123.838, -23.695);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_7

        // _0_0_8
        shape = new Rectangle2D.Double(705.2479858398438, -538.989990234375, 175.42300415039062, 63.2760009765625);
        g.setPaint(new Color(0x001A6D));
        g.fill(shape);
        g.setPaint(new Color(0x0075FF));
        g.draw(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 743.204f, -494.286f));

        // _0_0_9

        // _0_0_9_0

        // _0_0_9_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(2.074, -27.07);
        ((GeneralPath) shape).lineTo(16.646, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -22.061);
        ((GeneralPath) shape).lineTo(21.656, -16.682);
        ((GeneralPath) shape).lineTo(16.646, -11.672);
        ((GeneralPath) shape).lineTo(5.625, -11.672);
        ((GeneralPath) shape).lineTo(5.625, 0.352);
        ((GeneralPath) shape).lineTo(2.074, 0.352);
        ((GeneralPath) shape).lineTo(2.074, -27.07);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(5.625, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -15.047);
        ((GeneralPath) shape).lineTo(15.1, -15.047);
        ((GeneralPath) shape).lineTo(18.105, -18.053);
        ((GeneralPath) shape).lineTo(18.105, -20.689);
        ((GeneralPath) shape).lineTo(15.1, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -23.695);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_9_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(25.4, -27.07);
        ((GeneralPath) shape).lineTo(39.973, -27.07);
        ((GeneralPath) shape).lineTo(44.982, -22.061);
        ((GeneralPath) shape).lineTo(44.982, -16.682);
        ((GeneralPath) shape).lineTo(39.973, -11.672);
        ((GeneralPath) shape).lineTo(38.426, -11.672);
        ((GeneralPath) shape).lineTo(45.598, -0.879);
        ((GeneralPath) shape).lineTo(42.697, 1.037);
        ((GeneralPath) shape).lineTo(34.225, -11.672);
        ((GeneralPath) shape).lineTo(28.951, -11.672);
        ((GeneralPath) shape).lineTo(28.951, 0.352);
        ((GeneralPath) shape).lineTo(25.4, 0.352);
        ((GeneralPath) shape).lineTo(25.4, -27.07);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(28.951, -23.695);
        ((GeneralPath) shape).lineTo(28.951, -15.047);
        ((GeneralPath) shape).lineTo(38.426, -15.047);
        ((GeneralPath) shape).lineTo(41.432, -18.053);
        ((GeneralPath) shape).lineTo(41.432, -20.689);
        ((GeneralPath) shape).lineTo(38.426, -23.695);
        ((GeneralPath) shape).lineTo(28.951, -23.695);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_9_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(68.941, -4.658);
        ((GeneralPath) shape).lineTo(63.932, 0.352);
        ((GeneralPath) shape).lineTo(54.369, 0.352);
        ((GeneralPath) shape).lineTo(49.359, -4.658);
        ((GeneralPath) shape).lineTo(49.359, -22.061);
        ((GeneralPath) shape).lineTo(54.369, -27.07);
        ((GeneralPath) shape).lineTo(63.932, -27.07);
        ((GeneralPath) shape).lineTo(68.941, -22.061);
        ((GeneralPath) shape).lineTo(68.941, -4.658);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(55.916, -23.695);
        ((GeneralPath) shape).lineTo(52.91, -20.689);
        ((GeneralPath) shape).lineTo(52.91, -6.029);
        ((GeneralPath) shape).lineTo(55.916, -3.023);
        ((GeneralPath) shape).lineTo(62.385, -3.023);
        ((GeneralPath) shape).lineTo(65.391, -6.029);
        ((GeneralPath) shape).lineTo(65.391, -20.689);
        ((GeneralPath) shape).lineTo(62.385, -23.695);
        ((GeneralPath) shape).lineTo(55.916, -23.695);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_9_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(90.0, -19.863);
        ((GeneralPath) shape).lineTo(86.168, -23.695);
        ((GeneralPath) shape).lineTo(79.699, -23.695);
        ((GeneralPath) shape).lineTo(76.693, -20.689);
        ((GeneralPath) shape).lineTo(76.693, -6.029);
        ((GeneralPath) shape).lineTo(79.699, -3.023);
        ((GeneralPath) shape).lineTo(86.168, -3.023);
        ((GeneralPath) shape).lineTo(90.0, -6.855);
        ((GeneralPath) shape).lineTo(92.461, -4.395);
        ((GeneralPath) shape).lineTo(87.715, 0.352);
        ((GeneralPath) shape).lineTo(78.152, 0.352);
        ((GeneralPath) shape).lineTo(73.143, -4.658);
        ((GeneralPath) shape).lineTo(73.143, -22.061);
        ((GeneralPath) shape).lineTo(78.152, -27.07);
        ((GeneralPath) shape).lineTo(87.715, -27.07);
        ((GeneralPath) shape).lineTo(92.461, -22.324);
        ((GeneralPath) shape).lineTo(90.0, -19.863);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_9

        // _0_0_10
        shape = new Rectangle2D.Double(881.010009765625, -538.989990234375, 79.19300079345703, 63.2760009765625);
        g.setPaint(new Color(0x001A6D));
        g.fill(shape);
        g.setPaint(new Color(0x0075FF));
        g.draw(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -540));

        // _0_0_11

        // _0_0_11_0
        shape = new Ellipse2D.Double(910, 20.35700035095215, 24.285999298095703, 24.285999298095703);
        g.setPaint(WHITE);
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_11

        // _0_0_12
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(925.614, -522.857);
        ((GeneralPath) shape).curveTo(925.614, -523.498, 925.094, -524.018, 924.453, -524.018);
        ((GeneralPath) shape).lineTo(920.011, -524.018);
        ((GeneralPath) shape).curveTo(919.37, -524.018, 918.85, -523.498, 918.85, -522.857);
        ((GeneralPath) shape).lineTo(918.85, -492.054);
        ((GeneralPath) shape).curveTo(918.85, -491.413, 919.37, -490.893, 920.011, -490.893);
        ((GeneralPath) shape).lineTo(924.453, -490.893);
        ((GeneralPath) shape).curveTo(925.094, -490.893, 925.614, -491.413, 925.614, -492.054);
        ((GeneralPath) shape).lineTo(925.614, -522.857);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_13
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(932.406, -520.656);
        ((GeneralPath) shape).curveTo(931.563, -520.652, 927.628, -517.469, 923.219, -513.156);
        ((GeneralPath) shape).lineTo(921.438, -511.406);
        ((GeneralPath) shape).curveTo(922.224, -510.662, 923.032, -509.915, 923.875, -509.063);
        ((GeneralPath) shape).curveTo(924.722, -508.206, 925.449, -507.391, 926.188, -506.594);
        ((GeneralPath) shape).lineTo(927.938, -508.313);
        ((GeneralPath) shape).curveTo(932.641, -512.912, 936.075, -516.978, 935.625, -517.438);
        ((GeneralPath) shape).lineTo(932.531, -520.625);
        ((GeneralPath) shape).curveTo(932.503, -520.654, 932.462, -520.657, 932.406, -520.656);
        ((GeneralPath) shape).lineTo(932.406, -520.656);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(918.25, -508.281);
        ((GeneralPath) shape).lineTo(916.531, -506.594);
        ((GeneralPath) shape).curveTo(911.828, -501.994, 908.394, -497.928, 908.844, -497.469);
        ((GeneralPath) shape).lineTo(911.938, -494.281);
        ((GeneralPath) shape).curveTo(912.387, -493.822, 916.547, -497.15, 921.25, -501.75);
        ((GeneralPath) shape).lineTo(923.031, -503.5);
        ((GeneralPath) shape).curveTo(922.245, -504.244, 921.437, -504.992, 920.594, -505.844);
        ((GeneralPath) shape).curveTo(919.751, -506.696, 918.986, -507.487, 918.25, -508.281);
        ((GeneralPath) shape).closePath();

        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0, 1, -1, 0, 0, 0));

        // _0_0_14

        // _0_0_14_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-504.073, -937.634);
        ((GeneralPath) shape).curveTo(-504.073, -938.275, -504.593, -938.795, -505.234, -938.795);
        ((GeneralPath) shape).lineTo(-509.677, -938.795);
        ((GeneralPath) shape).curveTo(-510.317, -938.795, -510.837, -938.275, -510.837, -937.634);
        ((GeneralPath) shape).lineTo(-510.837, -906.83);
        ((GeneralPath) shape).curveTo(-510.837, -906.19, -510.317, -905.67, -509.677, -905.67);
        ((GeneralPath) shape).lineTo(-505.234, -905.67);
        ((GeneralPath) shape).curveTo(-504.593, -905.67, -504.073, -906.19, -504.073, -906.83);
        ((GeneralPath) shape).lineTo(-504.073, -937.634);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_14
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(-0.710882f, 0.703311f, -0.703311f, -0.710882f, 0, 0));

        // _0_0_15

        // _0_0_15_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-1009.12, -303.277);
        ((GeneralPath) shape).curveTo(-1009.12, -303.917, -1009.64, -304.437, -1010.28, -304.437);
        ((GeneralPath) shape).lineTo(-1014.72, -304.437);
        ((GeneralPath) shape).curveTo(-1015.36, -304.437, -1015.88, -303.917, -1015.88, -303.277);
        ((GeneralPath) shape).lineTo(-1015.88, -272.473);
        ((GeneralPath) shape).curveTo(-1015.88, -271.832, -1015.36, -271.312, -1014.72, -271.312);
        ((GeneralPath) shape).lineTo(-1010.28, -271.312);
        ((GeneralPath) shape).curveTo(-1009.64, -271.312, -1009.12, -271.832, -1009.12, -272.473);
        ((GeneralPath) shape).lineTo(-1009.12, -303.277);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_15
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.433824f, 0, 0, 0.433824f, 522.096f, -521.599f));

        // _0_0_16

        // _0_0_16_0
        shape = new Ellipse2D.Double(910, 20.35700035095215, 24.285999298095703, 24.285999298095703);
        g.setPaint(new Color(0x001A6D));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_16
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 51.4286f, -494.286f));

        // _0_0_17

        // _0_0_17_0

        // _0_0_17_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(2.074, -27.07);
        ((GeneralPath) shape).lineTo(16.646, -27.07);
        ((GeneralPath) shape).lineTo(21.656, -22.061);
        ((GeneralPath) shape).lineTo(21.656, -16.682);
        ((GeneralPath) shape).lineTo(16.646, -11.672);
        ((GeneralPath) shape).lineTo(5.625, -11.672);
        ((GeneralPath) shape).lineTo(5.625, 0.352);
        ((GeneralPath) shape).lineTo(2.074, 0.352);
        ((GeneralPath) shape).lineTo(2.074, -27.07);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(5.625, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -15.047);
        ((GeneralPath) shape).lineTo(15.1, -15.047);
        ((GeneralPath) shape).lineTo(18.105, -18.053);
        ((GeneralPath) shape).lineTo(18.105, -20.689);
        ((GeneralPath) shape).lineTo(15.1, -23.695);
        ((GeneralPath) shape).lineTo(5.625, -23.695);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_17_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(29.268, -23.695);
        ((GeneralPath) shape).lineTo(29.268, -15.047);
        ((GeneralPath) shape).lineTo(41.291, -15.047);
        ((GeneralPath) shape).lineTo(41.291, -11.672);
        ((GeneralPath) shape).lineTo(29.268, -11.672);
        ((GeneralPath) shape).lineTo(29.268, 0.352);
        ((GeneralPath) shape).lineTo(25.717, 0.352);
        ((GeneralPath) shape).lineTo(25.717, -27.07);
        ((GeneralPath) shape).lineTo(45.299, -27.07);
        ((GeneralPath) shape).lineTo(45.299, -23.695);
        ((GeneralPath) shape).lineTo(29.268, -23.695);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_17_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(49.904, -27.176);
        ((GeneralPath) shape).lineTo(63.369, -27.176);
        ((GeneralPath) shape).lineTo(68.379, -22.131);
        ((GeneralPath) shape).lineTo(68.379, -4.623);
        ((GeneralPath) shape).lineTo(63.369, 0.422);
        ((GeneralPath) shape).lineTo(49.904, 0.422);
        ((GeneralPath) shape).lineTo(49.904, -27.176);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(53.332, -23.818);
        ((GeneralPath) shape).lineTo(53.332, -2.848);
        ((GeneralPath) shape).lineTo(61.787, -2.848);
        ((GeneralPath) shape).lineTo(64.793, -5.906);
        ((GeneralPath) shape).lineTo(64.793, -20.76);
        ((GeneralPath) shape).lineTo(61.787, -23.818);
        ((GeneralPath) shape).lineTo(53.332, -23.818);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_17
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_18

        // _0_0_18_0
        shape = new Rectangle2D.Double(628.0700073242188, 935.06201171875, 330.32000732421875, 118.18800354003906);
        g.setPaint(new Color(0x500000));
        g.fill(shape);

        // _0_0_18_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(959.89, 1054.75);
        ((GeneralPath) shape).lineTo(626.57, 1054.75);
        ((GeneralPath) shape).lineTo(626.57, 933.562);
        ((GeneralPath) shape).lineTo(959.89, 933.562);
        ((GeneralPath) shape).lineTo(959.89, 1054.75);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(629.57, 936.562);
        ((GeneralPath) shape).lineTo(629.57, 1051.75);
        ((GeneralPath) shape).lineTo(956.89, 1051.75);
        ((GeneralPath) shape).lineTo(956.89, 936.562);
        ((GeneralPath) shape).lineTo(629.57, 936.562);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_18
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 795.705f, 495.937f));

        // _0_0_19

        // _0_0_19_0

        // _0_0_19_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-124.527, -20.866);
        ((GeneralPath) shape).lineTo(-130.934, -10.237);
        ((GeneralPath) shape).lineTo(-118.496, -10.237);
        ((GeneralPath) shape).lineTo(-124.527, -20.866);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-125.897, -25.104);
        ((GeneralPath) shape).lineTo(-123.5, -25.104);
        ((GeneralPath) shape).lineTo(-114.975, -9.748);
        ((GeneralPath) shape).lineTo(-114.975, 0.326);
        ((GeneralPath) shape).lineTo(-118.496, 0.326);
        ((GeneralPath) shape).lineTo(-118.496, -7.107);
        ((GeneralPath) shape).lineTo(-130.934, -7.107);
        ((GeneralPath) shape).lineTo(-130.934, 0.326);
        ((GeneralPath) shape).lineTo(-134.455, 0.326);
        ((GeneralPath) shape).lineTo(-134.455, -9.667);
        ((GeneralPath) shape).lineTo(-125.897, -25.104);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-111.812, -25.104);
        ((GeneralPath) shape).lineTo(-98.298, -25.104);
        ((GeneralPath) shape).lineTo(-93.652, -20.458);
        ((GeneralPath) shape).lineTo(-93.652, -15.47);
        ((GeneralPath) shape).lineTo(-98.298, -10.824);
        ((GeneralPath) shape).lineTo(-108.519, -10.824);
        ((GeneralPath) shape).lineTo(-108.519, 0.326);
        ((GeneralPath) shape).lineTo(-111.812, 0.326);
        ((GeneralPath) shape).lineTo(-111.812, -25.104);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-108.519, -21.974);
        ((GeneralPath) shape).lineTo(-108.519, -13.954);
        ((GeneralPath) shape).lineTo(-99.733, -13.954);
        ((GeneralPath) shape).lineTo(-96.945, -16.742);
        ((GeneralPath) shape).lineTo(-96.945, -19.187);
        ((GeneralPath) shape).lineTo(-99.733, -21.974);
        ((GeneralPath) shape).lineTo(-108.519, -21.974);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-81.165, -25.202);
        ((GeneralPath) shape).lineTo(-68.678, -25.202);
        ((GeneralPath) shape).lineTo(-64.032, -20.524);
        ((GeneralPath) shape).lineTo(-64.032, -4.287);
        ((GeneralPath) shape).lineTo(-68.678, 0.391);
        ((GeneralPath) shape).lineTo(-81.165, 0.391);
        ((GeneralPath) shape).lineTo(-81.165, -25.202);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-77.986, -22.089);
        ((GeneralPath) shape).lineTo(-77.986, -2.641);
        ((GeneralPath) shape).lineTo(-70.145, -2.641);
        ((GeneralPath) shape).lineTo(-67.358, -5.477);
        ((GeneralPath) shape).lineTo(-67.358, -19.252);
        ((GeneralPath) shape).lineTo(-70.145, -22.089);
        ((GeneralPath) shape).lineTo(-77.986, -22.089);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-52.344, -22.496);
        ((GeneralPath) shape).lineTo(-57.838, -22.496);
        ((GeneralPath) shape).lineTo(-57.838, -25.104);
        ((GeneralPath) shape).lineTo(-43.509, -25.104);
        ((GeneralPath) shape).lineTo(-43.509, -22.496);
        ((GeneralPath) shape).lineTo(-49.051, -22.496);
        ((GeneralPath) shape).lineTo(-49.051, -2.51);
        ((GeneralPath) shape).lineTo(-43.509, -2.51);
        ((GeneralPath) shape).lineTo(-43.509, 0.326);
        ((GeneralPath) shape).lineTo(-57.838, 0.326);
        ((GeneralPath) shape).lineTo(-57.838, -2.51);
        ((GeneralPath) shape).lineTo(-52.344, -2.51);
        ((GeneralPath) shape).lineTo(-52.344, -22.496);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-33.174, 0.326);
        ((GeneralPath) shape).lineTo(-37.575, -4.075);
        ((GeneralPath) shape).lineTo(-35.293, -6.358);
        ((GeneralPath) shape).lineTo(-31.739, -2.804);
        ((GeneralPath) shape).lineTo(-25.74, -2.804);
        ((GeneralPath) shape).lineTo(-22.953, -5.591);
        ((GeneralPath) shape).lineTo(-22.953, -8.037);
        ((GeneralPath) shape).lineTo(-25.74, -10.824);
        ((GeneralPath) shape).lineTo(-33.174, -10.824);
        ((GeneralPath) shape).lineTo(-37.819, -15.47);
        ((GeneralPath) shape).lineTo(-37.819, -20.458);
        ((GeneralPath) shape).lineTo(-33.174, -25.104);
        ((GeneralPath) shape).lineTo(-24.306, -25.104);
        ((GeneralPath) shape).lineTo(-19.904, -20.703);
        ((GeneralPath) shape).lineTo(-22.186, -18.421);
        ((GeneralPath) shape).lineTo(-25.74, -21.974);
        ((GeneralPath) shape).lineTo(-31.739, -21.974);
        ((GeneralPath) shape).lineTo(-34.527, -19.187);
        ((GeneralPath) shape).lineTo(-34.527, -16.742);
        ((GeneralPath) shape).lineTo(-31.739, -13.954);
        ((GeneralPath) shape).lineTo(-24.306, -13.954);
        ((GeneralPath) shape).lineTo(-19.66, -9.308);
        ((GeneralPath) shape).lineTo(-19.66, -4.32);
        ((GeneralPath) shape).lineTo(-24.306, 0.326);
        ((GeneralPath) shape).lineTo(-33.174, 0.326);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-0.13, -18.421);
        ((GeneralPath) shape).lineTo(-3.684, -21.974);
        ((GeneralPath) shape).lineTo(-9.683, -21.974);
        ((GeneralPath) shape).lineTo(-12.471, -19.187);
        ((GeneralPath) shape).lineTo(-12.471, -5.591);
        ((GeneralPath) shape).lineTo(-9.683, -2.804);
        ((GeneralPath) shape).lineTo(-3.684, -2.804);
        ((GeneralPath) shape).lineTo(-0.13, -6.358);
        ((GeneralPath) shape).lineTo(2.152, -4.075);
        ((GeneralPath) shape).lineTo(-2.25, 0.326);
        ((GeneralPath) shape).lineTo(-11.118, 0.326);
        ((GeneralPath) shape).lineTo(-15.764, -4.32);
        ((GeneralPath) shape).lineTo(-15.764, -20.458);
        ((GeneralPath) shape).lineTo(-11.118, -25.104);
        ((GeneralPath) shape).lineTo(-2.25, -25.104);
        ((GeneralPath) shape).lineTo(2.152, -20.703);
        ((GeneralPath) shape).lineTo(-0.13, -18.421);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(24.191, -4.32);
        ((GeneralPath) shape).lineTo(19.546, 0.326);
        ((GeneralPath) shape).lineTo(10.677, 0.326);
        ((GeneralPath) shape).lineTo(6.032, -4.32);
        ((GeneralPath) shape).lineTo(6.032, -20.458);
        ((GeneralPath) shape).lineTo(10.677, -25.104);
        ((GeneralPath) shape).lineTo(19.546, -25.104);
        ((GeneralPath) shape).lineTo(24.191, -20.458);
        ((GeneralPath) shape).lineTo(24.191, -4.32);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(12.112, -21.974);
        ((GeneralPath) shape).lineTo(9.324, -19.187);
        ((GeneralPath) shape).lineTo(9.324, -5.591);
        ((GeneralPath) shape).lineTo(12.112, -2.804);
        ((GeneralPath) shape).lineTo(18.111, -2.804);
        ((GeneralPath) shape).lineTo(20.899, -5.591);
        ((GeneralPath) shape).lineTo(20.899, -19.187);
        ((GeneralPath) shape).lineTo(18.111, -21.974);
        ((GeneralPath) shape).lineTo(12.112, -21.974);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_7
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(46.117, 0.326);
        ((GeneralPath) shape).lineTo(43.558, 0.326);
        ((GeneralPath) shape).lineTo(31.25, -18.144);
        ((GeneralPath) shape).lineTo(31.25, 0.326);
        ((GeneralPath) shape).lineTo(27.957, 0.326);
        ((GeneralPath) shape).lineTo(27.957, -25.104);
        ((GeneralPath) shape).lineTo(30.516, -25.104);
        ((GeneralPath) shape).lineTo(42.824, -6.635);
        ((GeneralPath) shape).lineTo(42.824, -25.104);
        ((GeneralPath) shape).lineTo(46.117, -25.104);
        ((GeneralPath) shape).lineTo(46.117, 0.326);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_8
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(68.042, 0.326);
        ((GeneralPath) shape).lineTo(65.483, 0.326);
        ((GeneralPath) shape).lineTo(53.176, -18.144);
        ((GeneralPath) shape).lineTo(53.176, 0.326);
        ((GeneralPath) shape).lineTo(49.883, 0.326);
        ((GeneralPath) shape).lineTo(49.883, -25.104);
        ((GeneralPath) shape).lineTo(52.442, -25.104);
        ((GeneralPath) shape).lineTo(64.75, -6.635);
        ((GeneralPath) shape).lineTo(64.75, -25.104);
        ((GeneralPath) shape).lineTo(68.042, -25.104);
        ((GeneralPath) shape).lineTo(68.042, 0.326);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_9
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(75.101, -21.974);
        ((GeneralPath) shape).lineTo(75.101, -13.954);
        ((GeneralPath) shape).lineTo(86.251, -13.954);
        ((GeneralPath) shape).lineTo(86.251, -10.824);
        ((GeneralPath) shape).lineTo(75.101, -10.824);
        ((GeneralPath) shape).lineTo(75.101, -2.804);
        ((GeneralPath) shape).lineTo(89.968, -2.804);
        ((GeneralPath) shape).lineTo(89.968, 0.326);
        ((GeneralPath) shape).lineTo(71.808, 0.326);
        ((GeneralPath) shape).lineTo(71.808, -25.104);
        ((GeneralPath) shape).lineTo(89.968, -25.104);
        ((GeneralPath) shape).lineTo(89.968, -21.974);
        ((GeneralPath) shape).lineTo(75.101, -21.974);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_10
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(109.497, -18.421);
        ((GeneralPath) shape).lineTo(105.943, -21.974);
        ((GeneralPath) shape).lineTo(99.945, -21.974);
        ((GeneralPath) shape).lineTo(97.157, -19.187);
        ((GeneralPath) shape).lineTo(97.157, -5.591);
        ((GeneralPath) shape).lineTo(99.945, -2.804);
        ((GeneralPath) shape).lineTo(105.943, -2.804);
        ((GeneralPath) shape).lineTo(109.497, -6.358);
        ((GeneralPath) shape).lineTo(111.779, -4.075);
        ((GeneralPath) shape).lineTo(107.378, 0.326);
        ((GeneralPath) shape).lineTo(98.51, 0.326);
        ((GeneralPath) shape).lineTo(93.864, -4.32);
        ((GeneralPath) shape).lineTo(93.864, -20.458);
        ((GeneralPath) shape).lineTo(98.51, -25.104);
        ((GeneralPath) shape).lineTo(107.378, -25.104);
        ((GeneralPath) shape).lineTo(111.779, -20.703);
        ((GeneralPath) shape).lineTo(109.497, -18.421);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_19_0_11
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(123.093, 0.326);
        ((GeneralPath) shape).lineTo(123.093, -21.974);
        ((GeneralPath) shape).lineTo(115.659, -21.974);
        ((GeneralPath) shape).lineTo(115.659, -25.104);
        ((GeneralPath) shape).lineTo(133.819, -25.104);
        ((GeneralPath) shape).lineTo(133.819, -21.974);
        ((GeneralPath) shape).lineTo(126.386, -21.974);
        ((GeneralPath) shape).lineTo(126.386, 0.326);
        ((GeneralPath) shape).lineTo(123.093, 0.326);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_19
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_20

        // _0_0_20_0
        shape = new Rectangle2D.Double(471.4360046386719, 935.06201171875, 156.447998046875, 39.029998779296875);
        g.setPaint(new Color(0x00E100));
        g.fill(shape);

        // _0_0_20_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(629.385, 975.592);
        ((GeneralPath) shape).lineTo(469.936, 975.592);
        ((GeneralPath) shape).lineTo(469.936, 933.562);
        ((GeneralPath) shape).lineTo(629.385, 933.562);
        ((GeneralPath) shape).lineTo(629.385, 975.592);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(472.936, 936.562);
        ((GeneralPath) shape).lineTo(472.936, 972.592);
        ((GeneralPath) shape).lineTo(626.385, 972.592);
        ((GeneralPath) shape).lineTo(626.385, 936.562);
        ((GeneralPath) shape).lineTo(472.936, 936.562);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_20
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_21

        // _0_0_21_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(629.385, 1015.23);
        ((GeneralPath) shape).lineTo(469.936, 1015.23);
        ((GeneralPath) shape).lineTo(469.936, 973.197);
        ((GeneralPath) shape).lineTo(629.385, 973.197);
        ((GeneralPath) shape).lineTo(629.385, 1015.23);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(472.936, 976.197);
        ((GeneralPath) shape).lineTo(472.936, 1012.23);
        ((GeneralPath) shape).lineTo(626.385, 1012.23);
        ((GeneralPath) shape).lineTo(626.385, 976.197);
        ((GeneralPath) shape).lineTo(472.936, 976.197);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_21
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_22

        // _0_0_22_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(629.385, 1054.93);
        ((GeneralPath) shape).lineTo(469.936, 1054.93);
        ((GeneralPath) shape).lineTo(469.936, 1012.9);
        ((GeneralPath) shape).lineTo(629.385, 1012.9);
        ((GeneralPath) shape).lineTo(629.385, 1054.93);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(472.936, 1015.9);
        ((GeneralPath) shape).lineTo(472.936, 1051.93);
        ((GeneralPath) shape).lineTo(626.385, 1051.93);
        ((GeneralPath) shape).lineTo(626.385, 1015.9);
        ((GeneralPath) shape).lineTo(472.936, 1015.9);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_22
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_23

        // _0_0_23_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(473.025, 975.592);
        ((GeneralPath) shape).lineTo(313.577, 975.592);
        ((GeneralPath) shape).lineTo(313.577, 933.562);
        ((GeneralPath) shape).lineTo(473.025, 933.562);
        ((GeneralPath) shape).lineTo(473.025, 975.592);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(316.577, 936.562);
        ((GeneralPath) shape).lineTo(316.577, 972.592);
        ((GeneralPath) shape).lineTo(470.025, 972.592);
        ((GeneralPath) shape).lineTo(470.025, 936.562);
        ((GeneralPath) shape).lineTo(316.577, 936.562);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_23
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_24

        // _0_0_24_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(473.025, 1015.23);
        ((GeneralPath) shape).lineTo(313.577, 1015.23);
        ((GeneralPath) shape).lineTo(313.577, 973.197);
        ((GeneralPath) shape).lineTo(473.025, 973.197);
        ((GeneralPath) shape).lineTo(473.025, 1015.23);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(316.577, 976.197);
        ((GeneralPath) shape).lineTo(316.577, 1012.23);
        ((GeneralPath) shape).lineTo(470.025, 1012.23);
        ((GeneralPath) shape).lineTo(470.025, 976.197);
        ((GeneralPath) shape).lineTo(316.577, 976.197);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_24
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_25

        // _0_0_25_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(473.025, 1054.93);
        ((GeneralPath) shape).lineTo(313.577, 1054.93);
        ((GeneralPath) shape).lineTo(313.577, 1012.9);
        ((GeneralPath) shape).lineTo(473.025, 1012.9);
        ((GeneralPath) shape).lineTo(473.025, 1054.93);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(316.577, 1015.9);
        ((GeneralPath) shape).lineTo(316.577, 1051.93);
        ((GeneralPath) shape).lineTo(470.025, 1051.93);
        ((GeneralPath) shape).lineTo(470.025, 1015.9);
        ((GeneralPath) shape).lineTo(316.577, 1015.9);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_25
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_26

        // _0_0_26_0
        shape = new Rectangle2D.Double(158.23399353027344, 935.06201171875, 156.447998046875, 39.029998779296875);
        g.setPaint(new Color(0x00E100));
        g.fill(shape);

        // _0_0_26_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(316.183, 975.592);
        ((GeneralPath) shape).lineTo(156.734, 975.592);
        ((GeneralPath) shape).lineTo(156.734, 933.562);
        ((GeneralPath) shape).lineTo(316.183, 933.562);
        ((GeneralPath) shape).lineTo(316.183, 975.592);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(159.734, 936.562);
        ((GeneralPath) shape).lineTo(159.734, 972.592);
        ((GeneralPath) shape).lineTo(313.183, 972.592);
        ((GeneralPath) shape).lineTo(313.183, 936.562);
        ((GeneralPath) shape).lineTo(159.734, 936.562);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_26
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_27

        // _0_0_27_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(316.183, 1015.23);
        ((GeneralPath) shape).lineTo(156.734, 1015.23);
        ((GeneralPath) shape).lineTo(156.734, 973.197);
        ((GeneralPath) shape).lineTo(316.183, 973.197);
        ((GeneralPath) shape).lineTo(316.183, 1015.23);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(159.734, 976.197);
        ((GeneralPath) shape).lineTo(159.734, 1012.23);
        ((GeneralPath) shape).lineTo(313.183, 1012.23);
        ((GeneralPath) shape).lineTo(313.183, 976.197);
        ((GeneralPath) shape).lineTo(159.734, 976.197);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_27
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_28

        // _0_0_28_0
        shape = new Rectangle2D.Double(158.23399353027344, 1014.4000244140625, 156.447998046875, 39.029998779296875);
        g.setPaint(new Color(0x00E100));
        g.fill(shape);

        // _0_0_28_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(316.183, 1054.93);
        ((GeneralPath) shape).lineTo(156.734, 1054.93);
        ((GeneralPath) shape).lineTo(156.734, 1012.9);
        ((GeneralPath) shape).lineTo(316.183, 1012.9);
        ((GeneralPath) shape).lineTo(316.183, 1054.93);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(159.734, 1015.9);
        ((GeneralPath) shape).lineTo(159.734, 1051.93);
        ((GeneralPath) shape).lineTo(313.183, 1051.93);
        ((GeneralPath) shape).lineTo(313.183, 1015.9);
        ((GeneralPath) shape).lineTo(159.734, 1015.9);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_28
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_29

        // _0_0_29_0
        shape = new Rectangle2D.Double(1.3919999599456787, 935.06201171875, 156.447998046875, 39.029998779296875);
        g.setPaint(new Color(0x00E100));
        g.fill(shape);

        // _0_0_29_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(159.34, 975.592);
        ((GeneralPath) shape).lineTo(-0.108, 975.592);
        ((GeneralPath) shape).lineTo(-0.108, 933.562);
        ((GeneralPath) shape).lineTo(159.34, 933.562);
        ((GeneralPath) shape).lineTo(159.34, 975.592);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(2.892, 936.562);
        ((GeneralPath) shape).lineTo(2.892, 972.592);
        ((GeneralPath) shape).lineTo(156.34, 972.592);
        ((GeneralPath) shape).lineTo(156.34, 936.562);
        ((GeneralPath) shape).lineTo(2.892, 936.562);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_29
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_30

        // _0_0_30_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(159.34, 1015.23);
        ((GeneralPath) shape).lineTo(-0.108, 1015.23);
        ((GeneralPath) shape).lineTo(-0.108, 973.197);
        ((GeneralPath) shape).lineTo(159.34, 973.197);
        ((GeneralPath) shape).lineTo(159.34, 1015.23);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(2.892, 976.197);
        ((GeneralPath) shape).lineTo(2.892, 1012.23);
        ((GeneralPath) shape).lineTo(156.34, 1012.23);
        ((GeneralPath) shape).lineTo(156.34, 976.197);
        ((GeneralPath) shape).lineTo(2.892, 976.197);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_30
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, -512.362f));

        // _0_0_31

        // _0_0_31_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(159.34, 1054.93);
        ((GeneralPath) shape).lineTo(-0.108, 1054.93);
        ((GeneralPath) shape).lineTo(-0.108, 1012.9);
        ((GeneralPath) shape).lineTo(159.34, 1012.9);
        ((GeneralPath) shape).lineTo(159.34, 1054.93);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(2.892, 1015.9);
        ((GeneralPath) shape).lineTo(2.892, 1051.93);
        ((GeneralPath) shape).lineTo(156.34, 1051.93);
        ((GeneralPath) shape).lineTo(156.34, 1015.9);
        ((GeneralPath) shape).lineTo(2.892, 1015.9);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_31
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 141.978f, 450.115f));

        // _0_0_32

        // _0_0_32_0

        // _0_0_32_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-121.758, -14.729);
        ((GeneralPath) shape).lineTo(-126.28, -7.227);
        ((GeneralPath) shape).lineTo(-117.5, -7.227);
        ((GeneralPath) shape).lineTo(-121.758, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-122.725, -17.721);
        ((GeneralPath) shape).lineTo(-121.033, -17.721);
        ((GeneralPath) shape).lineTo(-115.015, -6.881);
        ((GeneralPath) shape).lineTo(-115.015, 0.23);
        ((GeneralPath) shape).lineTo(-117.5, 0.23);
        ((GeneralPath) shape).lineTo(-117.5, -5.017);
        ((GeneralPath) shape).lineTo(-126.28, -5.017);
        ((GeneralPath) shape).lineTo(-126.28, 0.23);
        ((GeneralPath) shape).lineTo(-128.766, 0.23);
        ((GeneralPath) shape).lineTo(-128.766, -6.824);
        ((GeneralPath) shape).lineTo(-122.725, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_0_32_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-112.782, -17.721);
        ((GeneralPath) shape).lineTo(-103.243, -17.721);
        ((GeneralPath) shape).lineTo(-99.963, -14.442);
        ((GeneralPath) shape).lineTo(-99.963, -10.92);
        ((GeneralPath) shape).lineTo(-103.243, -7.641);
        ((GeneralPath) shape).lineTo(-110.458, -7.641);
        ((GeneralPath) shape).lineTo(-110.458, 0.23);
        ((GeneralPath) shape).lineTo(-112.782, 0.23);
        ((GeneralPath) shape).lineTo(-112.782, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-110.458, -15.512);
        ((GeneralPath) shape).lineTo(-110.458, -9.85);
        ((GeneralPath) shape).lineTo(-104.256, -9.85);
        ((GeneralPath) shape).lineTo(-102.288, -11.818);
        ((GeneralPath) shape).lineTo(-102.288, -13.544);
        ((GeneralPath) shape).lineTo(-104.256, -15.512);
        ((GeneralPath) shape).lineTo(-110.458, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_32_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-91.506, -17.721);
        ((GeneralPath) shape).lineTo(-89.699, -17.721);
        ((GeneralPath) shape).lineTo(-85.096, -10.794);
        ((GeneralPath) shape).lineTo(-80.493, -17.721);
        ((GeneralPath) shape).lineTo(-78.686, -17.721);
        ((GeneralPath) shape).lineTo(-78.686, 0.23);
        ((GeneralPath) shape).lineTo(-81.011, 0.23);
        ((GeneralPath) shape).lineTo(-81.011, -12.808);
        ((GeneralPath) shape).lineTo(-84.198, -8.032);
        ((GeneralPath) shape).lineTo(-85.994, -8.032);
        ((GeneralPath) shape).lineTo(-89.181, -12.808);
        ((GeneralPath) shape).lineTo(-89.181, 0.23);
        ((GeneralPath) shape).lineTo(-91.506, 0.23);
        ((GeneralPath) shape).lineTo(-91.506, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_32_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-69.527, -14.729);
        ((GeneralPath) shape).lineTo(-74.049, -7.227);
        ((GeneralPath) shape).lineTo(-65.269, -7.227);
        ((GeneralPath) shape).lineTo(-69.527, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-70.493, -17.721);
        ((GeneralPath) shape).lineTo(-68.802, -17.721);
        ((GeneralPath) shape).lineTo(-62.783, -6.881);
        ((GeneralPath) shape).lineTo(-62.783, 0.23);
        ((GeneralPath) shape).lineTo(-65.269, 0.23);
        ((GeneralPath) shape).lineTo(-65.269, -5.017);
        ((GeneralPath) shape).lineTo(-74.049, -5.017);
        ((GeneralPath) shape).lineTo(-74.049, 0.23);
        ((GeneralPath) shape).lineTo(-76.535, 0.23);
        ((GeneralPath) shape).lineTo(-76.535, -6.824);
        ((GeneralPath) shape).lineTo(-70.493, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_32_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-57.272, 0.23);
        ((GeneralPath) shape).lineTo(-60.378, -2.877);
        ((GeneralPath) shape).lineTo(-58.767, -4.488);
        ((GeneralPath) shape).lineTo(-56.259, -1.979);
        ((GeneralPath) shape).lineTo(-52.024, -1.979);
        ((GeneralPath) shape).lineTo(-50.056, -3.947);
        ((GeneralPath) shape).lineTo(-50.056, -5.673);
        ((GeneralPath) shape).lineTo(-52.024, -7.641);
        ((GeneralPath) shape).lineTo(-57.272, -7.641);
        ((GeneralPath) shape).lineTo(-60.551, -10.92);
        ((GeneralPath) shape).lineTo(-60.551, -14.442);
        ((GeneralPath) shape).lineTo(-57.272, -17.721);
        ((GeneralPath) shape).lineTo(-51.012, -17.721);
        ((GeneralPath) shape).lineTo(-47.905, -14.614);
        ((GeneralPath) shape).lineTo(-49.516, -13.003);
        ((GeneralPath) shape).lineTo(-52.024, -15.512);
        ((GeneralPath) shape).lineTo(-56.259, -15.512);
        ((GeneralPath) shape).lineTo(-58.227, -13.544);
        ((GeneralPath) shape).lineTo(-58.227, -11.818);
        ((GeneralPath) shape).lineTo(-56.259, -9.85);
        ((GeneralPath) shape).lineTo(-51.012, -9.85);
        ((GeneralPath) shape).lineTo(-47.732, -6.571);
        ((GeneralPath) shape).lineTo(-47.732, -3.049);
        ((GeneralPath) shape).lineTo(-51.012, 0.23);
        ((GeneralPath) shape).lineTo(-57.272, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_32_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.827, 0.23);
        ((GeneralPath) shape).lineTo(-39.827, -15.512);
        ((GeneralPath) shape).lineTo(-45.074, -15.512);
        ((GeneralPath) shape).lineTo(-45.074, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -15.512);
        ((GeneralPath) shape).lineTo(-37.502, -15.512);
        ((GeneralPath) shape).lineTo(-37.502, 0.23);
        ((GeneralPath) shape).lineTo(-39.827, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_32_0_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-27.272, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -9.85);
        ((GeneralPath) shape).lineTo(-19.401, -9.85);
        ((GeneralPath) shape).lineTo(-19.401, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_32_0_7
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-14.327, -17.721);
        ((GeneralPath) shape).lineTo(-4.787, -17.721);
        ((GeneralPath) shape).lineTo(-1.507, -14.442);
        ((GeneralPath) shape).lineTo(-1.507, -10.92);
        ((GeneralPath) shape).lineTo(-4.787, -7.641);
        ((GeneralPath) shape).lineTo(-5.8, -7.641);
        ((GeneralPath) shape).lineTo(-1.105, -0.575);
        ((GeneralPath) shape).lineTo(-3.003, 0.679);
        ((GeneralPath) shape).lineTo(-8.55, -7.641);
        ((GeneralPath) shape).lineTo(-12.002, -7.641);
        ((GeneralPath) shape).lineTo(-12.002, 0.23);
        ((GeneralPath) shape).lineTo(-14.327, 0.23);
        ((GeneralPath) shape).lineTo(-14.327, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-12.002, -15.512);
        ((GeneralPath) shape).lineTo(-12.002, -9.85);
        ((GeneralPath) shape).lineTo(-5.8, -9.85);
        ((GeneralPath) shape).lineTo(-3.832, -11.818);
        ((GeneralPath) shape).lineTo(-3.832, -13.544);
        ((GeneralPath) shape).lineTo(-5.8, -15.512);
        ((GeneralPath) shape).lineTo(-12.002, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_32
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 264.264f, 450.115f));

        // _0_0_33

        // _0_0_33_0

        // _0_0_33_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-47.732, -1.979);
        ((GeneralPath) shape).lineTo(-47.732, 0.23);
        ((GeneralPath) shape).lineTo(-60.551, 0.23);
        ((GeneralPath) shape).lineTo(-60.551, -17.721);
        ((GeneralPath) shape).lineTo(-58.227, -17.721);
        ((GeneralPath) shape).lineTo(-58.227, -1.979);
        ((GeneralPath) shape).lineTo(-47.732, -1.979);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_33_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-32.255, 0.23);
        ((GeneralPath) shape).lineTo(-34.061, 0.23);
        ((GeneralPath) shape).lineTo(-42.749, -12.808);
        ((GeneralPath) shape).lineTo(-42.749, 0.23);
        ((GeneralPath) shape).lineTo(-45.074, 0.23);
        ((GeneralPath) shape).lineTo(-45.074, -17.721);
        ((GeneralPath) shape).lineTo(-43.267, -17.721);
        ((GeneralPath) shape).lineTo(-34.579, -4.683);
        ((GeneralPath) shape).lineTo(-34.579, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_33_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-23.095, -14.729);
        ((GeneralPath) shape).lineTo(-27.617, -7.227);
        ((GeneralPath) shape).lineTo(-18.837, -7.227);
        ((GeneralPath) shape).lineTo(-23.095, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-24.062, -17.721);
        ((GeneralPath) shape).lineTo(-22.37, -17.721);
        ((GeneralPath) shape).lineTo(-16.352, -6.881);
        ((GeneralPath) shape).lineTo(-16.352, 0.23);
        ((GeneralPath) shape).lineTo(-18.837, 0.23);
        ((GeneralPath) shape).lineTo(-18.837, -5.017);
        ((GeneralPath) shape).lineTo(-27.617, -5.017);
        ((GeneralPath) shape).lineTo(-27.617, 0.23);
        ((GeneralPath) shape).lineTo(-30.103, 0.23);
        ((GeneralPath) shape).lineTo(-30.103, -6.824);
        ((GeneralPath) shape).lineTo(-24.062, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_33_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-13.44, -10.0);
        ((GeneralPath) shape).lineTo(-13.452, -17.56);
        ((GeneralPath) shape).lineTo(-11.289, -17.56);
        ((GeneralPath) shape).lineTo(-11.289, -10.759);
        ((GeneralPath) shape).lineTo(-7.779, -3.015);
        ((GeneralPath) shape).lineTo(-4.235, -10.759);
        ((GeneralPath) shape).lineTo(-4.235, -17.537);
        ((GeneralPath) shape).lineTo(-1.979, -17.537);
        ((GeneralPath) shape).lineTo(-1.991, -9.988);
        ((GeneralPath) shape).lineTo(-6.962, 0.23);
        ((GeneralPath) shape).lineTo(-8.596, 0.23);
        ((GeneralPath) shape).lineTo(-13.44, -10.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_33
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 424.264f, 452.257f));

        // _0_0_34

        // _0_0_34_0

        // _0_0_34_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-59.872, -10.0);
        ((GeneralPath) shape).lineTo(-59.884, -17.56);
        ((GeneralPath) shape).lineTo(-57.72, -17.56);
        ((GeneralPath) shape).lineTo(-57.72, -10.759);
        ((GeneralPath) shape).lineTo(-54.211, -3.015);
        ((GeneralPath) shape).lineTo(-50.666, -10.759);
        ((GeneralPath) shape).lineTo(-50.666, -17.537);
        ((GeneralPath) shape).lineTo(-48.411, -17.537);
        ((GeneralPath) shape).lineTo(-48.422, -9.988);
        ((GeneralPath) shape).lineTo(-53.394, 0.23);
        ((GeneralPath) shape).lineTo(-55.028, 0.23);
        ((GeneralPath) shape).lineTo(-59.872, -10.0);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_34_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-32.255, 0.23);
        ((GeneralPath) shape).lineTo(-34.061, 0.23);
        ((GeneralPath) shape).lineTo(-42.749, -12.808);
        ((GeneralPath) shape).lineTo(-42.749, 0.23);
        ((GeneralPath) shape).lineTo(-45.074, 0.23);
        ((GeneralPath) shape).lineTo(-45.074, -17.721);
        ((GeneralPath) shape).lineTo(-43.267, -17.721);
        ((GeneralPath) shape).lineTo(-34.579, -4.683);
        ((GeneralPath) shape).lineTo(-34.579, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_34_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-23.095, -14.729);
        ((GeneralPath) shape).lineTo(-27.617, -7.227);
        ((GeneralPath) shape).lineTo(-18.837, -7.227);
        ((GeneralPath) shape).lineTo(-23.095, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-24.062, -17.721);
        ((GeneralPath) shape).lineTo(-22.37, -17.721);
        ((GeneralPath) shape).lineTo(-16.352, -6.881);
        ((GeneralPath) shape).lineTo(-16.352, 0.23);
        ((GeneralPath) shape).lineTo(-18.837, 0.23);
        ((GeneralPath) shape).lineTo(-18.837, -5.017);
        ((GeneralPath) shape).lineTo(-27.617, -5.017);
        ((GeneralPath) shape).lineTo(-27.617, 0.23);
        ((GeneralPath) shape).lineTo(-30.103, 0.23);
        ((GeneralPath) shape).lineTo(-30.103, -6.824);
        ((GeneralPath) shape).lineTo(-24.062, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_34_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-13.44, -10.0);
        ((GeneralPath) shape).lineTo(-13.452, -17.56);
        ((GeneralPath) shape).lineTo(-11.289, -17.56);
        ((GeneralPath) shape).lineTo(-11.289, -10.759);
        ((GeneralPath) shape).lineTo(-7.779, -3.015);
        ((GeneralPath) shape).lineTo(-4.235, -10.759);
        ((GeneralPath) shape).lineTo(-4.235, -17.537);
        ((GeneralPath) shape).lineTo(-1.979, -17.537);
        ((GeneralPath) shape).lineTo(-1.991, -9.988);
        ((GeneralPath) shape).lineTo(-6.962, 0.23);
        ((GeneralPath) shape).lineTo(-8.596, 0.23);
        ((GeneralPath) shape).lineTo(-13.44, -10.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_34
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 365.693f, 490.115f));

        // _0_0_35

        // _0_0_35_0

        // _0_0_35_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-44.395, -10.0);
        ((GeneralPath) shape).lineTo(-44.406, -17.56);
        ((GeneralPath) shape).lineTo(-42.243, -17.56);
        ((GeneralPath) shape).lineTo(-42.243, -10.759);
        ((GeneralPath) shape).lineTo(-38.733, -3.015);
        ((GeneralPath) shape).lineTo(-35.189, -10.759);
        ((GeneralPath) shape).lineTo(-35.189, -17.537);
        ((GeneralPath) shape).lineTo(-32.934, -17.537);
        ((GeneralPath) shape).lineTo(-32.945, -9.988);
        ((GeneralPath) shape).lineTo(-37.916, 0.23);
        ((GeneralPath) shape).lineTo(-39.55, 0.23);
        ((GeneralPath) shape).lineTo(-44.395, -10.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_35_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-16.363, -16.916);
        ((GeneralPath) shape).lineTo(-28.101, 0.679);
        ((GeneralPath) shape).lineTo(-29.999, -0.575);
        ((GeneralPath) shape).lineTo(-18.262, -18.181);
        ((GeneralPath) shape).lineTo(-16.363, -16.916);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_35_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-10.84, 0.23);
        ((GeneralPath) shape).lineTo(-13.947, -2.877);
        ((GeneralPath) shape).lineTo(-12.336, -4.488);
        ((GeneralPath) shape).lineTo(-9.827, -1.979);
        ((GeneralPath) shape).lineTo(-5.593, -1.979);
        ((GeneralPath) shape).lineTo(-3.625, -3.947);
        ((GeneralPath) shape).lineTo(-3.625, -5.673);
        ((GeneralPath) shape).lineTo(-5.593, -7.641);
        ((GeneralPath) shape).lineTo(-10.84, -7.641);
        ((GeneralPath) shape).lineTo(-14.119, -10.92);
        ((GeneralPath) shape).lineTo(-14.119, -14.442);
        ((GeneralPath) shape).lineTo(-10.84, -17.721);
        ((GeneralPath) shape).lineTo(-4.58, -17.721);
        ((GeneralPath) shape).lineTo(-1.473, -14.614);
        ((GeneralPath) shape).lineTo(-3.084, -13.003);
        ((GeneralPath) shape).lineTo(-5.593, -15.512);
        ((GeneralPath) shape).lineTo(-9.827, -15.512);
        ((GeneralPath) shape).lineTo(-11.795, -13.544);
        ((GeneralPath) shape).lineTo(-11.795, -11.818);
        ((GeneralPath) shape).lineTo(-9.827, -9.85);
        ((GeneralPath) shape).lineTo(-4.58, -9.85);
        ((GeneralPath) shape).lineTo(-1.3, -6.571);
        ((GeneralPath) shape).lineTo(-1.3, -3.049);
        ((GeneralPath) shape).lineTo(-4.58, 0.23);
        ((GeneralPath) shape).lineTo(-10.84, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_35
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 365.693f, 530.115f));

        // _0_0_36

        // _0_0_36_0

        // _0_0_36_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-38.572, -14.729);
        ((GeneralPath) shape).lineTo(-43.095, -7.227);
        ((GeneralPath) shape).lineTo(-34.315, -7.227);
        ((GeneralPath) shape).lineTo(-38.572, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-39.539, -17.721);
        ((GeneralPath) shape).lineTo(-37.847, -17.721);
        ((GeneralPath) shape).lineTo(-31.829, -6.881);
        ((GeneralPath) shape).lineTo(-31.829, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, -6.824);
        ((GeneralPath) shape).lineTo(-39.539, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_36_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-16.778, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -17.721);
        ((GeneralPath) shape).lineTo(-27.272, -17.721);
        ((GeneralPath) shape).lineTo(-27.272, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, -1.979);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_36_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.872, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_36
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 569.693f, 450.115f));

        // _0_0_37

        // _0_0_37_0

        // _0_0_37_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-38.572, -14.729);
        ((GeneralPath) shape).lineTo(-43.095, -7.227);
        ((GeneralPath) shape).lineTo(-34.315, -7.227);
        ((GeneralPath) shape).lineTo(-38.572, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-39.539, -17.721);
        ((GeneralPath) shape).lineTo(-37.847, -17.721);
        ((GeneralPath) shape).lineTo(-31.829, -6.881);
        ((GeneralPath) shape).lineTo(-31.829, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, -6.824);
        ((GeneralPath) shape).lineTo(-39.539, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_0_37_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-16.363, -16.916);
        ((GeneralPath) shape).lineTo(-28.101, 0.679);
        ((GeneralPath) shape).lineTo(-29.999, -0.575);
        ((GeneralPath) shape).lineTo(-18.262, -18.181);
        ((GeneralPath) shape).lineTo(-16.363, -16.916);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_37_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.872, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_37
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 523.693f, 490.115f));

        // _0_0_38

        // _0_0_38_0

        // _0_0_38_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-41.794, 0.23);
        ((GeneralPath) shape).lineTo(-44.901, -2.877);
        ((GeneralPath) shape).lineTo(-43.29, -4.488);
        ((GeneralPath) shape).lineTo(-40.782, -1.979);
        ((GeneralPath) shape).lineTo(-36.547, -1.979);
        ((GeneralPath) shape).lineTo(-34.579, -3.947);
        ((GeneralPath) shape).lineTo(-34.579, -5.673);
        ((GeneralPath) shape).lineTo(-36.547, -7.641);
        ((GeneralPath) shape).lineTo(-41.794, -7.641);
        ((GeneralPath) shape).lineTo(-45.074, -10.92);
        ((GeneralPath) shape).lineTo(-45.074, -14.442);
        ((GeneralPath) shape).lineTo(-41.794, -17.721);
        ((GeneralPath) shape).lineTo(-35.534, -17.721);
        ((GeneralPath) shape).lineTo(-32.427, -14.614);
        ((GeneralPath) shape).lineTo(-34.038, -13.003);
        ((GeneralPath) shape).lineTo(-36.547, -15.512);
        ((GeneralPath) shape).lineTo(-40.782, -15.512);
        ((GeneralPath) shape).lineTo(-42.749, -13.544);
        ((GeneralPath) shape).lineTo(-42.749, -11.818);
        ((GeneralPath) shape).lineTo(-40.782, -9.85);
        ((GeneralPath) shape).lineTo(-35.534, -9.85);
        ((GeneralPath) shape).lineTo(-32.255, -6.571);
        ((GeneralPath) shape).lineTo(-32.255, -3.049);
        ((GeneralPath) shape).lineTo(-35.534, 0.23);
        ((GeneralPath) shape).lineTo(-41.794, 0.23);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_38_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-29.597, -17.721);
        ((GeneralPath) shape).lineTo(-20.057, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, -14.442);
        ((GeneralPath) shape).lineTo(-16.778, -10.92);
        ((GeneralPath) shape).lineTo(-20.057, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-27.272, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -9.85);
        ((GeneralPath) shape).lineTo(-21.07, -9.85);
        ((GeneralPath) shape).lineTo(-19.102, -11.818);
        ((GeneralPath) shape).lineTo(-19.102, -13.544);
        ((GeneralPath) shape).lineTo(-21.07, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_38_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-13.763, -17.79);
        ((GeneralPath) shape).lineTo(-4.948, -17.79);
        ((GeneralPath) shape).lineTo(-1.669, -14.488);
        ((GeneralPath) shape).lineTo(-1.669, -3.026);
        ((GeneralPath) shape).lineTo(-4.948, 0.276);
        ((GeneralPath) shape).lineTo(-13.763, 0.276);
        ((GeneralPath) shape).lineTo(-13.763, -17.79);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-11.519, -15.592);
        ((GeneralPath) shape).lineTo(-11.519, -1.864);
        ((GeneralPath) shape).lineTo(-5.984, -1.864);
        ((GeneralPath) shape).lineTo(-4.016, -3.866);
        ((GeneralPath) shape).lineTo(-4.016, -13.59);
        ((GeneralPath) shape).lineTo(-5.984, -15.592);
        ((GeneralPath) shape).lineTo(-11.519, -15.592);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_38
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 211.693f, 490.115f));

        // _0_0_39

        // _0_0_39_0

        // _0_0_39_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-45.074, -17.721);
        ((GeneralPath) shape).lineTo(-42.749, -17.721);
        ((GeneralPath) shape).lineTo(-42.749, -9.85);
        ((GeneralPath) shape).lineTo(-34.579, -9.85);
        ((GeneralPath) shape).lineTo(-34.579, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, 0.23);
        ((GeneralPath) shape).lineTo(-34.579, 0.23);
        ((GeneralPath) shape).lineTo(-34.579, -7.641);
        ((GeneralPath) shape).lineTo(-42.749, -7.641);
        ((GeneralPath) shape).lineTo(-42.749, 0.23);
        ((GeneralPath) shape).lineTo(-45.074, 0.23);
        ((GeneralPath) shape).lineTo(-45.074, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_39_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-29.24, -17.79);
        ((GeneralPath) shape).lineTo(-20.425, -17.79);
        ((GeneralPath) shape).lineTo(-17.146, -14.488);
        ((GeneralPath) shape).lineTo(-17.146, -3.026);
        ((GeneralPath) shape).lineTo(-20.425, 0.276);
        ((GeneralPath) shape).lineTo(-29.24, 0.276);
        ((GeneralPath) shape).lineTo(-29.24, -17.79);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-26.996, -15.592);
        ((GeneralPath) shape).lineTo(-26.996, -1.864);
        ((GeneralPath) shape).lineTo(-21.461, -1.864);
        ((GeneralPath) shape).lineTo(-19.493, -3.866);
        ((GeneralPath) shape).lineTo(-19.493, -13.59);
        ((GeneralPath) shape).lineTo(-21.461, -15.592);
        ((GeneralPath) shape).lineTo(-26.996, -15.592);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_39_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-3.084, -13.003);
        ((GeneralPath) shape).lineTo(-5.593, -15.512);
        ((GeneralPath) shape).lineTo(-9.827, -15.512);
        ((GeneralPath) shape).lineTo(-11.795, -13.544);
        ((GeneralPath) shape).lineTo(-11.795, -3.947);
        ((GeneralPath) shape).lineTo(-9.827, -1.979);
        ((GeneralPath) shape).lineTo(-5.593, -1.979);
        ((GeneralPath) shape).lineTo(-3.625, -3.947);
        ((GeneralPath) shape).lineTo(-3.625, -7.641);
        ((GeneralPath) shape).lineTo(-8.872, -7.641);
        ((GeneralPath) shape).lineTo(-8.872, -9.85);
        ((GeneralPath) shape).lineTo(-1.3, -9.85);
        ((GeneralPath) shape).lineTo(-1.3, -3.049);
        ((GeneralPath) shape).lineTo(-4.58, 0.23);
        ((GeneralPath) shape).lineTo(-10.84, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, -3.049);
        ((GeneralPath) shape).lineTo(-14.119, -14.442);
        ((GeneralPath) shape).lineTo(-10.84, -17.721);
        ((GeneralPath) shape).lineTo(-4.58, -17.721);
        ((GeneralPath) shape).lineTo(-1.473, -14.614);
        ((GeneralPath) shape).lineTo(-3.084, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_39
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 248.121f, 530.115f));

        // _0_0_40

        // _0_0_40_0

        // _0_0_40_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-20.126, -17.537);
        ((GeneralPath) shape).lineTo(-17.825, -17.537);
        ((GeneralPath) shape).lineTo(-17.825, -12.623);
        ((GeneralPath) shape).lineTo(-22.036, -8.412);
        ((GeneralPath) shape).lineTo(-22.036, 0.23);
        ((GeneralPath) shape).lineTo(-24.453, 0.23);
        ((GeneralPath) shape).lineTo(-24.453, -8.412);
        ((GeneralPath) shape).lineTo(-28.549, -12.623);
        ((GeneralPath) shape).lineTo(-28.549, -17.537);
        ((GeneralPath) shape).lineTo(-26.271, -17.537);
        ((GeneralPath) shape).lineTo(-26.271, -13.981);
        ((GeneralPath) shape).lineTo(-23.245, -10.817);
        ((GeneralPath) shape).lineTo(-20.126, -13.981);
        ((GeneralPath) shape).lineTo(-20.126, -17.537);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_0_40_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-13.763, -17.79);
        ((GeneralPath) shape).lineTo(-4.948, -17.79);
        ((GeneralPath) shape).lineTo(-1.669, -14.488);
        ((GeneralPath) shape).lineTo(-1.669, -3.026);
        ((GeneralPath) shape).lineTo(-4.948, 0.276);
        ((GeneralPath) shape).lineTo(-13.763, 0.276);
        ((GeneralPath) shape).lineTo(-13.763, -17.79);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-11.519, -15.592);
        ((GeneralPath) shape).lineTo(-11.519, -1.864);
        ((GeneralPath) shape).lineTo(-5.984, -1.864);
        ((GeneralPath) shape).lineTo(-4.016, -3.866);
        ((GeneralPath) shape).lineTo(-4.016, -13.59);
        ((GeneralPath) shape).lineTo(-5.984, -15.592);
        ((GeneralPath) shape).lineTo(-11.519, -15.592);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_40
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 114.121f, 530.115f));

        // _0_0_41

        // _0_0_41_0

        // _0_0_41_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-64.901, -13.003);
        ((GeneralPath) shape).lineTo(-67.409, -15.512);
        ((GeneralPath) shape).lineTo(-71.644, -15.512);
        ((GeneralPath) shape).lineTo(-73.612, -13.544);
        ((GeneralPath) shape).lineTo(-73.612, -3.947);
        ((GeneralPath) shape).lineTo(-71.644, -1.979);
        ((GeneralPath) shape).lineTo(-67.409, -1.979);
        ((GeneralPath) shape).lineTo(-64.901, -4.488);
        ((GeneralPath) shape).lineTo(-63.29, -2.877);
        ((GeneralPath) shape).lineTo(-66.397, 0.23);
        ((GeneralPath) shape).lineTo(-72.657, 0.23);
        ((GeneralPath) shape).lineTo(-75.936, -3.049);
        ((GeneralPath) shape).lineTo(-75.936, -14.442);
        ((GeneralPath) shape).lineTo(-72.657, -17.721);
        ((GeneralPath) shape).lineTo(-66.397, -17.721);
        ((GeneralPath) shape).lineTo(-63.29, -14.614);
        ((GeneralPath) shape).lineTo(-64.901, -13.003);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_41_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-47.732, -1.979);
        ((GeneralPath) shape).lineTo(-47.732, 0.23);
        ((GeneralPath) shape).lineTo(-60.551, 0.23);
        ((GeneralPath) shape).lineTo(-60.551, -17.721);
        ((GeneralPath) shape).lineTo(-58.227, -17.721);
        ((GeneralPath) shape).lineTo(-58.227, -1.979);
        ((GeneralPath) shape).lineTo(-47.732, -1.979);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_41_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.85, -15.88);
        ((GeneralPath) shape).lineTo(-43.727, -15.88);
        ((GeneralPath) shape).lineTo(-43.727, -17.721);
        ((GeneralPath) shape).lineTo(-33.613, -17.721);
        ((GeneralPath) shape).lineTo(-33.613, -15.88);
        ((GeneralPath) shape).lineTo(-37.525, -15.88);
        ((GeneralPath) shape).lineTo(-37.525, -1.772);
        ((GeneralPath) shape).lineTo(-33.613, -1.772);
        ((GeneralPath) shape).lineTo(-33.613, 0.23);
        ((GeneralPath) shape).lineTo(-43.727, 0.23);
        ((GeneralPath) shape).lineTo(-43.727, -1.772);
        ((GeneralPath) shape).lineTo(-39.85, -1.772);
        ((GeneralPath) shape).lineTo(-39.85, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_41_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-29.597, -17.721);
        ((GeneralPath) shape).lineTo(-27.79, -17.721);
        ((GeneralPath) shape).lineTo(-23.187, -10.794);
        ((GeneralPath) shape).lineTo(-18.584, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, 0.23);
        ((GeneralPath) shape).lineTo(-19.102, 0.23);
        ((GeneralPath) shape).lineTo(-19.102, -12.808);
        ((GeneralPath) shape).lineTo(-22.29, -8.032);
        ((GeneralPath) shape).lineTo(-24.085, -8.032);
        ((GeneralPath) shape).lineTo(-27.272, -12.808);
        ((GeneralPath) shape).lineTo(-27.272, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_41_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-11.047, -7.641);
        ((GeneralPath) shape).lineTo(-11.047, -1.979);
        ((GeneralPath) shape).lineTo(-6.237, -1.979);
        ((GeneralPath) shape).lineTo(-4.269, -3.947);
        ((GeneralPath) shape).lineTo(-4.269, -5.673);
        ((GeneralPath) shape).lineTo(-6.237, -7.641);
        ((GeneralPath) shape).lineTo(-11.047, -7.641);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-13.486, -17.721);
        ((GeneralPath) shape).lineTo(-5.224, -17.721);
        ((GeneralPath) shape).lineTo(-1.945, -14.442);
        ((GeneralPath) shape).lineTo(-1.945, -10.92);
        ((GeneralPath) shape).lineTo(-4.12, -8.745);
        ((GeneralPath) shape).lineTo(-1.945, -6.571);
        ((GeneralPath) shape).lineTo(-1.945, -3.049);
        ((GeneralPath) shape).lineTo(-5.224, 0.23);
        ((GeneralPath) shape).lineTo(-13.486, 0.23);
        ((GeneralPath) shape).lineTo(-13.486, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-11.024, -15.512);
        ((GeneralPath) shape).lineTo(-11.024, -9.85);
        ((GeneralPath) shape).lineTo(-6.237, -9.85);
        ((GeneralPath) shape).lineTo(-4.269, -11.818);
        ((GeneralPath) shape).lineTo(-4.269, -13.544);
        ((GeneralPath) shape).lineTo(-6.237, -15.512);
        ((GeneralPath) shape).lineTo(-11.024, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_41
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 76.475f, 490.115f));

        // _0_0_42

        // _0_0_42_0

        // _0_0_42_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-10.73, -13.003);
        ((GeneralPath) shape).lineTo(-13.239, -15.512);
        ((GeneralPath) shape).lineTo(-17.474, -15.512);
        ((GeneralPath) shape).lineTo(-19.441, -13.544);
        ((GeneralPath) shape).lineTo(-19.441, -3.947);
        ((GeneralPath) shape).lineTo(-17.474, -1.979);
        ((GeneralPath) shape).lineTo(-13.239, -1.979);
        ((GeneralPath) shape).lineTo(-10.73, -4.488);
        ((GeneralPath) shape).lineTo(-9.119, -2.877);
        ((GeneralPath) shape).lineTo(-12.226, 0.23);
        ((GeneralPath) shape).lineTo(-18.486, 0.23);
        ((GeneralPath) shape).lineTo(-21.766, -3.049);
        ((GeneralPath) shape).lineTo(-21.766, -14.442);
        ((GeneralPath) shape).lineTo(-18.486, -17.721);
        ((GeneralPath) shape).lineTo(-12.226, -17.721);
        ((GeneralPath) shape).lineTo(-9.119, -14.614);
        ((GeneralPath) shape).lineTo(-10.73, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_42_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-6.381, -17.721);
        ((GeneralPath) shape).lineTo(-4.574, -17.721);
        ((GeneralPath) shape).lineTo(0.029, -10.794);
        ((GeneralPath) shape).lineTo(4.632, -17.721);
        ((GeneralPath) shape).lineTo(6.438, -17.721);
        ((GeneralPath) shape).lineTo(6.438, 0.23);
        ((GeneralPath) shape).lineTo(4.114, 0.23);
        ((GeneralPath) shape).lineTo(4.114, -12.808);
        ((GeneralPath) shape).lineTo(0.926, -8.032);
        ((GeneralPath) shape).lineTo(-0.869, -8.032);
        ((GeneralPath) shape).lineTo(-4.056, -12.808);
        ((GeneralPath) shape).lineTo(-4.056, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_42_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(9.453, -17.79);
        ((GeneralPath) shape).lineTo(18.268, -17.79);
        ((GeneralPath) shape).lineTo(21.547, -14.488);
        ((GeneralPath) shape).lineTo(21.547, -3.026);
        ((GeneralPath) shape).lineTo(18.268, 0.276);
        ((GeneralPath) shape).lineTo(9.453, 0.276);
        ((GeneralPath) shape).lineTo(9.453, -17.79);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(11.697, -15.592);
        ((GeneralPath) shape).lineTo(11.697, -1.864);
        ((GeneralPath) shape).lineTo(17.232, -1.864);
        ((GeneralPath) shape).lineTo(19.2, -3.866);
        ((GeneralPath) shape).lineTo(19.2, -13.59);
        ((GeneralPath) shape).lineTo(17.232, -15.592);
        ((GeneralPath) shape).lineTo(11.697, -15.592);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_42
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 585.693f, 530.115f));

        // _0_0_43

        // _0_0_43_0

        // _0_0_43_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-70.781, 0.23);
        ((GeneralPath) shape).lineTo(-70.781, -15.512);
        ((GeneralPath) shape).lineTo(-76.028, -15.512);
        ((GeneralPath) shape).lineTo(-76.028, -17.721);
        ((GeneralPath) shape).lineTo(-63.209, -17.721);
        ((GeneralPath) shape).lineTo(-63.209, -15.512);
        ((GeneralPath) shape).lineTo(-68.457, -15.512);
        ((GeneralPath) shape).lineTo(-68.457, 0.23);
        ((GeneralPath) shape).lineTo(-70.781, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_43_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-47.732, -3.049);
        ((GeneralPath) shape).lineTo(-51.012, 0.23);
        ((GeneralPath) shape).lineTo(-57.272, 0.23);
        ((GeneralPath) shape).lineTo(-60.551, -3.049);
        ((GeneralPath) shape).lineTo(-60.551, -14.442);
        ((GeneralPath) shape).lineTo(-57.272, -17.721);
        ((GeneralPath) shape).lineTo(-51.012, -17.721);
        ((GeneralPath) shape).lineTo(-47.732, -14.442);
        ((GeneralPath) shape).lineTo(-47.732, -3.049);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-56.259, -15.512);
        ((GeneralPath) shape).lineTo(-58.227, -13.544);
        ((GeneralPath) shape).lineTo(-58.227, -3.947);
        ((GeneralPath) shape).lineTo(-56.259, -1.979);
        ((GeneralPath) shape).lineTo(-52.024, -1.979);
        ((GeneralPath) shape).lineTo(-50.056, -3.947);
        ((GeneralPath) shape).lineTo(-50.056, -13.544);
        ((GeneralPath) shape).lineTo(-52.024, -15.512);
        ((GeneralPath) shape).lineTo(-56.259, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_43_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-31.841, -16.916);
        ((GeneralPath) shape).lineTo(-43.578, 0.679);
        ((GeneralPath) shape).lineTo(-45.477, -0.575);
        ((GeneralPath) shape).lineTo(-33.739, -18.181);
        ((GeneralPath) shape).lineTo(-31.841, -16.916);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_43_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-18.561, -13.003);
        ((GeneralPath) shape).lineTo(-21.07, -15.512);
        ((GeneralPath) shape).lineTo(-25.304, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -13.544);
        ((GeneralPath) shape).lineTo(-27.272, -3.947);
        ((GeneralPath) shape).lineTo(-25.304, -1.979);
        ((GeneralPath) shape).lineTo(-21.07, -1.979);
        ((GeneralPath) shape).lineTo(-19.102, -3.947);
        ((GeneralPath) shape).lineTo(-19.102, -7.641);
        ((GeneralPath) shape).lineTo(-24.349, -7.641);
        ((GeneralPath) shape).lineTo(-24.349, -9.85);
        ((GeneralPath) shape).lineTo(-16.778, -9.85);
        ((GeneralPath) shape).lineTo(-16.778, -3.049);
        ((GeneralPath) shape).lineTo(-20.057, 0.23);
        ((GeneralPath) shape).lineTo(-26.317, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -3.049);
        ((GeneralPath) shape).lineTo(-29.597, -14.442);
        ((GeneralPath) shape).lineTo(-26.317, -17.721);
        ((GeneralPath) shape).lineTo(-20.057, -17.721);
        ((GeneralPath) shape).lineTo(-16.95, -14.614);
        ((GeneralPath) shape).lineTo(-18.561, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_43_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-7.618, -14.729);
        ((GeneralPath) shape).lineTo(-12.14, -7.227);
        ((GeneralPath) shape).lineTo(-3.36, -7.227);
        ((GeneralPath) shape).lineTo(-7.618, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-8.584, -17.721);
        ((GeneralPath) shape).lineTo(-6.893, -17.721);
        ((GeneralPath) shape).lineTo(-0.875, -6.881);
        ((GeneralPath) shape).lineTo(-0.875, 0.23);
        ((GeneralPath) shape).lineTo(-3.36, 0.23);
        ((GeneralPath) shape).lineTo(-3.36, -5.017);
        ((GeneralPath) shape).lineTo(-12.14, -5.017);
        ((GeneralPath) shape).lineTo(-12.14, 0.23);
        ((GeneralPath) shape).lineTo(-14.626, 0.23);
        ((GeneralPath) shape).lineTo(-14.626, -6.824);
        ((GeneralPath) shape).lineTo(-8.584, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_43
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 291.693f, 490.115f));

        // _0_0_44

        // _0_0_44_0

        // _0_0_44_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-40.218, 0.23);
        ((GeneralPath) shape).lineTo(-53.037, 0.23);
        ((GeneralPath) shape).lineTo(-53.037, -2.842);
        ((GeneralPath) shape).lineTo(-42.542, -11.818);
        ((GeneralPath) shape).lineTo(-42.542, -13.544);
        ((GeneralPath) shape).lineTo(-44.51, -15.512);
        ((GeneralPath) shape).lineTo(-48.745, -15.512);
        ((GeneralPath) shape).lineTo(-51.253, -13.003);
        ((GeneralPath) shape).lineTo(-52.864, -14.614);
        ((GeneralPath) shape).lineTo(-49.757, -17.721);
        ((GeneralPath) shape).lineTo(-43.497, -17.721);
        ((GeneralPath) shape).lineTo(-40.218, -14.442);
        ((GeneralPath) shape).lineTo(-40.218, -10.92);
        ((GeneralPath) shape).lineTo(-50.666, -1.979);
        ((GeneralPath) shape).lineTo(-40.218, -1.979);
        ((GeneralPath) shape).lineTo(-40.218, 0.23);
        ((GeneralPath) shape).closePath();

        g.setPaint(MAGENTA);
        g.fill(shape);

        // _0_0_44_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-24.292, -11.622);
        ((GeneralPath) shape).lineTo(-22.036, -9.355);
        ((GeneralPath) shape).lineTo(-22.036, -3.095);
        ((GeneralPath) shape).lineTo(-25.373, 0.23);
        ((GeneralPath) shape).lineTo(-29.907, 0.23);
        ((GeneralPath) shape).lineTo(-33.244, -3.107);
        ((GeneralPath) shape).lineTo(-33.244, -5.857);
        ((GeneralPath) shape).lineTo(-30.977, -5.857);
        ((GeneralPath) shape).lineTo(-30.977, -4.407);
        ((GeneralPath) shape).lineTo(-28.768, -2.175);
        ((GeneralPath) shape).lineTo(-26.513, -2.175);
        ((GeneralPath) shape).lineTo(-24.602, -4.085);
        ((GeneralPath) shape).lineTo(-24.602, -9.286);
        ((GeneralPath) shape).lineTo(-31.265, -9.286);
        ((GeneralPath) shape).lineTo(-33.244, -11.231);
        ((GeneralPath) shape).lineTo(-33.244, -17.94);
        ((GeneralPath) shape).lineTo(-21.956, -17.94);
        ((GeneralPath) shape).lineTo(-21.956, -15.535);
        ((GeneralPath) shape).lineTo(-30.678, -15.535);
        ((GeneralPath) shape).lineTo(-30.678, -11.622);
        ((GeneralPath) shape).lineTo(-24.292, -11.622);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_44_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-15.213, -3.061);
        ((GeneralPath) shape).lineTo(-15.213, -14.453);
        ((GeneralPath) shape).lineTo(-11.933, -17.733);
        ((GeneralPath) shape).lineTo(-6.686, -17.733);
        ((GeneralPath) shape).lineTo(-3.406, -14.453);
        ((GeneralPath) shape).lineTo(-3.406, -3.061);
        ((GeneralPath) shape).lineTo(-6.686, 0.219);
        ((GeneralPath) shape).lineTo(-11.933, 0.219);
        ((GeneralPath) shape).lineTo(-15.213, -3.061);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-11.427, -15.523);
        ((GeneralPath) shape).lineTo(-13.394, -13.556);
        ((GeneralPath) shape).lineTo(-13.394, -3.958);
        ((GeneralPath) shape).lineTo(-11.427, -1.991);
        ((GeneralPath) shape).lineTo(-7.192, -1.991);
        ((GeneralPath) shape).lineTo(-5.224, -3.958);
        ((GeneralPath) shape).lineTo(-5.224, -13.556);
        ((GeneralPath) shape).lineTo(-7.192, -15.523);
        ((GeneralPath) shape).lineTo(-11.427, -15.523);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_44
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 595.693f, 490.115f));

        // _0_0_45

        // _0_0_45_0

        // _0_0_45_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-34.176, -0.541);
        ((GeneralPath) shape).lineTo(-24.211, -13.671);
        ((GeneralPath) shape).lineTo(-24.223, -15.477);
        ((GeneralPath) shape).lineTo(-33.774, -15.477);
        ((GeneralPath) shape).lineTo(-33.774, -17.687);
        ((GeneralPath) shape).lineTo(-20.955, -17.687);
        ((GeneralPath) shape).lineTo(-20.955, -13.717);
        ((GeneralPath) shape).lineTo(-32.278, 0.713);
        ((GeneralPath) shape).lineTo(-34.176, -0.541);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_45_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-3.095, -14.338);
        ((GeneralPath) shape).lineTo(-5.915, -14.327);
        ((GeneralPath) shape).lineTo(-7.261, -15.512);
        ((GeneralPath) shape).lineTo(-10.092, -15.512);
        ((GeneralPath) shape).lineTo(-12.06, -13.544);
        ((GeneralPath) shape).lineTo(-12.06, -8.895);
        ((GeneralPath) shape).lineTo(-11.104, -9.85);
        ((GeneralPath) shape).lineTo(-6.513, -9.85);
        ((GeneralPath) shape).lineTo(-3.234, -6.571);
        ((GeneralPath) shape).lineTo(-3.234, -3.049);
        ((GeneralPath) shape).lineTo(-6.513, 0.23);
        ((GeneralPath) shape).lineTo(-11.104, 0.23);
        ((GeneralPath) shape).lineTo(-14.384, -3.049);
        ((GeneralPath) shape).lineTo(-14.384, -14.442);
        ((GeneralPath) shape).lineTo(-11.104, -17.721);
        ((GeneralPath) shape).lineTo(-6.49, -17.721);
        ((GeneralPath) shape).lineTo(-3.095, -14.338);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-7.526, -1.979);
        ((GeneralPath) shape).lineTo(-5.558, -3.947);
        ((GeneralPath) shape).lineTo(-5.558, -5.673);
        ((GeneralPath) shape).lineTo(-7.526, -7.641);
        ((GeneralPath) shape).lineTo(-10.092, -7.641);
        ((GeneralPath) shape).lineTo(-12.06, -5.673);
        ((GeneralPath) shape).lineTo(-12.06, -3.947);
        ((GeneralPath) shape).lineTo(-10.092, -1.979);
        ((GeneralPath) shape).lineTo(-7.526, -1.979);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_45
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 443.693f, 490.115f));

        // _0_0_46

        // _0_0_46_0

        // _0_0_46_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-53.612, -0.541);
        ((GeneralPath) shape).lineTo(-43.647, -13.671);
        ((GeneralPath) shape).lineTo(-43.658, -15.477);
        ((GeneralPath) shape).lineTo(-53.209, -15.477);
        ((GeneralPath) shape).lineTo(-53.209, -17.687);
        ((GeneralPath) shape).lineTo(-40.39, -17.687);
        ((GeneralPath) shape).lineTo(-40.39, -13.717);
        ((GeneralPath) shape).lineTo(-51.714, 0.713);
        ((GeneralPath) shape).lineTo(-53.612, -0.541);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_0_46_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-33.866, -3.061);
        ((GeneralPath) shape).lineTo(-33.866, -14.453);
        ((GeneralPath) shape).lineTo(-30.586, -17.733);
        ((GeneralPath) shape).lineTo(-25.339, -17.733);
        ((GeneralPath) shape).lineTo(-22.059, -14.453);
        ((GeneralPath) shape).lineTo(-22.059, -3.061);
        ((GeneralPath) shape).lineTo(-25.339, 0.219);
        ((GeneralPath) shape).lineTo(-30.586, 0.219);
        ((GeneralPath) shape).lineTo(-33.866, -3.061);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-30.08, -15.523);
        ((GeneralPath) shape).lineTo(-32.048, -13.556);
        ((GeneralPath) shape).lineTo(-32.048, -3.958);
        ((GeneralPath) shape).lineTo(-30.08, -1.991);
        ((GeneralPath) shape).lineTo(-25.845, -1.991);
        ((GeneralPath) shape).lineTo(-23.878, -3.958);
        ((GeneralPath) shape).lineTo(-23.878, -13.556);
        ((GeneralPath) shape).lineTo(-25.845, -15.523);
        ((GeneralPath) shape).lineTo(-30.08, -15.523);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_46_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-15.213, -3.061);
        ((GeneralPath) shape).lineTo(-15.213, -14.453);
        ((GeneralPath) shape).lineTo(-11.933, -17.733);
        ((GeneralPath) shape).lineTo(-6.686, -17.733);
        ((GeneralPath) shape).lineTo(-3.406, -14.453);
        ((GeneralPath) shape).lineTo(-3.406, -3.061);
        ((GeneralPath) shape).lineTo(-6.686, 0.219);
        ((GeneralPath) shape).lineTo(-11.933, 0.219);
        ((GeneralPath) shape).lineTo(-15.213, -3.061);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-11.427, -15.523);
        ((GeneralPath) shape).lineTo(-13.394, -13.556);
        ((GeneralPath) shape).lineTo(-13.394, -3.958);
        ((GeneralPath) shape).lineTo(-11.427, -1.991);
        ((GeneralPath) shape).lineTo(-7.192, -1.991);
        ((GeneralPath) shape).lineTo(-5.224, -3.958);
        ((GeneralPath) shape).lineTo(-5.224, -13.556);
        ((GeneralPath) shape).lineTo(-7.192, -15.523);
        ((GeneralPath) shape).lineTo(-11.427, -15.523);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_46
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 453.693f, 530.115f));

        // _0_0_47

        // _0_0_47_0

        // _0_0_47_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-71.955, -14.614);
        ((GeneralPath) shape).lineTo(-68.848, -17.721);
        ((GeneralPath) shape).lineTo(-62.588, -17.721);
        ((GeneralPath) shape).lineTo(-59.308, -14.442);
        ((GeneralPath) shape).lineTo(-59.308, -10.92);
        ((GeneralPath) shape).lineTo(-61.483, -8.745);
        ((GeneralPath) shape).lineTo(-59.308, -6.571);
        ((GeneralPath) shape).lineTo(-59.308, -3.049);
        ((GeneralPath) shape).lineTo(-62.588, 0.23);
        ((GeneralPath) shape).lineTo(-68.848, 0.23);
        ((GeneralPath) shape).lineTo(-71.955, -2.877);
        ((GeneralPath) shape).lineTo(-70.344, -4.488);
        ((GeneralPath) shape).lineTo(-67.835, -1.979);
        ((GeneralPath) shape).lineTo(-63.6, -1.979);
        ((GeneralPath) shape).lineTo(-61.633, -3.947);
        ((GeneralPath) shape).lineTo(-61.633, -5.673);
        ((GeneralPath) shape).lineTo(-63.6, -7.641);
        ((GeneralPath) shape).lineTo(-66.88, -7.641);
        ((GeneralPath) shape).lineTo(-66.88, -9.85);
        ((GeneralPath) shape).lineTo(-63.6, -9.85);
        ((GeneralPath) shape).lineTo(-61.633, -11.818);
        ((GeneralPath) shape).lineTo(-61.633, -13.544);
        ((GeneralPath) shape).lineTo(-63.6, -15.512);
        ((GeneralPath) shape).lineTo(-67.835, -15.512);
        ((GeneralPath) shape).lineTo(-70.344, -13.003);
        ((GeneralPath) shape).lineTo(-71.955, -14.614);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_47_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-52.519, -3.061);
        ((GeneralPath) shape).lineTo(-52.519, -14.453);
        ((GeneralPath) shape).lineTo(-49.239, -17.733);
        ((GeneralPath) shape).lineTo(-43.992, -17.733);
        ((GeneralPath) shape).lineTo(-40.713, -14.453);
        ((GeneralPath) shape).lineTo(-40.713, -3.061);
        ((GeneralPath) shape).lineTo(-43.992, 0.219);
        ((GeneralPath) shape).lineTo(-49.239, 0.219);
        ((GeneralPath) shape).lineTo(-52.519, -3.061);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-48.733, -15.523);
        ((GeneralPath) shape).lineTo(-50.701, -13.556);
        ((GeneralPath) shape).lineTo(-50.701, -3.958);
        ((GeneralPath) shape).lineTo(-48.733, -1.991);
        ((GeneralPath) shape).lineTo(-44.498, -1.991);
        ((GeneralPath) shape).lineTo(-42.531, -3.958);
        ((GeneralPath) shape).lineTo(-42.531, -13.556);
        ((GeneralPath) shape).lineTo(-44.498, -15.523);
        ((GeneralPath) shape).lineTo(-48.733, -15.523);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_47_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-33.866, -3.061);
        ((GeneralPath) shape).lineTo(-33.866, -14.453);
        ((GeneralPath) shape).lineTo(-30.586, -17.733);
        ((GeneralPath) shape).lineTo(-25.339, -17.733);
        ((GeneralPath) shape).lineTo(-22.059, -14.453);
        ((GeneralPath) shape).lineTo(-22.059, -3.061);
        ((GeneralPath) shape).lineTo(-25.339, 0.219);
        ((GeneralPath) shape).lineTo(-30.586, 0.219);
        ((GeneralPath) shape).lineTo(-33.866, -3.061);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-30.08, -15.523);
        ((GeneralPath) shape).lineTo(-32.048, -13.556);
        ((GeneralPath) shape).lineTo(-32.048, -3.958);
        ((GeneralPath) shape).lineTo(-30.08, -1.991);
        ((GeneralPath) shape).lineTo(-25.845, -1.991);
        ((GeneralPath) shape).lineTo(-23.878, -3.958);
        ((GeneralPath) shape).lineTo(-23.878, -13.556);
        ((GeneralPath) shape).lineTo(-25.845, -15.523);
        ((GeneralPath) shape).lineTo(-30.08, -15.523);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_0_47_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-15.213, -3.061);
        ((GeneralPath) shape).lineTo(-15.213, -14.453);
        ((GeneralPath) shape).lineTo(-11.933, -17.733);
        ((GeneralPath) shape).lineTo(-6.686, -17.733);
        ((GeneralPath) shape).lineTo(-3.406, -14.453);
        ((GeneralPath) shape).lineTo(-3.406, -3.061);
        ((GeneralPath) shape).lineTo(-6.686, 0.219);
        ((GeneralPath) shape).lineTo(-11.933, 0.219);
        ((GeneralPath) shape).lineTo(-15.213, -3.061);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-11.427, -15.523);
        ((GeneralPath) shape).lineTo(-13.394, -13.556);
        ((GeneralPath) shape).lineTo(-13.394, -3.958);
        ((GeneralPath) shape).lineTo(-11.427, -1.991);
        ((GeneralPath) shape).lineTo(-7.192, -1.991);
        ((GeneralPath) shape).lineTo(-5.224, -3.958);
        ((GeneralPath) shape).lineTo(-5.224, -13.556);
        ((GeneralPath) shape).lineTo(-7.192, -15.523);
        ((GeneralPath) shape).lineTo(-11.427, -15.523);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_0_47

        g.setTransform(transformations.pop()); // _0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, 27.6378f));

        // _0_1
        paint1(g, origAlpha, transformations);
    }

    private static void paint1(Graphics2D g, float origAlpha, java.util.LinkedList<AffineTransform> transformations) {
        Shape shape = null;

        // _0_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(612.339, 270.789);
        ((GeneralPath) shape).lineTo(612.601, 270.832);
        ((GeneralPath) shape).lineTo(612.925, 270.886);
        ((GeneralPath) shape).lineTo(613.307, 270.952);
        ((GeneralPath) shape).lineTo(613.739, 271.029);
        ((GeneralPath) shape).lineTo(614.218, 271.116);
        ((GeneralPath) shape).lineTo(614.475, 271.164);
        ((GeneralPath) shape).lineTo(614.74, 271.215);
        ((GeneralPath) shape).lineTo(615.015, 271.268);
        ((GeneralPath) shape).lineTo(615.299, 271.324);
        ((GeneralPath) shape).lineTo(615.59, 271.382);
        ((GeneralPath) shape).lineTo(615.888, 271.443);
        ((GeneralPath) shape).lineTo(616.194, 271.506);
        ((GeneralPath) shape).lineTo(616.505, 271.572);
        ((GeneralPath) shape).lineTo(616.822, 271.64);
        ((GeneralPath) shape).lineTo(617.143, 271.711);
        ((GeneralPath) shape).lineTo(617.469, 271.784);
        ((GeneralPath) shape).lineTo(617.799, 271.86);
        ((GeneralPath) shape).lineTo(618.131, 271.938);
        ((GeneralPath) shape).lineTo(618.466, 272.018);
        ((GeneralPath) shape).lineTo(618.803, 272.101);
        ((GeneralPath) shape).lineTo(619.14, 272.186);
        ((GeneralPath) shape).lineTo(619.479, 272.273);
        ((GeneralPath) shape).lineTo(619.817, 272.363);
        ((GeneralPath) shape).lineTo(620.155, 272.455);
        ((GeneralPath) shape).lineTo(620.492, 272.549);
        ((GeneralPath) shape).lineTo(620.827, 272.646);
        ((GeneralPath) shape).lineTo(621.158, 272.745);
        ((GeneralPath) shape).lineTo(621.325, 272.796);
        ((GeneralPath) shape).lineTo(621.489, 272.847);
        ((GeneralPath) shape).lineTo(621.653, 272.899);
        ((GeneralPath) shape).lineTo(621.816, 272.951);
        ((GeneralPath) shape).lineTo(621.978, 273.004);
        ((GeneralPath) shape).lineTo(622.139, 273.058);
        ((GeneralPath) shape).lineTo(622.298, 273.112);
        ((GeneralPath) shape).lineTo(622.457, 273.167);
        ((GeneralPath) shape).lineTo(622.614, 273.222);
        ((GeneralPath) shape).lineTo(622.771, 273.278);
        ((GeneralPath) shape).lineTo(622.925, 273.335);
        ((GeneralPath) shape).lineTo(623.079, 273.393);
        ((GeneralPath) shape).lineTo(623.231, 273.451);
        ((GeneralPath) shape).lineTo(623.382, 273.511);
        ((GeneralPath) shape).lineTo(623.531, 273.57);
        ((GeneralPath) shape).lineTo(623.678, 273.631);
        ((GeneralPath) shape).lineTo(623.824, 273.693);
        ((GeneralPath) shape).lineTo(623.968, 273.755);
        ((GeneralPath) shape).lineTo(624.111, 273.818);
        ((GeneralPath) shape).lineTo(624.251, 273.883);
        ((GeneralPath) shape).lineTo(624.391, 273.948);
        ((GeneralPath) shape).lineTo(624.528, 274.014);
        ((GeneralPath) shape).lineTo(624.663, 274.082);
        ((GeneralPath) shape).lineTo(624.797, 274.15);
        ((GeneralPath) shape).lineTo(624.929, 274.22);
        ((GeneralPath) shape).lineTo(625.059, 274.292);
        ((GeneralPath) shape).lineTo(625.187, 274.365);
        ((GeneralPath) shape).lineTo(625.313, 274.439);
        ((GeneralPath) shape).lineTo(625.438, 274.515);
        ((GeneralPath) shape).lineTo(625.56, 274.594);
        ((GeneralPath) shape).lineTo(625.681, 274.674);
        ((GeneralPath) shape).lineTo(625.8, 274.756);
        ((GeneralPath) shape).lineTo(625.917, 274.842);
        ((GeneralPath) shape).lineTo(626.032, 274.93);
        ((GeneralPath) shape).lineTo(626.145, 275.021);
        ((GeneralPath) shape).lineTo(626.256, 275.116);
        ((GeneralPath) shape).lineTo(626.36, 275.21);
        ((GeneralPath) shape).lineTo(626.419, 275.266);
        ((GeneralPath) shape).lineTo(626.472, 275.318);
        ((GeneralPath) shape).lineTo(626.525, 275.372);
        ((GeneralPath) shape).lineTo(626.578, 275.427);
        ((GeneralPath) shape).lineTo(626.629, 275.483);
        ((GeneralPath) shape).lineTo(626.68, 275.54);
        ((GeneralPath) shape).lineTo(626.73, 275.6);
        ((GeneralPath) shape).lineTo(626.779, 275.66);
        ((GeneralPath) shape).lineTo(626.828, 275.722);
        ((GeneralPath) shape).lineTo(626.876, 275.787);
        ((GeneralPath) shape).lineTo(626.922, 275.853);
        ((GeneralPath) shape).lineTo(626.968, 275.921);
        ((GeneralPath) shape).lineTo(627.012, 275.99);
        ((GeneralPath) shape).lineTo(627.056, 276.062);
        ((GeneralPath) shape).lineTo(627.098, 276.136);
        ((GeneralPath) shape).lineTo(627.138, 276.212);
        ((GeneralPath) shape).lineTo(627.177, 276.29);
        ((GeneralPath) shape).lineTo(627.214, 276.371);
        ((GeneralPath) shape).lineTo(627.249, 276.453);
        ((GeneralPath) shape).lineTo(627.282, 276.538);
        ((GeneralPath) shape).lineTo(627.312, 276.625);
        ((GeneralPath) shape).lineTo(627.34, 276.713);
        ((GeneralPath) shape).lineTo(627.365, 276.803);
        ((GeneralPath) shape).lineTo(627.388, 276.897);
        ((GeneralPath) shape).lineTo(627.407, 276.992);
        ((GeneralPath) shape).lineTo(627.423, 277.087);
        ((GeneralPath) shape).lineTo(627.436, 277.184);
        ((GeneralPath) shape).lineTo(627.445, 277.282);
        ((GeneralPath) shape).lineTo(627.451, 277.381);
        ((GeneralPath) shape).lineTo(627.453, 277.481);
        ((GeneralPath) shape).lineTo(627.451, 277.579);
        ((GeneralPath) shape).lineTo(627.445, 277.675);
        ((GeneralPath) shape).lineTo(627.437, 277.771);
        ((GeneralPath) shape).lineTo(627.425, 277.867);
        ((GeneralPath) shape).lineTo(627.41, 277.96);
        ((GeneralPath) shape).lineTo(627.391, 278.054);
        ((GeneralPath) shape).lineTo(627.369, 278.145);
        ((GeneralPath) shape).lineTo(627.345, 278.235);
        ((GeneralPath) shape).lineTo(627.318, 278.323);
        ((GeneralPath) shape).lineTo(627.289, 278.409);
        ((GeneralPath) shape).lineTo(627.257, 278.493);
        ((GeneralPath) shape).lineTo(627.223, 278.576);
        ((GeneralPath) shape).lineTo(627.187, 278.656);
        ((GeneralPath) shape).lineTo(627.148, 278.734);
        ((GeneralPath) shape).lineTo(627.109, 278.81);
        ((GeneralPath) shape).lineTo(627.068, 278.883);
        ((GeneralPath) shape).lineTo(627.025, 278.955);
        ((GeneralPath) shape).lineTo(626.981, 279.025);
        ((GeneralPath) shape).lineTo(626.936, 279.093);
        ((GeneralPath) shape).lineTo(626.89, 279.159);
        ((GeneralPath) shape).lineTo(626.843, 279.223);
        ((GeneralPath) shape).lineTo(626.794, 279.286);
        ((GeneralPath) shape).lineTo(626.746, 279.346);
        ((GeneralPath) shape).lineTo(626.696, 279.405);
        ((GeneralPath) shape).lineTo(626.645, 279.463);
        ((GeneralPath) shape).lineTo(626.594, 279.519);
        ((GeneralPath) shape).lineTo(626.542, 279.573);
        ((GeneralPath) shape).lineTo(626.49, 279.626);
        ((GeneralPath) shape).lineTo(626.437, 279.678);
        ((GeneralPath) shape).lineTo(626.378, 279.734);
        ((GeneralPath) shape).lineTo(626.276, 279.826);
        ((GeneralPath) shape).lineTo(626.165, 279.92);
        ((GeneralPath) shape).lineTo(626.053, 280.009);
        ((GeneralPath) shape).lineTo(625.939, 280.096);
        ((GeneralPath) shape).lineTo(625.824, 280.179);
        ((GeneralPath) shape).lineTo(625.706, 280.259);
        ((GeneralPath) shape).lineTo(625.587, 280.336);
        ((GeneralPath) shape).lineTo(625.467, 280.412);
        ((GeneralPath) shape).lineTo(625.344, 280.485);
        ((GeneralPath) shape).lineTo(625.221, 280.556);
        ((GeneralPath) shape).lineTo(625.095, 280.625);
        ((GeneralPath) shape).lineTo(624.968, 280.693);
        ((GeneralPath) shape).lineTo(624.838, 280.759);
        ((GeneralPath) shape).lineTo(624.708, 280.824);
        ((GeneralPath) shape).lineTo(624.576, 280.887);
        ((GeneralPath) shape).lineTo(624.442, 280.949);
        ((GeneralPath) shape).lineTo(624.306, 281.01);
        ((GeneralPath) shape).lineTo(624.169, 281.069);
        ((GeneralPath) shape).lineTo(624.03, 281.128);
        ((GeneralPath) shape).lineTo(623.89, 281.185);
        ((GeneralPath) shape).lineTo(623.748, 281.242);
        ((GeneralPath) shape).lineTo(623.605, 281.298);
        ((GeneralPath) shape).lineTo(623.46, 281.353);
        ((GeneralPath) shape).lineTo(623.314, 281.406);
        ((GeneralPath) shape).lineTo(623.166, 281.459);
        ((GeneralPath) shape).lineTo(623.017, 281.512);
        ((GeneralPath) shape).lineTo(622.867, 281.563);
        ((GeneralPath) shape).lineTo(622.716, 281.614);
        ((GeneralPath) shape).lineTo(622.563, 281.664);
        ((GeneralPath) shape).lineTo(622.41, 281.713);
        ((GeneralPath) shape).lineTo(622.255, 281.762);
        ((GeneralPath) shape).lineTo(622.099, 281.809);
        ((GeneralPath) shape).lineTo(621.942, 281.856);
        ((GeneralPath) shape).lineTo(621.785, 281.903);
        ((GeneralPath) shape).lineTo(621.627, 281.949);
        ((GeneralPath) shape).lineTo(621.467, 281.994);
        ((GeneralPath) shape).lineTo(621.306, 282.039);
        ((GeneralPath) shape).lineTo(620.986, 282.125);
        ((GeneralPath) shape).lineTo(620.662, 282.21);
        ((GeneralPath) shape).lineTo(620.337, 282.292);
        ((GeneralPath) shape).lineTo(620.011, 282.371);
        ((GeneralPath) shape).lineTo(619.684, 282.449);
        ((GeneralPath) shape).lineTo(619.357, 282.524);
        ((GeneralPath) shape).lineTo(619.031, 282.596);
        ((GeneralPath) shape).lineTo(618.706, 282.666);
        ((GeneralPath) shape).lineTo(618.383, 282.734);
        ((GeneralPath) shape).lineTo(618.063, 282.8);
        ((GeneralPath) shape).lineTo(617.745, 282.863);
        ((GeneralPath) shape).lineTo(617.431, 282.924);
        ((GeneralPath) shape).lineTo(617.12, 282.983);
        ((GeneralPath) shape).lineTo(616.815, 283.039);
        ((GeneralPath) shape).lineTo(616.515, 283.093);
        ((GeneralPath) shape).lineTo(616.22, 283.145);
        ((GeneralPath) shape).lineTo(615.932, 283.195);
        ((GeneralPath) shape).lineTo(615.651, 283.242);
        ((GeneralPath) shape).lineTo(615.378, 283.287);
        ((GeneralPath) shape).lineTo(615.113, 283.33);
        ((GeneralPath) shape).lineTo(614.857, 283.371);
        ((GeneralPath) shape).lineTo(614.61, 283.409);
        ((GeneralPath) shape).lineTo(614.148, 283.479);
        ((GeneralPath) shape).lineTo(613.731, 283.539);
        ((GeneralPath) shape).lineTo(613.364, 283.591);
        ((GeneralPath) shape).lineTo(613.051, 283.633);
        ((GeneralPath) shape).lineTo(612.798, 283.666);
        ((GeneralPath) shape).lineTo(612.609, 283.69);
        ((GeneralPath) shape).lineTo(612.37, 283.72);
        ((GeneralPath) shape).lineTo(540.012, 285.53);
        ((GeneralPath) shape).lineTo(539.993, 285.932);
        ((GeneralPath) shape).lineTo(539.902, 287.896);
        ((GeneralPath) shape).lineTo(539.856, 288.859);
        ((GeneralPath) shape).lineTo(539.81, 289.811);
        ((GeneralPath) shape).lineTo(539.763, 290.753);
        ((GeneralPath) shape).lineTo(539.714, 291.686);
        ((GeneralPath) shape).lineTo(539.664, 292.611);
        ((GeneralPath) shape).lineTo(539.612, 293.532);
        ((GeneralPath) shape).lineTo(539.558, 294.447);
        ((GeneralPath) shape).lineTo(539.53, 294.904);
        ((GeneralPath) shape).lineTo(539.501, 295.36);
        ((GeneralPath) shape).lineTo(539.471, 295.815);
        ((GeneralPath) shape).lineTo(539.44, 296.271);
        ((GeneralPath) shape).lineTo(539.409, 296.726);
        ((GeneralPath) shape).lineTo(539.376, 297.181);
        ((GeneralPath) shape).lineTo(539.343, 297.637);
        ((GeneralPath) shape).lineTo(539.308, 298.093);
        ((GeneralPath) shape).lineTo(539.273, 298.55);
        ((GeneralPath) shape).lineTo(539.236, 299.007);
        ((GeneralPath) shape).lineTo(539.198, 299.465);
        ((GeneralPath) shape).lineTo(539.159, 299.925);
        ((GeneralPath) shape).lineTo(539.118, 300.385);
        ((GeneralPath) shape).lineTo(539.076, 300.847);
        ((GeneralPath) shape).lineTo(539.033, 301.311);
        ((GeneralPath) shape).lineTo(538.988, 301.776);
        ((GeneralPath) shape).lineTo(538.942, 302.244);
        ((GeneralPath) shape).lineTo(538.894, 302.713);
        ((GeneralPath) shape).lineTo(538.845, 303.185);
        ((GeneralPath) shape).lineTo(538.794, 303.659);
        ((GeneralPath) shape).lineTo(538.741, 304.135);
        ((GeneralPath) shape).lineTo(538.687, 304.615);
        ((GeneralPath) shape).lineTo(538.631, 305.097);
        ((GeneralPath) shape).lineTo(538.573, 305.582);
        ((GeneralPath) shape).lineTo(538.513, 306.071);
        ((GeneralPath) shape).lineTo(538.451, 306.563);
        ((GeneralPath) shape).lineTo(538.387, 307.058);
        ((GeneralPath) shape).lineTo(538.321, 307.558);
        ((GeneralPath) shape).lineTo(538.253, 308.061);
        ((GeneralPath) shape).lineTo(538.183, 308.568);
        ((GeneralPath) shape).lineTo(538.111, 309.08);
        ((GeneralPath) shape).lineTo(538.037, 309.596);
        ((GeneralPath) shape).lineTo(537.96, 310.116);
        ((GeneralPath) shape).lineTo(537.881, 310.642);
        ((GeneralPath) shape).lineTo(537.8, 311.172);
        ((GeneralPath) shape).lineTo(537.716, 311.707);
        ((GeneralPath) shape).lineTo(537.63, 312.248);
        ((GeneralPath) shape).lineTo(537.55, 312.736);
        ((GeneralPath) shape).lineTo(889.18, 273.628);
        ((GeneralPath) shape).lineTo(889.902, 273.549);
        ((GeneralPath) shape).lineTo(890.613, 273.471);
        ((GeneralPath) shape).lineTo(891.312, 273.396);
        ((GeneralPath) shape).lineTo(892.0, 273.324);
        ((GeneralPath) shape).lineTo(892.677, 273.253);
        ((GeneralPath) shape).lineTo(893.343, 273.186);
        ((GeneralPath) shape).lineTo(893.998, 273.12);
        ((GeneralPath) shape).lineTo(894.642, 273.057);
        ((GeneralPath) shape).lineTo(895.275, 272.996);
        ((GeneralPath) shape).lineTo(895.897, 272.937);
        ((GeneralPath) shape).lineTo(896.509, 272.881);
        ((GeneralPath) shape).lineTo(897.111, 272.826);
        ((GeneralPath) shape).lineTo(897.702, 272.774);
        ((GeneralPath) shape).lineTo(898.283, 272.724);
        ((GeneralPath) shape).lineTo(898.854, 272.677);
        ((GeneralPath) shape).lineTo(899.415, 272.631);
        ((GeneralPath) shape).lineTo(899.966, 272.587);
        ((GeneralPath) shape).lineTo(900.508, 272.546);
        ((GeneralPath) shape).lineTo(901.039, 272.507);
        ((GeneralPath) shape).lineTo(901.561, 272.469);
        ((GeneralPath) shape).lineTo(902.074, 272.434);
        ((GeneralPath) shape).lineTo(902.577, 272.401);
        ((GeneralPath) shape).lineTo(903.071, 272.369);
        ((GeneralPath) shape).lineTo(903.556, 272.34);
        ((GeneralPath) shape).lineTo(904.032, 272.313);
        ((GeneralPath) shape).lineTo(904.499, 272.287);
        ((GeneralPath) shape).lineTo(904.957, 272.264);
        ((GeneralPath) shape).lineTo(905.407, 272.242);
        ((GeneralPath) shape).lineTo(905.848, 272.222);
        ((GeneralPath) shape).lineTo(906.28, 272.204);
        ((GeneralPath) shape).lineTo(906.705, 272.188);
        ((GeneralPath) shape).lineTo(907.121, 272.174);
        ((GeneralPath) shape).lineTo(907.529, 272.162);
        ((GeneralPath) shape).lineTo(907.928, 272.151);
        ((GeneralPath) shape).lineTo(908.32, 272.142);
        ((GeneralPath) shape).lineTo(908.705, 272.135);
        ((GeneralPath) shape).lineTo(909.081, 272.129);
        ((GeneralPath) shape).lineTo(909.45, 272.125);
        ((GeneralPath) shape).lineTo(909.812, 272.123);
        ((GeneralPath) shape).lineTo(910.166, 272.123);
        ((GeneralPath) shape).lineTo(910.513, 272.124);
        ((GeneralPath) shape).lineTo(910.853, 272.127);
        ((GeneralPath) shape).lineTo(911.186, 272.131);
        ((GeneralPath) shape).lineTo(911.512, 272.137);
        ((GeneralPath) shape).lineTo(911.832, 272.145);
        ((GeneralPath) shape).lineTo(912.145, 272.154);
        ((GeneralPath) shape).lineTo(912.451, 272.165);
        ((GeneralPath) shape).lineTo(912.751, 272.177);
        ((GeneralPath) shape).lineTo(913.044, 272.191);
        ((GeneralPath) shape).lineTo(913.332, 272.206);
        ((GeneralPath) shape).lineTo(913.613, 272.223);
        ((GeneralPath) shape).lineTo(913.889, 272.241);
        ((GeneralPath) shape).lineTo(914.158, 272.261);
        ((GeneralPath) shape).lineTo(914.422, 272.282);
        ((GeneralPath) shape).lineTo(914.68, 272.305);
        ((GeneralPath) shape).lineTo(914.933, 272.329);
        ((GeneralPath) shape).lineTo(915.181, 272.354);
        ((GeneralPath) shape).lineTo(915.423, 272.381);
        ((GeneralPath) shape).lineTo(915.66, 272.409);
        ((GeneralPath) shape).lineTo(915.892, 272.439);
        ((GeneralPath) shape).lineTo(916.119, 272.47);
        ((GeneralPath) shape).lineTo(916.341, 272.502);
        ((GeneralPath) shape).lineTo(916.559, 272.536);
        ((GeneralPath) shape).lineTo(916.772, 272.571);
        ((GeneralPath) shape).lineTo(916.981, 272.608);
        ((GeneralPath) shape).lineTo(917.185, 272.646);
        ((GeneralPath) shape).lineTo(917.385, 272.685);
        ((GeneralPath) shape).lineTo(917.58, 272.725);
        ((GeneralPath) shape).lineTo(917.772, 272.767);
        ((GeneralPath) shape).lineTo(917.96, 272.81);
        ((GeneralPath) shape).lineTo(918.143, 272.854);
        ((GeneralPath) shape).lineTo(918.324, 272.9);
        ((GeneralPath) shape).lineTo(918.5, 272.947);
        ((GeneralPath) shape).lineTo(918.672, 272.995);
        ((GeneralPath) shape).lineTo(918.841, 273.045);
        ((GeneralPath) shape).lineTo(919.007, 273.095);
        ((GeneralPath) shape).lineTo(919.169, 273.147);
        ((GeneralPath) shape).lineTo(919.328, 273.201);
        ((GeneralPath) shape).lineTo(919.484, 273.255);
        ((GeneralPath) shape).lineTo(919.636, 273.31);
        ((GeneralPath) shape).lineTo(919.786, 273.367);
        ((GeneralPath) shape).lineTo(919.932, 273.425);
        ((GeneralPath) shape).lineTo(920.075, 273.484);
        ((GeneralPath) shape).lineTo(920.216, 273.544);
        ((GeneralPath) shape).lineTo(920.353, 273.605);
        ((GeneralPath) shape).lineTo(920.488, 273.666);
        ((GeneralPath) shape).lineTo(920.62, 273.729);
        ((GeneralPath) shape).lineTo(920.749, 273.793);
        ((GeneralPath) shape).lineTo(920.875, 273.857);
        ((GeneralPath) shape).lineTo(920.998, 273.922);
        ((GeneralPath) shape).lineTo(921.119, 273.988);
        ((GeneralPath) shape).lineTo(921.238, 274.054);
        ((GeneralPath) shape).lineTo(921.354, 274.121);
        ((GeneralPath) shape).lineTo(921.467, 274.188);
        ((GeneralPath) shape).lineTo(921.578, 274.256);
        ((GeneralPath) shape).lineTo(921.686, 274.323);
        ((GeneralPath) shape).lineTo(921.792, 274.391);
        ((GeneralPath) shape).lineTo(921.896, 274.459);
        ((GeneralPath) shape).lineTo(921.998, 274.527);
        ((GeneralPath) shape).lineTo(922.097, 274.595);
        ((GeneralPath) shape).lineTo(922.195, 274.663);
        ((GeneralPath) shape).lineTo(922.293, 274.732);
        ((GeneralPath) shape).lineTo(922.475, 274.864);
        ((GeneralPath) shape).lineTo(922.653, 274.997);
        ((GeneralPath) shape).lineTo(922.825, 275.127);
        ((GeneralPath) shape).lineTo(922.991, 275.254);
        ((GeneralPath) shape).lineTo(923.153, 275.379);
        ((GeneralPath) shape).lineTo(923.312, 275.501);
        ((GeneralPath) shape).lineTo(923.467, 275.62);
        ((GeneralPath) shape).lineTo(923.62, 275.735);
        ((GeneralPath) shape).lineTo(923.775, 275.85);
        ((GeneralPath) shape).lineTo(923.85, 275.904);
        ((GeneralPath) shape).lineTo(923.927, 275.958);
        ((GeneralPath) shape).lineTo(924.003, 276.012);
        ((GeneralPath) shape).lineTo(924.081, 276.066);
        ((GeneralPath) shape).lineTo(924.159, 276.118);
        ((GeneralPath) shape).lineTo(924.237, 276.171);
        ((GeneralPath) shape).lineTo(924.317, 276.223);
        ((GeneralPath) shape).lineTo(925.417, 276.923);
        ((GeneralPath) shape).lineTo(925.162, 278.194);
        ((GeneralPath) shape).lineTo(925.138, 278.304);
        ((GeneralPath) shape).lineTo(925.112, 278.412);
        ((GeneralPath) shape).lineTo(925.084, 278.521);
        ((GeneralPath) shape).lineTo(925.054, 278.629);
        ((GeneralPath) shape).lineTo(925.022, 278.735);
        ((GeneralPath) shape).lineTo(924.988, 278.841);
        ((GeneralPath) shape).lineTo(924.952, 278.946);
        ((GeneralPath) shape).lineTo(924.914, 279.049);
        ((GeneralPath) shape).lineTo(924.875, 279.153);
        ((GeneralPath) shape).lineTo(924.833, 279.255);
        ((GeneralPath) shape).lineTo(924.79, 279.356);
        ((GeneralPath) shape).lineTo(924.745, 279.456);
        ((GeneralPath) shape).lineTo(924.699, 279.555);
        ((GeneralPath) shape).lineTo(924.65, 279.654);
        ((GeneralPath) shape).lineTo(924.6, 279.751);
        ((GeneralPath) shape).lineTo(924.548, 279.847);
        ((GeneralPath) shape).lineTo(924.495, 279.942);
        ((GeneralPath) shape).lineTo(924.44, 280.035);
        ((GeneralPath) shape).lineTo(924.383, 280.129);
        ((GeneralPath) shape).lineTo(924.325, 280.22);
        ((GeneralPath) shape).lineTo(924.266, 280.311);
        ((GeneralPath) shape).lineTo(924.205, 280.4);
        ((GeneralPath) shape).lineTo(924.143, 280.489);
        ((GeneralPath) shape).lineTo(924.079, 280.575);
        ((GeneralPath) shape).lineTo(924.015, 280.661);
        ((GeneralPath) shape).lineTo(923.948, 280.747);
        ((GeneralPath) shape).lineTo(923.881, 280.83);
        ((GeneralPath) shape).lineTo(923.812, 280.913);
        ((GeneralPath) shape).lineTo(923.743, 280.994);
        ((GeneralPath) shape).lineTo(923.672, 281.075);
        ((GeneralPath) shape).lineTo(923.599, 281.155);
        ((GeneralPath) shape).lineTo(923.526, 281.233);
        ((GeneralPath) shape).lineTo(923.452, 281.31);
        ((GeneralPath) shape).lineTo(923.377, 281.387);
        ((GeneralPath) shape).lineTo(923.3, 281.463);
        ((GeneralPath) shape).lineTo(923.223, 281.537);
        ((GeneralPath) shape).lineTo(923.145, 281.61);
        ((GeneralPath) shape).lineTo(923.066, 281.683);
        ((GeneralPath) shape).lineTo(922.985, 281.755);
        ((GeneralPath) shape).lineTo(922.904, 281.825);
        ((GeneralPath) shape).lineTo(922.822, 281.895);
        ((GeneralPath) shape).lineTo(922.739, 281.964);
        ((GeneralPath) shape).lineTo(922.655, 282.033);
        ((GeneralPath) shape).lineTo(922.571, 282.1);
        ((GeneralPath) shape).lineTo(922.485, 282.166);
        ((GeneralPath) shape).lineTo(922.399, 282.232);
        ((GeneralPath) shape).lineTo(922.311, 282.297);
        ((GeneralPath) shape).lineTo(922.223, 282.362);
        ((GeneralPath) shape).lineTo(922.135, 282.425);
        ((GeneralPath) shape).lineTo(922.045, 282.488);
        ((GeneralPath) shape).lineTo(921.954, 282.551);
        ((GeneralPath) shape).lineTo(921.863, 282.612);
        ((GeneralPath) shape).lineTo(921.771, 282.673);
        ((GeneralPath) shape).lineTo(921.678, 282.733);
        ((GeneralPath) shape).lineTo(921.585, 282.793);
        ((GeneralPath) shape).lineTo(921.491, 282.852);
        ((GeneralPath) shape).lineTo(921.393, 282.913);
        ((GeneralPath) shape).lineTo(921.203, 283.026);
        ((GeneralPath) shape).lineTo(921.008, 283.14);
        ((GeneralPath) shape).lineTo(920.81, 283.251);
        ((GeneralPath) shape).lineTo(920.609, 283.361);
        ((GeneralPath) shape).lineTo(920.405, 283.469);
        ((GeneralPath) shape).lineTo(920.199, 283.576);
        ((GeneralPath) shape).lineTo(919.99, 283.681);
        ((GeneralPath) shape).lineTo(919.778, 283.785);
        ((GeneralPath) shape).lineTo(919.563, 283.887);
        ((GeneralPath) shape).lineTo(919.346, 283.988);
        ((GeneralPath) shape).lineTo(919.126, 284.089);
        ((GeneralPath) shape).lineTo(918.903, 284.188);
        ((GeneralPath) shape).lineTo(918.678, 284.286);
        ((GeneralPath) shape).lineTo(918.45, 284.384);
        ((GeneralPath) shape).lineTo(918.22, 284.481);
        ((GeneralPath) shape).lineTo(917.988, 284.577);
        ((GeneralPath) shape).lineTo(917.752, 284.673);
        ((GeneralPath) shape).lineTo(917.515, 284.768);
        ((GeneralPath) shape).lineTo(917.275, 284.863);
        ((GeneralPath) shape).lineTo(917.033, 284.958);
        ((GeneralPath) shape).lineTo(916.788, 285.052);
        ((GeneralPath) shape).lineTo(916.542, 285.147);
        ((GeneralPath) shape).lineTo(916.291, 285.241);
        ((GeneralPath) shape).lineTo(915.788, 285.429);
        ((GeneralPath) shape).lineTo(915.275, 285.617);
        ((GeneralPath) shape).lineTo(914.753, 285.807);
        ((GeneralPath) shape).lineTo(914.224, 285.998);
        ((GeneralPath) shape).lineTo(913.144, 286.385);
        ((GeneralPath) shape).lineTo(912.593, 286.583);
        ((GeneralPath) shape).lineTo(912.036, 286.784);
        ((GeneralPath) shape).lineTo(911.472, 286.989);
        ((GeneralPath) shape).lineTo(910.903, 287.198);
        ((GeneralPath) shape).lineTo(910.328, 287.412);
        ((GeneralPath) shape).lineTo(910.04, 287.52);
        ((GeneralPath) shape).lineTo(909.75, 287.63);
        ((GeneralPath) shape).lineTo(909.459, 287.742);
        ((GeneralPath) shape).lineTo(909.166, 287.855);
        ((GeneralPath) shape).lineTo(908.872, 287.969);
        ((GeneralPath) shape).lineTo(908.578, 288.085);
        ((GeneralPath) shape).lineTo(908.283, 288.202);
        ((GeneralPath) shape).lineTo(907.986, 288.321);
        ((GeneralPath) shape).lineTo(907.689, 288.442);
        ((GeneralPath) shape).lineTo(907.391, 288.565);
        ((GeneralPath) shape).lineTo(907.092, 288.689);
        ((GeneralPath) shape).lineTo(906.793, 288.815);
        ((GeneralPath) shape).lineTo(906.493, 288.943);
        ((GeneralPath) shape).lineTo(906.192, 289.074);
        ((GeneralPath) shape).lineTo(905.891, 289.206);
        ((GeneralPath) shape).lineTo(905.589, 289.34);
        ((GeneralPath) shape).lineTo(905.287, 289.476);
        ((GeneralPath) shape).lineTo(904.985, 289.615);
        ((GeneralPath) shape).lineTo(904.682, 289.755);
        ((GeneralPath) shape).lineTo(904.378, 289.898);
        ((GeneralPath) shape).lineTo(904.075, 290.044);
        ((GeneralPath) shape).lineTo(903.771, 290.192);
        ((GeneralPath) shape).lineTo(903.467, 290.342);
        ((GeneralPath) shape).lineTo(903.163, 290.494);
        ((GeneralPath) shape).lineTo(902.859, 290.649);
        ((GeneralPath) shape).lineTo(902.555, 290.807);
        ((GeneralPath) shape).lineTo(902.251, 290.968);
        ((GeneralPath) shape).lineTo(901.947, 291.131);
        ((GeneralPath) shape).lineTo(901.644, 291.296);
        ((GeneralPath) shape).lineTo(901.34, 291.465);
        ((GeneralPath) shape).lineTo(901.037, 291.637);
        ((GeneralPath) shape).lineTo(900.734, 291.811);
        ((GeneralPath) shape).lineTo(900.431, 291.988);
        ((GeneralPath) shape).lineTo(900.129, 292.169);
        ((GeneralPath) shape).lineTo(899.825, 292.353);
        ((GeneralPath) shape).lineTo(899.676, 292.445);
        ((GeneralPath) shape).lineTo(899.525, 292.539);
        ((GeneralPath) shape).lineTo(899.374, 292.633);
        ((GeneralPath) shape).lineTo(899.224, 292.728);
        ((GeneralPath) shape).lineTo(899.074, 292.824);
        ((GeneralPath) shape).lineTo(898.924, 292.921);
        ((GeneralPath) shape).lineTo(898.774, 293.019);
        ((GeneralPath) shape).lineTo(898.624, 293.117);
        ((GeneralPath) shape).lineTo(898.474, 293.217);
        ((GeneralPath) shape).lineTo(898.325, 293.317);
        ((GeneralPath) shape).lineTo(898.176, 293.418);
        ((GeneralPath) shape).lineTo(898.027, 293.52);
        ((GeneralPath) shape).lineTo(897.878, 293.622);
        ((GeneralPath) shape).lineTo(897.729, 293.726);
        ((GeneralPath) shape).lineTo(897.58, 293.83);
        ((GeneralPath) shape).lineTo(897.432, 293.936);
        ((GeneralPath) shape).lineTo(897.284, 294.042);
        ((GeneralPath) shape).lineTo(897.136, 294.149);
        ((GeneralPath) shape).lineTo(896.988, 294.257);
        ((GeneralPath) shape).lineTo(896.841, 294.366);
        ((GeneralPath) shape).lineTo(896.694, 294.476);
        ((GeneralPath) shape).lineTo(896.547, 294.586);
        ((GeneralPath) shape).lineTo(896.4, 294.698);
        ((GeneralPath) shape).lineTo(896.254, 294.811);
        ((GeneralPath) shape).lineTo(896.107, 294.924);
        ((GeneralPath) shape).lineTo(895.962, 295.038);
        ((GeneralPath) shape).lineTo(895.816, 295.154);
        ((GeneralPath) shape).lineTo(895.67, 295.27);
        ((GeneralPath) shape).lineTo(895.525, 295.387);
        ((GeneralPath) shape).lineTo(895.381, 295.506);
        ((GeneralPath) shape).lineTo(895.236, 295.625);
        ((GeneralPath) shape).lineTo(895.092, 295.745);
        ((GeneralPath) shape).lineTo(894.948, 295.866);
        ((GeneralPath) shape).lineTo(894.804, 295.989);
        ((GeneralPath) shape).lineTo(894.661, 296.112);
        ((GeneralPath) shape).lineTo(894.518, 296.236);
        ((GeneralPath) shape).lineTo(894.375, 296.362);
        ((GeneralPath) shape).lineTo(893.829, 296.846);
        ((GeneralPath) shape).lineTo(615.815, 333.915);
        ((GeneralPath) shape).lineTo(619.05, 354.329);
        ((GeneralPath) shape).lineTo(616.585, 354.799);
        ((GeneralPath) shape).lineTo(619.151, 373.74);
        ((GeneralPath) shape).lineTo(623.035, 373.623);
        ((GeneralPath) shape).lineTo(622.918, 383.443);
        ((GeneralPath) shape).lineTo(610.616, 383.525);
        ((GeneralPath) shape).lineTo(610.984, 376.289);
        ((GeneralPath) shape).lineTo(608.389, 355.818);
        ((GeneralPath) shape).lineTo(605.345, 356.169);
        ((GeneralPath) shape).lineTo(601.604, 335.975);
        ((GeneralPath) shape).lineTo(532.117, 345.28);
        ((GeneralPath) shape).lineTo(492.296, 345.024);
        ((GeneralPath) shape).lineTo(492.276, 345.08);
        ((GeneralPath) shape).lineTo(492.249, 345.15);
        ((GeneralPath) shape).lineTo(492.221, 345.219);
        ((GeneralPath) shape).lineTo(492.192, 345.287);
        ((GeneralPath) shape).lineTo(492.162, 345.355);
        ((GeneralPath) shape).lineTo(492.131, 345.422);
        ((GeneralPath) shape).lineTo(492.099, 345.489);
        ((GeneralPath) shape).lineTo(492.065, 345.554);
        ((GeneralPath) shape).lineTo(492.027, 345.626);
        ((GeneralPath) shape).lineTo(491.959, 345.747);
        ((GeneralPath) shape).lineTo(491.883, 345.87);
        ((GeneralPath) shape).lineTo(491.804, 345.99);
        ((GeneralPath) shape).lineTo(491.72, 346.107);
        ((GeneralPath) shape).lineTo(491.634, 346.219);
        ((GeneralPath) shape).lineTo(491.545, 346.328);
        ((GeneralPath) shape).lineTo(491.453, 346.432);
        ((GeneralPath) shape).lineTo(491.358, 346.532);
        ((GeneralPath) shape).lineTo(491.261, 346.629);
        ((GeneralPath) shape).lineTo(491.163, 346.721);
        ((GeneralPath) shape).lineTo(491.062, 346.809);
        ((GeneralPath) shape).lineTo(490.961, 346.893);
        ((GeneralPath) shape).lineTo(490.858, 346.974);
        ((GeneralPath) shape).lineTo(490.754, 347.05);
        ((GeneralPath) shape).lineTo(490.65, 347.123);
        ((GeneralPath) shape).lineTo(490.544, 347.193);
        ((GeneralPath) shape).lineTo(490.439, 347.259);
        ((GeneralPath) shape).lineTo(490.333, 347.321);
        ((GeneralPath) shape).lineTo(490.227, 347.381);
        ((GeneralPath) shape).lineTo(490.12, 347.438);
        ((GeneralPath) shape).lineTo(490.014, 347.492);
        ((GeneralPath) shape).lineTo(489.908, 347.544);
        ((GeneralPath) shape).lineTo(489.802, 347.593);
        ((GeneralPath) shape).lineTo(489.697, 347.64);
        ((GeneralPath) shape).lineTo(489.591, 347.685);
        ((GeneralPath) shape).lineTo(489.486, 347.727);
        ((GeneralPath) shape).lineTo(489.381, 347.768);
        ((GeneralPath) shape).lineTo(489.277, 347.807);
        ((GeneralPath) shape).lineTo(489.173, 347.844);
        ((GeneralPath) shape).lineTo(489.07, 347.88);
        ((GeneralPath) shape).lineTo(488.966, 347.914);
        ((GeneralPath) shape).lineTo(488.863, 347.948);
        ((GeneralPath) shape).lineTo(488.762, 347.979);
        ((GeneralPath) shape).lineTo(488.657, 348.011);
        ((GeneralPath) shape).lineTo(488.46, 348.068);
        ((GeneralPath) shape).lineTo(488.261, 348.123);
        ((GeneralPath) shape).lineTo(488.065, 348.175);
        ((GeneralPath) shape).lineTo(487.873, 348.225);
        ((GeneralPath) shape).lineTo(487.684, 348.273);
        ((GeneralPath) shape).lineTo(487.315, 348.366);
        ((GeneralPath) shape).lineTo(487.138, 348.411);
        ((GeneralPath) shape).lineTo(486.965, 348.457);
        ((GeneralPath) shape).lineTo(486.797, 348.502);
        ((GeneralPath) shape).lineTo(486.635, 348.548);
        ((GeneralPath) shape).lineTo(486.475, 348.595);
        ((GeneralPath) shape).lineTo(486.402, 348.618);
        ((GeneralPath) shape).lineTo(486.327, 348.642);
        ((GeneralPath) shape).lineTo(486.254, 348.666);
        ((GeneralPath) shape).lineTo(486.184, 348.69);
        ((GeneralPath) shape).lineTo(486.115, 348.714);
        ((GeneralPath) shape).lineTo(486.048, 348.739);
        ((GeneralPath) shape).lineTo(485.982, 348.763);
        ((GeneralPath) shape).lineTo(485.919, 348.789);
        ((GeneralPath) shape).lineTo(485.857, 348.814);
        ((GeneralPath) shape).lineTo(485.798, 348.839);
        ((GeneralPath) shape).lineTo(485.741, 348.865);
        ((GeneralPath) shape).lineTo(485.686, 348.891);
        ((GeneralPath) shape).lineTo(485.633, 348.917);
        ((GeneralPath) shape).lineTo(485.581, 348.943);
        ((GeneralPath) shape).lineTo(485.533, 348.969);
        ((GeneralPath) shape).lineTo(485.486, 348.995);
        ((GeneralPath) shape).lineTo(485.441, 349.022);
        ((GeneralPath) shape).lineTo(485.399, 349.048);
        ((GeneralPath) shape).lineTo(485.359, 349.074);
        ((GeneralPath) shape).lineTo(485.32, 349.101);
        ((GeneralPath) shape).lineTo(485.284, 349.127);
        ((GeneralPath) shape).lineTo(485.25, 349.153);
        ((GeneralPath) shape).lineTo(485.217, 349.179);
        ((GeneralPath) shape).lineTo(485.186, 349.206);
        ((GeneralPath) shape).lineTo(485.156, 349.232);
        ((GeneralPath) shape).lineTo(485.129, 349.258);
        ((GeneralPath) shape).lineTo(485.102, 349.285);
        ((GeneralPath) shape).lineTo(485.077, 349.312);
        ((GeneralPath) shape).lineTo(485.053, 349.339);
        ((GeneralPath) shape).lineTo(485.03, 349.367);
        ((GeneralPath) shape).lineTo(485.008, 349.395);
        ((GeneralPath) shape).lineTo(484.987, 349.424);
        ((GeneralPath) shape).lineTo(484.966, 349.454);
        ((GeneralPath) shape).lineTo(484.946, 349.486);
        ((GeneralPath) shape).lineTo(484.926, 349.519);
        ((GeneralPath) shape).lineTo(484.904, 349.559);
        ((GeneralPath) shape).lineTo(484.898, 349.571);
        ((GeneralPath) shape).lineTo(484.888, 349.59);
        ((GeneralPath) shape).lineTo(484.879, 349.608);
        ((GeneralPath) shape).lineTo(484.87, 349.627);
        ((GeneralPath) shape).lineTo(484.862, 349.647);
        ((GeneralPath) shape).lineTo(484.853, 349.668);
        ((GeneralPath) shape).lineTo(484.844, 349.69);
        ((GeneralPath) shape).lineTo(484.835, 349.712);
        ((GeneralPath) shape).lineTo(484.827, 349.734);
        ((GeneralPath) shape).lineTo(484.819, 349.758);
        ((GeneralPath) shape).lineTo(484.81, 349.783);
        ((GeneralPath) shape).lineTo(484.802, 349.807);
        ((GeneralPath) shape).lineTo(484.794, 349.833);
        ((GeneralPath) shape).lineTo(484.787, 349.859);
        ((GeneralPath) shape).lineTo(484.779, 349.887);
        ((GeneralPath) shape).lineTo(484.772, 349.915);
        ((GeneralPath) shape).lineTo(484.764, 349.944);
        ((GeneralPath) shape).lineTo(484.758, 349.975);
        ((GeneralPath) shape).lineTo(484.751, 350.006);
        ((GeneralPath) shape).lineTo(484.744, 350.038);
        ((GeneralPath) shape).lineTo(484.738, 350.071);
        ((GeneralPath) shape).lineTo(484.732, 350.105);
        ((GeneralPath) shape).lineTo(484.727, 350.14);
        ((GeneralPath) shape).lineTo(484.721, 350.176);
        ((GeneralPath) shape).lineTo(484.716, 350.213);
        ((GeneralPath) shape).lineTo(484.712, 350.252);
        ((GeneralPath) shape).lineTo(484.707, 350.291);
        ((GeneralPath) shape).lineTo(484.704, 350.326);
        ((GeneralPath) shape).lineTo(484.697, 350.415);
        ((GeneralPath) shape).lineTo(484.692, 350.504);
        ((GeneralPath) shape).lineTo(484.689, 350.597);
        ((GeneralPath) shape).lineTo(484.688, 350.712);
        ((GeneralPath) shape).lineTo(484.541, 367.228);
        ((GeneralPath) shape).lineTo(481.041, 367.197);
        ((GeneralPath) shape).lineTo(481.188, 350.677);
        ((GeneralPath) shape).lineTo(481.19, 350.521);
        ((GeneralPath) shape).lineTo(481.195, 350.351);
        ((GeneralPath) shape).lineTo(481.205, 350.182);
        ((GeneralPath) shape).lineTo(481.217, 350.021);
        ((GeneralPath) shape).lineTo(481.226, 349.933);
        ((GeneralPath) shape).lineTo(481.235, 349.851);
        ((GeneralPath) shape).lineTo(481.244, 349.769);
        ((GeneralPath) shape).lineTo(481.255, 349.688);
        ((GeneralPath) shape).lineTo(481.267, 349.608);
        ((GeneralPath) shape).lineTo(481.28, 349.527);
        ((GeneralPath) shape).lineTo(481.294, 349.448);
        ((GeneralPath) shape).lineTo(481.309, 349.369);
        ((GeneralPath) shape).lineTo(481.325, 349.29);
        ((GeneralPath) shape).lineTo(481.341, 349.212);
        ((GeneralPath) shape).lineTo(481.359, 349.135);
        ((GeneralPath) shape).lineTo(481.378, 349.058);
        ((GeneralPath) shape).lineTo(481.398, 348.982);
        ((GeneralPath) shape).lineTo(481.419, 348.906);
        ((GeneralPath) shape).lineTo(481.441, 348.83);
        ((GeneralPath) shape).lineTo(481.464, 348.755);
        ((GeneralPath) shape).lineTo(481.488, 348.681);
        ((GeneralPath) shape).lineTo(481.513, 348.608);
        ((GeneralPath) shape).lineTo(481.539, 348.535);
        ((GeneralPath) shape).lineTo(481.566, 348.462);
        ((GeneralPath) shape).lineTo(481.594, 348.391);
        ((GeneralPath) shape).lineTo(481.623, 348.32);
        ((GeneralPath) shape).lineTo(481.653, 348.25);
        ((GeneralPath) shape).lineTo(481.684, 348.18);
        ((GeneralPath) shape).lineTo(481.716, 348.111);
        ((GeneralPath) shape).lineTo(481.749, 348.042);
        ((GeneralPath) shape).lineTo(481.783, 347.975);
        ((GeneralPath) shape).lineTo(481.821, 347.902);
        ((GeneralPath) shape).lineTo(481.89, 347.777);
        ((GeneralPath) shape).lineTo(481.966, 347.649);
        ((GeneralPath) shape).lineTo(482.045, 347.525);
        ((GeneralPath) shape).lineTo(482.128, 347.404);
        ((GeneralPath) shape).lineTo(482.214, 347.286);
        ((GeneralPath) shape).lineTo(482.303, 347.172);
        ((GeneralPath) shape).lineTo(482.394, 347.062);
        ((GeneralPath) shape).lineTo(482.489, 346.955);
        ((GeneralPath) shape).lineTo(482.585, 346.853);
        ((GeneralPath) shape).lineTo(482.683, 346.754);
        ((GeneralPath) shape).lineTo(482.784, 346.659);
        ((GeneralPath) shape).lineTo(482.886, 346.567);
        ((GeneralPath) shape).lineTo(482.988, 346.48);
        ((GeneralPath) shape).lineTo(483.093, 346.396);
        ((GeneralPath) shape).lineTo(483.199, 346.315);
        ((GeneralPath) shape).lineTo(483.306, 346.238);
        ((GeneralPath) shape).lineTo(483.413, 346.165);
        ((GeneralPath) shape).lineTo(483.521, 346.094);
        ((GeneralPath) shape).lineTo(483.629, 346.027);
        ((GeneralPath) shape).lineTo(483.738, 345.963);
        ((GeneralPath) shape).lineTo(483.847, 345.902);
        ((GeneralPath) shape).lineTo(483.956, 345.843);
        ((GeneralPath) shape).lineTo(484.065, 345.787);
        ((GeneralPath) shape).lineTo(484.174, 345.734);
        ((GeneralPath) shape).lineTo(484.283, 345.683);
        ((GeneralPath) shape).lineTo(484.392, 345.634);
        ((GeneralPath) shape).lineTo(484.5, 345.588);
        ((GeneralPath) shape).lineTo(484.608, 345.543);
        ((GeneralPath) shape).lineTo(484.716, 345.5);
        ((GeneralPath) shape).lineTo(484.824, 345.46);
        ((GeneralPath) shape).lineTo(484.93, 345.42);
        ((GeneralPath) shape).lineTo(485.037, 345.383);
        ((GeneralPath) shape).lineTo(485.143, 345.347);
        ((GeneralPath) shape).lineTo(485.248, 345.312);
        ((GeneralPath) shape).lineTo(485.353, 345.279);
        ((GeneralPath) shape).lineTo(485.46, 345.246);
        ((GeneralPath) shape).lineTo(485.663, 345.185);
        ((GeneralPath) shape).lineTo(485.866, 345.128);
        ((GeneralPath) shape).lineTo(486.066, 345.074);
        ((GeneralPath) shape).lineTo(486.262, 345.023);
        ((GeneralPath) shape).lineTo(486.454, 344.974);
        ((GeneralPath) shape).lineTo(486.825, 344.88);
        ((GeneralPath) shape).lineTo(487.004, 344.835);
        ((GeneralPath) shape).lineTo(487.177, 344.79);
        ((GeneralPath) shape).lineTo(487.343, 344.746);
        ((GeneralPath) shape).lineTo(487.504, 344.701);
        ((GeneralPath) shape).lineTo(487.661, 344.656);
        ((GeneralPath) shape).lineTo(487.732, 344.634);
        ((GeneralPath) shape).lineTo(487.805, 344.611);
        ((GeneralPath) shape).lineTo(487.875, 344.589);
        ((GeneralPath) shape).lineTo(487.943, 344.566);
        ((GeneralPath) shape).lineTo(488.009, 344.543);
        ((GeneralPath) shape).lineTo(488.073, 344.52);
        ((GeneralPath) shape).lineTo(488.134, 344.497);
        ((GeneralPath) shape).lineTo(488.194, 344.474);
        ((GeneralPath) shape).lineTo(488.251, 344.451);
        ((GeneralPath) shape).lineTo(488.305, 344.428);
        ((GeneralPath) shape).lineTo(488.357, 344.405);
        ((GeneralPath) shape).lineTo(488.407, 344.382);
        ((GeneralPath) shape).lineTo(488.454, 344.359);
        ((GeneralPath) shape).lineTo(488.499, 344.336);
        ((GeneralPath) shape).lineTo(488.541, 344.313);
        ((GeneralPath) shape).lineTo(488.581, 344.291);
        ((GeneralPath) shape).lineTo(488.618, 344.269);
        ((GeneralPath) shape).lineTo(488.652, 344.248);
        ((GeneralPath) shape).lineTo(488.684, 344.227);
        ((GeneralPath) shape).lineTo(488.714, 344.206);
        ((GeneralPath) shape).lineTo(488.742, 344.186);
        ((GeneralPath) shape).lineTo(488.767, 344.166);
        ((GeneralPath) shape).lineTo(488.79, 344.146);
        ((GeneralPath) shape).lineTo(488.812, 344.128);
        ((GeneralPath) shape).lineTo(488.831, 344.11);
        ((GeneralPath) shape).lineTo(488.849, 344.092);
        ((GeneralPath) shape).lineTo(488.865, 344.075);
        ((GeneralPath) shape).lineTo(488.88, 344.057);
        ((GeneralPath) shape).lineTo(488.894, 344.04);
        ((GeneralPath) shape).lineTo(488.907, 344.023);
        ((GeneralPath) shape).lineTo(488.92, 344.006);
        ((GeneralPath) shape).lineTo(488.932, 343.988);
        ((GeneralPath) shape).lineTo(488.943, 343.969);
        ((GeneralPath) shape).lineTo(488.958, 343.943);
        ((GeneralPath) shape).lineTo(488.96, 343.939);
        ((GeneralPath) shape).lineTo(488.966, 343.928);
        ((GeneralPath) shape).lineTo(488.971, 343.918);
        ((GeneralPath) shape).lineTo(488.976, 343.906);
        ((GeneralPath) shape).lineTo(488.982, 343.894);
        ((GeneralPath) shape).lineTo(488.987, 343.881);
        ((GeneralPath) shape).lineTo(488.992, 343.868);
        ((GeneralPath) shape).lineTo(488.998, 343.854);
        ((GeneralPath) shape).lineTo(489.003, 343.84);
        ((GeneralPath) shape).lineTo(489.008, 343.825);
        ((GeneralPath) shape).lineTo(489.013, 343.809);
        ((GeneralPath) shape).lineTo(489.018, 343.793);
        ((GeneralPath) shape).lineTo(489.023, 343.776);
        ((GeneralPath) shape).lineTo(489.028, 343.758);
        ((GeneralPath) shape).lineTo(489.033, 343.739);
        ((GeneralPath) shape).lineTo(489.037, 343.719);
        ((GeneralPath) shape).lineTo(489.042, 343.698);
        ((GeneralPath) shape).lineTo(489.047, 343.676);
        ((GeneralPath) shape).lineTo(489.051, 343.654);
        ((GeneralPath) shape).lineTo(489.055, 343.63);
        ((GeneralPath) shape).lineTo(489.059, 343.606);
        ((GeneralPath) shape).lineTo(489.062, 343.58);
        ((GeneralPath) shape).lineTo(489.066, 343.553);
        ((GeneralPath) shape).lineTo(489.068, 343.532);
        ((GeneralPath) shape).lineTo(489.074, 343.467);
        ((GeneralPath) shape).lineTo(489.078, 343.404);
        ((GeneralPath) shape).lineTo(489.081, 343.337);
        ((GeneralPath) shape).lineTo(489.104, 341.503);
        ((GeneralPath) shape).lineTo(531.895, 341.778);
        ((GeneralPath) shape).lineTo(604.439, 332.064);
        ((GeneralPath) shape).lineTo(608.191, 352.318);
        ((GeneralPath) shape).lineTo(611.426, 351.945);
        ((GeneralPath) shape).lineTo(614.496, 376.157);
        ((GeneralPath) shape).lineTo(614.3, 380.0);
        ((GeneralPath) shape).lineTo(619.459, 379.966);
        ((GeneralPath) shape).lineTo(619.492, 377.231);
        ((GeneralPath) shape).lineTo(616.105, 377.333);
        ((GeneralPath) shape).lineTo(612.671, 351.981);
        ((GeneralPath) shape).lineTo(615.062, 351.526);
        ((GeneralPath) shape).lineTo(611.796, 330.92);
        ((GeneralPath) shape).lineTo(892.311, 293.517);
        ((GeneralPath) shape).lineTo(892.372, 293.464);
        ((GeneralPath) shape).lineTo(892.529, 293.329);
        ((GeneralPath) shape).lineTo(892.686, 293.195);
        ((GeneralPath) shape).lineTo(892.844, 293.063);
        ((GeneralPath) shape).lineTo(893.002, 292.931);
        ((GeneralPath) shape).lineTo(893.16, 292.801);
        ((GeneralPath) shape).lineTo(893.318, 292.671);
        ((GeneralPath) shape).lineTo(893.477, 292.543);
        ((GeneralPath) shape).lineTo(893.636, 292.416);
        ((GeneralPath) shape).lineTo(893.795, 292.289);
        ((GeneralPath) shape).lineTo(893.955, 292.164);
        ((GeneralPath) shape).lineTo(894.114, 292.04);
        ((GeneralPath) shape).lineTo(894.274, 291.918);
        ((GeneralPath) shape).lineTo(894.434, 291.796);
        ((GeneralPath) shape).lineTo(894.595, 291.675);
        ((GeneralPath) shape).lineTo(894.755, 291.555);
        ((GeneralPath) shape).lineTo(894.916, 291.437);
        ((GeneralPath) shape).lineTo(895.077, 291.319);
        ((GeneralPath) shape).lineTo(895.238, 291.202);
        ((GeneralPath) shape).lineTo(895.399, 291.087);
        ((GeneralPath) shape).lineTo(895.561, 290.972);
        ((GeneralPath) shape).lineTo(895.722, 290.858);
        ((GeneralPath) shape).lineTo(895.884, 290.746);
        ((GeneralPath) shape).lineTo(896.046, 290.634);
        ((GeneralPath) shape).lineTo(896.208, 290.523);
        ((GeneralPath) shape).lineTo(896.37, 290.413);
        ((GeneralPath) shape).lineTo(896.533, 290.305);
        ((GeneralPath) shape).lineTo(896.695, 290.197);
        ((GeneralPath) shape).lineTo(896.857, 290.09);
        ((GeneralPath) shape).lineTo(897.02, 289.984);
        ((GeneralPath) shape).lineTo(897.183, 289.879);
        ((GeneralPath) shape).lineTo(897.345, 289.775);
        ((GeneralPath) shape).lineTo(897.508, 289.672);
        ((GeneralPath) shape).lineTo(897.671, 289.57);
        ((GeneralPath) shape).lineTo(897.834, 289.469);
        ((GeneralPath) shape).lineTo(897.999, 289.367);
        ((GeneralPath) shape).lineTo(898.324, 289.17);
        ((GeneralPath) shape).lineTo(898.65, 288.975);
        ((GeneralPath) shape).lineTo(898.976, 288.784);
        ((GeneralPath) shape).lineTo(899.303, 288.596);
        ((GeneralPath) shape).lineTo(899.629, 288.412);
        ((GeneralPath) shape).lineTo(899.955, 288.231);
        ((GeneralPath) shape).lineTo(900.281, 288.053);
        ((GeneralPath) shape).lineTo(900.607, 287.878);
        ((GeneralPath) shape).lineTo(900.933, 287.706);
        ((GeneralPath) shape).lineTo(901.258, 287.537);
        ((GeneralPath) shape).lineTo(901.582, 287.372);
        ((GeneralPath) shape).lineTo(901.907, 287.209);
        ((GeneralPath) shape).lineTo(902.231, 287.049);
        ((GeneralPath) shape).lineTo(902.554, 286.892);
        ((GeneralPath) shape).lineTo(902.876, 286.737);
        ((GeneralPath) shape).lineTo(903.198, 286.585);
        ((GeneralPath) shape).lineTo(903.519, 286.436);
        ((GeneralPath) shape).lineTo(903.839, 286.29);
        ((GeneralPath) shape).lineTo(904.159, 286.146);
        ((GeneralPath) shape).lineTo(904.477, 286.004);
        ((GeneralPath) shape).lineTo(904.794, 285.865);
        ((GeneralPath) shape).lineTo(905.111, 285.728);
        ((GeneralPath) shape).lineTo(905.426, 285.593);
        ((GeneralPath) shape).lineTo(905.74, 285.461);
        ((GeneralPath) shape).lineTo(906.053, 285.331);
        ((GeneralPath) shape).lineTo(906.364, 285.203);
        ((GeneralPath) shape).lineTo(906.675, 285.077);
        ((GeneralPath) shape).lineTo(906.983, 284.952);
        ((GeneralPath) shape).lineTo(907.291, 284.83);
        ((GeneralPath) shape).lineTo(907.597, 284.71);
        ((GeneralPath) shape).lineTo(907.901, 284.591);
        ((GeneralPath) shape).lineTo(908.203, 284.475);
        ((GeneralPath) shape).lineTo(908.504, 284.36);
        ((GeneralPath) shape).lineTo(908.804, 284.246);
        ((GeneralPath) shape).lineTo(909.102, 284.134);
        ((GeneralPath) shape).lineTo(909.69, 283.915);
        ((GeneralPath) shape).lineTo(910.271, 283.702);
        ((GeneralPath) shape).lineTo(910.843, 283.493);
        ((GeneralPath) shape).lineTo(911.407, 283.29);
        ((GeneralPath) shape).lineTo(911.961, 283.091);
        ((GeneralPath) shape).lineTo(913.04, 282.704);
        ((GeneralPath) shape).lineTo(913.562, 282.516);
        ((GeneralPath) shape).lineTo(914.074, 282.33);
        ((GeneralPath) shape).lineTo(914.573, 282.146);
        ((GeneralPath) shape).lineTo(915.062, 281.964);
        ((GeneralPath) shape).lineTo(915.299, 281.875);
        ((GeneralPath) shape).lineTo(915.534, 281.785);
        ((GeneralPath) shape).lineTo(915.766, 281.695);
        ((GeneralPath) shape).lineTo(915.994, 281.606);
        ((GeneralPath) shape).lineTo(916.219, 281.517);
        ((GeneralPath) shape).lineTo(916.44, 281.428);
        ((GeneralPath) shape).lineTo(916.658, 281.34);
        ((GeneralPath) shape).lineTo(916.871, 281.251);
        ((GeneralPath) shape).lineTo(917.081, 281.163);
        ((GeneralPath) shape).lineTo(917.287, 281.075);
        ((GeneralPath) shape).lineTo(917.489, 280.986);
        ((GeneralPath) shape).lineTo(917.687, 280.898);
        ((GeneralPath) shape).lineTo(917.88, 280.81);
        ((GeneralPath) shape).lineTo(918.07, 280.722);
        ((GeneralPath) shape).lineTo(918.255, 280.633);
        ((GeneralPath) shape).lineTo(918.435, 280.545);
        ((GeneralPath) shape).lineTo(918.611, 280.456);
        ((GeneralPath) shape).lineTo(918.783, 280.368);
        ((GeneralPath) shape).lineTo(918.95, 280.279);
        ((GeneralPath) shape).lineTo(919.112, 280.191);
        ((GeneralPath) shape).lineTo(919.269, 280.102);
        ((GeneralPath) shape).lineTo(919.422, 280.013);
        ((GeneralPath) shape).lineTo(919.573, 279.923);
        ((GeneralPath) shape).lineTo(919.642, 279.88);
        ((GeneralPath) shape).lineTo(919.713, 279.836);
        ((GeneralPath) shape).lineTo(919.782, 279.791);
        ((GeneralPath) shape).lineTo(919.85, 279.747);
        ((GeneralPath) shape).lineTo(919.917, 279.703);
        ((GeneralPath) shape).lineTo(919.983, 279.658);
        ((GeneralPath) shape).lineTo(920.048, 279.614);
        ((GeneralPath) shape).lineTo(920.11, 279.57);
        ((GeneralPath) shape).lineTo(920.172, 279.526);
        ((GeneralPath) shape).lineTo(920.233, 279.481);
        ((GeneralPath) shape).lineTo(920.292, 279.437);
        ((GeneralPath) shape).lineTo(920.349, 279.393);
        ((GeneralPath) shape).lineTo(920.406, 279.349);
        ((GeneralPath) shape).lineTo(920.462, 279.305);
        ((GeneralPath) shape).lineTo(920.516, 279.261);
        ((GeneralPath) shape).lineTo(920.568, 279.218);
        ((GeneralPath) shape).lineTo(920.619, 279.174);
        ((GeneralPath) shape).lineTo(920.669, 279.13);
        ((GeneralPath) shape).lineTo(920.718, 279.087);
        ((GeneralPath) shape).lineTo(920.765, 279.044);
        ((GeneralPath) shape).lineTo(920.812, 279.0);
        ((GeneralPath) shape).lineTo(920.857, 278.957);
        ((GeneralPath) shape).lineTo(920.9, 278.914);
        ((GeneralPath) shape).lineTo(920.942, 278.871);
        ((GeneralPath) shape).lineTo(920.984, 278.828);
        ((GeneralPath) shape).lineTo(921.023, 278.785);
        ((GeneralPath) shape).lineTo(921.061, 278.743);
        ((GeneralPath) shape).lineTo(921.099, 278.701);
        ((GeneralPath) shape).lineTo(921.135, 278.658);
        ((GeneralPath) shape).lineTo(921.17, 278.616);
        ((GeneralPath) shape).lineTo(921.204, 278.574);
        ((GeneralPath) shape).lineTo(921.236, 278.533);
        ((GeneralPath) shape).lineTo(921.268, 278.491);
        ((GeneralPath) shape).lineTo(921.299, 278.449);
        ((GeneralPath) shape).lineTo(921.328, 278.407);
        ((GeneralPath) shape).lineTo(921.336, 278.396);
        ((GeneralPath) shape).lineTo(921.182, 278.278);
        ((GeneralPath) shape).lineTo(921.018, 278.152);
        ((GeneralPath) shape).lineTo(920.859, 278.03);
        ((GeneralPath) shape).lineTo(920.704, 277.911);
        ((GeneralPath) shape).lineTo(920.553, 277.797);
        ((GeneralPath) shape).lineTo(920.405, 277.686);
        ((GeneralPath) shape).lineTo(920.255, 277.578);
        ((GeneralPath) shape).lineTo(920.184, 277.528);
        ((GeneralPath) shape).lineTo(920.111, 277.477);
        ((GeneralPath) shape).lineTo(920.038, 277.427);
        ((GeneralPath) shape).lineTo(919.964, 277.377);
        ((GeneralPath) shape).lineTo(919.89, 277.329);
        ((GeneralPath) shape).lineTo(919.815, 277.281);
        ((GeneralPath) shape).lineTo(919.74, 277.234);
        ((GeneralPath) shape).lineTo(919.664, 277.188);
        ((GeneralPath) shape).lineTo(919.587, 277.142);
        ((GeneralPath) shape).lineTo(919.509, 277.097);
        ((GeneralPath) shape).lineTo(919.429, 277.053);
        ((GeneralPath) shape).lineTo(919.348, 277.009);
        ((GeneralPath) shape).lineTo(919.265, 276.965);
        ((GeneralPath) shape).lineTo(919.18, 276.922);
        ((GeneralPath) shape).lineTo(919.094, 276.879);
        ((GeneralPath) shape).lineTo(919.005, 276.837);
        ((GeneralPath) shape).lineTo(918.914, 276.795);
        ((GeneralPath) shape).lineTo(918.82, 276.753);
        ((GeneralPath) shape).lineTo(918.724, 276.712);
        ((GeneralPath) shape).lineTo(918.624, 276.671);
        ((GeneralPath) shape).lineTo(918.522, 276.631);
        ((GeneralPath) shape).lineTo(918.417, 276.591);
        ((GeneralPath) shape).lineTo(918.308, 276.552);
        ((GeneralPath) shape).lineTo(918.196, 276.512);
        ((GeneralPath) shape).lineTo(918.08, 276.474);
        ((GeneralPath) shape).lineTo(917.961, 276.435);
        ((GeneralPath) shape).lineTo(917.837, 276.398);
        ((GeneralPath) shape).lineTo(917.71, 276.36);
        ((GeneralPath) shape).lineTo(917.578, 276.324);
        ((GeneralPath) shape).lineTo(917.442, 276.287);
        ((GeneralPath) shape).lineTo(917.302, 276.252);
        ((GeneralPath) shape).lineTo(917.157, 276.217);
        ((GeneralPath) shape).lineTo(917.008, 276.182);
        ((GeneralPath) shape).lineTo(916.854, 276.149);
        ((GeneralPath) shape).lineTo(916.694, 276.116);
        ((GeneralPath) shape).lineTo(916.53, 276.084);
        ((GeneralPath) shape).lineTo(916.361, 276.052);
        ((GeneralPath) shape).lineTo(916.186, 276.022);
        ((GeneralPath) shape).lineTo(916.006, 275.992);
        ((GeneralPath) shape).lineTo(915.821, 275.964);
        ((GeneralPath) shape).lineTo(915.63, 275.936);
        ((GeneralPath) shape).lineTo(915.433, 275.909);
        ((GeneralPath) shape).lineTo(915.231, 275.883);
        ((GeneralPath) shape).lineTo(915.023, 275.858);
        ((GeneralPath) shape).lineTo(914.809, 275.834);
        ((GeneralPath) shape).lineTo(914.589, 275.812);
        ((GeneralPath) shape).lineTo(914.362, 275.79);
        ((GeneralPath) shape).lineTo(914.13, 275.77);
        ((GeneralPath) shape).lineTo(913.891, 275.751);
        ((GeneralPath) shape).lineTo(913.645, 275.733);
        ((GeneralPath) shape).lineTo(913.393, 275.716);
        ((GeneralPath) shape).lineTo(913.135, 275.701);
        ((GeneralPath) shape).lineTo(912.87, 275.687);
        ((GeneralPath) shape).lineTo(912.597, 275.674);
        ((GeneralPath) shape).lineTo(912.318, 275.662);
        ((GeneralPath) shape).lineTo(912.032, 275.652);
        ((GeneralPath) shape).lineTo(911.739, 275.644);
        ((GeneralPath) shape).lineTo(911.438, 275.637);
        ((GeneralPath) shape).lineTo(911.131, 275.631);
        ((GeneralPath) shape).lineTo(910.815, 275.627);
        ((GeneralPath) shape).lineTo(910.493, 275.624);
        ((GeneralPath) shape).lineTo(910.162, 275.623);
        ((GeneralPath) shape).lineTo(909.824, 275.623);
        ((GeneralPath) shape).lineTo(909.479, 275.625);
        ((GeneralPath) shape).lineTo(909.125, 275.629);
        ((GeneralPath) shape).lineTo(908.763, 275.634);
        ((GeneralPath) shape).lineTo(908.393, 275.641);
        ((GeneralPath) shape).lineTo(908.015, 275.65);
        ((GeneralPath) shape).lineTo(907.629, 275.66);
        ((GeneralPath) shape).lineTo(907.234, 275.672);
        ((GeneralPath) shape).lineTo(906.831, 275.686);
        ((GeneralPath) shape).lineTo(906.419, 275.702);
        ((GeneralPath) shape).lineTo(905.999, 275.719);
        ((GeneralPath) shape).lineTo(905.569, 275.738);
        ((GeneralPath) shape).lineTo(905.131, 275.759);
        ((GeneralPath) shape).lineTo(904.684, 275.782);
        ((GeneralPath) shape).lineTo(904.228, 275.807);
        ((GeneralPath) shape).lineTo(903.762, 275.834);
        ((GeneralPath) shape).lineTo(903.287, 275.863);
        ((GeneralPath) shape).lineTo(902.803, 275.893);
        ((GeneralPath) shape).lineTo(902.309, 275.926);
        ((GeneralPath) shape).lineTo(901.806, 275.961);
        ((GeneralPath) shape).lineTo(901.293, 275.997);
        ((GeneralPath) shape).lineTo(900.77, 276.036);
        ((GeneralPath) shape).lineTo(900.238, 276.077);
        ((GeneralPath) shape).lineTo(899.695, 276.12);
        ((GeneralPath) shape).lineTo(899.142, 276.165);
        ((GeneralPath) shape).lineTo(898.579, 276.212);
        ((GeneralPath) shape).lineTo(898.006, 276.261);
        ((GeneralPath) shape).lineTo(897.422, 276.312);
        ((GeneralPath) shape).lineTo(896.828, 276.366);
        ((GeneralPath) shape).lineTo(896.223, 276.422);
        ((GeneralPath) shape).lineTo(895.607, 276.48);
        ((GeneralPath) shape).lineTo(894.98, 276.54);
        ((GeneralPath) shape).lineTo(894.343, 276.603);
        ((GeneralPath) shape).lineTo(893.695, 276.668);
        ((GeneralPath) shape).lineTo(893.035, 276.735);
        ((GeneralPath) shape).lineTo(892.364, 276.805);
        ((GeneralPath) shape).lineTo(891.682, 276.877);
        ((GeneralPath) shape).lineTo(890.988, 276.951);
        ((GeneralPath) shape).lineTo(890.283, 277.028);
        ((GeneralPath) shape).lineTo(889.565, 277.107);
        ((GeneralPath) shape).lineTo(533.312, 316.729);
        ((GeneralPath) shape).lineTo(533.811, 313.869);
        ((GeneralPath) shape).lineTo(533.905, 313.316);
        ((GeneralPath) shape).lineTo(533.998, 312.769);
        ((GeneralPath) shape).lineTo(534.087, 312.227);
        ((GeneralPath) shape).lineTo(534.174, 311.691);
        ((GeneralPath) shape).lineTo(534.259, 311.161);
        ((GeneralPath) shape).lineTo(534.341, 310.636);
        ((GeneralPath) shape).lineTo(534.421, 310.116);
        ((GeneralPath) shape).lineTo(534.498, 309.601);
        ((GeneralPath) shape).lineTo(534.573, 309.091);
        ((GeneralPath) shape).lineTo(534.646, 308.585);
        ((GeneralPath) shape).lineTo(534.717, 308.084);
        ((GeneralPath) shape).lineTo(534.786, 307.588);
        ((GeneralPath) shape).lineTo(534.852, 307.095);
        ((GeneralPath) shape).lineTo(534.916, 306.606);
        ((GeneralPath) shape).lineTo(534.979, 306.121);
        ((GeneralPath) shape).lineTo(535.039, 305.64);
        ((GeneralPath) shape).lineTo(535.098, 305.162);
        ((GeneralPath) shape).lineTo(535.155, 304.687);
        ((GeneralPath) shape).lineTo(535.21, 304.215);
        ((GeneralPath) shape).lineTo(535.263, 303.746);
        ((GeneralPath) shape).lineTo(535.315, 303.279);
        ((GeneralPath) shape).lineTo(535.364, 302.815);
        ((GeneralPath) shape).lineTo(535.413, 302.354);
        ((GeneralPath) shape).lineTo(535.459, 301.894);
        ((GeneralPath) shape).lineTo(535.505, 301.437);
        ((GeneralPath) shape).lineTo(535.549, 300.981);
        ((GeneralPath) shape).lineTo(535.591, 300.527);
        ((GeneralPath) shape).lineTo(535.632, 300.074);
        ((GeneralPath) shape).lineTo(535.672, 299.622);
        ((GeneralPath) shape).lineTo(535.71, 299.172);
        ((GeneralPath) shape).lineTo(535.747, 298.722);
        ((GeneralPath) shape).lineTo(535.784, 298.273);
        ((GeneralPath) shape).lineTo(535.819, 297.825);
        ((GeneralPath) shape).lineTo(535.853, 297.377);
        ((GeneralPath) shape).lineTo(535.885, 296.929);
        ((GeneralPath) shape).lineTo(535.917, 296.481);
        ((GeneralPath) shape).lineTo(535.948, 296.032);
        ((GeneralPath) shape).lineTo(535.979, 295.584);
        ((GeneralPath) shape).lineTo(536.008, 295.135);
        ((GeneralPath) shape).lineTo(536.037, 294.685);
        ((GeneralPath) shape).lineTo(536.064, 294.235);
        ((GeneralPath) shape).lineTo(536.118, 293.329);
        ((GeneralPath) shape).lineTo(536.17, 292.419);
        ((GeneralPath) shape).lineTo(536.219, 291.501);
        ((GeneralPath) shape).lineTo(536.267, 290.575);
        ((GeneralPath) shape).lineTo(536.314, 289.639);
        ((GeneralPath) shape).lineTo(536.36, 288.691);
        ((GeneralPath) shape).lineTo(536.405, 287.732);
        ((GeneralPath) shape).lineTo(536.497, 285.768);
        ((GeneralPath) shape).lineTo(536.668, 282.113);
        ((GeneralPath) shape).lineTo(612.113, 280.225);
        ((GeneralPath) shape).lineTo(612.176, 280.217);
        ((GeneralPath) shape).lineTo(612.35, 280.195);
        ((GeneralPath) shape).lineTo(612.589, 280.164);
        ((GeneralPath) shape).lineTo(612.886, 280.124);
        ((GeneralPath) shape).lineTo(613.236, 280.074);
        ((GeneralPath) shape).lineTo(613.636, 280.016);
        ((GeneralPath) shape).lineTo(614.08, 279.949);
        ((GeneralPath) shape).lineTo(614.315, 279.913);
        ((GeneralPath) shape).lineTo(614.56, 279.874);
        ((GeneralPath) shape).lineTo(614.814, 279.833);
        ((GeneralPath) shape).lineTo(615.075, 279.79);
        ((GeneralPath) shape).lineTo(615.344, 279.745);
        ((GeneralPath) shape).lineTo(615.619, 279.697);
        ((GeneralPath) shape).lineTo(615.9, 279.648);
        ((GeneralPath) shape).lineTo(616.186, 279.596);
        ((GeneralPath) shape).lineTo(616.477, 279.542);
        ((GeneralPath) shape).lineTo(616.772, 279.486);
        ((GeneralPath) shape).lineTo(617.07, 279.429);
        ((GeneralPath) shape).lineTo(617.37, 279.369);
        ((GeneralPath) shape).lineTo(617.673, 279.307);
        ((GeneralPath) shape).lineTo(617.977, 279.243);
        ((GeneralPath) shape).lineTo(618.282, 279.177);
        ((GeneralPath) shape).lineTo(618.586, 279.109);
        ((GeneralPath) shape).lineTo(618.891, 279.04);
        ((GeneralPath) shape).lineTo(619.193, 278.968);
        ((GeneralPath) shape).lineTo(619.494, 278.895);
        ((GeneralPath) shape).lineTo(619.792, 278.82);
        ((GeneralPath) shape).lineTo(620.087, 278.743);
        ((GeneralPath) shape).lineTo(620.38, 278.663);
        ((GeneralPath) shape).lineTo(620.521, 278.624);
        ((GeneralPath) shape).lineTo(620.663, 278.584);
        ((GeneralPath) shape).lineTo(620.804, 278.543);
        ((GeneralPath) shape).lineTo(620.944, 278.502);
        ((GeneralPath) shape).lineTo(621.082, 278.461);
        ((GeneralPath) shape).lineTo(621.218, 278.419);
        ((GeneralPath) shape).lineTo(621.352, 278.377);
        ((GeneralPath) shape).lineTo(621.485, 278.334);
        ((GeneralPath) shape).lineTo(621.615, 278.291);
        ((GeneralPath) shape).lineTo(621.744, 278.248);
        ((GeneralPath) shape).lineTo(621.87, 278.205);
        ((GeneralPath) shape).lineTo(621.994, 278.161);
        ((GeneralPath) shape).lineTo(622.116, 278.118);
        ((GeneralPath) shape).lineTo(622.235, 278.074);
        ((GeneralPath) shape).lineTo(622.352, 278.03);
        ((GeneralPath) shape).lineTo(622.466, 277.985);
        ((GeneralPath) shape).lineTo(622.577, 277.941);
        ((GeneralPath) shape).lineTo(622.685, 277.897);
        ((GeneralPath) shape).lineTo(622.79, 277.852);
        ((GeneralPath) shape).lineTo(622.892, 277.808);
        ((GeneralPath) shape).lineTo(622.99, 277.764);
        ((GeneralPath) shape).lineTo(623.085, 277.72);
        ((GeneralPath) shape).lineTo(623.177, 277.676);
        ((GeneralPath) shape).lineTo(623.265, 277.633);
        ((GeneralPath) shape).lineTo(623.349, 277.59);
        ((GeneralPath) shape).lineTo(623.428, 277.547);
        ((GeneralPath) shape).lineTo(623.504, 277.506);
        ((GeneralPath) shape).lineTo(623.563, 277.472);
        ((GeneralPath) shape).lineTo(623.509, 277.439);
        ((GeneralPath) shape).lineTo(623.433, 277.393);
        ((GeneralPath) shape).lineTo(623.352, 277.347);
        ((GeneralPath) shape).lineTo(623.266, 277.3);
        ((GeneralPath) shape).lineTo(623.177, 277.253);
        ((GeneralPath) shape).lineTo(623.084, 277.205);
        ((GeneralPath) shape).lineTo(622.986, 277.156);
        ((GeneralPath) shape).lineTo(622.886, 277.108);
        ((GeneralPath) shape).lineTo(622.782, 277.059);
        ((GeneralPath) shape).lineTo(622.674, 277.01);
        ((GeneralPath) shape).lineTo(622.563, 276.961);
        ((GeneralPath) shape).lineTo(622.449, 276.911);
        ((GeneralPath) shape).lineTo(622.332, 276.862);
        ((GeneralPath) shape).lineTo(622.212, 276.813);
        ((GeneralPath) shape).lineTo(622.09, 276.763);
        ((GeneralPath) shape).lineTo(621.964, 276.714);
        ((GeneralPath) shape).lineTo(621.837, 276.665);
        ((GeneralPath) shape).lineTo(621.706, 276.616);
        ((GeneralPath) shape).lineTo(621.574, 276.568);
        ((GeneralPath) shape).lineTo(621.439, 276.519);
        ((GeneralPath) shape).lineTo(621.302, 276.471);
        ((GeneralPath) shape).lineTo(621.164, 276.423);
        ((GeneralPath) shape).lineTo(621.023, 276.375);
        ((GeneralPath) shape).lineTo(620.881, 276.328);
        ((GeneralPath) shape).lineTo(620.736, 276.28);
        ((GeneralPath) shape).lineTo(620.591, 276.234);
        ((GeneralPath) shape).lineTo(620.443, 276.187);
        ((GeneralPath) shape).lineTo(620.295, 276.141);
        ((GeneralPath) shape).lineTo(620.147, 276.096);
        ((GeneralPath) shape).lineTo(619.841, 276.005);
        ((GeneralPath) shape).lineTo(619.533, 275.916);
        ((GeneralPath) shape).lineTo(619.222, 275.828);
        ((GeneralPath) shape).lineTo(618.908, 275.743);
        ((GeneralPath) shape).lineTo(618.593, 275.659);
        ((GeneralPath) shape).lineTo(618.275, 275.577);
        ((GeneralPath) shape).lineTo(617.958, 275.497);
        ((GeneralPath) shape).lineTo(617.64, 275.419);
        ((GeneralPath) shape).lineTo(617.323, 275.343);
        ((GeneralPath) shape).lineTo(617.008, 275.269);
        ((GeneralPath) shape).lineTo(616.695, 275.197);
        ((GeneralPath) shape).lineTo(616.384, 275.128);
        ((GeneralPath) shape).lineTo(616.077, 275.06);
        ((GeneralPath) shape).lineTo(615.774, 274.995);
        ((GeneralPath) shape).lineTo(615.476, 274.932);
        ((GeneralPath) shape).lineTo(615.184, 274.871);
        ((GeneralPath) shape).lineTo(614.897, 274.813);
        ((GeneralPath) shape).lineTo(614.618, 274.757);
        ((GeneralPath) shape).lineTo(614.345, 274.703);
        ((GeneralPath) shape).lineTo(614.081, 274.652);
        ((GeneralPath) shape).lineTo(613.826, 274.604);
        ((GeneralPath) shape).lineTo(613.581, 274.558);
        ((GeneralPath) shape).lineTo(613.118, 274.473);
        ((GeneralPath) shape).lineTo(612.703, 274.399);
        ((GeneralPath) shape).lineTo(612.337, 274.336);
        ((GeneralPath) shape).lineTo(612.028, 274.284);
        ((GeneralPath) shape).lineTo(611.78, 274.244);
        ((GeneralPath) shape).lineTo(611.598, 274.214);
        ((GeneralPath) shape).lineTo(611.578, 274.211);
        ((GeneralPath) shape).lineTo(536.78, 274.211);
        ((GeneralPath) shape).lineTo(536.857, 272.355);
        ((GeneralPath) shape).lineTo(536.86, 272.252);
        ((GeneralPath) shape).lineTo(536.865, 272.077);
        ((GeneralPath) shape).lineTo(536.869, 271.828);
        ((GeneralPath) shape).lineTo(536.871, 271.68);
        ((GeneralPath) shape).lineTo(536.872, 271.514);
        ((GeneralPath) shape).lineTo(536.873, 271.331);
        ((GeneralPath) shape).lineTo(536.872, 271.132);
        ((GeneralPath) shape).lineTo(536.87, 270.917);
        ((GeneralPath) shape).lineTo(536.868, 270.686);
        ((GeneralPath) shape).lineTo(536.863, 270.44);
        ((GeneralPath) shape).lineTo(536.857, 270.178);
        ((GeneralPath) shape).lineTo(536.85, 269.903);
        ((GeneralPath) shape).lineTo(536.84, 269.612);
        ((GeneralPath) shape).lineTo(536.828, 269.308);
        ((GeneralPath) shape).lineTo(536.814, 268.99);
        ((GeneralPath) shape).lineTo(536.798, 268.658);
        ((GeneralPath) shape).lineTo(536.779, 268.313);
        ((GeneralPath) shape).lineTo(536.757, 267.956);
        ((GeneralPath) shape).lineTo(536.732, 267.586);
        ((GeneralPath) shape).lineTo(536.704, 267.203);
        ((GeneralPath) shape).lineTo(536.673, 266.809);
        ((GeneralPath) shape).lineTo(536.638, 266.403);
        ((GeneralPath) shape).lineTo(536.6, 265.986);
        ((GeneralPath) shape).lineTo(536.558, 265.557);
        ((GeneralPath) shape).lineTo(536.535, 265.34);
        ((GeneralPath) shape).lineTo(536.512, 265.119);
        ((GeneralPath) shape).lineTo(536.487, 264.896);
        ((GeneralPath) shape).lineTo(536.462, 264.67);
        ((GeneralPath) shape).lineTo(536.435, 264.442);
        ((GeneralPath) shape).lineTo(536.407, 264.211);
        ((GeneralPath) shape).lineTo(536.378, 263.978);
        ((GeneralPath) shape).lineTo(536.348, 263.743);
        ((GeneralPath) shape).lineTo(536.317, 263.505);
        ((GeneralPath) shape).lineTo(536.285, 263.265);
        ((GeneralPath) shape).lineTo(536.251, 263.022);
        ((GeneralPath) shape).lineTo(536.216, 262.777);
        ((GeneralPath) shape).lineTo(536.18, 262.531);
        ((GeneralPath) shape).lineTo(536.143, 262.282);
        ((GeneralPath) shape).lineTo(536.104, 262.03);
        ((GeneralPath) shape).lineTo(536.064, 261.777);
        ((GeneralPath) shape).lineTo(536.023, 261.522);
        ((GeneralPath) shape).lineTo(535.981, 261.265);
        ((GeneralPath) shape).lineTo(535.937, 261.006);
        ((GeneralPath) shape).lineTo(535.891, 260.745);
        ((GeneralPath) shape).lineTo(535.845, 260.482);
        ((GeneralPath) shape).lineTo(535.797, 260.217);
        ((GeneralPath) shape).lineTo(535.747, 259.95);
        ((GeneralPath) shape).lineTo(535.696, 259.682);
        ((GeneralPath) shape).lineTo(535.643, 259.412);
        ((GeneralPath) shape).lineTo(535.589, 259.14);
        ((GeneralPath) shape).lineTo(535.534, 258.867);
        ((GeneralPath) shape).lineTo(535.477, 258.592);
        ((GeneralPath) shape).lineTo(535.418, 258.315);
        ((GeneralPath) shape).lineTo(535.357, 258.037);
        ((GeneralPath) shape).lineTo(535.296, 257.757);
        ((GeneralPath) shape).lineTo(535.232, 257.476);
        ((GeneralPath) shape).lineTo(535.167, 257.194);
        ((GeneralPath) shape).lineTo(535.1, 256.91);
        ((GeneralPath) shape).lineTo(535.031, 256.625);
        ((GeneralPath) shape).lineTo(534.961, 256.339);
        ((GeneralPath) shape).lineTo(534.889, 256.051);
        ((GeneralPath) shape).lineTo(534.815, 255.762);
        ((GeneralPath) shape).lineTo(534.74, 255.472);
        ((GeneralPath) shape).lineTo(534.662, 255.181);
        ((GeneralPath) shape).lineTo(534.583, 254.889);
        ((GeneralPath) shape).lineTo(534.502, 254.596);
        ((GeneralPath) shape).lineTo(534.419, 254.301);
        ((GeneralPath) shape).lineTo(534.334, 254.006);
        ((GeneralPath) shape).lineTo(534.248, 253.71);
        ((GeneralPath) shape).lineTo(534.159, 253.413);
        ((GeneralPath) shape).lineTo(534.069, 253.115);
        ((GeneralPath) shape).lineTo(533.976, 252.816);
        ((GeneralPath) shape).lineTo(533.882, 252.517);
        ((GeneralPath) shape).lineTo(533.786, 252.217);
        ((GeneralPath) shape).lineTo(533.687, 251.916);
        ((GeneralPath) shape).lineTo(533.587, 251.614);
        ((GeneralPath) shape).lineTo(533.484, 251.312);
        ((GeneralPath) shape).lineTo(533.379, 251.009);
        ((GeneralPath) shape).lineTo(533.273, 250.706);
        ((GeneralPath) shape).lineTo(533.164, 250.402);
        ((GeneralPath) shape).lineTo(533.053, 250.097);
        ((GeneralPath) shape).lineTo(532.94, 249.792);
        ((GeneralPath) shape).lineTo(532.825, 249.487);
        ((GeneralPath) shape).lineTo(532.707, 249.182);
        ((GeneralPath) shape).lineTo(532.587, 248.876);
        ((GeneralPath) shape).lineTo(532.466, 248.57);
        ((GeneralPath) shape).lineTo(532.341, 248.263);
        ((GeneralPath) shape).lineTo(532.215, 247.957);
        ((GeneralPath) shape).lineTo(532.086, 247.65);
        ((GeneralPath) shape).lineTo(531.955, 247.343);
        ((GeneralPath) shape).lineTo(531.822, 247.036);
        ((GeneralPath) shape).lineTo(531.686, 246.729);
        ((GeneralPath) shape).lineTo(531.548, 246.422);
        ((GeneralPath) shape).lineTo(531.407, 246.116);
        ((GeneralPath) shape).lineTo(531.264, 245.809);
        ((GeneralPath) shape).lineTo(531.119, 245.502);
        ((GeneralPath) shape).lineTo(530.971, 245.195);
        ((GeneralPath) shape).lineTo(530.821, 244.889);
        ((GeneralPath) shape).lineTo(530.668, 244.583);
        ((GeneralPath) shape).lineTo(530.513, 244.277);
        ((GeneralPath) shape).lineTo(530.355, 243.971);
        ((GeneralPath) shape).lineTo(530.195, 243.666);
        ((GeneralPath) shape).lineTo(530.032, 243.361);
        ((GeneralPath) shape).lineTo(529.867, 243.056);
        ((GeneralPath) shape).lineTo(529.698, 242.752);
        ((GeneralPath) shape).lineTo(529.528, 242.448);
        ((GeneralPath) shape).lineTo(529.354, 242.145);
        ((GeneralPath) shape).lineTo(529.178, 241.843);
        ((GeneralPath) shape).lineTo(529.0, 241.541);
        ((GeneralPath) shape).lineTo(528.818, 241.239);
        ((GeneralPath) shape).lineTo(528.634, 240.939);
        ((GeneralPath) shape).lineTo(528.447, 240.639);
        ((GeneralPath) shape).lineTo(528.257, 240.339);
        ((GeneralPath) shape).lineTo(528.065, 240.041);
        ((GeneralPath) shape).lineTo(527.87, 239.743);
        ((GeneralPath) shape).lineTo(527.672, 239.446);
        ((GeneralPath) shape).lineTo(527.47, 239.149);
        ((GeneralPath) shape).lineTo(527.369, 239.003);
        ((GeneralPath) shape).lineTo(527.267, 238.855);
        ((GeneralPath) shape).lineTo(527.164, 238.708);
        ((GeneralPath) shape).lineTo(527.06, 238.561);
        ((GeneralPath) shape).lineTo(526.956, 238.415);
        ((GeneralPath) shape).lineTo(526.851, 238.268);
        ((GeneralPath) shape).lineTo(526.745, 238.122);
        ((GeneralPath) shape).lineTo(526.638, 237.976);
        ((GeneralPath) shape).lineTo(526.531, 237.831);
        ((GeneralPath) shape).lineTo(526.423, 237.685);
        ((GeneralPath) shape).lineTo(526.314, 237.54);
        ((GeneralPath) shape).lineTo(526.204, 237.395);
        ((GeneralPath) shape).lineTo(526.094, 237.251);
        ((GeneralPath) shape).lineTo(525.983, 237.107);
        ((GeneralPath) shape).lineTo(525.871, 236.963);
        ((GeneralPath) shape).lineTo(525.759, 236.819);
        ((GeneralPath) shape).lineTo(525.645, 236.676);
        ((GeneralPath) shape).lineTo(525.531, 236.533);
        ((GeneralPath) shape).lineTo(525.416, 236.39);
        ((GeneralPath) shape).lineTo(525.301, 236.248);
        ((GeneralPath) shape).lineTo(525.184, 236.106);
        ((GeneralPath) shape).lineTo(525.067, 235.964);
        ((GeneralPath) shape).lineTo(524.949, 235.823);
        ((GeneralPath) shape).lineTo(524.831, 235.681);
        ((GeneralPath) shape).lineTo(524.711, 235.541);
        ((GeneralPath) shape).lineTo(524.591, 235.4);
        ((GeneralPath) shape).lineTo(524.47, 235.26);
        ((GeneralPath) shape).lineTo(524.348, 235.12);
        ((GeneralPath) shape).lineTo(524.225, 234.981);
        ((GeneralPath) shape).lineTo(524.102, 234.842);
        ((GeneralPath) shape).lineTo(523.978, 234.703);
        ((GeneralPath) shape).lineTo(523.853, 234.565);
        ((GeneralPath) shape).lineTo(523.727, 234.427);
        ((GeneralPath) shape).lineTo(523.6, 234.29);
        ((GeneralPath) shape).lineTo(523.473, 234.153);
        ((GeneralPath) shape).lineTo(523.345, 234.016);
        ((GeneralPath) shape).lineTo(523.216, 233.879);
        ((GeneralPath) shape).lineTo(523.086, 233.743);
        ((GeneralPath) shape).lineTo(522.955, 233.608);
        ((GeneralPath) shape).lineTo(522.824, 233.473);
        ((GeneralPath) shape).lineTo(522.692, 233.338);
        ((GeneralPath) shape).lineTo(522.558, 233.203);
        ((GeneralPath) shape).lineTo(522.424, 233.069);
        ((GeneralPath) shape).lineTo(522.29, 232.936);
        ((GeneralPath) shape).lineTo(522.154, 232.803);
        ((GeneralPath) shape).lineTo(522.018, 232.67);
        ((GeneralPath) shape).lineTo(521.88, 232.538);
        ((GeneralPath) shape).lineTo(521.742, 232.406);
        ((GeneralPath) shape).lineTo(521.603, 232.275);
        ((GeneralPath) shape).lineTo(521.463, 232.144);
        ((GeneralPath) shape).lineTo(521.323, 232.013);
        ((GeneralPath) shape).lineTo(521.181, 231.883);
        ((GeneralPath) shape).lineTo(521.039, 231.754);
        ((GeneralPath) shape).lineTo(520.896, 231.625);
        ((GeneralPath) shape).lineTo(520.752, 231.496);
        ((GeneralPath) shape).lineTo(520.607, 231.368);
        ((GeneralPath) shape).lineTo(520.461, 231.24);
        ((GeneralPath) shape).lineTo(520.314, 231.113);
        ((GeneralPath) shape).lineTo(520.167, 230.986);
        ((GeneralPath) shape).lineTo(520.018, 230.86);
        ((GeneralPath) shape).lineTo(519.869, 230.734);
        ((GeneralPath) shape).lineTo(519.719, 230.609);
        ((GeneralPath) shape).lineTo(519.568, 230.484);
        ((GeneralPath) shape).lineTo(519.416, 230.36);
        ((GeneralPath) shape).lineTo(519.263, 230.236);
        ((GeneralPath) shape).lineTo(519.109, 230.113);
        ((GeneralPath) shape).lineTo(518.955, 229.99);
        ((GeneralPath) shape).lineTo(518.799, 229.868);
        ((GeneralPath) shape).lineTo(518.643, 229.747);
        ((GeneralPath) shape).lineTo(518.485, 229.625);
        ((GeneralPath) shape).lineTo(518.327, 229.505);
        ((GeneralPath) shape).lineTo(518.168, 229.385);
        ((GeneralPath) shape).lineTo(518.008, 229.265);
        ((GeneralPath) shape).lineTo(517.847, 229.146);
        ((GeneralPath) shape).lineTo(517.685, 229.028);
        ((GeneralPath) shape).lineTo(517.522, 228.91);
        ((GeneralPath) shape).lineTo(517.358, 228.793);
        ((GeneralPath) shape).lineTo(517.194, 228.676);
        ((GeneralPath) shape).lineTo(517.028, 228.56);
        ((GeneralPath) shape).lineTo(516.862, 228.444);
        ((GeneralPath) shape).lineTo(516.694, 228.329);
        ((GeneralPath) shape).lineTo(516.525, 228.215);
        ((GeneralPath) shape).lineTo(516.356, 228.101);
        ((GeneralPath) shape).lineTo(516.186, 227.988);
        ((GeneralPath) shape).lineTo(516.014, 227.875);
        ((GeneralPath) shape).lineTo(515.842, 227.763);
        ((GeneralPath) shape).lineTo(515.669, 227.652);
        ((GeneralPath) shape).lineTo(515.495, 227.541);
        ((GeneralPath) shape).lineTo(515.32, 227.431);
        ((GeneralPath) shape).lineTo(515.144, 227.321);
        ((GeneralPath) shape).lineTo(514.967, 227.212);
        ((GeneralPath) shape).lineTo(514.788, 227.104);
        ((GeneralPath) shape).lineTo(514.609, 226.996);
        ((GeneralPath) shape).lineTo(514.429, 226.889);
        ((GeneralPath) shape).lineTo(514.249, 226.783);
        ((GeneralPath) shape).lineTo(514.067, 226.677);
        ((GeneralPath) shape).lineTo(513.884, 226.572);
        ((GeneralPath) shape).lineTo(513.699, 226.467);
        ((GeneralPath) shape).lineTo(513.515, 226.363);
        ((GeneralPath) shape).lineTo(513.329, 226.26);
        ((GeneralPath) shape).lineTo(513.142, 226.158);
        ((GeneralPath) shape).lineTo(512.954, 226.056);
        ((GeneralPath) shape).lineTo(512.765, 225.955);
        ((GeneralPath) shape).lineTo(512.575, 225.854);
        ((GeneralPath) shape).lineTo(512.384, 225.755);
        ((GeneralPath) shape).lineTo(512.192, 225.656);
        ((GeneralPath) shape).lineTo(511.999, 225.557);
        ((GeneralPath) shape).lineTo(511.804, 225.46);
        ((GeneralPath) shape).lineTo(511.609, 225.363);
        ((GeneralPath) shape).lineTo(511.413, 225.266);
        ((GeneralPath) shape).lineTo(511.216, 225.171);
        ((GeneralPath) shape).lineTo(511.018, 225.076);
        ((GeneralPath) shape).lineTo(510.818, 224.982);
        ((GeneralPath) shape).lineTo(510.618, 224.888);
        ((GeneralPath) shape).lineTo(510.417, 224.796);
        ((GeneralPath) shape).lineTo(510.214, 224.704);
        ((GeneralPath) shape).lineTo(510.011, 224.613);
        ((GeneralPath) shape).lineTo(509.806, 224.522);
        ((GeneralPath) shape).lineTo(509.601, 224.433);
        ((GeneralPath) shape).lineTo(509.394, 224.344);
        ((GeneralPath) shape).lineTo(509.186, 224.256);
        ((GeneralPath) shape).lineTo(508.978, 224.168);
        ((GeneralPath) shape).lineTo(508.768, 224.082);
        ((GeneralPath) shape).lineTo(508.557, 223.996);
        ((GeneralPath) shape).lineTo(508.345, 223.911);
        ((GeneralPath) shape).lineTo(508.132, 223.826);
        ((GeneralPath) shape).lineTo(507.917, 223.743);
        ((GeneralPath) shape).lineTo(507.702, 223.66);
        ((GeneralPath) shape).lineTo(507.486, 223.578);
        ((GeneralPath) shape).lineTo(507.268, 223.497);
        ((GeneralPath) shape).lineTo(507.049, 223.417);
        ((GeneralPath) shape).lineTo(506.83, 223.337);
        ((GeneralPath) shape).lineTo(506.609, 223.259);
        ((GeneralPath) shape).lineTo(506.387, 223.181);
        ((GeneralPath) shape).lineTo(506.164, 223.104);
        ((GeneralPath) shape).lineTo(505.94, 223.028);
        ((GeneralPath) shape).lineTo(505.714, 222.952);
        ((GeneralPath) shape).lineTo(505.488, 222.878);
        ((GeneralPath) shape).lineTo(505.26, 222.804);
        ((GeneralPath) shape).lineTo(505.032, 222.731);
        ((GeneralPath) shape).lineTo(504.802, 222.66);
        ((GeneralPath) shape).lineTo(504.571, 222.588);
        ((GeneralPath) shape).lineTo(504.339, 222.518);
        ((GeneralPath) shape).lineTo(504.105, 222.449);
        ((GeneralPath) shape).lineTo(503.871, 222.38);
        ((GeneralPath) shape).lineTo(503.635, 222.313);
        ((GeneralPath) shape).lineTo(503.399, 222.246);
        ((GeneralPath) shape).lineTo(503.161, 222.18);
        ((GeneralPath) shape).lineTo(502.922, 222.115);
        ((GeneralPath) shape).lineTo(502.682, 222.051);
        ((GeneralPath) shape).lineTo(502.44, 221.988);
        ((GeneralPath) shape).lineTo(502.198, 221.926);
        ((GeneralPath) shape).lineTo(501.954, 221.865);
        ((GeneralPath) shape).lineTo(501.709, 221.805);
        ((GeneralPath) shape).lineTo(501.463, 221.745);
        ((GeneralPath) shape).lineTo(501.216, 221.687);
        ((GeneralPath) shape).lineTo(500.967, 221.629);
        ((GeneralPath) shape).lineTo(500.718, 221.572);
        ((GeneralPath) shape).lineTo(500.467, 221.517);
        ((GeneralPath) shape).lineTo(500.215, 221.462);
        ((GeneralPath) shape).lineTo(499.961, 221.408);
        ((GeneralPath) shape).lineTo(499.707, 221.355);
        ((GeneralPath) shape).lineTo(499.451, 221.304);
        ((GeneralPath) shape).lineTo(499.194, 221.253);
        ((GeneralPath) shape).lineTo(498.936, 221.203);
        ((GeneralPath) shape).lineTo(498.677, 221.154);
        ((GeneralPath) shape).lineTo(498.416, 221.106);
        ((GeneralPath) shape).lineTo(498.155, 221.059);
        ((GeneralPath) shape).lineTo(497.892, 221.013);
        ((GeneralPath) shape).lineTo(497.628, 220.968);
        ((GeneralPath) shape).lineTo(497.362, 220.924);
        ((GeneralPath) shape).lineTo(497.095, 220.882);
        ((GeneralPath) shape).lineTo(496.828, 220.84);
        ((GeneralPath) shape).lineTo(496.558, 220.799);
        ((GeneralPath) shape).lineTo(496.288, 220.759);
        ((GeneralPath) shape).lineTo(496.016, 220.72);
        ((GeneralPath) shape).lineTo(495.743, 220.682);
        ((GeneralPath) shape).lineTo(495.469, 220.646);
        ((GeneralPath) shape).lineTo(495.194, 220.61);
        ((GeneralPath) shape).lineTo(494.917, 220.575);
        ((GeneralPath) shape).lineTo(494.639, 220.542);
        ((GeneralPath) shape).lineTo(494.36, 220.509);
        ((GeneralPath) shape).lineTo(494.08, 220.478);
        ((GeneralPath) shape).lineTo(493.798, 220.448);
        ((GeneralPath) shape).lineTo(493.515, 220.418);
        ((GeneralPath) shape).lineTo(493.231, 220.39);
        ((GeneralPath) shape).lineTo(492.945, 220.363);
        ((GeneralPath) shape).lineTo(492.658, 220.337);
        ((GeneralPath) shape).lineTo(492.37, 220.312);
        ((GeneralPath) shape).lineTo(492.081, 220.288);
        ((GeneralPath) shape).lineTo(491.79, 220.266);
        ((GeneralPath) shape).lineTo(491.498, 220.244);
        ((GeneralPath) shape).lineTo(491.205, 220.224);
        ((GeneralPath) shape).lineTo(490.91, 220.204);
        ((GeneralPath) shape).lineTo(490.614, 220.186);
        ((GeneralPath) shape).lineTo(490.317, 220.169);
        ((GeneralPath) shape).lineTo(490.018, 220.153);
        ((GeneralPath) shape).lineTo(489.718, 220.138);
        ((GeneralPath) shape).lineTo(489.417, 220.125);
        ((GeneralPath) shape).lineTo(489.115, 220.112);
        ((GeneralPath) shape).lineTo(488.811, 220.101);
        ((GeneralPath) shape).lineTo(488.506, 220.091);
        ((GeneralPath) shape).lineTo(488.199, 220.082);
        ((GeneralPath) shape).lineTo(487.892, 220.074);
        ((GeneralPath) shape).lineTo(487.583, 220.067);
        ((GeneralPath) shape).lineTo(487.272, 220.062);
        ((GeneralPath) shape).lineTo(486.96, 220.058);
        ((GeneralPath) shape).lineTo(486.647, 220.055);
        ((GeneralPath) shape).lineTo(486.333, 220.053);
        ((GeneralPath) shape).lineTo(484.33, 220.049);
        ((GeneralPath) shape).lineTo(484.103, 213.877);
        ((GeneralPath) shape).lineTo(483.907, 208.629);
        ((GeneralPath) shape).lineTo(483.688, 202.766);
        ((GeneralPath) shape).lineTo(483.449, 196.432);
        ((GeneralPath) shape).lineTo(483.197, 189.774);
        ((GeneralPath) shape).lineTo(482.938, 182.935);
        ((GeneralPath) shape).lineTo(482.42, 169.296);
        ((GeneralPath) shape).lineTo(481.938, 156.671);
        ((GeneralPath) shape).lineTo(481.538, 146.221);
        ((GeneralPath) shape).lineTo(481.168, 136.545);
        ((GeneralPath) shape).lineTo(481.167, 136.542);
        ((GeneralPath) shape).lineTo(481.162, 136.507);
        ((GeneralPath) shape).lineTo(481.154, 136.462);
        ((GeneralPath) shape).lineTo(481.143, 136.408);
        ((GeneralPath) shape).lineTo(481.129, 136.348);
        ((GeneralPath) shape).lineTo(481.112, 136.276);
        ((GeneralPath) shape).lineTo(481.104, 136.246);
        ((GeneralPath) shape).lineTo(481.094, 136.21);
        ((GeneralPath) shape).lineTo(481.083, 136.173);
        ((GeneralPath) shape).lineTo(481.071, 136.135);
        ((GeneralPath) shape).lineTo(481.059, 136.097);
        ((GeneralPath) shape).lineTo(481.046, 136.058);
        ((GeneralPath) shape).lineTo(481.032, 136.02);
        ((GeneralPath) shape).lineTo(481.018, 135.981);
        ((GeneralPath) shape).lineTo(481.002, 135.942);
        ((GeneralPath) shape).lineTo(480.987, 135.904);
        ((GeneralPath) shape).lineTo(480.97, 135.866);
        ((GeneralPath) shape).lineTo(480.953, 135.829);
        ((GeneralPath) shape).lineTo(480.936, 135.792);
        ((GeneralPath) shape).lineTo(480.917, 135.756);
        ((GeneralPath) shape).lineTo(480.899, 135.721);
        ((GeneralPath) shape).lineTo(480.88, 135.687);
        ((GeneralPath) shape).lineTo(480.861, 135.655);
        ((GeneralPath) shape).lineTo(480.841, 135.623);
        ((GeneralPath) shape).lineTo(480.822, 135.594);
        ((GeneralPath) shape).lineTo(480.802, 135.565);
        ((GeneralPath) shape).lineTo(480.782, 135.538);
        ((GeneralPath) shape).lineTo(480.762, 135.513);
        ((GeneralPath) shape).lineTo(480.743, 135.49);
        ((GeneralPath) shape).lineTo(480.723, 135.467);
        ((GeneralPath) shape).lineTo(480.704, 135.447);
        ((GeneralPath) shape).lineTo(480.685, 135.428);
        ((GeneralPath) shape).lineTo(480.667, 135.411);
        ((GeneralPath) shape).lineTo(480.643, 135.39);
        ((GeneralPath) shape).lineTo(480.639, 135.387);
        ((GeneralPath) shape).lineTo(480.63, 135.379);
        ((GeneralPath) shape).lineTo(480.622, 135.373);
        ((GeneralPath) shape).lineTo(480.613, 135.366);
        ((GeneralPath) shape).lineTo(480.604, 135.36);
        ((GeneralPath) shape).lineTo(480.595, 135.354);
        ((GeneralPath) shape).lineTo(480.586, 135.348);
        ((GeneralPath) shape).lineTo(480.577, 135.342);
        ((GeneralPath) shape).lineTo(480.568, 135.337);
        ((GeneralPath) shape).lineTo(480.559, 135.332);
        ((GeneralPath) shape).lineTo(480.55, 135.327);
        ((GeneralPath) shape).lineTo(480.541, 135.322);
        ((GeneralPath) shape).lineTo(480.531, 135.317);
        ((GeneralPath) shape).lineTo(480.522, 135.312);
        ((GeneralPath) shape).lineTo(480.512, 135.308);
        ((GeneralPath) shape).lineTo(480.501, 135.303);
        ((GeneralPath) shape).lineTo(480.491, 135.299);
        ((GeneralPath) shape).lineTo(480.48, 135.295);
        ((GeneralPath) shape).lineTo(480.468, 135.291);
        ((GeneralPath) shape).lineTo(480.457, 135.287);
        ((GeneralPath) shape).lineTo(480.444, 135.283);
        ((GeneralPath) shape).lineTo(480.431, 135.279);
        ((GeneralPath) shape).lineTo(480.418, 135.276);
        ((GeneralPath) shape).lineTo(480.403, 135.272);
        ((GeneralPath) shape).lineTo(480.389, 135.269);
        ((GeneralPath) shape).lineTo(480.373, 135.266);
        ((GeneralPath) shape).lineTo(480.356, 135.262);
        ((GeneralPath) shape).lineTo(480.339, 135.26);
        ((GeneralPath) shape).lineTo(480.321, 135.257);
        ((GeneralPath) shape).lineTo(480.302, 135.255);
        ((GeneralPath) shape).lineTo(480.282, 135.253);
        ((GeneralPath) shape).lineTo(480.26, 135.251);
        ((GeneralPath) shape).lineTo(480.238, 135.249);
        ((GeneralPath) shape).lineTo(480.215, 135.248);
        ((GeneralPath) shape).lineTo(480.191, 135.248);
        ((GeneralPath) shape).lineTo(480.165, 135.247);
        ((GeneralPath) shape).lineTo(480.139, 135.248);
        ((GeneralPath) shape).lineTo(480.115, 135.248);
        ((GeneralPath) shape).lineTo(480.091, 135.249);
        ((GeneralPath) shape).lineTo(480.069, 135.251);
        ((GeneralPath) shape).lineTo(480.048, 135.253);
        ((GeneralPath) shape).lineTo(480.028, 135.255);
        ((GeneralPath) shape).lineTo(480.009, 135.257);
        ((GeneralPath) shape).lineTo(479.99, 135.26);
        ((GeneralPath) shape).lineTo(479.973, 135.262);
        ((GeneralPath) shape).lineTo(479.957, 135.266);
        ((GeneralPath) shape).lineTo(479.941, 135.269);
        ((GeneralPath) shape).lineTo(479.926, 135.272);
        ((GeneralPath) shape).lineTo(479.912, 135.276);
        ((GeneralPath) shape).lineTo(479.898, 135.279);
        ((GeneralPath) shape).lineTo(479.885, 135.283);
        ((GeneralPath) shape).lineTo(479.873, 135.287);
        ((GeneralPath) shape).lineTo(479.861, 135.291);
        ((GeneralPath) shape).lineTo(479.85, 135.295);
        ((GeneralPath) shape).lineTo(479.839, 135.299);
        ((GeneralPath) shape).lineTo(479.829, 135.303);
        ((GeneralPath) shape).lineTo(479.818, 135.308);
        ((GeneralPath) shape).lineTo(479.808, 135.312);
        ((GeneralPath) shape).lineTo(479.799, 135.317);
        ((GeneralPath) shape).lineTo(479.789, 135.322);
        ((GeneralPath) shape).lineTo(479.78, 135.327);
        ((GeneralPath) shape).lineTo(479.771, 135.332);
        ((GeneralPath) shape).lineTo(479.762, 135.337);
        ((GeneralPath) shape).lineTo(479.753, 135.342);
        ((GeneralPath) shape).lineTo(479.744, 135.348);
        ((GeneralPath) shape).lineTo(479.735, 135.354);
        ((GeneralPath) shape).lineTo(479.726, 135.36);
        ((GeneralPath) shape).lineTo(479.717, 135.366);
        ((GeneralPath) shape).lineTo(479.708, 135.373);
        ((GeneralPath) shape).lineTo(479.699, 135.38);
        ((GeneralPath) shape).lineTo(479.69, 135.387);
        ((GeneralPath) shape).lineTo(479.687, 135.39);
        ((GeneralPath) shape).lineTo(479.663, 135.411);
        ((GeneralPath) shape).lineTo(479.644, 135.428);
        ((GeneralPath) shape).lineTo(479.625, 135.447);
        ((GeneralPath) shape).lineTo(479.606, 135.467);
        ((GeneralPath) shape).lineTo(479.587, 135.489);
        ((GeneralPath) shape).lineTo(479.567, 135.513);
        ((GeneralPath) shape).lineTo(479.548, 135.538);
        ((GeneralPath) shape).lineTo(479.528, 135.565);
        ((GeneralPath) shape).lineTo(479.508, 135.594);
        ((GeneralPath) shape).lineTo(479.489, 135.623);
        ((GeneralPath) shape).lineTo(479.469, 135.655);
        ((GeneralPath) shape).lineTo(479.45, 135.687);
        ((GeneralPath) shape).lineTo(479.431, 135.721);
        ((GeneralPath) shape).lineTo(479.412, 135.756);
        ((GeneralPath) shape).lineTo(479.394, 135.792);
        ((GeneralPath) shape).lineTo(479.376, 135.829);
        ((GeneralPath) shape).lineTo(479.36, 135.866);
        ((GeneralPath) shape).lineTo(479.343, 135.904);
        ((GeneralPath) shape).lineTo(479.327, 135.943);
        ((GeneralPath) shape).lineTo(479.312, 135.981);
        ((GeneralPath) shape).lineTo(479.297, 136.02);
        ((GeneralPath) shape).lineTo(479.284, 136.058);
        ((GeneralPath) shape).lineTo(479.271, 136.097);
        ((GeneralPath) shape).lineTo(479.258, 136.135);
        ((GeneralPath) shape).lineTo(479.247, 136.173);
        ((GeneralPath) shape).lineTo(479.236, 136.21);
        ((GeneralPath) shape).lineTo(479.226, 136.246);
        ((GeneralPath) shape).lineTo(479.218, 136.276);
        ((GeneralPath) shape).lineTo(479.2, 136.347);
        ((GeneralPath) shape).lineTo(479.187, 136.408);
        ((GeneralPath) shape).lineTo(479.176, 136.462);
        ((GeneralPath) shape).lineTo(479.168, 136.507);
        ((GeneralPath) shape).lineTo(479.162, 136.542);
        ((GeneralPath) shape).lineTo(479.162, 136.545);
        ((GeneralPath) shape).lineTo(478.791, 146.221);
        ((GeneralPath) shape).lineTo(478.392, 156.671);
        ((GeneralPath) shape).lineTo(477.91, 169.296);
        ((GeneralPath) shape).lineTo(477.391, 182.935);
        ((GeneralPath) shape).lineTo(477.132, 189.774);
        ((GeneralPath) shape).lineTo(476.881, 196.432);
        ((GeneralPath) shape).lineTo(476.642, 202.766);
        ((GeneralPath) shape).lineTo(476.422, 208.629);
        ((GeneralPath) shape).lineTo(476.227, 213.877);
        ((GeneralPath) shape).lineTo(476.0, 220.049);
        ((GeneralPath) shape).lineTo(473.997, 220.053);
        ((GeneralPath) shape).lineTo(473.683, 220.055);
        ((GeneralPath) shape).lineTo(473.369, 220.058);
        ((GeneralPath) shape).lineTo(473.058, 220.062);
        ((GeneralPath) shape).lineTo(472.747, 220.067);
        ((GeneralPath) shape).lineTo(472.438, 220.074);
        ((GeneralPath) shape).lineTo(472.13, 220.082);
        ((GeneralPath) shape).lineTo(471.824, 220.091);
        ((GeneralPath) shape).lineTo(471.519, 220.101);
        ((GeneralPath) shape).lineTo(471.215, 220.112);
        ((GeneralPath) shape).lineTo(470.912, 220.125);
        ((GeneralPath) shape).lineTo(470.611, 220.138);
        ((GeneralPath) shape).lineTo(470.311, 220.153);
        ((GeneralPath) shape).lineTo(470.013, 220.169);
        ((GeneralPath) shape).lineTo(469.716, 220.186);
        ((GeneralPath) shape).lineTo(469.42, 220.204);
        ((GeneralPath) shape).lineTo(469.125, 220.224);
        ((GeneralPath) shape).lineTo(468.832, 220.244);
        ((GeneralPath) shape).lineTo(468.54, 220.266);
        ((GeneralPath) shape).lineTo(468.249, 220.288);
        ((GeneralPath) shape).lineTo(467.96, 220.312);
        ((GeneralPath) shape).lineTo(467.671, 220.337);
        ((GeneralPath) shape).lineTo(467.385, 220.363);
        ((GeneralPath) shape).lineTo(467.099, 220.39);
        ((GeneralPath) shape).lineTo(466.815, 220.418);
        ((GeneralPath) shape).lineTo(466.532, 220.448);
        ((GeneralPath) shape).lineTo(466.25, 220.478);
        ((GeneralPath) shape).lineTo(465.97, 220.509);
        ((GeneralPath) shape).lineTo(465.69, 220.542);
        ((GeneralPath) shape).lineTo(465.412, 220.575);
        ((GeneralPath) shape).lineTo(465.136, 220.61);
        ((GeneralPath) shape).lineTo(464.86, 220.646);
        ((GeneralPath) shape).lineTo(464.586, 220.682);
        ((GeneralPath) shape).lineTo(464.313, 220.72);
        ((GeneralPath) shape).lineTo(464.042, 220.759);
        ((GeneralPath) shape).lineTo(463.771, 220.799);
        ((GeneralPath) shape).lineTo(463.502, 220.84);
        ((GeneralPath) shape).lineTo(463.234, 220.882);
        ((GeneralPath) shape).lineTo(462.968, 220.924);
        ((GeneralPath) shape).lineTo(462.702, 220.968);
        ((GeneralPath) shape).lineTo(462.438, 221.013);
        ((GeneralPath) shape).lineTo(462.175, 221.059);
        ((GeneralPath) shape).lineTo(461.913, 221.106);
        ((GeneralPath) shape).lineTo(461.653, 221.154);
        ((GeneralPath) shape).lineTo(461.393, 221.203);
        ((GeneralPath) shape).lineTo(461.135, 221.253);
        ((GeneralPath) shape).lineTo(460.878, 221.304);
        ((GeneralPath) shape).lineTo(460.623, 221.355);
        ((GeneralPath) shape).lineTo(460.368, 221.408);
        ((GeneralPath) shape).lineTo(460.115, 221.462);
        ((GeneralPath) shape).lineTo(459.863, 221.517);
        ((GeneralPath) shape).lineTo(459.612, 221.572);
        ((GeneralPath) shape).lineTo(459.363, 221.629);
        ((GeneralPath) shape).lineTo(459.114, 221.687);
        ((GeneralPath) shape).lineTo(458.867, 221.745);
        ((GeneralPath) shape).lineTo(458.621, 221.805);
        ((GeneralPath) shape).lineTo(458.376, 221.865);
        ((GeneralPath) shape).lineTo(458.132, 221.926);
        ((GeneralPath) shape).lineTo(457.89, 221.988);
        ((GeneralPath) shape).lineTo(457.648, 222.051);
        ((GeneralPath) shape).lineTo(457.408, 222.115);
        ((GeneralPath) shape).lineTo(457.169, 222.18);
        ((GeneralPath) shape).lineTo(456.931, 222.246);
        ((GeneralPath) shape).lineTo(456.694, 222.313);
        ((GeneralPath) shape).lineTo(456.459, 222.38);
        ((GeneralPath) shape).lineTo(456.224, 222.449);
        ((GeneralPath) shape).lineTo(455.991, 222.518);
        ((GeneralPath) shape).lineTo(455.759, 222.588);
        ((GeneralPath) shape).lineTo(455.528, 222.66);
        ((GeneralPath) shape).lineTo(455.298, 222.731);
        ((GeneralPath) shape).lineTo(455.069, 222.804);
        ((GeneralPath) shape).lineTo(454.842, 222.878);
        ((GeneralPath) shape).lineTo(454.615, 222.952);
        ((GeneralPath) shape).lineTo(454.39, 223.028);
        ((GeneralPath) shape).lineTo(454.166, 223.104);
        ((GeneralPath) shape).lineTo(453.943, 223.181);
        ((GeneralPath) shape).lineTo(453.721, 223.259);
        ((GeneralPath) shape).lineTo(453.5, 223.337);
        ((GeneralPath) shape).lineTo(453.28, 223.417);
        ((GeneralPath) shape).lineTo(453.062, 223.497);
        ((GeneralPath) shape).lineTo(452.844, 223.578);
        ((GeneralPath) shape).lineTo(452.628, 223.66);
        ((GeneralPath) shape).lineTo(452.412, 223.743);
        ((GeneralPath) shape).lineTo(452.198, 223.826);
        ((GeneralPath) shape).lineTo(451.985, 223.911);
        ((GeneralPath) shape).lineTo(451.773, 223.996);
        ((GeneralPath) shape).lineTo(451.562, 224.082);
        ((GeneralPath) shape).lineTo(451.352, 224.168);
        ((GeneralPath) shape).lineTo(451.143, 224.256);
        ((GeneralPath) shape).lineTo(450.935, 224.344);
        ((GeneralPath) shape).lineTo(450.729, 224.433);
        ((GeneralPath) shape).lineTo(450.523, 224.522);
        ((GeneralPath) shape).lineTo(450.319, 224.613);
        ((GeneralPath) shape).lineTo(450.115, 224.704);
        ((GeneralPath) shape).lineTo(449.913, 224.796);
        ((GeneralPath) shape).lineTo(449.711, 224.888);
        ((GeneralPath) shape).lineTo(449.511, 224.982);
        ((GeneralPath) shape).lineTo(449.312, 225.076);
        ((GeneralPath) shape).lineTo(449.114, 225.171);
        ((GeneralPath) shape).lineTo(448.916, 225.266);
        ((GeneralPath) shape).lineTo(448.72, 225.363);
        ((GeneralPath) shape).lineTo(448.525, 225.46);
        ((GeneralPath) shape).lineTo(448.331, 225.557);
        ((GeneralPath) shape).lineTo(448.138, 225.656);
        ((GeneralPath) shape).lineTo(447.946, 225.755);
        ((GeneralPath) shape).lineTo(447.755, 225.854);
        ((GeneralPath) shape).lineTo(447.565, 225.955);
        ((GeneralPath) shape).lineTo(447.376, 226.056);
        ((GeneralPath) shape).lineTo(447.188, 226.158);
        ((GeneralPath) shape).lineTo(447.001, 226.26);
        ((GeneralPath) shape).lineTo(446.815, 226.363);
        ((GeneralPath) shape).lineTo(446.63, 226.467);
        ((GeneralPath) shape).lineTo(446.446, 226.572);
        ((GeneralPath) shape).lineTo(446.263, 226.677);
        ((GeneralPath) shape).lineTo(446.081, 226.783);
        ((GeneralPath) shape).lineTo(445.9, 226.889);
        ((GeneralPath) shape).lineTo(445.72, 226.996);
        ((GeneralPath) shape).lineTo(445.541, 227.104);
        ((GeneralPath) shape).lineTo(445.363, 227.212);
        ((GeneralPath) shape).lineTo(445.186, 227.321);
        ((GeneralPath) shape).lineTo(445.01, 227.431);
        ((GeneralPath) shape).lineTo(444.835, 227.541);
        ((GeneralPath) shape).lineTo(444.661, 227.652);
        ((GeneralPath) shape).lineTo(444.487, 227.763);
        ((GeneralPath) shape).lineTo(444.315, 227.875);
        ((GeneralPath) shape).lineTo(444.144, 227.988);
        ((GeneralPath) shape).lineTo(443.974, 228.101);
        ((GeneralPath) shape).lineTo(443.804, 228.215);
        ((GeneralPath) shape).lineTo(443.636, 228.329);
        ((GeneralPath) shape).lineTo(443.468, 228.444);
        ((GeneralPath) shape).lineTo(443.302, 228.56);
        ((GeneralPath) shape).lineTo(443.136, 228.676);
        ((GeneralPath) shape).lineTo(442.971, 228.793);
        ((GeneralPath) shape).lineTo(442.808, 228.91);
        ((GeneralPath) shape).lineTo(442.645, 229.028);
        ((GeneralPath) shape).lineTo(442.483, 229.146);
        ((GeneralPath) shape).lineTo(442.322, 229.265);
        ((GeneralPath) shape).lineTo(442.162, 229.385);
        ((GeneralPath) shape).lineTo(442.003, 229.505);
        ((GeneralPath) shape).lineTo(441.844, 229.625);
        ((GeneralPath) shape).lineTo(441.687, 229.746);
        ((GeneralPath) shape).lineTo(441.531, 229.868);
        ((GeneralPath) shape).lineTo(441.375, 229.99);
        ((GeneralPath) shape).lineTo(441.22, 230.113);
        ((GeneralPath) shape).lineTo(441.067, 230.236);
        ((GeneralPath) shape).lineTo(440.914, 230.36);
        ((GeneralPath) shape).lineTo(440.762, 230.484);
        ((GeneralPath) shape).lineTo(440.611, 230.609);
        ((GeneralPath) shape).lineTo(440.461, 230.734);
        ((GeneralPath) shape).lineTo(440.311, 230.86);
        ((GeneralPath) shape).lineTo(440.163, 230.986);
        ((GeneralPath) shape).lineTo(440.015, 231.113);
        ((GeneralPath) shape).lineTo(439.869, 231.24);
        ((GeneralPath) shape).lineTo(439.723, 231.368);
        ((GeneralPath) shape).lineTo(439.578, 231.496);
        ((GeneralPath) shape).lineTo(439.434, 231.625);
        ((GeneralPath) shape).lineTo(439.291, 231.754);
        ((GeneralPath) shape).lineTo(439.148, 231.883);
        ((GeneralPath) shape).lineTo(439.007, 232.013);
        ((GeneralPath) shape).lineTo(438.866, 232.144);
        ((GeneralPath) shape).lineTo(438.726, 232.275);
        ((GeneralPath) shape).lineTo(438.587, 232.406);
        ((GeneralPath) shape).lineTo(438.449, 232.538);
        ((GeneralPath) shape).lineTo(438.312, 232.67);
        ((GeneralPath) shape).lineTo(438.176, 232.803);
        ((GeneralPath) shape).lineTo(438.04, 232.936);
        ((GeneralPath) shape).lineTo(437.905, 233.069);
        ((GeneralPath) shape).lineTo(437.771, 233.204);
        ((GeneralPath) shape).lineTo(437.638, 233.338);
        ((GeneralPath) shape).lineTo(437.506, 233.473);
        ((GeneralPath) shape).lineTo(437.374, 233.608);
        ((GeneralPath) shape).lineTo(437.244, 233.743);
        ((GeneralPath) shape).lineTo(437.114, 233.879);
        ((GeneralPath) shape).lineTo(436.985, 234.016);
        ((GeneralPath) shape).lineTo(436.857, 234.153);
        ((GeneralPath) shape).lineTo(436.729, 234.29);
        ((GeneralPath) shape).lineTo(436.603, 234.427);
        ((GeneralPath) shape).lineTo(436.477, 234.565);
        ((GeneralPath) shape).lineTo(436.352, 234.703);
        ((GeneralPath) shape).lineTo(436.228, 234.842);
        ((GeneralPath) shape).lineTo(436.104, 234.981);
        ((GeneralPath) shape).lineTo(435.982, 235.12);
        ((GeneralPath) shape).lineTo(435.86, 235.26);
        ((GeneralPath) shape).lineTo(435.739, 235.4);
        ((GeneralPath) shape).lineTo(435.619, 235.541);
        ((GeneralPath) shape).lineTo(435.499, 235.681);
        ((GeneralPath) shape).lineTo(435.38, 235.823);
        ((GeneralPath) shape).lineTo(435.262, 235.964);
        ((GeneralPath) shape).lineTo(435.145, 236.106);
        ((GeneralPath) shape).lineTo(435.029, 236.248);
        ((GeneralPath) shape).lineTo(434.913, 236.39);
        ((GeneralPath) shape).lineTo(434.798, 236.533);
        ((GeneralPath) shape).lineTo(434.684, 236.676);
        ((GeneralPath) shape).lineTo(434.571, 236.819);
        ((GeneralPath) shape).lineTo(434.458, 236.963);
        ((GeneralPath) shape).lineTo(434.347, 237.107);
        ((GeneralPath) shape).lineTo(434.236, 237.251);
        ((GeneralPath) shape).lineTo(434.125, 237.396);
        ((GeneralPath) shape).lineTo(434.016, 237.54);
        ((GeneralPath) shape).lineTo(433.907, 237.685);
        ((GeneralPath) shape).lineTo(433.799, 237.831);
        ((GeneralPath) shape).lineTo(433.691, 237.976);
        ((GeneralPath) shape).lineTo(433.585, 238.122);
        ((GeneralPath) shape).lineTo(433.479, 238.268);
        ((GeneralPath) shape).lineTo(433.374, 238.415);
        ((GeneralPath) shape).lineTo(433.269, 238.561);
        ((GeneralPath) shape).lineTo(433.166, 238.708);
        ((GeneralPath) shape).lineTo(433.063, 238.855);
        ((GeneralPath) shape).lineTo(432.96, 239.003);
        ((GeneralPath) shape).lineTo(432.86, 239.149);
        ((GeneralPath) shape).lineTo(432.658, 239.446);
        ((GeneralPath) shape).lineTo(432.46, 239.743);
        ((GeneralPath) shape).lineTo(432.265, 240.041);
        ((GeneralPath) shape).lineTo(432.072, 240.339);
        ((GeneralPath) shape).lineTo(431.883, 240.639);
        ((GeneralPath) shape).lineTo(431.696, 240.939);
        ((GeneralPath) shape).lineTo(431.512, 241.239);
        ((GeneralPath) shape).lineTo(431.33, 241.541);
        ((GeneralPath) shape).lineTo(431.151, 241.843);
        ((GeneralPath) shape).lineTo(430.975, 242.145);
        ((GeneralPath) shape).lineTo(430.802, 242.448);
        ((GeneralPath) shape).lineTo(430.631, 242.752);
        ((GeneralPath) shape).lineTo(430.463, 243.056);
        ((GeneralPath) shape).lineTo(430.298, 243.361);
        ((GeneralPath) shape).lineTo(430.135, 243.666);
        ((GeneralPath) shape).lineTo(429.974, 243.971);
        ((GeneralPath) shape).lineTo(429.817, 244.277);
        ((GeneralPath) shape).lineTo(429.661, 244.583);
        ((GeneralPath) shape).lineTo(429.509, 244.889);
        ((GeneralPath) shape).lineTo(429.358, 245.195);
        ((GeneralPath) shape).lineTo(429.211, 245.502);
        ((GeneralPath) shape).lineTo(429.065, 245.809);
        ((GeneralPath) shape).lineTo(428.922, 246.116);
        ((GeneralPath) shape).lineTo(428.782, 246.422);
        ((GeneralPath) shape).lineTo(428.644, 246.729);
        ((GeneralPath) shape).lineTo(428.508, 247.036);
        ((GeneralPath) shape).lineTo(428.375, 247.343);
        ((GeneralPath) shape).lineTo(428.244, 247.65);
        ((GeneralPath) shape).lineTo(428.115, 247.957);
        ((GeneralPath) shape).lineTo(427.988, 248.264);
        ((GeneralPath) shape).lineTo(427.864, 248.57);
        ((GeneralPath) shape).lineTo(427.742, 248.876);
        ((GeneralPath) shape).lineTo(427.623, 249.182);
        ((GeneralPath) shape).lineTo(427.505, 249.487);
        ((GeneralPath) shape).lineTo(427.39, 249.792);
        ((GeneralPath) shape).lineTo(427.277, 250.097);
        ((GeneralPath) shape).lineTo(427.166, 250.402);
        ((GeneralPath) shape).lineTo(427.057, 250.705);
        ((GeneralPath) shape).lineTo(426.95, 251.009);
        ((GeneralPath) shape).lineTo(426.846, 251.312);
        ((GeneralPath) shape).lineTo(426.743, 251.614);
        ((GeneralPath) shape).lineTo(426.643, 251.916);
        ((GeneralPath) shape).lineTo(426.544, 252.217);
        ((GeneralPath) shape).lineTo(426.448, 252.517);
        ((GeneralPath) shape).lineTo(426.353, 252.816);
        ((GeneralPath) shape).lineTo(426.261, 253.115);
        ((GeneralPath) shape).lineTo(426.17, 253.413);
        ((GeneralPath) shape).lineTo(426.082, 253.71);
        ((GeneralPath) shape).lineTo(425.995, 254.006);
        ((GeneralPath) shape).lineTo(425.91, 254.301);
        ((GeneralPath) shape).lineTo(425.828, 254.596);
        ((GeneralPath) shape).lineTo(425.747, 254.889);
        ((GeneralPath) shape).lineTo(425.667, 255.181);
        ((GeneralPath) shape).lineTo(425.59, 255.472);
        ((GeneralPath) shape).lineTo(425.514, 255.762);
        ((GeneralPath) shape).lineTo(425.441, 256.051);
        ((GeneralPath) shape).lineTo(425.369, 256.339);
        ((GeneralPath) shape).lineTo(425.298, 256.625);
        ((GeneralPath) shape).lineTo(425.23, 256.91);
        ((GeneralPath) shape).lineTo(425.163, 257.194);
        ((GeneralPath) shape).lineTo(425.098, 257.476);
        ((GeneralPath) shape).lineTo(425.034, 257.757);
        ((GeneralPath) shape).lineTo(424.972, 258.037);
        ((GeneralPath) shape).lineTo(424.912, 258.315);
        ((GeneralPath) shape).lineTo(424.853, 258.591);
        ((GeneralPath) shape).lineTo(424.796, 258.867);
        ((GeneralPath) shape).lineTo(424.74, 259.14);
        ((GeneralPath) shape).lineTo(424.686, 259.412);
        ((GeneralPath) shape).lineTo(424.634, 259.682);
        ((GeneralPath) shape).lineTo(424.583, 259.95);
        ((GeneralPath) shape).lineTo(424.533, 260.217);
        ((GeneralPath) shape).lineTo(424.485, 260.482);
        ((GeneralPath) shape).lineTo(424.438, 260.745);
        ((GeneralPath) shape).lineTo(424.393, 261.006);
        ((GeneralPath) shape).lineTo(424.349, 261.265);
        ((GeneralPath) shape).lineTo(424.306, 261.522);
        ((GeneralPath) shape).lineTo(424.265, 261.777);
        ((GeneralPath) shape).lineTo(424.225, 262.03);
        ((GeneralPath) shape).lineTo(424.187, 262.282);
        ((GeneralPath) shape).lineTo(424.149, 262.531);
        ((GeneralPath) shape).lineTo(424.113, 262.777);
        ((GeneralPath) shape).lineTo(424.079, 263.022);
        ((GeneralPath) shape).lineTo(424.045, 263.265);
        ((GeneralPath) shape).lineTo(424.013, 263.505);
        ((GeneralPath) shape).lineTo(423.981, 263.743);
        ((GeneralPath) shape).lineTo(423.951, 263.978);
        ((GeneralPath) shape).lineTo(423.922, 264.211);
        ((GeneralPath) shape).lineTo(423.895, 264.442);
        ((GeneralPath) shape).lineTo(423.868, 264.67);
        ((GeneralPath) shape).lineTo(423.842, 264.896);
        ((GeneralPath) shape).lineTo(423.818, 265.119);
        ((GeneralPath) shape).lineTo(423.794, 265.34);
        ((GeneralPath) shape).lineTo(423.772, 265.557);
        ((GeneralPath) shape).lineTo(423.73, 265.986);
        ((GeneralPath) shape).lineTo(423.691, 266.403);
        ((GeneralPath) shape).lineTo(423.657, 266.809);
        ((GeneralPath) shape).lineTo(423.625, 267.203);
        ((GeneralPath) shape).lineTo(423.598, 267.586);
        ((GeneralPath) shape).lineTo(423.573, 267.956);
        ((GeneralPath) shape).lineTo(423.551, 268.313);
        ((GeneralPath) shape).lineTo(423.532, 268.658);
        ((GeneralPath) shape).lineTo(423.515, 268.99);
        ((GeneralPath) shape).lineTo(423.501, 269.308);
        ((GeneralPath) shape).lineTo(423.49, 269.612);
        ((GeneralPath) shape).lineTo(423.48, 269.903);
        ((GeneralPath) shape).lineTo(423.472, 270.178);
        ((GeneralPath) shape).lineTo(423.466, 270.44);
        ((GeneralPath) shape).lineTo(423.462, 270.686);
        ((GeneralPath) shape).lineTo(423.459, 270.916);
        ((GeneralPath) shape).lineTo(423.458, 271.132);
        ((GeneralPath) shape).lineTo(423.457, 271.331);
        ((GeneralPath) shape).lineTo(423.457, 271.514);
        ((GeneralPath) shape).lineTo(423.459, 271.68);
        ((GeneralPath) shape).lineTo(423.46, 271.828);
        ((GeneralPath) shape).lineTo(423.465, 272.077);
        ((GeneralPath) shape).lineTo(423.469, 272.252);
        ((GeneralPath) shape).lineTo(423.473, 272.355);
        ((GeneralPath) shape).lineTo(423.552, 274.211);
        ((GeneralPath) shape).lineTo(348.751, 274.211);
        ((GeneralPath) shape).lineTo(348.732, 274.214);
        ((GeneralPath) shape).lineTo(348.55, 274.244);
        ((GeneralPath) shape).lineTo(348.302, 274.284);
        ((GeneralPath) shape).lineTo(347.992, 274.336);
        ((GeneralPath) shape).lineTo(347.627, 274.399);
        ((GeneralPath) shape).lineTo(347.211, 274.473);
        ((GeneralPath) shape).lineTo(346.749, 274.558);
        ((GeneralPath) shape).lineTo(346.504, 274.604);
        ((GeneralPath) shape).lineTo(346.249, 274.652);
        ((GeneralPath) shape).lineTo(345.984, 274.703);
        ((GeneralPath) shape).lineTo(345.712, 274.757);
        ((GeneralPath) shape).lineTo(345.432, 274.813);
        ((GeneralPath) shape).lineTo(345.146, 274.871);
        ((GeneralPath) shape).lineTo(344.853, 274.932);
        ((GeneralPath) shape).lineTo(344.555, 274.995);
        ((GeneralPath) shape).lineTo(344.252, 275.06);
        ((GeneralPath) shape).lineTo(343.945, 275.128);
        ((GeneralPath) shape).lineTo(343.635, 275.197);
        ((GeneralPath) shape).lineTo(343.322, 275.269);
        ((GeneralPath) shape).lineTo(343.006, 275.343);
        ((GeneralPath) shape).lineTo(342.69, 275.419);
        ((GeneralPath) shape).lineTo(342.372, 275.497);
        ((GeneralPath) shape).lineTo(342.054, 275.577);
        ((GeneralPath) shape).lineTo(341.737, 275.659);
        ((GeneralPath) shape).lineTo(341.421, 275.743);
        ((GeneralPath) shape).lineTo(341.108, 275.828);
        ((GeneralPath) shape).lineTo(340.796, 275.916);
        ((GeneralPath) shape).lineTo(340.489, 276.005);
        ((GeneralPath) shape).lineTo(340.183, 276.096);
        ((GeneralPath) shape).lineTo(340.035, 276.141);
        ((GeneralPath) shape).lineTo(339.886, 276.187);
        ((GeneralPath) shape).lineTo(339.739, 276.234);
        ((GeneralPath) shape).lineTo(339.593, 276.28);
        ((GeneralPath) shape).lineTo(339.449, 276.328);
        ((GeneralPath) shape).lineTo(339.307, 276.375);
        ((GeneralPath) shape).lineTo(339.166, 276.423);
        ((GeneralPath) shape).lineTo(339.027, 276.471);
        ((GeneralPath) shape).lineTo(338.89, 276.519);
        ((GeneralPath) shape).lineTo(338.756, 276.568);
        ((GeneralPath) shape).lineTo(338.623, 276.616);
        ((GeneralPath) shape).lineTo(338.493, 276.665);
        ((GeneralPath) shape).lineTo(338.365, 276.714);
        ((GeneralPath) shape).lineTo(338.24, 276.763);
        ((GeneralPath) shape).lineTo(338.117, 276.813);
        ((GeneralPath) shape).lineTo(337.998, 276.862);
        ((GeneralPath) shape).lineTo(337.881, 276.911);
        ((GeneralPath) shape).lineTo(337.767, 276.961);
        ((GeneralPath) shape).lineTo(337.656, 277.01);
        ((GeneralPath) shape).lineTo(337.548, 277.059);
        ((GeneralPath) shape).lineTo(337.444, 277.108);
        ((GeneralPath) shape).lineTo(337.343, 277.156);
        ((GeneralPath) shape).lineTo(337.246, 277.205);
        ((GeneralPath) shape).lineTo(337.153, 277.253);
        ((GeneralPath) shape).lineTo(337.063, 277.3);
        ((GeneralPath) shape).lineTo(336.978, 277.347);
        ((GeneralPath) shape).lineTo(336.897, 277.393);
        ((GeneralPath) shape).lineTo(336.82, 277.438);
        ((GeneralPath) shape).lineTo(336.766, 277.472);
        ((GeneralPath) shape).lineTo(336.826, 277.506);
        ((GeneralPath) shape).lineTo(336.901, 277.547);
        ((GeneralPath) shape).lineTo(336.981, 277.59);
        ((GeneralPath) shape).lineTo(337.065, 277.633);
        ((GeneralPath) shape).lineTo(337.153, 277.676);
        ((GeneralPath) shape).lineTo(337.244, 277.72);
        ((GeneralPath) shape).lineTo(337.339, 277.764);
        ((GeneralPath) shape).lineTo(337.438, 277.808);
        ((GeneralPath) shape).lineTo(337.54, 277.852);
        ((GeneralPath) shape).lineTo(337.645, 277.897);
        ((GeneralPath) shape).lineTo(337.753, 277.941);
        ((GeneralPath) shape).lineTo(337.864, 277.985);
        ((GeneralPath) shape).lineTo(337.978, 278.03);
        ((GeneralPath) shape).lineTo(338.094, 278.074);
        ((GeneralPath) shape).lineTo(338.214, 278.118);
        ((GeneralPath) shape).lineTo(338.335, 278.161);
        ((GeneralPath) shape).lineTo(338.46, 278.205);
        ((GeneralPath) shape).lineTo(338.586, 278.248);
        ((GeneralPath) shape).lineTo(338.714, 278.291);
        ((GeneralPath) shape).lineTo(338.845, 278.334);
        ((GeneralPath) shape).lineTo(338.977, 278.377);
        ((GeneralPath) shape).lineTo(339.112, 278.419);
        ((GeneralPath) shape).lineTo(339.248, 278.461);
        ((GeneralPath) shape).lineTo(339.386, 278.502);
        ((GeneralPath) shape).lineTo(339.525, 278.543);
        ((GeneralPath) shape).lineTo(339.666, 278.584);
        ((GeneralPath) shape).lineTo(339.808, 278.624);
        ((GeneralPath) shape).lineTo(339.95, 278.663);
        ((GeneralPath) shape).lineTo(340.243, 278.743);
        ((GeneralPath) shape).lineTo(340.537, 278.82);
        ((GeneralPath) shape).lineTo(340.836, 278.895);
        ((GeneralPath) shape).lineTo(341.136, 278.968);
        ((GeneralPath) shape).lineTo(341.439, 279.04);
        ((GeneralPath) shape).lineTo(341.743, 279.109);
        ((GeneralPath) shape).lineTo(342.048, 279.177);
        ((GeneralPath) shape).lineTo(342.353, 279.243);
        ((GeneralPath) shape).lineTo(342.657, 279.307);
        ((GeneralPath) shape).lineTo(342.959, 279.369);
        ((GeneralPath) shape).lineTo(343.26, 279.429);
        ((GeneralPath) shape).lineTo(343.558, 279.486);
        ((GeneralPath) shape).lineTo(343.853, 279.542);
        ((GeneralPath) shape).lineTo(344.143, 279.596);
        ((GeneralPath) shape).lineTo(344.43, 279.648);
        ((GeneralPath) shape).lineTo(344.711, 279.697);
        ((GeneralPath) shape).lineTo(344.986, 279.745);
        ((GeneralPath) shape).lineTo(345.254, 279.79);
        ((GeneralPath) shape).lineTo(345.516, 279.833);
        ((GeneralPath) shape).lineTo(345.769, 279.874);
        ((GeneralPath) shape).lineTo(346.015, 279.913);
        ((GeneralPath) shape).lineTo(346.25, 279.949);
        ((GeneralPath) shape).lineTo(346.694, 280.016);
        ((GeneralPath) shape).lineTo(347.093, 280.074);
        ((GeneralPath) shape).lineTo(347.444, 280.124);
        ((GeneralPath) shape).lineTo(347.741, 280.164);
        ((GeneralPath) shape).lineTo(347.979, 280.195);
        ((GeneralPath) shape).lineTo(348.154, 280.217);
        ((GeneralPath) shape).lineTo(348.217, 280.225);
        ((GeneralPath) shape).lineTo(423.711, 282.114);
        ((GeneralPath) shape).lineTo(423.749, 284.36);
        ((GeneralPath) shape).lineTo(423.76, 284.919);
        ((GeneralPath) shape).lineTo(423.773, 285.472);
        ((GeneralPath) shape).lineTo(423.786, 286.019);
        ((GeneralPath) shape).lineTo(423.801, 286.56);
        ((GeneralPath) shape).lineTo(423.818, 287.095);
        ((GeneralPath) shape).lineTo(423.835, 287.624);
        ((GeneralPath) shape).lineTo(423.854, 288.148);
        ((GeneralPath) shape).lineTo(423.874, 288.666);
        ((GeneralPath) shape).lineTo(423.896, 289.179);
        ((GeneralPath) shape).lineTo(423.919, 289.688);
        ((GeneralPath) shape).lineTo(423.943, 290.192);
        ((GeneralPath) shape).lineTo(423.968, 290.691);
        ((GeneralPath) shape).lineTo(423.994, 291.186);
        ((GeneralPath) shape).lineTo(424.022, 291.677);
        ((GeneralPath) shape).lineTo(424.051, 292.163);
        ((GeneralPath) shape).lineTo(424.081, 292.647);
        ((GeneralPath) shape).lineTo(424.112, 293.126);
        ((GeneralPath) shape).lineTo(424.145, 293.602);
        ((GeneralPath) shape).lineTo(424.178, 294.075);
        ((GeneralPath) shape).lineTo(424.213, 294.544);
        ((GeneralPath) shape).lineTo(424.249, 295.011);
        ((GeneralPath) shape).lineTo(424.286, 295.475);
        ((GeneralPath) shape).lineTo(424.324, 295.937);
        ((GeneralPath) shape).lineTo(424.363, 296.396);
        ((GeneralPath) shape).lineTo(424.403, 296.853);
        ((GeneralPath) shape).lineTo(424.444, 297.309);
        ((GeneralPath) shape).lineTo(424.487, 297.762);
        ((GeneralPath) shape).lineTo(424.53, 298.214);
        ((GeneralPath) shape).lineTo(424.574, 298.665);
        ((GeneralPath) shape).lineTo(424.62, 299.115);
        ((GeneralPath) shape).lineTo(424.666, 299.563);
        ((GeneralPath) shape).lineTo(424.714, 300.011);
        ((GeneralPath) shape).lineTo(424.762, 300.458);
        ((GeneralPath) shape).lineTo(424.812, 300.904);
        ((GeneralPath) shape).lineTo(424.862, 301.351);
        ((GeneralPath) shape).lineTo(424.914, 301.797);
        ((GeneralPath) shape).lineTo(424.966, 302.243);
        ((GeneralPath) shape).lineTo(425.019, 302.69);
        ((GeneralPath) shape).lineTo(425.074, 303.137);
        ((GeneralPath) shape).lineTo(425.129, 303.585);
        ((GeneralPath) shape).lineTo(425.185, 304.033);
        ((GeneralPath) shape).lineTo(425.3, 304.934);
        ((GeneralPath) shape).lineTo(425.418, 305.84);
        ((GeneralPath) shape).lineTo(425.54, 306.754);
        ((GeneralPath) shape).lineTo(425.665, 307.675);
        ((GeneralPath) shape).lineTo(425.793, 308.607);
        ((GeneralPath) shape).lineTo(425.925, 309.55);
        ((GeneralPath) shape).lineTo(426.059, 310.505);
        ((GeneralPath) shape).lineTo(426.337, 312.462);
        ((GeneralPath) shape).lineTo(426.945, 316.721);
        ((GeneralPath) shape).lineTo(70.764, 277.107);
        ((GeneralPath) shape).lineTo(70.047, 277.028);
        ((GeneralPath) shape).lineTo(69.341, 276.951);
        ((GeneralPath) shape).lineTo(68.648, 276.877);
        ((GeneralPath) shape).lineTo(67.966, 276.805);
        ((GeneralPath) shape).lineTo(67.295, 276.735);
        ((GeneralPath) shape).lineTo(66.635, 276.668);
        ((GeneralPath) shape).lineTo(65.987, 276.603);
        ((GeneralPath) shape).lineTo(65.349, 276.54);
        ((GeneralPath) shape).lineTo(64.723, 276.48);
        ((GeneralPath) shape).lineTo(64.107, 276.422);
        ((GeneralPath) shape).lineTo(63.502, 276.366);
        ((GeneralPath) shape).lineTo(62.908, 276.312);
        ((GeneralPath) shape).lineTo(62.324, 276.261);
        ((GeneralPath) shape).lineTo(61.751, 276.212);
        ((GeneralPath) shape).lineTo(61.188, 276.165);
        ((GeneralPath) shape).lineTo(60.635, 276.12);
        ((GeneralPath) shape).lineTo(60.092, 276.077);
        ((GeneralPath) shape).lineTo(59.559, 276.036);
        ((GeneralPath) shape).lineTo(59.037, 275.997);
        ((GeneralPath) shape).lineTo(58.524, 275.961);
        ((GeneralPath) shape).lineTo(58.02, 275.926);
        ((GeneralPath) shape).lineTo(57.527, 275.893);
        ((GeneralPath) shape).lineTo(57.042, 275.863);
        ((GeneralPath) shape).lineTo(56.568, 275.834);
        ((GeneralPath) shape).lineTo(56.102, 275.807);
        ((GeneralPath) shape).lineTo(55.646, 275.782);
        ((GeneralPath) shape).lineTo(55.199, 275.759);
        ((GeneralPath) shape).lineTo(54.76, 275.738);
        ((GeneralPath) shape).lineTo(54.331, 275.719);
        ((GeneralPath) shape).lineTo(53.911, 275.702);
        ((GeneralPath) shape).lineTo(53.499, 275.686);
        ((GeneralPath) shape).lineTo(53.096, 275.672);
        ((GeneralPath) shape).lineTo(52.701, 275.66);
        ((GeneralPath) shape).lineTo(52.314, 275.65);
        ((GeneralPath) shape).lineTo(51.936, 275.641);
        ((GeneralPath) shape).lineTo(51.567, 275.634);
        ((GeneralPath) shape).lineTo(51.205, 275.629);
        ((GeneralPath) shape).lineTo(50.851, 275.625);
        ((GeneralPath) shape).lineTo(50.505, 275.623);
        ((GeneralPath) shape).lineTo(50.167, 275.623);
        ((GeneralPath) shape).lineTo(49.837, 275.624);
        ((GeneralPath) shape).lineTo(49.514, 275.627);
        ((GeneralPath) shape).lineTo(49.199, 275.631);
        ((GeneralPath) shape).lineTo(48.891, 275.637);
        ((GeneralPath) shape).lineTo(48.591, 275.644);
        ((GeneralPath) shape).lineTo(48.298, 275.652);
        ((GeneralPath) shape).lineTo(48.012, 275.662);
        ((GeneralPath) shape).lineTo(47.732, 275.674);
        ((GeneralPath) shape).lineTo(47.46, 275.687);
        ((GeneralPath) shape).lineTo(47.195, 275.701);
        ((GeneralPath) shape).lineTo(46.936, 275.716);
        ((GeneralPath) shape).lineTo(46.684, 275.733);
        ((GeneralPath) shape).lineTo(46.439, 275.751);
        ((GeneralPath) shape).lineTo(46.2, 275.77);
        ((GeneralPath) shape).lineTo(45.968, 275.79);
        ((GeneralPath) shape).lineTo(45.741, 275.812);
        ((GeneralPath) shape).lineTo(45.521, 275.834);
        ((GeneralPath) shape).lineTo(45.307, 275.858);
        ((GeneralPath) shape).lineTo(45.099, 275.883);
        ((GeneralPath) shape).lineTo(44.896, 275.909);
        ((GeneralPath) shape).lineTo(44.7, 275.936);
        ((GeneralPath) shape).lineTo(44.509, 275.964);
        ((GeneralPath) shape).lineTo(44.323, 275.992);
        ((GeneralPath) shape).lineTo(44.144, 276.022);
        ((GeneralPath) shape).lineTo(43.969, 276.052);
        ((GeneralPath) shape).lineTo(43.8, 276.084);
        ((GeneralPath) shape).lineTo(43.635, 276.116);
        ((GeneralPath) shape).lineTo(43.476, 276.149);
        ((GeneralPath) shape).lineTo(43.322, 276.182);
        ((GeneralPath) shape).lineTo(43.172, 276.217);
        ((GeneralPath) shape).lineTo(43.028, 276.252);
        ((GeneralPath) shape).lineTo(42.887, 276.287);
        ((GeneralPath) shape).lineTo(42.751, 276.323);
        ((GeneralPath) shape).lineTo(42.62, 276.36);
        ((GeneralPath) shape).lineTo(42.492, 276.398);
        ((GeneralPath) shape).lineTo(42.369, 276.435);
        ((GeneralPath) shape).lineTo(42.249, 276.474);
        ((GeneralPath) shape).lineTo(42.134, 276.512);
        ((GeneralPath) shape).lineTo(42.022, 276.551);
        ((GeneralPath) shape).lineTo(41.913, 276.591);
        ((GeneralPath) shape).lineTo(41.808, 276.631);
        ((GeneralPath) shape).lineTo(41.705, 276.671);
        ((GeneralPath) shape).lineTo(41.606, 276.712);
        ((GeneralPath) shape).lineTo(41.51, 276.753);
        ((GeneralPath) shape).lineTo(41.416, 276.795);
        ((GeneralPath) shape).lineTo(41.325, 276.837);
        ((GeneralPath) shape).lineTo(41.236, 276.879);
        ((GeneralPath) shape).lineTo(41.149, 276.922);
        ((GeneralPath) shape).lineTo(41.065, 276.965);
        ((GeneralPath) shape).lineTo(40.982, 277.008);
        ((GeneralPath) shape).lineTo(40.901, 277.053);
        ((GeneralPath) shape).lineTo(40.821, 277.097);
        ((GeneralPath) shape).lineTo(40.743, 277.142);
        ((GeneralPath) shape).lineTo(40.666, 277.188);
        ((GeneralPath) shape).lineTo(40.59, 277.234);
        ((GeneralPath) shape).lineTo(40.514, 277.281);
        ((GeneralPath) shape).lineTo(40.44, 277.329);
        ((GeneralPath) shape).lineTo(40.366, 277.378);
        ((GeneralPath) shape).lineTo(40.292, 277.427);
        ((GeneralPath) shape).lineTo(40.219, 277.477);
        ((GeneralPath) shape).lineTo(40.146, 277.528);
        ((GeneralPath) shape).lineTo(40.075, 277.578);
        ((GeneralPath) shape).lineTo(39.925, 277.686);
        ((GeneralPath) shape).lineTo(39.777, 277.797);
        ((GeneralPath) shape).lineTo(39.625, 277.911);
        ((GeneralPath) shape).lineTo(39.471, 278.03);
        ((GeneralPath) shape).lineTo(39.312, 278.152);
        ((GeneralPath) shape).lineTo(39.148, 278.278);
        ((GeneralPath) shape).lineTo(38.994, 278.396);
        ((GeneralPath) shape).lineTo(39.002, 278.407);
        ((GeneralPath) shape).lineTo(39.031, 278.449);
        ((GeneralPath) shape).lineTo(39.062, 278.491);
        ((GeneralPath) shape).lineTo(39.093, 278.532);
        ((GeneralPath) shape).lineTo(39.126, 278.574);
        ((GeneralPath) shape).lineTo(39.16, 278.616);
        ((GeneralPath) shape).lineTo(39.195, 278.658);
        ((GeneralPath) shape).lineTo(39.231, 278.7);
        ((GeneralPath) shape).lineTo(39.268, 278.743);
        ((GeneralPath) shape).lineTo(39.307, 278.785);
        ((GeneralPath) shape).lineTo(39.346, 278.828);
        ((GeneralPath) shape).lineTo(39.387, 278.871);
        ((GeneralPath) shape).lineTo(39.43, 278.914);
        ((GeneralPath) shape).lineTo(39.473, 278.957);
        ((GeneralPath) shape).lineTo(39.518, 279.0);
        ((GeneralPath) shape).lineTo(39.564, 279.043);
        ((GeneralPath) shape).lineTo(39.612, 279.087);
        ((GeneralPath) shape).lineTo(39.66, 279.13);
        ((GeneralPath) shape).lineTo(39.71, 279.174);
        ((GeneralPath) shape).lineTo(39.762, 279.217);
        ((GeneralPath) shape).lineTo(39.814, 279.261);
        ((GeneralPath) shape).lineTo(39.868, 279.305);
        ((GeneralPath) shape).lineTo(39.923, 279.349);
        ((GeneralPath) shape).lineTo(39.98, 279.393);
        ((GeneralPath) shape).lineTo(40.038, 279.437);
        ((GeneralPath) shape).lineTo(40.097, 279.481);
        ((GeneralPath) shape).lineTo(40.158, 279.526);
        ((GeneralPath) shape).lineTo(40.219, 279.57);
        ((GeneralPath) shape).lineTo(40.282, 279.614);
        ((GeneralPath) shape).lineTo(40.347, 279.658);
        ((GeneralPath) shape).lineTo(40.412, 279.703);
        ((GeneralPath) shape).lineTo(40.479, 279.747);
        ((GeneralPath) shape).lineTo(40.547, 279.791);
        ((GeneralPath) shape).lineTo(40.617, 279.836);
        ((GeneralPath) shape).lineTo(40.688, 279.88);
        ((GeneralPath) shape).lineTo(40.757, 279.923);
        ((GeneralPath) shape).lineTo(40.907, 280.013);
        ((GeneralPath) shape).lineTo(41.06, 280.102);
        ((GeneralPath) shape).lineTo(41.218, 280.191);
        ((GeneralPath) shape).lineTo(41.38, 280.279);
        ((GeneralPath) shape).lineTo(41.547, 280.368);
        ((GeneralPath) shape).lineTo(41.718, 280.456);
        ((GeneralPath) shape).lineTo(41.895, 280.545);
        ((GeneralPath) shape).lineTo(42.075, 280.633);
        ((GeneralPath) shape).lineTo(42.26, 280.722);
        ((GeneralPath) shape).lineTo(42.45, 280.81);
        ((GeneralPath) shape).lineTo(42.643, 280.898);
        ((GeneralPath) shape).lineTo(42.841, 280.986);
        ((GeneralPath) shape).lineTo(43.043, 281.075);
        ((GeneralPath) shape).lineTo(43.249, 281.163);
        ((GeneralPath) shape).lineTo(43.458, 281.251);
        ((GeneralPath) shape).lineTo(43.672, 281.34);
        ((GeneralPath) shape).lineTo(43.889, 281.428);
        ((GeneralPath) shape).lineTo(44.111, 281.517);
        ((GeneralPath) shape).lineTo(44.335, 281.606);
        ((GeneralPath) shape).lineTo(44.564, 281.695);
        ((GeneralPath) shape).lineTo(44.796, 281.785);
        ((GeneralPath) shape).lineTo(45.031, 281.874);
        ((GeneralPath) shape).lineTo(45.268, 281.964);
        ((GeneralPath) shape).lineTo(45.756, 282.146);
        ((GeneralPath) shape).lineTo(46.256, 282.33);
        ((GeneralPath) shape).lineTo(46.767, 282.515);
        ((GeneralPath) shape).lineTo(47.29, 282.704);
        ((GeneralPath) shape).lineTo(48.369, 283.091);
        ((GeneralPath) shape).lineTo(48.923, 283.29);
        ((GeneralPath) shape).lineTo(49.486, 283.493);
        ((GeneralPath) shape).lineTo(50.059, 283.702);
        ((GeneralPath) shape).lineTo(50.64, 283.915);
        ((GeneralPath) shape).lineTo(51.228, 284.134);
        ((GeneralPath) shape).lineTo(51.526, 284.246);
        ((GeneralPath) shape).lineTo(51.825, 284.359);
        ((GeneralPath) shape).lineTo(52.126, 284.475);
        ((GeneralPath) shape).lineTo(52.429, 284.591);
        ((GeneralPath) shape).lineTo(52.733, 284.71);
        ((GeneralPath) shape).lineTo(53.039, 284.83);
        ((GeneralPath) shape).lineTo(53.346, 284.952);
        ((GeneralPath) shape).lineTo(53.655, 285.076);
        ((GeneralPath) shape).lineTo(53.965, 285.203);
        ((GeneralPath) shape).lineTo(54.277, 285.331);
        ((GeneralPath) shape).lineTo(54.59, 285.461);
        ((GeneralPath) shape).lineTo(54.904, 285.593);
        ((GeneralPath) shape).lineTo(55.219, 285.728);
        ((GeneralPath) shape).lineTo(55.535, 285.865);
        ((GeneralPath) shape).lineTo(55.853, 286.004);
        ((GeneralPath) shape).lineTo(56.171, 286.146);
        ((GeneralPath) shape).lineTo(56.491, 286.29);
        ((GeneralPath) shape).lineTo(56.811, 286.436);
        ((GeneralPath) shape).lineTo(57.132, 286.585);
        ((GeneralPath) shape).lineTo(57.454, 286.737);
        ((GeneralPath) shape).lineTo(57.776, 286.892);
        ((GeneralPath) shape).lineTo(58.099, 287.049);
        ((GeneralPath) shape).lineTo(58.423, 287.209);
        ((GeneralPath) shape).lineTo(58.747, 287.372);
        ((GeneralPath) shape).lineTo(59.072, 287.537);
        ((GeneralPath) shape).lineTo(59.397, 287.706);
        ((GeneralPath) shape).lineTo(59.723, 287.878);
        ((GeneralPath) shape).lineTo(60.048, 288.053);
        ((GeneralPath) shape).lineTo(60.374, 288.231);
        ((GeneralPath) shape).lineTo(60.701, 288.412);
        ((GeneralPath) shape).lineTo(61.027, 288.596);
        ((GeneralPath) shape).lineTo(61.353, 288.784);
        ((GeneralPath) shape).lineTo(61.68, 288.975);
        ((GeneralPath) shape).lineTo(62.006, 289.17);
        ((GeneralPath) shape).lineTo(62.331, 289.367);
        ((GeneralPath) shape).lineTo(62.495, 289.469);
        ((GeneralPath) shape).lineTo(62.658, 289.57);
        ((GeneralPath) shape).lineTo(62.821, 289.672);
        ((GeneralPath) shape).lineTo(62.984, 289.775);
        ((GeneralPath) shape).lineTo(63.147, 289.879);
        ((GeneralPath) shape).lineTo(63.31, 289.984);
        ((GeneralPath) shape).lineTo(63.472, 290.09);
        ((GeneralPath) shape).lineTo(63.635, 290.197);
        ((GeneralPath) shape).lineTo(63.797, 290.305);
        ((GeneralPath) shape).lineTo(63.96, 290.413);
        ((GeneralPath) shape).lineTo(64.122, 290.523);
        ((GeneralPath) shape).lineTo(64.284, 290.634);
        ((GeneralPath) shape).lineTo(64.446, 290.746);
        ((GeneralPath) shape).lineTo(64.607, 290.858);
        ((GeneralPath) shape).lineTo(64.769, 290.972);
        ((GeneralPath) shape).lineTo(64.931, 291.086);
        ((GeneralPath) shape).lineTo(65.092, 291.202);
        ((GeneralPath) shape).lineTo(65.253, 291.319);
        ((GeneralPath) shape).lineTo(65.414, 291.436);
        ((GeneralPath) shape).lineTo(65.575, 291.555);
        ((GeneralPath) shape).lineTo(65.735, 291.675);
        ((GeneralPath) shape).lineTo(65.896, 291.796);
        ((GeneralPath) shape).lineTo(66.056, 291.918);
        ((GeneralPath) shape).lineTo(66.216, 292.04);
        ((GeneralPath) shape).lineTo(66.375, 292.164);
        ((GeneralPath) shape).lineTo(66.535, 292.289);
        ((GeneralPath) shape).lineTo(66.694, 292.416);
        ((GeneralPath) shape).lineTo(66.853, 292.543);
        ((GeneralPath) shape).lineTo(67.012, 292.671);
        ((GeneralPath) shape).lineTo(67.17, 292.8);
        ((GeneralPath) shape).lineTo(67.328, 292.931);
        ((GeneralPath) shape).lineTo(67.486, 293.063);
        ((GeneralPath) shape).lineTo(67.644, 293.195);
        ((GeneralPath) shape).lineTo(67.801, 293.329);
        ((GeneralPath) shape).lineTo(67.958, 293.464);
        ((GeneralPath) shape).lineTo(68.019, 293.517);
        ((GeneralPath) shape).lineTo(348.533, 330.92);
        ((GeneralPath) shape).lineTo(345.267, 351.526);
        ((GeneralPath) shape).lineTo(347.659, 351.981);
        ((GeneralPath) shape).lineTo(344.224, 377.333);
        ((GeneralPath) shape).lineTo(340.838, 377.231);
        ((GeneralPath) shape).lineTo(340.871, 379.966);
        ((GeneralPath) shape).lineTo(346.03, 380.0);
        ((GeneralPath) shape).lineTo(345.834, 376.157);
        ((GeneralPath) shape).lineTo(348.904, 351.945);
        ((GeneralPath) shape).lineTo(352.139, 352.318);
        ((GeneralPath) shape).lineTo(355.89, 332.064);
        ((GeneralPath) shape).lineTo(428.435, 341.778);
        ((GeneralPath) shape).lineTo(471.226, 341.503);
        ((GeneralPath) shape).lineTo(471.249, 343.341);
        ((GeneralPath) shape).lineTo(471.25, 343.369);
        ((GeneralPath) shape).lineTo(471.251, 343.402);
        ((GeneralPath) shape).lineTo(471.253, 343.434);
        ((GeneralPath) shape).lineTo(471.256, 343.464);
        ((GeneralPath) shape).lineTo(471.258, 343.493);
        ((GeneralPath) shape).lineTo(471.261, 343.522);
        ((GeneralPath) shape).lineTo(471.264, 343.549);
        ((GeneralPath) shape).lineTo(471.268, 343.575);
        ((GeneralPath) shape).lineTo(471.271, 343.6);
        ((GeneralPath) shape).lineTo(471.275, 343.624);
        ((GeneralPath) shape).lineTo(471.279, 343.647);
        ((GeneralPath) shape).lineTo(471.283, 343.669);
        ((GeneralPath) shape).lineTo(471.288, 343.69);
        ((GeneralPath) shape).lineTo(471.292, 343.71);
        ((GeneralPath) shape).lineTo(471.297, 343.729);
        ((GeneralPath) shape).lineTo(471.302, 343.747);
        ((GeneralPath) shape).lineTo(471.307, 343.765);
        ((GeneralPath) shape).lineTo(471.312, 343.782);
        ((GeneralPath) shape).lineTo(471.317, 343.798);
        ((GeneralPath) shape).lineTo(471.322, 343.813);
        ((GeneralPath) shape).lineTo(471.327, 343.828);
        ((GeneralPath) shape).lineTo(471.332, 343.842);
        ((GeneralPath) shape).lineTo(471.338, 343.855);
        ((GeneralPath) shape).lineTo(471.343, 343.868);
        ((GeneralPath) shape).lineTo(471.348, 343.879);
        ((GeneralPath) shape).lineTo(471.354, 343.892);
        ((GeneralPath) shape).lineTo(471.359, 343.903);
        ((GeneralPath) shape).lineTo(471.364, 343.914);
        ((GeneralPath) shape).lineTo(471.369, 343.924);
        ((GeneralPath) shape).lineTo(471.375, 343.934);
        ((GeneralPath) shape).lineTo(471.381, 343.945);
        ((GeneralPath) shape).lineTo(471.386, 343.954);
        ((GeneralPath) shape).lineTo(471.394, 343.966);
        ((GeneralPath) shape).lineTo(471.41, 343.99);
        ((GeneralPath) shape).lineTo(471.423, 344.008);
        ((GeneralPath) shape).lineTo(471.436, 344.025);
        ((GeneralPath) shape).lineTo(471.451, 344.042);
        ((GeneralPath) shape).lineTo(471.467, 344.06);
        ((GeneralPath) shape).lineTo(471.484, 344.077);
        ((GeneralPath) shape).lineTo(471.503, 344.096);
        ((GeneralPath) shape).lineTo(471.523, 344.114);
        ((GeneralPath) shape).lineTo(471.546, 344.133);
        ((GeneralPath) shape).lineTo(471.57, 344.153);
        ((GeneralPath) shape).lineTo(471.597, 344.174);
        ((GeneralPath) shape).lineTo(471.626, 344.194);
        ((GeneralPath) shape).lineTo(471.657, 344.216);
        ((GeneralPath) shape).lineTo(471.691, 344.238);
        ((GeneralPath) shape).lineTo(471.728, 344.26);
        ((GeneralPath) shape).lineTo(471.766, 344.282);
        ((GeneralPath) shape).lineTo(471.808, 344.305);
        ((GeneralPath) shape).lineTo(471.852, 344.329);
        ((GeneralPath) shape).lineTo(471.899, 344.352);
        ((GeneralPath) shape).lineTo(471.948, 344.375);
        ((GeneralPath) shape).lineTo(472.0, 344.398);
        ((GeneralPath) shape).lineTo(472.054, 344.422);
        ((GeneralPath) shape).lineTo(472.111, 344.446);
        ((GeneralPath) shape).lineTo(472.17, 344.469);
        ((GeneralPath) shape).lineTo(472.232, 344.493);
        ((GeneralPath) shape).lineTo(472.296, 344.516);
        ((GeneralPath) shape).lineTo(472.362, 344.539);
        ((GeneralPath) shape).lineTo(472.43, 344.562);
        ((GeneralPath) shape).lineTo(472.5, 344.585);
        ((GeneralPath) shape).lineTo(472.573, 344.608);
        ((GeneralPath) shape).lineTo(472.648, 344.631);
        ((GeneralPath) shape).lineTo(472.721, 344.652);
        ((GeneralPath) shape).lineTo(472.883, 344.698);
        ((GeneralPath) shape).lineTo(473.048, 344.743);
        ((GeneralPath) shape).lineTo(473.219, 344.788);
        ((GeneralPath) shape).lineTo(473.397, 344.832);
        ((GeneralPath) shape).lineTo(473.58, 344.878);
        ((GeneralPath) shape).lineTo(473.959, 344.971);
        ((GeneralPath) shape).lineTo(474.156, 345.02);
        ((GeneralPath) shape).lineTo(474.356, 345.072);
        ((GeneralPath) shape).lineTo(474.56, 345.125);
        ((GeneralPath) shape).lineTo(474.768, 345.182);
        ((GeneralPath) shape).lineTo(474.974, 345.242);
        ((GeneralPath) shape).lineTo(475.083, 345.275);
        ((GeneralPath) shape).lineTo(475.19, 345.308);
        ((GeneralPath) shape).lineTo(475.296, 345.343);
        ((GeneralPath) shape).lineTo(475.405, 345.379);
        ((GeneralPath) shape).lineTo(475.512, 345.416);
        ((GeneralPath) shape).lineTo(475.621, 345.455);
        ((GeneralPath) shape).lineTo(475.731, 345.496);
        ((GeneralPath) shape).lineTo(475.84, 345.538);
        ((GeneralPath) shape).lineTo(475.949, 345.582);
        ((GeneralPath) shape).lineTo(476.06, 345.628);
        ((GeneralPath) shape).lineTo(476.17, 345.677);
        ((GeneralPath) shape).lineTo(476.281, 345.727);
        ((GeneralPath) shape).lineTo(476.392, 345.78);
        ((GeneralPath) shape).lineTo(476.502, 345.835);
        ((GeneralPath) shape).lineTo(476.613, 345.893);
        ((GeneralPath) shape).lineTo(476.724, 345.954);
        ((GeneralPath) shape).lineTo(476.834, 346.018);
        ((GeneralPath) shape).lineTo(476.944, 346.084);
        ((GeneralPath) shape).lineTo(477.054, 346.154);
        ((GeneralPath) shape).lineTo(477.163, 346.227);
        ((GeneralPath) shape).lineTo(477.271, 346.304);
        ((GeneralPath) shape).lineTo(477.378, 346.384);
        ((GeneralPath) shape).lineTo(477.485, 346.467);
        ((GeneralPath) shape).lineTo(477.59, 346.554);
        ((GeneralPath) shape).lineTo(477.694, 346.645);
        ((GeneralPath) shape).lineTo(477.796, 346.74);
        ((GeneralPath) shape).lineTo(477.896, 346.838);
        ((GeneralPath) shape).lineTo(477.994, 346.94);
        ((GeneralPath) shape).lineTo(478.09, 347.047);
        ((GeneralPath) shape).lineTo(478.184, 347.157);
        ((GeneralPath) shape).lineTo(478.275, 347.27);
        ((GeneralPath) shape).lineTo(478.363, 347.388);
        ((GeneralPath) shape).lineTo(478.447, 347.509);
        ((GeneralPath) shape).lineTo(478.525, 347.627);
        ((GeneralPath) shape).lineTo(478.568, 347.697);
        ((GeneralPath) shape).lineTo(478.607, 347.762);
        ((GeneralPath) shape).lineTo(478.644, 347.827);
        ((GeneralPath) shape).lineTo(478.681, 347.893);
        ((GeneralPath) shape).lineTo(478.716, 347.96);
        ((GeneralPath) shape).lineTo(478.751, 348.028);
        ((GeneralPath) shape).lineTo(478.785, 348.097);
        ((GeneralPath) shape).lineTo(478.818, 348.166);
        ((GeneralPath) shape).lineTo(478.85, 348.235);
        ((GeneralPath) shape).lineTo(478.881, 348.306);
        ((GeneralPath) shape).lineTo(478.911, 348.378);
        ((GeneralPath) shape).lineTo(478.94, 348.45);
        ((GeneralPath) shape).lineTo(478.967, 348.522);
        ((GeneralPath) shape).lineTo(478.994, 348.596);
        ((GeneralPath) shape).lineTo(479.02, 348.67);
        ((GeneralPath) shape).lineTo(479.044, 348.744);
        ((GeneralPath) shape).lineTo(479.068, 348.819);
        ((GeneralPath) shape).lineTo(479.09, 348.895);
        ((GeneralPath) shape).lineTo(479.112, 348.972);
        ((GeneralPath) shape).lineTo(479.133, 349.049);
        ((GeneralPath) shape).lineTo(479.152, 349.126);
        ((GeneralPath) shape).lineTo(479.17, 349.203);
        ((GeneralPath) shape).lineTo(479.188, 349.282);
        ((GeneralPath) shape).lineTo(479.204, 349.362);
        ((GeneralPath) shape).lineTo(479.219, 349.441);
        ((GeneralPath) shape).lineTo(479.234, 349.521);
        ((GeneralPath) shape).lineTo(479.247, 349.602);
        ((GeneralPath) shape).lineTo(479.259, 349.683);
        ((GeneralPath) shape).lineTo(479.27, 349.764);
        ((GeneralPath) shape).lineTo(479.281, 349.846);
        ((GeneralPath) shape).lineTo(479.29, 349.929);
        ((GeneralPath) shape).lineTo(479.298, 350.012);
        ((GeneralPath) shape).lineTo(479.305, 350.095);
        ((GeneralPath) shape).lineTo(479.311, 350.179);
        ((GeneralPath) shape).lineTo(479.317, 350.264);
        ((GeneralPath) shape).lineTo(479.321, 350.349);
        ((GeneralPath) shape).lineTo(479.324, 350.435);
        ((GeneralPath) shape).lineTo(479.327, 350.526);
        ((GeneralPath) shape).lineTo(479.328, 350.677);
        ((GeneralPath) shape).lineTo(479.475, 367.197);
        ((GeneralPath) shape).lineTo(475.975, 367.228);
        ((GeneralPath) shape).lineTo(475.829, 350.712);
        ((GeneralPath) shape).lineTo(475.827, 350.592);
        ((GeneralPath) shape).lineTo(475.826, 350.551);
        ((GeneralPath) shape).lineTo(475.824, 350.506);
        ((GeneralPath) shape).lineTo(475.822, 350.461);
        ((GeneralPath) shape).lineTo(475.819, 350.418);
        ((GeneralPath) shape).lineTo(475.816, 350.376);
        ((GeneralPath) shape).lineTo(475.813, 350.335);
        ((GeneralPath) shape).lineTo(475.809, 350.295);
        ((GeneralPath) shape).lineTo(475.805, 350.256);
        ((GeneralPath) shape).lineTo(475.8, 350.219);
        ((GeneralPath) shape).lineTo(475.795, 350.182);
        ((GeneralPath) shape).lineTo(475.789, 350.146);
        ((GeneralPath) shape).lineTo(475.784, 350.111);
        ((GeneralPath) shape).lineTo(475.778, 350.078);
        ((GeneralPath) shape).lineTo(475.771, 350.045);
        ((GeneralPath) shape).lineTo(475.765, 350.014);
        ((GeneralPath) shape).lineTo(475.758, 349.984);
        ((GeneralPath) shape).lineTo(475.751, 349.953);
        ((GeneralPath) shape).lineTo(475.744, 349.924);
        ((GeneralPath) shape).lineTo(475.736, 349.896);
        ((GeneralPath) shape).lineTo(475.729, 349.87);
        ((GeneralPath) shape).lineTo(475.721, 349.844);
        ((GeneralPath) shape).lineTo(475.713, 349.818);
        ((GeneralPath) shape).lineTo(475.705, 349.794);
        ((GeneralPath) shape).lineTo(475.697, 349.77);
        ((GeneralPath) shape).lineTo(475.689, 349.747);
        ((GeneralPath) shape).lineTo(475.68, 349.724);
        ((GeneralPath) shape).lineTo(475.671, 349.702);
        ((GeneralPath) shape).lineTo(475.663, 349.682);
        ((GeneralPath) shape).lineTo(475.654, 349.662);
        ((GeneralPath) shape).lineTo(475.644, 349.641);
        ((GeneralPath) shape).lineTo(475.635, 349.622);
        ((GeneralPath) shape).lineTo(475.626, 349.604);
        ((GeneralPath) shape).lineTo(475.617, 349.586);
        ((GeneralPath) shape).lineTo(475.607, 349.568);
        ((GeneralPath) shape).lineTo(475.598, 349.551);
        ((GeneralPath) shape).lineTo(475.588, 349.533);
        ((GeneralPath) shape).lineTo(475.578, 349.517);
        ((GeneralPath) shape).lineTo(475.572, 349.507);
        ((GeneralPath) shape).lineTo(475.547, 349.47);
        ((GeneralPath) shape).lineTo(475.526, 349.44);
        ((GeneralPath) shape).lineTo(475.505, 349.411);
        ((GeneralPath) shape).lineTo(475.482, 349.382);
        ((GeneralPath) shape).lineTo(475.458, 349.354);
        ((GeneralPath) shape).lineTo(475.433, 349.327);
        ((GeneralPath) shape).lineTo(475.407, 349.3);
        ((GeneralPath) shape).lineTo(475.38, 349.273);
        ((GeneralPath) shape).lineTo(475.351, 349.246);
        ((GeneralPath) shape).lineTo(475.32, 349.219);
        ((GeneralPath) shape).lineTo(475.288, 349.192);
        ((GeneralPath) shape).lineTo(475.254, 349.165);
        ((GeneralPath) shape).lineTo(475.218, 349.138);
        ((GeneralPath) shape).lineTo(475.18, 349.112);
        ((GeneralPath) shape).lineTo(475.139, 349.085);
        ((GeneralPath) shape).lineTo(475.097, 349.058);
        ((GeneralPath) shape).lineTo(475.053, 349.031);
        ((GeneralPath) shape).lineTo(475.006, 349.004);
        ((GeneralPath) shape).lineTo(474.958, 348.977);
        ((GeneralPath) shape).lineTo(474.907, 348.951);
        ((GeneralPath) shape).lineTo(474.853, 348.924);
        ((GeneralPath) shape).lineTo(474.798, 348.898);
        ((GeneralPath) shape).lineTo(474.741, 348.871);
        ((GeneralPath) shape).lineTo(474.681, 348.845);
        ((GeneralPath) shape).lineTo(474.62, 348.819);
        ((GeneralPath) shape).lineTo(474.556, 348.794);
        ((GeneralPath) shape).lineTo(474.49, 348.768);
        ((GeneralPath) shape).lineTo(474.422, 348.743);
        ((GeneralPath) shape).lineTo(474.353, 348.718);
        ((GeneralPath) shape).lineTo(474.281, 348.694);
        ((GeneralPath) shape).lineTo(474.208, 348.669);
        ((GeneralPath) shape).lineTo(474.133, 348.645);
        ((GeneralPath) shape).lineTo(474.056, 348.621);
        ((GeneralPath) shape).lineTo(473.981, 348.599);
        ((GeneralPath) shape).lineTo(473.816, 348.551);
        ((GeneralPath) shape).lineTo(473.649, 348.505);
        ((GeneralPath) shape).lineTo(473.477, 348.459);
        ((GeneralPath) shape).lineTo(473.299, 348.414);
        ((GeneralPath) shape).lineTo(473.117, 348.368);
        ((GeneralPath) shape).lineTo(472.74, 348.276);
        ((GeneralPath) shape).lineTo(472.547, 348.228);
        ((GeneralPath) shape).lineTo(472.35, 348.178);
        ((GeneralPath) shape).lineTo(472.15, 348.126);
        ((GeneralPath) shape).lineTo(471.948, 348.071);
        ((GeneralPath) shape).lineTo(471.746, 348.014);
        ((GeneralPath) shape).lineTo(471.64, 347.983);
        ((GeneralPath) shape).lineTo(471.536, 347.951);
        ((GeneralPath) shape).lineTo(471.432, 347.918);
        ((GeneralPath) shape).lineTo(471.327, 347.884);
        ((GeneralPath) shape).lineTo(471.222, 347.848);
        ((GeneralPath) shape).lineTo(471.116, 347.811);
        ((GeneralPath) shape).lineTo(471.01, 347.772);
        ((GeneralPath) shape).lineTo(470.904, 347.732);
        ((GeneralPath) shape).lineTo(470.797, 347.69);
        ((GeneralPath) shape).lineTo(470.69, 347.645);
        ((GeneralPath) shape).lineTo(470.583, 347.599);
        ((GeneralPath) shape).lineTo(470.475, 347.55);
        ((GeneralPath) shape).lineTo(470.368, 347.499);
        ((GeneralPath) shape).lineTo(470.26, 347.446);
        ((GeneralPath) shape).lineTo(470.153, 347.389);
        ((GeneralPath) shape).lineTo(470.045, 347.33);
        ((GeneralPath) shape).lineTo(469.937, 347.268);
        ((GeneralPath) shape).lineTo(469.83, 347.202);
        ((GeneralPath) shape).lineTo(469.724, 347.134);
        ((GeneralPath) shape).lineTo(469.617, 347.061);
        ((GeneralPath) shape).lineTo(469.512, 346.985);
        ((GeneralPath) shape).lineTo(469.407, 346.906);
        ((GeneralPath) shape).lineTo(469.304, 346.822);
        ((GeneralPath) shape).lineTo(469.202, 346.735);
        ((GeneralPath) shape).lineTo(469.102, 346.643);
        ((GeneralPath) shape).lineTo(469.003, 346.547);
        ((GeneralPath) shape).lineTo(468.907, 346.447);
        ((GeneralPath) shape).lineTo(468.813, 346.343);
        ((GeneralPath) shape).lineTo(468.722, 346.235);
        ((GeneralPath) shape).lineTo(468.633, 346.122);
        ((GeneralPath) shape).lineTo(468.548, 346.006);
        ((GeneralPath) shape).lineTo(468.471, 345.892);
        ((GeneralPath) shape).lineTo(468.428, 345.825);
        ((GeneralPath) shape).lineTo(468.389, 345.762);
        ((GeneralPath) shape).lineTo(468.352, 345.698);
        ((GeneralPath) shape).lineTo(468.315, 345.634);
        ((GeneralPath) shape).lineTo(468.28, 345.569);
        ((GeneralPath) shape).lineTo(468.246, 345.503);
        ((GeneralPath) shape).lineTo(468.212, 345.436);
        ((GeneralPath) shape).lineTo(468.18, 345.369);
        ((GeneralPath) shape).lineTo(468.15, 345.302);
        ((GeneralPath) shape).lineTo(468.12, 345.232);
        ((GeneralPath) shape).lineTo(468.091, 345.163);
        ((GeneralPath) shape).lineTo(468.063, 345.092);
        ((GeneralPath) shape).lineTo(468.038, 345.024);
        ((GeneralPath) shape).lineTo(428.213, 345.279);
        ((GeneralPath) shape).lineTo(358.726, 335.975);
        ((GeneralPath) shape).lineTo(354.985, 356.169);
        ((GeneralPath) shape).lineTo(351.941, 355.818);
        ((GeneralPath) shape).lineTo(349.345, 376.289);
        ((GeneralPath) shape).lineTo(349.714, 383.524);
        ((GeneralPath) shape).lineTo(337.412, 383.443);
        ((GeneralPath) shape).lineTo(337.295, 373.623);
        ((GeneralPath) shape).lineTo(341.179, 373.74);
        ((GeneralPath) shape).lineTo(343.745, 354.799);
        ((GeneralPath) shape).lineTo(341.279, 354.329);
        ((GeneralPath) shape).lineTo(344.515, 333.915);
        ((GeneralPath) shape).lineTo(66.5, 296.846);
        ((GeneralPath) shape).lineTo(65.955, 296.361);
        ((GeneralPath) shape).lineTo(65.812, 296.236);
        ((GeneralPath) shape).lineTo(65.669, 296.112);
        ((GeneralPath) shape).lineTo(65.526, 295.989);
        ((GeneralPath) shape).lineTo(65.382, 295.866);
        ((GeneralPath) shape).lineTo(65.238, 295.745);
        ((GeneralPath) shape).lineTo(65.094, 295.625);
        ((GeneralPath) shape).lineTo(64.949, 295.506);
        ((GeneralPath) shape).lineTo(64.804, 295.387);
        ((GeneralPath) shape).lineTo(64.659, 295.27);
        ((GeneralPath) shape).lineTo(64.514, 295.154);
        ((GeneralPath) shape).lineTo(64.368, 295.038);
        ((GeneralPath) shape).lineTo(64.222, 294.924);
        ((GeneralPath) shape).lineTo(64.076, 294.81);
        ((GeneralPath) shape).lineTo(63.93, 294.698);
        ((GeneralPath) shape).lineTo(63.783, 294.586);
        ((GeneralPath) shape).lineTo(63.636, 294.475);
        ((GeneralPath) shape).lineTo(63.489, 294.366);
        ((GeneralPath) shape).lineTo(63.341, 294.257);
        ((GeneralPath) shape).lineTo(63.194, 294.149);
        ((GeneralPath) shape).lineTo(63.046, 294.042);
        ((GeneralPath) shape).lineTo(62.898, 293.936);
        ((GeneralPath) shape).lineTo(62.749, 293.83);
        ((GeneralPath) shape).lineTo(62.601, 293.726);
        ((GeneralPath) shape).lineTo(62.452, 293.622);
        ((GeneralPath) shape).lineTo(62.303, 293.52);
        ((GeneralPath) shape).lineTo(62.154, 293.418);
        ((GeneralPath) shape).lineTo(62.005, 293.317);
        ((GeneralPath) shape).lineTo(61.855, 293.217);
        ((GeneralPath) shape).lineTo(61.706, 293.117);
        ((GeneralPath) shape).lineTo(61.556, 293.019);
        ((GeneralPath) shape).lineTo(61.406, 292.921);
        ((GeneralPath) shape).lineTo(61.256, 292.824);
        ((GeneralPath) shape).lineTo(61.106, 292.728);
        ((GeneralPath) shape).lineTo(60.955, 292.633);
        ((GeneralPath) shape).lineTo(60.805, 292.539);
        ((GeneralPath) shape).lineTo(60.654, 292.445);
        ((GeneralPath) shape).lineTo(60.505, 292.353);
        ((GeneralPath) shape).lineTo(60.201, 292.169);
        ((GeneralPath) shape).lineTo(59.899, 291.988);
        ((GeneralPath) shape).lineTo(59.596, 291.811);
        ((GeneralPath) shape).lineTo(59.293, 291.637);
        ((GeneralPath) shape).lineTo(58.99, 291.465);
        ((GeneralPath) shape).lineTo(58.686, 291.296);
        ((GeneralPath) shape).lineTo(58.382, 291.131);
        ((GeneralPath) shape).lineTo(58.078, 290.968);
        ((GeneralPath) shape).lineTo(57.774, 290.807);
        ((GeneralPath) shape).lineTo(57.47, 290.649);
        ((GeneralPath) shape).lineTo(57.166, 290.494);
        ((GeneralPath) shape).lineTo(56.862, 290.342);
        ((GeneralPath) shape).lineTo(56.559, 290.191);
        ((GeneralPath) shape).lineTo(56.255, 290.044);
        ((GeneralPath) shape).lineTo(55.951, 289.898);
        ((GeneralPath) shape).lineTo(55.648, 289.755);
        ((GeneralPath) shape).lineTo(55.345, 289.615);
        ((GeneralPath) shape).lineTo(55.043, 289.476);
        ((GeneralPath) shape).lineTo(54.74, 289.34);
        ((GeneralPath) shape).lineTo(54.439, 289.206);
        ((GeneralPath) shape).lineTo(54.138, 289.074);
        ((GeneralPath) shape).lineTo(53.837, 288.943);
        ((GeneralPath) shape).lineTo(53.537, 288.815);
        ((GeneralPath) shape).lineTo(53.237, 288.689);
        ((GeneralPath) shape).lineTo(52.939, 288.565);
        ((GeneralPath) shape).lineTo(52.641, 288.442);
        ((GeneralPath) shape).lineTo(52.343, 288.321);
        ((GeneralPath) shape).lineTo(52.047, 288.202);
        ((GeneralPath) shape).lineTo(51.752, 288.085);
        ((GeneralPath) shape).lineTo(51.457, 287.969);
        ((GeneralPath) shape).lineTo(51.164, 287.855);
        ((GeneralPath) shape).lineTo(50.871, 287.742);
        ((GeneralPath) shape).lineTo(50.58, 287.63);
        ((GeneralPath) shape).lineTo(50.29, 287.52);
        ((GeneralPath) shape).lineTo(50.002, 287.412);
        ((GeneralPath) shape).lineTo(49.426, 287.198);
        ((GeneralPath) shape).lineTo(48.857, 286.989);
        ((GeneralPath) shape).lineTo(48.294, 286.784);
        ((GeneralPath) shape).lineTo(47.737, 286.583);
        ((GeneralPath) shape).lineTo(47.186, 286.385);
        ((GeneralPath) shape).lineTo(46.106, 285.997);
        ((GeneralPath) shape).lineTo(45.576, 285.807);
        ((GeneralPath) shape).lineTo(45.055, 285.617);
        ((GeneralPath) shape).lineTo(44.542, 285.429);
        ((GeneralPath) shape).lineTo(44.038, 285.241);
        ((GeneralPath) shape).lineTo(43.788, 285.147);
        ((GeneralPath) shape).lineTo(43.541, 285.052);
        ((GeneralPath) shape).lineTo(43.297, 284.958);
        ((GeneralPath) shape).lineTo(43.055, 284.863);
        ((GeneralPath) shape).lineTo(42.815, 284.768);
        ((GeneralPath) shape).lineTo(42.577, 284.673);
        ((GeneralPath) shape).lineTo(42.342, 284.577);
        ((GeneralPath) shape).lineTo(42.11, 284.481);
        ((GeneralPath) shape).lineTo(41.879, 284.384);
        ((GeneralPath) shape).lineTo(41.652, 284.286);
        ((GeneralPath) shape).lineTo(41.427, 284.188);
        ((GeneralPath) shape).lineTo(41.204, 284.089);
        ((GeneralPath) shape).lineTo(40.984, 283.988);
        ((GeneralPath) shape).lineTo(40.767, 283.887);
        ((GeneralPath) shape).lineTo(40.552, 283.784);
        ((GeneralPath) shape).lineTo(40.34, 283.681);
        ((GeneralPath) shape).lineTo(40.131, 283.576);
        ((GeneralPath) shape).lineTo(39.924, 283.469);
        ((GeneralPath) shape).lineTo(39.721, 283.361);
        ((GeneralPath) shape).lineTo(39.52, 283.251);
        ((GeneralPath) shape).lineTo(39.322, 283.14);
        ((GeneralPath) shape).lineTo(39.126, 283.026);
        ((GeneralPath) shape).lineTo(38.937, 282.912);
        ((GeneralPath) shape).lineTo(38.839, 282.852);
        ((GeneralPath) shape).lineTo(38.745, 282.793);
        ((GeneralPath) shape).lineTo(38.651, 282.733);
        ((GeneralPath) shape).lineTo(38.559, 282.673);
        ((GeneralPath) shape).lineTo(38.467, 282.612);
        ((GeneralPath) shape).lineTo(38.375, 282.55);
        ((GeneralPath) shape).lineTo(38.285, 282.488);
        ((GeneralPath) shape).lineTo(38.195, 282.425);
        ((GeneralPath) shape).lineTo(38.106, 282.362);
        ((GeneralPath) shape).lineTo(38.018, 282.297);
        ((GeneralPath) shape).lineTo(37.931, 282.232);
        ((GeneralPath) shape).lineTo(37.845, 282.166);
        ((GeneralPath) shape).lineTo(37.759, 282.1);
        ((GeneralPath) shape).lineTo(37.674, 282.032);
        ((GeneralPath) shape).lineTo(37.591, 281.964);
        ((GeneralPath) shape).lineTo(37.508, 281.895);
        ((GeneralPath) shape).lineTo(37.426, 281.825);
        ((GeneralPath) shape).lineTo(37.344, 281.755);
        ((GeneralPath) shape).lineTo(37.264, 281.683);
        ((GeneralPath) shape).lineTo(37.185, 281.61);
        ((GeneralPath) shape).lineTo(37.106, 281.537);
        ((GeneralPath) shape).lineTo(37.029, 281.462);
        ((GeneralPath) shape).lineTo(36.953, 281.387);
        ((GeneralPath) shape).lineTo(36.878, 281.31);
        ((GeneralPath) shape).lineTo(36.803, 281.233);
        ((GeneralPath) shape).lineTo(36.73, 281.155);
        ((GeneralPath) shape).lineTo(36.658, 281.075);
        ((GeneralPath) shape).lineTo(36.587, 280.994);
        ((GeneralPath) shape).lineTo(36.517, 280.913);
        ((GeneralPath) shape).lineTo(36.449, 280.83);
        ((GeneralPath) shape).lineTo(36.381, 280.746);
        ((GeneralPath) shape).lineTo(36.315, 280.661);
        ((GeneralPath) shape).lineTo(36.25, 280.575);
        ((GeneralPath) shape).lineTo(36.187, 280.488);
        ((GeneralPath) shape).lineTo(36.125, 280.4);
        ((GeneralPath) shape).lineTo(36.064, 280.311);
        ((GeneralPath) shape).lineTo(36.004, 280.22);
        ((GeneralPath) shape).lineTo(35.946, 280.129);
        ((GeneralPath) shape).lineTo(35.89, 280.036);
        ((GeneralPath) shape).lineTo(35.835, 279.942);
        ((GeneralPath) shape).lineTo(35.782, 279.847);
        ((GeneralPath) shape).lineTo(35.73, 279.751);
        ((GeneralPath) shape).lineTo(35.68, 279.654);
        ((GeneralPath) shape).lineTo(35.632, 279.556);
        ((GeneralPath) shape).lineTo(35.585, 279.456);
        ((GeneralPath) shape).lineTo(35.54, 279.356);
        ((GeneralPath) shape).lineTo(35.497, 279.255);
        ((GeneralPath) shape).lineTo(35.455, 279.153);
        ((GeneralPath) shape).lineTo(35.416, 279.05);
        ((GeneralPath) shape).lineTo(35.378, 278.946);
        ((GeneralPath) shape).lineTo(35.342, 278.841);
        ((GeneralPath) shape).lineTo(35.308, 278.735);
        ((GeneralPath) shape).lineTo(35.276, 278.628);
        ((GeneralPath) shape).lineTo(35.246, 278.521);
        ((GeneralPath) shape).lineTo(35.218, 278.413);
        ((GeneralPath) shape).lineTo(35.192, 278.304);
        ((GeneralPath) shape).lineTo(35.168, 278.194);
        ((GeneralPath) shape).lineTo(34.912, 276.923);
        ((GeneralPath) shape).lineTo(36.013, 276.223);
        ((GeneralPath) shape).lineTo(36.092, 276.171);
        ((GeneralPath) shape).lineTo(36.171, 276.118);
        ((GeneralPath) shape).lineTo(36.249, 276.066);
        ((GeneralPath) shape).lineTo(36.326, 276.012);
        ((GeneralPath) shape).lineTo(36.403, 275.958);
        ((GeneralPath) shape).lineTo(36.48, 275.904);
        ((GeneralPath) shape).lineTo(36.554, 275.85);
        ((GeneralPath) shape).lineTo(36.709, 275.735);
        ((GeneralPath) shape).lineTo(36.863, 275.62);
        ((GeneralPath) shape).lineTo(37.018, 275.501);
        ((GeneralPath) shape).lineTo(37.176, 275.379);
        ((GeneralPath) shape).lineTo(37.338, 275.254);
        ((GeneralPath) shape).lineTo(37.505, 275.127);
        ((GeneralPath) shape).lineTo(37.677, 274.997);
        ((GeneralPath) shape).lineTo(37.855, 274.864);
        ((GeneralPath) shape).lineTo(38.037, 274.732);
        ((GeneralPath) shape).lineTo(38.135, 274.663);
        ((GeneralPath) shape).lineTo(38.232, 274.595);
        ((GeneralPath) shape).lineTo(38.332, 274.527);
        ((GeneralPath) shape).lineTo(38.434, 274.459);
        ((GeneralPath) shape).lineTo(38.537, 274.391);
        ((GeneralPath) shape).lineTo(38.644, 274.323);
        ((GeneralPath) shape).lineTo(38.752, 274.256);
        ((GeneralPath) shape).lineTo(38.863, 274.188);
        ((GeneralPath) shape).lineTo(38.976, 274.121);
        ((GeneralPath) shape).lineTo(39.092, 274.054);
        ((GeneralPath) shape).lineTo(39.21, 273.988);
        ((GeneralPath) shape).lineTo(39.331, 273.922);
        ((GeneralPath) shape).lineTo(39.455, 273.857);
        ((GeneralPath) shape).lineTo(39.581, 273.793);
        ((GeneralPath) shape).lineTo(39.71, 273.729);
        ((GeneralPath) shape).lineTo(39.842, 273.666);
        ((GeneralPath) shape).lineTo(39.976, 273.605);
        ((GeneralPath) shape).lineTo(40.114, 273.544);
        ((GeneralPath) shape).lineTo(40.254, 273.484);
        ((GeneralPath) shape).lineTo(40.398, 273.425);
        ((GeneralPath) shape).lineTo(40.544, 273.367);
        ((GeneralPath) shape).lineTo(40.693, 273.31);
        ((GeneralPath) shape).lineTo(40.846, 273.255);
        ((GeneralPath) shape).lineTo(41.002, 273.2);
        ((GeneralPath) shape).lineTo(41.16, 273.147);
        ((GeneralPath) shape).lineTo(41.323, 273.095);
        ((GeneralPath) shape).lineTo(41.488, 273.045);
        ((GeneralPath) shape).lineTo(41.657, 272.995);
        ((GeneralPath) shape).lineTo(41.83, 272.947);
        ((GeneralPath) shape).lineTo(42.006, 272.9);
        ((GeneralPath) shape).lineTo(42.186, 272.854);
        ((GeneralPath) shape).lineTo(42.37, 272.81);
        ((GeneralPath) shape).lineTo(42.558, 272.767);
        ((GeneralPath) shape).lineTo(42.749, 272.725);
        ((GeneralPath) shape).lineTo(42.945, 272.685);
        ((GeneralPath) shape).lineTo(43.145, 272.645);
        ((GeneralPath) shape).lineTo(43.349, 272.608);
        ((GeneralPath) shape).lineTo(43.558, 272.571);
        ((GeneralPath) shape).lineTo(43.771, 272.536);
        ((GeneralPath) shape).lineTo(43.988, 272.502);
        ((GeneralPath) shape).lineTo(44.211, 272.47);
        ((GeneralPath) shape).lineTo(44.438, 272.439);
        ((GeneralPath) shape).lineTo(44.67, 272.409);
        ((GeneralPath) shape).lineTo(44.907, 272.381);
        ((GeneralPath) shape).lineTo(45.149, 272.354);
        ((GeneralPath) shape).lineTo(45.396, 272.329);
        ((GeneralPath) shape).lineTo(45.649, 272.305);
        ((GeneralPath) shape).lineTo(45.908, 272.282);
        ((GeneralPath) shape).lineTo(46.172, 272.261);
        ((GeneralPath) shape).lineTo(46.441, 272.241);
        ((GeneralPath) shape).lineTo(46.716, 272.223);
        ((GeneralPath) shape).lineTo(46.998, 272.206);
        ((GeneralPath) shape).lineTo(47.285, 272.191);
        ((GeneralPath) shape).lineTo(47.579, 272.177);
        ((GeneralPath) shape).lineTo(47.879, 272.165);
        ((GeneralPath) shape).lineTo(48.185, 272.154);
        ((GeneralPath) shape).lineTo(48.498, 272.145);
        ((GeneralPath) shape).lineTo(48.817, 272.137);
        ((GeneralPath) shape).lineTo(49.143, 272.131);
        ((GeneralPath) shape).lineTo(49.476, 272.127);
        ((GeneralPath) shape).lineTo(49.816, 272.124);
        ((GeneralPath) shape).lineTo(50.163, 272.123);
        ((GeneralPath) shape).lineTo(50.518, 272.123);
        ((GeneralPath) shape).lineTo(50.879, 272.125);
        ((GeneralPath) shape).lineTo(51.248, 272.129);
        ((GeneralPath) shape).lineTo(51.625, 272.135);
        ((GeneralPath) shape).lineTo(52.009, 272.142);
        ((GeneralPath) shape).lineTo(52.401, 272.151);
        ((GeneralPath) shape).lineTo(52.801, 272.162);
        ((GeneralPath) shape).lineTo(53.209, 272.174);
        ((GeneralPath) shape).lineTo(53.625, 272.188);
        ((GeneralPath) shape).lineTo(54.049, 272.204);
        ((GeneralPath) shape).lineTo(54.482, 272.222);
        ((GeneralPath) shape).lineTo(54.923, 272.242);
        ((GeneralPath) shape).lineTo(55.372, 272.264);
        ((GeneralPath) shape).lineTo(55.831, 272.287);
        ((GeneralPath) shape).lineTo(56.298, 272.313);
        ((GeneralPath) shape).lineTo(56.774, 272.34);
        ((GeneralPath) shape).lineTo(57.259, 272.369);
        ((GeneralPath) shape).lineTo(57.753, 272.401);
        ((GeneralPath) shape).lineTo(58.256, 272.434);
        ((GeneralPath) shape).lineTo(58.769, 272.469);
        ((GeneralPath) shape).lineTo(59.291, 272.507);
        ((GeneralPath) shape).lineTo(59.822, 272.546);
        ((GeneralPath) shape).lineTo(60.363, 272.587);
        ((GeneralPath) shape).lineTo(60.915, 272.631);
        ((GeneralPath) shape).lineTo(61.475, 272.676);
        ((GeneralPath) shape).lineTo(62.046, 272.724);
        ((GeneralPath) shape).lineTo(62.627, 272.774);
        ((GeneralPath) shape).lineTo(63.219, 272.826);
        ((GeneralPath) shape).lineTo(63.82, 272.881);
        ((GeneralPath) shape).lineTo(64.432, 272.937);
        ((GeneralPath) shape).lineTo(65.055, 272.996);
        ((GeneralPath) shape).lineTo(65.688, 273.057);
        ((GeneralPath) shape).lineTo(66.332, 273.12);
        ((GeneralPath) shape).lineTo(66.987, 273.186);
        ((GeneralPath) shape).lineTo(67.653, 273.253);
        ((GeneralPath) shape).lineTo(68.33, 273.324);
        ((GeneralPath) shape).lineTo(69.018, 273.396);
        ((GeneralPath) shape).lineTo(69.717, 273.471);
        ((GeneralPath) shape).lineTo(70.428, 273.548);
        ((GeneralPath) shape).lineTo(71.15, 273.628);
        ((GeneralPath) shape).lineTo(422.842, 312.743);
        ((GeneralPath) shape).lineTo(422.594, 310.995);
        ((GeneralPath) shape).lineTo(422.458, 310.035);
        ((GeneralPath) shape).lineTo(422.326, 309.087);
        ((GeneralPath) shape).lineTo(422.197, 308.149);
        ((GeneralPath) shape).lineTo(422.071, 307.22);
        ((GeneralPath) shape).lineTo(421.948, 306.298);
        ((GeneralPath) shape).lineTo(421.828, 305.382);
        ((GeneralPath) shape).lineTo(421.712, 304.471);
        ((GeneralPath) shape).lineTo(421.655, 304.016);
        ((GeneralPath) shape).lineTo(421.599, 303.562);
        ((GeneralPath) shape).lineTo(421.544, 303.108);
        ((GeneralPath) shape).lineTo(421.49, 302.655);
        ((GeneralPath) shape).lineTo(421.437, 302.201);
        ((GeneralPath) shape).lineTo(421.385, 301.747);
        ((GeneralPath) shape).lineTo(421.334, 301.293);
        ((GeneralPath) shape).lineTo(421.283, 300.839);
        ((GeneralPath) shape).lineTo(421.234, 300.384);
        ((GeneralPath) shape).lineTo(421.185, 299.928);
        ((GeneralPath) shape).lineTo(421.138, 299.471);
        ((GeneralPath) shape).lineTo(421.092, 299.013);
        ((GeneralPath) shape).lineTo(421.046, 298.553);
        ((GeneralPath) shape).lineTo(421.002, 298.092);
        ((GeneralPath) shape).lineTo(420.959, 297.629);
        ((GeneralPath) shape).lineTo(420.917, 297.165);
        ((GeneralPath) shape).lineTo(420.876, 296.698);
        ((GeneralPath) shape).lineTo(420.836, 296.229);
        ((GeneralPath) shape).lineTo(420.797, 295.758);
        ((GeneralPath) shape).lineTo(420.759, 295.284);
        ((GeneralPath) shape).lineTo(420.723, 294.807);
        ((GeneralPath) shape).lineTo(420.687, 294.327);
        ((GeneralPath) shape).lineTo(420.653, 293.845);
        ((GeneralPath) shape).lineTo(420.62, 293.358);
        ((GeneralPath) shape).lineTo(420.588, 292.869);
        ((GeneralPath) shape).lineTo(420.557, 292.376);
        ((GeneralPath) shape).lineTo(420.528, 291.879);
        ((GeneralPath) shape).lineTo(420.5, 291.378);
        ((GeneralPath) shape).lineTo(420.473, 290.873);
        ((GeneralPath) shape).lineTo(420.447, 290.363);
        ((GeneralPath) shape).lineTo(420.422, 289.85);
        ((GeneralPath) shape).lineTo(420.399, 289.331);
        ((GeneralPath) shape).lineTo(420.377, 288.807);
        ((GeneralPath) shape).lineTo(420.357, 288.279);
        ((GeneralPath) shape).lineTo(420.337, 287.745);
        ((GeneralPath) shape).lineTo(420.319, 287.206);
        ((GeneralPath) shape).lineTo(420.303, 286.662);
        ((GeneralPath) shape).lineTo(420.287, 286.111);
        ((GeneralPath) shape).lineTo(420.274, 285.555);
        ((GeneralPath) shape).lineTo(420.273, 285.529);
        ((GeneralPath) shape).lineTo(347.96, 283.72);
        ((GeneralPath) shape).lineTo(347.72, 283.69);
        ((GeneralPath) shape).lineTo(347.531, 283.666);
        ((GeneralPath) shape).lineTo(347.278, 283.633);
        ((GeneralPath) shape).lineTo(346.966, 283.591);
        ((GeneralPath) shape).lineTo(346.599, 283.539);
        ((GeneralPath) shape).lineTo(346.181, 283.479);
        ((GeneralPath) shape).lineTo(345.72, 283.409);
        ((GeneralPath) shape).lineTo(345.472, 283.371);
        ((GeneralPath) shape).lineTo(345.216, 283.33);
        ((GeneralPath) shape).lineTo(344.951, 283.287);
        ((GeneralPath) shape).lineTo(344.678, 283.242);
        ((GeneralPath) shape).lineTo(344.397, 283.195);
        ((GeneralPath) shape).lineTo(344.109, 283.145);
        ((GeneralPath) shape).lineTo(343.815, 283.093);
        ((GeneralPath) shape).lineTo(343.515, 283.039);
        ((GeneralPath) shape).lineTo(343.209, 282.983);
        ((GeneralPath) shape).lineTo(342.899, 282.924);
        ((GeneralPath) shape).lineTo(342.585, 282.863);
        ((GeneralPath) shape).lineTo(342.267, 282.8);
        ((GeneralPath) shape).lineTo(341.946, 282.734);
        ((GeneralPath) shape).lineTo(341.623, 282.666);
        ((GeneralPath) shape).lineTo(341.298, 282.596);
        ((GeneralPath) shape).lineTo(340.972, 282.524);
        ((GeneralPath) shape).lineTo(340.646, 282.449);
        ((GeneralPath) shape).lineTo(340.319, 282.371);
        ((GeneralPath) shape).lineTo(339.993, 282.292);
        ((GeneralPath) shape).lineTo(339.667, 282.21);
        ((GeneralPath) shape).lineTo(339.344, 282.125);
        ((GeneralPath) shape).lineTo(339.024, 282.039);
        ((GeneralPath) shape).lineTo(338.862, 281.994);
        ((GeneralPath) shape).lineTo(338.703, 281.949);
        ((GeneralPath) shape).lineTo(338.545, 281.903);
        ((GeneralPath) shape).lineTo(338.387, 281.856);
        ((GeneralPath) shape).lineTo(338.231, 281.809);
        ((GeneralPath) shape).lineTo(338.075, 281.762);
        ((GeneralPath) shape).lineTo(337.92, 281.713);
        ((GeneralPath) shape).lineTo(337.767, 281.664);
        ((GeneralPath) shape).lineTo(337.614, 281.614);
        ((GeneralPath) shape).lineTo(337.463, 281.563);
        ((GeneralPath) shape).lineTo(337.312, 281.512);
        ((GeneralPath) shape).lineTo(337.164, 281.459);
        ((GeneralPath) shape).lineTo(337.016, 281.406);
        ((GeneralPath) shape).lineTo(336.87, 281.353);
        ((GeneralPath) shape).lineTo(336.725, 281.298);
        ((GeneralPath) shape).lineTo(336.582, 281.242);
        ((GeneralPath) shape).lineTo(336.44, 281.185);
        ((GeneralPath) shape).lineTo(336.3, 281.128);
        ((GeneralPath) shape).lineTo(336.161, 281.069);
        ((GeneralPath) shape).lineTo(336.024, 281.01);
        ((GeneralPath) shape).lineTo(335.888, 280.949);
        ((GeneralPath) shape).lineTo(335.754, 280.887);
        ((GeneralPath) shape).lineTo(335.622, 280.824);
        ((GeneralPath) shape).lineTo(335.491, 280.759);
        ((GeneralPath) shape).lineTo(335.362, 280.693);
        ((GeneralPath) shape).lineTo(335.235, 280.625);
        ((GeneralPath) shape).lineTo(335.109, 280.556);
        ((GeneralPath) shape).lineTo(334.985, 280.485);
        ((GeneralPath) shape).lineTo(334.863, 280.412);
        ((GeneralPath) shape).lineTo(334.742, 280.336);
        ((GeneralPath) shape).lineTo(334.623, 280.259);
        ((GeneralPath) shape).lineTo(334.506, 280.179);
        ((GeneralPath) shape).lineTo(334.39, 280.096);
        ((GeneralPath) shape).lineTo(334.277, 280.009);
        ((GeneralPath) shape).lineTo(334.165, 279.92);
        ((GeneralPath) shape).lineTo(334.054, 279.826);
        ((GeneralPath) shape).lineTo(333.951, 279.733);
        ((GeneralPath) shape).lineTo(333.893, 279.678);
        ((GeneralPath) shape).lineTo(333.84, 279.626);
        ((GeneralPath) shape).lineTo(333.787, 279.573);
        ((GeneralPath) shape).lineTo(333.736, 279.519);
        ((GeneralPath) shape).lineTo(333.684, 279.463);
        ((GeneralPath) shape).lineTo(333.634, 279.405);
        ((GeneralPath) shape).lineTo(333.584, 279.346);
        ((GeneralPath) shape).lineTo(333.535, 279.286);
        ((GeneralPath) shape).lineTo(333.487, 279.223);
        ((GeneralPath) shape).lineTo(333.44, 279.159);
        ((GeneralPath) shape).lineTo(333.393, 279.093);
        ((GeneralPath) shape).lineTo(333.349, 279.025);
        ((GeneralPath) shape).lineTo(333.305, 278.955);
        ((GeneralPath) shape).lineTo(333.262, 278.883);
        ((GeneralPath) shape).lineTo(333.221, 278.809);
        ((GeneralPath) shape).lineTo(333.181, 278.734);
        ((GeneralPath) shape).lineTo(333.143, 278.656);
        ((GeneralPath) shape).lineTo(333.107, 278.576);
        ((GeneralPath) shape).lineTo(333.073, 278.493);
        ((GeneralPath) shape).lineTo(333.041, 278.409);
        ((GeneralPath) shape).lineTo(333.012, 278.323);
        ((GeneralPath) shape).lineTo(332.985, 278.235);
        ((GeneralPath) shape).lineTo(332.96, 278.145);
        ((GeneralPath) shape).lineTo(332.939, 278.054);
        ((GeneralPath) shape).lineTo(332.92, 277.961);
        ((GeneralPath) shape).lineTo(332.905, 277.867);
        ((GeneralPath) shape).lineTo(332.893, 277.771);
        ((GeneralPath) shape).lineTo(332.884, 277.675);
        ((GeneralPath) shape).lineTo(332.879, 277.578);
        ((GeneralPath) shape).lineTo(332.877, 277.481);
        ((GeneralPath) shape).lineTo(332.879, 277.381);
        ((GeneralPath) shape).lineTo(332.885, 277.282);
        ((GeneralPath) shape).lineTo(332.894, 277.184);
        ((GeneralPath) shape).lineTo(332.907, 277.087);
        ((GeneralPath) shape).lineTo(332.923, 276.991);
        ((GeneralPath) shape).lineTo(332.942, 276.897);
        ((GeneralPath) shape).lineTo(332.964, 276.804);
        ((GeneralPath) shape).lineTo(332.99, 276.713);
        ((GeneralPath) shape).lineTo(333.018, 276.625);
        ((GeneralPath) shape).lineTo(333.048, 276.538);
        ((GeneralPath) shape).lineTo(333.081, 276.453);
        ((GeneralPath) shape).lineTo(333.116, 276.37);
        ((GeneralPath) shape).lineTo(333.153, 276.291);
        ((GeneralPath) shape).lineTo(333.191, 276.212);
        ((GeneralPath) shape).lineTo(333.232, 276.136);
        ((GeneralPath) shape).lineTo(333.274, 276.062);
        ((GeneralPath) shape).lineTo(333.317, 275.99);
        ((GeneralPath) shape).lineTo(333.362, 275.921);
        ((GeneralPath) shape).lineTo(333.407, 275.853);
        ((GeneralPath) shape).lineTo(333.454, 275.787);
        ((GeneralPath) shape).lineTo(333.502, 275.723);
        ((GeneralPath) shape).lineTo(333.55, 275.66);
        ((GeneralPath) shape).lineTo(333.6, 275.6);
        ((GeneralPath) shape).lineTo(333.65, 275.541);
        ((GeneralPath) shape).lineTo(333.701, 275.483);
        ((GeneralPath) shape).lineTo(333.752, 275.427);
        ((GeneralPath) shape).lineTo(333.804, 275.372);
        ((GeneralPath) shape).lineTo(333.857, 275.318);
        ((GeneralPath) shape).lineTo(333.911, 275.266);
        ((GeneralPath) shape).lineTo(333.969, 275.21);
        ((GeneralPath) shape).lineTo(334.073, 275.116);
        ((GeneralPath) shape).lineTo(334.185, 275.021);
        ((GeneralPath) shape).lineTo(334.298, 274.93);
        ((GeneralPath) shape).lineTo(334.413, 274.842);
        ((GeneralPath) shape).lineTo(334.53, 274.756);
        ((GeneralPath) shape).lineTo(334.649, 274.674);
        ((GeneralPath) shape).lineTo(334.769, 274.594);
        ((GeneralPath) shape).lineTo(334.892, 274.515);
        ((GeneralPath) shape).lineTo(335.017, 274.439);
        ((GeneralPath) shape).lineTo(335.143, 274.364);
        ((GeneralPath) shape).lineTo(335.271, 274.292);
        ((GeneralPath) shape).lineTo(335.401, 274.22);
        ((GeneralPath) shape).lineTo(335.533, 274.15);
        ((GeneralPath) shape).lineTo(335.666, 274.082);
        ((GeneralPath) shape).lineTo(335.802, 274.014);
        ((GeneralPath) shape).lineTo(335.939, 273.948);
        ((GeneralPath) shape).lineTo(336.078, 273.883);
        ((GeneralPath) shape).lineTo(336.219, 273.818);
        ((GeneralPath) shape).lineTo(336.362, 273.755);
        ((GeneralPath) shape).lineTo(336.506, 273.693);
        ((GeneralPath) shape).lineTo(336.652, 273.631);
        ((GeneralPath) shape).lineTo(336.799, 273.57);
        ((GeneralPath) shape).lineTo(336.948, 273.511);
        ((GeneralPath) shape).lineTo(337.099, 273.451);
        ((GeneralPath) shape).lineTo(337.251, 273.393);
        ((GeneralPath) shape).lineTo(337.404, 273.335);
        ((GeneralPath) shape).lineTo(337.559, 273.278);
        ((GeneralPath) shape).lineTo(337.715, 273.222);
        ((GeneralPath) shape).lineTo(337.873, 273.167);
        ((GeneralPath) shape).lineTo(338.031, 273.112);
        ((GeneralPath) shape).lineTo(338.191, 273.058);
        ((GeneralPath) shape).lineTo(338.352, 273.004);
        ((GeneralPath) shape).lineTo(338.514, 272.951);
        ((GeneralPath) shape).lineTo(338.677, 272.899);
        ((GeneralPath) shape).lineTo(338.84, 272.847);
        ((GeneralPath) shape).lineTo(339.005, 272.796);
        ((GeneralPath) shape).lineTo(339.172, 272.745);
        ((GeneralPath) shape).lineTo(339.503, 272.646);
        ((GeneralPath) shape).lineTo(339.838, 272.549);
        ((GeneralPath) shape).lineTo(340.175, 272.455);
        ((GeneralPath) shape).lineTo(340.512, 272.363);
        ((GeneralPath) shape).lineTo(340.851, 272.273);
        ((GeneralPath) shape).lineTo(341.189, 272.186);
        ((GeneralPath) shape).lineTo(341.527, 272.101);
        ((GeneralPath) shape).lineTo(341.864, 272.018);
        ((GeneralPath) shape).lineTo(342.199, 271.938);
        ((GeneralPath) shape).lineTo(342.531, 271.86);
        ((GeneralPath) shape).lineTo(342.86, 271.784);
        ((GeneralPath) shape).lineTo(343.186, 271.711);
        ((GeneralPath) shape).lineTo(343.508, 271.64);
        ((GeneralPath) shape).lineTo(343.825, 271.572);
        ((GeneralPath) shape).lineTo(344.136, 271.506);
        ((GeneralPath) shape).lineTo(344.441, 271.443);
        ((GeneralPath) shape).lineTo(344.74, 271.382);
        ((GeneralPath) shape).lineTo(345.031, 271.324);
        ((GeneralPath) shape).lineTo(345.315, 271.268);
        ((GeneralPath) shape).lineTo(345.589, 271.215);
        ((GeneralPath) shape).lineTo(345.855, 271.164);
        ((GeneralPath) shape).lineTo(346.112, 271.116);
        ((GeneralPath) shape).lineTo(346.59, 271.029);
        ((GeneralPath) shape).lineTo(347.023, 270.952);
        ((GeneralPath) shape).lineTo(347.404, 270.886);
        ((GeneralPath) shape).lineTo(347.728, 270.832);
        ((GeneralPath) shape).lineTo(347.99, 270.789);
        ((GeneralPath) shape).lineTo(348.186, 270.757);
        ((GeneralPath) shape).lineTo(348.481, 270.711);
        ((GeneralPath) shape).lineTo(419.962, 270.711);
        ((GeneralPath) shape).lineTo(419.963, 270.633);
        ((GeneralPath) shape).lineTo(419.967, 270.369);
        ((GeneralPath) shape).lineTo(419.974, 270.09);
        ((GeneralPath) shape).lineTo(419.982, 269.796);
        ((GeneralPath) shape).lineTo(419.992, 269.487);
        ((GeneralPath) shape).lineTo(420.004, 269.164);
        ((GeneralPath) shape).lineTo(420.019, 268.826);
        ((GeneralPath) shape).lineTo(420.037, 268.475);
        ((GeneralPath) shape).lineTo(420.057, 268.11);
        ((GeneralPath) shape).lineTo(420.08, 267.732);
        ((GeneralPath) shape).lineTo(420.106, 267.341);
        ((GeneralPath) shape).lineTo(420.136, 266.938);
        ((GeneralPath) shape).lineTo(420.168, 266.522);
        ((GeneralPath) shape).lineTo(420.205, 266.094);
        ((GeneralPath) shape).lineTo(420.245, 265.655);
        ((GeneralPath) shape).lineTo(420.29, 265.206);
        ((GeneralPath) shape).lineTo(420.313, 264.975);
        ((GeneralPath) shape).lineTo(420.338, 264.742);
        ((GeneralPath) shape).lineTo(420.364, 264.507);
        ((GeneralPath) shape).lineTo(420.391, 264.27);
        ((GeneralPath) shape).lineTo(420.419, 264.03);
        ((GeneralPath) shape).lineTo(420.448, 263.787);
        ((GeneralPath) shape).lineTo(420.479, 263.541);
        ((GeneralPath) shape).lineTo(420.51, 263.294);
        ((GeneralPath) shape).lineTo(420.543, 263.043);
        ((GeneralPath) shape).lineTo(420.577, 262.791);
        ((GeneralPath) shape).lineTo(420.613, 262.536);
        ((GeneralPath) shape).lineTo(420.649, 262.278);
        ((GeneralPath) shape).lineTo(420.687, 262.019);
        ((GeneralPath) shape).lineTo(420.726, 261.756);
        ((GeneralPath) shape).lineTo(420.767, 261.492);
        ((GeneralPath) shape).lineTo(420.809, 261.226);
        ((GeneralPath) shape).lineTo(420.852, 260.957);
        ((GeneralPath) shape).lineTo(420.897, 260.687);
        ((GeneralPath) shape).lineTo(420.943, 260.414);
        ((GeneralPath) shape).lineTo(420.991, 260.139);
        ((GeneralPath) shape).lineTo(421.04, 259.862);
        ((GeneralPath) shape).lineTo(421.091, 259.583);
        ((GeneralPath) shape).lineTo(421.143, 259.303);
        ((GeneralPath) shape).lineTo(421.197, 259.02);
        ((GeneralPath) shape).lineTo(421.252, 258.736);
        ((GeneralPath) shape).lineTo(421.309, 258.45);
        ((GeneralPath) shape).lineTo(421.368, 258.162);
        ((GeneralPath) shape).lineTo(421.428, 257.872);
        ((GeneralPath) shape).lineTo(421.49, 257.581);
        ((GeneralPath) shape).lineTo(421.553, 257.287);
        ((GeneralPath) shape).lineTo(421.619, 256.993);
        ((GeneralPath) shape).lineTo(421.686, 256.697);
        ((GeneralPath) shape).lineTo(421.754, 256.399);
        ((GeneralPath) shape).lineTo(421.825, 256.099);
        ((GeneralPath) shape).lineTo(421.897, 255.799);
        ((GeneralPath) shape).lineTo(421.972, 255.497);
        ((GeneralPath) shape).lineTo(422.048, 255.193);
        ((GeneralPath) shape).lineTo(422.126, 254.888);
        ((GeneralPath) shape).lineTo(422.205, 254.582);
        ((GeneralPath) shape).lineTo(422.287, 254.274);
        ((GeneralPath) shape).lineTo(422.371, 253.965);
        ((GeneralPath) shape).lineTo(422.456, 253.656);
        ((GeneralPath) shape).lineTo(422.544, 253.344);
        ((GeneralPath) shape).lineTo(422.633, 253.032);
        ((GeneralPath) shape).lineTo(422.725, 252.719);
        ((GeneralPath) shape).lineTo(422.819, 252.405);
        ((GeneralPath) shape).lineTo(422.915, 252.089);
        ((GeneralPath) shape).lineTo(423.013, 251.773);
        ((GeneralPath) shape).lineTo(423.112, 251.456);
        ((GeneralPath) shape).lineTo(423.215, 251.137);
        ((GeneralPath) shape).lineTo(423.319, 250.819);
        ((GeneralPath) shape).lineTo(423.425, 250.499);
        ((GeneralPath) shape).lineTo(423.534, 250.178);
        ((GeneralPath) shape).lineTo(423.645, 249.857);
        ((GeneralPath) shape).lineTo(423.758, 249.535);
        ((GeneralPath) shape).lineTo(423.874, 249.212);
        ((GeneralPath) shape).lineTo(423.992, 248.889);
        ((GeneralPath) shape).lineTo(424.112, 248.565);
        ((GeneralPath) shape).lineTo(424.235, 248.241);
        ((GeneralPath) shape).lineTo(424.359, 247.916);
        ((GeneralPath) shape).lineTo(424.487, 247.591);
        ((GeneralPath) shape).lineTo(424.617, 247.265);
        ((GeneralPath) shape).lineTo(424.749, 246.939);
        ((GeneralPath) shape).lineTo(424.883, 246.612);
        ((GeneralPath) shape).lineTo(425.021, 246.285);
        ((GeneralPath) shape).lineTo(425.16, 245.958);
        ((GeneralPath) shape).lineTo(425.302, 245.631);
        ((GeneralPath) shape).lineTo(425.447, 245.304);
        ((GeneralPath) shape).lineTo(425.595, 244.976);
        ((GeneralPath) shape).lineTo(425.745, 244.648);
        ((GeneralPath) shape).lineTo(425.897, 244.32);
        ((GeneralPath) shape).lineTo(426.053, 243.993);
        ((GeneralPath) shape).lineTo(426.211, 243.665);
        ((GeneralPath) shape).lineTo(426.371, 243.337);
        ((GeneralPath) shape).lineTo(426.535, 243.01);
        ((GeneralPath) shape).lineTo(426.701, 242.682);
        ((GeneralPath) shape).lineTo(426.87, 242.355);
        ((GeneralPath) shape).lineTo(427.042, 242.028);
        ((GeneralPath) shape).lineTo(427.216, 241.701);
        ((GeneralPath) shape).lineTo(427.394, 241.374);
        ((GeneralPath) shape).lineTo(427.574, 241.048);
        ((GeneralPath) shape).lineTo(427.757, 240.722);
        ((GeneralPath) shape).lineTo(427.944, 240.396);
        ((GeneralPath) shape).lineTo(428.133, 240.071);
        ((GeneralPath) shape).lineTo(428.325, 239.747);
        ((GeneralPath) shape).lineTo(428.52, 239.423);
        ((GeneralPath) shape).lineTo(428.718, 239.099);
        ((GeneralPath) shape).lineTo(428.919, 238.776);
        ((GeneralPath) shape).lineTo(429.123, 238.454);
        ((GeneralPath) shape).lineTo(429.331, 238.133);
        ((GeneralPath) shape).lineTo(429.541, 237.812);
        ((GeneralPath) shape).lineTo(429.755, 237.492);
        ((GeneralPath) shape).lineTo(429.97, 237.174);
        ((GeneralPath) shape).lineTo(430.081, 237.014);
        ((GeneralPath) shape).lineTo(430.191, 236.855);
        ((GeneralPath) shape).lineTo(430.302, 236.696);
        ((GeneralPath) shape).lineTo(430.414, 236.537);
        ((GeneralPath) shape).lineTo(430.527, 236.379);
        ((GeneralPath) shape).lineTo(430.64, 236.221);
        ((GeneralPath) shape).lineTo(430.755, 236.063);
        ((GeneralPath) shape).lineTo(430.87, 235.905);
        ((GeneralPath) shape).lineTo(430.986, 235.748);
        ((GeneralPath) shape).lineTo(431.103, 235.591);
        ((GeneralPath) shape).lineTo(431.22, 235.434);
        ((GeneralPath) shape).lineTo(431.339, 235.278);
        ((GeneralPath) shape).lineTo(431.458, 235.121);
        ((GeneralPath) shape).lineTo(431.578, 234.965);
        ((GeneralPath) shape).lineTo(431.699, 234.81);
        ((GeneralPath) shape).lineTo(431.821, 234.654);
        ((GeneralPath) shape).lineTo(431.944, 234.499);
        ((GeneralPath) shape).lineTo(432.067, 234.344);
        ((GeneralPath) shape).lineTo(432.191, 234.19);
        ((GeneralPath) shape).lineTo(432.316, 234.036);
        ((GeneralPath) shape).lineTo(432.443, 233.882);
        ((GeneralPath) shape).lineTo(432.569, 233.728);
        ((GeneralPath) shape).lineTo(432.697, 233.575);
        ((GeneralPath) shape).lineTo(432.826, 233.422);
        ((GeneralPath) shape).lineTo(432.955, 233.27);
        ((GeneralPath) shape).lineTo(433.086, 233.118);
        ((GeneralPath) shape).lineTo(433.217, 232.966);
        ((GeneralPath) shape).lineTo(433.349, 232.814);
        ((GeneralPath) shape).lineTo(433.482, 232.663);
        ((GeneralPath) shape).lineTo(433.616, 232.513);
        ((GeneralPath) shape).lineTo(433.75, 232.362);
        ((GeneralPath) shape).lineTo(433.886, 232.212);
        ((GeneralPath) shape).lineTo(434.022, 232.063);
        ((GeneralPath) shape).lineTo(434.16, 231.913);
        ((GeneralPath) shape).lineTo(434.298, 231.765);
        ((GeneralPath) shape).lineTo(434.437, 231.616);
        ((GeneralPath) shape).lineTo(434.577, 231.468);
        ((GeneralPath) shape).lineTo(434.718, 231.321);
        ((GeneralPath) shape).lineTo(434.86, 231.174);
        ((GeneralPath) shape).lineTo(435.002, 231.027);
        ((GeneralPath) shape).lineTo(435.146, 230.88);
        ((GeneralPath) shape).lineTo(435.291, 230.734);
        ((GeneralPath) shape).lineTo(435.436, 230.589);
        ((GeneralPath) shape).lineTo(435.582, 230.444);
        ((GeneralPath) shape).lineTo(435.73, 230.299);
        ((GeneralPath) shape).lineTo(435.878, 230.155);
        ((GeneralPath) shape).lineTo(436.027, 230.012);
        ((GeneralPath) shape).lineTo(436.177, 229.869);
        ((GeneralPath) shape).lineTo(436.328, 229.726);
        ((GeneralPath) shape).lineTo(436.48, 229.584);
        ((GeneralPath) shape).lineTo(436.633, 229.442);
        ((GeneralPath) shape).lineTo(436.786, 229.301);
        ((GeneralPath) shape).lineTo(436.941, 229.16);
        ((GeneralPath) shape).lineTo(437.097, 229.019);
        ((GeneralPath) shape).lineTo(437.253, 228.88);
        ((GeneralPath) shape).lineTo(437.411, 228.74);
        ((GeneralPath) shape).lineTo(437.569, 228.602);
        ((GeneralPath) shape).lineTo(437.728, 228.463);
        ((GeneralPath) shape).lineTo(437.889, 228.326);
        ((GeneralPath) shape).lineTo(438.05, 228.188);
        ((GeneralPath) shape).lineTo(438.212, 228.052);
        ((GeneralPath) shape).lineTo(438.376, 227.916);
        ((GeneralPath) shape).lineTo(438.54, 227.78);
        ((GeneralPath) shape).lineTo(438.705, 227.645);
        ((GeneralPath) shape).lineTo(438.871, 227.511);
        ((GeneralPath) shape).lineTo(439.038, 227.377);
        ((GeneralPath) shape).lineTo(439.206, 227.243);
        ((GeneralPath) shape).lineTo(439.375, 227.111);
        ((GeneralPath) shape).lineTo(439.545, 226.978);
        ((GeneralPath) shape).lineTo(439.716, 226.847);
        ((GeneralPath) shape).lineTo(439.888, 226.716);
        ((GeneralPath) shape).lineTo(440.061, 226.585);
        ((GeneralPath) shape).lineTo(440.235, 226.455);
        ((GeneralPath) shape).lineTo(440.41, 226.326);
        ((GeneralPath) shape).lineTo(440.586, 226.197);
        ((GeneralPath) shape).lineTo(440.763, 226.069);
        ((GeneralPath) shape).lineTo(440.94, 225.942);
        ((GeneralPath) shape).lineTo(441.119, 225.815);
        ((GeneralPath) shape).lineTo(441.299, 225.689);
        ((GeneralPath) shape).lineTo(441.48, 225.564);
        ((GeneralPath) shape).lineTo(441.662, 225.439);
        ((GeneralPath) shape).lineTo(441.845, 225.315);
        ((GeneralPath) shape).lineTo(442.029, 225.191);
        ((GeneralPath) shape).lineTo(442.214, 225.068);
        ((GeneralPath) shape).lineTo(442.4, 224.946);
        ((GeneralPath) shape).lineTo(442.587, 224.824);
        ((GeneralPath) shape).lineTo(442.774, 224.703);
        ((GeneralPath) shape).lineTo(442.963, 224.583);
        ((GeneralPath) shape).lineTo(443.153, 224.464);
        ((GeneralPath) shape).lineTo(443.344, 224.345);
        ((GeneralPath) shape).lineTo(443.537, 224.227);
        ((GeneralPath) shape).lineTo(443.729, 224.109);
        ((GeneralPath) shape).lineTo(443.923, 223.993);
        ((GeneralPath) shape).lineTo(444.119, 223.876);
        ((GeneralPath) shape).lineTo(444.315, 223.761);
        ((GeneralPath) shape).lineTo(444.512, 223.646);
        ((GeneralPath) shape).lineTo(444.71, 223.533);
        ((GeneralPath) shape).lineTo(444.909, 223.419);
        ((GeneralPath) shape).lineTo(445.11, 223.307);
        ((GeneralPath) shape).lineTo(445.311, 223.195);
        ((GeneralPath) shape).lineTo(445.513, 223.084);
        ((GeneralPath) shape).lineTo(445.717, 222.974);
        ((GeneralPath) shape).lineTo(445.921, 222.865);
        ((GeneralPath) shape).lineTo(446.127, 222.756);
        ((GeneralPath) shape).lineTo(446.333, 222.648);
        ((GeneralPath) shape).lineTo(446.541, 222.541);
        ((GeneralPath) shape).lineTo(446.75, 222.435);
        ((GeneralPath) shape).lineTo(446.96, 222.329);
        ((GeneralPath) shape).lineTo(447.17, 222.224);
        ((GeneralPath) shape).lineTo(447.382, 222.12);
        ((GeneralPath) shape).lineTo(447.595, 222.017);
        ((GeneralPath) shape).lineTo(447.809, 221.915);
        ((GeneralPath) shape).lineTo(448.024, 221.813);
        ((GeneralPath) shape).lineTo(448.241, 221.712);
        ((GeneralPath) shape).lineTo(448.458, 221.613);
        ((GeneralPath) shape).lineTo(448.676, 221.513);
        ((GeneralPath) shape).lineTo(448.896, 221.415);
        ((GeneralPath) shape).lineTo(449.116, 221.318);
        ((GeneralPath) shape).lineTo(449.338, 221.221);
        ((GeneralPath) shape).lineTo(449.56, 221.125);
        ((GeneralPath) shape).lineTo(449.784, 221.03);
        ((GeneralPath) shape).lineTo(450.009, 220.936);
        ((GeneralPath) shape).lineTo(450.235, 220.843);
        ((GeneralPath) shape).lineTo(450.462, 220.751);
        ((GeneralPath) shape).lineTo(450.69, 220.659);
        ((GeneralPath) shape).lineTo(450.919, 220.568);
        ((GeneralPath) shape).lineTo(451.149, 220.479);
        ((GeneralPath) shape).lineTo(451.381, 220.39);
        ((GeneralPath) shape).lineTo(451.613, 220.302);
        ((GeneralPath) shape).lineTo(451.847, 220.215);
        ((GeneralPath) shape).lineTo(452.082, 220.128);
        ((GeneralPath) shape).lineTo(452.318, 220.043);
        ((GeneralPath) shape).lineTo(452.554, 219.959);
        ((GeneralPath) shape).lineTo(452.793, 219.875);
        ((GeneralPath) shape).lineTo(453.032, 219.793);
        ((GeneralPath) shape).lineTo(453.272, 219.711);
        ((GeneralPath) shape).lineTo(453.513, 219.63);
        ((GeneralPath) shape).lineTo(453.756, 219.551);
        ((GeneralPath) shape).lineTo(454.0, 219.472);
        ((GeneralPath) shape).lineTo(454.244, 219.394);
        ((GeneralPath) shape).lineTo(454.49, 219.317);
        ((GeneralPath) shape).lineTo(454.738, 219.241);
        ((GeneralPath) shape).lineTo(454.986, 219.166);
        ((GeneralPath) shape).lineTo(455.235, 219.092);
        ((GeneralPath) shape).lineTo(455.485, 219.018);
        ((GeneralPath) shape).lineTo(455.737, 218.946);
        ((GeneralPath) shape).lineTo(455.99, 218.875);
        ((GeneralPath) shape).lineTo(456.244, 218.805);
        ((GeneralPath) shape).lineTo(456.499, 218.736);
        ((GeneralPath) shape).lineTo(456.755, 218.667);
        ((GeneralPath) shape).lineTo(457.012, 218.6);
        ((GeneralPath) shape).lineTo(457.271, 218.534);
        ((GeneralPath) shape).lineTo(457.53, 218.468);
        ((GeneralPath) shape).lineTo(457.791, 218.404);
        ((GeneralPath) shape).lineTo(458.053, 218.341);
        ((GeneralPath) shape).lineTo(458.316, 218.279);
        ((GeneralPath) shape).lineTo(458.581, 218.217);
        ((GeneralPath) shape).lineTo(458.846, 218.157);
        ((GeneralPath) shape).lineTo(459.113, 218.098);
        ((GeneralPath) shape).lineTo(459.38, 218.04);
        ((GeneralPath) shape).lineTo(459.649, 217.983);
        ((GeneralPath) shape).lineTo(459.919, 217.927);
        ((GeneralPath) shape).lineTo(460.191, 217.872);
        ((GeneralPath) shape).lineTo(460.463, 217.818);
        ((GeneralPath) shape).lineTo(460.737, 217.765);
        ((GeneralPath) shape).lineTo(461.012, 217.713);
        ((GeneralPath) shape).lineTo(461.288, 217.662);
        ((GeneralPath) shape).lineTo(461.565, 217.613);
        ((GeneralPath) shape).lineTo(461.844, 217.564);
        ((GeneralPath) shape).lineTo(462.123, 217.517);
        ((GeneralPath) shape).lineTo(462.404, 217.47);
        ((GeneralPath) shape).lineTo(462.686, 217.425);
        ((GeneralPath) shape).lineTo(462.969, 217.38);
        ((GeneralPath) shape).lineTo(463.254, 217.337);
        ((GeneralPath) shape).lineTo(463.539, 217.295);
        ((GeneralPath) shape).lineTo(463.826, 217.254);
        ((GeneralPath) shape).lineTo(464.114, 217.214);
        ((GeneralPath) shape).lineTo(464.403, 217.176);
        ((GeneralPath) shape).lineTo(464.694, 217.138);
        ((GeneralPath) shape).lineTo(464.985, 217.101);
        ((GeneralPath) shape).lineTo(465.278, 217.066);
        ((GeneralPath) shape).lineTo(465.572, 217.032);
        ((GeneralPath) shape).lineTo(465.868, 216.999);
        ((GeneralPath) shape).lineTo(466.164, 216.967);
        ((GeneralPath) shape).lineTo(466.462, 216.936);
        ((GeneralPath) shape).lineTo(466.761, 216.906);
        ((GeneralPath) shape).lineTo(467.061, 216.878);
        ((GeneralPath) shape).lineTo(467.363, 216.851);
        ((GeneralPath) shape).lineTo(467.666, 216.824);
        ((GeneralPath) shape).lineTo(467.969, 216.799);
        ((GeneralPath) shape).lineTo(468.275, 216.776);
        ((GeneralPath) shape).lineTo(468.581, 216.753);
        ((GeneralPath) shape).lineTo(468.889, 216.732);
        ((GeneralPath) shape).lineTo(469.198, 216.711);
        ((GeneralPath) shape).lineTo(469.508, 216.692);
        ((GeneralPath) shape).lineTo(469.819, 216.674);
        ((GeneralPath) shape).lineTo(470.132, 216.658);
        ((GeneralPath) shape).lineTo(470.446, 216.642);
        ((GeneralPath) shape).lineTo(470.761, 216.628);
        ((GeneralPath) shape).lineTo(471.078, 216.615);
        ((GeneralPath) shape).lineTo(471.396, 216.603);
        ((GeneralPath) shape).lineTo(471.715, 216.593);
        ((GeneralPath) shape).lineTo(472.035, 216.583);
        ((GeneralPath) shape).lineTo(472.356, 216.575);
        ((GeneralPath) shape).lineTo(472.626, 216.569);
        ((GeneralPath) shape).lineTo(472.729, 213.748);
        ((GeneralPath) shape).lineTo(472.925, 208.498);
        ((GeneralPath) shape).lineTo(473.144, 202.634);
        ((GeneralPath) shape).lineTo(473.383, 196.3);
        ((GeneralPath) shape).lineTo(473.635, 189.642);
        ((GeneralPath) shape).lineTo(473.894, 182.802);
        ((GeneralPath) shape).lineTo(474.413, 169.162);
        ((GeneralPath) shape).lineTo(474.894, 156.538);
        ((GeneralPath) shape).lineTo(475.294, 146.087);
        ((GeneralPath) shape).lineTo(475.67, 136.274);
        ((GeneralPath) shape).lineTo(475.682, 136.164);
        ((GeneralPath) shape).lineTo(475.69, 136.097);
        ((GeneralPath) shape).lineTo(475.702, 136.015);
        ((GeneralPath) shape).lineTo(475.718, 135.918);
        ((GeneralPath) shape).lineTo(475.737, 135.809);
        ((GeneralPath) shape).lineTo(475.762, 135.686);
        ((GeneralPath) shape).lineTo(475.792, 135.551);
        ((GeneralPath) shape).lineTo(475.826, 135.41);
        ((GeneralPath) shape).lineTo(475.848, 135.328);
        ((GeneralPath) shape).lineTo(475.87, 135.249);
        ((GeneralPath) shape).lineTo(475.894, 135.167);
        ((GeneralPath) shape).lineTo(475.92, 135.084);
        ((GeneralPath) shape).lineTo(475.948, 134.998);
        ((GeneralPath) shape).lineTo(475.978, 134.909);
        ((GeneralPath) shape).lineTo(476.01, 134.819);
        ((GeneralPath) shape).lineTo(476.044, 134.728);
        ((GeneralPath) shape).lineTo(476.081, 134.634);
        ((GeneralPath) shape).lineTo(476.12, 134.539);
        ((GeneralPath) shape).lineTo(476.162, 134.443);
        ((GeneralPath) shape).lineTo(476.207, 134.344);
        ((GeneralPath) shape).lineTo(476.254, 134.245);
        ((GeneralPath) shape).lineTo(476.305, 134.145);
        ((GeneralPath) shape).lineTo(476.359, 134.043);
        ((GeneralPath) shape).lineTo(476.416, 133.941);
        ((GeneralPath) shape).lineTo(476.477, 133.838);
        ((GeneralPath) shape).lineTo(476.541, 133.735);
        ((GeneralPath) shape).lineTo(476.609, 133.631);
        ((GeneralPath) shape).lineTo(476.682, 133.528);
        ((GeneralPath) shape).lineTo(476.758, 133.424);
        ((GeneralPath) shape).lineTo(476.839, 133.32);
        ((GeneralPath) shape).lineTo(476.924, 133.218);
        ((GeneralPath) shape).lineTo(477.014, 133.115);
        ((GeneralPath) shape).lineTo(477.109, 133.014);
        ((GeneralPath) shape).lineTo(477.208, 132.915);
        ((GeneralPath) shape).lineTo(477.313, 132.817);
        ((GeneralPath) shape).lineTo(477.416, 132.726);
        ((GeneralPath) shape).lineTo(477.479, 132.673);
        ((GeneralPath) shape).lineTo(477.537, 132.627);
        ((GeneralPath) shape).lineTo(477.596, 132.582);
        ((GeneralPath) shape).lineTo(477.657, 132.537);
        ((GeneralPath) shape).lineTo(477.719, 132.492);
        ((GeneralPath) shape).lineTo(477.782, 132.449);
        ((GeneralPath) shape).lineTo(477.847, 132.406);
        ((GeneralPath) shape).lineTo(477.912, 132.365);
        ((GeneralPath) shape).lineTo(477.979, 132.325);
        ((GeneralPath) shape).lineTo(478.048, 132.285);
        ((GeneralPath) shape).lineTo(478.117, 132.246);
        ((GeneralPath) shape).lineTo(478.188, 132.209);
        ((GeneralPath) shape).lineTo(478.26, 132.173);
        ((GeneralPath) shape).lineTo(478.333, 132.138);
        ((GeneralPath) shape).lineTo(478.407, 132.105);
        ((GeneralPath) shape).lineTo(478.482, 132.073);
        ((GeneralPath) shape).lineTo(478.558, 132.042);
        ((GeneralPath) shape).lineTo(478.636, 132.012);
        ((GeneralPath) shape).lineTo(478.714, 131.984);
        ((GeneralPath) shape).lineTo(478.794, 131.957);
        ((GeneralPath) shape).lineTo(478.874, 131.932);
        ((GeneralPath) shape).lineTo(478.955, 131.909);
        ((GeneralPath) shape).lineTo(479.037, 131.887);
        ((GeneralPath) shape).lineTo(479.12, 131.866);
        ((GeneralPath) shape).lineTo(479.203, 131.847);
        ((GeneralPath) shape).lineTo(479.288, 131.83);
        ((GeneralPath) shape).lineTo(479.373, 131.814);
        ((GeneralPath) shape).lineTo(479.459, 131.8);
        ((GeneralPath) shape).lineTo(479.545, 131.788);
        ((GeneralPath) shape).lineTo(479.632, 131.777);
        ((GeneralPath) shape).lineTo(479.719, 131.768);
        ((GeneralPath) shape).lineTo(479.807, 131.76);
        ((GeneralPath) shape).lineTo(479.896, 131.755);
        ((GeneralPath) shape).lineTo(479.985, 131.75);
        ((GeneralPath) shape).lineTo(480.075, 131.748);
        ((GeneralPath) shape).lineTo(480.165, 131.747);
        ((GeneralPath) shape).lineTo(480.255, 131.748);
        ((GeneralPath) shape).lineTo(480.344, 131.75);
        ((GeneralPath) shape).lineTo(480.433, 131.755);
        ((GeneralPath) shape).lineTo(480.522, 131.76);
        ((GeneralPath) shape).lineTo(480.61, 131.768);
        ((GeneralPath) shape).lineTo(480.698, 131.777);
        ((GeneralPath) shape).lineTo(480.785, 131.788);
        ((GeneralPath) shape).lineTo(480.871, 131.8);
        ((GeneralPath) shape).lineTo(480.957, 131.814);
        ((GeneralPath) shape).lineTo(481.042, 131.83);
        ((GeneralPath) shape).lineTo(481.126, 131.847);
        ((GeneralPath) shape).lineTo(481.21, 131.866);
        ((GeneralPath) shape).lineTo(481.293, 131.887);
        ((GeneralPath) shape).lineTo(481.375, 131.909);
        ((GeneralPath) shape).lineTo(481.456, 131.932);
        ((GeneralPath) shape).lineTo(481.536, 131.957);
        ((GeneralPath) shape).lineTo(481.616, 131.984);
        ((GeneralPath) shape).lineTo(481.694, 132.012);
        ((GeneralPath) shape).lineTo(481.771, 132.042);
        ((GeneralPath) shape).lineTo(481.848, 132.073);
        ((GeneralPath) shape).lineTo(481.923, 132.105);
        ((GeneralPath) shape).lineTo(481.997, 132.138);
        ((GeneralPath) shape).lineTo(482.07, 132.173);
        ((GeneralPath) shape).lineTo(482.142, 132.209);
        ((GeneralPath) shape).lineTo(482.213, 132.246);
        ((GeneralPath) shape).lineTo(482.282, 132.285);
        ((GeneralPath) shape).lineTo(482.35, 132.324);
        ((GeneralPath) shape).lineTo(482.417, 132.365);
        ((GeneralPath) shape).lineTo(482.483, 132.407);
        ((GeneralPath) shape).lineTo(482.548, 132.449);
        ((GeneralPath) shape).lineTo(482.611, 132.492);
        ((GeneralPath) shape).lineTo(482.673, 132.537);
        ((GeneralPath) shape).lineTo(482.733, 132.581);
        ((GeneralPath) shape).lineTo(482.793, 132.627);
        ((GeneralPath) shape).lineTo(482.851, 132.674);
        ((GeneralPath) shape).lineTo(482.913, 132.726);
        ((GeneralPath) shape).lineTo(483.017, 132.816);
        ((GeneralPath) shape).lineTo(483.122, 132.915);
        ((GeneralPath) shape).lineTo(483.221, 133.014);
        ((GeneralPath) shape).lineTo(483.316, 133.115);
        ((GeneralPath) shape).lineTo(483.405, 133.217);
        ((GeneralPath) shape).lineTo(483.491, 133.32);
        ((GeneralPath) shape).lineTo(483.572, 133.424);
        ((GeneralPath) shape).lineTo(483.648, 133.528);
        ((GeneralPath) shape).lineTo(483.72, 133.631);
        ((GeneralPath) shape).lineTo(483.789, 133.735);
        ((GeneralPath) shape).lineTo(483.853, 133.838);
        ((GeneralPath) shape).lineTo(483.914, 133.941);
        ((GeneralPath) shape).lineTo(483.971, 134.043);
        ((GeneralPath) shape).lineTo(484.025, 134.145);
        ((GeneralPath) shape).lineTo(484.075, 134.245);
        ((GeneralPath) shape).lineTo(484.123, 134.344);
        ((GeneralPath) shape).lineTo(484.168, 134.442);
        ((GeneralPath) shape).lineTo(484.21, 134.539);
        ((GeneralPath) shape).lineTo(484.249, 134.634);
        ((GeneralPath) shape).lineTo(484.286, 134.727);
        ((GeneralPath) shape).lineTo(484.32, 134.819);
        ((GeneralPath) shape).lineTo(484.352, 134.91);
        ((GeneralPath) shape).lineTo(484.382, 134.998);
        ((GeneralPath) shape).lineTo(484.41, 135.084);
        ((GeneralPath) shape).lineTo(484.436, 135.167);
        ((GeneralPath) shape).lineTo(484.459, 135.249);
        ((GeneralPath) shape).lineTo(484.482, 135.328);
        ((GeneralPath) shape).lineTo(484.503, 135.41);
        ((GeneralPath) shape).lineTo(484.538, 135.551);
        ((GeneralPath) shape).lineTo(484.568, 135.686);
        ((GeneralPath) shape).lineTo(484.592, 135.809);
        ((GeneralPath) shape).lineTo(484.612, 135.918);
        ((GeneralPath) shape).lineTo(484.628, 136.015);
        ((GeneralPath) shape).lineTo(484.639, 136.097);
        ((GeneralPath) shape).lineTo(484.648, 136.164);
        ((GeneralPath) shape).lineTo(484.66, 136.274);
        ((GeneralPath) shape).lineTo(485.036, 146.087);
        ((GeneralPath) shape).lineTo(485.435, 156.538);
        ((GeneralPath) shape).lineTo(485.917, 169.162);
        ((GeneralPath) shape).lineTo(486.436, 182.802);
        ((GeneralPath) shape).lineTo(486.695, 189.642);
        ((GeneralPath) shape).lineTo(486.947, 196.3);
        ((GeneralPath) shape).lineTo(487.185, 202.634);
        ((GeneralPath) shape).lineTo(487.405, 208.498);
        ((GeneralPath) shape).lineTo(487.6, 213.748);
        ((GeneralPath) shape).lineTo(487.704, 216.569);
        ((GeneralPath) shape).lineTo(487.973, 216.575);
        ((GeneralPath) shape).lineTo(488.295, 216.583);
        ((GeneralPath) shape).lineTo(488.615, 216.593);
        ((GeneralPath) shape).lineTo(488.934, 216.603);
        ((GeneralPath) shape).lineTo(489.252, 216.615);
        ((GeneralPath) shape).lineTo(489.568, 216.628);
        ((GeneralPath) shape).lineTo(489.884, 216.642);
        ((GeneralPath) shape).lineTo(490.198, 216.658);
        ((GeneralPath) shape).lineTo(490.51, 216.674);
        ((GeneralPath) shape).lineTo(490.822, 216.692);
        ((GeneralPath) shape).lineTo(491.132, 216.711);
        ((GeneralPath) shape).lineTo(491.441, 216.732);
        ((GeneralPath) shape).lineTo(491.749, 216.753);
        ((GeneralPath) shape).lineTo(492.055, 216.776);
        ((GeneralPath) shape).lineTo(492.36, 216.799);
        ((GeneralPath) shape).lineTo(492.664, 216.824);
        ((GeneralPath) shape).lineTo(492.967, 216.851);
        ((GeneralPath) shape).lineTo(493.268, 216.878);
        ((GeneralPath) shape).lineTo(493.569, 216.906);
        ((GeneralPath) shape).lineTo(493.868, 216.936);
        ((GeneralPath) shape).lineTo(494.165, 216.967);
        ((GeneralPath) shape).lineTo(494.462, 216.999);
        ((GeneralPath) shape).lineTo(494.757, 217.032);
        ((GeneralPath) shape).lineTo(495.051, 217.066);
        ((GeneralPath) shape).lineTo(495.344, 217.101);
        ((GeneralPath) shape).lineTo(495.636, 217.138);
        ((GeneralPath) shape).lineTo(495.926, 217.176);
        ((GeneralPath) shape).lineTo(496.216, 217.214);
        ((GeneralPath) shape).lineTo(496.504, 217.254);
        ((GeneralPath) shape).lineTo(496.79, 217.295);
        ((GeneralPath) shape).lineTo(497.076, 217.337);
        ((GeneralPath) shape).lineTo(497.361, 217.38);
        ((GeneralPath) shape).lineTo(497.644, 217.425);
        ((GeneralPath) shape).lineTo(497.926, 217.47);
        ((GeneralPath) shape).lineTo(498.207, 217.517);
        ((GeneralPath) shape).lineTo(498.486, 217.564);
        ((GeneralPath) shape).lineTo(498.765, 217.613);
        ((GeneralPath) shape).lineTo(499.042, 217.662);
        ((GeneralPath) shape).lineTo(499.318, 217.713);
        ((GeneralPath) shape).lineTo(499.593, 217.765);
        ((GeneralPath) shape).lineTo(499.866, 217.818);
        ((GeneralPath) shape).lineTo(500.139, 217.872);
        ((GeneralPath) shape).lineTo(500.41, 217.927);
        ((GeneralPath) shape).lineTo(500.68, 217.983);
        ((GeneralPath) shape).lineTo(500.949, 218.04);
        ((GeneralPath) shape).lineTo(501.217, 218.098);
        ((GeneralPath) shape).lineTo(501.484, 218.157);
        ((GeneralPath) shape).lineTo(501.749, 218.217);
        ((GeneralPath) shape).lineTo(502.013, 218.279);
        ((GeneralPath) shape).lineTo(502.277, 218.341);
        ((GeneralPath) shape).lineTo(502.538, 218.404);
        ((GeneralPath) shape).lineTo(502.799, 218.468);
        ((GeneralPath) shape).lineTo(503.059, 218.534);
        ((GeneralPath) shape).lineTo(503.317, 218.6);
        ((GeneralPath) shape).lineTo(503.575, 218.667);
        ((GeneralPath) shape).lineTo(503.831, 218.736);
        ((GeneralPath) shape).lineTo(504.086, 218.805);
        ((GeneralPath) shape).lineTo(504.34, 218.875);
        ((GeneralPath) shape).lineTo(504.593, 218.946);
        ((GeneralPath) shape).lineTo(504.844, 219.018);
        ((GeneralPath) shape).lineTo(505.095, 219.092);
        ((GeneralPath) shape).lineTo(505.344, 219.166);
        ((GeneralPath) shape).lineTo(505.592, 219.241);
        ((GeneralPath) shape).lineTo(505.839, 219.317);
        ((GeneralPath) shape).lineTo(506.085, 219.394);
        ((GeneralPath) shape).lineTo(506.33, 219.472);
        ((GeneralPath) shape).lineTo(506.574, 219.551);
        ((GeneralPath) shape).lineTo(506.816, 219.63);
        ((GeneralPath) shape).lineTo(507.058, 219.711);
        ((GeneralPath) shape).lineTo(507.298, 219.793);
        ((GeneralPath) shape).lineTo(507.537, 219.875);
        ((GeneralPath) shape).lineTo(507.775, 219.959);
        ((GeneralPath) shape).lineTo(508.012, 220.043);
        ((GeneralPath) shape).lineTo(508.248, 220.128);
        ((GeneralPath) shape).lineTo(508.483, 220.215);
        ((GeneralPath) shape).lineTo(508.716, 220.302);
        ((GeneralPath) shape).lineTo(508.949, 220.39);
        ((GeneralPath) shape).lineTo(509.18, 220.479);
        ((GeneralPath) shape).lineTo(509.411, 220.568);
        ((GeneralPath) shape).lineTo(509.64, 220.659);
        ((GeneralPath) shape).lineTo(509.868, 220.751);
        ((GeneralPath) shape).lineTo(510.095, 220.843);
        ((GeneralPath) shape).lineTo(510.321, 220.936);
        ((GeneralPath) shape).lineTo(510.546, 221.03);
        ((GeneralPath) shape).lineTo(510.769, 221.125);
        ((GeneralPath) shape).lineTo(510.992, 221.221);
        ((GeneralPath) shape).lineTo(511.214, 221.318);
        ((GeneralPath) shape).lineTo(511.434, 221.415);
        ((GeneralPath) shape).lineTo(511.654, 221.513);
        ((GeneralPath) shape).lineTo(511.872, 221.613);
        ((GeneralPath) shape).lineTo(512.089, 221.712);
        ((GeneralPath) shape).lineTo(512.305, 221.813);
        ((GeneralPath) shape).lineTo(512.52, 221.915);
        ((GeneralPath) shape).lineTo(512.734, 222.017);
        ((GeneralPath) shape).lineTo(512.947, 222.12);
        ((GeneralPath) shape).lineTo(513.159, 222.224);
        ((GeneralPath) shape).lineTo(513.37, 222.329);
        ((GeneralPath) shape).lineTo(513.58, 222.435);
        ((GeneralPath) shape).lineTo(513.789, 222.541);
        ((GeneralPath) shape).lineTo(513.996, 222.648);
        ((GeneralPath) shape).lineTo(514.203, 222.756);
        ((GeneralPath) shape).lineTo(514.408, 222.865);
        ((GeneralPath) shape).lineTo(514.613, 222.974);
        ((GeneralPath) shape).lineTo(514.816, 223.084);
        ((GeneralPath) shape).lineTo(515.019, 223.195);
        ((GeneralPath) shape).lineTo(515.22, 223.307);
        ((GeneralPath) shape).lineTo(515.42, 223.419);
        ((GeneralPath) shape).lineTo(515.62, 223.533);
        ((GeneralPath) shape).lineTo(515.818, 223.646);
        ((GeneralPath) shape).lineTo(516.015, 223.761);
        ((GeneralPath) shape).lineTo(516.211, 223.876);
        ((GeneralPath) shape).lineTo(516.406, 223.993);
        ((GeneralPath) shape).lineTo(516.6, 224.109);
        ((GeneralPath) shape).lineTo(516.793, 224.227);
        ((GeneralPath) shape).lineTo(516.985, 224.345);
        ((GeneralPath) shape).lineTo(517.176, 224.464);
        ((GeneralPath) shape).lineTo(517.366, 224.583);
        ((GeneralPath) shape).lineTo(517.555, 224.703);
        ((GeneralPath) shape).lineTo(517.743, 224.824);
        ((GeneralPath) shape).lineTo(517.93, 224.946);
        ((GeneralPath) shape).lineTo(518.116, 225.068);
        ((GeneralPath) shape).lineTo(518.301, 225.191);
        ((GeneralPath) shape).lineTo(518.485, 225.315);
        ((GeneralPath) shape).lineTo(518.668, 225.439);
        ((GeneralPath) shape).lineTo(518.85, 225.564);
        ((GeneralPath) shape).lineTo(519.03, 225.689);
        ((GeneralPath) shape).lineTo(519.21, 225.815);
        ((GeneralPath) shape).lineTo(519.389, 225.942);
        ((GeneralPath) shape).lineTo(519.567, 226.069);
        ((GeneralPath) shape).lineTo(519.744, 226.198);
        ((GeneralPath) shape).lineTo(519.92, 226.326);
        ((GeneralPath) shape).lineTo(520.095, 226.455);
        ((GeneralPath) shape).lineTo(520.269, 226.585);
        ((GeneralPath) shape).lineTo(520.442, 226.716);
        ((GeneralPath) shape).lineTo(520.614, 226.847);
        ((GeneralPath) shape).lineTo(520.784, 226.978);
        ((GeneralPath) shape).lineTo(520.954, 227.11);
        ((GeneralPath) shape).lineTo(521.124, 227.243);
        ((GeneralPath) shape).lineTo(521.292, 227.377);
        ((GeneralPath) shape).lineTo(521.459, 227.511);
        ((GeneralPath) shape).lineTo(521.625, 227.645);
        ((GeneralPath) shape).lineTo(521.79, 227.78);
        ((GeneralPath) shape).lineTo(521.954, 227.916);
        ((GeneralPath) shape).lineTo(522.117, 228.052);
        ((GeneralPath) shape).lineTo(522.28, 228.188);
        ((GeneralPath) shape).lineTo(522.441, 228.326);
        ((GeneralPath) shape).lineTo(522.601, 228.463);
        ((GeneralPath) shape).lineTo(522.761, 228.602);
        ((GeneralPath) shape).lineTo(522.919, 228.74);
        ((GeneralPath) shape).lineTo(523.077, 228.88);
        ((GeneralPath) shape).lineTo(523.233, 229.019);
        ((GeneralPath) shape).lineTo(523.389, 229.16);
        ((GeneralPath) shape).lineTo(523.543, 229.3);
        ((GeneralPath) shape).lineTo(523.697, 229.442);
        ((GeneralPath) shape).lineTo(523.85, 229.584);
        ((GeneralPath) shape).lineTo(524.002, 229.726);
        ((GeneralPath) shape).lineTo(524.153, 229.869);
        ((GeneralPath) shape).lineTo(524.303, 230.012);
        ((GeneralPath) shape).lineTo(524.452, 230.155);
        ((GeneralPath) shape).lineTo(524.6, 230.3);
        ((GeneralPath) shape).lineTo(524.747, 230.444);
        ((GeneralPath) shape).lineTo(524.894, 230.589);
        ((GeneralPath) shape).lineTo(525.039, 230.735);
        ((GeneralPath) shape).lineTo(525.184, 230.88);
        ((GeneralPath) shape).lineTo(525.327, 231.027);
        ((GeneralPath) shape).lineTo(525.47, 231.174);
        ((GeneralPath) shape).lineTo(525.612, 231.321);
        ((GeneralPath) shape).lineTo(525.753, 231.468);
        ((GeneralPath) shape).lineTo(525.893, 231.616);
        ((GeneralPath) shape).lineTo(526.032, 231.765);
        ((GeneralPath) shape).lineTo(526.17, 231.914);
        ((GeneralPath) shape).lineTo(526.307, 232.063);
        ((GeneralPath) shape).lineTo(526.444, 232.212);
        ((GeneralPath) shape).lineTo(526.579, 232.362);
        ((GeneralPath) shape).lineTo(526.714, 232.513);
        ((GeneralPath) shape).lineTo(526.848, 232.663);
        ((GeneralPath) shape).lineTo(526.981, 232.814);
        ((GeneralPath) shape).lineTo(527.113, 232.966);
        ((GeneralPath) shape).lineTo(527.244, 233.118);
        ((GeneralPath) shape).lineTo(527.375, 233.27);
        ((GeneralPath) shape).lineTo(527.504, 233.422);
        ((GeneralPath) shape).lineTo(527.632, 233.575);
        ((GeneralPath) shape).lineTo(527.76, 233.728);
        ((GeneralPath) shape).lineTo(527.887, 233.882);
        ((GeneralPath) shape).lineTo(528.013, 234.036);
        ((GeneralPath) shape).lineTo(528.138, 234.19);
        ((GeneralPath) shape).lineTo(528.263, 234.344);
        ((GeneralPath) shape).lineTo(528.386, 234.499);
        ((GeneralPath) shape).lineTo(528.509, 234.654);
        ((GeneralPath) shape).lineTo(528.631, 234.81);
        ((GeneralPath) shape).lineTo(528.751, 234.965);
        ((GeneralPath) shape).lineTo(528.872, 235.121);
        ((GeneralPath) shape).lineTo(528.991, 235.278);
        ((GeneralPath) shape).lineTo(529.109, 235.434);
        ((GeneralPath) shape).lineTo(529.227, 235.591);
        ((GeneralPath) shape).lineTo(529.344, 235.748);
        ((GeneralPath) shape).lineTo(529.46, 235.905);
        ((GeneralPath) shape).lineTo(529.575, 236.063);
        ((GeneralPath) shape).lineTo(529.689, 236.221);
        ((GeneralPath) shape).lineTo(529.803, 236.379);
        ((GeneralPath) shape).lineTo(529.916, 236.537);
        ((GeneralPath) shape).lineTo(530.028, 236.696);
        ((GeneralPath) shape).lineTo(530.139, 236.855);
        ((GeneralPath) shape).lineTo(530.249, 237.014);
        ((GeneralPath) shape).lineTo(530.359, 237.174);
        ((GeneralPath) shape).lineTo(530.575, 237.492);
        ((GeneralPath) shape).lineTo(530.789, 237.812);
        ((GeneralPath) shape).lineTo(530.999, 238.133);
        ((GeneralPath) shape).lineTo(531.206, 238.454);
        ((GeneralPath) shape).lineTo(531.411, 238.776);
        ((GeneralPath) shape).lineTo(531.612, 239.099);
        ((GeneralPath) shape).lineTo(531.81, 239.423);
        ((GeneralPath) shape).lineTo(532.005, 239.747);
        ((GeneralPath) shape).lineTo(532.197, 240.071);
        ((GeneralPath) shape).lineTo(532.386, 240.396);
        ((GeneralPath) shape).lineTo(532.572, 240.722);
        ((GeneralPath) shape).lineTo(532.755, 241.048);
        ((GeneralPath) shape).lineTo(532.936, 241.374);
        ((GeneralPath) shape).lineTo(533.113, 241.701);
        ((GeneralPath) shape).lineTo(533.288, 242.027);
        ((GeneralPath) shape).lineTo(533.46, 242.355);
        ((GeneralPath) shape).lineTo(533.629, 242.682);
        ((GeneralPath) shape).lineTo(533.795, 243.009);
        ((GeneralPath) shape).lineTo(533.958, 243.337);
        ((GeneralPath) shape).lineTo(534.119, 243.665);
        ((GeneralPath) shape).lineTo(534.277, 243.993);
        ((GeneralPath) shape).lineTo(534.432, 244.321);
        ((GeneralPath) shape).lineTo(534.585, 244.648);
        ((GeneralPath) shape).lineTo(534.735, 244.976);
        ((GeneralPath) shape).lineTo(534.882, 245.304);
        ((GeneralPath) shape).lineTo(535.027, 245.631);
        ((GeneralPath) shape).lineTo(535.169, 245.958);
        ((GeneralPath) shape).lineTo(535.309, 246.285);
        ((GeneralPath) shape).lineTo(535.446, 246.612);
        ((GeneralPath) shape).lineTo(535.581, 246.939);
        ((GeneralPath) shape).lineTo(535.713, 247.265);
        ((GeneralPath) shape).lineTo(535.843, 247.591);
        ((GeneralPath) shape).lineTo(535.97, 247.916);
        ((GeneralPath) shape).lineTo(536.095, 248.241);
        ((GeneralPath) shape).lineTo(536.218, 248.565);
        ((GeneralPath) shape).lineTo(536.338, 248.889);
        ((GeneralPath) shape).lineTo(536.456, 249.212);
        ((GeneralPath) shape).lineTo(536.571, 249.535);
        ((GeneralPath) shape).lineTo(536.684, 249.857);
        ((GeneralPath) shape).lineTo(536.795, 250.178);
        ((GeneralPath) shape).lineTo(536.904, 250.499);
        ((GeneralPath) shape).lineTo(537.011, 250.819);
        ((GeneralPath) shape).lineTo(537.115, 251.137);
        ((GeneralPath) shape).lineTo(537.217, 251.456);
        ((GeneralPath) shape).lineTo(537.317, 251.773);
        ((GeneralPath) shape).lineTo(537.415, 252.089);
        ((GeneralPath) shape).lineTo(537.511, 252.405);
        ((GeneralPath) shape).lineTo(537.605, 252.719);
        ((GeneralPath) shape).lineTo(537.696, 253.032);
        ((GeneralPath) shape).lineTo(537.786, 253.344);
        ((GeneralPath) shape).lineTo(537.873, 253.656);
        ((GeneralPath) shape).lineTo(537.959, 253.965);
        ((GeneralPath) shape).lineTo(538.043, 254.274);
        ((GeneralPath) shape).lineTo(538.124, 254.582);
        ((GeneralPath) shape).lineTo(538.204, 254.888);
        ((GeneralPath) shape).lineTo(538.282, 255.193);
        ((GeneralPath) shape).lineTo(538.358, 255.497);
        ((GeneralPath) shape).lineTo(538.432, 255.799);
        ((GeneralPath) shape).lineTo(538.505, 256.099);
        ((GeneralPath) shape).lineTo(538.575, 256.399);
        ((GeneralPath) shape).lineTo(538.644, 256.697);
        ((GeneralPath) shape).lineTo(538.711, 256.993);
        ((GeneralPath) shape).lineTo(538.776, 257.287);
        ((GeneralPath) shape).lineTo(538.84, 257.581);
        ((GeneralPath) shape).lineTo(538.902, 257.872);
        ((GeneralPath) shape).lineTo(538.962, 258.162);
        ((GeneralPath) shape).lineTo(539.021, 258.45);
        ((GeneralPath) shape).lineTo(539.077, 258.736);
        ((GeneralPath) shape).lineTo(539.133, 259.02);
        ((GeneralPath) shape).lineTo(539.187, 259.303);
        ((GeneralPath) shape).lineTo(539.239, 259.584);
        ((GeneralPath) shape).lineTo(539.289, 259.862);
        ((GeneralPath) shape).lineTo(539.339, 260.139);
        ((GeneralPath) shape).lineTo(539.386, 260.414);
        ((GeneralPath) shape).lineTo(539.433, 260.687);
        ((GeneralPath) shape).lineTo(539.477, 260.957);
        ((GeneralPath) shape).lineTo(539.521, 261.226);
        ((GeneralPath) shape).lineTo(539.563, 261.492);
        ((GeneralPath) shape).lineTo(539.603, 261.756);
        ((GeneralPath) shape).lineTo(539.643, 262.019);
        ((GeneralPath) shape).lineTo(539.68, 262.278);
        ((GeneralPath) shape).lineTo(539.717, 262.536);
        ((GeneralPath) shape).lineTo(539.752, 262.791);
        ((GeneralPath) shape).lineTo(539.786, 263.043);
        ((GeneralPath) shape).lineTo(539.819, 263.294);
        ((GeneralPath) shape).lineTo(539.851, 263.542);
        ((GeneralPath) shape).lineTo(539.881, 263.787);
        ((GeneralPath) shape).lineTo(539.911, 264.03);
        ((GeneralPath) shape).lineTo(539.939, 264.27);
        ((GeneralPath) shape).lineTo(539.966, 264.507);
        ((GeneralPath) shape).lineTo(539.991, 264.742);
        ((GeneralPath) shape).lineTo(540.016, 264.974);
        ((GeneralPath) shape).lineTo(540.04, 265.206);
        ((GeneralPath) shape).lineTo(540.084, 265.655);
        ((GeneralPath) shape).lineTo(540.125, 266.094);
        ((GeneralPath) shape).lineTo(540.161, 266.522);
        ((GeneralPath) shape).lineTo(540.194, 266.938);
        ((GeneralPath) shape).lineTo(540.224, 267.341);
        ((GeneralPath) shape).lineTo(540.25, 267.732);
        ((GeneralPath) shape).lineTo(540.273, 268.11);
        ((GeneralPath) shape).lineTo(540.293, 268.475);
        ((GeneralPath) shape).lineTo(540.31, 268.826);
        ((GeneralPath) shape).lineTo(540.325, 269.164);
        ((GeneralPath) shape).lineTo(540.338, 269.487);
        ((GeneralPath) shape).lineTo(540.348, 269.796);
        ((GeneralPath) shape).lineTo(540.356, 270.09);
        ((GeneralPath) shape).lineTo(540.362, 270.369);
        ((GeneralPath) shape).lineTo(540.367, 270.633);
        ((GeneralPath) shape).lineTo(540.368, 270.711);
        ((GeneralPath) shape).lineTo(611.849, 270.711);
        ((GeneralPath) shape).lineTo(612.144, 270.757);
        ((GeneralPath) shape).lineTo(612.339, 270.789);
        ((GeneralPath) shape).closePath();
        paint2(g, origAlpha, transformations);
    }

    private static void paint2(Graphics2D g, float origAlpha, java.util.LinkedList<AffineTransform> transformations) {
        Shape shape = null;

        g.setPaint(new Color(0x80FFFFFF, true));
        g.fill(shape);

        // _0_1_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(475.844, 105.592);
        ((GeneralPath) shape).lineTo(475.844, 110.46);
        ((GeneralPath) shape).lineTo(484.592, 110.46);
        ((GeneralPath) shape).lineTo(484.592, 105.592);

        g.setPaint(WHITE);
        g.setStroke(new BasicStroke(1.9f, 0, 0, 4));
        g.draw(shape);

        // _0_1_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(441.385, 105.592);
        ((GeneralPath) shape).lineTo(441.385, 110.72);

        g.draw(shape);

        // _0_1_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(519.051, 105.592);
        ((GeneralPath) shape).lineTo(519.051, 110.72);

        g.draw(shape);

        // _0_1_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(909.337, 181.426);
        ((GeneralPath) shape).lineTo(904.474, 181.426);
        ((GeneralPath) shape).lineTo(904.474, 190.182);
        ((GeneralPath) shape).lineTo(909.337, 190.182);

        g.draw(shape);

        // _0_1_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(909.337, 146.933);
        ((GeneralPath) shape).lineTo(904.214, 146.933);

        g.draw(shape);

        // _0_1_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(909.337, 224.675);
        ((GeneralPath) shape).lineTo(904.214, 224.675);

        g.draw(shape);

        // _0_1_7
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(52.552, 181.426);
        ((GeneralPath) shape).lineTo(57.415, 181.426);
        ((GeneralPath) shape).lineTo(57.415, 190.182);
        ((GeneralPath) shape).lineTo(52.552, 190.182);

        g.draw(shape);

        // _0_1_8
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(52.552, 146.933);
        ((GeneralPath) shape).lineTo(57.675, 146.933);

        g.draw(shape);

        // _0_1_9
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(52.552, 224.675);
        ((GeneralPath) shape).lineTo(57.675, 224.675);

        g.draw(shape);

        // _0_1_10
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(306.563, 195.164);
        ((GeneralPath) shape).lineTo(311.426, 195.164);
        ((GeneralPath) shape).lineTo(311.426, 203.92);
        ((GeneralPath) shape).lineTo(306.563, 203.92);

        g.draw(shape);

        // _0_1_11
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(306.563, 160.671);
        ((GeneralPath) shape).lineTo(311.686, 160.671);

        g.draw(shape);

        // _0_1_12
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(306.563, 238.413);
        ((GeneralPath) shape).lineTo(311.686, 238.413);

        g.draw(shape);

        // _0_1_13
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(519.179, 430.265);
        ((GeneralPath) shape).lineTo(439.477, 430.265);

        g.setStroke(new BasicStroke(2, 0, 0, 4));
        g.draw(shape);

        // _0_1_14
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(519.051, 424.056);
        ((GeneralPath) shape).lineTo(519.051, 436.705);

        g.draw(shape);

        // _0_1_15
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(439.237, 424.056);
        ((GeneralPath) shape).lineTo(439.237, 436.705);

        g.draw(shape);

        // _0_1_16
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(640.229, 157.862);
        ((GeneralPath) shape).lineTo(640.229, 237.642);

        g.draw(shape);

        // _0_1_17
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(634.025, 158.884);
        ((GeneralPath) shape).lineTo(646.662, 158.884);

        g.setStroke(new BasicStroke(1.9f, 0, 0, 4));
        g.draw(shape);

        // _0_1_18
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(634.025, 238.775);
        ((GeneralPath) shape).lineTo(646.662, 238.775);

        g.draw(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 0, 0));

        // _0_1_19
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 106.378f, 133.433f));

        // _0_1_19_0

        // _0_1_19_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.656, -1.739);
        ((GeneralPath) shape).lineTo(5.656, 0.202);
        ((GeneralPath) shape).lineTo(-5.605, 0.202);
        ((GeneralPath) shape).lineTo(-5.605, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -1.739);
        ((GeneralPath) shape).lineTo(5.656, -1.739);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_19_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 106.378f, 159.311f));

        // _0_1_19_1

        // _0_1_19_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-13.49, -12.939);
        ((GeneralPath) shape).lineTo(-17.463, -6.348);
        ((GeneralPath) shape).lineTo(-9.75, -6.348);
        ((GeneralPath) shape).lineTo(-13.49, -12.939);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-14.339, -15.567);
        ((GeneralPath) shape).lineTo(-12.853, -15.567);
        ((GeneralPath) shape).lineTo(-7.566, -6.045);
        ((GeneralPath) shape).lineTo(-7.566, 0.202);
        ((GeneralPath) shape).lineTo(-9.75, 0.202);
        ((GeneralPath) shape).lineTo(-9.75, -4.407);
        ((GeneralPath) shape).lineTo(-17.463, -4.407);
        ((GeneralPath) shape).lineTo(-17.463, 0.202);
        ((GeneralPath) shape).lineTo(-19.646, 0.202);
        ((GeneralPath) shape).lineTo(-19.646, -5.994);
        ((GeneralPath) shape).lineTo(-14.339, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_19_1_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-1.016, -13.95);
        ((GeneralPath) shape).lineTo(-4.423, -13.95);
        ((GeneralPath) shape).lineTo(-4.423, -15.567);
        ((GeneralPath) shape).lineTo(4.463, -15.567);
        ((GeneralPath) shape).lineTo(4.463, -13.95);
        ((GeneralPath) shape).lineTo(1.026, -13.95);
        ((GeneralPath) shape).lineTo(1.026, -1.557);
        ((GeneralPath) shape).lineTo(4.463, -1.557);
        ((GeneralPath) shape).lineTo(4.463, 0.202);
        ((GeneralPath) shape).lineTo(-4.423, 0.202);
        ((GeneralPath) shape).lineTo(-4.423, -1.557);
        ((GeneralPath) shape).lineTo(-1.016, -1.557);
        ((GeneralPath) shape).lineTo(-1.016, -13.95);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_19_1_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(19.252, -1.739);
        ((GeneralPath) shape).lineTo(19.252, 0.202);
        ((GeneralPath) shape).lineTo(7.991, 0.202);
        ((GeneralPath) shape).lineTo(7.991, -15.567);
        ((GeneralPath) shape).lineTo(10.033, -15.567);
        ((GeneralPath) shape).lineTo(10.033, -1.739);
        ((GeneralPath) shape).lineTo(19.252, -1.739);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_19_1

        g.setTransform(transformations.pop()); // _0_1_19
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 0, 0));

        // _0_1_20
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 860.54f, 133.433f));

        // _0_1_20_0

        // _0_1_20_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-5.787, -15.567);
        ((GeneralPath) shape).lineTo(2.593, -15.567);
        ((GeneralPath) shape).lineTo(5.474, -12.686);
        ((GeneralPath) shape).lineTo(5.474, -9.593);
        ((GeneralPath) shape).lineTo(2.593, -6.712);
        ((GeneralPath) shape).lineTo(1.703, -6.712);
        ((GeneralPath) shape).lineTo(5.828, -0.505);
        ((GeneralPath) shape).lineTo(4.16, 0.596);
        ((GeneralPath) shape).lineTo(-0.713, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-3.745, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -8.653);
        ((GeneralPath) shape).lineTo(1.703, -8.653);
        ((GeneralPath) shape).lineTo(3.432, -10.382);
        ((GeneralPath) shape).lineTo(3.432, -11.898);
        ((GeneralPath) shape).lineTo(1.703, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_20_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 860.54f, 159.311f));

        // _0_1_20_1

        // _0_1_20_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-13.49, -12.939);
        ((GeneralPath) shape).lineTo(-17.463, -6.348);
        ((GeneralPath) shape).lineTo(-9.75, -6.348);
        ((GeneralPath) shape).lineTo(-13.49, -12.939);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-14.339, -15.567);
        ((GeneralPath) shape).lineTo(-12.853, -15.567);
        ((GeneralPath) shape).lineTo(-7.566, -6.045);
        ((GeneralPath) shape).lineTo(-7.566, 0.202);
        ((GeneralPath) shape).lineTo(-9.75, 0.202);
        ((GeneralPath) shape).lineTo(-9.75, -4.407);
        ((GeneralPath) shape).lineTo(-17.463, -4.407);
        ((GeneralPath) shape).lineTo(-17.463, 0.202);
        ((GeneralPath) shape).lineTo(-19.646, 0.202);
        ((GeneralPath) shape).lineTo(-19.646, -5.994);
        ((GeneralPath) shape).lineTo(-14.339, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_20_1_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-1.016, -13.95);
        ((GeneralPath) shape).lineTo(-4.423, -13.95);
        ((GeneralPath) shape).lineTo(-4.423, -15.567);
        ((GeneralPath) shape).lineTo(4.463, -15.567);
        ((GeneralPath) shape).lineTo(4.463, -13.95);
        ((GeneralPath) shape).lineTo(1.026, -13.95);
        ((GeneralPath) shape).lineTo(1.026, -1.557);
        ((GeneralPath) shape).lineTo(4.463, -1.557);
        ((GeneralPath) shape).lineTo(4.463, 0.202);
        ((GeneralPath) shape).lineTo(-4.423, 0.202);
        ((GeneralPath) shape).lineTo(-4.423, -1.557);
        ((GeneralPath) shape).lineTo(-1.016, -1.557);
        ((GeneralPath) shape).lineTo(-1.016, -13.95);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_20_1_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(19.252, -1.739);
        ((GeneralPath) shape).lineTo(19.252, 0.202);
        ((GeneralPath) shape).lineTo(7.991, 0.202);
        ((GeneralPath) shape).lineTo(7.991, -15.567);
        ((GeneralPath) shape).lineTo(10.033, -15.567);
        ((GeneralPath) shape).lineTo(10.033, -1.739);
        ((GeneralPath) shape).lineTo(19.252, -1.739);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_20_1

        g.setTransform(transformations.pop()); // _0_1_20
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 0, 0));

        // _0_1_21
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 579.578f, 192.583f));

        // _0_1_21_0

        // _0_1_21_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-32.797, -15.567);
        ((GeneralPath) shape).lineTo(-24.417, -15.567);
        ((GeneralPath) shape).lineTo(-21.536, -12.686);
        ((GeneralPath) shape).lineTo(-21.536, -9.593);
        ((GeneralPath) shape).lineTo(-24.417, -6.712);
        ((GeneralPath) shape).lineTo(-30.756, -6.712);
        ((GeneralPath) shape).lineTo(-30.756, 0.202);
        ((GeneralPath) shape).lineTo(-32.797, 0.202);
        ((GeneralPath) shape).lineTo(-32.797, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-30.756, -13.626);
        ((GeneralPath) shape).lineTo(-30.756, -8.653);
        ((GeneralPath) shape).lineTo(-25.307, -8.653);
        ((GeneralPath) shape).lineTo(-23.578, -10.382);
        ((GeneralPath) shape).lineTo(-23.578, -11.898);
        ((GeneralPath) shape).lineTo(-25.307, -13.626);
        ((GeneralPath) shape).lineTo(-30.756, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_21_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-14.612, -13.95);
        ((GeneralPath) shape).lineTo(-18.019, -13.95);
        ((GeneralPath) shape).lineTo(-18.019, -15.567);
        ((GeneralPath) shape).lineTo(-9.133, -15.567);
        ((GeneralPath) shape).lineTo(-9.133, -13.95);
        ((GeneralPath) shape).lineTo(-12.57, -13.95);
        ((GeneralPath) shape).lineTo(-12.57, -1.557);
        ((GeneralPath) shape).lineTo(-9.133, -1.557);
        ((GeneralPath) shape).lineTo(-9.133, 0.202);
        ((GeneralPath) shape).lineTo(-18.019, 0.202);
        ((GeneralPath) shape).lineTo(-18.019, -1.557);
        ((GeneralPath) shape).lineTo(-14.612, -1.557);
        ((GeneralPath) shape).lineTo(-14.612, -13.95);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_21_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-0.996, 0.202);
        ((GeneralPath) shape).lineTo(-0.996, -13.626);
        ((GeneralPath) shape).lineTo(-5.605, -13.626);
        ((GeneralPath) shape).lineTo(-5.605, -15.567);
        ((GeneralPath) shape).lineTo(5.656, -15.567);
        ((GeneralPath) shape).lineTo(5.656, -13.626);
        ((GeneralPath) shape).lineTo(1.046, -13.626);
        ((GeneralPath) shape).lineTo(1.046, 0.202);
        ((GeneralPath) shape).lineTo(-0.996, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_21_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.766, -11.423);
        ((GeneralPath) shape).lineTo(15.562, -13.626);
        ((GeneralPath) shape).lineTo(11.842, -13.626);
        ((GeneralPath) shape).lineTo(10.114, -11.898);
        ((GeneralPath) shape).lineTo(10.114, -3.467);
        ((GeneralPath) shape).lineTo(11.842, -1.739);
        ((GeneralPath) shape).lineTo(15.562, -1.739);
        ((GeneralPath) shape).lineTo(17.766, -3.942);
        ((GeneralPath) shape).lineTo(19.181, -2.527);
        ((GeneralPath) shape).lineTo(16.452, 0.202);
        ((GeneralPath) shape).lineTo(10.953, 0.202);
        ((GeneralPath) shape).lineTo(8.072, -2.679);
        ((GeneralPath) shape).lineTo(8.072, -12.686);
        ((GeneralPath) shape).lineTo(10.953, -15.567);
        ((GeneralPath) shape).lineTo(16.452, -15.567);
        ((GeneralPath) shape).lineTo(19.181, -12.838);
        ((GeneralPath) shape).lineTo(17.766, -11.423);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_21_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(21.587, -15.567);
        ((GeneralPath) shape).lineTo(23.629, -15.567);
        ((GeneralPath) shape).lineTo(23.629, -8.653);
        ((GeneralPath) shape).lineTo(30.806, -8.653);
        ((GeneralPath) shape).lineTo(30.806, -15.567);
        ((GeneralPath) shape).lineTo(32.848, -15.567);
        ((GeneralPath) shape).lineTo(32.848, 0.202);
        ((GeneralPath) shape).lineTo(30.806, 0.202);
        ((GeneralPath) shape).lineTo(30.806, -6.712);
        ((GeneralPath) shape).lineTo(23.629, -6.712);
        ((GeneralPath) shape).lineTo(23.629, 0.202);
        ((GeneralPath) shape).lineTo(21.587, 0.202);
        ((GeneralPath) shape).lineTo(21.587, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_21_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 579.578f, 218.461f));

        // _0_1_21_1

        // _0_1_21_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-21.39, 0.202);
        ((GeneralPath) shape).lineTo(-21.39, -13.626);
        ((GeneralPath) shape).lineTo(-25.999, -13.626);
        ((GeneralPath) shape).lineTo(-25.999, -15.567);
        ((GeneralPath) shape).lineTo(-14.738, -15.567);
        ((GeneralPath) shape).lineTo(-14.738, -13.626);
        ((GeneralPath) shape).lineTo(-19.348, -13.626);
        ((GeneralPath) shape).lineTo(-19.348, 0.202);
        ((GeneralPath) shape).lineTo(-21.39, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_21_1_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-12.585, -15.567);
        ((GeneralPath) shape).lineTo(-4.205, -15.567);
        ((GeneralPath) shape).lineTo(-1.324, -12.686);
        ((GeneralPath) shape).lineTo(-1.324, -9.593);
        ((GeneralPath) shape).lineTo(-4.205, -6.712);
        ((GeneralPath) shape).lineTo(-5.095, -6.712);
        ((GeneralPath) shape).lineTo(-0.97, -0.505);
        ((GeneralPath) shape).lineTo(-2.638, 0.596);
        ((GeneralPath) shape).lineTo(-7.511, -6.712);
        ((GeneralPath) shape).lineTo(-10.543, -6.712);
        ((GeneralPath) shape).lineTo(-10.543, 0.202);
        ((GeneralPath) shape).lineTo(-12.585, 0.202);
        ((GeneralPath) shape).lineTo(-12.585, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-10.543, -13.626);
        ((GeneralPath) shape).lineTo(-10.543, -8.653);
        ((GeneralPath) shape).lineTo(-5.095, -8.653);
        ((GeneralPath) shape).lineTo(-3.366, -10.382);
        ((GeneralPath) shape).lineTo(-3.366, -11.898);
        ((GeneralPath) shape).lineTo(-5.095, -13.626);
        ((GeneralPath) shape).lineTo(-10.543, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_21_1_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.782, -13.95);
        ((GeneralPath) shape).lineTo(2.376, -13.95);
        ((GeneralPath) shape).lineTo(2.376, -15.567);
        ((GeneralPath) shape).lineTo(11.261, -15.567);
        ((GeneralPath) shape).lineTo(11.261, -13.95);
        ((GeneralPath) shape).lineTo(7.824, -13.95);
        ((GeneralPath) shape).lineTo(7.824, -1.557);
        ((GeneralPath) shape).lineTo(11.261, -1.557);
        ((GeneralPath) shape).lineTo(11.261, 0.202);
        ((GeneralPath) shape).lineTo(2.376, 0.202);
        ((GeneralPath) shape).lineTo(2.376, -1.557);
        ((GeneralPath) shape).lineTo(5.782, -1.557);
        ((GeneralPath) shape).lineTo(5.782, -13.95);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_21_1_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(14.789, -15.567);
        ((GeneralPath) shape).lineTo(16.376, -15.567);
        ((GeneralPath) shape).lineTo(20.419, -9.482);
        ((GeneralPath) shape).lineTo(24.463, -15.567);
        ((GeneralPath) shape).lineTo(26.05, -15.567);
        ((GeneralPath) shape).lineTo(26.05, 0.202);
        ((GeneralPath) shape).lineTo(24.008, 0.202);
        ((GeneralPath) shape).lineTo(24.008, -11.251);
        ((GeneralPath) shape).lineTo(21.208, -7.056);
        ((GeneralPath) shape).lineTo(19.631, -7.056);
        ((GeneralPath) shape).lineTo(16.831, -11.251);
        ((GeneralPath) shape).lineTo(16.831, 0.202);
        ((GeneralPath) shape).lineTo(14.789, 0.202);
        ((GeneralPath) shape).lineTo(14.789, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_21_1

        g.setTransform(transformations.pop()); // _0_1_21
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 372.372f, 206.414f));

        // _0_1_22

        // _0_1_22_0

        // _0_1_22_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-23.118, 0.202);
        ((GeneralPath) shape).lineTo(-25.848, -2.527);
        ((GeneralPath) shape).lineTo(-24.433, -3.942);
        ((GeneralPath) shape).lineTo(-22.229, -1.739);
        ((GeneralPath) shape).lineTo(-18.509, -1.739);
        ((GeneralPath) shape).lineTo(-16.78, -3.467);
        ((GeneralPath) shape).lineTo(-16.78, -4.984);
        ((GeneralPath) shape).lineTo(-18.509, -6.712);
        ((GeneralPath) shape).lineTo(-23.118, -6.712);
        ((GeneralPath) shape).lineTo(-25.999, -9.593);
        ((GeneralPath) shape).lineTo(-25.999, -12.686);
        ((GeneralPath) shape).lineTo(-23.118, -15.567);
        ((GeneralPath) shape).lineTo(-17.619, -15.567);
        ((GeneralPath) shape).lineTo(-14.89, -12.838);
        ((GeneralPath) shape).lineTo(-16.305, -11.423);
        ((GeneralPath) shape).lineTo(-18.509, -13.626);
        ((GeneralPath) shape).lineTo(-22.229, -13.626);
        ((GeneralPath) shape).lineTo(-23.957, -11.898);
        ((GeneralPath) shape).lineTo(-23.957, -10.382);
        ((GeneralPath) shape).lineTo(-22.229, -8.653);
        ((GeneralPath) shape).lineTo(-17.619, -8.653);
        ((GeneralPath) shape).lineTo(-14.738, -5.772);
        ((GeneralPath) shape).lineTo(-14.738, -2.679);
        ((GeneralPath) shape).lineTo(-17.619, 0.202);
        ((GeneralPath) shape).lineTo(-23.118, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_22_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-7.794, 0.202);
        ((GeneralPath) shape).lineTo(-7.794, -13.626);
        ((GeneralPath) shape).lineTo(-12.403, -13.626);
        ((GeneralPath) shape).lineTo(-12.403, -15.567);
        ((GeneralPath) shape).lineTo(-1.142, -15.567);
        ((GeneralPath) shape).lineTo(-1.142, -13.626);
        ((GeneralPath) shape).lineTo(-5.752, -13.626);
        ((GeneralPath) shape).lineTo(-5.752, 0.202);
        ((GeneralPath) shape).lineTo(-7.794, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_22_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.904, -12.939);
        ((GeneralPath) shape).lineTo(2.932, -6.348);
        ((GeneralPath) shape).lineTo(10.644, -6.348);
        ((GeneralPath) shape).lineTo(6.904, -12.939);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(6.055, -15.567);
        ((GeneralPath) shape).lineTo(7.541, -15.567);
        ((GeneralPath) shape).lineTo(12.828, -6.045);
        ((GeneralPath) shape).lineTo(12.828, 0.202);
        ((GeneralPath) shape).lineTo(10.644, 0.202);
        ((GeneralPath) shape).lineTo(10.644, -4.407);
        ((GeneralPath) shape).lineTo(2.932, -4.407);
        ((GeneralPath) shape).lineTo(2.932, 0.202);
        ((GeneralPath) shape).lineTo(0.748, 0.202);
        ((GeneralPath) shape).lineTo(0.748, -5.994);
        ((GeneralPath) shape).lineTo(6.055, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_22_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.488, -6.712);
        ((GeneralPath) shape).lineTo(17.488, -1.739);
        ((GeneralPath) shape).lineTo(21.713, -1.739);
        ((GeneralPath) shape).lineTo(23.442, -3.467);
        ((GeneralPath) shape).lineTo(23.442, -4.984);
        ((GeneralPath) shape).lineTo(21.713, -6.712);
        ((GeneralPath) shape).lineTo(17.488, -6.712);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(15.345, -15.567);
        ((GeneralPath) shape).lineTo(22.603, -15.567);
        ((GeneralPath) shape).lineTo(25.484, -12.686);
        ((GeneralPath) shape).lineTo(25.484, -9.593);
        ((GeneralPath) shape).lineTo(23.573, -7.683);
        ((GeneralPath) shape).lineTo(25.484, -5.772);
        ((GeneralPath) shape).lineTo(25.484, -2.679);
        ((GeneralPath) shape).lineTo(22.603, 0.202);
        ((GeneralPath) shape).lineTo(15.345, 0.202);
        ((GeneralPath) shape).lineTo(15.345, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(17.508, -13.626);
        ((GeneralPath) shape).lineTo(17.508, -8.653);
        ((GeneralPath) shape).lineTo(21.713, -8.653);
        ((GeneralPath) shape).lineTo(23.442, -10.382);
        ((GeneralPath) shape).lineTo(23.442, -11.898);
        ((GeneralPath) shape).lineTo(21.713, -13.626);
        ((GeneralPath) shape).lineTo(17.508, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_22
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 479.847f, 79.1798f));

        // _0_1_23

        // _0_1_23_0

        // _0_1_23_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-19.383, -15.567);
        ((GeneralPath) shape).lineTo(-11.003, -15.567);
        ((GeneralPath) shape).lineTo(-8.122, -12.686);
        ((GeneralPath) shape).lineTo(-8.122, -9.593);
        ((GeneralPath) shape).lineTo(-11.003, -6.712);
        ((GeneralPath) shape).lineTo(-11.893, -6.712);
        ((GeneralPath) shape).lineTo(-7.768, -0.505);
        ((GeneralPath) shape).lineTo(-9.436, 0.596);
        ((GeneralPath) shape).lineTo(-14.309, -6.712);
        ((GeneralPath) shape).lineTo(-17.341, -6.712);
        ((GeneralPath) shape).lineTo(-17.341, 0.202);
        ((GeneralPath) shape).lineTo(-19.383, 0.202);
        ((GeneralPath) shape).lineTo(-19.383, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-17.341, -13.626);
        ((GeneralPath) shape).lineTo(-17.341, -8.653);
        ((GeneralPath) shape).lineTo(-11.893, -8.653);
        ((GeneralPath) shape).lineTo(-10.164, -10.382);
        ((GeneralPath) shape).lineTo(-10.164, -11.898);
        ((GeneralPath) shape).lineTo(-11.893, -13.626);
        ((GeneralPath) shape).lineTo(-17.341, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_23_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-3.563, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -3.467);
        ((GeneralPath) shape).lineTo(-1.835, -1.739);
        ((GeneralPath) shape).lineTo(1.885, -1.739);
        ((GeneralPath) shape).lineTo(3.614, -3.467);
        ((GeneralPath) shape).lineTo(3.614, -15.567);
        ((GeneralPath) shape).lineTo(5.656, -15.567);
        ((GeneralPath) shape).lineTo(5.656, -2.679);
        ((GeneralPath) shape).lineTo(2.775, 0.202);
        ((GeneralPath) shape).lineTo(-2.724, 0.202);
        ((GeneralPath) shape).lineTo(-5.605, -2.679);
        ((GeneralPath) shape).lineTo(-5.605, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_23_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(8.304, -15.628);
        ((GeneralPath) shape).lineTo(16.047, -15.628);
        ((GeneralPath) shape).lineTo(18.928, -12.727);
        ((GeneralPath) shape).lineTo(18.928, -2.659);
        ((GeneralPath) shape).lineTo(16.047, 0.243);
        ((GeneralPath) shape).lineTo(8.304, 0.243);
        ((GeneralPath) shape).lineTo(8.304, -15.628);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(10.275, -13.697);
        ((GeneralPath) shape).lineTo(10.275, -1.638);
        ((GeneralPath) shape).lineTo(15.138, -1.638);
        ((GeneralPath) shape).lineTo(16.866, -3.397);
        ((GeneralPath) shape).lineTo(16.866, -11.938);
        ((GeneralPath) shape).lineTo(15.138, -13.697);
        ((GeneralPath) shape).lineTo(10.275, -13.697);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_23
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 0, 0));

        // _0_1_24
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 480.079f, 472.489f));

        // _0_1_24_0

        // _0_1_24_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-19.383, -15.567);
        ((GeneralPath) shape).lineTo(-11.003, -15.567);
        ((GeneralPath) shape).lineTo(-8.122, -12.686);
        ((GeneralPath) shape).lineTo(-8.122, -9.593);
        ((GeneralPath) shape).lineTo(-11.003, -6.712);
        ((GeneralPath) shape).lineTo(-11.893, -6.712);
        ((GeneralPath) shape).lineTo(-7.768, -0.505);
        ((GeneralPath) shape).lineTo(-9.436, 0.596);
        ((GeneralPath) shape).lineTo(-14.309, -6.712);
        ((GeneralPath) shape).lineTo(-17.341, -6.712);
        ((GeneralPath) shape).lineTo(-17.341, 0.202);
        ((GeneralPath) shape).lineTo(-19.383, 0.202);
        ((GeneralPath) shape).lineTo(-19.383, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-17.341, -13.626);
        ((GeneralPath) shape).lineTo(-17.341, -8.653);
        ((GeneralPath) shape).lineTo(-11.893, -8.653);
        ((GeneralPath) shape).lineTo(-10.164, -10.382);
        ((GeneralPath) shape).lineTo(-10.164, -11.898);
        ((GeneralPath) shape).lineTo(-11.893, -13.626);
        ((GeneralPath) shape).lineTo(-17.341, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_24_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-3.563, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -3.467);
        ((GeneralPath) shape).lineTo(-1.835, -1.739);
        ((GeneralPath) shape).lineTo(1.885, -1.739);
        ((GeneralPath) shape).lineTo(3.614, -3.467);
        ((GeneralPath) shape).lineTo(3.614, -15.567);
        ((GeneralPath) shape).lineTo(5.656, -15.567);
        ((GeneralPath) shape).lineTo(5.656, -2.679);
        ((GeneralPath) shape).lineTo(2.775, 0.202);
        ((GeneralPath) shape).lineTo(-2.724, 0.202);
        ((GeneralPath) shape).lineTo(-5.605, -2.679);
        ((GeneralPath) shape).lineTo(-5.605, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_24_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(8.304, -15.628);
        ((GeneralPath) shape).lineTo(16.047, -15.628);
        ((GeneralPath) shape).lineTo(18.928, -12.727);
        ((GeneralPath) shape).lineTo(18.928, -2.659);
        ((GeneralPath) shape).lineTo(16.047, 0.243);
        ((GeneralPath) shape).lineTo(8.304, 0.243);
        ((GeneralPath) shape).lineTo(8.304, -15.628);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(10.275, -13.697);
        ((GeneralPath) shape).lineTo(10.275, -1.638);
        ((GeneralPath) shape).lineTo(15.138, -1.638);
        ((GeneralPath) shape).lineTo(16.866, -3.397);
        ((GeneralPath) shape).lineTo(16.866, -11.938);
        ((GeneralPath) shape).lineTo(15.138, -13.697);
        ((GeneralPath) shape).lineTo(10.275, -13.697);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_24_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 480.079f, 498.367f));

        // _0_1_24_1

        // _0_1_24_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-21.39, 0.202);
        ((GeneralPath) shape).lineTo(-21.39, -13.626);
        ((GeneralPath) shape).lineTo(-25.999, -13.626);
        ((GeneralPath) shape).lineTo(-25.999, -15.567);
        ((GeneralPath) shape).lineTo(-14.738, -15.567);
        ((GeneralPath) shape).lineTo(-14.738, -13.626);
        ((GeneralPath) shape).lineTo(-19.348, -13.626);
        ((GeneralPath) shape).lineTo(-19.348, 0.202);
        ((GeneralPath) shape).lineTo(-21.39, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_24_1_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-12.585, -15.567);
        ((GeneralPath) shape).lineTo(-4.205, -15.567);
        ((GeneralPath) shape).lineTo(-1.324, -12.686);
        ((GeneralPath) shape).lineTo(-1.324, -9.593);
        ((GeneralPath) shape).lineTo(-4.205, -6.712);
        ((GeneralPath) shape).lineTo(-5.095, -6.712);
        ((GeneralPath) shape).lineTo(-0.97, -0.505);
        ((GeneralPath) shape).lineTo(-2.638, 0.596);
        ((GeneralPath) shape).lineTo(-7.511, -6.712);
        ((GeneralPath) shape).lineTo(-10.543, -6.712);
        ((GeneralPath) shape).lineTo(-10.543, 0.202);
        ((GeneralPath) shape).lineTo(-12.585, 0.202);
        ((GeneralPath) shape).lineTo(-12.585, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-10.543, -13.626);
        ((GeneralPath) shape).lineTo(-10.543, -8.653);
        ((GeneralPath) shape).lineTo(-5.095, -8.653);
        ((GeneralPath) shape).lineTo(-3.366, -10.382);
        ((GeneralPath) shape).lineTo(-3.366, -11.898);
        ((GeneralPath) shape).lineTo(-5.095, -13.626);
        ((GeneralPath) shape).lineTo(-10.543, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_24_1_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.782, -13.95);
        ((GeneralPath) shape).lineTo(2.376, -13.95);
        ((GeneralPath) shape).lineTo(2.376, -15.567);
        ((GeneralPath) shape).lineTo(11.261, -15.567);
        ((GeneralPath) shape).lineTo(11.261, -13.95);
        ((GeneralPath) shape).lineTo(7.824, -13.95);
        ((GeneralPath) shape).lineTo(7.824, -1.557);
        ((GeneralPath) shape).lineTo(11.261, -1.557);
        ((GeneralPath) shape).lineTo(11.261, 0.202);
        ((GeneralPath) shape).lineTo(2.376, 0.202);
        ((GeneralPath) shape).lineTo(2.376, -1.557);
        ((GeneralPath) shape).lineTo(5.782, -1.557);
        ((GeneralPath) shape).lineTo(5.782, -13.95);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_24_1_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(14.789, -15.567);
        ((GeneralPath) shape).lineTo(16.376, -15.567);
        ((GeneralPath) shape).lineTo(20.419, -9.482);
        ((GeneralPath) shape).lineTo(24.463, -15.567);
        ((GeneralPath) shape).lineTo(26.05, -15.567);
        ((GeneralPath) shape).lineTo(26.05, 0.202);
        ((GeneralPath) shape).lineTo(24.008, 0.202);
        ((GeneralPath) shape).lineTo(24.008, -11.251);
        ((GeneralPath) shape).lineTo(21.208, -7.056);
        ((GeneralPath) shape).lineTo(19.631, -7.056);
        ((GeneralPath) shape).lineTo(16.831, -11.251);
        ((GeneralPath) shape).lineTo(16.831, 0.202);
        ((GeneralPath) shape).lineTo(14.789, 0.202);
        ((GeneralPath) shape).lineTo(14.789, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_24_1

        g.setTransform(transformations.pop()); // _0_1_24

        // _0_1_25
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(669.789, 353.302);
        ((GeneralPath) shape).lineTo(669.789, 418.137);

        g.setStroke(new BasicStroke(2, 0, 0, 4));
        g.draw(shape);

        // _0_1_26
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(663.586, 419.27);
        ((GeneralPath) shape).lineTo(676.223, 419.27);

        g.setStroke(new BasicStroke(1.9f, 0, 0, 4));
        g.draw(shape);

        // _0_1_27
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(289.156, 353.302);
        ((GeneralPath) shape).lineTo(289.156, 418.137);

        g.setStroke(new BasicStroke(2, 0, 0, 4));
        g.draw(shape);

        // _0_1_28
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(295.359, 419.27);
        ((GeneralPath) shape).lineTo(282.722, 419.27);

        g.setStroke(new BasicStroke(1.9f, 0, 0, 4));
        g.draw(shape);

        // _0_1_29
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(674.342, 353.512);
        ((GeneralPath) shape).lineTo(669.773, 346.712);
        ((GeneralPath) shape).lineTo(665.204, 353.512);
        ((GeneralPath) shape).lineTo(674.342, 353.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_30
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(293.663, 353.512);
        ((GeneralPath) shape).lineTo(289.094, 346.712);
        ((GeneralPath) shape).lineTo(284.524, 353.512);
        ((GeneralPath) shape).lineTo(293.663, 353.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 0, 0));

        // _0_1_31
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 239.888f, 360.526f));

        // _0_1_31_0

        // _0_1_31_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.656, -1.739);
        ((GeneralPath) shape).lineTo(5.656, 0.202);
        ((GeneralPath) shape).lineTo(-5.605, 0.202);
        ((GeneralPath) shape).lineTo(-5.605, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -15.567);
        ((GeneralPath) shape).lineTo(-3.563, -1.739);
        ((GeneralPath) shape).lineTo(5.656, -1.739);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_31_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 239.888f, 386.405f));

        // _0_1_31_1

        // _0_1_31_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-16.502, -6.712);
        ((GeneralPath) shape).lineTo(-16.502, -1.739);
        ((GeneralPath) shape).lineTo(-12.277, -1.739);
        ((GeneralPath) shape).lineTo(-10.548, -3.467);
        ((GeneralPath) shape).lineTo(-10.548, -4.984);
        ((GeneralPath) shape).lineTo(-12.277, -6.712);
        ((GeneralPath) shape).lineTo(-16.502, -6.712);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-18.645, -15.567);
        ((GeneralPath) shape).lineTo(-11.387, -15.567);
        ((GeneralPath) shape).lineTo(-8.506, -12.686);
        ((GeneralPath) shape).lineTo(-8.506, -9.593);
        ((GeneralPath) shape).lineTo(-10.417, -7.683);
        ((GeneralPath) shape).lineTo(-8.506, -5.772);
        ((GeneralPath) shape).lineTo(-8.506, -2.679);
        ((GeneralPath) shape).lineTo(-11.387, 0.202);
        ((GeneralPath) shape).lineTo(-18.645, 0.202);
        ((GeneralPath) shape).lineTo(-18.645, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-16.482, -13.626);
        ((GeneralPath) shape).lineTo(-16.482, -8.653);
        ((GeneralPath) shape).lineTo(-12.277, -8.653);
        ((GeneralPath) shape).lineTo(-10.548, -10.382);
        ((GeneralPath) shape).lineTo(-10.548, -11.898);
        ((GeneralPath) shape).lineTo(-12.277, -13.626);
        ((GeneralPath) shape).lineTo(-16.482, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_31_1_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-5.787, -15.567);
        ((GeneralPath) shape).lineTo(2.593, -15.567);
        ((GeneralPath) shape).lineTo(5.474, -12.686);
        ((GeneralPath) shape).lineTo(5.474, -9.593);
        ((GeneralPath) shape).lineTo(2.593, -6.712);
        ((GeneralPath) shape).lineTo(1.703, -6.712);
        ((GeneralPath) shape).lineTo(5.828, -0.505);
        ((GeneralPath) shape).lineTo(4.16, 0.596);
        ((GeneralPath) shape).lineTo(-0.713, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-3.745, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -8.653);
        ((GeneralPath) shape).lineTo(1.703, -8.653);
        ((GeneralPath) shape).lineTo(3.432, -10.382);
        ((GeneralPath) shape).lineTo(3.432, -11.898);
        ((GeneralPath) shape).lineTo(1.703, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_31_1_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(19.454, -0.768);
        ((GeneralPath) shape).lineTo(18.039, 0.637);
        ((GeneralPath) shape).lineTo(11.125, -6.267);
        ((GeneralPath) shape).lineTo(9.841, -4.984);
        ((GeneralPath) shape).lineTo(9.841, 0.202);
        ((GeneralPath) shape).lineTo(7.799, 0.202);
        ((GeneralPath) shape).lineTo(7.799, -15.567);
        ((GeneralPath) shape).lineTo(9.841, -15.567);
        ((GeneralPath) shape).lineTo(9.841, -7.814);
        ((GeneralPath) shape).lineTo(18.039, -16.012);
        ((GeneralPath) shape).lineTo(19.454, -14.597);
        ((GeneralPath) shape).lineTo(12.54, -7.683);
        ((GeneralPath) shape).lineTo(19.454, -0.768);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_31_1

        g.setTransform(transformations.pop()); // _0_1_31
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 0, 0));

        // _0_1_32
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 721.961f, 360.526f));

        // _0_1_32_0

        // _0_1_32_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-5.787, -15.567);
        ((GeneralPath) shape).lineTo(2.593, -15.567);
        ((GeneralPath) shape).lineTo(5.474, -12.686);
        ((GeneralPath) shape).lineTo(5.474, -9.593);
        ((GeneralPath) shape).lineTo(2.593, -6.712);
        ((GeneralPath) shape).lineTo(1.703, -6.712);
        ((GeneralPath) shape).lineTo(5.828, -0.505);
        ((GeneralPath) shape).lineTo(4.16, 0.596);
        ((GeneralPath) shape).lineTo(-0.713, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-3.745, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -8.653);
        ((GeneralPath) shape).lineTo(1.703, -8.653);
        ((GeneralPath) shape).lineTo(3.432, -10.382);
        ((GeneralPath) shape).lineTo(3.432, -11.898);
        ((GeneralPath) shape).lineTo(1.703, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_32_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 721.961f, 386.405f));

        // _0_1_32_1

        // _0_1_32_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-16.502, -6.712);
        ((GeneralPath) shape).lineTo(-16.502, -1.739);
        ((GeneralPath) shape).lineTo(-12.277, -1.739);
        ((GeneralPath) shape).lineTo(-10.548, -3.467);
        ((GeneralPath) shape).lineTo(-10.548, -4.984);
        ((GeneralPath) shape).lineTo(-12.277, -6.712);
        ((GeneralPath) shape).lineTo(-16.502, -6.712);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-18.645, -15.567);
        ((GeneralPath) shape).lineTo(-11.387, -15.567);
        ((GeneralPath) shape).lineTo(-8.506, -12.686);
        ((GeneralPath) shape).lineTo(-8.506, -9.593);
        ((GeneralPath) shape).lineTo(-10.417, -7.683);
        ((GeneralPath) shape).lineTo(-8.506, -5.772);
        ((GeneralPath) shape).lineTo(-8.506, -2.679);
        ((GeneralPath) shape).lineTo(-11.387, 0.202);
        ((GeneralPath) shape).lineTo(-18.645, 0.202);
        ((GeneralPath) shape).lineTo(-18.645, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-16.482, -13.626);
        ((GeneralPath) shape).lineTo(-16.482, -8.653);
        ((GeneralPath) shape).lineTo(-12.277, -8.653);
        ((GeneralPath) shape).lineTo(-10.548, -10.382);
        ((GeneralPath) shape).lineTo(-10.548, -11.898);
        ((GeneralPath) shape).lineTo(-12.277, -13.626);
        ((GeneralPath) shape).lineTo(-16.482, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_32_1_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-5.787, -15.567);
        ((GeneralPath) shape).lineTo(2.593, -15.567);
        ((GeneralPath) shape).lineTo(5.474, -12.686);
        ((GeneralPath) shape).lineTo(5.474, -9.593);
        ((GeneralPath) shape).lineTo(2.593, -6.712);
        ((GeneralPath) shape).lineTo(1.703, -6.712);
        ((GeneralPath) shape).lineTo(5.828, -0.505);
        ((GeneralPath) shape).lineTo(4.16, 0.596);
        ((GeneralPath) shape).lineTo(-0.713, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, -6.712);
        ((GeneralPath) shape).lineTo(-3.745, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, 0.202);
        ((GeneralPath) shape).lineTo(-5.787, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-3.745, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -8.653);
        ((GeneralPath) shape).lineTo(1.703, -8.653);
        ((GeneralPath) shape).lineTo(3.432, -10.382);
        ((GeneralPath) shape).lineTo(3.432, -11.898);
        ((GeneralPath) shape).lineTo(1.703, -13.626);
        ((GeneralPath) shape).lineTo(-3.745, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_32_1_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(19.454, -0.768);
        ((GeneralPath) shape).lineTo(18.039, 0.637);
        ((GeneralPath) shape).lineTo(11.125, -6.267);
        ((GeneralPath) shape).lineTo(9.841, -4.984);
        ((GeneralPath) shape).lineTo(9.841, 0.202);
        ((GeneralPath) shape).lineTo(7.799, 0.202);
        ((GeneralPath) shape).lineTo(7.799, -15.567);
        ((GeneralPath) shape).lineTo(9.841, -15.567);
        ((GeneralPath) shape).lineTo(9.841, -7.814);
        ((GeneralPath) shape).lineTo(18.039, -16.012);
        ((GeneralPath) shape).lineTo(19.454, -14.597);
        ((GeneralPath) shape).lineTo(12.54, -7.683);
        ((GeneralPath) shape).lineTo(19.454, -0.768);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_32_1

        g.setTransform(transformations.pop()); // _0_1_32
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 472.879f, 374.967f));

        // _0_1_33

        // _0_1_33_0

        // _0_1_33_0_0

        // _0_1_33_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-50.599, -11.423);
        ((GeneralPath) shape).lineTo(-52.802, -13.626);
        ((GeneralPath) shape).lineTo(-56.522, -13.626);
        ((GeneralPath) shape).lineTo(-58.251, -11.898);
        ((GeneralPath) shape).lineTo(-58.251, -3.467);
        ((GeneralPath) shape).lineTo(-56.522, -1.739);
        ((GeneralPath) shape).lineTo(-52.802, -1.739);
        ((GeneralPath) shape).lineTo(-51.074, -3.467);
        ((GeneralPath) shape).lineTo(-51.074, -6.712);
        ((GeneralPath) shape).lineTo(-55.683, -6.712);
        ((GeneralPath) shape).lineTo(-55.683, -8.653);
        ((GeneralPath) shape).lineTo(-49.032, -8.653);
        ((GeneralPath) shape).lineTo(-49.032, -2.679);
        ((GeneralPath) shape).lineTo(-51.913, 0.202);
        ((GeneralPath) shape).lineTo(-57.412, 0.202);
        ((GeneralPath) shape).lineTo(-60.293, -2.679);
        ((GeneralPath) shape).lineTo(-60.293, -12.686);
        ((GeneralPath) shape).lineTo(-57.412, -15.567);
        ((GeneralPath) shape).lineTo(-51.913, -15.567);
        ((GeneralPath) shape).lineTo(-49.184, -12.838);
        ((GeneralPath) shape).lineTo(-50.599, -11.423);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_33_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-35.436, 0.202);
        ((GeneralPath) shape).lineTo(-37.023, 0.202);
        ((GeneralPath) shape).lineTo(-44.655, -11.251);
        ((GeneralPath) shape).lineTo(-44.655, 0.202);
        ((GeneralPath) shape).lineTo(-46.697, 0.202);
        ((GeneralPath) shape).lineTo(-46.697, -15.567);
        ((GeneralPath) shape).lineTo(-45.11, -15.567);
        ((GeneralPath) shape).lineTo(-37.478, -4.114);
        ((GeneralPath) shape).lineTo(-37.478, -15.567);
        ((GeneralPath) shape).lineTo(-35.436, -15.567);
        ((GeneralPath) shape).lineTo(-35.436, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_33_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-32.787, -15.628);
        ((GeneralPath) shape).lineTo(-25.044, -15.628);
        ((GeneralPath) shape).lineTo(-22.163, -12.727);
        ((GeneralPath) shape).lineTo(-22.163, -2.659);
        ((GeneralPath) shape).lineTo(-25.044, 0.243);
        ((GeneralPath) shape).lineTo(-32.787, 0.243);
        ((GeneralPath) shape).lineTo(-32.787, -15.628);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-30.816, -13.697);
        ((GeneralPath) shape).lineTo(-30.816, -1.638);
        ((GeneralPath) shape).lineTo(-25.954, -1.638);
        ((GeneralPath) shape).lineTo(-24.225, -3.397);
        ((GeneralPath) shape).lineTo(-24.225, -11.938);
        ((GeneralPath) shape).lineTo(-25.954, -13.697);
        ((GeneralPath) shape).lineTo(-30.816, -13.697);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_33_0_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(16.957, 0.0);
        ((GeneralPath) shape).lineTo(16.957, -10.978);
        ((GeneralPath) shape).lineTo(18.949, -10.978);
        ((GeneralPath) shape).lineTo(18.949, -8.916);
        ((GeneralPath) shape).curveTo(19.919, -10.452, 21.166, -11.221, 22.689, -11.221);
        ((GeneralPath) shape).curveTo(24.158, -11.221, 25.159, -10.452, 25.691, -8.916);
        ((GeneralPath) shape).curveTo(26.635, -10.459, 27.861, -11.231, 29.371, -11.231);
        ((GeneralPath) shape).curveTo(30.341, -11.231, 31.092, -10.946, 31.625, -10.377);
        ((GeneralPath) shape).curveTo(32.157, -9.807, 32.423, -9.01, 32.423, -7.986);
        ((GeneralPath) shape).lineTo(32.423, 0.0);
        ((GeneralPath) shape).lineTo(30.422, 0.0);
        ((GeneralPath) shape).lineTo(30.422, -7.672);
        ((GeneralPath) shape).curveTo(30.422, -8.926, 29.927, -9.553, 28.936, -9.553);
        ((GeneralPath) shape).curveTo(27.905, -9.553, 26.823, -8.821, 25.691, -7.359);
        ((GeneralPath) shape).lineTo(25.691, 0.0);
        ((GeneralPath) shape).lineTo(23.69, 0.0);
        ((GeneralPath) shape).lineTo(23.69, -7.672);
        ((GeneralPath) shape).curveTo(23.69, -8.933, 23.184, -9.563, 22.173, -9.563);
        ((GeneralPath) shape).curveTo(21.169, -9.563, 20.094, -8.828, 18.949, -7.359);
        ((GeneralPath) shape).lineTo(18.949, 0.0);
        ((GeneralPath) shape).lineTo(16.957, 0.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_33_0_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(38.367, 0.202);
        ((GeneralPath) shape).lineTo(35.638, -2.527);
        ((GeneralPath) shape).lineTo(37.053, -3.942);
        ((GeneralPath) shape).lineTo(39.257, -1.739);
        ((GeneralPath) shape).lineTo(42.977, -1.739);
        ((GeneralPath) shape).lineTo(44.705, -3.467);
        ((GeneralPath) shape).lineTo(44.705, -4.984);
        ((GeneralPath) shape).lineTo(42.977, -6.712);
        ((GeneralPath) shape).lineTo(38.367, -6.712);
        ((GeneralPath) shape).lineTo(35.486, -9.593);
        ((GeneralPath) shape).lineTo(35.486, -12.686);
        ((GeneralPath) shape).lineTo(38.367, -15.567);
        ((GeneralPath) shape).lineTo(43.866, -15.567);
        ((GeneralPath) shape).lineTo(46.596, -12.838);
        ((GeneralPath) shape).lineTo(45.181, -11.423);
        ((GeneralPath) shape).lineTo(42.977, -13.626);
        ((GeneralPath) shape).lineTo(39.257, -13.626);
        ((GeneralPath) shape).lineTo(37.528, -11.898);
        ((GeneralPath) shape).lineTo(37.528, -10.382);
        ((GeneralPath) shape).lineTo(39.257, -8.653);
        ((GeneralPath) shape).lineTo(43.866, -8.653);
        ((GeneralPath) shape).lineTo(46.747, -5.772);
        ((GeneralPath) shape).lineTo(46.747, -2.679);
        ((GeneralPath) shape).lineTo(43.866, 0.202);
        ((GeneralPath) shape).lineTo(38.367, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_33_0_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(49.083, -15.567);
        ((GeneralPath) shape).lineTo(51.124, -15.567);
        ((GeneralPath) shape).lineTo(51.124, -4.114);
        ((GeneralPath) shape).lineTo(53.925, -8.309);
        ((GeneralPath) shape).lineTo(55.501, -8.309);
        ((GeneralPath) shape).lineTo(58.302, -4.114);
        ((GeneralPath) shape).lineTo(58.302, -15.567);
        ((GeneralPath) shape).lineTo(60.344, -15.567);
        ((GeneralPath) shape).lineTo(60.344, 0.202);
        ((GeneralPath) shape).lineTo(58.756, 0.202);
        ((GeneralPath) shape).lineTo(54.713, -5.883);
        ((GeneralPath) shape).lineTo(50.67, 0.202);
        ((GeneralPath) shape).lineTo(49.083, 0.202);
        ((GeneralPath) shape).lineTo(49.083, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_33

        // _0_1_34
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(224.56, 496.817);
        ((GeneralPath) shape).curveTo(184.354, 498.182, 141.567, 499.432, 107.564, 499.788);
        ((GeneralPath) shape).curveTo(34.893, 500.546, 36.354, 485.781, 36.354, 485.781);
        ((GeneralPath) shape).curveTo(36.354, 485.781, 32.929, 462.837, 105.599, 463.596);
        ((GeneralPath) shape).curveTo(139.697, 463.952, 187.941, 470.798, 228.222, 477.776);

        g.setPaint(new Color(0x80FFFFFF, true));
        g.setStroke(new BasicStroke(3, 1, 1, 4));
        g.draw(shape);

        // _0_1_35
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(334.693, 494.083);
        ((GeneralPath) shape).lineTo(341.059, 493.903);
        ((GeneralPath) shape).curveTo(338.023, 528.38, 323.399, 551.005, 309.805, 565.035);
        ((GeneralPath) shape).lineTo(305.109, 560.13);

        g.setPaint(WHITE);
        g.setStroke(new BasicStroke(2.5f, 0, 0, 4));
        g.draw(shape);

        // _0_1_36
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(321.454, 536.455);
        ((GeneralPath) shape).lineTo(327.472, 540.087);

        g.draw(shape);

        // _0_1_37
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(331.406, 511.676);
        ((GeneralPath) shape).lineTo(338.233, 513.362);

        g.draw(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 361.003f, 494.487f));

        // _0_1_38

        // _0_1_38_0

        // _0_1_38_0_0

        // _0_1_38_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-7.465, -2.078);
        ((GeneralPath) shape).lineTo(-7.465, -9.813);
        ((GeneralPath) shape).lineTo(-5.238, -12.039);
        ((GeneralPath) shape).lineTo(-1.676, -12.039);
        ((GeneralPath) shape).lineTo(0.551, -9.813);
        ((GeneralPath) shape).lineTo(0.551, -2.078);
        ((GeneralPath) shape).lineTo(-1.676, 0.148);
        ((GeneralPath) shape).lineTo(-5.238, 0.148);
        ((GeneralPath) shape).lineTo(-7.465, -2.078);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-4.895, -10.539);
        ((GeneralPath) shape).lineTo(-6.23, -9.203);
        ((GeneralPath) shape).lineTo(-6.23, -2.688);
        ((GeneralPath) shape).lineTo(-4.895, -1.352);
        ((GeneralPath) shape).lineTo(-2.02, -1.352);
        ((GeneralPath) shape).lineTo(-0.684, -2.688);
        ((GeneralPath) shape).lineTo(-0.684, -9.203);
        ((GeneralPath) shape).lineTo(-2.02, -10.539);
        ((GeneralPath) shape).lineTo(-4.895, -10.539);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_38_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.309, -7.906);
        ((GeneralPath) shape).curveTo(5.684, -7.906, 5.146, -8.134, 4.695, -8.59);
        ((GeneralPath) shape).curveTo(4.245, -9.046, 4.02, -9.589, 4.02, -10.219);
        ((GeneralPath) shape).curveTo(4.02, -10.854, 4.246, -11.398, 4.699, -11.852);
        ((GeneralPath) shape).curveTo(5.152, -12.305, 5.697, -12.531, 6.332, -12.531);
        ((GeneralPath) shape).curveTo(6.967, -12.531, 7.512, -12.305, 7.965, -11.852);
        ((GeneralPath) shape).curveTo(8.418, -11.398, 8.645, -10.854, 8.645, -10.219);
        ((GeneralPath) shape).curveTo(8.645, -9.573, 8.418, -9.026, 7.965, -8.578);
        ((GeneralPath) shape).curveTo(7.512, -8.13, 6.96, -7.906, 6.309, -7.906);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(6.316, -8.867);
        ((GeneralPath) shape).curveTo(6.697, -8.867, 7.02, -8.999, 7.285, -9.262);
        ((GeneralPath) shape).curveTo(7.551, -9.525, 7.684, -9.844, 7.684, -10.219);
        ((GeneralPath) shape).curveTo(7.684, -10.589, 7.551, -10.905, 7.285, -11.168);
        ((GeneralPath) shape).curveTo(7.02, -11.431, 6.702, -11.563, 6.332, -11.563);
        ((GeneralPath) shape).curveTo(5.962, -11.563, 5.645, -11.431, 5.379, -11.168);
        ((GeneralPath) shape).curveTo(5.113, -10.905, 4.98, -10.589, 4.98, -10.219);
        ((GeneralPath) shape).curveTo(4.98, -9.849, 5.112, -9.531, 5.375, -9.266);
        ((GeneralPath) shape).curveTo(5.638, -9.0, 5.952, -8.867, 6.316, -8.867);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_38
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 363.86f, 514.844f));

        // _0_1_39

        // _0_1_39_0

        // _0_1_39_0_0

        // _0_1_39_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-13.148, -1.313);
        ((GeneralPath) shape).lineTo(-10.352, -1.313);
        ((GeneralPath) shape).lineTo(-10.352, -9.766);
        ((GeneralPath) shape).lineTo(-13.0, -7.117);
        ((GeneralPath) shape).lineTo(-13.0, -9.305);
        ((GeneralPath) shape).lineTo(-10.25, -12.07);
        ((GeneralPath) shape).lineTo(-8.773, -12.07);
        ((GeneralPath) shape).lineTo(-8.773, -1.297);
        ((GeneralPath) shape).lineTo(-6.391, -1.297);
        ((GeneralPath) shape).lineTo(-6.391, 0.117);
        ((GeneralPath) shape).lineTo(-13.148, 0.117);
        ((GeneralPath) shape).lineTo(-13.148, -1.313);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_39_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-1.773, -2.078);
        ((GeneralPath) shape).lineTo(-1.773, -9.813);
        ((GeneralPath) shape).lineTo(0.453, -12.039);
        ((GeneralPath) shape).lineTo(4.016, -12.039);
        ((GeneralPath) shape).lineTo(6.242, -9.813);
        ((GeneralPath) shape).lineTo(6.242, -2.078);
        ((GeneralPath) shape).lineTo(4.016, 0.148);
        ((GeneralPath) shape).lineTo(0.453, 0.148);
        ((GeneralPath) shape).lineTo(-1.773, -2.078);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(0.797, -10.539);
        ((GeneralPath) shape).lineTo(-0.539, -9.203);
        ((GeneralPath) shape).lineTo(-0.539, -2.688);
        ((GeneralPath) shape).lineTo(0.797, -1.352);
        ((GeneralPath) shape).lineTo(3.672, -1.352);
        ((GeneralPath) shape).lineTo(5.008, -2.688);
        ((GeneralPath) shape).lineTo(5.008, -9.203);
        ((GeneralPath) shape).lineTo(3.672, -10.539);
        ((GeneralPath) shape).lineTo(0.797, -10.539);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_39_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(12.0, -7.906);
        ((GeneralPath) shape).curveTo(11.375, -7.906, 10.837, -8.134, 10.387, -8.59);
        ((GeneralPath) shape).curveTo(9.936, -9.046, 9.711, -9.589, 9.711, -10.219);
        ((GeneralPath) shape).curveTo(9.711, -10.854, 9.938, -11.398, 10.391, -11.852);
        ((GeneralPath) shape).curveTo(10.844, -12.305, 11.388, -12.531, 12.023, -12.531);
        ((GeneralPath) shape).curveTo(12.659, -12.531, 13.203, -12.305, 13.656, -11.852);
        ((GeneralPath) shape).curveTo(14.109, -11.398, 14.336, -10.854, 14.336, -10.219);
        ((GeneralPath) shape).curveTo(14.336, -9.573, 14.109, -9.026, 13.656, -8.578);
        ((GeneralPath) shape).curveTo(13.203, -8.13, 12.651, -7.906, 12.0, -7.906);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(12.008, -8.867);
        ((GeneralPath) shape).curveTo(12.388, -8.867, 12.711, -8.999, 12.977, -9.262);
        ((GeneralPath) shape).curveTo(13.242, -9.525, 13.375, -9.844, 13.375, -10.219);
        ((GeneralPath) shape).curveTo(13.375, -10.589, 13.242, -10.905, 12.977, -11.168);
        ((GeneralPath) shape).curveTo(12.711, -11.431, 12.393, -11.563, 12.023, -11.563);
        ((GeneralPath) shape).curveTo(11.654, -11.563, 11.336, -11.431, 11.07, -11.168);
        ((GeneralPath) shape).curveTo(10.805, -10.905, 10.672, -10.589, 10.672, -10.219);
        ((GeneralPath) shape).curveTo(10.672, -9.849, 10.803, -9.531, 11.066, -9.266);
        ((GeneralPath) shape).curveTo(11.329, -9.0, 11.643, -8.867, 12.008, -8.867);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_39
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 355.289f, 541.987f));

        // _0_1_40

        // _0_1_40_0

        // _0_1_40_0_0

        // _0_1_40_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-5.266, 0.156);
        ((GeneralPath) shape).lineTo(-13.969, 0.156);
        ((GeneralPath) shape).lineTo(-13.969, -1.93);
        ((GeneralPath) shape).lineTo(-6.844, -8.023);
        ((GeneralPath) shape).lineTo(-6.844, -9.195);
        ((GeneralPath) shape).lineTo(-8.18, -10.531);
        ((GeneralPath) shape).lineTo(-11.055, -10.531);
        ((GeneralPath) shape).lineTo(-12.758, -8.828);
        ((GeneralPath) shape).lineTo(-13.852, -9.922);
        ((GeneralPath) shape).lineTo(-11.742, -12.031);
        ((GeneralPath) shape).lineTo(-7.492, -12.031);
        ((GeneralPath) shape).lineTo(-5.266, -9.805);
        ((GeneralPath) shape).lineTo(-5.266, -7.414);
        ((GeneralPath) shape).lineTo(-12.359, -1.344);
        ((GeneralPath) shape).lineTo(-5.266, -1.344);
        ((GeneralPath) shape).lineTo(-5.266, 0.156);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_40_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(5.547, -7.891);
        ((GeneralPath) shape).lineTo(7.078, -6.352);
        ((GeneralPath) shape).lineTo(7.078, -2.102);
        ((GeneralPath) shape).lineTo(4.813, 0.156);
        ((GeneralPath) shape).lineTo(1.734, 0.156);
        ((GeneralPath) shape).lineTo(-0.531, -2.109);
        ((GeneralPath) shape).lineTo(-0.531, -3.977);
        ((GeneralPath) shape).lineTo(1.008, -3.977);
        ((GeneralPath) shape).lineTo(1.008, -2.992);
        ((GeneralPath) shape).lineTo(2.508, -1.477);
        ((GeneralPath) shape).lineTo(4.039, -1.477);
        ((GeneralPath) shape).lineTo(5.336, -2.773);
        ((GeneralPath) shape).lineTo(5.336, -6.305);
        ((GeneralPath) shape).lineTo(0.813, -6.305);
        ((GeneralPath) shape).lineTo(-0.531, -7.625);
        ((GeneralPath) shape).lineTo(-0.531, -12.18);
        ((GeneralPath) shape).lineTo(7.133, -12.18);
        ((GeneralPath) shape).lineTo(7.133, -10.547);
        ((GeneralPath) shape).lineTo(1.211, -10.547);
        ((GeneralPath) shape).lineTo(1.211, -7.891);
        ((GeneralPath) shape).lineTo(5.547, -7.891);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_40_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(12.82, -7.906);
        ((GeneralPath) shape).curveTo(12.195, -7.906, 11.658, -8.134, 11.207, -8.59);
        ((GeneralPath) shape).curveTo(10.757, -9.046, 10.531, -9.589, 10.531, -10.219);
        ((GeneralPath) shape).curveTo(10.531, -10.854, 10.758, -11.398, 11.211, -11.852);
        ((GeneralPath) shape).curveTo(11.664, -12.305, 12.208, -12.531, 12.844, -12.531);
        ((GeneralPath) shape).curveTo(13.479, -12.531, 14.023, -12.305, 14.477, -11.852);
        ((GeneralPath) shape).curveTo(14.93, -11.398, 15.156, -10.854, 15.156, -10.219);
        ((GeneralPath) shape).curveTo(15.156, -9.573, 14.93, -9.026, 14.477, -8.578);
        ((GeneralPath) shape).curveTo(14.023, -8.13, 13.471, -7.906, 12.82, -7.906);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(12.828, -8.867);
        ((GeneralPath) shape).curveTo(13.208, -8.867, 13.531, -8.999, 13.797, -9.262);
        ((GeneralPath) shape).curveTo(14.063, -9.525, 14.195, -9.844, 14.195, -10.219);
        ((GeneralPath) shape).curveTo(14.195, -10.589, 14.063, -10.905, 13.797, -11.168);
        ((GeneralPath) shape).curveTo(13.531, -11.431, 13.214, -11.563, 12.844, -11.563);
        ((GeneralPath) shape).curveTo(12.474, -11.563, 12.156, -11.431, 11.891, -11.168);
        ((GeneralPath) shape).curveTo(11.625, -10.905, 11.492, -10.589, 11.492, -10.219);
        ((GeneralPath) shape).curveTo(11.492, -9.849, 11.624, -9.531, 11.887, -9.266);
        ((GeneralPath) shape).curveTo(12.15, -9.0, 12.464, -8.867, 12.828, -8.867);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_40
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 344.218f, 567.344f));

        // _0_1_41

        // _0_1_41_0

        // _0_1_41_0_0

        // _0_1_41_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-9.035, -4.945);
        ((GeneralPath) shape).lineTo(-9.035, -9.547);
        ((GeneralPath) shape).lineTo(-12.559, -4.945);
        ((GeneralPath) shape).lineTo(-9.035, -4.945);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-5.676, -4.945);
        ((GeneralPath) shape).lineTo(-5.676, -3.445);
        ((GeneralPath) shape).lineTo(-7.457, -3.445);
        ((GeneralPath) shape).lineTo(-7.457, 0.117);
        ((GeneralPath) shape).lineTo(-9.035, 0.117);
        ((GeneralPath) shape).lineTo(-9.035, -3.445);
        ((GeneralPath) shape).lineTo(-14.379, -3.445);
        ((GeneralPath) shape).lineTo(-14.379, -4.828);
        ((GeneralPath) shape).lineTo(-9.012, -12.07);
        ((GeneralPath) shape).lineTo(-7.457, -12.07);
        ((GeneralPath) shape).lineTo(-7.457, -4.945);
        ((GeneralPath) shape).lineTo(-5.676, -4.945);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_41_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-0.832, -2.078);
        ((GeneralPath) shape).lineTo(-0.832, -9.813);
        ((GeneralPath) shape).lineTo(1.395, -12.039);
        ((GeneralPath) shape).lineTo(4.957, -12.039);
        ((GeneralPath) shape).lineTo(7.184, -9.813);
        ((GeneralPath) shape).lineTo(7.184, -2.078);
        ((GeneralPath) shape).lineTo(4.957, 0.148);
        ((GeneralPath) shape).lineTo(1.395, 0.148);
        ((GeneralPath) shape).lineTo(-0.832, -2.078);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(1.738, -10.539);
        ((GeneralPath) shape).lineTo(0.402, -9.203);
        ((GeneralPath) shape).lineTo(0.402, -2.688);
        ((GeneralPath) shape).lineTo(1.738, -1.352);
        ((GeneralPath) shape).lineTo(4.613, -1.352);
        ((GeneralPath) shape).lineTo(5.949, -2.688);
        ((GeneralPath) shape).lineTo(5.949, -9.203);
        ((GeneralPath) shape).lineTo(4.613, -10.539);
        ((GeneralPath) shape).lineTo(1.738, -10.539);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_41_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(12.941, -7.906);
        ((GeneralPath) shape).curveTo(12.316, -7.906, 11.779, -8.134, 11.328, -8.59);
        ((GeneralPath) shape).curveTo(10.878, -9.046, 10.652, -9.589, 10.652, -10.219);
        ((GeneralPath) shape).curveTo(10.652, -10.854, 10.879, -11.398, 11.332, -11.852);
        ((GeneralPath) shape).curveTo(11.785, -12.305, 12.329, -12.531, 12.965, -12.531);
        ((GeneralPath) shape).curveTo(13.6, -12.531, 14.145, -12.305, 14.598, -11.852);
        ((GeneralPath) shape).curveTo(15.051, -11.398, 15.277, -10.854, 15.277, -10.219);
        ((GeneralPath) shape).curveTo(15.277, -9.573, 15.051, -9.026, 14.598, -8.578);
        ((GeneralPath) shape).curveTo(14.145, -8.13, 13.592, -7.906, 12.941, -7.906);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(12.949, -8.867);
        ((GeneralPath) shape).curveTo(13.329, -8.867, 13.652, -8.999, 13.918, -9.262);
        ((GeneralPath) shape).curveTo(14.184, -9.525, 14.316, -9.844, 14.316, -10.219);
        ((GeneralPath) shape).curveTo(14.316, -10.589, 14.184, -10.905, 13.918, -11.168);
        ((GeneralPath) shape).curveTo(13.652, -11.431, 13.335, -11.563, 12.965, -11.563);
        ((GeneralPath) shape).curveTo(12.595, -11.563, 12.277, -11.431, 12.012, -11.168);
        ((GeneralPath) shape).curveTo(11.746, -10.905, 11.613, -10.589, 11.613, -10.219);
        ((GeneralPath) shape).curveTo(11.613, -9.849, 11.745, -9.531, 12.008, -9.266);
        ((GeneralPath) shape).curveTo(12.271, -9.0, 12.585, -8.867, 12.949, -8.867);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_41
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 361.121f, 484.25f));

        // _0_1_42

        // _0_1_42_0

        // _0_1_42_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(0.311, -2.428);
        ((GeneralPath) shape).curveTo(0.311, -3.327, 0.561, -3.993, 1.061, -4.426);
        ((GeneralPath) shape).curveTo(1.478, -4.786, 1.987, -4.966, 2.588, -4.966);
        ((GeneralPath) shape).curveTo(3.256, -4.966, 3.801, -4.747, 4.225, -4.309);
        ((GeneralPath) shape).curveTo(4.649, -3.872, 4.86, -3.268, 4.86, -2.497);
        ((GeneralPath) shape).curveTo(4.86, -1.872, 4.767, -1.38, 4.579, -1.022);
        ((GeneralPath) shape).curveTo(4.392, -0.664, 4.119, -0.386, 3.761, -0.187);
        ((GeneralPath) shape).curveTo(3.403, 0.011, 3.012, 0.11, 2.588, 0.11);
        ((GeneralPath) shape).curveTo(1.908, 0.11, 1.359, -0.108, 0.94, -0.544);
        ((GeneralPath) shape).curveTo(0.52, -0.98, 0.311, -1.608, 0.311, -2.428);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(1.157, -2.428);
        ((GeneralPath) shape).curveTo(1.157, -1.806, 1.292, -1.34, 1.564, -1.031);
        ((GeneralPath) shape).curveTo(1.835, -0.722, 2.176, -0.567, 2.588, -0.567);
        ((GeneralPath) shape).curveTo(2.996, -0.567, 3.336, -0.722, 3.608, -1.033);
        ((GeneralPath) shape).curveTo(3.879, -1.344, 4.015, -1.818, 4.015, -2.455);
        ((GeneralPath) shape).curveTo(4.015, -3.056, 3.878, -3.511, 3.605, -3.82);
        ((GeneralPath) shape).curveTo(3.332, -4.13, 2.993, -4.284, 2.588, -4.284);
        ((GeneralPath) shape).curveTo(2.176, -4.284, 1.835, -4.13, 1.564, -3.823);
        ((GeneralPath) shape).curveTo(1.292, -3.515, 1.157, -3.05, 1.157, -2.428);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_42
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 369.478f, 504.786f));

        // _0_1_43

        // _0_1_43_0

        // _0_1_43_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(0.311, -2.428);
        ((GeneralPath) shape).curveTo(0.311, -3.327, 0.561, -3.993, 1.061, -4.426);
        ((GeneralPath) shape).curveTo(1.478, -4.786, 1.987, -4.966, 2.588, -4.966);
        ((GeneralPath) shape).curveTo(3.256, -4.966, 3.801, -4.747, 4.225, -4.309);
        ((GeneralPath) shape).curveTo(4.649, -3.872, 4.86, -3.268, 4.86, -2.497);
        ((GeneralPath) shape).curveTo(4.86, -1.872, 4.767, -1.38, 4.579, -1.022);
        ((GeneralPath) shape).curveTo(4.392, -0.664, 4.119, -0.386, 3.761, -0.187);
        ((GeneralPath) shape).curveTo(3.403, 0.011, 3.012, 0.11, 2.588, 0.11);
        ((GeneralPath) shape).curveTo(1.908, 0.11, 1.359, -0.108, 0.94, -0.544);
        ((GeneralPath) shape).curveTo(0.52, -0.98, 0.311, -1.608, 0.311, -2.428);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(1.157, -2.428);
        ((GeneralPath) shape).curveTo(1.157, -1.806, 1.292, -1.34, 1.564, -1.031);
        ((GeneralPath) shape).curveTo(1.835, -0.722, 2.176, -0.567, 2.588, -0.567);
        ((GeneralPath) shape).curveTo(2.996, -0.567, 3.336, -0.722, 3.608, -1.033);
        ((GeneralPath) shape).curveTo(3.879, -1.344, 4.015, -1.818, 4.015, -2.455);
        ((GeneralPath) shape).curveTo(4.015, -3.056, 3.878, -3.511, 3.605, -3.82);
        ((GeneralPath) shape).curveTo(3.332, -4.13, 2.993, -4.284, 2.588, -4.284);
        ((GeneralPath) shape).curveTo(2.176, -4.284, 1.835, -4.13, 1.564, -3.823);
        ((GeneralPath) shape).curveTo(1.292, -3.515, 1.157, -3.05, 1.157, -2.428);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_43
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 361.799f, 531.929f));

        // _0_1_44

        // _0_1_44_0

        // _0_1_44_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(0.311, -2.428);
        ((GeneralPath) shape).curveTo(0.311, -3.327, 0.561, -3.993, 1.061, -4.426);
        ((GeneralPath) shape).curveTo(1.478, -4.786, 1.987, -4.966, 2.588, -4.966);
        ((GeneralPath) shape).curveTo(3.256, -4.966, 3.801, -4.747, 4.225, -4.309);
        ((GeneralPath) shape).curveTo(4.649, -3.872, 4.86, -3.268, 4.86, -2.497);
        ((GeneralPath) shape).curveTo(4.86, -1.872, 4.767, -1.38, 4.579, -1.022);
        ((GeneralPath) shape).curveTo(4.392, -0.664, 4.119, -0.386, 3.761, -0.187);
        ((GeneralPath) shape).curveTo(3.403, 0.011, 3.012, 0.11, 2.588, 0.11);
        ((GeneralPath) shape).curveTo(1.908, 0.11, 1.359, -0.108, 0.94, -0.544);
        ((GeneralPath) shape).curveTo(0.52, -0.98, 0.311, -1.608, 0.311, -2.428);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(1.157, -2.428);
        ((GeneralPath) shape).curveTo(1.157, -1.806, 1.292, -1.34, 1.564, -1.031);
        ((GeneralPath) shape).curveTo(1.835, -0.722, 2.176, -0.567, 2.588, -0.567);
        ((GeneralPath) shape).curveTo(2.996, -0.567, 3.336, -0.722, 3.608, -1.033);
        ((GeneralPath) shape).curveTo(3.879, -1.344, 4.015, -1.818, 4.015, -2.455);
        ((GeneralPath) shape).curveTo(4.015, -3.056, 3.878, -3.511, 3.605, -3.82);
        ((GeneralPath) shape).curveTo(3.332, -4.13, 2.993, -4.284, 2.588, -4.284);
        ((GeneralPath) shape).curveTo(2.176, -4.284, 1.835, -4.13, 1.564, -3.823);
        ((GeneralPath) shape).curveTo(1.292, -3.515, 1.157, -3.05, 1.157, -2.428);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_44
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 350.906f, 557.607f));

        // _0_1_45

        // _0_1_45_0

        // _0_1_45_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(0.311, -2.428);
        ((GeneralPath) shape).curveTo(0.311, -3.327, 0.561, -3.993, 1.061, -4.426);
        ((GeneralPath) shape).curveTo(1.478, -4.786, 1.987, -4.966, 2.588, -4.966);
        ((GeneralPath) shape).curveTo(3.256, -4.966, 3.801, -4.747, 4.225, -4.309);
        ((GeneralPath) shape).curveTo(4.649, -3.872, 4.86, -3.268, 4.86, -2.497);
        ((GeneralPath) shape).curveTo(4.86, -1.872, 4.767, -1.38, 4.579, -1.022);
        ((GeneralPath) shape).curveTo(4.392, -0.664, 4.119, -0.386, 3.761, -0.187);
        ((GeneralPath) shape).curveTo(3.403, 0.011, 3.012, 0.11, 2.588, 0.11);
        ((GeneralPath) shape).curveTo(1.908, 0.11, 1.359, -0.108, 0.94, -0.544);
        ((GeneralPath) shape).curveTo(0.52, -0.98, 0.311, -1.608, 0.311, -2.428);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(1.157, -2.428);
        ((GeneralPath) shape).curveTo(1.157, -1.806, 1.292, -1.34, 1.564, -1.031);
        ((GeneralPath) shape).curveTo(1.835, -0.722, 2.176, -0.567, 2.588, -0.567);
        ((GeneralPath) shape).curveTo(2.996, -0.567, 3.336, -0.722, 3.608, -1.033);
        ((GeneralPath) shape).curveTo(3.879, -1.344, 4.015, -1.818, 4.015, -2.455);
        ((GeneralPath) shape).curveTo(4.015, -3.056, 3.878, -3.511, 3.605, -3.82);
        ((GeneralPath) shape).curveTo(3.332, -4.13, 2.993, -4.284, 2.588, -4.284);
        ((GeneralPath) shape).curveTo(2.176, -4.284, 1.835, -4.13, 1.564, -3.823);
        ((GeneralPath) shape).curveTo(1.292, -3.515, 1.157, -3.05, 1.157, -2.428);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_45
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 126.325f, 444.13f));

        // _0_1_46

        // _0_1_46_0

        // _0_1_46_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-30.756, -13.626);
        ((GeneralPath) shape).lineTo(-30.756, -8.653);
        ((GeneralPath) shape).lineTo(-23.841, -8.653);
        ((GeneralPath) shape).lineTo(-23.841, -6.712);
        ((GeneralPath) shape).lineTo(-30.756, -6.712);
        ((GeneralPath) shape).lineTo(-30.756, 0.202);
        ((GeneralPath) shape).lineTo(-32.797, 0.202);
        ((GeneralPath) shape).lineTo(-32.797, -15.567);
        ((GeneralPath) shape).lineTo(-21.536, -15.567);
        ((GeneralPath) shape).lineTo(-21.536, -13.626);
        ((GeneralPath) shape).lineTo(-30.756, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_46_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-7.94, -1.739);
        ((GeneralPath) shape).lineTo(-7.94, 0.202);
        ((GeneralPath) shape).lineTo(-19.201, 0.202);
        ((GeneralPath) shape).lineTo(-19.201, -15.567);
        ((GeneralPath) shape).lineTo(-17.159, -15.567);
        ((GeneralPath) shape).lineTo(-17.159, -1.739);
        ((GeneralPath) shape).lineTo(-7.94, -1.739);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_46_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(0.106, -12.939);
        ((GeneralPath) shape).lineTo(-3.867, -6.348);
        ((GeneralPath) shape).lineTo(3.846, -6.348);
        ((GeneralPath) shape).lineTo(0.106, -12.939);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-0.743, -15.567);
        ((GeneralPath) shape).lineTo(0.743, -15.567);
        ((GeneralPath) shape).lineTo(6.03, -6.045);
        ((GeneralPath) shape).lineTo(6.03, 0.202);
        ((GeneralPath) shape).lineTo(3.846, 0.202);
        ((GeneralPath) shape).lineTo(3.846, -4.407);
        ((GeneralPath) shape).lineTo(-3.867, -4.407);
        ((GeneralPath) shape).lineTo(-3.867, 0.202);
        ((GeneralPath) shape).lineTo(-6.05, 0.202);
        ((GeneralPath) shape).lineTo(-6.05, -5.994);
        ((GeneralPath) shape).lineTo(-0.743, -15.567);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_46_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(7.991, -15.567);
        ((GeneralPath) shape).lineTo(16.371, -15.567);
        ((GeneralPath) shape).lineTo(19.252, -12.686);
        ((GeneralPath) shape).lineTo(19.252, -9.593);
        ((GeneralPath) shape).lineTo(16.371, -6.712);
        ((GeneralPath) shape).lineTo(10.033, -6.712);
        ((GeneralPath) shape).lineTo(10.033, 0.202);
        ((GeneralPath) shape).lineTo(7.991, 0.202);
        ((GeneralPath) shape).lineTo(7.991, -15.567);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(10.033, -13.626);
        ((GeneralPath) shape).lineTo(10.033, -8.653);
        ((GeneralPath) shape).lineTo(15.481, -8.653);
        ((GeneralPath) shape).lineTo(17.21, -10.382);
        ((GeneralPath) shape).lineTo(17.21, -11.898);
        ((GeneralPath) shape).lineTo(15.481, -13.626);
        ((GeneralPath) shape).lineTo(10.033, -13.626);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_46_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(24.468, 0.202);
        ((GeneralPath) shape).lineTo(21.739, -2.527);
        ((GeneralPath) shape).lineTo(23.154, -3.942);
        ((GeneralPath) shape).lineTo(25.358, -1.739);
        ((GeneralPath) shape).lineTo(29.078, -1.739);
        ((GeneralPath) shape).lineTo(30.806, -3.467);
        ((GeneralPath) shape).lineTo(30.806, -4.984);
        ((GeneralPath) shape).lineTo(29.078, -6.712);
        ((GeneralPath) shape).lineTo(24.468, -6.712);
        ((GeneralPath) shape).lineTo(21.587, -9.593);
        ((GeneralPath) shape).lineTo(21.587, -12.686);
        ((GeneralPath) shape).lineTo(24.468, -15.567);
        ((GeneralPath) shape).lineTo(29.967, -15.567);
        ((GeneralPath) shape).lineTo(32.696, -12.838);
        ((GeneralPath) shape).lineTo(31.281, -11.423);
        ((GeneralPath) shape).lineTo(29.078, -13.626);
        ((GeneralPath) shape).lineTo(25.358, -13.626);
        ((GeneralPath) shape).lineTo(23.629, -11.898);
        ((GeneralPath) shape).lineTo(23.629, -10.382);
        ((GeneralPath) shape).lineTo(25.358, -8.653);
        ((GeneralPath) shape).lineTo(29.967, -8.653);
        ((GeneralPath) shape).lineTo(32.848, -5.772);
        ((GeneralPath) shape).lineTo(32.848, -2.679);
        ((GeneralPath) shape).lineTo(29.967, 0.202);
        ((GeneralPath) shape).lineTo(24.468, 0.202);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_46
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 721.264f, 489.006f));

        // _0_1_47

        // _0_1_47_0

        // _0_1_47_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-60.758, -17.721);
        ((GeneralPath) shape).lineTo(-51.219, -17.721);
        ((GeneralPath) shape).lineTo(-47.939, -14.442);
        ((GeneralPath) shape).lineTo(-47.939, -10.92);
        ((GeneralPath) shape).lineTo(-51.219, -7.641);
        ((GeneralPath) shape).lineTo(-52.231, -7.641);
        ((GeneralPath) shape).lineTo(-47.536, -0.575);
        ((GeneralPath) shape).lineTo(-49.435, 0.679);
        ((GeneralPath) shape).lineTo(-54.982, -7.641);
        ((GeneralPath) shape).lineTo(-58.434, -7.641);
        ((GeneralPath) shape).lineTo(-58.434, 0.23);
        ((GeneralPath) shape).lineTo(-60.758, 0.23);
        ((GeneralPath) shape).lineTo(-60.758, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-58.434, -15.512);
        ((GeneralPath) shape).lineTo(-58.434, -9.85);
        ((GeneralPath) shape).lineTo(-52.231, -9.85);
        ((GeneralPath) shape).lineTo(-50.264, -11.818);
        ((GeneralPath) shape).lineTo(-50.264, -13.544);
        ((GeneralPath) shape).lineTo(-52.231, -15.512);
        ((GeneralPath) shape).lineTo(-58.434, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_47_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-32.255, -3.049);
        ((GeneralPath) shape).lineTo(-35.534, 0.23);
        ((GeneralPath) shape).lineTo(-41.794, 0.23);
        ((GeneralPath) shape).lineTo(-45.074, -3.049);
        ((GeneralPath) shape).lineTo(-45.074, -14.442);
        ((GeneralPath) shape).lineTo(-41.794, -17.721);
        ((GeneralPath) shape).lineTo(-35.534, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -14.442);
        ((GeneralPath) shape).lineTo(-32.255, -3.049);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-40.782, -15.512);
        ((GeneralPath) shape).lineTo(-42.749, -13.544);
        ((GeneralPath) shape).lineTo(-42.749, -3.947);
        ((GeneralPath) shape).lineTo(-40.782, -1.979);
        ((GeneralPath) shape).lineTo(-36.547, -1.979);
        ((GeneralPath) shape).lineTo(-34.579, -3.947);
        ((GeneralPath) shape).lineTo(-34.579, -13.544);
        ((GeneralPath) shape).lineTo(-36.547, -15.512);
        ((GeneralPath) shape).lineTo(-40.782, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_47_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-16.778, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -17.721);
        ((GeneralPath) shape).lineTo(-27.272, -17.721);
        ((GeneralPath) shape).lineTo(-27.272, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, -1.979);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_47_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-1.3, -1.979);
        ((GeneralPath) shape).lineTo(-1.3, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-11.795, -17.721);
        ((GeneralPath) shape).lineTo(-11.795, -1.979);
        ((GeneralPath) shape).lineTo(-1.3, -1.979);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_47
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 721.264f, 553.275f));

        // _0_1_48

        // _0_1_48_0

        // _0_1_48_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-76.028, -17.721);
        ((GeneralPath) shape).lineTo(-66.489, -17.721);
        ((GeneralPath) shape).lineTo(-63.209, -14.442);
        ((GeneralPath) shape).lineTo(-63.209, -10.92);
        ((GeneralPath) shape).lineTo(-66.489, -7.641);
        ((GeneralPath) shape).lineTo(-73.704, -7.641);
        ((GeneralPath) shape).lineTo(-73.704, 0.23);
        ((GeneralPath) shape).lineTo(-76.028, 0.23);
        ((GeneralPath) shape).lineTo(-76.028, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-73.704, -15.512);
        ((GeneralPath) shape).lineTo(-73.704, -9.85);
        ((GeneralPath) shape).lineTo(-67.501, -9.85);
        ((GeneralPath) shape).lineTo(-65.534, -11.818);
        ((GeneralPath) shape).lineTo(-65.534, -13.544);
        ((GeneralPath) shape).lineTo(-67.501, -15.512);
        ((GeneralPath) shape).lineTo(-73.704, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_48_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-55.327, -15.88);
        ((GeneralPath) shape).lineTo(-59.205, -15.88);
        ((GeneralPath) shape).lineTo(-59.205, -17.721);
        ((GeneralPath) shape).lineTo(-49.09, -17.721);
        ((GeneralPath) shape).lineTo(-49.09, -15.88);
        ((GeneralPath) shape).lineTo(-53.002, -15.88);
        ((GeneralPath) shape).lineTo(-53.002, -1.772);
        ((GeneralPath) shape).lineTo(-49.09, -1.772);
        ((GeneralPath) shape).lineTo(-49.09, 0.23);
        ((GeneralPath) shape).lineTo(-59.205, 0.23);
        ((GeneralPath) shape).lineTo(-59.205, -1.772);
        ((GeneralPath) shape).lineTo(-55.327, -1.772);
        ((GeneralPath) shape).lineTo(-55.327, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_48_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.827, 0.23);
        ((GeneralPath) shape).lineTo(-39.827, -15.512);
        ((GeneralPath) shape).lineTo(-45.074, -15.512);
        ((GeneralPath) shape).lineTo(-45.074, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -17.721);
        ((GeneralPath) shape).lineTo(-32.255, -15.512);
        ((GeneralPath) shape).lineTo(-37.502, -15.512);
        ((GeneralPath) shape).lineTo(-37.502, 0.23);
        ((GeneralPath) shape).lineTo(-39.827, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_48_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-18.469, -13.003);
        ((GeneralPath) shape).lineTo(-20.978, -15.512);
        ((GeneralPath) shape).lineTo(-25.212, -15.512);
        ((GeneralPath) shape).lineTo(-27.18, -13.544);
        ((GeneralPath) shape).lineTo(-27.18, -3.947);
        ((GeneralPath) shape).lineTo(-25.212, -1.979);
        ((GeneralPath) shape).lineTo(-20.978, -1.979);
        ((GeneralPath) shape).lineTo(-18.469, -4.488);
        ((GeneralPath) shape).lineTo(-16.858, -2.877);
        ((GeneralPath) shape).lineTo(-19.965, 0.23);
        ((GeneralPath) shape).lineTo(-26.225, 0.23);
        ((GeneralPath) shape).lineTo(-29.505, -3.049);
        ((GeneralPath) shape).lineTo(-29.505, -14.442);
        ((GeneralPath) shape).lineTo(-26.225, -17.721);
        ((GeneralPath) shape).lineTo(-19.965, -17.721);
        ((GeneralPath) shape).lineTo(-16.858, -14.614);
        ((GeneralPath) shape).lineTo(-18.469, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_48_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-11.795, -17.721);
        ((GeneralPath) shape).lineTo(-11.795, -9.85);
        ((GeneralPath) shape).lineTo(-3.625, -9.85);
        ((GeneralPath) shape).lineTo(-3.625, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, 0.23);
        ((GeneralPath) shape).lineTo(-3.625, 0.23);
        ((GeneralPath) shape).lineTo(-3.625, -7.641);
        ((GeneralPath) shape).lineTo(-11.795, -7.641);
        ((GeneralPath) shape).lineTo(-11.795, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_48
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 721.264f, 617.544f));

        // _0_1_49

        // _0_1_49_0

        // _0_1_49_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-35.603, -17.537);
        ((GeneralPath) shape).lineTo(-33.302, -17.537);
        ((GeneralPath) shape).lineTo(-33.302, -12.623);
        ((GeneralPath) shape).lineTo(-37.514, -8.412);
        ((GeneralPath) shape).lineTo(-37.514, 0.23);
        ((GeneralPath) shape).lineTo(-39.93, 0.23);
        ((GeneralPath) shape).lineTo(-39.93, -8.412);
        ((GeneralPath) shape).lineTo(-44.027, -12.623);
        ((GeneralPath) shape).lineTo(-44.027, -17.537);
        ((GeneralPath) shape).lineTo(-41.748, -17.537);
        ((GeneralPath) shape).lineTo(-41.748, -13.981);
        ((GeneralPath) shape).lineTo(-38.722, -10.817);
        ((GeneralPath) shape).lineTo(-35.603, -13.981);
        ((GeneralPath) shape).lineTo(-35.603, -17.537);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_49_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-23.095, -14.729);
        ((GeneralPath) shape).lineTo(-27.617, -7.227);
        ((GeneralPath) shape).lineTo(-18.837, -7.227);
        ((GeneralPath) shape).lineTo(-23.095, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-24.062, -17.721);
        ((GeneralPath) shape).lineTo(-22.37, -17.721);
        ((GeneralPath) shape).lineTo(-16.352, -6.881);
        ((GeneralPath) shape).lineTo(-16.352, 0.23);
        ((GeneralPath) shape).lineTo(-18.837, 0.23);
        ((GeneralPath) shape).lineTo(-18.837, -5.017);
        ((GeneralPath) shape).lineTo(-27.617, -5.017);
        ((GeneralPath) shape).lineTo(-27.617, 0.23);
        ((GeneralPath) shape).lineTo(-30.103, 0.23);
        ((GeneralPath) shape).lineTo(-30.103, -6.824);
        ((GeneralPath) shape).lineTo(-24.062, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_49_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-11.795, -17.721);
        ((GeneralPath) shape).lineTo(-11.795, -4.683);
        ((GeneralPath) shape).lineTo(-8.607, -9.459);
        ((GeneralPath) shape).lineTo(-6.812, -9.459);
        ((GeneralPath) shape).lineTo(-3.625, -4.683);
        ((GeneralPath) shape).lineTo(-3.625, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, 0.23);
        ((GeneralPath) shape).lineTo(-3.107, 0.23);
        ((GeneralPath) shape).lineTo(-7.71, -6.697);
        ((GeneralPath) shape).lineTo(-12.313, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_49
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 721.264f, 747.241f));

        // _0_1_50

        // _0_1_50_0

        // _0_1_50_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-143.737, -17.721);
        ((GeneralPath) shape).lineTo(-134.197, -17.721);
        ((GeneralPath) shape).lineTo(-130.918, -14.442);
        ((GeneralPath) shape).lineTo(-130.918, -10.92);
        ((GeneralPath) shape).lineTo(-134.197, -7.641);
        ((GeneralPath) shape).lineTo(-141.412, -7.641);
        ((GeneralPath) shape).lineTo(-141.412, 0.23);
        ((GeneralPath) shape).lineTo(-143.737, 0.23);
        ((GeneralPath) shape).lineTo(-143.737, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-141.412, -15.512);
        ((GeneralPath) shape).lineTo(-141.412, -9.85);
        ((GeneralPath) shape).lineTo(-135.21, -9.85);
        ((GeneralPath) shape).lineTo(-133.242, -11.818);
        ((GeneralPath) shape).lineTo(-133.242, -13.544);
        ((GeneralPath) shape).lineTo(-135.21, -15.512);
        ((GeneralPath) shape).lineTo(-141.412, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-123.035, -15.88);
        ((GeneralPath) shape).lineTo(-126.913, -15.88);
        ((GeneralPath) shape).lineTo(-126.913, -17.721);
        ((GeneralPath) shape).lineTo(-116.798, -17.721);
        ((GeneralPath) shape).lineTo(-116.798, -15.88);
        ((GeneralPath) shape).lineTo(-120.711, -15.88);
        ((GeneralPath) shape).lineTo(-120.711, -1.772);
        ((GeneralPath) shape).lineTo(-116.798, -1.772);
        ((GeneralPath) shape).lineTo(-116.798, 0.23);
        ((GeneralPath) shape).lineTo(-126.913, 0.23);
        ((GeneralPath) shape).lineTo(-126.913, -1.772);
        ((GeneralPath) shape).lineTo(-123.035, -1.772);
        ((GeneralPath) shape).lineTo(-123.035, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-107.535, 0.23);
        ((GeneralPath) shape).lineTo(-107.535, -15.512);
        ((GeneralPath) shape).lineTo(-112.782, -15.512);
        ((GeneralPath) shape).lineTo(-112.782, -17.721);
        ((GeneralPath) shape).lineTo(-99.963, -17.721);
        ((GeneralPath) shape).lineTo(-99.963, -15.512);
        ((GeneralPath) shape).lineTo(-105.211, -15.512);
        ((GeneralPath) shape).lineTo(-105.211, 0.23);
        ((GeneralPath) shape).lineTo(-107.535, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-86.178, -13.003);
        ((GeneralPath) shape).lineTo(-88.686, -15.512);
        ((GeneralPath) shape).lineTo(-92.921, -15.512);
        ((GeneralPath) shape).lineTo(-94.889, -13.544);
        ((GeneralPath) shape).lineTo(-94.889, -3.947);
        ((GeneralPath) shape).lineTo(-92.921, -1.979);
        ((GeneralPath) shape).lineTo(-88.686, -1.979);
        ((GeneralPath) shape).lineTo(-86.178, -4.488);
        ((GeneralPath) shape).lineTo(-84.567, -2.877);
        ((GeneralPath) shape).lineTo(-87.674, 0.23);
        ((GeneralPath) shape).lineTo(-93.934, 0.23);
        ((GeneralPath) shape).lineTo(-97.213, -3.049);
        ((GeneralPath) shape).lineTo(-97.213, -14.442);
        ((GeneralPath) shape).lineTo(-93.934, -17.721);
        ((GeneralPath) shape).lineTo(-87.674, -17.721);
        ((GeneralPath) shape).lineTo(-84.567, -14.614);
        ((GeneralPath) shape).lineTo(-86.178, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-81.828, -17.721);
        ((GeneralPath) shape).lineTo(-79.503, -17.721);
        ((GeneralPath) shape).lineTo(-79.503, -9.85);
        ((GeneralPath) shape).lineTo(-71.333, -9.85);
        ((GeneralPath) shape).lineTo(-71.333, -17.721);
        ((GeneralPath) shape).lineTo(-69.009, -17.721);
        ((GeneralPath) shape).lineTo(-69.009, 0.23);
        ((GeneralPath) shape).lineTo(-71.333, 0.23);
        ((GeneralPath) shape).lineTo(-71.333, -7.641);
        ((GeneralPath) shape).lineTo(-79.503, -7.641);
        ((GeneralPath) shape).lineTo(-79.503, 0.23);
        ((GeneralPath) shape).lineTo(-81.828, 0.23);
        ((GeneralPath) shape).lineTo(-81.828, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-55.304, 0.23);
        ((GeneralPath) shape).lineTo(-55.304, -15.512);
        ((GeneralPath) shape).lineTo(-60.551, -15.512);
        ((GeneralPath) shape).lineTo(-60.551, -17.721);
        ((GeneralPath) shape).lineTo(-47.732, -17.721);
        ((GeneralPath) shape).lineTo(-47.732, -15.512);
        ((GeneralPath) shape).lineTo(-52.979, -15.512);
        ((GeneralPath) shape).lineTo(-52.979, 0.23);
        ((GeneralPath) shape).lineTo(-55.304, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-45.281, -17.721);
        ((GeneralPath) shape).lineTo(-35.741, -17.721);
        ((GeneralPath) shape).lineTo(-32.462, -14.442);
        ((GeneralPath) shape).lineTo(-32.462, -10.92);
        ((GeneralPath) shape).lineTo(-35.741, -7.641);
        ((GeneralPath) shape).lineTo(-36.754, -7.641);
        ((GeneralPath) shape).lineTo(-32.059, -0.575);
        ((GeneralPath) shape).lineTo(-33.958, 0.679);
        ((GeneralPath) shape).lineTo(-39.504, -7.641);
        ((GeneralPath) shape).lineTo(-42.957, -7.641);
        ((GeneralPath) shape).lineTo(-42.957, 0.23);
        ((GeneralPath) shape).lineTo(-45.281, 0.23);
        ((GeneralPath) shape).lineTo(-45.281, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-42.957, -15.512);
        ((GeneralPath) shape).lineTo(-42.957, -9.85);
        ((GeneralPath) shape).lineTo(-36.754, -9.85);
        ((GeneralPath) shape).lineTo(-34.786, -11.818);
        ((GeneralPath) shape).lineTo(-34.786, -13.544);
        ((GeneralPath) shape).lineTo(-36.754, -15.512);
        ((GeneralPath) shape).lineTo(-42.957, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_7
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-24.372, -15.88);
        ((GeneralPath) shape).lineTo(-28.25, -15.88);
        ((GeneralPath) shape).lineTo(-28.25, -17.721);
        ((GeneralPath) shape).lineTo(-18.135, -17.721);
        ((GeneralPath) shape).lineTo(-18.135, -15.88);
        ((GeneralPath) shape).lineTo(-22.048, -15.88);
        ((GeneralPath) shape).lineTo(-22.048, -1.772);
        ((GeneralPath) shape).lineTo(-18.135, -1.772);
        ((GeneralPath) shape).lineTo(-18.135, 0.23);
        ((GeneralPath) shape).lineTo(-28.25, 0.23);
        ((GeneralPath) shape).lineTo(-28.25, -1.772);
        ((GeneralPath) shape).lineTo(-24.372, -1.772);
        ((GeneralPath) shape).lineTo(-24.372, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_50_0_8
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-12.313, -17.721);
        ((GeneralPath) shape).lineTo(-7.71, -10.794);
        ((GeneralPath) shape).lineTo(-3.107, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, 0.23);
        ((GeneralPath) shape).lineTo(-3.625, 0.23);
        ((GeneralPath) shape).lineTo(-3.625, -12.808);
        ((GeneralPath) shape).lineTo(-6.812, -8.032);
        ((GeneralPath) shape).lineTo(-8.607, -8.032);
        ((GeneralPath) shape).lineTo(-11.795, -12.808);
        ((GeneralPath) shape).lineTo(-11.795, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_50
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 721.264f, 683.719f));

        // _0_1_51

        // _0_1_51_0

        // _0_1_51_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-112.99, -17.721);
        ((GeneralPath) shape).lineTo(-103.45, -17.721);
        ((GeneralPath) shape).lineTo(-100.17, -14.442);
        ((GeneralPath) shape).lineTo(-100.17, -10.92);
        ((GeneralPath) shape).lineTo(-103.45, -7.641);
        ((GeneralPath) shape).lineTo(-104.463, -7.641);
        ((GeneralPath) shape).lineTo(-99.768, -0.575);
        ((GeneralPath) shape).lineTo(-101.666, 0.679);
        ((GeneralPath) shape).lineTo(-107.213, -7.641);
        ((GeneralPath) shape).lineTo(-110.665, -7.641);
        ((GeneralPath) shape).lineTo(-110.665, 0.23);
        ((GeneralPath) shape).lineTo(-112.99, 0.23);
        ((GeneralPath) shape).lineTo(-112.99, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-110.665, -15.512);
        ((GeneralPath) shape).lineTo(-110.665, -9.85);
        ((GeneralPath) shape).lineTo(-104.463, -9.85);
        ((GeneralPath) shape).lineTo(-102.495, -11.818);
        ((GeneralPath) shape).lineTo(-102.495, -13.544);
        ((GeneralPath) shape).lineTo(-104.463, -15.512);
        ((GeneralPath) shape).lineTo(-110.665, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_51_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-94.981, -17.721);
        ((GeneralPath) shape).lineTo(-94.981, -3.947);
        ((GeneralPath) shape).lineTo(-93.013, -1.979);
        ((GeneralPath) shape).lineTo(-88.778, -1.979);
        ((GeneralPath) shape).lineTo(-86.811, -3.947);
        ((GeneralPath) shape).lineTo(-86.811, -17.721);
        ((GeneralPath) shape).lineTo(-84.486, -17.721);
        ((GeneralPath) shape).lineTo(-84.486, -3.049);
        ((GeneralPath) shape).lineTo(-87.766, 0.23);
        ((GeneralPath) shape).lineTo(-94.026, 0.23);
        ((GeneralPath) shape).lineTo(-97.305, -3.049);
        ((GeneralPath) shape).lineTo(-97.305, -17.721);
        ((GeneralPath) shape).lineTo(-94.981, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_51_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-81.471, -17.79);
        ((GeneralPath) shape).lineTo(-72.657, -17.79);
        ((GeneralPath) shape).lineTo(-69.377, -14.488);
        ((GeneralPath) shape).lineTo(-69.377, -3.026);
        ((GeneralPath) shape).lineTo(-72.657, 0.276);
        ((GeneralPath) shape).lineTo(-81.471, 0.276);
        ((GeneralPath) shape).lineTo(-81.471, -17.79);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-79.227, -15.592);
        ((GeneralPath) shape).lineTo(-79.227, -1.864);
        ((GeneralPath) shape).lineTo(-73.692, -1.864);
        ((GeneralPath) shape).lineTo(-71.725, -3.866);
        ((GeneralPath) shape).lineTo(-71.725, -13.59);
        ((GeneralPath) shape).lineTo(-73.692, -15.592);
        ((GeneralPath) shape).lineTo(-79.227, -15.592);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_51_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-55.304, 0.23);
        ((GeneralPath) shape).lineTo(-55.304, -15.512);
        ((GeneralPath) shape).lineTo(-60.551, -15.512);
        ((GeneralPath) shape).lineTo(-60.551, -17.721);
        ((GeneralPath) shape).lineTo(-47.732, -17.721);
        ((GeneralPath) shape).lineTo(-47.732, -15.512);
        ((GeneralPath) shape).lineTo(-52.979, -15.512);
        ((GeneralPath) shape).lineTo(-52.979, 0.23);
        ((GeneralPath) shape).lineTo(-55.304, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_51_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-45.281, -17.721);
        ((GeneralPath) shape).lineTo(-35.741, -17.721);
        ((GeneralPath) shape).lineTo(-32.462, -14.442);
        ((GeneralPath) shape).lineTo(-32.462, -10.92);
        ((GeneralPath) shape).lineTo(-35.741, -7.641);
        ((GeneralPath) shape).lineTo(-36.754, -7.641);
        ((GeneralPath) shape).lineTo(-32.059, -0.575);
        ((GeneralPath) shape).lineTo(-33.958, 0.679);
        ((GeneralPath) shape).lineTo(-39.504, -7.641);
        ((GeneralPath) shape).lineTo(-42.957, -7.641);
        ((GeneralPath) shape).lineTo(-42.957, 0.23);
        ((GeneralPath) shape).lineTo(-45.281, 0.23);
        ((GeneralPath) shape).lineTo(-45.281, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-42.957, -15.512);
        ((GeneralPath) shape).lineTo(-42.957, -9.85);
        ((GeneralPath) shape).lineTo(-36.754, -9.85);
        ((GeneralPath) shape).lineTo(-34.786, -11.818);
        ((GeneralPath) shape).lineTo(-34.786, -13.544);
        ((GeneralPath) shape).lineTo(-36.754, -15.512);
        ((GeneralPath) shape).lineTo(-42.957, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_51_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-24.372, -15.88);
        ((GeneralPath) shape).lineTo(-28.25, -15.88);
        ((GeneralPath) shape).lineTo(-28.25, -17.721);
        ((GeneralPath) shape).lineTo(-18.135, -17.721);
        ((GeneralPath) shape).lineTo(-18.135, -15.88);
        ((GeneralPath) shape).lineTo(-22.048, -15.88);
        ((GeneralPath) shape).lineTo(-22.048, -1.772);
        ((GeneralPath) shape).lineTo(-18.135, -1.772);
        ((GeneralPath) shape).lineTo(-18.135, 0.23);
        ((GeneralPath) shape).lineTo(-28.25, 0.23);
        ((GeneralPath) shape).lineTo(-28.25, -1.772);
        ((GeneralPath) shape).lineTo(-24.372, -1.772);
        ((GeneralPath) shape).lineTo(-24.372, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_51_0_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-12.313, -17.721);
        ((GeneralPath) shape).lineTo(-7.71, -10.794);
        ((GeneralPath) shape).lineTo(-3.107, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, 0.23);
        ((GeneralPath) shape).lineTo(-3.625, 0.23);
        ((GeneralPath) shape).lineTo(-3.625, -12.808);
        ((GeneralPath) shape).lineTo(-6.812, -8.032);
        ((GeneralPath) shape).lineTo(-8.607, -8.032);
        ((GeneralPath) shape).lineTo(-11.795, -12.808);
        ((GeneralPath) shape).lineTo(-11.795, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, 0.23);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_51
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 721.264f, 810.351f));

        // _0_1_52

        // _0_1_52_0

        // _0_1_52_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-73.704, -15.512);
        ((GeneralPath) shape).lineTo(-73.704, -9.85);
        ((GeneralPath) shape).lineTo(-65.833, -9.85);
        ((GeneralPath) shape).lineTo(-65.833, -7.641);
        ((GeneralPath) shape).lineTo(-73.704, -7.641);
        ((GeneralPath) shape).lineTo(-73.704, 0.23);
        ((GeneralPath) shape).lineTo(-76.028, 0.23);
        ((GeneralPath) shape).lineTo(-76.028, -17.721);
        ((GeneralPath) shape).lineTo(-63.209, -17.721);
        ((GeneralPath) shape).lineTo(-63.209, -15.512);
        ((GeneralPath) shape).lineTo(-73.704, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_52_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-47.732, -1.979);
        ((GeneralPath) shape).lineTo(-47.732, 0.23);
        ((GeneralPath) shape).lineTo(-60.551, 0.23);
        ((GeneralPath) shape).lineTo(-60.551, -17.721);
        ((GeneralPath) shape).lineTo(-58.227, -17.721);
        ((GeneralPath) shape).lineTo(-58.227, -1.979);
        ((GeneralPath) shape).lineTo(-47.732, -1.979);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_52_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-38.572, -14.729);
        ((GeneralPath) shape).lineTo(-43.095, -7.227);
        ((GeneralPath) shape).lineTo(-34.315, -7.227);
        ((GeneralPath) shape).lineTo(-38.572, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-39.539, -17.721);
        ((GeneralPath) shape).lineTo(-37.847, -17.721);
        ((GeneralPath) shape).lineTo(-31.829, -6.881);
        ((GeneralPath) shape).lineTo(-31.829, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, -6.824);
        ((GeneralPath) shape).lineTo(-39.539, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_52_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-29.597, -17.721);
        ((GeneralPath) shape).lineTo(-20.057, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, -14.442);
        ((GeneralPath) shape).lineTo(-16.778, -10.92);
        ((GeneralPath) shape).lineTo(-20.057, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-27.272, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -9.85);
        ((GeneralPath) shape).lineTo(-21.07, -9.85);
        ((GeneralPath) shape).lineTo(-19.102, -11.818);
        ((GeneralPath) shape).lineTo(-19.102, -13.544);
        ((GeneralPath) shape).lineTo(-21.07, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_52_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-10.84, 0.23);
        ((GeneralPath) shape).lineTo(-13.947, -2.877);
        ((GeneralPath) shape).lineTo(-12.336, -4.488);
        ((GeneralPath) shape).lineTo(-9.827, -1.979);
        ((GeneralPath) shape).lineTo(-5.593, -1.979);
        ((GeneralPath) shape).lineTo(-3.625, -3.947);
        ((GeneralPath) shape).lineTo(-3.625, -5.673);
        ((GeneralPath) shape).lineTo(-5.593, -7.641);
        ((GeneralPath) shape).lineTo(-10.84, -7.641);
        ((GeneralPath) shape).lineTo(-14.119, -10.92);
        ((GeneralPath) shape).lineTo(-14.119, -14.442);
        ((GeneralPath) shape).lineTo(-10.84, -17.721);
        ((GeneralPath) shape).lineTo(-4.58, -17.721);
        ((GeneralPath) shape).lineTo(-1.473, -14.614);
        ((GeneralPath) shape).lineTo(-3.084, -13.003);
        ((GeneralPath) shape).lineTo(-5.593, -15.512);
        ((GeneralPath) shape).lineTo(-9.827, -15.512);
        ((GeneralPath) shape).lineTo(-11.795, -13.544);
        ((GeneralPath) shape).lineTo(-11.795, -11.818);
        ((GeneralPath) shape).lineTo(-9.827, -9.85);
        ((GeneralPath) shape).lineTo(-4.58, -9.85);
        ((GeneralPath) shape).lineTo(-1.3, -6.571);
        ((GeneralPath) shape).lineTo(-1.3, -3.049);
        ((GeneralPath) shape).lineTo(-4.58, 0.23);
        ((GeneralPath) shape).lineTo(-10.84, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_52
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 721.264f, 874.62f));

        // _0_1_53

        // _0_1_53_0

        // _0_1_53_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-88.433, -7.641);
        ((GeneralPath) shape).lineTo(-88.433, -1.979);
        ((GeneralPath) shape).lineTo(-83.623, -1.979);
        ((GeneralPath) shape).lineTo(-81.655, -3.947);
        ((GeneralPath) shape).lineTo(-81.655, -5.673);
        ((GeneralPath) shape).lineTo(-83.623, -7.641);
        ((GeneralPath) shape).lineTo(-88.433, -7.641);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-90.873, -17.721);
        ((GeneralPath) shape).lineTo(-82.61, -17.721);
        ((GeneralPath) shape).lineTo(-79.331, -14.442);
        ((GeneralPath) shape).lineTo(-79.331, -10.92);
        ((GeneralPath) shape).lineTo(-81.506, -8.745);
        ((GeneralPath) shape).lineTo(-79.331, -6.571);
        ((GeneralPath) shape).lineTo(-79.331, -3.049);
        ((GeneralPath) shape).lineTo(-82.61, 0.23);
        ((GeneralPath) shape).lineTo(-90.873, 0.23);
        ((GeneralPath) shape).lineTo(-90.873, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-88.41, -15.512);
        ((GeneralPath) shape).lineTo(-88.41, -9.85);
        ((GeneralPath) shape).lineTo(-83.623, -9.85);
        ((GeneralPath) shape).lineTo(-81.655, -11.818);
        ((GeneralPath) shape).lineTo(-81.655, -13.544);
        ((GeneralPath) shape).lineTo(-83.623, -15.512);
        ((GeneralPath) shape).lineTo(-88.41, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_53_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-76.235, -17.721);
        ((GeneralPath) shape).lineTo(-66.696, -17.721);
        ((GeneralPath) shape).lineTo(-63.416, -14.442);
        ((GeneralPath) shape).lineTo(-63.416, -10.92);
        ((GeneralPath) shape).lineTo(-66.696, -7.641);
        ((GeneralPath) shape).lineTo(-67.709, -7.641);
        ((GeneralPath) shape).lineTo(-63.014, -0.575);
        ((GeneralPath) shape).lineTo(-64.912, 0.679);
        ((GeneralPath) shape).lineTo(-70.459, -7.641);
        ((GeneralPath) shape).lineTo(-73.911, -7.641);
        ((GeneralPath) shape).lineTo(-73.911, 0.23);
        ((GeneralPath) shape).lineTo(-76.235, 0.23);
        ((GeneralPath) shape).lineTo(-76.235, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-73.911, -15.512);
        ((GeneralPath) shape).lineTo(-73.911, -9.85);
        ((GeneralPath) shape).lineTo(-67.709, -9.85);
        ((GeneralPath) shape).lineTo(-65.741, -11.818);
        ((GeneralPath) shape).lineTo(-65.741, -13.544);
        ((GeneralPath) shape).lineTo(-67.709, -15.512);
        ((GeneralPath) shape).lineTo(-73.911, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_53_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-54.049, -14.729);
        ((GeneralPath) shape).lineTo(-58.572, -7.227);
        ((GeneralPath) shape).lineTo(-49.792, -7.227);
        ((GeneralPath) shape).lineTo(-54.049, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-55.016, -17.721);
        ((GeneralPath) shape).lineTo(-53.325, -17.721);
        ((GeneralPath) shape).lineTo(-47.306, -6.881);
        ((GeneralPath) shape).lineTo(-47.306, 0.23);
        ((GeneralPath) shape).lineTo(-49.792, 0.23);
        ((GeneralPath) shape).lineTo(-49.792, -5.017);
        ((GeneralPath) shape).lineTo(-58.572, -5.017);
        ((GeneralPath) shape).lineTo(-58.572, 0.23);
        ((GeneralPath) shape).lineTo(-61.057, 0.23);
        ((GeneralPath) shape).lineTo(-61.057, -6.824);
        ((GeneralPath) shape).lineTo(-55.016, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_53_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-32.025, -0.875);
        ((GeneralPath) shape).lineTo(-33.636, 0.725);
        ((GeneralPath) shape).lineTo(-41.507, -7.134);
        ((GeneralPath) shape).lineTo(-42.968, -5.673);
        ((GeneralPath) shape).lineTo(-42.968, 0.23);
        ((GeneralPath) shape).lineTo(-45.292, 0.23);
        ((GeneralPath) shape).lineTo(-45.292, -17.721);
        ((GeneralPath) shape).lineTo(-42.968, -17.721);
        ((GeneralPath) shape).lineTo(-42.968, -8.895);
        ((GeneralPath) shape).lineTo(-33.636, -18.227);
        ((GeneralPath) shape).lineTo(-32.025, -16.616);
        ((GeneralPath) shape).lineTo(-39.896, -8.745);
        ((GeneralPath) shape).lineTo(-32.025, -0.875);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_53_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-27.272, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -9.85);
        ((GeneralPath) shape).lineTo(-19.401, -9.85);
        ((GeneralPath) shape).lineTo(-19.401, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, -7.641);
        ((GeneralPath) shape).lineTo(-27.272, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, -1.979);
        ((GeneralPath) shape).lineTo(-16.778, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, 0.23);
        ((GeneralPath) shape).lineTo(-29.597, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, -17.721);
        ((GeneralPath) shape).lineTo(-16.778, -15.512);
        ((GeneralPath) shape).lineTo(-27.272, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_53_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-10.84, 0.23);
        ((GeneralPath) shape).lineTo(-13.947, -2.877);
        ((GeneralPath) shape).lineTo(-12.336, -4.488);
        ((GeneralPath) shape).lineTo(-9.827, -1.979);
        ((GeneralPath) shape).lineTo(-5.593, -1.979);
        ((GeneralPath) shape).lineTo(-3.625, -3.947);
        ((GeneralPath) shape).lineTo(-3.625, -5.673);
        ((GeneralPath) shape).lineTo(-5.593, -7.641);
        ((GeneralPath) shape).lineTo(-10.84, -7.641);
        ((GeneralPath) shape).lineTo(-14.119, -10.92);
        ((GeneralPath) shape).lineTo(-14.119, -14.442);
        ((GeneralPath) shape).lineTo(-10.84, -17.721);
        ((GeneralPath) shape).lineTo(-4.58, -17.721);
        ((GeneralPath) shape).lineTo(-1.473, -14.614);
        ((GeneralPath) shape).lineTo(-3.084, -13.003);
        ((GeneralPath) shape).lineTo(-5.593, -15.512);
        ((GeneralPath) shape).lineTo(-9.827, -15.512);
        ((GeneralPath) shape).lineTo(-11.795, -13.544);
        ((GeneralPath) shape).lineTo(-11.795, -11.818);
        ((GeneralPath) shape).lineTo(-9.827, -9.85);
        ((GeneralPath) shape).lineTo(-4.58, -9.85);
        ((GeneralPath) shape).lineTo(-1.3, -6.571);
        ((GeneralPath) shape).lineTo(-1.3, -3.049);
        ((GeneralPath) shape).lineTo(-4.58, 0.23);
        ((GeneralPath) shape).lineTo(-10.84, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_53
        paint3(g, origAlpha, transformations);
    }

    private static void paint3(Graphics2D g, float origAlpha, java.util.LinkedList<AffineTransform> transformations) {
        Shape shape = null;

        // _0_1_54
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(347.374, 613.928);
        ((GeneralPath) shape).lineTo(347.503, 613.986);
        ((GeneralPath) shape).lineTo(347.631, 614.046);
        ((GeneralPath) shape).lineTo(347.757, 614.109);
        ((GeneralPath) shape).lineTo(347.881, 614.175);
        ((GeneralPath) shape).lineTo(348.002, 614.242);
        ((GeneralPath) shape).lineTo(348.121, 614.312);
        ((GeneralPath) shape).lineTo(348.239, 614.384);
        ((GeneralPath) shape).lineTo(348.355, 614.458);
        ((GeneralPath) shape).lineTo(348.469, 614.534);
        ((GeneralPath) shape).lineTo(348.581, 614.612);
        ((GeneralPath) shape).lineTo(348.691, 614.692);
        ((GeneralPath) shape).lineTo(348.799, 614.773);
        ((GeneralPath) shape).lineTo(348.904, 614.856);
        ((GeneralPath) shape).lineTo(349.009, 614.941);
        ((GeneralPath) shape).lineTo(349.111, 615.027);
        ((GeneralPath) shape).lineTo(349.211, 615.115);
        ((GeneralPath) shape).lineTo(349.309, 615.204);
        ((GeneralPath) shape).lineTo(349.406, 615.295);
        ((GeneralPath) shape).lineTo(349.501, 615.387);
        ((GeneralPath) shape).lineTo(349.594, 615.48);
        ((GeneralPath) shape).lineTo(349.685, 615.574);
        ((GeneralPath) shape).lineTo(349.774, 615.67);
        ((GeneralPath) shape).lineTo(349.861, 615.766);
        ((GeneralPath) shape).lineTo(349.947, 615.863);
        ((GeneralPath) shape).lineTo(350.032, 615.962);
        ((GeneralPath) shape).lineTo(350.115, 616.062);
        ((GeneralPath) shape).lineTo(350.195, 616.162);
        ((GeneralPath) shape).lineTo(350.275, 616.263);
        ((GeneralPath) shape).lineTo(350.353, 616.365);
        ((GeneralPath) shape).lineTo(350.429, 616.467);
        ((GeneralPath) shape).lineTo(350.504, 616.57);
        ((GeneralPath) shape).lineTo(350.577, 616.674);
        ((GeneralPath) shape).lineTo(350.648, 616.779);
        ((GeneralPath) shape).lineTo(350.719, 616.884);
        ((GeneralPath) shape).lineTo(350.788, 616.99);
        ((GeneralPath) shape).lineTo(350.855, 617.096);
        ((GeneralPath) shape).lineTo(350.921, 617.202);
        ((GeneralPath) shape).lineTo(350.986, 617.309);
        ((GeneralPath) shape).lineTo(351.05, 617.416);
        ((GeneralPath) shape).lineTo(351.112, 617.524);
        ((GeneralPath) shape).lineTo(351.173, 617.632);
        ((GeneralPath) shape).lineTo(351.233, 617.74);
        ((GeneralPath) shape).lineTo(351.291, 617.848);
        ((GeneralPath) shape).lineTo(351.348, 617.956);
        ((GeneralPath) shape).lineTo(351.404, 618.065);
        ((GeneralPath) shape).lineTo(351.459, 618.174);
        ((GeneralPath) shape).lineTo(351.513, 618.283);
        ((GeneralPath) shape).lineTo(351.566, 618.391);
        ((GeneralPath) shape).lineTo(351.617, 618.501);
        ((GeneralPath) shape).lineTo(351.668, 618.609);
        ((GeneralPath) shape).lineTo(351.717, 618.718);
        ((GeneralPath) shape).lineTo(351.766, 618.827);
        ((GeneralPath) shape).lineTo(351.813, 618.935);
        ((GeneralPath) shape).lineTo(351.859, 619.044);
        ((GeneralPath) shape).lineTo(351.906, 619.154);
        ((GeneralPath) shape).lineTo(351.992, 619.367);
        ((GeneralPath) shape).lineTo(352.076, 619.581);
        ((GeneralPath) shape).lineTo(352.157, 619.794);
        ((GeneralPath) shape).lineTo(352.233, 620.005);
        ((GeneralPath) shape).lineTo(352.307, 620.214);
        ((GeneralPath) shape).lineTo(352.377, 620.42);
        ((GeneralPath) shape).lineTo(352.444, 620.624);
        ((GeneralPath) shape).lineTo(352.508, 620.825);
        ((GeneralPath) shape).lineTo(352.569, 621.022);
        ((GeneralPath) shape).lineTo(352.627, 621.216);
        ((GeneralPath) shape).lineTo(352.682, 621.407);
        ((GeneralPath) shape).lineTo(352.735, 621.593);
        ((GeneralPath) shape).lineTo(352.785, 621.774);
        ((GeneralPath) shape).lineTo(352.833, 621.951);
        ((GeneralPath) shape).lineTo(352.878, 622.123);
        ((GeneralPath) shape).lineTo(352.921, 622.29);
        ((GeneralPath) shape).lineTo(353.0, 622.603);
        ((GeneralPath) shape).lineTo(353.071, 622.892);
        ((GeneralPath) shape).lineTo(353.09, 622.972);
        ((GeneralPath) shape).lineTo(353.546, 622.99);
        ((GeneralPath) shape).lineTo(354.015, 623.011);
        ((GeneralPath) shape).lineTo(354.465, 623.033);
        ((GeneralPath) shape).lineTo(354.686, 623.046);
        ((GeneralPath) shape).lineTo(354.901, 623.058);
        ((GeneralPath) shape).lineTo(355.114, 623.072);
        ((GeneralPath) shape).lineTo(355.321, 623.086);
        ((GeneralPath) shape).lineTo(355.526, 623.1);
        ((GeneralPath) shape).lineTo(355.727, 623.116);
        ((GeneralPath) shape).lineTo(355.924, 623.132);
        ((GeneralPath) shape).lineTo(356.119, 623.149);
        ((GeneralPath) shape).lineTo(356.31, 623.167);
        ((GeneralPath) shape).lineTo(356.497, 623.186);
        ((GeneralPath) shape).lineTo(356.682, 623.205);
        ((GeneralPath) shape).lineTo(356.863, 623.226);
        ((GeneralPath) shape).lineTo(357.043, 623.248);
        ((GeneralPath) shape).lineTo(357.219, 623.271);
        ((GeneralPath) shape).lineTo(357.392, 623.294);
        ((GeneralPath) shape).lineTo(357.564, 623.319);
        ((GeneralPath) shape).lineTo(357.732, 623.346);
        ((GeneralPath) shape).lineTo(357.898, 623.373);
        ((GeneralPath) shape).lineTo(358.062, 623.402);
        ((GeneralPath) shape).lineTo(358.225, 623.432);
        ((GeneralPath) shape).lineTo(358.384, 623.464);
        ((GeneralPath) shape).lineTo(358.542, 623.497);
        ((GeneralPath) shape).lineTo(358.698, 623.531);
        ((GeneralPath) shape).lineTo(358.852, 623.568);
        ((GeneralPath) shape).lineTo(359.004, 623.606);
        ((GeneralPath) shape).lineTo(359.154, 623.645);
        ((GeneralPath) shape).lineTo(359.303, 623.686);
        ((GeneralPath) shape).lineTo(359.45, 623.729);
        ((GeneralPath) shape).lineTo(359.596, 623.774);
        ((GeneralPath) shape).lineTo(359.739, 623.82);
        ((GeneralPath) shape).lineTo(359.882, 623.869);
        ((GeneralPath) shape).lineTo(360.023, 623.92);
        ((GeneralPath) shape).lineTo(360.162, 623.972);
        ((GeneralPath) shape).lineTo(360.301, 624.026);
        ((GeneralPath) shape).lineTo(360.437, 624.083);
        ((GeneralPath) shape).lineTo(360.572, 624.141);
        ((GeneralPath) shape).lineTo(360.706, 624.202);
        ((GeneralPath) shape).lineTo(360.839, 624.264);
        ((GeneralPath) shape).lineTo(360.97, 624.328);
        ((GeneralPath) shape).lineTo(361.1, 624.395);
        ((GeneralPath) shape).lineTo(361.228, 624.463);
        ((GeneralPath) shape).lineTo(361.355, 624.534);
        ((GeneralPath) shape).lineTo(361.482, 624.607);
        ((GeneralPath) shape).lineTo(361.607, 624.681);
        ((GeneralPath) shape).lineTo(361.73, 624.757);
        ((GeneralPath) shape).lineTo(361.853, 624.836);
        ((GeneralPath) shape).lineTo(361.974, 624.916);
        ((GeneralPath) shape).lineTo(362.094, 624.998);
        ((GeneralPath) shape).lineTo(362.213, 625.083);
        ((GeneralPath) shape).lineTo(362.332, 625.169);
        ((GeneralPath) shape).lineTo(362.45, 625.257);
        ((GeneralPath) shape).lineTo(362.566, 625.346);
        ((GeneralPath) shape).lineTo(362.682, 625.438);
        ((GeneralPath) shape).lineTo(363.259, 625.904);
        ((GeneralPath) shape).lineTo(363.358, 626.676);
        ((GeneralPath) shape).lineTo(363.388, 626.915);
        ((GeneralPath) shape).lineTo(363.464, 627.557);
        ((GeneralPath) shape).lineTo(363.564, 628.414);
        ((GeneralPath) shape).lineTo(363.684, 629.47);
        ((GeneralPath) shape).lineTo(363.825, 630.71);
        ((GeneralPath) shape).lineTo(363.984, 632.119);
        ((GeneralPath) shape).lineTo(364.349, 635.376);
        ((GeneralPath) shape).lineTo(364.765, 639.114);
        ((GeneralPath) shape).lineTo(365.218, 643.203);
        ((GeneralPath) shape).lineTo(365.695, 647.516);
        ((GeneralPath) shape).lineTo(366.181, 651.921);
        ((GeneralPath) shape).lineTo(367.126, 660.498);
        ((GeneralPath) shape).lineTo(367.939, 667.902);
        ((GeneralPath) shape).lineTo(368.623, 674.137);
        ((GeneralPath) shape).lineTo(393.12, 683.064);
        ((GeneralPath) shape).lineTo(504.39, 683.064);
        ((GeneralPath) shape).lineTo(504.401, 683.063);
        ((GeneralPath) shape).lineTo(504.472, 683.062);
        ((GeneralPath) shape).lineTo(504.553, 683.062);
        ((GeneralPath) shape).lineTo(504.647, 683.066);
        ((GeneralPath) shape).lineTo(504.752, 683.072);
        ((GeneralPath) shape).lineTo(504.869, 683.082);
        ((GeneralPath) shape).lineTo(504.997, 683.097);
        ((GeneralPath) shape).lineTo(505.131, 683.117);
        ((GeneralPath) shape).lineTo(505.21, 683.13);
        ((GeneralPath) shape).lineTo(505.286, 683.144);
        ((GeneralPath) shape).lineTo(505.364, 683.16);
        ((GeneralPath) shape).lineTo(505.445, 683.178);
        ((GeneralPath) shape).lineTo(505.528, 683.197);
        ((GeneralPath) shape).lineTo(505.613, 683.219);
        ((GeneralPath) shape).lineTo(505.7, 683.242);
        ((GeneralPath) shape).lineTo(505.789, 683.268);
        ((GeneralPath) shape).lineTo(505.881, 683.296);
        ((GeneralPath) shape).lineTo(505.975, 683.327);
        ((GeneralPath) shape).lineTo(506.07, 683.359);
        ((GeneralPath) shape).lineTo(506.167, 683.394);
        ((GeneralPath) shape).lineTo(506.266, 683.432);
        ((GeneralPath) shape).lineTo(506.367, 683.473);
        ((GeneralPath) shape).lineTo(506.469, 683.516);
        ((GeneralPath) shape).lineTo(506.572, 683.562);
        ((GeneralPath) shape).lineTo(506.677, 683.61);
        ((GeneralPath) shape).lineTo(506.784, 683.662);
        ((GeneralPath) shape).lineTo(506.892, 683.717);
        ((GeneralPath) shape).lineTo(507.001, 683.775);
        ((GeneralPath) shape).lineTo(507.111, 683.836);
        ((GeneralPath) shape).lineTo(507.222, 683.9);
        ((GeneralPath) shape).lineTo(507.335, 683.968);
        ((GeneralPath) shape).lineTo(507.447, 684.039);
        ((GeneralPath) shape).lineTo(507.561, 684.113);
        ((GeneralPath) shape).lineTo(507.676, 684.191);
        ((GeneralPath) shape).lineTo(507.792, 684.272);
        ((GeneralPath) shape).lineTo(507.908, 684.357);
        ((GeneralPath) shape).lineTo(508.024, 684.446);
        ((GeneralPath) shape).lineTo(508.142, 684.539);
        ((GeneralPath) shape).lineTo(508.259, 684.635);
        ((GeneralPath) shape).lineTo(508.377, 684.735);
        ((GeneralPath) shape).lineTo(508.496, 684.839);
        ((GeneralPath) shape).lineTo(508.614, 684.947);
        ((GeneralPath) shape).lineTo(508.733, 685.059);
        ((GeneralPath) shape).lineTo(508.852, 685.175);
        ((GeneralPath) shape).lineTo(508.971, 685.295);
        ((GeneralPath) shape).lineTo(509.09, 685.42);
        ((GeneralPath) shape).lineTo(509.209, 685.548);
        ((GeneralPath) shape).lineTo(509.328, 685.682);
        ((GeneralPath) shape).lineTo(509.446, 685.819);
        ((GeneralPath) shape).lineTo(509.565, 685.961);
        ((GeneralPath) shape).lineTo(509.683, 686.107);
        ((GeneralPath) shape).lineTo(509.801, 686.257);
        ((GeneralPath) shape).lineTo(509.919, 686.413);
        ((GeneralPath) shape).lineTo(510.036, 686.573);
        ((GeneralPath) shape).lineTo(510.153, 686.738);
        ((GeneralPath) shape).lineTo(510.269, 686.907);
        ((GeneralPath) shape).lineTo(510.385, 687.082);
        ((GeneralPath) shape).lineTo(510.5, 687.261);
        ((GeneralPath) shape).lineTo(510.615, 687.445);
        ((GeneralPath) shape).lineTo(510.729, 687.634);
        ((GeneralPath) shape).lineTo(510.842, 687.829);
        ((GeneralPath) shape).lineTo(510.955, 688.028);
        ((GeneralPath) shape).lineTo(511.066, 688.233);
        ((GeneralPath) shape).lineTo(511.177, 688.443);
        ((GeneralPath) shape).lineTo(511.287, 688.658);
        ((GeneralPath) shape).lineTo(511.395, 688.877);
        ((GeneralPath) shape).lineTo(511.451, 688.991);
        ((GeneralPath) shape).lineTo(511.505, 689.105);
        ((GeneralPath) shape).lineTo(511.558, 689.221);
        ((GeneralPath) shape).lineTo(511.612, 689.337);
        ((GeneralPath) shape).lineTo(511.665, 689.455);
        ((GeneralPath) shape).lineTo(511.718, 689.575);
        ((GeneralPath) shape).lineTo(511.771, 689.696);
        ((GeneralPath) shape).lineTo(511.823, 689.818);
        ((GeneralPath) shape).lineTo(511.875, 689.942);
        ((GeneralPath) shape).lineTo(511.927, 690.067);
        ((GeneralPath) shape).lineTo(511.979, 690.194);
        ((GeneralPath) shape).lineTo(512.03, 690.322);
        ((GeneralPath) shape).lineTo(512.081, 690.452);
        ((GeneralPath) shape).lineTo(512.132, 690.583);
        ((GeneralPath) shape).lineTo(512.182, 690.715);
        ((GeneralPath) shape).lineTo(512.233, 690.85);
        ((GeneralPath) shape).lineTo(512.282, 690.985);
        ((GeneralPath) shape).lineTo(512.332, 691.123);
        ((GeneralPath) shape).lineTo(512.381, 691.261);
        ((GeneralPath) shape).lineTo(512.43, 691.402);
        ((GeneralPath) shape).lineTo(512.478, 691.544);
        ((GeneralPath) shape).lineTo(512.526, 691.687);
        ((GeneralPath) shape).lineTo(512.574, 691.832);
        ((GeneralPath) shape).lineTo(512.622, 691.979);
        ((GeneralPath) shape).lineTo(512.669, 692.127);
        ((GeneralPath) shape).lineTo(512.716, 692.277);
        ((GeneralPath) shape).lineTo(512.762, 692.428);
        ((GeneralPath) shape).lineTo(512.808, 692.581);
        ((GeneralPath) shape).lineTo(512.854, 692.736);
        ((GeneralPath) shape).lineTo(512.899, 692.892);
        ((GeneralPath) shape).lineTo(512.944, 693.05);
        ((GeneralPath) shape).lineTo(512.988, 693.21);
        ((GeneralPath) shape).lineTo(513.032, 693.371);
        ((GeneralPath) shape).lineTo(513.076, 693.534);
        ((GeneralPath) shape).lineTo(513.119, 693.699);
        ((GeneralPath) shape).lineTo(513.162, 693.866);
        ((GeneralPath) shape).lineTo(513.205, 694.034);
        ((GeneralPath) shape).lineTo(513.247, 694.203);
        ((GeneralPath) shape).lineTo(513.288, 694.375);
        ((GeneralPath) shape).lineTo(513.33, 694.548);
        ((GeneralPath) shape).lineTo(513.37, 694.724);
        ((GeneralPath) shape).lineTo(513.411, 694.9);
        ((GeneralPath) shape).lineTo(513.45, 695.079);
        ((GeneralPath) shape).lineTo(513.49, 695.259);
        ((GeneralPath) shape).lineTo(513.529, 695.442);
        ((GeneralPath) shape).lineTo(513.567, 695.626);
        ((GeneralPath) shape).lineTo(513.605, 695.812);
        ((GeneralPath) shape).lineTo(513.643, 695.999);
        ((GeneralPath) shape).lineTo(513.68, 696.189);
        ((GeneralPath) shape).lineTo(513.717, 696.38);
        ((GeneralPath) shape).lineTo(513.753, 696.573);
        ((GeneralPath) shape).lineTo(513.788, 696.768);
        ((GeneralPath) shape).lineTo(513.823, 696.965);
        ((GeneralPath) shape).lineTo(513.858, 697.164);
        ((GeneralPath) shape).lineTo(513.892, 697.365);
        ((GeneralPath) shape).lineTo(513.925, 697.568);
        ((GeneralPath) shape).lineTo(513.959, 697.772);
        ((GeneralPath) shape).lineTo(513.991, 697.979);
        ((GeneralPath) shape).lineTo(514.023, 698.187);
        ((GeneralPath) shape).lineTo(514.054, 698.397);
        ((GeneralPath) shape).lineTo(514.085, 698.61);
        ((GeneralPath) shape).lineTo(514.116, 698.824);
        ((GeneralPath) shape).lineTo(514.145, 699.04);
        ((GeneralPath) shape).lineTo(514.175, 699.258);
        ((GeneralPath) shape).lineTo(514.203, 699.479);
        ((GeneralPath) shape).lineTo(514.231, 699.701);
        ((GeneralPath) shape).lineTo(514.259, 699.925);
        ((GeneralPath) shape).lineTo(514.286, 700.151);
        ((GeneralPath) shape).lineTo(514.312, 700.38);
        ((GeneralPath) shape).lineTo(514.338, 700.61);
        ((GeneralPath) shape).lineTo(514.363, 700.843);
        ((GeneralPath) shape).lineTo(514.388, 701.077);
        ((GeneralPath) shape).lineTo(514.411, 701.314);
        ((GeneralPath) shape).lineTo(514.435, 701.552);
        ((GeneralPath) shape).lineTo(514.457, 701.793);
        ((GeneralPath) shape).lineTo(514.479, 702.036);
        ((GeneralPath) shape).lineTo(514.501, 702.281);
        ((GeneralPath) shape).lineTo(514.522, 702.528);
        ((GeneralPath) shape).lineTo(514.542, 702.777);
        ((GeneralPath) shape).lineTo(514.561, 703.029);
        ((GeneralPath) shape).lineTo(514.58, 703.282);
        ((GeneralPath) shape).lineTo(514.598, 703.538);
        ((GeneralPath) shape).lineTo(514.616, 703.796);
        ((GeneralPath) shape).lineTo(514.632, 704.056);
        ((GeneralPath) shape).lineTo(514.649, 704.318);
        ((GeneralPath) shape).lineTo(514.664, 704.583);
        ((GeneralPath) shape).lineTo(514.679, 704.849);
        ((GeneralPath) shape).lineTo(514.693, 705.118);
        ((GeneralPath) shape).lineTo(514.706, 705.389);
        ((GeneralPath) shape).lineTo(514.719, 705.663);
        ((GeneralPath) shape).lineTo(514.731, 705.939);
        ((GeneralPath) shape).lineTo(514.742, 706.216);
        ((GeneralPath) shape).lineTo(514.752, 706.497);
        ((GeneralPath) shape).lineTo(514.762, 706.779);
        ((GeneralPath) shape).lineTo(514.771, 707.064);
        ((GeneralPath) shape).lineTo(514.779, 707.351);
        ((GeneralPath) shape).lineTo(514.787, 707.641);
        ((GeneralPath) shape).lineTo(514.793, 707.932);
        ((GeneralPath) shape).lineTo(514.799, 708.226);
        ((GeneralPath) shape).lineTo(514.804, 708.523);
        ((GeneralPath) shape).lineTo(514.809, 708.822);
        ((GeneralPath) shape).lineTo(514.812, 709.123);
        ((GeneralPath) shape).lineTo(514.815, 709.426);
        ((GeneralPath) shape).lineTo(514.817, 709.733);
        ((GeneralPath) shape).lineTo(514.819, 710.342);
        ((GeneralPath) shape).lineTo(514.818, 710.945);
        ((GeneralPath) shape).lineTo(514.815, 711.542);
        ((GeneralPath) shape).lineTo(514.809, 712.132);
        ((GeneralPath) shape).lineTo(514.8, 712.715);
        ((GeneralPath) shape).lineTo(514.789, 713.292);
        ((GeneralPath) shape).lineTo(514.775, 713.862);
        ((GeneralPath) shape).lineTo(514.758, 714.426);
        ((GeneralPath) shape).lineTo(514.74, 714.983);
        ((GeneralPath) shape).lineTo(514.718, 715.534);
        ((GeneralPath) shape).lineTo(514.695, 716.079);
        ((GeneralPath) shape).lineTo(514.669, 716.617);
        ((GeneralPath) shape).lineTo(514.641, 717.15);
        ((GeneralPath) shape).lineTo(514.61, 717.675);
        ((GeneralPath) shape).lineTo(514.578, 718.195);
        ((GeneralPath) shape).lineTo(514.543, 718.708);
        ((GeneralPath) shape).lineTo(514.506, 719.215);
        ((GeneralPath) shape).lineTo(514.467, 719.716);
        ((GeneralPath) shape).lineTo(514.427, 720.211);
        ((GeneralPath) shape).lineTo(514.384, 720.7);
        ((GeneralPath) shape).lineTo(514.339, 721.182);
        ((GeneralPath) shape).lineTo(514.292, 721.659);
        ((GeneralPath) shape).lineTo(514.243, 722.13);
        ((GeneralPath) shape).lineTo(514.193, 722.594);
        ((GeneralPath) shape).lineTo(514.141, 723.053);
        ((GeneralPath) shape).lineTo(514.087, 723.506);
        ((GeneralPath) shape).lineTo(514.031, 723.953);
        ((GeneralPath) shape).lineTo(513.974, 724.394);
        ((GeneralPath) shape).lineTo(513.915, 724.829);
        ((GeneralPath) shape).lineTo(513.855, 725.259);
        ((GeneralPath) shape).lineTo(513.793, 725.682);
        ((GeneralPath) shape).lineTo(513.73, 726.101);
        ((GeneralPath) shape).lineTo(513.665, 726.513);
        ((GeneralPath) shape).lineTo(513.598, 726.92);
        ((GeneralPath) shape).lineTo(513.531, 727.321);
        ((GeneralPath) shape).lineTo(513.462, 727.716);
        ((GeneralPath) shape).lineTo(513.392, 728.106);
        ((GeneralPath) shape).lineTo(513.32, 728.49);
        ((GeneralPath) shape).lineTo(513.248, 728.869);
        ((GeneralPath) shape).lineTo(513.174, 729.242);
        ((GeneralPath) shape).lineTo(513.099, 729.61);
        ((GeneralPath) shape).lineTo(513.023, 729.973);
        ((GeneralPath) shape).lineTo(512.946, 730.33);
        ((GeneralPath) shape).lineTo(512.868, 730.682);
        ((GeneralPath) shape).lineTo(512.79, 731.028);
        ((GeneralPath) shape).lineTo(512.71, 731.369);
        ((GeneralPath) shape).lineTo(512.629, 731.705);
        ((GeneralPath) shape).lineTo(512.548, 732.036);
        ((GeneralPath) shape).lineTo(512.466, 732.361);
        ((GeneralPath) shape).lineTo(512.383, 732.681);
        ((GeneralPath) shape).lineTo(512.299, 732.996);
        ((GeneralPath) shape).lineTo(512.215, 733.307);
        ((GeneralPath) shape).lineTo(512.13, 733.611);
        ((GeneralPath) shape).lineTo(512.045, 733.911);
        ((GeneralPath) shape).lineTo(511.959, 734.206);
        ((GeneralPath) shape).lineTo(511.873, 734.496);
        ((GeneralPath) shape).lineTo(511.786, 734.781);
        ((GeneralPath) shape).lineTo(511.699, 735.061);
        ((GeneralPath) shape).lineTo(511.611, 735.336);
        ((GeneralPath) shape).lineTo(511.523, 735.606);
        ((GeneralPath) shape).lineTo(511.435, 735.871);
        ((GeneralPath) shape).lineTo(511.347, 736.132);
        ((GeneralPath) shape).lineTo(511.258, 736.387);
        ((GeneralPath) shape).lineTo(511.17, 736.638);
        ((GeneralPath) shape).lineTo(511.081, 736.885);
        ((GeneralPath) shape).lineTo(510.992, 737.126);
        ((GeneralPath) shape).lineTo(510.904, 737.363);
        ((GeneralPath) shape).lineTo(510.815, 737.595);
        ((GeneralPath) shape).lineTo(510.726, 737.823);
        ((GeneralPath) shape).lineTo(510.638, 738.046);
        ((GeneralPath) shape).lineTo(510.549, 738.264);
        ((GeneralPath) shape).lineTo(510.461, 738.478);
        ((GeneralPath) shape).lineTo(510.373, 738.688);
        ((GeneralPath) shape).lineTo(510.285, 738.893);
        ((GeneralPath) shape).lineTo(510.198, 739.094);
        ((GeneralPath) shape).lineTo(510.111, 739.29);
        ((GeneralPath) shape).lineTo(510.024, 739.482);
        ((GeneralPath) shape).lineTo(509.938, 739.669);
        ((GeneralPath) shape).lineTo(509.853, 739.853);
        ((GeneralPath) shape).lineTo(509.768, 740.031);
        ((GeneralPath) shape).lineTo(509.683, 740.206);
        ((GeneralPath) shape).lineTo(509.599, 740.377);
        ((GeneralPath) shape).lineTo(509.516, 740.543);
        ((GeneralPath) shape).lineTo(509.434, 740.705);
        ((GeneralPath) shape).lineTo(509.352, 740.863);
        ((GeneralPath) shape).lineTo(509.271, 741.017);
        ((GeneralPath) shape).lineTo(509.191, 741.167);
        ((GeneralPath) shape).lineTo(509.112, 741.313);
        ((GeneralPath) shape).lineTo(509.034, 741.455);
        ((GeneralPath) shape).lineTo(508.956, 741.592);
        ((GeneralPath) shape).lineTo(508.88, 741.726);
        ((GeneralPath) shape).lineTo(508.805, 741.856);
        ((GeneralPath) shape).lineTo(508.731, 741.982);
        ((GeneralPath) shape).lineTo(508.658, 742.104);
        ((GeneralPath) shape).lineTo(508.586, 742.223);
        ((GeneralPath) shape).lineTo(508.515, 742.337);
        ((GeneralPath) shape).lineTo(508.446, 742.448);
        ((GeneralPath) shape).lineTo(508.378, 742.555);
        ((GeneralPath) shape).lineTo(508.311, 742.658);
        ((GeneralPath) shape).lineTo(508.245, 742.76);
        ((GeneralPath) shape).lineTo(508.12, 742.946);
        ((GeneralPath) shape).lineTo(508.0, 743.12);
        ((GeneralPath) shape).lineTo(507.887, 743.28);
        ((GeneralPath) shape).lineTo(507.78, 743.426);
        ((GeneralPath) shape).lineTo(507.68, 743.559);
        ((GeneralPath) shape).lineTo(507.588, 743.677);
        ((GeneralPath) shape).lineTo(507.504, 743.783);
        ((GeneralPath) shape).lineTo(507.428, 743.876);
        ((GeneralPath) shape).lineTo(507.361, 743.956);
        ((GeneralPath) shape).lineTo(507.299, 744.027);
        ((GeneralPath) shape).lineTo(507.216, 744.119);
        ((GeneralPath) shape).lineTo(506.743, 744.62);
        ((GeneralPath) shape).lineTo(367.503, 745.589);
        ((GeneralPath) shape).lineTo(355.859, 838.005);
        ((GeneralPath) shape).lineTo(397.224, 838.293);
        ((GeneralPath) shape).lineTo(397.246, 838.293);
        ((GeneralPath) shape).lineTo(397.337, 838.294);
        ((GeneralPath) shape).lineTo(397.463, 838.298);
        ((GeneralPath) shape).lineTo(397.546, 838.303);
        ((GeneralPath) shape).lineTo(397.632, 838.31);
        ((GeneralPath) shape).lineTo(397.728, 838.319);
        ((GeneralPath) shape).lineTo(397.83, 838.33);
        ((GeneralPath) shape).lineTo(397.939, 838.343);
        ((GeneralPath) shape).lineTo(398.056, 838.36);
        ((GeneralPath) shape).lineTo(398.18, 838.38);
        ((GeneralPath) shape).lineTo(398.31, 838.404);
        ((GeneralPath) shape).lineTo(398.446, 838.431);
        ((GeneralPath) shape).lineTo(398.588, 838.462);
        ((GeneralPath) shape).lineTo(398.736, 838.499);
        ((GeneralPath) shape).lineTo(398.889, 838.539);
        ((GeneralPath) shape).lineTo(399.043, 838.584);
        ((GeneralPath) shape).lineTo(399.127, 838.61);
        ((GeneralPath) shape).lineTo(399.209, 838.637);
        ((GeneralPath) shape).lineTo(399.292, 838.665);
        ((GeneralPath) shape).lineTo(399.377, 838.694);
        ((GeneralPath) shape).lineTo(399.461, 838.725);
        ((GeneralPath) shape).lineTo(399.547, 838.757);
        ((GeneralPath) shape).lineTo(399.634, 838.791);
        ((GeneralPath) shape).lineTo(399.722, 838.826);
        ((GeneralPath) shape).lineTo(399.81, 838.864);
        ((GeneralPath) shape).lineTo(399.9, 838.903);
        ((GeneralPath) shape).lineTo(399.989, 838.943);
        ((GeneralPath) shape).lineTo(400.08, 838.985);
        ((GeneralPath) shape).lineTo(400.172, 839.03);
        ((GeneralPath) shape).lineTo(400.264, 839.076);
        ((GeneralPath) shape).lineTo(400.356, 839.124);
        ((GeneralPath) shape).lineTo(400.448, 839.174);
        ((GeneralPath) shape).lineTo(400.542, 839.226);
        ((GeneralPath) shape).lineTo(400.636, 839.28);
        ((GeneralPath) shape).lineTo(400.73, 839.336);
        ((GeneralPath) shape).lineTo(400.825, 839.394);
        ((GeneralPath) shape).lineTo(400.919, 839.454);
        ((GeneralPath) shape).lineTo(401.014, 839.516);
        ((GeneralPath) shape).lineTo(401.109, 839.58);
        ((GeneralPath) shape).lineTo(401.203, 839.647);
        ((GeneralPath) shape).lineTo(401.299, 839.716);
        ((GeneralPath) shape).lineTo(401.394, 839.787);
        ((GeneralPath) shape).lineTo(401.489, 839.86);
        ((GeneralPath) shape).lineTo(401.583, 839.936);
        ((GeneralPath) shape).lineTo(401.677, 840.014);
        ((GeneralPath) shape).lineTo(401.772, 840.094);
        ((GeneralPath) shape).lineTo(401.866, 840.177);
        ((GeneralPath) shape).lineTo(401.959, 840.262);
        ((GeneralPath) shape).lineTo(402.052, 840.35);
        ((GeneralPath) shape).lineTo(402.145, 840.44);
        ((GeneralPath) shape).lineTo(402.237, 840.532);
        ((GeneralPath) shape).lineTo(402.329, 840.628);
        ((GeneralPath) shape).lineTo(402.419, 840.725);
        ((GeneralPath) shape).lineTo(402.509, 840.825);
        ((GeneralPath) shape).lineTo(402.599, 840.928);
        ((GeneralPath) shape).lineTo(402.687, 841.033);
        ((GeneralPath) shape).lineTo(402.775, 841.141);
        ((GeneralPath) shape).lineTo(402.862, 841.251);
        ((GeneralPath) shape).lineTo(402.948, 841.364);
        ((GeneralPath) shape).lineTo(403.033, 841.48);
        ((GeneralPath) shape).lineTo(403.116, 841.598);
        ((GeneralPath) shape).lineTo(403.199, 841.719);
        ((GeneralPath) shape).lineTo(403.281, 841.843);
        ((GeneralPath) shape).lineTo(403.361, 841.969);
        ((GeneralPath) shape).lineTo(403.44, 842.098);
        ((GeneralPath) shape).lineTo(403.518, 842.229);
        ((GeneralPath) shape).lineTo(403.595, 842.364);
        ((GeneralPath) shape).lineTo(403.67, 842.501);
        ((GeneralPath) shape).lineTo(403.743, 842.64);
        ((GeneralPath) shape).lineTo(403.816, 842.783);
        ((GeneralPath) shape).lineTo(403.886, 842.928);
        ((GeneralPath) shape).lineTo(403.956, 843.076);
        ((GeneralPath) shape).lineTo(404.024, 843.227);
        ((GeneralPath) shape).lineTo(404.09, 843.381);
        ((GeneralPath) shape).lineTo(404.154, 843.537);
        ((GeneralPath) shape).lineTo(404.217, 843.696);
        ((GeneralPath) shape).lineTo(404.278, 843.858);
        ((GeneralPath) shape).lineTo(404.338, 844.023);
        ((GeneralPath) shape).lineTo(404.395, 844.191);
        ((GeneralPath) shape).lineTo(404.451, 844.361);
        ((GeneralPath) shape).lineTo(404.505, 844.535);
        ((GeneralPath) shape).lineTo(404.558, 844.711);
        ((GeneralPath) shape).lineTo(404.608, 844.891);
        ((GeneralPath) shape).lineTo(404.656, 845.073);
        ((GeneralPath) shape).lineTo(404.703, 845.259);
        ((GeneralPath) shape).lineTo(404.747, 845.447);
        ((GeneralPath) shape).lineTo(404.79, 845.638);
        ((GeneralPath) shape).lineTo(404.831, 845.833);
        ((GeneralPath) shape).lineTo(404.869, 846.03);
        ((GeneralPath) shape).lineTo(404.905, 846.231);
        ((GeneralPath) shape).lineTo(404.94, 846.435);
        ((GeneralPath) shape).lineTo(404.972, 846.641);
        ((GeneralPath) shape).lineTo(405.002, 846.851);
        ((GeneralPath) shape).lineTo(405.03, 847.065);
        ((GeneralPath) shape).lineTo(405.055, 847.281);
        ((GeneralPath) shape).lineTo(405.079, 847.501);
        ((GeneralPath) shape).lineTo(405.1, 847.724);
        ((GeneralPath) shape).lineTo(405.119, 847.95);
        ((GeneralPath) shape).lineTo(405.135, 848.18);
        ((GeneralPath) shape).lineTo(405.149, 848.413);
        ((GeneralPath) shape).lineTo(405.161, 848.649);
        ((GeneralPath) shape).lineTo(405.17, 848.889);
        ((GeneralPath) shape).lineTo(405.177, 849.132);
        ((GeneralPath) shape).lineTo(405.181, 849.379);
        ((GeneralPath) shape).lineTo(405.183, 849.629);
        ((GeneralPath) shape).lineTo(405.182, 849.883);
        ((GeneralPath) shape).lineTo(405.179, 850.091);
        ((GeneralPath) shape).lineTo(405.174, 850.298);
        ((GeneralPath) shape).lineTo(405.167, 850.506);
        ((GeneralPath) shape).lineTo(405.158, 850.714);
        ((GeneralPath) shape).lineTo(405.146, 850.922);
        ((GeneralPath) shape).lineTo(405.132, 851.13);
        ((GeneralPath) shape).lineTo(405.116, 851.338);
        ((GeneralPath) shape).lineTo(405.098, 851.546);
        ((GeneralPath) shape).lineTo(405.079, 851.754);
        ((GeneralPath) shape).lineTo(405.057, 851.962);
        ((GeneralPath) shape).lineTo(405.033, 852.17);
        ((GeneralPath) shape).lineTo(405.007, 852.378);
        ((GeneralPath) shape).lineTo(404.98, 852.585);
        ((GeneralPath) shape).lineTo(404.951, 852.793);
        ((GeneralPath) shape).lineTo(404.92, 853.0);
        ((GeneralPath) shape).lineTo(404.887, 853.207);
        ((GeneralPath) shape).lineTo(404.853, 853.413);
        ((GeneralPath) shape).lineTo(404.817, 853.62);
        ((GeneralPath) shape).lineTo(404.779, 853.826);
        ((GeneralPath) shape).lineTo(404.74, 854.032);
        ((GeneralPath) shape).lineTo(404.699, 854.237);
        ((GeneralPath) shape).lineTo(404.657, 854.442);
        ((GeneralPath) shape).lineTo(404.614, 854.647);
        ((GeneralPath) shape).lineTo(404.569, 854.851);
        ((GeneralPath) shape).lineTo(404.522, 855.055);
        ((GeneralPath) shape).lineTo(404.474, 855.258);
        ((GeneralPath) shape).lineTo(404.425, 855.461);
        ((GeneralPath) shape).lineTo(404.375, 855.664);
        ((GeneralPath) shape).lineTo(404.323, 855.865);
        ((GeneralPath) shape).lineTo(404.271, 856.066);
        ((GeneralPath) shape).lineTo(404.217, 856.267);
        ((GeneralPath) shape).lineTo(404.162, 856.467);
        ((GeneralPath) shape).lineTo(404.105, 856.666);
        ((GeneralPath) shape).lineTo(404.048, 856.865);
        ((GeneralPath) shape).lineTo(403.99, 857.063);
        ((GeneralPath) shape).lineTo(403.931, 857.26);
        ((GeneralPath) shape).lineTo(403.87, 857.456);
        ((GeneralPath) shape).lineTo(403.809, 857.652);
        ((GeneralPath) shape).lineTo(403.747, 857.846);
        ((GeneralPath) shape).lineTo(403.684, 858.04);
        ((GeneralPath) shape).lineTo(403.621, 858.233);
        ((GeneralPath) shape).lineTo(403.556, 858.425);
        ((GeneralPath) shape).lineTo(403.491, 858.616);
        ((GeneralPath) shape).lineTo(403.425, 858.807);
        ((GeneralPath) shape).lineTo(403.358, 858.996);
        ((GeneralPath) shape).lineTo(403.291, 859.184);
        ((GeneralPath) shape).lineTo(403.223, 859.371);
        ((GeneralPath) shape).lineTo(403.155, 859.557);
        ((GeneralPath) shape).lineTo(403.086, 859.742);
        ((GeneralPath) shape).lineTo(403.016, 859.926);
        ((GeneralPath) shape).lineTo(402.946, 860.109);
        ((GeneralPath) shape).lineTo(402.875, 860.29);
        ((GeneralPath) shape).lineTo(402.805, 860.47);
        ((GeneralPath) shape).lineTo(402.733, 860.649);
        ((GeneralPath) shape).lineTo(402.661, 860.827);
        ((GeneralPath) shape).lineTo(402.589, 861.005);
        ((GeneralPath) shape).lineTo(402.445, 861.353);
        ((GeneralPath) shape).lineTo(402.299, 861.696);
        ((GeneralPath) shape).lineTo(402.153, 862.034);
        ((GeneralPath) shape).lineTo(402.006, 862.365);
        ((GeneralPath) shape).lineTo(401.859, 862.69);
        ((GeneralPath) shape).lineTo(401.713, 863.009);
        ((GeneralPath) shape).lineTo(401.567, 863.322);
        ((GeneralPath) shape).lineTo(401.421, 863.627);
        ((GeneralPath) shape).lineTo(401.277, 863.925);
        ((GeneralPath) shape).lineTo(401.133, 864.216);
        ((GeneralPath) shape).lineTo(400.992, 864.499);
        ((GeneralPath) shape).lineTo(400.852, 864.774);
        ((GeneralPath) shape).lineTo(400.714, 865.042);
        ((GeneralPath) shape).lineTo(400.578, 865.3);
        ((GeneralPath) shape).lineTo(400.444, 865.551);
        ((GeneralPath) shape).lineTo(400.314, 865.793);
        ((GeneralPath) shape).lineTo(400.186, 866.025);
        ((GeneralPath) shape).lineTo(400.062, 866.248);
        ((GeneralPath) shape).lineTo(399.941, 866.463);
        ((GeneralPath) shape).lineTo(399.824, 866.667);
        ((GeneralPath) shape).lineTo(399.711, 866.862);
        ((GeneralPath) shape).lineTo(399.601, 867.046);
        ((GeneralPath) shape).lineTo(399.497, 867.221);
        ((GeneralPath) shape).lineTo(399.397, 867.385);
        ((GeneralPath) shape).lineTo(399.301, 867.54);
        ((GeneralPath) shape).lineTo(399.21, 867.683);
        ((GeneralPath) shape).lineTo(399.125, 867.817);
        ((GeneralPath) shape).lineTo(399.044, 867.94);
        ((GeneralPath) shape).lineTo(398.967, 868.053);
        ((GeneralPath) shape).lineTo(398.894, 868.158);
        ((GeneralPath) shape).lineTo(398.823, 868.256);
        ((GeneralPath) shape).lineTo(398.752, 868.349);
        ((GeneralPath) shape).lineTo(398.681, 868.438);
        ((GeneralPath) shape).lineTo(398.629, 868.497);
        ((GeneralPath) shape).lineTo(398.571, 868.559);
        ((GeneralPath) shape).lineTo(398.487, 868.639);
        ((GeneralPath) shape).lineTo(398.354, 868.749);
        ((GeneralPath) shape).lineTo(398.075, 868.922);
        ((GeneralPath) shape).lineTo(397.673, 869.031);
        ((GeneralPath) shape).lineTo(390.209, 869.119);
        ((GeneralPath) shape).lineTo(382.701, 869.208);
        ((GeneralPath) shape).lineTo(374.021, 869.309);
        ((GeneralPath) shape).lineTo(365.225, 869.41);
        ((GeneralPath) shape).lineTo(361.113, 869.456);
        ((GeneralPath) shape).lineTo(357.368, 869.498);
        ((GeneralPath) shape).lineTo(354.121, 869.533);
        ((GeneralPath) shape).lineTo(351.506, 869.561);
        ((GeneralPath) shape).lineTo(349.834, 869.576);
        ((GeneralPath) shape).lineTo(349.83, 869.586);
        ((GeneralPath) shape).lineTo(349.793, 869.677);
        ((GeneralPath) shape).lineTo(349.754, 869.767);
        ((GeneralPath) shape).lineTo(349.715, 869.857);
        ((GeneralPath) shape).lineTo(349.674, 869.945);
        ((GeneralPath) shape).lineTo(349.632, 870.032);
        ((GeneralPath) shape).lineTo(349.59, 870.119);
        ((GeneralPath) shape).lineTo(349.546, 870.205);
        ((GeneralPath) shape).lineTo(349.502, 870.29);
        ((GeneralPath) shape).lineTo(349.457, 870.374);
        ((GeneralPath) shape).lineTo(349.412, 870.457);
        ((GeneralPath) shape).lineTo(349.366, 870.539);
        ((GeneralPath) shape).lineTo(349.319, 870.621);
        ((GeneralPath) shape).lineTo(349.272, 870.702);
        ((GeneralPath) shape).lineTo(349.222, 870.784);
        ((GeneralPath) shape).lineTo(349.127, 870.938);
        ((GeneralPath) shape).lineTo(349.029, 871.09);
        ((GeneralPath) shape).lineTo(348.93, 871.238);
        ((GeneralPath) shape).lineTo(348.83, 871.381);
        ((GeneralPath) shape).lineTo(348.73, 871.519);
        ((GeneralPath) shape).lineTo(348.629, 871.653);
        ((GeneralPath) shape).lineTo(348.528, 871.781);
        ((GeneralPath) shape).lineTo(348.427, 871.905);
        ((GeneralPath) shape).lineTo(348.326, 872.023);
        ((GeneralPath) shape).lineTo(348.227, 872.132);
        ((GeneralPath) shape).lineTo(348.173, 872.191);
        ((GeneralPath) shape).lineTo(348.12, 872.245);
        ((GeneralPath) shape).lineTo(348.066, 872.299);
        ((GeneralPath) shape).lineTo(348.012, 872.351);
        ((GeneralPath) shape).lineTo(347.957, 872.402);
        ((GeneralPath) shape).lineTo(347.898, 872.454);
        ((GeneralPath) shape).lineTo(347.837, 872.506);
        ((GeneralPath) shape).lineTo(347.771, 872.558);
        ((GeneralPath) shape).lineTo(347.699, 872.612);
        ((GeneralPath) shape).lineTo(347.62, 872.666);
        ((GeneralPath) shape).lineTo(347.53, 872.721);
        ((GeneralPath) shape).lineTo(347.424, 872.779);
        ((GeneralPath) shape).lineTo(347.294, 872.838);
        ((GeneralPath) shape).lineTo(347.134, 872.895);
        ((GeneralPath) shape).lineTo(346.928, 872.944);
        ((GeneralPath) shape).lineTo(346.674, 872.965);
        ((GeneralPath) shape).lineTo(346.421, 872.944);
        ((GeneralPath) shape).lineTo(346.215, 872.895);
        ((GeneralPath) shape).lineTo(346.054, 872.838);
        ((GeneralPath) shape).lineTo(345.923, 872.779);
        ((GeneralPath) shape).lineTo(345.818, 872.721);
        ((GeneralPath) shape).lineTo(345.729, 872.666);
        ((GeneralPath) shape).lineTo(345.649, 872.612);
        ((GeneralPath) shape).lineTo(345.577, 872.558);
        ((GeneralPath) shape).lineTo(345.511, 872.506);
        ((GeneralPath) shape).lineTo(345.45, 872.454);
        ((GeneralPath) shape).lineTo(345.392, 872.403);
        ((GeneralPath) shape).lineTo(345.336, 872.351);
        ((GeneralPath) shape).lineTo(345.282, 872.299);
        ((GeneralPath) shape).lineTo(345.228, 872.245);
        ((GeneralPath) shape).lineTo(345.176, 872.191);
        ((GeneralPath) shape).lineTo(345.121, 872.132);
        ((GeneralPath) shape).lineTo(345.022, 872.023);
        ((GeneralPath) shape).lineTo(344.921, 871.905);
        ((GeneralPath) shape).lineTo(344.82, 871.781);
        ((GeneralPath) shape).lineTo(344.719, 871.653);
        ((GeneralPath) shape).lineTo(344.619, 871.519);
        ((GeneralPath) shape).lineTo(344.518, 871.381);
        ((GeneralPath) shape).lineTo(344.418, 871.238);
        ((GeneralPath) shape).lineTo(344.319, 871.09);
        ((GeneralPath) shape).lineTo(344.221, 870.938);
        ((GeneralPath) shape).lineTo(344.126, 870.784);
        ((GeneralPath) shape).lineTo(344.077, 870.702);
        ((GeneralPath) shape).lineTo(344.029, 870.621);
        ((GeneralPath) shape).lineTo(343.983, 870.54);
        ((GeneralPath) shape).lineTo(343.936, 870.457);
        ((GeneralPath) shape).lineTo(343.891, 870.374);
        ((GeneralPath) shape).lineTo(343.846, 870.29);
        ((GeneralPath) shape).lineTo(343.802, 870.205);
        ((GeneralPath) shape).lineTo(343.758, 870.118);
        ((GeneralPath) shape).lineTo(343.716, 870.032);
        ((GeneralPath) shape).lineTo(343.674, 869.945);
        ((GeneralPath) shape).lineTo(343.633, 869.856);
        ((GeneralPath) shape).lineTo(343.594, 869.767);
        ((GeneralPath) shape).lineTo(343.555, 869.677);
        ((GeneralPath) shape).lineTo(343.518, 869.586);
        ((GeneralPath) shape).lineTo(343.514, 869.576);
        ((GeneralPath) shape).lineTo(341.843, 869.561);
        ((GeneralPath) shape).lineTo(339.227, 869.533);
        ((GeneralPath) shape).lineTo(335.98, 869.498);
        ((GeneralPath) shape).lineTo(332.235, 869.456);
        ((GeneralPath) shape).lineTo(328.124, 869.41);
        ((GeneralPath) shape).lineTo(319.327, 869.309);
        ((GeneralPath) shape).lineTo(310.647, 869.208);
        ((GeneralPath) shape).lineTo(303.14, 869.119);
        ((GeneralPath) shape).lineTo(295.675, 869.031);
        ((GeneralPath) shape).lineTo(295.274, 868.922);
        ((GeneralPath) shape).lineTo(294.995, 868.75);
        ((GeneralPath) shape).lineTo(294.862, 868.64);
        ((GeneralPath) shape).lineTo(294.779, 868.561);
        ((GeneralPath) shape).lineTo(294.719, 868.497);
        ((GeneralPath) shape).lineTo(294.668, 868.438);
        ((GeneralPath) shape).lineTo(294.596, 868.35);
        ((GeneralPath) shape).lineTo(294.525, 868.256);
        ((GeneralPath) shape).lineTo(294.455, 868.158);
        ((GeneralPath) shape).lineTo(294.382, 868.054);
        ((GeneralPath) shape).lineTo(294.305, 867.94);
        ((GeneralPath) shape).lineTo(294.224, 867.817);
        ((GeneralPath) shape).lineTo(294.138, 867.684);
        ((GeneralPath) shape).lineTo(294.047, 867.54);
        ((GeneralPath) shape).lineTo(293.952, 867.386);
        ((GeneralPath) shape).lineTo(293.851, 867.221);
        ((GeneralPath) shape).lineTo(293.747, 867.047);
        ((GeneralPath) shape).lineTo(293.638, 866.862);
        ((GeneralPath) shape).lineTo(293.524, 866.667);
        ((GeneralPath) shape).lineTo(293.407, 866.463);
        ((GeneralPath) shape).lineTo(293.286, 866.249);
        ((GeneralPath) shape).lineTo(293.162, 866.025);
        ((GeneralPath) shape).lineTo(293.034, 865.793);
        ((GeneralPath) shape).lineTo(292.904, 865.551);
        ((GeneralPath) shape).lineTo(292.771, 865.301);
        ((GeneralPath) shape).lineTo(292.635, 865.042);
        ((GeneralPath) shape).lineTo(292.497, 864.775);
        ((GeneralPath) shape).lineTo(292.357, 864.499);
        ((GeneralPath) shape).lineTo(292.215, 864.216);
        ((GeneralPath) shape).lineTo(292.072, 863.925);
        ((GeneralPath) shape).lineTo(291.927, 863.627);
        ((GeneralPath) shape).lineTo(291.782, 863.322);
        ((GeneralPath) shape).lineTo(291.635, 863.01);
        ((GeneralPath) shape).lineTo(291.489, 862.691);
        ((GeneralPath) shape).lineTo(291.342, 862.365);
        ((GeneralPath) shape).lineTo(291.195, 862.034);
        ((GeneralPath) shape).lineTo(291.049, 861.696);
        ((GeneralPath) shape).lineTo(290.903, 861.353);
        ((GeneralPath) shape).lineTo(290.759, 861.005);
        ((GeneralPath) shape).lineTo(290.687, 860.827);
        ((GeneralPath) shape).lineTo(290.615, 860.65);
        ((GeneralPath) shape).lineTo(290.544, 860.471);
        ((GeneralPath) shape).lineTo(290.473, 860.29);
        ((GeneralPath) shape).lineTo(290.402, 860.109);
        ((GeneralPath) shape).lineTo(290.332, 859.926);
        ((GeneralPath) shape).lineTo(290.263, 859.742);
        ((GeneralPath) shape).lineTo(290.194, 859.557);
        ((GeneralPath) shape).lineTo(290.125, 859.371);
        ((GeneralPath) shape).lineTo(290.057, 859.184);
        ((GeneralPath) shape).lineTo(289.99, 858.996);
        ((GeneralPath) shape).lineTo(289.923, 858.807);
        ((GeneralPath) shape).lineTo(289.857, 858.616);
        ((GeneralPath) shape).lineTo(289.792, 858.425);
        ((GeneralPath) shape).lineTo(289.727, 858.233);
        ((GeneralPath) shape).lineTo(289.664, 858.04);
        ((GeneralPath) shape).lineTo(289.601, 857.846);
        ((GeneralPath) shape).lineTo(289.539, 857.652);
        ((GeneralPath) shape).lineTo(289.478, 857.456);
        ((GeneralPath) shape).lineTo(289.418, 857.26);
        ((GeneralPath) shape).lineTo(289.358, 857.063);
        ((GeneralPath) shape).lineTo(289.3, 856.865);
        ((GeneralPath) shape).lineTo(289.243, 856.666);
        ((GeneralPath) shape).lineTo(289.187, 856.467);
        ((GeneralPath) shape).lineTo(289.132, 856.267);
        ((GeneralPath) shape).lineTo(289.078, 856.067);
        ((GeneralPath) shape).lineTo(289.025, 855.865);
        ((GeneralPath) shape).lineTo(288.973, 855.664);
        ((GeneralPath) shape).lineTo(288.923, 855.461);
        ((GeneralPath) shape).lineTo(288.874, 855.258);
        ((GeneralPath) shape).lineTo(288.826, 855.055);
        ((GeneralPath) shape).lineTo(288.78, 854.851);
        ((GeneralPath) shape).lineTo(288.735, 854.647);
        ((GeneralPath) shape).lineTo(288.691, 854.442);
        ((GeneralPath) shape).lineTo(288.649, 854.237);
        ((GeneralPath) shape).lineTo(288.608, 854.032);
        ((GeneralPath) shape).lineTo(288.569, 853.826);
        ((GeneralPath) shape).lineTo(288.531, 853.62);
        ((GeneralPath) shape).lineTo(288.495, 853.414);
        ((GeneralPath) shape).lineTo(288.461, 853.207);
        ((GeneralPath) shape).lineTo(288.428, 853.0);
        ((GeneralPath) shape).lineTo(288.397, 852.793);
        ((GeneralPath) shape).lineTo(288.368, 852.585);
        ((GeneralPath) shape).lineTo(288.341, 852.378);
        ((GeneralPath) shape).lineTo(288.315, 852.17);
        ((GeneralPath) shape).lineTo(288.291, 851.962);
        ((GeneralPath) shape).lineTo(288.27, 851.754);
        ((GeneralPath) shape).lineTo(288.25, 851.546);
        ((GeneralPath) shape).lineTo(288.232, 851.338);
        ((GeneralPath) shape).lineTo(288.216, 851.13);
        ((GeneralPath) shape).lineTo(288.202, 850.922);
        ((GeneralPath) shape).lineTo(288.191, 850.714);
        ((GeneralPath) shape).lineTo(288.181, 850.506);
        ((GeneralPath) shape).lineTo(288.174, 850.298);
        ((GeneralPath) shape).lineTo(288.169, 850.091);
        ((GeneralPath) shape).lineTo(288.166, 849.883);
        ((GeneralPath) shape).lineTo(288.165, 849.629);
        ((GeneralPath) shape).lineTo(288.167, 849.379);
        ((GeneralPath) shape).lineTo(288.171, 849.132);
        ((GeneralPath) shape).lineTo(288.178, 848.889);
        ((GeneralPath) shape).lineTo(288.187, 848.649);
        ((GeneralPath) shape).lineTo(288.199, 848.413);
        ((GeneralPath) shape).lineTo(288.213, 848.18);
        ((GeneralPath) shape).lineTo(288.23, 847.95);
        ((GeneralPath) shape).lineTo(288.248, 847.724);
        ((GeneralPath) shape).lineTo(288.269, 847.501);
        ((GeneralPath) shape).lineTo(288.293, 847.281);
        ((GeneralPath) shape).lineTo(288.318, 847.065);
        ((GeneralPath) shape).lineTo(288.346, 846.851);
        ((GeneralPath) shape).lineTo(288.376, 846.641);
        ((GeneralPath) shape).lineTo(288.408, 846.434);
        ((GeneralPath) shape).lineTo(288.443, 846.231);
        ((GeneralPath) shape).lineTo(288.479, 846.03);
        ((GeneralPath) shape).lineTo(288.518, 845.833);
        ((GeneralPath) shape).lineTo(288.558, 845.638);
        ((GeneralPath) shape).lineTo(288.601, 845.447);
        ((GeneralPath) shape).lineTo(288.645, 845.259);
        ((GeneralPath) shape).lineTo(288.692, 845.073);
        ((GeneralPath) shape).lineTo(288.74, 844.891);
        ((GeneralPath) shape).lineTo(288.791, 844.711);
        ((GeneralPath) shape).lineTo(288.843, 844.535);
        ((GeneralPath) shape).lineTo(288.897, 844.361);
        ((GeneralPath) shape).lineTo(288.953, 844.191);
        ((GeneralPath) shape).lineTo(289.01, 844.023);
        ((GeneralPath) shape).lineTo(289.07, 843.858);
        ((GeneralPath) shape).lineTo(289.131, 843.696);
        ((GeneralPath) shape).lineTo(289.194, 843.537);
        ((GeneralPath) shape).lineTo(289.258, 843.381);
        ((GeneralPath) shape).lineTo(289.325, 843.227);
        ((GeneralPath) shape).lineTo(289.392, 843.076);
        ((GeneralPath) shape).lineTo(289.462, 842.928);
        ((GeneralPath) shape).lineTo(289.532, 842.783);
        ((GeneralPath) shape).lineTo(289.605, 842.64);
        ((GeneralPath) shape).lineTo(289.678, 842.501);
        ((GeneralPath) shape).lineTo(289.754, 842.364);
        ((GeneralPath) shape).lineTo(289.83, 842.229);
        ((GeneralPath) shape).lineTo(289.908, 842.098);
        ((GeneralPath) shape).lineTo(289.987, 841.969);
        ((GeneralPath) shape).lineTo(290.067, 841.843);
        ((GeneralPath) shape).lineTo(290.149, 841.719);
        ((GeneralPath) shape).lineTo(290.232, 841.598);
        ((GeneralPath) shape).lineTo(290.315, 841.48);
        ((GeneralPath) shape).lineTo(290.4, 841.364);
        ((GeneralPath) shape).lineTo(290.486, 841.251);
        ((GeneralPath) shape).lineTo(290.573, 841.141);
        ((GeneralPath) shape).lineTo(290.661, 841.033);
        ((GeneralPath) shape).lineTo(290.749, 840.928);
        ((GeneralPath) shape).lineTo(290.839, 840.825);
        ((GeneralPath) shape).lineTo(290.929, 840.725);
        ((GeneralPath) shape).lineTo(291.02, 840.628);
        ((GeneralPath) shape).lineTo(291.111, 840.532);
        ((GeneralPath) shape).lineTo(291.203, 840.44);
        ((GeneralPath) shape).lineTo(291.296, 840.35);
        ((GeneralPath) shape).lineTo(291.389, 840.262);
        ((GeneralPath) shape).lineTo(291.482, 840.177);
        ((GeneralPath) shape).lineTo(291.576, 840.094);
        ((GeneralPath) shape).lineTo(291.671, 840.014);
        ((GeneralPath) shape).lineTo(291.765, 839.936);
        ((GeneralPath) shape).lineTo(291.86, 839.86);
        ((GeneralPath) shape).lineTo(291.955, 839.787);
        ((GeneralPath) shape).lineTo(292.05, 839.716);
        ((GeneralPath) shape).lineTo(292.145, 839.647);
        ((GeneralPath) shape).lineTo(292.24, 839.58);
        ((GeneralPath) shape).lineTo(292.334, 839.516);
        ((GeneralPath) shape).lineTo(292.429, 839.454);
        ((GeneralPath) shape).lineTo(292.524, 839.394);
        ((GeneralPath) shape).lineTo(292.618, 839.336);
        ((GeneralPath) shape).lineTo(292.712, 839.28);
        ((GeneralPath) shape).lineTo(292.806, 839.226);
        ((GeneralPath) shape).lineTo(292.9, 839.174);
        ((GeneralPath) shape).lineTo(292.992, 839.124);
        ((GeneralPath) shape).lineTo(293.084, 839.076);
        ((GeneralPath) shape).lineTo(293.176, 839.03);
        ((GeneralPath) shape).lineTo(293.268, 838.985);
        ((GeneralPath) shape).lineTo(293.359, 838.943);
        ((GeneralPath) shape).lineTo(293.448, 838.903);
        ((GeneralPath) shape).lineTo(293.538, 838.864);
        ((GeneralPath) shape).lineTo(293.627, 838.826);
        ((GeneralPath) shape).lineTo(293.714, 838.791);
        ((GeneralPath) shape).lineTo(293.801, 838.757);
        ((GeneralPath) shape).lineTo(293.887, 838.725);
        ((GeneralPath) shape).lineTo(293.972, 838.694);
        ((GeneralPath) shape).lineTo(294.056, 838.665);
        ((GeneralPath) shape).lineTo(294.139, 838.637);
        ((GeneralPath) shape).lineTo(294.221, 838.61);
        ((GeneralPath) shape).lineTo(294.306, 838.584);
        ((GeneralPath) shape).lineTo(294.459, 838.539);
        ((GeneralPath) shape).lineTo(294.612, 838.499);
        ((GeneralPath) shape).lineTo(294.76, 838.462);
        ((GeneralPath) shape).lineTo(294.902, 838.431);
        ((GeneralPath) shape).lineTo(295.038, 838.404);
        ((GeneralPath) shape).lineTo(295.168, 838.38);
        ((GeneralPath) shape).lineTo(295.292, 838.36);
        ((GeneralPath) shape).lineTo(295.409, 838.343);
        ((GeneralPath) shape).lineTo(295.518, 838.33);
        ((GeneralPath) shape).lineTo(295.62, 838.319);
        ((GeneralPath) shape).lineTo(295.716, 838.31);
        ((GeneralPath) shape).lineTo(295.802, 838.303);
        ((GeneralPath) shape).lineTo(295.886, 838.298);
        ((GeneralPath) shape).lineTo(296.011, 838.294);
        ((GeneralPath) shape).lineTo(296.102, 838.293);
        ((GeneralPath) shape).lineTo(296.124, 838.293);
        ((GeneralPath) shape).lineTo(337.49, 838.005);
        ((GeneralPath) shape).lineTo(325.845, 745.589);
        ((GeneralPath) shape).lineTo(186.605, 744.62);
        ((GeneralPath) shape).lineTo(186.132, 744.119);
        ((GeneralPath) shape).lineTo(186.049, 744.027);
        ((GeneralPath) shape).lineTo(185.988, 743.956);
        ((GeneralPath) shape).lineTo(185.92, 743.876);
        ((GeneralPath) shape).lineTo(185.844, 743.783);
        ((GeneralPath) shape).lineTo(185.76, 743.677);
        ((GeneralPath) shape).lineTo(185.668, 743.559);
        ((GeneralPath) shape).lineTo(185.568, 743.426);
        ((GeneralPath) shape).lineTo(185.462, 743.28);
        ((GeneralPath) shape).lineTo(185.348, 743.12);
        ((GeneralPath) shape).lineTo(185.228, 742.946);
        ((GeneralPath) shape).lineTo(185.104, 742.76);
        ((GeneralPath) shape).lineTo(185.037, 742.658);
        ((GeneralPath) shape).lineTo(184.97, 742.555);
        ((GeneralPath) shape).lineTo(184.902, 742.448);
        ((GeneralPath) shape).lineTo(184.833, 742.337);
        ((GeneralPath) shape).lineTo(184.762, 742.223);
        ((GeneralPath) shape).lineTo(184.691, 742.104);
        ((GeneralPath) shape).lineTo(184.618, 741.982);
        ((GeneralPath) shape).lineTo(184.543, 741.856);
        ((GeneralPath) shape).lineTo(184.468, 741.726);
        ((GeneralPath) shape).lineTo(184.392, 741.592);
        ((GeneralPath) shape).lineTo(184.315, 741.455);
        ((GeneralPath) shape).lineTo(184.236, 741.313);
        ((GeneralPath) shape).lineTo(184.157, 741.167);
        ((GeneralPath) shape).lineTo(184.077, 741.017);
        ((GeneralPath) shape).lineTo(183.996, 740.863);
        ((GeneralPath) shape).lineTo(183.914, 740.705);
        ((GeneralPath) shape).lineTo(183.832, 740.543);
        ((GeneralPath) shape).lineTo(183.749, 740.377);
        ((GeneralPath) shape).lineTo(183.665, 740.206);
        ((GeneralPath) shape).lineTo(183.581, 740.031);
        ((GeneralPath) shape).lineTo(183.495, 739.853);
        ((GeneralPath) shape).lineTo(183.41, 739.669);
        ((GeneralPath) shape).lineTo(183.324, 739.482);
        ((GeneralPath) shape).lineTo(183.237, 739.29);
        ((GeneralPath) shape).lineTo(183.15, 739.094);
        ((GeneralPath) shape).lineTo(183.063, 738.893);
        ((GeneralPath) shape).lineTo(182.975, 738.688);
        ((GeneralPath) shape).lineTo(182.887, 738.478);
        ((GeneralPath) shape).lineTo(182.799, 738.264);
        ((GeneralPath) shape).lineTo(182.711, 738.046);
        ((GeneralPath) shape).lineTo(182.622, 737.823);
        ((GeneralPath) shape).lineTo(182.533, 737.595);
        ((GeneralPath) shape).lineTo(182.445, 737.363);
        ((GeneralPath) shape).lineTo(182.356, 737.126);
        ((GeneralPath) shape).lineTo(182.267, 736.885);
        ((GeneralPath) shape).lineTo(182.178, 736.638);
        ((GeneralPath) shape).lineTo(182.09, 736.387);
        ((GeneralPath) shape).lineTo(182.001, 736.132);
        ((GeneralPath) shape).lineTo(181.913, 735.871);
        ((GeneralPath) shape).lineTo(181.825, 735.606);
        ((GeneralPath) shape).lineTo(181.737, 735.336);
        ((GeneralPath) shape).lineTo(181.65, 735.061);
        ((GeneralPath) shape).lineTo(181.562, 734.781);
        ((GeneralPath) shape).lineTo(181.476, 734.496);
        ((GeneralPath) shape).lineTo(181.389, 734.206);
        ((GeneralPath) shape).lineTo(181.303, 733.911);
        ((GeneralPath) shape).lineTo(181.218, 733.611);
        ((GeneralPath) shape).lineTo(181.133, 733.306);
        ((GeneralPath) shape).lineTo(181.049, 732.996);
        ((GeneralPath) shape).lineTo(180.965, 732.681);
        ((GeneralPath) shape).lineTo(180.882, 732.361);
        ((GeneralPath) shape).lineTo(180.8, 732.036);
        ((GeneralPath) shape).lineTo(180.719, 731.705);
        ((GeneralPath) shape).lineTo(180.638, 731.369);
        ((GeneralPath) shape).lineTo(180.559, 731.028);
        ((GeneralPath) shape).lineTo(180.48, 730.682);
        ((GeneralPath) shape).lineTo(180.402, 730.33);
        ((GeneralPath) shape).lineTo(180.325, 729.973);
        ((GeneralPath) shape).lineTo(180.249, 729.61);
        ((GeneralPath) shape).lineTo(180.174, 729.242);
        ((GeneralPath) shape).lineTo(180.1, 728.869);
        ((GeneralPath) shape).lineTo(180.028, 728.49);
        ((GeneralPath) shape).lineTo(179.956, 728.106);
        ((GeneralPath) shape).lineTo(179.886, 727.716);
        ((GeneralPath) shape).lineTo(179.817, 727.321);
        ((GeneralPath) shape).lineTo(179.75, 726.92);
        ((GeneralPath) shape).lineTo(179.684, 726.513);
        ((GeneralPath) shape).lineTo(179.619, 726.101);
        ((GeneralPath) shape).lineTo(179.555, 725.682);
        ((GeneralPath) shape).lineTo(179.493, 725.259);
        ((GeneralPath) shape).lineTo(179.433, 724.829);
        ((GeneralPath) shape).lineTo(179.374, 724.394);
        ((GeneralPath) shape).lineTo(179.317, 723.953);
        ((GeneralPath) shape).lineTo(179.261, 723.506);
        ((GeneralPath) shape).lineTo(179.207, 723.053);
        ((GeneralPath) shape).lineTo(179.155, 722.594);
        ((GeneralPath) shape).lineTo(179.105, 722.13);
        ((GeneralPath) shape).lineTo(179.056, 721.659);
        ((GeneralPath) shape).lineTo(179.009, 721.182);
        ((GeneralPath) shape).lineTo(178.965, 720.7);
        ((GeneralPath) shape).lineTo(178.922, 720.211);
        ((GeneralPath) shape).lineTo(178.881, 719.716);
        ((GeneralPath) shape).lineTo(178.842, 719.215);
        ((GeneralPath) shape).lineTo(178.805, 718.708);
        ((GeneralPath) shape).lineTo(178.77, 718.195);
        ((GeneralPath) shape).lineTo(178.738, 717.675);
        ((GeneralPath) shape).lineTo(178.707, 717.15);
        ((GeneralPath) shape).lineTo(178.679, 716.617);
        ((GeneralPath) shape).lineTo(178.653, 716.079);
        ((GeneralPath) shape).lineTo(178.63, 715.535);
        ((GeneralPath) shape).lineTo(178.609, 714.983);
        ((GeneralPath) shape).lineTo(178.59, 714.426);
        ((GeneralPath) shape).lineTo(178.573, 713.862);
        ((GeneralPath) shape).lineTo(178.56, 713.292);
        ((GeneralPath) shape).lineTo(178.548, 712.715);
        ((GeneralPath) shape).lineTo(178.539, 712.132);
        ((GeneralPath) shape).lineTo(178.533, 711.542);
        ((GeneralPath) shape).lineTo(178.53, 710.945);
        ((GeneralPath) shape).lineTo(178.529, 710.342);
        ((GeneralPath) shape).lineTo(178.531, 709.733);
        ((GeneralPath) shape).lineTo(178.533, 709.426);
        ((GeneralPath) shape).lineTo(178.536, 709.123);
        ((GeneralPath) shape).lineTo(178.539, 708.822);
        ((GeneralPath) shape).lineTo(178.544, 708.523);
        ((GeneralPath) shape).lineTo(178.549, 708.226);
        ((GeneralPath) shape).lineTo(178.555, 707.932);
        ((GeneralPath) shape).lineTo(178.562, 707.641);
        ((GeneralPath) shape).lineTo(178.569, 707.351);
        ((GeneralPath) shape).lineTo(178.577, 707.064);
        ((GeneralPath) shape).lineTo(178.586, 706.779);
        ((GeneralPath) shape).lineTo(178.596, 706.497);
        ((GeneralPath) shape).lineTo(178.606, 706.216);
        ((GeneralPath) shape).lineTo(178.618, 705.938);
        ((GeneralPath) shape).lineTo(178.63, 705.663);
        ((GeneralPath) shape).lineTo(178.642, 705.389);
        ((GeneralPath) shape).lineTo(178.655, 705.118);
        ((GeneralPath) shape).lineTo(178.67, 704.849);
        ((GeneralPath) shape).lineTo(178.684, 704.583);
        ((GeneralPath) shape).lineTo(178.7, 704.318);
        ((GeneralPath) shape).lineTo(178.716, 704.056);
        ((GeneralPath) shape).lineTo(178.733, 703.796);
        ((GeneralPath) shape).lineTo(178.75, 703.538);
        ((GeneralPath) shape).lineTo(178.768, 703.282);
        ((GeneralPath) shape).lineTo(178.787, 703.029);
        ((GeneralPath) shape).lineTo(178.806, 702.777);
        ((GeneralPath) shape).lineTo(178.827, 702.528);
        ((GeneralPath) shape).lineTo(178.847, 702.281);
        ((GeneralPath) shape).lineTo(178.869, 702.036);
        ((GeneralPath) shape).lineTo(178.891, 701.793);
        ((GeneralPath) shape).lineTo(178.913, 701.552);
        ((GeneralPath) shape).lineTo(178.937, 701.314);
        ((GeneralPath) shape).lineTo(178.961, 701.077);
        ((GeneralPath) shape).lineTo(178.985, 700.843);
        ((GeneralPath) shape).lineTo(179.01, 700.61);
        ((GeneralPath) shape).lineTo(179.036, 700.38);
        ((GeneralPath) shape).lineTo(179.062, 700.151);
        ((GeneralPath) shape).lineTo(179.089, 699.925);
        ((GeneralPath) shape).lineTo(179.117, 699.701);
        ((GeneralPath) shape).lineTo(179.145, 699.479);
        ((GeneralPath) shape).lineTo(179.174, 699.258);
        ((GeneralPath) shape).lineTo(179.203, 699.04);
        ((GeneralPath) shape).lineTo(179.233, 698.824);
        ((GeneralPath) shape).lineTo(179.263, 698.61);
        ((GeneralPath) shape).lineTo(179.294, 698.397);
        ((GeneralPath) shape).lineTo(179.325, 698.187);
        ((GeneralPath) shape).lineTo(179.357, 697.979);
        ((GeneralPath) shape).lineTo(179.39, 697.772);
        ((GeneralPath) shape).lineTo(179.423, 697.568);
        ((GeneralPath) shape).lineTo(179.456, 697.365);
        ((GeneralPath) shape).lineTo(179.49, 697.164);
        ((GeneralPath) shape).lineTo(179.525, 696.965);
        ((GeneralPath) shape).lineTo(179.56, 696.768);
        ((GeneralPath) shape).lineTo(179.596, 696.573);
        ((GeneralPath) shape).lineTo(179.632, 696.38);
        ((GeneralPath) shape).lineTo(179.668, 696.189);
        ((GeneralPath) shape).lineTo(179.705, 695.999);
        ((GeneralPath) shape).lineTo(179.743, 695.812);
        ((GeneralPath) shape).lineTo(179.781, 695.626);
        ((GeneralPath) shape).lineTo(179.819, 695.442);
        ((GeneralPath) shape).lineTo(179.858, 695.259);
        ((GeneralPath) shape).lineTo(179.898, 695.079);
        ((GeneralPath) shape).lineTo(179.938, 694.9);
        ((GeneralPath) shape).lineTo(179.978, 694.724);
        ((GeneralPath) shape).lineTo(180.019, 694.548);
        ((GeneralPath) shape).lineTo(180.06, 694.375);
        ((GeneralPath) shape).lineTo(180.101, 694.203);
        ((GeneralPath) shape).lineTo(180.144, 694.034);
        ((GeneralPath) shape).lineTo(180.186, 693.865);
        ((GeneralPath) shape).lineTo(180.229, 693.699);
        ((GeneralPath) shape).lineTo(180.272, 693.534);
        ((GeneralPath) shape).lineTo(180.316, 693.371);
        ((GeneralPath) shape).lineTo(180.36, 693.21);
        ((GeneralPath) shape).lineTo(180.404, 693.05);
        ((GeneralPath) shape).lineTo(180.449, 692.892);
        ((GeneralPath) shape).lineTo(180.495, 692.736);
        ((GeneralPath) shape).lineTo(180.54, 692.581);
        ((GeneralPath) shape).lineTo(180.586, 692.428);
        ((GeneralPath) shape).lineTo(180.633, 692.277);
        ((GeneralPath) shape).lineTo(180.679, 692.127);
        ((GeneralPath) shape).lineTo(180.726, 691.979);
        ((GeneralPath) shape).lineTo(180.774, 691.832);
        ((GeneralPath) shape).lineTo(180.822, 691.687);
        ((GeneralPath) shape).lineTo(180.87, 691.544);
        ((GeneralPath) shape).lineTo(180.918, 691.402);
        ((GeneralPath) shape).lineTo(180.967, 691.261);
        ((GeneralPath) shape).lineTo(181.016, 691.123);
        ((GeneralPath) shape).lineTo(181.066, 690.985);
        ((GeneralPath) shape).lineTo(181.116, 690.85);
        ((GeneralPath) shape).lineTo(181.166, 690.715);
        ((GeneralPath) shape).lineTo(181.216, 690.583);
        ((GeneralPath) shape).lineTo(181.267, 690.452);
        ((GeneralPath) shape).lineTo(181.318, 690.322);
        ((GeneralPath) shape).lineTo(181.369, 690.194);
        ((GeneralPath) shape).lineTo(181.421, 690.067);
        ((GeneralPath) shape).lineTo(181.473, 689.942);
        ((GeneralPath) shape).lineTo(181.525, 689.818);
        ((GeneralPath) shape).lineTo(181.577, 689.696);
        ((GeneralPath) shape).lineTo(181.63, 689.575);
        ((GeneralPath) shape).lineTo(181.683, 689.455);
        ((GeneralPath) shape).lineTo(181.736, 689.337);
        ((GeneralPath) shape).lineTo(181.79, 689.221);
        ((GeneralPath) shape).lineTo(181.844, 689.105);
        ((GeneralPath) shape).lineTo(181.898, 688.991);
        ((GeneralPath) shape).lineTo(181.953, 688.877);
        ((GeneralPath) shape).lineTo(182.061, 688.658);
        ((GeneralPath) shape).lineTo(182.171, 688.443);
        ((GeneralPath) shape).lineTo(182.282, 688.233);
        ((GeneralPath) shape).lineTo(182.394, 688.028);
        ((GeneralPath) shape).lineTo(182.506, 687.829);
        ((GeneralPath) shape).lineTo(182.619, 687.634);
        ((GeneralPath) shape).lineTo(182.733, 687.445);
        ((GeneralPath) shape).lineTo(182.848, 687.261);
        ((GeneralPath) shape).lineTo(182.963, 687.081);
        ((GeneralPath) shape).lineTo(183.079, 686.907);
        ((GeneralPath) shape).lineTo(183.195, 686.738);
        ((GeneralPath) shape).lineTo(183.312, 686.573);
        ((GeneralPath) shape).lineTo(183.429, 686.413);
        ((GeneralPath) shape).lineTo(183.547, 686.257);
        ((GeneralPath) shape).lineTo(183.665, 686.107);
        ((GeneralPath) shape).lineTo(183.783, 685.961);
        ((GeneralPath) shape).lineTo(183.902, 685.819);
        ((GeneralPath) shape).lineTo(184.02, 685.682);
        ((GeneralPath) shape).lineTo(184.139, 685.548);
        ((GeneralPath) shape).lineTo(184.258, 685.42);
        ((GeneralPath) shape).lineTo(184.377, 685.295);
        ((GeneralPath) shape).lineTo(184.496, 685.175);
        ((GeneralPath) shape).lineTo(184.615, 685.059);
        ((GeneralPath) shape).lineTo(184.734, 684.947);
        ((GeneralPath) shape).lineTo(184.852, 684.839);
        ((GeneralPath) shape).lineTo(184.971, 684.735);
        ((GeneralPath) shape).lineTo(185.089, 684.635);
        ((GeneralPath) shape).lineTo(185.207, 684.539);
        ((GeneralPath) shape).lineTo(185.324, 684.446);
        ((GeneralPath) shape).lineTo(185.44, 684.358);
        ((GeneralPath) shape).lineTo(185.557, 684.272);
        ((GeneralPath) shape).lineTo(185.672, 684.191);
        ((GeneralPath) shape).lineTo(185.787, 684.113);
        ((GeneralPath) shape).lineTo(185.901, 684.039);
        ((GeneralPath) shape).lineTo(186.014, 683.968);
        ((GeneralPath) shape).lineTo(186.126, 683.9);
        ((GeneralPath) shape).lineTo(186.237, 683.836);
        ((GeneralPath) shape).lineTo(186.347, 683.775);
        ((GeneralPath) shape).lineTo(186.456, 683.717);
        ((GeneralPath) shape).lineTo(186.564, 683.662);
        ((GeneralPath) shape).lineTo(186.671, 683.61);
        ((GeneralPath) shape).lineTo(186.776, 683.562);
        ((GeneralPath) shape).lineTo(186.879, 683.516);
        ((GeneralPath) shape).lineTo(186.981, 683.473);
        ((GeneralPath) shape).lineTo(187.082, 683.432);
        ((GeneralPath) shape).lineTo(187.181, 683.394);
        ((GeneralPath) shape).lineTo(187.278, 683.359);
        ((GeneralPath) shape).lineTo(187.373, 683.327);
        ((GeneralPath) shape).lineTo(187.467, 683.296);
        ((GeneralPath) shape).lineTo(187.559, 683.268);
        ((GeneralPath) shape).lineTo(187.648, 683.242);
        ((GeneralPath) shape).lineTo(187.735, 683.219);
        ((GeneralPath) shape).lineTo(187.82, 683.197);
        ((GeneralPath) shape).lineTo(187.903, 683.178);
        ((GeneralPath) shape).lineTo(187.984, 683.16);
        ((GeneralPath) shape).lineTo(188.063, 683.144);
        ((GeneralPath) shape).lineTo(188.138, 683.13);
        ((GeneralPath) shape).lineTo(188.217, 683.117);
        ((GeneralPath) shape).lineTo(188.351, 683.097);
        ((GeneralPath) shape).lineTo(188.479, 683.082);
        ((GeneralPath) shape).lineTo(188.596, 683.072);
        ((GeneralPath) shape).lineTo(188.701, 683.066);
        ((GeneralPath) shape).lineTo(188.795, 683.062);
        ((GeneralPath) shape).lineTo(188.876, 683.062);
        ((GeneralPath) shape).lineTo(188.947, 683.063);
        ((GeneralPath) shape).lineTo(188.958, 683.064);
        ((GeneralPath) shape).lineTo(300.228, 683.064);
        ((GeneralPath) shape).lineTo(324.725, 674.137);
        ((GeneralPath) shape).lineTo(325.409, 667.902);
        ((GeneralPath) shape).lineTo(326.223, 660.498);
        ((GeneralPath) shape).lineTo(327.167, 651.921);
        ((GeneralPath) shape).lineTo(327.653, 647.516);
        ((GeneralPath) shape).lineTo(328.13, 643.203);
        ((GeneralPath) shape).lineTo(328.583, 639.114);
        ((GeneralPath) shape).lineTo(329.0, 635.376);
        ((GeneralPath) shape).lineTo(329.364, 632.119);
        ((GeneralPath) shape).lineTo(329.523, 630.71);
        ((GeneralPath) shape).lineTo(329.664, 629.47);
        ((GeneralPath) shape).lineTo(329.785, 628.414);
        ((GeneralPath) shape).lineTo(329.884, 627.557);
        ((GeneralPath) shape).lineTo(329.96, 626.915);
        ((GeneralPath) shape).lineTo(329.99, 626.676);
        ((GeneralPath) shape).lineTo(330.089, 625.904);
        ((GeneralPath) shape).lineTo(330.666, 625.438);
        ((GeneralPath) shape).lineTo(330.782, 625.346);
        ((GeneralPath) shape).lineTo(330.899, 625.257);
        ((GeneralPath) shape).lineTo(331.017, 625.168);
        ((GeneralPath) shape).lineTo(331.135, 625.083);
        ((GeneralPath) shape).lineTo(331.254, 624.999);
        ((GeneralPath) shape).lineTo(331.374, 624.916);
        ((GeneralPath) shape).lineTo(331.496, 624.836);
        ((GeneralPath) shape).lineTo(331.618, 624.757);
        ((GeneralPath) shape).lineTo(331.742, 624.681);
        ((GeneralPath) shape).lineTo(331.866, 624.607);
        ((GeneralPath) shape).lineTo(331.993, 624.534);
        ((GeneralPath) shape).lineTo(332.12, 624.463);
        ((GeneralPath) shape).lineTo(332.248, 624.395);
        ((GeneralPath) shape).lineTo(332.378, 624.328);
        ((GeneralPath) shape).lineTo(332.509, 624.264);
        ((GeneralPath) shape).lineTo(332.642, 624.202);
        ((GeneralPath) shape).lineTo(332.776, 624.141);
        ((GeneralPath) shape).lineTo(332.911, 624.083);
        ((GeneralPath) shape).lineTo(333.048, 624.026);
        ((GeneralPath) shape).lineTo(333.186, 623.972);
        ((GeneralPath) shape).lineTo(333.325, 623.92);
        ((GeneralPath) shape).lineTo(333.466, 623.869);
        ((GeneralPath) shape).lineTo(333.609, 623.82);
        ((GeneralPath) shape).lineTo(333.753, 623.774);
        ((GeneralPath) shape).lineTo(333.898, 623.729);
        ((GeneralPath) shape).lineTo(334.045, 623.686);
        ((GeneralPath) shape).lineTo(334.194, 623.645);
        ((GeneralPath) shape).lineTo(334.344, 623.605);
        ((GeneralPath) shape).lineTo(334.496, 623.568);
        ((GeneralPath) shape).lineTo(334.651, 623.531);
        ((GeneralPath) shape).lineTo(334.807, 623.497);
        ((GeneralPath) shape).lineTo(334.964, 623.464);
        ((GeneralPath) shape).lineTo(335.124, 623.432);
        ((GeneralPath) shape).lineTo(335.286, 623.402);
        ((GeneralPath) shape).lineTo(335.45, 623.373);
        ((GeneralPath) shape).lineTo(335.616, 623.346);
        ((GeneralPath) shape).lineTo(335.785, 623.319);
        ((GeneralPath) shape).lineTo(335.956, 623.294);
        ((GeneralPath) shape).lineTo(336.129, 623.271);
        ((GeneralPath) shape).lineTo(336.306, 623.248);
        ((GeneralPath) shape).lineTo(336.485, 623.226);
        ((GeneralPath) shape).lineTo(336.667, 623.205);
        ((GeneralPath) shape).lineTo(336.851, 623.186);
        ((GeneralPath) shape).lineTo(337.039, 623.167);
        ((GeneralPath) shape).lineTo(337.23, 623.149);
        ((GeneralPath) shape).lineTo(337.424, 623.132);
        ((GeneralPath) shape).lineTo(337.621, 623.116);
        ((GeneralPath) shape).lineTo(337.822, 623.1);
        ((GeneralPath) shape).lineTo(338.027, 623.086);
        ((GeneralPath) shape).lineTo(338.235, 623.072);
        ((GeneralPath) shape).lineTo(338.447, 623.058);
        ((GeneralPath) shape).lineTo(338.662, 623.046);
        ((GeneralPath) shape).lineTo(338.883, 623.033);
        ((GeneralPath) shape).lineTo(339.334, 623.011);
        ((GeneralPath) shape).lineTo(339.802, 622.99);
        ((GeneralPath) shape).lineTo(340.258, 622.972);
        ((GeneralPath) shape).lineTo(340.277, 622.892);
        ((GeneralPath) shape).lineTo(340.348, 622.603);
        ((GeneralPath) shape).lineTo(340.427, 622.29);
        ((GeneralPath) shape).lineTo(340.47, 622.123);
        ((GeneralPath) shape).lineTo(340.515, 621.951);
        ((GeneralPath) shape).lineTo(340.563, 621.774);
        ((GeneralPath) shape).lineTo(340.613, 621.593);
        ((GeneralPath) shape).lineTo(340.666, 621.407);
        ((GeneralPath) shape).lineTo(340.721, 621.216);
        ((GeneralPath) shape).lineTo(340.779, 621.022);
        ((GeneralPath) shape).lineTo(340.84, 620.825);
        ((GeneralPath) shape).lineTo(340.904, 620.624);
        ((GeneralPath) shape).lineTo(340.971, 620.42);
        ((GeneralPath) shape).lineTo(341.041, 620.214);
        ((GeneralPath) shape).lineTo(341.115, 620.005);
        ((GeneralPath) shape).lineTo(341.192, 619.794);
        ((GeneralPath) shape).lineTo(341.272, 619.581);
        ((GeneralPath) shape).lineTo(341.356, 619.367);
        ((GeneralPath) shape).lineTo(341.443, 619.154);
        ((GeneralPath) shape).lineTo(341.489, 619.043);
        ((GeneralPath) shape).lineTo(341.535, 618.935);
        ((GeneralPath) shape).lineTo(341.583, 618.827);
        ((GeneralPath) shape).lineTo(341.631, 618.718);
        ((GeneralPath) shape).lineTo(341.68, 618.609);
        ((GeneralPath) shape).lineTo(341.731, 618.5);
        ((GeneralPath) shape).lineTo(341.782, 618.392);
        ((GeneralPath) shape).lineTo(341.835, 618.283);
        ((GeneralPath) shape).lineTo(341.889, 618.174);
        ((GeneralPath) shape).lineTo(341.944, 618.065);
        ((GeneralPath) shape).lineTo(342.0, 617.956);
        ((GeneralPath) shape).lineTo(342.057, 617.848);
        ((GeneralPath) shape).lineTo(342.116, 617.74);
        ((GeneralPath) shape).lineTo(342.175, 617.632);
        ((GeneralPath) shape).lineTo(342.236, 617.524);
        ((GeneralPath) shape).lineTo(342.298, 617.416);
        ((GeneralPath) shape).lineTo(342.362, 617.309);
        ((GeneralPath) shape).lineTo(342.427, 617.202);
        ((GeneralPath) shape).lineTo(342.493, 617.095);
        ((GeneralPath) shape).lineTo(342.56, 616.989);
        ((GeneralPath) shape).lineTo(342.629, 616.884);
        ((GeneralPath) shape).lineTo(342.7, 616.779);
        ((GeneralPath) shape).lineTo(342.771, 616.674);
        ((GeneralPath) shape).lineTo(342.845, 616.57);
        ((GeneralPath) shape).lineTo(342.919, 616.467);
        ((GeneralPath) shape).lineTo(342.996, 616.364);
        ((GeneralPath) shape).lineTo(343.073, 616.263);
        ((GeneralPath) shape).lineTo(343.153, 616.162);
        ((GeneralPath) shape).lineTo(343.234, 616.061);
        ((GeneralPath) shape).lineTo(343.316, 615.962);
        ((GeneralPath) shape).lineTo(343.4, 615.864);
        ((GeneralPath) shape).lineTo(343.487, 615.766);
        ((GeneralPath) shape).lineTo(343.574, 615.669);
        ((GeneralPath) shape).lineTo(343.663, 615.574);
        ((GeneralPath) shape).lineTo(343.754, 615.48);
        ((GeneralPath) shape).lineTo(343.847, 615.387);
        ((GeneralPath) shape).lineTo(343.942, 615.295);
        ((GeneralPath) shape).lineTo(344.039, 615.204);
        ((GeneralPath) shape).lineTo(344.137, 615.115);
        ((GeneralPath) shape).lineTo(344.237, 615.027);
        ((GeneralPath) shape).lineTo(344.339, 614.941);
        ((GeneralPath) shape).lineTo(344.444, 614.856);
        ((GeneralPath) shape).lineTo(344.55, 614.773);
        ((GeneralPath) shape).lineTo(344.657, 614.692);
        ((GeneralPath) shape).lineTo(344.767, 614.612);
        ((GeneralPath) shape).lineTo(344.88, 614.534);
        ((GeneralPath) shape).lineTo(344.993, 614.458);
        ((GeneralPath) shape).lineTo(345.109, 614.384);
        ((GeneralPath) shape).lineTo(345.226, 614.312);
        ((GeneralPath) shape).lineTo(345.346, 614.242);
        ((GeneralPath) shape).lineTo(345.468, 614.175);
        ((GeneralPath) shape).lineTo(345.591, 614.109);
        ((GeneralPath) shape).lineTo(345.717, 614.046);
        ((GeneralPath) shape).lineTo(345.845, 613.986);
        ((GeneralPath) shape).lineTo(345.974, 613.928);
        ((GeneralPath) shape).lineTo(346.674, 613.631);
        ((GeneralPath) shape).lineTo(347.374, 613.928);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(346.634, 616.97);
        ((GeneralPath) shape).lineTo(346.569, 617.013);
        ((GeneralPath) shape).lineTo(346.505, 617.057);
        ((GeneralPath) shape).lineTo(346.442, 617.103);
        ((GeneralPath) shape).lineTo(346.379, 617.151);
        ((GeneralPath) shape).lineTo(346.317, 617.2);
        ((GeneralPath) shape).lineTo(346.255, 617.25);
        ((GeneralPath) shape).lineTo(346.194, 617.302);
        ((GeneralPath) shape).lineTo(346.133, 617.355);
        ((GeneralPath) shape).lineTo(346.073, 617.409);
        ((GeneralPath) shape).lineTo(346.013, 617.465);
        ((GeneralPath) shape).lineTo(345.954, 617.523);
        ((GeneralPath) shape).lineTo(345.895, 617.581);
        ((GeneralPath) shape).lineTo(345.837, 617.642);
        ((GeneralPath) shape).lineTo(345.779, 617.703);
        ((GeneralPath) shape).lineTo(345.722, 617.766);
        ((GeneralPath) shape).lineTo(345.666, 617.83);
        ((GeneralPath) shape).lineTo(345.61, 617.896);
        ((GeneralPath) shape).lineTo(345.555, 617.963);
        ((GeneralPath) shape).lineTo(345.5, 618.03);
        ((GeneralPath) shape).lineTo(345.445, 618.1);
        ((GeneralPath) shape).lineTo(345.392, 618.17);
        ((GeneralPath) shape).lineTo(345.339, 618.241);
        ((GeneralPath) shape).lineTo(345.286, 618.314);
        ((GeneralPath) shape).lineTo(345.234, 618.388);
        ((GeneralPath) shape).lineTo(345.183, 618.462);
        ((GeneralPath) shape).lineTo(345.132, 618.538);
        ((GeneralPath) shape).lineTo(345.082, 618.615);
        ((GeneralPath) shape).lineTo(345.032, 618.693);
        ((GeneralPath) shape).lineTo(344.984, 618.771);
        ((GeneralPath) shape).lineTo(344.935, 618.851);
        ((GeneralPath) shape).lineTo(344.888, 618.931);
        ((GeneralPath) shape).lineTo(344.841, 619.012);
        ((GeneralPath) shape).lineTo(344.794, 619.094);
        ((GeneralPath) shape).lineTo(344.749, 619.177);
        ((GeneralPath) shape).lineTo(344.704, 619.261);
        ((GeneralPath) shape).lineTo(344.659, 619.345);
        ((GeneralPath) shape).lineTo(344.616, 619.43);
        ((GeneralPath) shape).lineTo(344.573, 619.515);
        ((GeneralPath) shape).lineTo(344.53, 619.6);
        ((GeneralPath) shape).lineTo(344.488, 619.687);
        ((GeneralPath) shape).lineTo(344.447, 619.774);
        ((GeneralPath) shape).lineTo(344.407, 619.861);
        ((GeneralPath) shape).lineTo(344.367, 619.948);
        ((GeneralPath) shape).lineTo(344.328, 620.036);
        ((GeneralPath) shape).lineTo(344.289, 620.125);
        ((GeneralPath) shape).lineTo(344.251, 620.213);
        ((GeneralPath) shape).lineTo(344.215, 620.3);
        ((GeneralPath) shape).lineTo(344.142, 620.48);
        ((GeneralPath) shape).lineTo(344.072, 620.659);
        ((GeneralPath) shape).lineTo(344.004, 620.837);
        ((GeneralPath) shape).lineTo(343.939, 621.016);
        ((GeneralPath) shape).lineTo(343.877, 621.194);
        ((GeneralPath) shape).lineTo(343.816, 621.371);
        ((GeneralPath) shape).lineTo(343.759, 621.547);
        ((GeneralPath) shape).lineTo(343.703, 621.722);
        ((GeneralPath) shape).lineTo(343.65, 621.895);
        ((GeneralPath) shape).lineTo(343.599, 622.066);
        ((GeneralPath) shape).lineTo(343.549, 622.234);
        ((GeneralPath) shape).lineTo(343.502, 622.4);
        ((GeneralPath) shape).lineTo(343.457, 622.563);
        ((GeneralPath) shape).lineTo(343.414, 622.723);
        ((GeneralPath) shape).lineTo(343.373, 622.88);
        ((GeneralPath) shape).lineTo(343.334, 623.031);
        ((GeneralPath) shape).lineTo(343.26, 623.327);
        ((GeneralPath) shape).lineTo(343.192, 623.603);
        ((GeneralPath) shape).lineTo(343.129, 623.861);
        ((GeneralPath) shape).lineTo(343.071, 624.1);
        ((GeneralPath) shape).lineTo(343.015, 624.318);
        ((GeneralPath) shape).lineTo(342.987, 624.425);
        ((GeneralPath) shape).lineTo(342.958, 624.528);
        ((GeneralPath) shape).lineTo(342.928, 624.63);
        ((GeneralPath) shape).lineTo(342.894, 624.735);
        ((GeneralPath) shape).lineTo(342.853, 624.848);
        ((GeneralPath) shape).lineTo(342.804, 624.965);
        ((GeneralPath) shape).lineTo(342.762, 625.051);
        ((GeneralPath) shape).lineTo(342.715, 625.139);
        ((GeneralPath) shape).lineTo(342.65, 625.243);
        ((GeneralPath) shape).lineTo(342.556, 625.371);
        ((GeneralPath) shape).lineTo(342.413, 625.526);
        ((GeneralPath) shape).lineTo(342.192, 625.702);
        ((GeneralPath) shape).lineTo(341.857, 625.868);
        ((GeneralPath) shape).lineTo(341.531, 625.931);
        ((GeneralPath) shape).lineTo(340.894, 625.951);
        ((GeneralPath) shape).lineTo(340.4, 625.969);
        ((GeneralPath) shape).lineTo(339.928, 625.987);
        ((GeneralPath) shape).lineTo(339.475, 626.008);
        ((GeneralPath) shape).lineTo(339.041, 626.029);
        ((GeneralPath) shape).lineTo(338.834, 626.041);
        ((GeneralPath) shape).lineTo(338.629, 626.053);
        ((GeneralPath) shape).lineTo(338.43, 626.065);
        ((GeneralPath) shape).lineTo(338.235, 626.079);
        ((GeneralPath) shape).lineTo(338.045, 626.092);
        ((GeneralPath) shape).lineTo(337.859, 626.106);
        ((GeneralPath) shape).lineTo(337.677, 626.121);
        ((GeneralPath) shape).lineTo(337.501, 626.137);
        ((GeneralPath) shape).lineTo(337.328, 626.153);
        ((GeneralPath) shape).lineTo(337.159, 626.17);
        ((GeneralPath) shape).lineTo(336.995, 626.187);
        ((GeneralPath) shape).lineTo(336.834, 626.206);
        ((GeneralPath) shape).lineTo(336.678, 626.225);
        ((GeneralPath) shape).lineTo(336.526, 626.244);
        ((GeneralPath) shape).lineTo(336.377, 626.265);
        ((GeneralPath) shape).lineTo(336.233, 626.286);
        ((GeneralPath) shape).lineTo(336.092, 626.308);
        ((GeneralPath) shape).lineTo(335.954, 626.331);
        ((GeneralPath) shape).lineTo(335.82, 626.354);
        ((GeneralPath) shape).lineTo(335.69, 626.378);
        ((GeneralPath) shape).lineTo(335.563, 626.403);
        ((GeneralPath) shape).lineTo(335.439, 626.43);
        ((GeneralPath) shape).lineTo(335.319, 626.456);
        ((GeneralPath) shape).lineTo(335.201, 626.484);
        ((GeneralPath) shape).lineTo(335.087, 626.512);
        ((GeneralPath) shape).lineTo(334.975, 626.542);
        ((GeneralPath) shape).lineTo(334.866, 626.572);
        ((GeneralPath) shape).lineTo(334.76, 626.603);
        ((GeneralPath) shape).lineTo(334.656, 626.635);
        ((GeneralPath) shape).lineTo(334.554, 626.668);
        ((GeneralPath) shape).lineTo(334.455, 626.701);
        ((GeneralPath) shape).lineTo(334.358, 626.736);
        ((GeneralPath) shape).lineTo(334.263, 626.772);
        ((GeneralPath) shape).lineTo(334.17, 626.809);
        ((GeneralPath) shape).lineTo(334.078, 626.847);
        ((GeneralPath) shape).lineTo(333.987, 626.886);
        ((GeneralPath) shape).lineTo(333.899, 626.926);
        ((GeneralPath) shape).lineTo(333.811, 626.967);
        ((GeneralPath) shape).lineTo(333.724, 627.01);
        ((GeneralPath) shape).lineTo(333.638, 627.054);
        ((GeneralPath) shape).lineTo(333.553, 627.099);
        ((GeneralPath) shape).lineTo(333.468, 627.146);
        ((GeneralPath) shape).lineTo(333.384, 627.194);
        ((GeneralPath) shape).lineTo(333.3, 627.244);
        ((GeneralPath) shape).lineTo(333.216, 627.296);
        ((GeneralPath) shape).lineTo(333.133, 627.35);
        ((GeneralPath) shape).lineTo(333.049, 627.406);
        ((GeneralPath) shape).lineTo(332.965, 627.463);
        ((GeneralPath) shape).lineTo(332.912, 627.5);
        ((GeneralPath) shape).lineTo(332.864, 627.907);
        ((GeneralPath) shape).lineTo(332.765, 628.757);
        ((GeneralPath) shape).lineTo(332.644, 629.809);
        ((GeneralPath) shape).lineTo(332.504, 631.047);
        ((GeneralPath) shape).lineTo(332.346, 632.454);
        ((GeneralPath) shape).lineTo(331.981, 635.709);
        ((GeneralPath) shape).lineTo(331.565, 639.445);
        ((GeneralPath) shape).lineTo(331.112, 643.534);
        ((GeneralPath) shape).lineTo(330.635, 647.845);
        ((GeneralPath) shape).lineTo(330.149, 652.25);
        ((GeneralPath) shape).lineTo(329.205, 660.826);
        ((GeneralPath) shape).lineTo(328.391, 668.23);
        ((GeneralPath) shape).lineTo(327.504, 676.318);
        ((GeneralPath) shape).lineTo(300.757, 686.064);
        ((GeneralPath) shape).lineTo(188.888, 686.064);
        ((GeneralPath) shape).lineTo(188.854, 686.062);
        ((GeneralPath) shape).lineTo(188.841, 686.063);
        ((GeneralPath) shape).lineTo(188.817, 686.064);
        ((GeneralPath) shape).lineTo(188.784, 686.067);
        ((GeneralPath) shape).lineTo(188.743, 686.072);
        ((GeneralPath) shape).lineTo(188.687, 686.08);
        ((GeneralPath) shape).lineTo(188.664, 686.084);
        ((GeneralPath) shape).lineTo(188.634, 686.089);
        ((GeneralPath) shape).lineTo(188.602, 686.096);
        ((GeneralPath) shape).lineTo(188.568, 686.103);
        ((GeneralPath) shape).lineTo(188.532, 686.112);
        ((GeneralPath) shape).lineTo(188.494, 686.121);
        ((GeneralPath) shape).lineTo(188.453, 686.132);
        ((GeneralPath) shape).lineTo(188.412, 686.144);
        ((GeneralPath) shape).lineTo(188.368, 686.158);
        ((GeneralPath) shape).lineTo(188.323, 686.172);
        ((GeneralPath) shape).lineTo(188.276, 686.188);
        ((GeneralPath) shape).lineTo(188.227, 686.206);
        ((GeneralPath) shape).lineTo(188.177, 686.225);
        ((GeneralPath) shape).lineTo(188.125, 686.246);
        ((GeneralPath) shape).lineTo(188.071, 686.269);
        ((GeneralPath) shape).lineTo(188.016, 686.293);
        ((GeneralPath) shape).lineTo(187.958, 686.32);
        ((GeneralPath) shape).lineTo(187.9, 686.348);
        ((GeneralPath) shape).lineTo(187.84, 686.379);
        ((GeneralPath) shape).lineTo(187.779, 686.412);
        ((GeneralPath) shape).lineTo(187.716, 686.447);
        ((GeneralPath) shape).lineTo(187.651, 686.484);
        ((GeneralPath) shape).lineTo(187.585, 686.523);
        ((GeneralPath) shape).lineTo(187.518, 686.566);
        ((GeneralPath) shape).lineTo(187.449, 686.611);
        ((GeneralPath) shape).lineTo(187.379, 686.658);
        ((GeneralPath) shape).lineTo(187.308, 686.709);
        ((GeneralPath) shape).lineTo(187.235, 686.762);
        ((GeneralPath) shape).lineTo(187.161, 686.818);
        ((GeneralPath) shape).lineTo(187.086, 686.877);
        ((GeneralPath) shape).lineTo(187.009, 686.94);
        ((GeneralPath) shape).lineTo(186.932, 687.006);
        ((GeneralPath) shape).lineTo(186.853, 687.075);
        ((GeneralPath) shape).lineTo(186.773, 687.148);
        ((GeneralPath) shape).lineTo(186.692, 687.224);
        ((GeneralPath) shape).lineTo(186.61, 687.304);
        ((GeneralPath) shape).lineTo(186.528, 687.387);
        ((GeneralPath) shape).lineTo(186.444, 687.475);
        ((GeneralPath) shape).lineTo(186.359, 687.567);
        ((GeneralPath) shape).lineTo(186.274, 687.662);
        ((GeneralPath) shape).lineTo(186.187, 687.762);
        ((GeneralPath) shape).lineTo(186.1, 687.866);
        ((GeneralPath) shape).lineTo(186.013, 687.975);
        ((GeneralPath) shape).lineTo(185.924, 688.088);
        ((GeneralPath) shape).lineTo(185.835, 688.205);
        ((GeneralPath) shape).lineTo(185.746, 688.327);
        ((GeneralPath) shape).lineTo(185.656, 688.454);
        ((GeneralPath) shape).lineTo(185.565, 688.586);
        ((GeneralPath) shape).lineTo(185.475, 688.723);
        ((GeneralPath) shape).lineTo(185.384, 688.864);
        ((GeneralPath) shape).lineTo(185.292, 689.011);
        ((GeneralPath) shape).lineTo(185.201, 689.163);
        ((GeneralPath) shape).lineTo(185.109, 689.321);
        ((GeneralPath) shape).lineTo(185.017, 689.484);
        ((GeneralPath) shape).lineTo(184.925, 689.652);
        ((GeneralPath) shape).lineTo(184.833, 689.826);
        ((GeneralPath) shape).lineTo(184.742, 690.005);
        ((GeneralPath) shape).lineTo(184.649, 690.193);
        ((GeneralPath) shape).lineTo(184.604, 690.286);
        ((GeneralPath) shape).lineTo(184.558, 690.382);
        ((GeneralPath) shape).lineTo(184.513, 690.48);
        ((GeneralPath) shape).lineTo(184.467, 690.579);
        ((GeneralPath) shape).lineTo(184.422, 690.68);
        ((GeneralPath) shape).lineTo(184.376, 690.782);
        ((GeneralPath) shape).lineTo(184.331, 690.886);
        ((GeneralPath) shape).lineTo(184.286, 690.992);
        ((GeneralPath) shape).lineTo(184.241, 691.098);
        ((GeneralPath) shape).lineTo(184.196, 691.207);
        ((GeneralPath) shape).lineTo(184.151, 691.317);
        ((GeneralPath) shape).lineTo(184.106, 691.429);
        ((GeneralPath) shape).lineTo(184.062, 691.542);
        ((GeneralPath) shape).lineTo(184.017, 691.657);
        ((GeneralPath) shape).lineTo(183.973, 691.773);
        ((GeneralPath) shape).lineTo(183.929, 691.891);
        ((GeneralPath) shape).lineTo(183.885, 692.011);
        ((GeneralPath) shape).lineTo(183.841, 692.132);
        ((GeneralPath) shape).lineTo(183.798, 692.255);
        ((GeneralPath) shape).lineTo(183.754, 692.38);
        ((GeneralPath) shape).lineTo(183.711, 692.506);
        ((GeneralPath) shape).lineTo(183.668, 692.634);
        ((GeneralPath) shape).lineTo(183.626, 692.764);
        ((GeneralPath) shape).lineTo(183.583, 692.895);
        ((GeneralPath) shape).lineTo(183.541, 693.028);
        ((GeneralPath) shape).lineTo(183.499, 693.163);
        ((GeneralPath) shape).lineTo(183.457, 693.299);
        ((GeneralPath) shape).lineTo(183.415, 693.438);
        ((GeneralPath) shape).lineTo(183.374, 693.577);
        ((GeneralPath) shape).lineTo(183.333, 693.719);
        ((GeneralPath) shape).lineTo(183.292, 693.863);
        ((GeneralPath) shape).lineTo(183.252, 694.008);
        ((GeneralPath) shape).lineTo(183.212, 694.155);
        ((GeneralPath) shape).lineTo(183.172, 694.304);
        ((GeneralPath) shape).lineTo(183.132, 694.454);
        ((GeneralPath) shape).lineTo(183.093, 694.607);
        ((GeneralPath) shape).lineTo(183.054, 694.761);
        ((GeneralPath) shape).lineTo(183.015, 694.917);
        ((GeneralPath) shape).lineTo(182.977, 695.075);
        ((GeneralPath) shape).lineTo(182.939, 695.235);
        ((GeneralPath) shape).lineTo(182.901, 695.397);
        ((GeneralPath) shape).lineTo(182.864, 695.56);
        ((GeneralPath) shape).lineTo(182.827, 695.726);
        ((GeneralPath) shape).lineTo(182.791, 695.893);
        ((GeneralPath) shape).lineTo(182.754, 696.062);
        ((GeneralPath) shape).lineTo(182.719, 696.233);
        ((GeneralPath) shape).lineTo(182.683, 696.406);
        ((GeneralPath) shape).lineTo(182.648, 696.581);
        ((GeneralPath) shape).lineTo(182.614, 696.758);
        ((GeneralPath) shape).lineTo(182.58, 696.937);
        ((GeneralPath) shape).lineTo(182.546, 697.118);
        ((GeneralPath) shape).lineTo(182.512, 697.301);
        ((GeneralPath) shape).lineTo(182.479, 697.486);
        ((GeneralPath) shape).lineTo(182.447, 697.672);
        ((GeneralPath) shape).lineTo(182.415, 697.861);
        ((GeneralPath) shape).lineTo(182.383, 698.052);
        ((GeneralPath) shape).lineTo(182.352, 698.245);
        ((GeneralPath) shape).lineTo(182.322, 698.439);
        ((GeneralPath) shape).lineTo(182.291, 698.636);
        ((GeneralPath) shape).lineTo(182.262, 698.835);
        ((GeneralPath) shape).lineTo(182.232, 699.036);
        ((GeneralPath) shape).lineTo(182.204, 699.239);
        ((GeneralPath) shape).lineTo(182.175, 699.444);
        ((GeneralPath) shape).lineTo(182.148, 699.651);
        ((GeneralPath) shape).lineTo(182.121, 699.86);
        ((GeneralPath) shape).lineTo(182.094, 700.072);
        ((GeneralPath) shape).lineTo(182.068, 700.285);
        ((GeneralPath) shape).lineTo(182.042, 700.5);
        ((GeneralPath) shape).lineTo(182.017, 700.718);
        ((GeneralPath) shape).lineTo(181.992, 700.938);
        ((GeneralPath) shape).lineTo(181.968, 701.16);
        ((GeneralPath) shape).lineTo(181.945, 701.384);
        ((GeneralPath) shape).lineTo(181.922, 701.61);
        ((GeneralPath) shape).lineTo(181.9, 701.839);
        ((GeneralPath) shape).lineTo(181.878, 702.069);
        ((GeneralPath) shape).lineTo(181.857, 702.302);
        ((GeneralPath) shape).lineTo(181.836, 702.537);
        ((GeneralPath) shape).lineTo(181.816, 702.774);
        ((GeneralPath) shape).lineTo(181.797, 703.014);
        ((GeneralPath) shape).lineTo(181.778, 703.255);
        ((GeneralPath) shape).lineTo(181.76, 703.499);
        ((GeneralPath) shape).lineTo(181.743, 703.745);
        ((GeneralPath) shape).lineTo(181.726, 703.994);
        ((GeneralPath) shape).lineTo(181.71, 704.244);
        ((GeneralPath) shape).lineTo(181.694, 704.497);
        ((GeneralPath) shape).lineTo(181.679, 704.753);
        ((GeneralPath) shape).lineTo(181.665, 705.01);
        ((GeneralPath) shape).lineTo(181.652, 705.27);
        ((GeneralPath) shape).lineTo(181.639, 705.532);
        ((GeneralPath) shape).lineTo(181.627, 705.797);
        ((GeneralPath) shape).lineTo(181.615, 706.064);
        ((GeneralPath) shape).lineTo(181.604, 706.333);
        ((GeneralPath) shape).lineTo(181.594, 706.604);
        ((GeneralPath) shape).lineTo(181.585, 706.878);
        ((GeneralPath) shape).lineTo(181.576, 707.154);
        ((GeneralPath) shape).lineTo(181.568, 707.433);
        ((GeneralPath) shape).lineTo(181.561, 707.714);
        ((GeneralPath) shape).lineTo(181.554, 707.997);
        ((GeneralPath) shape).lineTo(181.548, 708.283);
        ((GeneralPath) shape).lineTo(181.543, 708.571);
        ((GeneralPath) shape).lineTo(181.539, 708.862);
        ((GeneralPath) shape).lineTo(181.536, 709.155);
        ((GeneralPath) shape).lineTo(181.533, 709.451);
        ((GeneralPath) shape).lineTo(181.531, 709.748);
        ((GeneralPath) shape).lineTo(181.529, 710.345);
        ((GeneralPath) shape).lineTo(181.53, 710.934);
        ((GeneralPath) shape).lineTo(181.533, 711.517);
        ((GeneralPath) shape).lineTo(181.539, 712.093);
        ((GeneralPath) shape).lineTo(181.548, 712.663);
        ((GeneralPath) shape).lineTo(181.559, 713.226);
        ((GeneralPath) shape).lineTo(181.572, 713.782);
        ((GeneralPath) shape).lineTo(181.588, 714.332);
        ((GeneralPath) shape).lineTo(181.607, 714.875);
        ((GeneralPath) shape).lineTo(181.627, 715.412);
        ((GeneralPath) shape).lineTo(181.65, 715.942);
        ((GeneralPath) shape).lineTo(181.675, 716.466);
        ((GeneralPath) shape).lineTo(181.703, 716.984);
        ((GeneralPath) shape).lineTo(181.732, 717.495);
        ((GeneralPath) shape).lineTo(181.764, 718.0);
        ((GeneralPath) shape).lineTo(181.798, 718.498);
        ((GeneralPath) shape).lineTo(181.833, 718.99);
        ((GeneralPath) shape).lineTo(181.871, 719.476);
        ((GeneralPath) shape).lineTo(181.911, 719.956);
        ((GeneralPath) shape).lineTo(181.952, 720.43);
        ((GeneralPath) shape).lineTo(181.996, 720.897);
        ((GeneralPath) shape).lineTo(182.041, 721.359);
        ((GeneralPath) shape).lineTo(182.088, 721.814);
        ((GeneralPath) shape).lineTo(182.137, 722.263);
        ((GeneralPath) shape).lineTo(182.187, 722.706);
        ((GeneralPath) shape).lineTo(182.239, 723.143);
        ((GeneralPath) shape).lineTo(182.293, 723.575);
        ((GeneralPath) shape).lineTo(182.348, 724.0);
        ((GeneralPath) shape).lineTo(182.405, 724.419);
        ((GeneralPath) shape).lineTo(182.463, 724.833);
        ((GeneralPath) shape).lineTo(182.523, 725.241);
        ((GeneralPath) shape).lineTo(182.584, 725.643);
        ((GeneralPath) shape).lineTo(182.646, 726.039);
        ((GeneralPath) shape).lineTo(182.709, 726.429);
        ((GeneralPath) shape).lineTo(182.774, 726.814);
        ((GeneralPath) shape).lineTo(182.84, 727.193);
        ((GeneralPath) shape).lineTo(182.908, 727.566);
        ((GeneralPath) shape).lineTo(182.976, 727.934);
        ((GeneralPath) shape).lineTo(183.045, 728.296);
        ((GeneralPath) shape).lineTo(183.116, 728.653);
        ((GeneralPath) shape).lineTo(183.187, 729.004);
        ((GeneralPath) shape).lineTo(183.26, 729.35);
        ((GeneralPath) shape).lineTo(183.333, 729.69);
        ((GeneralPath) shape).lineTo(183.407, 730.025);
        ((GeneralPath) shape).lineTo(183.482, 730.354);
        ((GeneralPath) shape).lineTo(183.558, 730.678);
        ((GeneralPath) shape).lineTo(183.634, 730.997);
        ((GeneralPath) shape).lineTo(183.711, 731.31);
        ((GeneralPath) shape).lineTo(183.789, 731.618);
        ((GeneralPath) shape).lineTo(183.867, 731.921);
        ((GeneralPath) shape).lineTo(183.946, 732.219);
        ((GeneralPath) shape).lineTo(184.026, 732.511);
        ((GeneralPath) shape).lineTo(184.106, 732.799);
        ((GeneralPath) shape).lineTo(184.186, 733.081);
        ((GeneralPath) shape).lineTo(184.267, 733.358);
        ((GeneralPath) shape).lineTo(184.348, 733.63);
        ((GeneralPath) shape).lineTo(184.429, 733.898);
        ((GeneralPath) shape).lineTo(184.511, 734.16);
        ((GeneralPath) shape).lineTo(184.593, 734.417);
        ((GeneralPath) shape).lineTo(184.675, 734.669);
        ((GeneralPath) shape).lineTo(184.757, 734.917);
        ((GeneralPath) shape).lineTo(184.839, 735.16);
        ((GeneralPath) shape).lineTo(184.922, 735.397);
        ((GeneralPath) shape).lineTo(185.004, 735.63);
        ((GeneralPath) shape).lineTo(185.086, 735.859);
        ((GeneralPath) shape).lineTo(185.168, 736.082);
        ((GeneralPath) shape).lineTo(185.25, 736.301);
        ((GeneralPath) shape).lineTo(185.332, 736.515);
        ((GeneralPath) shape).lineTo(185.414, 736.725);
        ((GeneralPath) shape).lineTo(185.495, 736.93);
        ((GeneralPath) shape).lineTo(185.576, 737.13);
        ((GeneralPath) shape).lineTo(185.657, 737.326);
        ((GeneralPath) shape).lineTo(185.738, 737.518);
        ((GeneralPath) shape).lineTo(185.817, 737.705);
        ((GeneralPath) shape).lineTo(185.897, 737.887);
        ((GeneralPath) shape).lineTo(185.976, 738.065);
        ((GeneralPath) shape).lineTo(186.054, 738.239);
        ((GeneralPath) shape).lineTo(186.132, 738.408);
        ((GeneralPath) shape).lineTo(186.209, 738.574);
        ((GeneralPath) shape).lineTo(186.286, 738.735);
        ((GeneralPath) shape).lineTo(186.361, 738.891);
        ((GeneralPath) shape).lineTo(186.436, 739.044);
        ((GeneralPath) shape).lineTo(186.511, 739.192);
        ((GeneralPath) shape).lineTo(186.584, 739.336);
        ((GeneralPath) shape).lineTo(186.656, 739.476);
        ((GeneralPath) shape).lineTo(186.728, 739.612);
        ((GeneralPath) shape).lineTo(186.798, 739.744);
        ((GeneralPath) shape).lineTo(186.868, 739.872);
        ((GeneralPath) shape).lineTo(186.936, 739.996);
        ((GeneralPath) shape).lineTo(187.004, 740.116);
        ((GeneralPath) shape).lineTo(187.07, 740.232);
        ((GeneralPath) shape).lineTo(187.135, 740.344);
        ((GeneralPath) shape).lineTo(187.198, 740.453);
        ((GeneralPath) shape).lineTo(187.261, 740.557);
        ((GeneralPath) shape).lineTo(187.322, 740.658);
        ((GeneralPath) shape).lineTo(187.382, 740.755);
        ((GeneralPath) shape).lineTo(187.44, 740.848);
        ((GeneralPath) shape).lineTo(187.497, 740.938);
        ((GeneralPath) shape).lineTo(187.552, 741.024);
        ((GeneralPath) shape).lineTo(187.605, 741.104);
        ((GeneralPath) shape).lineTo(187.71, 741.26);
        ((GeneralPath) shape).lineTo(187.806, 741.4);
        ((GeneralPath) shape).lineTo(187.896, 741.526);
        ((GeneralPath) shape).lineTo(187.971, 741.629);
        ((GeneralPath) shape).lineTo(328.493, 742.607);
        ((GeneralPath) shape).lineTo(340.888, 840.982);
        ((GeneralPath) shape).lineTo(296.117, 841.293);
        ((GeneralPath) shape).lineTo(296.101, 841.293);
        ((GeneralPath) shape).lineTo(296.083, 841.293);
        ((GeneralPath) shape).lineTo(296.032, 841.295);
        ((GeneralPath) shape).lineTo(296.004, 841.297);
        ((GeneralPath) shape).lineTo(295.963, 841.3);
        ((GeneralPath) shape).lineTo(295.918, 841.304);
        ((GeneralPath) shape).lineTo(295.865, 841.31);
        ((GeneralPath) shape).lineTo(295.807, 841.317);
        ((GeneralPath) shape).lineTo(295.744, 841.326);
        ((GeneralPath) shape).lineTo(295.675, 841.337);
        ((GeneralPath) shape).lineTo(295.602, 841.35);
        ((GeneralPath) shape).lineTo(295.523, 841.366);
        ((GeneralPath) shape).lineTo(295.441, 841.384);
        ((GeneralPath) shape).lineTo(295.355, 841.405);
        ((GeneralPath) shape).lineTo(295.265, 841.429);
        ((GeneralPath) shape).lineTo(295.168, 841.458);
        ((GeneralPath) shape).lineTo(295.124, 841.471);
        ((GeneralPath) shape).lineTo(295.076, 841.487);
        ((GeneralPath) shape).lineTo(295.027, 841.503);
        ((GeneralPath) shape).lineTo(294.977, 841.52);
        ((GeneralPath) shape).lineTo(294.926, 841.539);
        ((GeneralPath) shape).lineTo(294.875, 841.558);
        ((GeneralPath) shape).lineTo(294.823, 841.578);
        ((GeneralPath) shape).lineTo(294.771, 841.599);
        ((GeneralPath) shape).lineTo(294.719, 841.622);
        ((GeneralPath) shape).lineTo(294.666, 841.645);
        ((GeneralPath) shape).lineTo(294.612, 841.669);
        ((GeneralPath) shape).lineTo(294.558, 841.694);
        ((GeneralPath) shape).lineTo(294.504, 841.72);
        ((GeneralPath) shape).lineTo(294.449, 841.748);
        ((GeneralPath) shape).lineTo(294.393, 841.777);
        ((GeneralPath) shape).lineTo(294.338, 841.807);
        ((GeneralPath) shape).lineTo(294.282, 841.837);
        ((GeneralPath) shape).lineTo(294.226, 841.869);
        ((GeneralPath) shape).lineTo(294.17, 841.903);
        ((GeneralPath) shape).lineTo(294.114, 841.938);
        ((GeneralPath) shape).lineTo(294.057, 841.974);
        ((GeneralPath) shape).lineTo(294.0, 842.011);
        ((GeneralPath) shape).lineTo(293.943, 842.05);
        ((GeneralPath) shape).lineTo(293.886, 842.09);
        ((GeneralPath) shape).lineTo(293.829, 842.131);
        ((GeneralPath) shape).lineTo(293.772, 842.174);
        ((GeneralPath) shape).lineTo(293.714, 842.218);
        ((GeneralPath) shape).lineTo(293.657, 842.264);
        ((GeneralPath) shape).lineTo(293.599, 842.312);
        ((GeneralPath) shape).lineTo(293.542, 842.361);
        ((GeneralPath) shape).lineTo(293.485, 842.411);
        ((GeneralPath) shape).lineTo(293.427, 842.464);
        ((GeneralPath) shape).lineTo(293.37, 842.517);
        ((GeneralPath) shape).lineTo(293.313, 842.573);
        ((GeneralPath) shape).lineTo(293.256, 842.631);
        ((GeneralPath) shape).lineTo(293.198, 842.69);
        ((GeneralPath) shape).lineTo(293.142, 842.751);
        ((GeneralPath) shape).lineTo(293.085, 842.814);
        ((GeneralPath) shape).lineTo(293.028, 842.879);
        ((GeneralPath) shape).lineTo(292.972, 842.946);
        ((GeneralPath) shape).lineTo(292.916, 843.015);
        ((GeneralPath) shape).lineTo(292.86, 843.086);
        ((GeneralPath) shape).lineTo(292.804, 843.159);
        ((GeneralPath) shape).lineTo(292.749, 843.235);
        ((GeneralPath) shape).lineTo(292.694, 843.312);
        ((GeneralPath) shape).lineTo(292.639, 843.392);
        ((GeneralPath) shape).lineTo(292.585, 843.474);
        ((GeneralPath) shape).lineTo(292.531, 843.559);
        ((GeneralPath) shape).lineTo(292.478, 843.646);
        ((GeneralPath) shape).lineTo(292.425, 843.735);
        ((GeneralPath) shape).lineTo(292.372, 843.827);
        ((GeneralPath) shape).lineTo(292.321, 843.922);
        ((GeneralPath) shape).lineTo(292.269, 844.019);
        ((GeneralPath) shape).lineTo(292.219, 844.119);
        ((GeneralPath) shape).lineTo(292.169, 844.222);
        ((GeneralPath) shape).lineTo(292.119, 844.327);
        ((GeneralPath) shape).lineTo(292.071, 844.435);
        ((GeneralPath) shape).lineTo(292.023, 844.546);
        ((GeneralPath) shape).lineTo(291.976, 844.66);
        ((GeneralPath) shape).lineTo(291.93, 844.777);
        ((GeneralPath) shape).lineTo(291.884, 844.897);
        ((GeneralPath) shape).lineTo(291.84, 845.02);
        ((GeneralPath) shape).lineTo(291.797, 845.146);
        ((GeneralPath) shape).lineTo(291.755, 845.275);
        ((GeneralPath) shape).lineTo(291.713, 845.407);
        ((GeneralPath) shape).lineTo(291.673, 845.543);
        ((GeneralPath) shape).lineTo(291.634, 845.681);
        ((GeneralPath) shape).lineTo(291.596, 845.824);
        ((GeneralPath) shape).lineTo(291.56, 845.969);
        ((GeneralPath) shape).lineTo(291.525, 846.118);
        ((GeneralPath) shape).lineTo(291.491, 846.27);
        ((GeneralPath) shape).lineTo(291.458, 846.426);
        ((GeneralPath) shape).lineTo(291.427, 846.585);
        ((GeneralPath) shape).lineTo(291.398, 846.748);
        ((GeneralPath) shape).lineTo(291.37, 846.914);
        ((GeneralPath) shape).lineTo(291.343, 847.084);
        ((GeneralPath) shape).lineTo(291.319, 847.257);
        ((GeneralPath) shape).lineTo(291.296, 847.435);
        ((GeneralPath) shape).lineTo(291.274, 847.616);
        ((GeneralPath) shape).lineTo(291.255, 847.8);
        ((GeneralPath) shape).lineTo(291.237, 847.989);
        ((GeneralPath) shape).lineTo(291.221, 848.181);
        ((GeneralPath) shape).lineTo(291.207, 848.377);
        ((GeneralPath) shape).lineTo(291.195, 848.577);
        ((GeneralPath) shape).lineTo(291.185, 848.781);
        ((GeneralPath) shape).lineTo(291.177, 848.989);
        ((GeneralPath) shape).lineTo(291.171, 849.2);
        ((GeneralPath) shape).lineTo(291.167, 849.416);
        ((GeneralPath) shape).lineTo(291.165, 849.636);
        ((GeneralPath) shape).lineTo(291.166, 849.859);
        ((GeneralPath) shape).lineTo(291.168, 850.034);
        ((GeneralPath) shape).lineTo(291.172, 850.209);
        ((GeneralPath) shape).lineTo(291.179, 850.385);
        ((GeneralPath) shape).lineTo(291.187, 850.562);
        ((GeneralPath) shape).lineTo(291.197, 850.739);
        ((GeneralPath) shape).lineTo(291.208, 850.917);
        ((GeneralPath) shape).lineTo(291.222, 851.096);
        ((GeneralPath) shape).lineTo(291.237, 851.275);
        ((GeneralPath) shape).lineTo(291.255, 851.455);
        ((GeneralPath) shape).lineTo(291.274, 851.636);
        ((GeneralPath) shape).lineTo(291.294, 851.816);
        ((GeneralPath) shape).lineTo(291.317, 851.998);
        ((GeneralPath) shape).lineTo(291.341, 852.18);
        ((GeneralPath) shape).lineTo(291.366, 852.362);
        ((GeneralPath) shape).lineTo(291.394, 852.544);
        ((GeneralPath) shape).lineTo(291.422, 852.727);
        ((GeneralPath) shape).lineTo(291.453, 852.91);
        ((GeneralPath) shape).lineTo(291.485, 853.093);
        ((GeneralPath) shape).lineTo(291.518, 853.277);
        ((GeneralPath) shape).lineTo(291.553, 853.46);
        ((GeneralPath) shape).lineTo(291.59, 853.644);
        ((GeneralPath) shape).lineTo(291.627, 853.828);
        ((GeneralPath) shape).lineTo(291.667, 854.011);
        ((GeneralPath) shape).lineTo(291.707, 854.195);
        ((GeneralPath) shape).lineTo(291.749, 854.379);
        ((GeneralPath) shape).lineTo(291.792, 854.563);
        ((GeneralPath) shape).lineTo(291.836, 854.746);
        ((GeneralPath) shape).lineTo(291.882, 854.93);
        ((GeneralPath) shape).lineTo(291.929, 855.113);
        ((GeneralPath) shape).lineTo(291.977, 855.296);
        ((GeneralPath) shape).lineTo(292.026, 855.479);
        ((GeneralPath) shape).lineTo(292.077, 855.662);
        ((GeneralPath) shape).lineTo(292.128, 855.844);
        ((GeneralPath) shape).lineTo(292.18, 856.026);
        ((GeneralPath) shape).lineTo(292.234, 856.207);
        ((GeneralPath) shape).lineTo(292.288, 856.388);
        ((GeneralPath) shape).lineTo(292.344, 856.569);
        ((GeneralPath) shape).lineTo(292.4, 856.749);
        ((GeneralPath) shape).lineTo(292.457, 856.928);
        ((GeneralPath) shape).lineTo(292.515, 857.107);
        ((GeneralPath) shape).lineTo(292.574, 857.286);
        ((GeneralPath) shape).lineTo(292.634, 857.463);
        ((GeneralPath) shape).lineTo(292.694, 857.64);
        ((GeneralPath) shape).lineTo(292.755, 857.817);
        ((GeneralPath) shape).lineTo(292.817, 857.992);
        ((GeneralPath) shape).lineTo(292.88, 858.167);
        ((GeneralPath) shape).lineTo(292.943, 858.341);
        ((GeneralPath) shape).lineTo(293.006, 858.514);
        ((GeneralPath) shape).lineTo(293.071, 858.687);
        ((GeneralPath) shape).lineTo(293.136, 858.858);
        ((GeneralPath) shape).lineTo(293.201, 859.028);
        ((GeneralPath) shape).lineTo(293.267, 859.198);
        ((GeneralPath) shape).lineTo(293.333, 859.366);
        ((GeneralPath) shape).lineTo(293.4, 859.534);
        ((GeneralPath) shape).lineTo(293.467, 859.7);
        ((GeneralPath) shape).lineTo(293.534, 859.864);
        ((GeneralPath) shape).lineTo(293.67, 860.192);
        ((GeneralPath) shape).lineTo(293.806, 860.514);
        ((GeneralPath) shape).lineTo(293.944, 860.831);
        ((GeneralPath) shape).lineTo(294.081, 861.142);
        ((GeneralPath) shape).lineTo(294.219, 861.448);
        ((GeneralPath) shape).lineTo(294.357, 861.747);
        ((GeneralPath) shape).lineTo(294.494, 862.041);
        ((GeneralPath) shape).lineTo(294.631, 862.328);
        ((GeneralPath) shape).lineTo(294.767, 862.608);
        ((GeneralPath) shape).lineTo(294.902, 862.881);
        ((GeneralPath) shape).lineTo(295.035, 863.147);
        ((GeneralPath) shape).lineTo(295.166, 863.406);
        ((GeneralPath) shape).lineTo(295.296, 863.656);
        ((GeneralPath) shape).lineTo(295.423, 863.899);
        ((GeneralPath) shape).lineTo(295.547, 864.133);
        ((GeneralPath) shape).lineTo(295.669, 864.358);
        ((GeneralPath) shape).lineTo(295.788, 864.575);
        ((GeneralPath) shape).lineTo(295.903, 864.782);
        ((GeneralPath) shape).lineTo(296.015, 864.98);
        ((GeneralPath) shape).lineTo(296.123, 865.168);
        ((GeneralPath) shape).lineTo(296.226, 865.345);
        ((GeneralPath) shape).lineTo(296.325, 865.513);
        ((GeneralPath) shape).lineTo(296.419, 865.669);
        ((GeneralPath) shape).lineTo(296.507, 865.815);
        ((GeneralPath) shape).lineTo(296.59, 865.949);
        ((GeneralPath) shape).lineTo(296.65, 866.042);
        ((GeneralPath) shape).lineTo(303.175, 866.12);
        ((GeneralPath) shape).lineTo(310.682, 866.208);
        ((GeneralPath) shape).lineTo(319.362, 866.309);
        ((GeneralPath) shape).lineTo(328.158, 866.41);
        ((GeneralPath) shape).lineTo(332.269, 866.457);
        ((GeneralPath) shape).lineTo(336.013, 866.498);
        ((GeneralPath) shape).lineTo(339.259, 866.534);
        ((GeneralPath) shape).lineTo(341.872, 866.561);
        ((GeneralPath) shape).lineTo(343.722, 866.578);
        ((GeneralPath) shape).lineTo(344.317, 866.583);
        ((GeneralPath) shape).lineTo(346.045, 866.591);
        ((GeneralPath) shape).lineTo(346.164, 867.996);
        ((GeneralPath) shape).lineTo(346.166, 868.013);
        ((GeneralPath) shape).lineTo(346.169, 868.035);
        ((GeneralPath) shape).lineTo(346.173, 868.06);
        ((GeneralPath) shape).lineTo(346.179, 868.089);
        ((GeneralPath) shape).lineTo(346.187, 868.122);
        ((GeneralPath) shape).lineTo(346.196, 868.157);
        ((GeneralPath) shape).lineTo(346.206, 868.194);
        ((GeneralPath) shape).lineTo(346.218, 868.234);
        ((GeneralPath) shape).lineTo(346.231, 868.277);
        ((GeneralPath) shape).lineTo(346.246, 868.321);
        ((GeneralPath) shape).lineTo(346.263, 868.367);
        ((GeneralPath) shape).lineTo(346.281, 868.415);
        ((GeneralPath) shape).lineTo(346.3, 868.464);
        ((GeneralPath) shape).lineTo(346.321, 868.515);
        ((GeneralPath) shape).lineTo(346.343, 868.567);
        ((GeneralPath) shape).lineTo(346.367, 868.619);
        ((GeneralPath) shape).lineTo(346.391, 868.672);
        ((GeneralPath) shape).lineTo(346.417, 868.727);
        ((GeneralPath) shape).lineTo(346.444, 868.782);
        ((GeneralPath) shape).lineTo(346.472, 868.836);
        ((GeneralPath) shape).lineTo(346.5, 868.892);
        ((GeneralPath) shape).lineTo(346.53, 868.948);
        ((GeneralPath) shape).lineTo(346.561, 869.004);
        ((GeneralPath) shape).lineTo(346.592, 869.059);
        ((GeneralPath) shape).lineTo(346.624, 869.115);
        ((GeneralPath) shape).lineTo(346.657, 869.171);
        ((GeneralPath) shape).lineTo(346.674, 869.2);
        ((GeneralPath) shape).lineTo(346.691, 869.171);
        ((GeneralPath) shape).lineTo(346.724, 869.115);
        ((GeneralPath) shape).lineTo(346.756, 869.06);
        ((GeneralPath) shape).lineTo(346.787, 869.004);
        ((GeneralPath) shape).lineTo(346.818, 868.948);
        ((GeneralPath) shape).lineTo(346.848, 868.892);
        ((GeneralPath) shape).lineTo(346.876, 868.836);
        ((GeneralPath) shape).lineTo(346.904, 868.781);
        ((GeneralPath) shape).lineTo(346.931, 868.727);
        ((GeneralPath) shape).lineTo(346.957, 868.672);
        ((GeneralPath) shape).lineTo(346.982, 868.619);
        ((GeneralPath) shape).lineTo(347.005, 868.566);
        ((GeneralPath) shape).lineTo(347.027, 868.515);
        ((GeneralPath) shape).lineTo(347.048, 868.464);
        ((GeneralPath) shape).lineTo(347.067, 868.415);
        ((GeneralPath) shape).lineTo(347.085, 868.367);
        ((GeneralPath) shape).lineTo(347.102, 868.321);
        ((GeneralPath) shape).lineTo(347.117, 868.277);
        ((GeneralPath) shape).lineTo(347.13, 868.234);
        ((GeneralPath) shape).lineTo(347.142, 868.194);
        ((GeneralPath) shape).lineTo(347.153, 868.156);
        ((GeneralPath) shape).lineTo(347.161, 868.122);
        ((GeneralPath) shape).lineTo(347.169, 868.09);
        ((GeneralPath) shape).lineTo(347.175, 868.061);
        ((GeneralPath) shape).lineTo(347.179, 868.035);
        ((GeneralPath) shape).lineTo(347.182, 868.013);
        ((GeneralPath) shape).lineTo(347.184, 867.995);
        ((GeneralPath) shape).lineTo(347.304, 866.591);
        ((GeneralPath) shape).lineTo(349.031, 866.583);
        ((GeneralPath) shape).lineTo(349.626, 866.578);
        ((GeneralPath) shape).lineTo(351.476, 866.561);
        ((GeneralPath) shape).lineTo(354.09, 866.534);
        ((GeneralPath) shape).lineTo(357.335, 866.498);
        ((GeneralPath) shape).lineTo(361.079, 866.457);
        ((GeneralPath) shape).lineTo(365.19, 866.41);
        ((GeneralPath) shape).lineTo(373.986, 866.309);
        ((GeneralPath) shape).lineTo(382.666, 866.208);
        ((GeneralPath) shape).lineTo(390.173, 866.12);
        ((GeneralPath) shape).lineTo(396.698, 866.042);
        ((GeneralPath) shape).lineTo(396.758, 865.948);
        ((GeneralPath) shape).lineTo(396.841, 865.815);
        ((GeneralPath) shape).lineTo(396.929, 865.669);
        ((GeneralPath) shape).lineTo(397.023, 865.512);
        ((GeneralPath) shape).lineTo(397.122, 865.345);
        ((GeneralPath) shape).lineTo(397.226, 865.167);
        ((GeneralPath) shape).lineTo(397.333, 864.979);
        ((GeneralPath) shape).lineTo(397.445, 864.782);
        ((GeneralPath) shape).lineTo(397.56, 864.574);
        ((GeneralPath) shape).lineTo(397.679, 864.358);
        ((GeneralPath) shape).lineTo(397.801, 864.133);
        ((GeneralPath) shape).lineTo(397.925, 863.898);
        ((GeneralPath) shape).lineTo(398.053, 863.656);
        ((GeneralPath) shape).lineTo(398.182, 863.405);
        ((GeneralPath) shape).lineTo(398.314, 863.147);
        ((GeneralPath) shape).lineTo(398.447, 862.881);
        ((GeneralPath) shape).lineTo(398.581, 862.608);
        ((GeneralPath) shape).lineTo(398.717, 862.328);
        ((GeneralPath) shape).lineTo(398.854, 862.041);
        ((GeneralPath) shape).lineTo(398.991, 861.747);
        ((GeneralPath) shape).lineTo(399.129, 861.447);
        ((GeneralPath) shape).lineTo(399.267, 861.142);
        ((GeneralPath) shape).lineTo(399.405, 860.83);
        ((GeneralPath) shape).lineTo(399.542, 860.514);
        ((GeneralPath) shape).lineTo(399.678, 860.192);
        ((GeneralPath) shape).lineTo(399.815, 859.864);
        ((GeneralPath) shape).lineTo(399.881, 859.7);
        ((GeneralPath) shape).lineTo(399.948, 859.533);
        ((GeneralPath) shape).lineTo(400.015, 859.366);
        ((GeneralPath) shape).lineTo(400.081, 859.198);
        ((GeneralPath) shape).lineTo(400.147, 859.028);
        ((GeneralPath) shape).lineTo(400.213, 858.858);
        ((GeneralPath) shape).lineTo(400.277, 858.686);
        ((GeneralPath) shape).lineTo(400.342, 858.514);
        ((GeneralPath) shape).lineTo(400.405, 858.341);
        ((GeneralPath) shape).lineTo(400.469, 858.167);
        ((GeneralPath) shape).lineTo(400.531, 857.992);
        ((GeneralPath) shape).lineTo(400.593, 857.817);
        ((GeneralPath) shape).lineTo(400.654, 857.64);
        ((GeneralPath) shape).lineTo(400.715, 857.463);
        ((GeneralPath) shape).lineTo(400.774, 857.286);
        ((GeneralPath) shape).lineTo(400.833, 857.107);
        ((GeneralPath) shape).lineTo(400.891, 856.928);
        ((GeneralPath) shape).lineTo(400.948, 856.749);
        ((GeneralPath) shape).lineTo(401.005, 856.569);
        ((GeneralPath) shape).lineTo(401.06, 856.388);
        ((GeneralPath) shape).lineTo(401.114, 856.207);
        ((GeneralPath) shape).lineTo(401.168, 856.026);
        ((GeneralPath) shape).lineTo(401.22, 855.844);
        ((GeneralPath) shape).lineTo(401.272, 855.661);
        ((GeneralPath) shape).lineTo(401.322, 855.479);
        ((GeneralPath) shape).lineTo(401.371, 855.296);
        ((GeneralPath) shape).lineTo(401.419, 855.113);
        ((GeneralPath) shape).lineTo(401.466, 854.93);
        ((GeneralPath) shape).lineTo(401.512, 854.746);
        ((GeneralPath) shape).lineTo(401.556, 854.563);
        ((GeneralPath) shape).lineTo(401.599, 854.379);
        ((GeneralPath) shape).lineTo(401.641, 854.195);
        ((GeneralPath) shape).lineTo(401.682, 854.011);
        ((GeneralPath) shape).lineTo(401.721, 853.828);
        ((GeneralPath) shape).lineTo(401.759, 853.644);
        ((GeneralPath) shape).lineTo(401.795, 853.46);
        ((GeneralPath) shape).lineTo(401.83, 853.276);
        ((GeneralPath) shape).lineTo(401.863, 853.093);
        ((GeneralPath) shape).lineTo(401.895, 852.91);
        ((GeneralPath) shape).lineTo(401.926, 852.727);
        ((GeneralPath) shape).lineTo(401.955, 852.544);
        ((GeneralPath) shape).lineTo(401.982, 852.362);
        ((GeneralPath) shape).lineTo(402.008, 852.179);
        ((GeneralPath) shape).lineTo(402.032, 851.998);
        ((GeneralPath) shape).lineTo(402.054, 851.816);
        ((GeneralPath) shape).lineTo(402.075, 851.636);
        ((GeneralPath) shape).lineTo(402.094, 851.455);
        ((GeneralPath) shape).lineTo(402.111, 851.275);
        ((GeneralPath) shape).lineTo(402.126, 851.096);
        ((GeneralPath) shape).lineTo(402.14, 850.917);
        ((GeneralPath) shape).lineTo(402.152, 850.739);
        ((GeneralPath) shape).lineTo(402.161, 850.562);
        ((GeneralPath) shape).lineTo(402.17, 850.385);
        ((GeneralPath) shape).lineTo(402.176, 850.209);
        ((GeneralPath) shape).lineTo(402.18, 850.034);
        ((GeneralPath) shape).lineTo(402.182, 849.859);
        ((GeneralPath) shape).lineTo(402.183, 849.636);
        ((GeneralPath) shape).lineTo(402.181, 849.416);
        ((GeneralPath) shape).lineTo(402.178, 849.2);
        ((GeneralPath) shape).lineTo(402.172, 848.989);
        ((GeneralPath) shape).lineTo(402.164, 848.781);
        ((GeneralPath) shape).lineTo(402.154, 848.577);
        ((GeneralPath) shape).lineTo(402.141, 848.377);
        ((GeneralPath) shape).lineTo(402.127, 848.181);
        ((GeneralPath) shape).lineTo(402.111, 847.989);
        ((GeneralPath) shape).lineTo(402.094, 847.8);
        ((GeneralPath) shape).lineTo(402.074, 847.616);
        ((GeneralPath) shape).lineTo(402.053, 847.435);
        ((GeneralPath) shape).lineTo(402.03, 847.258);
        ((GeneralPath) shape).lineTo(402.005, 847.084);
        ((GeneralPath) shape).lineTo(401.978, 846.914);
        ((GeneralPath) shape).lineTo(401.95, 846.748);
        ((GeneralPath) shape).lineTo(401.921, 846.585);
        ((GeneralPath) shape).lineTo(401.89, 846.426);
        ((GeneralPath) shape).lineTo(401.857, 846.27);
        ((GeneralPath) shape).lineTo(401.823, 846.118);
        ((GeneralPath) shape).lineTo(401.788, 845.969);
        ((GeneralPath) shape).lineTo(401.752, 845.824);
        ((GeneralPath) shape).lineTo(401.714, 845.682);
        ((GeneralPath) shape).lineTo(401.675, 845.543);
        ((GeneralPath) shape).lineTo(401.635, 845.407);
        ((GeneralPath) shape).lineTo(401.594, 845.275);
        ((GeneralPath) shape).lineTo(401.551, 845.146);
        ((GeneralPath) shape).lineTo(401.508, 845.02);
        ((GeneralPath) shape).lineTo(401.464, 844.897);
        ((GeneralPath) shape).lineTo(401.418, 844.777);
        ((GeneralPath) shape).lineTo(401.372, 844.66);
        ((GeneralPath) shape).lineTo(401.325, 844.546);
        ((GeneralPath) shape).lineTo(401.277, 844.435);
        ((GeneralPath) shape).lineTo(401.229, 844.327);
        ((GeneralPath) shape).lineTo(401.18, 844.222);
        ((GeneralPath) shape).lineTo(401.13, 844.119);
        ((GeneralPath) shape).lineTo(401.079, 844.019);
        ((GeneralPath) shape).lineTo(401.028, 843.922);
        ((GeneralPath) shape).lineTo(400.976, 843.827);
        ((GeneralPath) shape).lineTo(400.923, 843.735);
        ((GeneralPath) shape).lineTo(400.87, 843.646);
        ((GeneralPath) shape).lineTo(400.817, 843.559);
        ((GeneralPath) shape).lineTo(400.763, 843.475);
        ((GeneralPath) shape).lineTo(400.709, 843.392);
        ((GeneralPath) shape).lineTo(400.654, 843.312);
        ((GeneralPath) shape).lineTo(400.599, 843.235);
        ((GeneralPath) shape).lineTo(400.544, 843.159);
        ((GeneralPath) shape).lineTo(400.488, 843.086);
        ((GeneralPath) shape).lineTo(400.432, 843.015);
        ((GeneralPath) shape).lineTo(400.376, 842.946);
        ((GeneralPath) shape).lineTo(400.32, 842.879);
        ((GeneralPath) shape).lineTo(400.263, 842.814);
        ((GeneralPath) shape).lineTo(400.207, 842.751);
        ((GeneralPath) shape).lineTo(400.15, 842.69);
        ((GeneralPath) shape).lineTo(400.093, 842.631);
        ((GeneralPath) shape).lineTo(400.036, 842.573);
        ((GeneralPath) shape).lineTo(399.978, 842.517);
        ((GeneralPath) shape).lineTo(399.921, 842.464);
        ((GeneralPath) shape).lineTo(399.863, 842.411);
        ((GeneralPath) shape).lineTo(399.806, 842.361);
        ((GeneralPath) shape).lineTo(399.749, 842.312);
        ((GeneralPath) shape).lineTo(399.691, 842.264);
        ((GeneralPath) shape).lineTo(399.634, 842.218);
        ((GeneralPath) shape).lineTo(399.576, 842.174);
        ((GeneralPath) shape).lineTo(399.519, 842.131);
        ((GeneralPath) shape).lineTo(399.462, 842.09);
        ((GeneralPath) shape).lineTo(399.405, 842.05);
        ((GeneralPath) shape).lineTo(399.348, 842.011);
        ((GeneralPath) shape).lineTo(399.291, 841.974);
        ((GeneralPath) shape).lineTo(399.234, 841.938);
        ((GeneralPath) shape).lineTo(399.178, 841.903);
        ((GeneralPath) shape).lineTo(399.122, 841.869);
        ((GeneralPath) shape).lineTo(399.066, 841.837);
        ((GeneralPath) shape).lineTo(399.011, 841.807);
        ((GeneralPath) shape).lineTo(398.955, 841.777);
        ((GeneralPath) shape).lineTo(398.899, 841.748);
        ((GeneralPath) shape).lineTo(398.845, 841.72);
        ((GeneralPath) shape).lineTo(398.791, 841.694);
        ((GeneralPath) shape).lineTo(398.736, 841.669);
        ((GeneralPath) shape).lineTo(398.683, 841.645);
        ((GeneralPath) shape).lineTo(398.63, 841.622);
        ((GeneralPath) shape).lineTo(398.577, 841.599);
        ((GeneralPath) shape).lineTo(398.525, 841.578);
        ((GeneralPath) shape).lineTo(398.473, 841.558);
        ((GeneralPath) shape).lineTo(398.422, 841.539);
        ((GeneralPath) shape).lineTo(398.371, 841.52);
        ((GeneralPath) shape).lineTo(398.322, 841.503);
        ((GeneralPath) shape).lineTo(398.273, 841.487);
        ((GeneralPath) shape).lineTo(398.224, 841.471);
        ((GeneralPath) shape).lineTo(398.18, 841.458);
        ((GeneralPath) shape).lineTo(398.083, 841.429);
        ((GeneralPath) shape).lineTo(397.993, 841.405);
        ((GeneralPath) shape).lineTo(397.907, 841.384);
        ((GeneralPath) shape).lineTo(397.825, 841.366);
        ((GeneralPath) shape).lineTo(397.747, 841.35);
        ((GeneralPath) shape).lineTo(397.673, 841.337);
        ((GeneralPath) shape).lineTo(397.605, 841.326);
        ((GeneralPath) shape).lineTo(397.541, 841.317);
        ((GeneralPath) shape).lineTo(397.483, 841.31);
        ((GeneralPath) shape).lineTo(397.43, 841.304);
        ((GeneralPath) shape).lineTo(397.385, 841.3);
        ((GeneralPath) shape).lineTo(397.345, 841.297);
        ((GeneralPath) shape).lineTo(397.316, 841.295);
        ((GeneralPath) shape).lineTo(397.265, 841.293);
        ((GeneralPath) shape).lineTo(397.247, 841.293);
        ((GeneralPath) shape).lineTo(397.231, 841.293);
        ((GeneralPath) shape).lineTo(352.46, 840.982);
        ((GeneralPath) shape).lineTo(364.855, 742.607);
        ((GeneralPath) shape).lineTo(505.378, 741.629);
        ((GeneralPath) shape).lineTo(505.453, 741.526);
        ((GeneralPath) shape).lineTo(505.542, 741.4);
        ((GeneralPath) shape).lineTo(505.639, 741.26);
        ((GeneralPath) shape).lineTo(505.743, 741.104);
        ((GeneralPath) shape).lineTo(505.796, 741.024);
        ((GeneralPath) shape).lineTo(505.851, 740.938);
        ((GeneralPath) shape).lineTo(505.908, 740.848);
        ((GeneralPath) shape).lineTo(505.966, 740.755);
        ((GeneralPath) shape).lineTo(506.026, 740.658);
        ((GeneralPath) shape).lineTo(506.087, 740.557);
        ((GeneralPath) shape).lineTo(506.15, 740.453);
        ((GeneralPath) shape).lineTo(506.214, 740.344);
        ((GeneralPath) shape).lineTo(506.279, 740.232);
        ((GeneralPath) shape).lineTo(506.345, 740.116);
        ((GeneralPath) shape).lineTo(506.412, 739.996);
        ((GeneralPath) shape).lineTo(506.481, 739.872);
        ((GeneralPath) shape).lineTo(506.55, 739.744);
        ((GeneralPath) shape).lineTo(506.62, 739.612);
        ((GeneralPath) shape).lineTo(506.692, 739.476);
        ((GeneralPath) shape).lineTo(506.764, 739.336);
        ((GeneralPath) shape).lineTo(506.838, 739.192);
        ((GeneralPath) shape).lineTo(506.912, 739.044);
        ((GeneralPath) shape).lineTo(506.987, 738.891);
        ((GeneralPath) shape).lineTo(507.062, 738.735);
        ((GeneralPath) shape).lineTo(507.139, 738.574);
        ((GeneralPath) shape).lineTo(507.216, 738.408);
        ((GeneralPath) shape).lineTo(507.294, 738.239);
        ((GeneralPath) shape).lineTo(507.372, 738.065);
        ((GeneralPath) shape).lineTo(507.451, 737.887);
        ((GeneralPath) shape).lineTo(507.531, 737.705);
        ((GeneralPath) shape).lineTo(507.611, 737.518);
        ((GeneralPath) shape).lineTo(507.691, 737.326);
        ((GeneralPath) shape).lineTo(507.772, 737.13);
        ((GeneralPath) shape).lineTo(507.853, 736.93);
        ((GeneralPath) shape).lineTo(507.934, 736.725);
        ((GeneralPath) shape).lineTo(508.016, 736.515);
        ((GeneralPath) shape).lineTo(508.098, 736.301);
        ((GeneralPath) shape).lineTo(508.18, 736.082);
        ((GeneralPath) shape).lineTo(508.262, 735.859);
        ((GeneralPath) shape).lineTo(508.344, 735.63);
        ((GeneralPath) shape).lineTo(508.427, 735.397);
        ((GeneralPath) shape).lineTo(508.509, 735.16);
        ((GeneralPath) shape).lineTo(508.591, 734.917);
        ((GeneralPath) shape).lineTo(508.673, 734.669);
        ((GeneralPath) shape).lineTo(508.755, 734.417);
        ((GeneralPath) shape).lineTo(508.837, 734.16);
        ((GeneralPath) shape).lineTo(508.919, 733.898);
        ((GeneralPath) shape).lineTo(509.0, 733.63);
        ((GeneralPath) shape).lineTo(509.081, 733.358);
        ((GeneralPath) shape).lineTo(509.162, 733.081);
        ((GeneralPath) shape).lineTo(509.242, 732.799);
        ((GeneralPath) shape).lineTo(509.322, 732.511);
        ((GeneralPath) shape).lineTo(509.402, 732.219);
        ((GeneralPath) shape).lineTo(509.481, 731.921);
        ((GeneralPath) shape).lineTo(509.559, 731.618);
        ((GeneralPath) shape).lineTo(509.637, 731.31);
        ((GeneralPath) shape).lineTo(509.714, 730.997);
        ((GeneralPath) shape).lineTo(509.791, 730.678);
        ((GeneralPath) shape).lineTo(509.866, 730.354);
        ((GeneralPath) shape).lineTo(509.941, 730.025);
        ((GeneralPath) shape).lineTo(510.015, 729.69);
        ((GeneralPath) shape).lineTo(510.089, 729.35);
        ((GeneralPath) shape).lineTo(510.161, 729.004);
        ((GeneralPath) shape).lineTo(510.232, 728.653);
        ((GeneralPath) shape).lineTo(510.303, 728.296);
        ((GeneralPath) shape).lineTo(510.372, 727.934);
        ((GeneralPath) shape).lineTo(510.441, 727.566);
        ((GeneralPath) shape).lineTo(510.508, 727.193);
        ((GeneralPath) shape).lineTo(510.574, 726.814);
        ((GeneralPath) shape).lineTo(510.639, 726.429);
        ((GeneralPath) shape).lineTo(510.702, 726.039);
        ((GeneralPath) shape).lineTo(510.765, 725.643);
        ((GeneralPath) shape).lineTo(510.826, 725.241);
        ((GeneralPath) shape).lineTo(510.885, 724.833);
        ((GeneralPath) shape).lineTo(510.943, 724.419);
        ((GeneralPath) shape).lineTo(511.0, 724.0);
        ((GeneralPath) shape).lineTo(511.055, 723.575);
        ((GeneralPath) shape).lineTo(511.109, 723.143);
        ((GeneralPath) shape).lineTo(511.161, 722.706);
        ((GeneralPath) shape).lineTo(511.211, 722.263);
        ((GeneralPath) shape).lineTo(511.26, 721.814);
        ((GeneralPath) shape).lineTo(511.307, 721.359);
        ((GeneralPath) shape).lineTo(511.352, 720.897);
        ((GeneralPath) shape).lineTo(511.396, 720.43);
        ((GeneralPath) shape).lineTo(511.437, 719.956);
        ((GeneralPath) shape).lineTo(511.477, 719.476);
        ((GeneralPath) shape).lineTo(511.515, 718.99);
        ((GeneralPath) shape).lineTo(511.551, 718.498);
        ((GeneralPath) shape).lineTo(511.584, 718.0);
        ((GeneralPath) shape).lineTo(511.616, 717.495);
        ((GeneralPath) shape).lineTo(511.645, 716.984);
        ((GeneralPath) shape).lineTo(511.673, 716.466);
        ((GeneralPath) shape).lineTo(511.698, 715.942);
        ((GeneralPath) shape).lineTo(511.721, 715.412);
        ((GeneralPath) shape).lineTo(511.742, 714.875);
        ((GeneralPath) shape).lineTo(511.76, 714.332);
        ((GeneralPath) shape).lineTo(511.776, 713.782);
        ((GeneralPath) shape).lineTo(511.789, 713.226);
        ((GeneralPath) shape).lineTo(511.8, 712.663);
        ((GeneralPath) shape).lineTo(511.809, 712.093);
        ((GeneralPath) shape).lineTo(511.815, 711.517);
        ((GeneralPath) shape).lineTo(511.818, 710.934);
        ((GeneralPath) shape).lineTo(511.819, 710.345);
        ((GeneralPath) shape).lineTo(511.817, 709.748);
        ((GeneralPath) shape).lineTo(511.815, 709.451);
        ((GeneralPath) shape).lineTo(511.813, 709.155);
        ((GeneralPath) shape).lineTo(511.809, 708.862);
        ((GeneralPath) shape).lineTo(511.805, 708.571);
        ((GeneralPath) shape).lineTo(511.8, 708.283);
        ((GeneralPath) shape).lineTo(511.794, 707.997);
        ((GeneralPath) shape).lineTo(511.787, 707.714);
        ((GeneralPath) shape).lineTo(511.78, 707.433);
        ((GeneralPath) shape).lineTo(511.772, 707.154);
        ((GeneralPath) shape).lineTo(511.764, 706.878);
        ((GeneralPath) shape).lineTo(511.754, 706.604);
        ((GeneralPath) shape).lineTo(511.744, 706.333);
        ((GeneralPath) shape).lineTo(511.733, 706.063);
        ((GeneralPath) shape).lineTo(511.722, 705.797);
        ((GeneralPath) shape).lineTo(511.709, 705.532);
        ((GeneralPath) shape).lineTo(511.697, 705.27);
        ((GeneralPath) shape).lineTo(511.683, 705.01);
        ((GeneralPath) shape).lineTo(511.669, 704.753);
        ((GeneralPath) shape).lineTo(511.654, 704.497);
        ((GeneralPath) shape).lineTo(511.638, 704.245);
        ((GeneralPath) shape).lineTo(511.622, 703.994);
        ((GeneralPath) shape).lineTo(511.605, 703.745);
        ((GeneralPath) shape).lineTo(511.588, 703.499);
        ((GeneralPath) shape).lineTo(511.57, 703.255);
        ((GeneralPath) shape).lineTo(511.551, 703.014);
        ((GeneralPath) shape).lineTo(511.532, 702.774);
        ((GeneralPath) shape).lineTo(511.512, 702.537);
        ((GeneralPath) shape).lineTo(511.491, 702.302);
        ((GeneralPath) shape).lineTo(511.47, 702.069);
        ((GeneralPath) shape).lineTo(511.448, 701.839);
        ((GeneralPath) shape).lineTo(511.426, 701.61);
        ((GeneralPath) shape).lineTo(511.403, 701.384);
        ((GeneralPath) shape).lineTo(511.38, 701.16);
        ((GeneralPath) shape).lineTo(511.356, 700.938);
        ((GeneralPath) shape).lineTo(511.331, 700.718);
        ((GeneralPath) shape).lineTo(511.306, 700.5);
        ((GeneralPath) shape).lineTo(511.281, 700.285);
        ((GeneralPath) shape).lineTo(511.254, 700.072);
        ((GeneralPath) shape).lineTo(511.228, 699.86);
        ((GeneralPath) shape).lineTo(511.2, 699.651);
        ((GeneralPath) shape).lineTo(511.173, 699.444);
        ((GeneralPath) shape).lineTo(511.145, 699.239);
        ((GeneralPath) shape).lineTo(511.116, 699.036);
        ((GeneralPath) shape).lineTo(511.087, 698.835);
        ((GeneralPath) shape).lineTo(511.057, 698.636);
        ((GeneralPath) shape).lineTo(511.027, 698.439);
        ((GeneralPath) shape).lineTo(510.996, 698.245);
        ((GeneralPath) shape).lineTo(510.965, 698.052);
        ((GeneralPath) shape).lineTo(510.933, 697.861);
        ((GeneralPath) shape).lineTo(510.901, 697.672);
        ((GeneralPath) shape).lineTo(510.869, 697.486);
        ((GeneralPath) shape).lineTo(510.836, 697.301);
        ((GeneralPath) shape).lineTo(510.802, 697.118);
        ((GeneralPath) shape).lineTo(510.769, 696.937);
        ((GeneralPath) shape).lineTo(510.734, 696.758);
        ((GeneralPath) shape).lineTo(510.7, 696.581);
        ((GeneralPath) shape).lineTo(510.665, 696.406);
        ((GeneralPath) shape).lineTo(510.629, 696.233);
        ((GeneralPath) shape).lineTo(510.594, 696.062);
        ((GeneralPath) shape).lineTo(510.558, 695.893);
        ((GeneralPath) shape).lineTo(510.521, 695.726);
        ((GeneralPath) shape).lineTo(510.484, 695.56);
        ((GeneralPath) shape).lineTo(510.447, 695.397);
        ((GeneralPath) shape).lineTo(510.409, 695.235);
        ((GeneralPath) shape).lineTo(510.371, 695.075);
        ((GeneralPath) shape).lineTo(510.333, 694.917);
        ((GeneralPath) shape).lineTo(510.294, 694.761);
        ((GeneralPath) shape).lineTo(510.255, 694.607);
        ((GeneralPath) shape).lineTo(510.216, 694.455);
        ((GeneralPath) shape).lineTo(510.176, 694.304);
        ((GeneralPath) shape).lineTo(510.137, 694.155);
        ((GeneralPath) shape).lineTo(510.096, 694.008);
        ((GeneralPath) shape).lineTo(510.056, 693.863);
        ((GeneralPath) shape).lineTo(510.015, 693.719);
        ((GeneralPath) shape).lineTo(509.974, 693.577);
        ((GeneralPath) shape).lineTo(509.933, 693.438);
        ((GeneralPath) shape).lineTo(509.891, 693.299);
        ((GeneralPath) shape).lineTo(509.849, 693.163);
        ((GeneralPath) shape).lineTo(509.807, 693.028);
        ((GeneralPath) shape).lineTo(509.765, 692.895);
        ((GeneralPath) shape).lineTo(509.723, 692.764);
        ((GeneralPath) shape).lineTo(509.68, 692.634);
        ((GeneralPath) shape).lineTo(509.637, 692.506);
        ((GeneralPath) shape).lineTo(509.594, 692.38);
        ((GeneralPath) shape).lineTo(509.55, 692.255);
        ((GeneralPath) shape).lineTo(509.507, 692.132);
        ((GeneralPath) shape).lineTo(509.463, 692.011);
        ((GeneralPath) shape).lineTo(509.419, 691.891);
        ((GeneralPath) shape).lineTo(509.375, 691.773);
        ((GeneralPath) shape).lineTo(509.331, 691.657);
        ((GeneralPath) shape).lineTo(509.286, 691.542);
        ((GeneralPath) shape).lineTo(509.242, 691.429);
        ((GeneralPath) shape).lineTo(509.197, 691.317);
        ((GeneralPath) shape).lineTo(509.152, 691.207);
        ((GeneralPath) shape).lineTo(509.107, 691.098);
        ((GeneralPath) shape).lineTo(509.062, 690.992);
        ((GeneralPath) shape).lineTo(509.017, 690.886);
        ((GeneralPath) shape).lineTo(508.972, 690.782);
        ((GeneralPath) shape).lineTo(508.926, 690.68);
        ((GeneralPath) shape).lineTo(508.881, 690.579);
        ((GeneralPath) shape).lineTo(508.835, 690.48);
        ((GeneralPath) shape).lineTo(508.79, 690.382);
        ((GeneralPath) shape).lineTo(508.744, 690.286);
        ((GeneralPath) shape).lineTo(508.7, 690.193);
        ((GeneralPath) shape).lineTo(508.607, 690.005);
        ((GeneralPath) shape).lineTo(508.515, 689.826);
        ((GeneralPath) shape).lineTo(508.423, 689.652);
        ((GeneralPath) shape).lineTo(508.331, 689.484);
        ((GeneralPath) shape).lineTo(508.239, 689.321);
        ((GeneralPath) shape).lineTo(508.148, 689.163);
        ((GeneralPath) shape).lineTo(508.056, 689.011);
        ((GeneralPath) shape).lineTo(507.965, 688.864);
        ((GeneralPath) shape).lineTo(507.873, 688.723);
        ((GeneralPath) shape).lineTo(507.783, 688.586);
        ((GeneralPath) shape).lineTo(507.692, 688.454);
        ((GeneralPath) shape).lineTo(507.602, 688.327);
        ((GeneralPath) shape).lineTo(507.513, 688.205);
        ((GeneralPath) shape).lineTo(507.424, 688.088);
        ((GeneralPath) shape).lineTo(507.335, 687.975);
        ((GeneralPath) shape).lineTo(507.248, 687.866);
        ((GeneralPath) shape).lineTo(507.161, 687.762);
        ((GeneralPath) shape).lineTo(507.074, 687.662);
        ((GeneralPath) shape).lineTo(506.989, 687.567);
        ((GeneralPath) shape).lineTo(506.904, 687.475);
        ((GeneralPath) shape).lineTo(506.82, 687.387);
        ((GeneralPath) shape).lineTo(506.738, 687.304);
        ((GeneralPath) shape).lineTo(506.656, 687.224);
        ((GeneralPath) shape).lineTo(506.575, 687.148);
        ((GeneralPath) shape).lineTo(506.495, 687.075);
        ((GeneralPath) shape).lineTo(506.417, 687.006);
        ((GeneralPath) shape).lineTo(506.339, 686.94);
        ((GeneralPath) shape).lineTo(506.263, 686.877);
        ((GeneralPath) shape).lineTo(506.187, 686.818);
        ((GeneralPath) shape).lineTo(506.113, 686.762);
        ((GeneralPath) shape).lineTo(506.041, 686.709);
        ((GeneralPath) shape).lineTo(505.969, 686.658);
        ((GeneralPath) shape).lineTo(505.899, 686.611);
        ((GeneralPath) shape).lineTo(505.83, 686.566);
        ((GeneralPath) shape).lineTo(505.763, 686.523);
        ((GeneralPath) shape).lineTo(505.697, 686.484);
        ((GeneralPath) shape).lineTo(505.633, 686.447);
        ((GeneralPath) shape).lineTo(505.57, 686.412);
        ((GeneralPath) shape).lineTo(505.508, 686.379);
        ((GeneralPath) shape).lineTo(505.448, 686.348);
        ((GeneralPath) shape).lineTo(505.39, 686.32);
        ((GeneralPath) shape).lineTo(505.333, 686.294);
        ((GeneralPath) shape).lineTo(505.277, 686.269);
        ((GeneralPath) shape).lineTo(505.223, 686.246);
        ((GeneralPath) shape).lineTo(505.171, 686.225);
        ((GeneralPath) shape).lineTo(505.121, 686.206);
        ((GeneralPath) shape).lineTo(505.072, 686.188);
        ((GeneralPath) shape).lineTo(505.025, 686.172);
        ((GeneralPath) shape).lineTo(504.98, 686.158);
        ((GeneralPath) shape).lineTo(504.937, 686.144);
        ((GeneralPath) shape).lineTo(504.895, 686.132);
        ((GeneralPath) shape).lineTo(504.855, 686.121);
        ((GeneralPath) shape).lineTo(504.817, 686.112);
        ((GeneralPath) shape).lineTo(504.781, 686.103);
        ((GeneralPath) shape).lineTo(504.747, 686.096);
        ((GeneralPath) shape).lineTo(504.714, 686.089);
        ((GeneralPath) shape).lineTo(504.684, 686.084);
        ((GeneralPath) shape).lineTo(504.661, 686.08);
        ((GeneralPath) shape).lineTo(504.606, 686.072);
        ((GeneralPath) shape).lineTo(504.564, 686.067);
        ((GeneralPath) shape).lineTo(504.531, 686.064);
        ((GeneralPath) shape).lineTo(504.508, 686.063);
        ((GeneralPath) shape).lineTo(504.495, 686.062);
        ((GeneralPath) shape).lineTo(504.46, 686.064);
        ((GeneralPath) shape).lineTo(392.591, 686.064);
        ((GeneralPath) shape).lineTo(365.844, 676.318);
        ((GeneralPath) shape).lineTo(364.957, 668.23);
        ((GeneralPath) shape).lineTo(364.144, 660.826);
        ((GeneralPath) shape).lineTo(363.199, 652.25);
        ((GeneralPath) shape).lineTo(362.713, 647.845);
        ((GeneralPath) shape).lineTo(362.237, 643.534);
        ((GeneralPath) shape).lineTo(361.783, 639.445);
        ((GeneralPath) shape).lineTo(361.367, 635.709);
        ((GeneralPath) shape).lineTo(361.003, 632.454);
        ((GeneralPath) shape).lineTo(360.844, 631.047);
        ((GeneralPath) shape).lineTo(360.704, 629.809);
        ((GeneralPath) shape).lineTo(360.583, 628.757);
        ((GeneralPath) shape).lineTo(360.485, 627.907);
        ((GeneralPath) shape).lineTo(360.436, 627.5);
        ((GeneralPath) shape).lineTo(360.383, 627.463);
        ((GeneralPath) shape).lineTo(360.299, 627.405);
        ((GeneralPath) shape).lineTo(360.215, 627.35);
        ((GeneralPath) shape).lineTo(360.132, 627.296);
        ((GeneralPath) shape).lineTo(360.048, 627.244);
        ((GeneralPath) shape).lineTo(359.964, 627.194);
        ((GeneralPath) shape).lineTo(359.88, 627.146);
        ((GeneralPath) shape).lineTo(359.796, 627.099);
        ((GeneralPath) shape).lineTo(359.71, 627.054);
        ((GeneralPath) shape).lineTo(359.624, 627.01);
        ((GeneralPath) shape).lineTo(359.538, 626.967);
        ((GeneralPath) shape).lineTo(359.45, 626.926);
        ((GeneralPath) shape).lineTo(359.361, 626.886);
        ((GeneralPath) shape).lineTo(359.27, 626.847);
        ((GeneralPath) shape).lineTo(359.179, 626.809);
        ((GeneralPath) shape).lineTo(359.085, 626.772);
        ((GeneralPath) shape).lineTo(358.99, 626.736);
        ((GeneralPath) shape).lineTo(358.893, 626.701);
        ((GeneralPath) shape).lineTo(358.794, 626.668);
        ((GeneralPath) shape).lineTo(358.692, 626.635);
        ((GeneralPath) shape).lineTo(358.588, 626.603);
        ((GeneralPath) shape).lineTo(358.482, 626.572);
        ((GeneralPath) shape).lineTo(358.373, 626.542);
        ((GeneralPath) shape).lineTo(358.261, 626.512);
        ((GeneralPath) shape).lineTo(358.147, 626.484);
        ((GeneralPath) shape).lineTo(358.03, 626.456);
        ((GeneralPath) shape).lineTo(357.909, 626.43);
        ((GeneralPath) shape).lineTo(357.785, 626.403);
        ((GeneralPath) shape).lineTo(357.658, 626.378);
        ((GeneralPath) shape).lineTo(357.528, 626.354);
        ((GeneralPath) shape).lineTo(357.394, 626.331);
        ((GeneralPath) shape).lineTo(357.257, 626.308);
        ((GeneralPath) shape).lineTo(357.115, 626.286);
        ((GeneralPath) shape).lineTo(356.971, 626.265);
        ((GeneralPath) shape).lineTo(356.822, 626.244);
        ((GeneralPath) shape).lineTo(356.67, 626.225);
        ((GeneralPath) shape).lineTo(356.514, 626.206);
        ((GeneralPath) shape).lineTo(356.354, 626.187);
        ((GeneralPath) shape).lineTo(356.189, 626.17);
        ((GeneralPath) shape).lineTo(356.02, 626.153);
        ((GeneralPath) shape).lineTo(355.847, 626.137);
        ((GeneralPath) shape).lineTo(355.671, 626.121);
        ((GeneralPath) shape).lineTo(355.489, 626.106);
        ((GeneralPath) shape).lineTo(355.304, 626.092);
        ((GeneralPath) shape).lineTo(355.113, 626.079);
        ((GeneralPath) shape).lineTo(354.918, 626.065);
        ((GeneralPath) shape).lineTo(354.719, 626.053);
        ((GeneralPath) shape).lineTo(354.515, 626.041);
        ((GeneralPath) shape).lineTo(354.307, 626.029);
        ((GeneralPath) shape).lineTo(353.873, 626.008);
        ((GeneralPath) shape).lineTo(353.421, 625.987);
        ((GeneralPath) shape).lineTo(352.948, 625.969);
        ((GeneralPath) shape).lineTo(352.454, 625.951);
        ((GeneralPath) shape).lineTo(351.818, 625.931);
        ((GeneralPath) shape).lineTo(351.491, 625.868);
        ((GeneralPath) shape).lineTo(351.157, 625.702);
        ((GeneralPath) shape).lineTo(350.935, 625.526);
        ((GeneralPath) shape).lineTo(350.793, 625.373);
        ((GeneralPath) shape).lineTo(350.698, 625.244);
        ((GeneralPath) shape).lineTo(350.632, 625.138);
        ((GeneralPath) shape).lineTo(350.586, 625.052);
        ((GeneralPath) shape).lineTo(350.545, 624.966);
        ((GeneralPath) shape).lineTo(350.495, 624.848);
        ((GeneralPath) shape).lineTo(350.455, 624.735);
        ((GeneralPath) shape).lineTo(350.421, 624.63);
        ((GeneralPath) shape).lineTo(350.39, 624.528);
        ((GeneralPath) shape).lineTo(350.361, 624.425);
        ((GeneralPath) shape).lineTo(350.333, 624.318);
        ((GeneralPath) shape).lineTo(350.278, 624.1);
        ((GeneralPath) shape).lineTo(350.219, 623.861);
        ((GeneralPath) shape).lineTo(350.156, 623.603);
        ((GeneralPath) shape).lineTo(350.088, 623.327);
        ((GeneralPath) shape).lineTo(350.014, 623.031);
        ((GeneralPath) shape).lineTo(349.975, 622.88);
        ((GeneralPath) shape).lineTo(349.934, 622.723);
        ((GeneralPath) shape).lineTo(349.891, 622.563);
        ((GeneralPath) shape).lineTo(349.846, 622.4);
        ((GeneralPath) shape).lineTo(349.799, 622.234);
        ((GeneralPath) shape).lineTo(349.75, 622.066);
        ((GeneralPath) shape).lineTo(349.699, 621.895);
        ((GeneralPath) shape).lineTo(349.645, 621.722);
        ((GeneralPath) shape).lineTo(349.59, 621.547);
        ((GeneralPath) shape).lineTo(349.532, 621.371);
        ((GeneralPath) shape).lineTo(349.472, 621.194);
        ((GeneralPath) shape).lineTo(349.409, 621.016);
        ((GeneralPath) shape).lineTo(349.344, 620.838);
        ((GeneralPath) shape).lineTo(349.276, 620.659);
        ((GeneralPath) shape).lineTo(349.206, 620.48);
        ((GeneralPath) shape).lineTo(349.133, 620.3);
        ((GeneralPath) shape).lineTo(349.097, 620.213);
        ((GeneralPath) shape).lineTo(349.059, 620.125);
        ((GeneralPath) shape).lineTo(349.02, 620.036);
        ((GeneralPath) shape).lineTo(348.981, 619.949);
        ((GeneralPath) shape).lineTo(348.941, 619.861);
        ((GeneralPath) shape).lineTo(348.901, 619.774);
        ((GeneralPath) shape).lineTo(348.86, 619.687);
        ((GeneralPath) shape).lineTo(348.818, 619.601);
        ((GeneralPath) shape).lineTo(348.775, 619.515);
        ((GeneralPath) shape).lineTo(348.732, 619.429);
        ((GeneralPath) shape).lineTo(348.689, 619.345);
        ((GeneralPath) shape).lineTo(348.645, 619.261);
        ((GeneralPath) shape).lineTo(348.599, 619.177);
        ((GeneralPath) shape).lineTo(348.554, 619.094);
        ((GeneralPath) shape).lineTo(348.507, 619.012);
        ((GeneralPath) shape).lineTo(348.46, 618.931);
        ((GeneralPath) shape).lineTo(348.413, 618.851);
        ((GeneralPath) shape).lineTo(348.365, 618.772);
        ((GeneralPath) shape).lineTo(348.316, 618.693);
        ((GeneralPath) shape).lineTo(348.266, 618.615);
        ((GeneralPath) shape).lineTo(348.216, 618.538);
        ((GeneralPath) shape).lineTo(348.166, 618.463);
        ((GeneralPath) shape).lineTo(348.114, 618.388);
        ((GeneralPath) shape).lineTo(348.062, 618.314);
        ((GeneralPath) shape).lineTo(348.01, 618.241);
        ((GeneralPath) shape).lineTo(347.956, 618.17);
        ((GeneralPath) shape).lineTo(347.903, 618.1);
        ((GeneralPath) shape).lineTo(347.848, 618.03);
        ((GeneralPath) shape).lineTo(347.793, 617.962);
        ((GeneralPath) shape).lineTo(347.738, 617.896);
        ((GeneralPath) shape).lineTo(347.682, 617.831);
        ((GeneralPath) shape).lineTo(347.626, 617.766);
        ((GeneralPath) shape).lineTo(347.568, 617.703);
        ((GeneralPath) shape).lineTo(347.511, 617.642);
        ((GeneralPath) shape).lineTo(347.453, 617.582);
        ((GeneralPath) shape).lineTo(347.394, 617.523);
        ((GeneralPath) shape).lineTo(347.335, 617.465);
        ((GeneralPath) shape).lineTo(347.275, 617.409);
        ((GeneralPath) shape).lineTo(347.215, 617.355);
        ((GeneralPath) shape).lineTo(347.154, 617.302);
        ((GeneralPath) shape).lineTo(347.093, 617.25);
        ((GeneralPath) shape).lineTo(347.031, 617.2);
        ((GeneralPath) shape).lineTo(346.969, 617.151);
        ((GeneralPath) shape).lineTo(346.906, 617.103);
        ((GeneralPath) shape).lineTo(346.843, 617.057);
        ((GeneralPath) shape).lineTo(346.779, 617.013);
        ((GeneralPath) shape).lineTo(346.714, 616.97);
        ((GeneralPath) shape).lineTo(346.674, 616.944);
        ((GeneralPath) shape).lineTo(346.634, 616.97);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x80FFFFFF, true));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 80.9341f, 815.332f));

        // _0_1_55

        // _0_1_55_0

        // _0_1_55_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-27.646, -13.717);
        ((GeneralPath) shape).lineTo(-30.292, -16.363);
        ((GeneralPath) shape).lineTo(-34.759, -16.363);
        ((GeneralPath) shape).lineTo(-36.835, -14.287);
        ((GeneralPath) shape).lineTo(-36.835, -4.164);
        ((GeneralPath) shape).lineTo(-34.759, -2.088);
        ((GeneralPath) shape).lineTo(-30.292, -2.088);
        ((GeneralPath) shape).lineTo(-27.646, -4.734);
        ((GeneralPath) shape).lineTo(-25.946, -3.035);
        ((GeneralPath) shape).lineTo(-29.224, 0.243);
        ((GeneralPath) shape).lineTo(-35.827, 0.243);
        ((GeneralPath) shape).lineTo(-39.287, -3.217);
        ((GeneralPath) shape).lineTo(-39.287, -15.234);
        ((GeneralPath) shape).lineTo(-35.827, -18.694);
        ((GeneralPath) shape).lineTo(-29.224, -18.694);
        ((GeneralPath) shape).lineTo(-25.946, -15.416);
        ((GeneralPath) shape).lineTo(-27.646, -13.717);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_1_55_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-16.199, -15.538);
        ((GeneralPath) shape).lineTo(-20.97, -7.623);
        ((GeneralPath) shape).lineTo(-11.708, -7.623);
        ((GeneralPath) shape).lineTo(-16.199, -15.538);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-17.219, -18.694);
        ((GeneralPath) shape).lineTo(-15.434, -18.694);
        ((GeneralPath) shape).lineTo(-9.086, -7.259);
        ((GeneralPath) shape).lineTo(-9.086, 0.243);
        ((GeneralPath) shape).lineTo(-11.708, 0.243);
        ((GeneralPath) shape).lineTo(-11.708, -5.292);
        ((GeneralPath) shape).lineTo(-20.97, -5.292);
        ((GeneralPath) shape).lineTo(-20.97, 0.243);
        ((GeneralPath) shape).lineTo(-23.592, 0.243);
        ((GeneralPath) shape).lineTo(-23.592, -7.198);
        ((GeneralPath) shape).lineTo(-17.219, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_55_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-3.49, -8.06);
        ((GeneralPath) shape).lineTo(-3.49, -2.088);
        ((GeneralPath) shape).lineTo(1.584, -2.088);
        ((GeneralPath) shape).lineTo(3.66, -4.164);
        ((GeneralPath) shape).lineTo(3.66, -5.984);
        ((GeneralPath) shape).lineTo(1.584, -8.06);
        ((GeneralPath) shape).lineTo(-3.49, -8.06);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-6.063, -18.694);
        ((GeneralPath) shape).lineTo(2.652, -18.694);
        ((GeneralPath) shape).lineTo(6.112, -15.234);
        ((GeneralPath) shape).lineTo(6.112, -11.52);
        ((GeneralPath) shape).lineTo(3.818, -9.225);
        ((GeneralPath) shape).lineTo(6.112, -6.931);
        ((GeneralPath) shape).lineTo(6.112, -3.217);
        ((GeneralPath) shape).lineTo(2.652, 0.243);
        ((GeneralPath) shape).lineTo(-6.063, 0.243);
        ((GeneralPath) shape).lineTo(-6.063, -18.694);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-3.466, -16.363);
        ((GeneralPath) shape).lineTo(-3.466, -10.391);
        ((GeneralPath) shape).lineTo(1.584, -10.391);
        ((GeneralPath) shape).lineTo(3.66, -12.466);
        ((GeneralPath) shape).lineTo(3.66, -14.287);
        ((GeneralPath) shape).lineTo(1.584, -16.363);
        ((GeneralPath) shape).lineTo(-3.466, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_55_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(15.107, -16.751);
        ((GeneralPath) shape).lineTo(11.016, -16.751);
        ((GeneralPath) shape).lineTo(11.016, -18.694);
        ((GeneralPath) shape).lineTo(21.686, -18.694);
        ((GeneralPath) shape).lineTo(21.686, -16.751);
        ((GeneralPath) shape).lineTo(17.559, -16.751);
        ((GeneralPath) shape).lineTo(17.559, -1.869);
        ((GeneralPath) shape).lineTo(21.686, -1.869);
        ((GeneralPath) shape).lineTo(21.686, 0.243);
        ((GeneralPath) shape).lineTo(11.016, 0.243);
        ((GeneralPath) shape).lineTo(11.016, -1.869);
        ((GeneralPath) shape).lineTo(15.107, -1.869);
        ((GeneralPath) shape).lineTo(15.107, -16.751);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_55_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(39.445, 0.243);
        ((GeneralPath) shape).lineTo(37.539, 0.243);
        ((GeneralPath) shape).lineTo(28.374, -13.51);
        ((GeneralPath) shape).lineTo(28.374, 0.243);
        ((GeneralPath) shape).lineTo(25.922, 0.243);
        ((GeneralPath) shape).lineTo(25.922, -18.694);
        ((GeneralPath) shape).lineTo(27.828, -18.694);
        ((GeneralPath) shape).lineTo(36.993, -4.94);
        ((GeneralPath) shape).lineTo(36.993, -18.694);
        ((GeneralPath) shape).lineTo(39.445, -18.694);
        ((GeneralPath) shape).lineTo(39.445, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_55
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 212.957f, 815.332f));

        // _0_1_56

        // _0_1_56_0

        // _0_1_56_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-25.685, 0.243);
        ((GeneralPath) shape).lineTo(-25.685, -16.363);
        ((GeneralPath) shape).lineTo(-31.221, -16.363);
        ((GeneralPath) shape).lineTo(-31.221, -18.694);
        ((GeneralPath) shape).lineTo(-17.698, -18.694);
        ((GeneralPath) shape).lineTo(-17.698, -16.363);
        ((GeneralPath) shape).lineTo(-23.233, -16.363);
        ((GeneralPath) shape).lineTo(-23.233, 0.243);
        ((GeneralPath) shape).lineTo(-25.685, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_56_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-12.442, -16.363);
        ((GeneralPath) shape).lineTo(-12.442, -10.391);
        ((GeneralPath) shape).lineTo(-4.139, -10.391);
        ((GeneralPath) shape).lineTo(-4.139, -8.06);
        ((GeneralPath) shape).lineTo(-12.442, -8.06);
        ((GeneralPath) shape).lineTo(-12.442, -2.088);
        ((GeneralPath) shape).lineTo(-1.372, -2.088);
        ((GeneralPath) shape).lineTo(-1.372, 0.243);
        ((GeneralPath) shape).lineTo(-14.894, 0.243);
        ((GeneralPath) shape).lineTo(-14.894, -18.694);
        ((GeneralPath) shape).lineTo(-1.372, -18.694);
        ((GeneralPath) shape).lineTo(-1.372, -16.363);
        ((GeneralPath) shape).lineTo(-12.442, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_56_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(1.432, -18.694);
        ((GeneralPath) shape).lineTo(3.338, -18.694);
        ((GeneralPath) shape).lineTo(8.194, -11.386);
        ((GeneralPath) shape).lineTo(13.049, -18.694);
        ((GeneralPath) shape).lineTo(14.955, -18.694);
        ((GeneralPath) shape).lineTo(14.955, 0.243);
        ((GeneralPath) shape).lineTo(12.503, 0.243);
        ((GeneralPath) shape).lineTo(12.503, -13.51);
        ((GeneralPath) shape).lineTo(9.14, -8.473);
        ((GeneralPath) shape).lineTo(7.247, -8.473);
        ((GeneralPath) shape).lineTo(3.884, -13.51);
        ((GeneralPath) shape).lineTo(3.884, 0.243);
        ((GeneralPath) shape).lineTo(1.432, 0.243);
        ((GeneralPath) shape).lineTo(1.432, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_56_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.759, -18.694);
        ((GeneralPath) shape).lineTo(27.822, -18.694);
        ((GeneralPath) shape).lineTo(31.281, -15.234);
        ((GeneralPath) shape).lineTo(31.281, -11.52);
        ((GeneralPath) shape).lineTo(27.822, -8.06);
        ((GeneralPath) shape).lineTo(20.211, -8.06);
        ((GeneralPath) shape).lineTo(20.211, 0.243);
        ((GeneralPath) shape).lineTo(17.759, 0.243);
        ((GeneralPath) shape).lineTo(17.759, -18.694);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(20.211, -16.363);
        ((GeneralPath) shape).lineTo(20.211, -10.391);
        ((GeneralPath) shape).lineTo(26.754, -10.391);
        ((GeneralPath) shape).lineTo(28.829, -12.466);
        ((GeneralPath) shape).lineTo(28.829, -14.287);
        ((GeneralPath) shape).lineTo(26.754, -16.363);
        ((GeneralPath) shape).lineTo(20.211, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_56

        // _0_1_57
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(254.786, 878.148);
        ((GeneralPath) shape).lineTo(172.5, 878.148);
        ((GeneralPath) shape).lineTo(172.5, 785.148);
        ((GeneralPath) shape).lineTo(254.786, 785.148);
        ((GeneralPath) shape).lineTo(254.786, 878.148);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(175.5, 788.148);
        ((GeneralPath) shape).lineTo(175.5, 875.148);
        ((GeneralPath) shape).lineTo(251.786, 875.148);
        ((GeneralPath) shape).lineTo(251.786, 788.148);
        ((GeneralPath) shape).lineTo(175.5, 788.148);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_58
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(133.5, 878.505);
        ((GeneralPath) shape).lineTo(29.786, 878.505);
        ((GeneralPath) shape).lineTo(29.786, 785.148);
        ((GeneralPath) shape).lineTo(133.5, 785.148);
        ((GeneralPath) shape).lineTo(133.5, 878.505);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(32.786, 788.148);
        ((GeneralPath) shape).lineTo(32.786, 875.505);
        ((GeneralPath) shape).lineTo(130.5, 875.505);
        ((GeneralPath) shape).lineTo(130.5, 788.148);
        ((GeneralPath) shape).lineTo(32.786, 788.148);
        ((GeneralPath) shape).closePath();

        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 218.932f, 867.344f));

        // _0_1_59

        // _0_1_59_0

        // _0_1_59_0_0

        // _0_1_59_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-20.766, -2.07);
        ((GeneralPath) shape).lineTo(-22.992, 0.156);
        ((GeneralPath) shape).lineTo(-27.242, 0.156);
        ((GeneralPath) shape).lineTo(-29.469, -2.07);
        ((GeneralPath) shape).lineTo(-29.469, -9.805);
        ((GeneralPath) shape).lineTo(-27.242, -12.031);
        ((GeneralPath) shape).lineTo(-22.992, -12.031);
        ((GeneralPath) shape).lineTo(-20.766, -9.805);
        ((GeneralPath) shape).lineTo(-20.766, -2.07);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-26.555, -10.531);
        ((GeneralPath) shape).lineTo(-27.891, -9.195);
        ((GeneralPath) shape).lineTo(-27.891, -2.68);
        ((GeneralPath) shape).lineTo(-26.555, -1.344);
        ((GeneralPath) shape).lineTo(-23.68, -1.344);
        ((GeneralPath) shape).lineTo(-22.344, -2.68);
        ((GeneralPath) shape).lineTo(-22.344, -9.195);
        ((GeneralPath) shape).lineTo(-23.68, -10.531);
        ((GeneralPath) shape).lineTo(-26.555, -10.531);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_59_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-14.547, -10.0);
        ((GeneralPath) shape).lineTo(-17.617, -4.906);
        ((GeneralPath) shape).lineTo(-11.656, -4.906);
        ((GeneralPath) shape).lineTo(-14.547, -10.0);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-15.203, -12.031);
        ((GeneralPath) shape).lineTo(-14.055, -12.031);
        ((GeneralPath) shape).lineTo(-9.969, -4.672);
        ((GeneralPath) shape).lineTo(-9.969, 0.156);
        ((GeneralPath) shape).lineTo(-11.656, 0.156);
        ((GeneralPath) shape).lineTo(-11.656, -3.406);
        ((GeneralPath) shape).lineTo(-17.617, -3.406);
        ((GeneralPath) shape).lineTo(-17.617, 0.156);
        ((GeneralPath) shape).lineTo(-19.305, 0.156);
        ((GeneralPath) shape).lineTo(-19.305, -4.633);
        ((GeneralPath) shape).lineTo(-15.203, -12.031);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_59_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-4.891, 0.156);
        ((GeneralPath) shape).lineTo(-4.891, -10.531);
        ((GeneralPath) shape).lineTo(-8.453, -10.531);
        ((GeneralPath) shape).lineTo(-8.453, -12.031);
        ((GeneralPath) shape).lineTo(0.25, -12.031);
        ((GeneralPath) shape).lineTo(0.25, -10.531);
        ((GeneralPath) shape).lineTo(-3.313, -10.531);
        ((GeneralPath) shape).lineTo(-3.313, 0.156);
        ((GeneralPath) shape).lineTo(-4.891, 0.156);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_59_0_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(15.445, -10.531);
        ((GeneralPath) shape).lineTo(15.445, -6.688);
        ((GeneralPath) shape).lineTo(20.789, -6.688);
        ((GeneralPath) shape).lineTo(20.789, -5.188);
        ((GeneralPath) shape).lineTo(15.445, -5.188);
        ((GeneralPath) shape).lineTo(15.445, 0.156);
        ((GeneralPath) shape).lineTo(13.867, 0.156);
        ((GeneralPath) shape).lineTo(13.867, -12.031);
        ((GeneralPath) shape).lineTo(22.57, -12.031);
        ((GeneralPath) shape).lineTo(22.57, -10.531);
        ((GeneralPath) shape).lineTo(15.445, -10.531);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_59_0_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(26.898, -7.906);
        ((GeneralPath) shape).curveTo(26.273, -7.906, 25.736, -8.134, 25.285, -8.59);
        ((GeneralPath) shape).curveTo(24.835, -9.046, 24.609, -9.589, 24.609, -10.219);
        ((GeneralPath) shape).curveTo(24.609, -10.854, 24.836, -11.398, 25.289, -11.852);
        ((GeneralPath) shape).curveTo(25.742, -12.305, 26.286, -12.531, 26.922, -12.531);
        ((GeneralPath) shape).curveTo(27.557, -12.531, 28.102, -12.305, 28.555, -11.852);
        ((GeneralPath) shape).curveTo(29.008, -11.398, 29.234, -10.854, 29.234, -10.219);
        ((GeneralPath) shape).curveTo(29.234, -9.573, 29.008, -9.026, 28.555, -8.578);
        ((GeneralPath) shape).curveTo(28.102, -8.13, 27.549, -7.906, 26.898, -7.906);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(26.906, -8.867);
        ((GeneralPath) shape).curveTo(27.286, -8.867, 27.609, -8.999, 27.875, -9.262);
        ((GeneralPath) shape).curveTo(28.141, -9.525, 28.273, -9.844, 28.273, -10.219);
        ((GeneralPath) shape).curveTo(28.273, -10.589, 28.141, -10.905, 27.875, -11.168);
        ((GeneralPath) shape).curveTo(27.609, -11.431, 27.292, -11.563, 26.922, -11.563);
        ((GeneralPath) shape).curveTo(26.552, -11.563, 26.234, -11.431, 25.969, -11.168);
        ((GeneralPath) shape).curveTo(25.703, -10.905, 25.57, -10.589, 25.57, -10.219);
        ((GeneralPath) shape).curveTo(25.57, -9.849, 25.702, -9.531, 25.965, -9.266);
        ((GeneralPath) shape).curveTo(26.228, -9.0, 26.542, -8.867, 26.906, -8.867);
        ((GeneralPath) shape).closePath();

        g.fill(shape);
        paint4(g, origAlpha, transformations);
    }

    private static void paint4(Graphics2D g, float origAlpha, java.util.LinkedList<AffineTransform> transformations) {
        Shape shape = null;

        g.setTransform(transformations.pop()); // _0_1_59
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 224.906f, 855.607f));

        // _0_1_60

        // _0_1_60_0

        // _0_1_60_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(0.311, -2.428);
        ((GeneralPath) shape).curveTo(0.311, -3.327, 0.561, -3.993, 1.061, -4.426);
        ((GeneralPath) shape).curveTo(1.478, -4.786, 1.987, -4.966, 2.588, -4.966);
        ((GeneralPath) shape).curveTo(3.256, -4.966, 3.801, -4.747, 4.225, -4.309);
        ((GeneralPath) shape).curveTo(4.649, -3.872, 4.86, -3.268, 4.86, -2.497);
        ((GeneralPath) shape).curveTo(4.86, -1.872, 4.767, -1.38, 4.579, -1.022);
        ((GeneralPath) shape).curveTo(4.392, -0.664, 4.119, -0.386, 3.761, -0.187);
        ((GeneralPath) shape).curveTo(3.403, 0.011, 3.012, 0.11, 2.588, 0.11);
        ((GeneralPath) shape).curveTo(1.908, 0.11, 1.359, -0.108, 0.94, -0.544);
        ((GeneralPath) shape).curveTo(0.52, -0.98, 0.311, -1.608, 0.311, -2.428);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(1.157, -2.428);
        ((GeneralPath) shape).curveTo(1.157, -1.806, 1.292, -1.34, 1.564, -1.031);
        ((GeneralPath) shape).curveTo(1.835, -0.722, 2.176, -0.567, 2.588, -0.567);
        ((GeneralPath) shape).curveTo(2.996, -0.567, 3.336, -0.722, 3.608, -1.033);
        ((GeneralPath) shape).curveTo(3.879, -1.344, 4.015, -1.818, 4.015, -2.455);
        ((GeneralPath) shape).curveTo(4.015, -3.056, 3.878, -3.511, 3.605, -3.82);
        ((GeneralPath) shape).curveTo(3.332, -4.13, 2.993, -4.284, 2.588, -4.284);
        ((GeneralPath) shape).curveTo(2.176, -4.284, 1.835, -4.13, 1.564, -3.823);
        ((GeneralPath) shape).curveTo(1.292, -3.515, 1.157, -3.05, 1.157, -2.428);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_60
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.999515f, 0, 0, 1.00048f, 80.9319f, 867.344f));

        // _0_1_61

        // _0_1_61_0

        // _0_1_61_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-40.992, -5.188);
        ((GeneralPath) shape).lineTo(-40.992, -1.344);
        ((GeneralPath) shape).lineTo(-37.727, -1.344);
        ((GeneralPath) shape).lineTo(-36.391, -2.68);
        ((GeneralPath) shape).lineTo(-36.391, -3.852);
        ((GeneralPath) shape).lineTo(-37.727, -5.188);
        ((GeneralPath) shape).lineTo(-40.992, -5.188);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-42.648, -12.031);
        ((GeneralPath) shape).lineTo(-37.039, -12.031);
        ((GeneralPath) shape).lineTo(-34.813, -9.805);
        ((GeneralPath) shape).lineTo(-34.813, -7.414);
        ((GeneralPath) shape).lineTo(-36.289, -5.938);
        ((GeneralPath) shape).lineTo(-34.813, -4.461);
        ((GeneralPath) shape).lineTo(-34.813, -2.07);
        ((GeneralPath) shape).lineTo(-37.039, 0.156);
        ((GeneralPath) shape).lineTo(-42.648, 0.156);
        ((GeneralPath) shape).lineTo(-42.648, -12.031);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-40.977, -10.531);
        ((GeneralPath) shape).lineTo(-40.977, -6.688);
        ((GeneralPath) shape).lineTo(-37.727, -6.688);
        ((GeneralPath) shape).lineTo(-36.391, -8.023);
        ((GeneralPath) shape).lineTo(-36.391, -9.195);
        ((GeneralPath) shape).lineTo(-37.727, -10.531);
        ((GeneralPath) shape).lineTo(-40.977, -10.531);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_61_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-28.156, -10.0);
        ((GeneralPath) shape).lineTo(-31.227, -4.906);
        ((GeneralPath) shape).lineTo(-25.266, -4.906);
        ((GeneralPath) shape).lineTo(-28.156, -10.0);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-28.813, -12.031);
        ((GeneralPath) shape).lineTo(-27.664, -12.031);
        ((GeneralPath) shape).lineTo(-23.578, -4.672);
        ((GeneralPath) shape).lineTo(-23.578, 0.156);
        ((GeneralPath) shape).lineTo(-25.266, 0.156);
        ((GeneralPath) shape).lineTo(-25.266, -3.406);
        ((GeneralPath) shape).lineTo(-31.227, -3.406);
        ((GeneralPath) shape).lineTo(-31.227, 0.156);
        ((GeneralPath) shape).lineTo(-32.914, 0.156);
        ((GeneralPath) shape).lineTo(-32.914, -4.633);
        ((GeneralPath) shape).lineTo(-28.813, -12.031);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_61_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-22.203, -12.031);
        ((GeneralPath) shape).lineTo(-15.727, -12.031);
        ((GeneralPath) shape).lineTo(-13.5, -9.805);
        ((GeneralPath) shape).lineTo(-13.5, -7.414);
        ((GeneralPath) shape).lineTo(-15.727, -5.188);
        ((GeneralPath) shape).lineTo(-16.414, -5.188);
        ((GeneralPath) shape).lineTo(-13.227, -0.391);
        ((GeneralPath) shape).lineTo(-14.516, 0.461);
        ((GeneralPath) shape).lineTo(-18.281, -5.188);
        ((GeneralPath) shape).lineTo(-20.625, -5.188);
        ((GeneralPath) shape).lineTo(-20.625, 0.156);
        ((GeneralPath) shape).lineTo(-22.203, 0.156);
        ((GeneralPath) shape).lineTo(-22.203, -12.031);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-20.625, -10.531);
        ((GeneralPath) shape).lineTo(-20.625, -6.688);
        ((GeneralPath) shape).lineTo(-16.414, -6.688);
        ((GeneralPath) shape).lineTo(-15.078, -8.023);
        ((GeneralPath) shape).lineTo(-15.078, -9.195);
        ((GeneralPath) shape).lineTo(-16.414, -10.531);
        ((GeneralPath) shape).lineTo(-20.625, -10.531);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_61_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-2.852, -2.07);
        ((GeneralPath) shape).lineTo(-5.078, 0.156);
        ((GeneralPath) shape).lineTo(-9.328, 0.156);
        ((GeneralPath) shape).lineTo(-11.555, -2.07);
        ((GeneralPath) shape).lineTo(-11.555, -9.805);
        ((GeneralPath) shape).lineTo(-9.328, -12.031);
        ((GeneralPath) shape).lineTo(-5.078, -12.031);
        ((GeneralPath) shape).lineTo(-2.852, -9.805);
        ((GeneralPath) shape).lineTo(-2.852, -2.07);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-8.641, -10.531);
        ((GeneralPath) shape).lineTo(-9.977, -9.195);
        ((GeneralPath) shape).lineTo(-9.977, -2.68);
        ((GeneralPath) shape).lineTo(-8.641, -1.344);
        ((GeneralPath) shape).lineTo(-5.766, -1.344);
        ((GeneralPath) shape).lineTo(-4.43, -2.68);
        ((GeneralPath) shape).lineTo(-4.43, -9.195);
        ((GeneralPath) shape).lineTo(-5.766, -10.531);
        ((GeneralPath) shape).lineTo(-8.641, -10.531);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_61_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.438, -10.781);
        ((GeneralPath) shape).lineTo(3.805, -10.781);
        ((GeneralPath) shape).lineTo(3.805, -12.031);
        ((GeneralPath) shape).lineTo(10.672, -12.031);
        ((GeneralPath) shape).lineTo(10.672, -10.781);
        ((GeneralPath) shape).lineTo(8.016, -10.781);
        ((GeneralPath) shape).lineTo(8.016, -1.203);
        ((GeneralPath) shape).lineTo(10.672, -1.203);
        ((GeneralPath) shape).lineTo(10.672, 0.156);
        ((GeneralPath) shape).lineTo(3.805, 0.156);
        ((GeneralPath) shape).lineTo(3.805, -1.203);
        ((GeneralPath) shape).lineTo(6.438, -1.203);
        ((GeneralPath) shape).lineTo(6.438, -10.781);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_61_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(22.102, 0.156);
        ((GeneralPath) shape).lineTo(20.875, 0.156);
        ((GeneralPath) shape).lineTo(14.977, -8.695);
        ((GeneralPath) shape).lineTo(14.977, 0.156);
        ((GeneralPath) shape).lineTo(13.398, 0.156);
        ((GeneralPath) shape).lineTo(13.398, -12.031);
        ((GeneralPath) shape).lineTo(14.625, -12.031);
        ((GeneralPath) shape).lineTo(20.523, -3.18);
        ((GeneralPath) shape).lineTo(20.523, -12.031);
        ((GeneralPath) shape).lineTo(22.102, -12.031);
        ((GeneralPath) shape).lineTo(22.102, 0.156);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_61_0_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(23.906, -12.031);
        ((GeneralPath) shape).lineTo(25.484, -12.031);
        ((GeneralPath) shape).lineTo(25.484, -6.688);
        ((GeneralPath) shape).lineTo(31.031, -6.688);
        ((GeneralPath) shape).lineTo(31.031, -12.031);
        ((GeneralPath) shape).lineTo(32.609, -12.031);
        ((GeneralPath) shape).lineTo(32.609, 0.156);
        ((GeneralPath) shape).lineTo(31.031, 0.156);
        ((GeneralPath) shape).lineTo(31.031, -5.188);
        ((GeneralPath) shape).lineTo(25.484, -5.188);
        ((GeneralPath) shape).lineTo(25.484, 0.156);
        ((GeneralPath) shape).lineTo(23.906, 0.156);
        ((GeneralPath) shape).lineTo(23.906, -12.031);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_61_0_7
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(41.906, -8.828);
        ((GeneralPath) shape).lineTo(40.203, -10.531);
        ((GeneralPath) shape).lineTo(37.328, -10.531);
        ((GeneralPath) shape).lineTo(35.992, -9.195);
        ((GeneralPath) shape).lineTo(35.992, -2.68);
        ((GeneralPath) shape).lineTo(37.328, -1.344);
        ((GeneralPath) shape).lineTo(40.203, -1.344);
        ((GeneralPath) shape).lineTo(41.539, -2.68);
        ((GeneralPath) shape).lineTo(41.539, -5.188);
        ((GeneralPath) shape).lineTo(37.977, -5.188);
        ((GeneralPath) shape).lineTo(37.977, -6.688);
        ((GeneralPath) shape).lineTo(43.117, -6.688);
        ((GeneralPath) shape).lineTo(43.117, -2.07);
        ((GeneralPath) shape).lineTo(40.891, 0.156);
        ((GeneralPath) shape).lineTo(36.641, 0.156);
        ((GeneralPath) shape).lineTo(34.414, -2.07);
        ((GeneralPath) shape).lineTo(34.414, -9.805);
        ((GeneralPath) shape).lineTo(36.641, -12.031);
        ((GeneralPath) shape).lineTo(40.891, -12.031);
        ((GeneralPath) shape).lineTo(43.0, -9.922);
        ((GeneralPath) shape).lineTo(41.906, -8.828);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_61
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 108.053f, 912.29f));

        // _0_1_62

        // _0_1_62_0

        // _0_1_62_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-76.202, -15.707);
        ((GeneralPath) shape).lineTo(-68.42, -15.707);
        ((GeneralPath) shape).lineTo(-65.524, -12.791);
        ((GeneralPath) shape).lineTo(-65.524, -2.672);
        ((GeneralPath) shape).lineTo(-68.42, 0.244);
        ((GeneralPath) shape).lineTo(-76.202, 0.244);
        ((GeneralPath) shape).lineTo(-76.202, -15.707);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-74.221, -13.766);
        ((GeneralPath) shape).lineTo(-74.221, -1.646);
        ((GeneralPath) shape).lineTo(-69.334, -1.646);
        ((GeneralPath) shape).lineTo(-67.597, -3.414);
        ((GeneralPath) shape).lineTo(-67.597, -11.998);
        ((GeneralPath) shape).lineTo(-69.334, -13.766);
        ((GeneralPath) shape).lineTo(-74.221, -13.766);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-60.8, -13.695);
        ((GeneralPath) shape).lineTo(-60.8, -8.697);
        ((GeneralPath) shape).lineTo(-53.851, -8.697);
        ((GeneralPath) shape).lineTo(-53.851, -6.746);
        ((GeneralPath) shape).lineTo(-60.8, -6.746);
        ((GeneralPath) shape).lineTo(-60.8, -1.747);
        ((GeneralPath) shape).lineTo(-51.535, -1.747);
        ((GeneralPath) shape).lineTo(-51.535, 0.203);
        ((GeneralPath) shape).lineTo(-62.852, 0.203);
        ((GeneralPath) shape).lineTo(-62.852, -15.646);
        ((GeneralPath) shape).lineTo(-51.535, -15.646);
        ((GeneralPath) shape).lineTo(-51.535, -13.695);
        ((GeneralPath) shape).lineTo(-60.8, -13.695);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-37.87, 0.203);
        ((GeneralPath) shape).lineTo(-39.465, 0.203);
        ((GeneralPath) shape).lineTo(-47.135, -11.308);
        ((GeneralPath) shape).lineTo(-47.135, 0.203);
        ((GeneralPath) shape).lineTo(-49.188, 0.203);
        ((GeneralPath) shape).lineTo(-49.188, -15.646);
        ((GeneralPath) shape).lineTo(-47.593, -15.646);
        ((GeneralPath) shape).lineTo(-39.922, -4.135);
        ((GeneralPath) shape).lineTo(-39.922, -15.646);
        ((GeneralPath) shape).lineTo(-37.87, -15.646);
        ((GeneralPath) shape).lineTo(-37.87, 0.203);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-32.628, 0.203);
        ((GeneralPath) shape).lineTo(-35.371, -2.54);
        ((GeneralPath) shape).lineTo(-33.948, -3.962);
        ((GeneralPath) shape).lineTo(-31.734, -1.747);
        ((GeneralPath) shape).lineTo(-27.995, -1.747);
        ((GeneralPath) shape).lineTo(-26.257, -3.485);
        ((GeneralPath) shape).lineTo(-26.257, -5.009);
        ((GeneralPath) shape).lineTo(-27.995, -6.746);
        ((GeneralPath) shape).lineTo(-32.628, -6.746);
        ((GeneralPath) shape).lineTo(-35.523, -9.641);
        ((GeneralPath) shape).lineTo(-35.523, -12.75);
        ((GeneralPath) shape).lineTo(-32.628, -15.646);
        ((GeneralPath) shape).lineTo(-27.101, -15.646);
        ((GeneralPath) shape).lineTo(-24.358, -12.903);
        ((GeneralPath) shape).lineTo(-25.78, -11.48);
        ((GeneralPath) shape).lineTo(-27.995, -13.695);
        ((GeneralPath) shape).lineTo(-31.734, -13.695);
        ((GeneralPath) shape).lineTo(-33.471, -11.958);
        ((GeneralPath) shape).lineTo(-33.471, -10.434);
        ((GeneralPath) shape).lineTo(-31.734, -8.697);
        ((GeneralPath) shape).lineTo(-27.101, -8.697);
        ((GeneralPath) shape).lineTo(-24.205, -5.801);
        ((GeneralPath) shape).lineTo(-24.205, -2.692);
        ((GeneralPath) shape).lineTo(-27.101, 0.203);
        ((GeneralPath) shape).lineTo(-32.628, 0.203);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-17.246, -14.02);
        ((GeneralPath) shape).lineTo(-20.67, -14.02);
        ((GeneralPath) shape).lineTo(-20.67, -15.646);
        ((GeneralPath) shape).lineTo(-11.739, -15.646);
        ((GeneralPath) shape).lineTo(-11.739, -14.02);
        ((GeneralPath) shape).lineTo(-15.194, -14.02);
        ((GeneralPath) shape).lineTo(-15.194, -1.565);
        ((GeneralPath) shape).lineTo(-11.739, -1.565);
        ((GeneralPath) shape).lineTo(-11.739, 0.203);
        ((GeneralPath) shape).lineTo(-20.67, 0.203);
        ((GeneralPath) shape).lineTo(-20.67, -1.565);
        ((GeneralPath) shape).lineTo(-17.246, -1.565);
        ((GeneralPath) shape).lineTo(-17.246, -14.02);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-3.561, 0.203);
        ((GeneralPath) shape).lineTo(-3.561, -13.695);
        ((GeneralPath) shape).lineTo(-8.194, -13.695);
        ((GeneralPath) shape).lineTo(-8.194, -15.646);
        ((GeneralPath) shape).lineTo(3.124, -15.646);
        ((GeneralPath) shape).lineTo(3.124, -13.695);
        ((GeneralPath) shape).lineTo(-1.509, -13.695);
        ((GeneralPath) shape).lineTo(-1.509, 0.203);
        ((GeneralPath) shape).lineTo(-3.561, 0.203);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(13.832, -15.483);
        ((GeneralPath) shape).lineTo(15.864, -15.483);
        ((GeneralPath) shape).lineTo(15.864, -11.145);
        ((GeneralPath) shape).lineTo(12.146, -7.427);
        ((GeneralPath) shape).lineTo(12.146, 0.203);
        ((GeneralPath) shape).lineTo(10.012, 0.203);
        ((GeneralPath) shape).lineTo(10.012, -7.427);
        ((GeneralPath) shape).lineTo(6.395, -11.145);
        ((GeneralPath) shape).lineTo(6.395, -15.483);
        ((GeneralPath) shape).lineTo(8.407, -15.483);
        ((GeneralPath) shape).lineTo(8.407, -12.344);
        ((GeneralPath) shape).lineTo(11.079, -9.55);
        ((GeneralPath) shape).lineTo(13.832, -12.344);
        ((GeneralPath) shape).lineTo(13.832, -15.483);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_7
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(29.996, -13.004);
        ((GeneralPath) shape).lineTo(26.003, -6.38);
        ((GeneralPath) shape).lineTo(33.755, -6.38);
        ((GeneralPath) shape).lineTo(29.996, -13.004);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(29.143, -15.646);
        ((GeneralPath) shape).lineTo(30.636, -15.646);
        ((GeneralPath) shape).lineTo(35.95, -6.075);
        ((GeneralPath) shape).lineTo(35.95, 0.203);
        ((GeneralPath) shape).lineTo(33.755, 0.203);
        ((GeneralPath) shape).lineTo(33.755, -4.43);
        ((GeneralPath) shape).lineTo(26.003, -4.43);
        ((GeneralPath) shape).lineTo(26.003, 0.203);
        ((GeneralPath) shape).lineTo(23.809, 0.203);
        ((GeneralPath) shape).lineTo(23.809, -6.025);
        ((GeneralPath) shape).lineTo(29.143, -15.646);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_8
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(49.238, -1.747);
        ((GeneralPath) shape).lineTo(49.238, 0.203);
        ((GeneralPath) shape).lineTo(37.921, 0.203);
        ((GeneralPath) shape).lineTo(37.921, -15.646);
        ((GeneralPath) shape).lineTo(39.973, -15.646);
        ((GeneralPath) shape).lineTo(39.973, -1.747);
        ((GeneralPath) shape).lineTo(49.238, -1.747);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_9
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(56.218, 0.203);
        ((GeneralPath) shape).lineTo(56.218, -13.695);
        ((GeneralPath) shape).lineTo(51.585, -13.695);
        ((GeneralPath) shape).lineTo(51.585, -15.646);
        ((GeneralPath) shape).lineTo(62.903, -15.646);
        ((GeneralPath) shape).lineTo(62.903, -13.695);
        ((GeneralPath) shape).lineTo(58.27, -13.695);
        ((GeneralPath) shape).lineTo(58.27, 0.203);
        ((GeneralPath) shape).lineTo(56.218, 0.203);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_62_0_10
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(70.726, -9.001);
        ((GeneralPath) shape).lineTo(71.092, -9.001);
        ((GeneralPath) shape).curveTo(71.552, -9.001, 71.94, -8.844, 72.255, -8.529);
        ((GeneralPath) shape).curveTo(72.57, -8.214, 72.727, -7.847, 72.727, -7.427);
        ((GeneralPath) shape).curveTo(72.727, -6.98, 72.568, -6.602, 72.25, -6.294);
        ((GeneralPath) shape).curveTo(71.932, -5.986, 71.546, -5.832, 71.092, -5.832);
        ((GeneralPath) shape).lineTo(70.726, -5.832);
        ((GeneralPath) shape).curveTo(70.265, -5.832, 69.878, -5.989, 69.563, -6.304);
        ((GeneralPath) shape).curveTo(69.248, -6.619, 69.09, -6.986, 69.09, -7.406);
        ((GeneralPath) shape).curveTo(69.09, -7.853, 69.25, -8.231, 69.568, -8.539);
        ((GeneralPath) shape).curveTo(69.886, -8.847, 70.272, -9.001, 70.726, -9.001);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(70.726, -2.652);
        ((GeneralPath) shape).lineTo(71.092, -2.652);
        ((GeneralPath) shape).curveTo(71.552, -2.652, 71.94, -2.496, 72.255, -2.184);
        ((GeneralPath) shape).curveTo(72.57, -1.873, 72.727, -1.504, 72.727, -1.077);
        ((GeneralPath) shape).curveTo(72.727, -0.637, 72.568, -0.262, 72.25, 0.046);
        ((GeneralPath) shape).curveTo(71.932, 0.354, 71.546, 0.508, 71.092, 0.508);
        ((GeneralPath) shape).lineTo(70.726, 0.508);
        ((GeneralPath) shape).curveTo(70.265, 0.508, 69.878, 0.352, 69.563, 0.041);
        ((GeneralPath) shape).curveTo(69.248, -0.271, 69.09, -0.64, 69.09, -1.067);
        ((GeneralPath) shape).curveTo(69.09, -1.507, 69.25, -1.881, 69.568, -2.189);
        ((GeneralPath) shape).curveTo(69.886, -2.498, 70.272, -2.652, 70.726, -2.652);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_62

        g.setTransform(transformations.pop()); // _0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0, 27.6378f));

        // _0_2

        // _0_2_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(292.862, 211.193);
        ((GeneralPath) shape).lineTo(304.484, 219.039);
        ((GeneralPath) shape).lineTo(292.862, 226.885);
        ((GeneralPath) shape).lineTo(292.862, 211.193);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.706764f, -0.707449f, 0.706764f, 0.707449f, 0, 0));

        // _0_2_1

        // _0_2_1_0
        shape = new Rectangle2D.Double(67.22599792480469, 377.44000244140625, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.setStroke(new BasicStroke(2, 0, 0, 4));
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_1

        // _0_2_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(627.425, 181.193);
        ((GeneralPath) shape).lineTo(639.047, 189.039);
        ((GeneralPath) shape).lineTo(627.425, 196.885);
        ((GeneralPath) shape).lineTo(627.425, 181.193);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00B3FF));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.706764f, -0.707449f, 0.706764f, 0.707449f, 0, 0));

        // _0_2_3

        // _0_2_3_0
        shape = new Rectangle2D.Double(318.1059875488281, 591.5689697265625, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_3

        // _0_2_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(488.044, 417.472);
        ((GeneralPath) shape).lineTo(480.206, 429.106);
        ((GeneralPath) shape).lineTo(472.368, 417.472);
        ((GeneralPath) shape).lineTo(488.044, 417.472);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00B3FF));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.706764f, 0.707449f, -0.706764f, 0.707449f, 0, 0));

        // _0_2_5

        // _0_2_5_0
        shape = new Rectangle2D.Double(645.4429931640625, -33.40299987792969, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_5

        // _0_2_6
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(38.957, 207.573);
        ((GeneralPath) shape).lineTo(50.579, 215.419);
        ((GeneralPath) shape).lineTo(38.957, 223.265);
        ((GeneralPath) shape).lineTo(38.957, 207.573);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.706764f, -0.707449f, 0.706764f, 0.707449f, 0, 0));

        // _0_2_7

        // _0_2_7_0
        shape = new Rectangle2D.Double(-116.11399841308594, 201.52999877929688, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_7

        // _0_2_8
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(923.103, 148.394);
        ((GeneralPath) shape).lineTo(911.481, 156.24);
        ((GeneralPath) shape).lineTo(923.103, 164.086);
        ((GeneralPath) shape).lineTo(923.103, 148.394);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(-0.706764f, -0.707449f, -0.706764f, 0.707449f, 0, 0));

        // _0_2_9

        // _0_2_9_0
        shape = new Rectangle2D.Double(-742.3499755859375, -533.4509887695312, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_9

        // _0_2_10
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(488.188, 91.922);
        ((GeneralPath) shape).lineTo(480.35, 103.555);
        ((GeneralPath) shape).lineTo(472.512, 91.922);
        ((GeneralPath) shape).lineTo(488.188, 91.922);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.706764f, 0.707449f, -0.706764f, 0.707449f, 0, 0));

        // _0_2_11

        // _0_2_11_0
        shape = new Rectangle2D.Double(419.6400146484375, -259.4100036621094, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_11

        // _0_2_12
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(656.986, 390.742);
        ((GeneralPath) shape).lineTo(668.608, 398.588);
        ((GeneralPath) shape).lineTo(656.986, 406.435);
        ((GeneralPath) shape).lineTo(656.986, 390.742);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.706764f, -0.707449f, 0.706764f, 0.707449f, 0, 0));

        // _0_2_13

        // _0_2_13_0
        shape = new Rectangle2D.Double(207.21099853515625, 744.2899780273438, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_13

        // _0_2_14
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(301.96, 390.742);
        ((GeneralPath) shape).lineTo(290.337, 398.588);
        ((GeneralPath) shape).lineTo(301.96, 406.435);
        ((GeneralPath) shape).lineTo(301.96, 390.742);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(-0.706764f, -0.707449f, -0.706764f, 0.707449f, 0, 0));

        // _0_2_15

        // _0_2_15_0
        shape = new Rectangle2D.Double(-471.1940002441406, 65.88400268554688, 8.95199966430664, 8.95199966430664);
        g.setPaint(WHITE);
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_15

        // _0_2_16
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(218.95, 484.732);
        ((GeneralPath) shape).curveTo(220.355, 479.698, 225.575, 476.756, 230.622, 478.064);
        ((GeneralPath) shape).curveTo(272.402, 497.819, 305.789, 515.745, 305.789, 515.745);
        ((GeneralPath) shape).curveTo(305.789, 515.745, 269.585, 507.236, 225.326, 496.416);
        ((GeneralPath) shape).curveTo(220.487, 494.987, 217.544, 489.767, 218.95, 484.732);
        ((GeneralPath) shape).lineTo(218.95, 484.732);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        g.setPaint(WHITE);
        g.setStroke(new BasicStroke(3, 1, 1, 4));
        g.draw(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.941646f, -0.336604f, 0.336759f, 0.941591f, 0, 0));

        // _0_2_17

        // _0_2_17_0
        shape = new Rectangle2D.Double(110.3499984741211, 600.8519897460938, 9.437999725341797, 9.425000190734863);
        g.setStroke(new BasicStroke(2, 0, 0, 4));
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_2_17

        // _0_2_18
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(338.587, 362.199);
        ((GeneralPath) shape).curveTo(338.587, 360.031, 336.826, 358.271, 334.658, 358.271);
        ((GeneralPath) shape).lineTo(325.623, 358.271);
        ((GeneralPath) shape).curveTo(323.455, 358.271, 321.695, 360.031, 321.695, 362.199);
        ((GeneralPath) shape).lineTo(321.695, 396.084);
        ((GeneralPath) shape).curveTo(321.695, 398.252, 323.455, 400.013, 325.623, 400.013);
        ((GeneralPath) shape).lineTo(334.658, 400.013);
        ((GeneralPath) shape).curveTo(336.826, 400.013, 338.587, 398.252, 338.587, 396.084);
        ((GeneralPath) shape).lineTo(338.587, 362.199);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        g.setPaint(WHITE);
        g.setStroke(new BasicStroke(2.5f, 0, 0, 4));
        g.draw(shape);

        // _0_2_19
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(488.554, 371.076);
        ((GeneralPath) shape).curveTo(488.554, 368.908, 486.794, 367.147, 484.626, 367.147);
        ((GeneralPath) shape).lineTo(475.591, 367.147);
        ((GeneralPath) shape).curveTo(473.423, 367.147, 471.662, 368.908, 471.662, 371.076);
        ((GeneralPath) shape).lineTo(471.662, 404.961);
        ((GeneralPath) shape).curveTo(471.662, 407.129, 473.423, 408.889, 475.591, 408.889);
        ((GeneralPath) shape).lineTo(484.626, 408.889);
        ((GeneralPath) shape).curveTo(486.794, 408.889, 488.554, 407.129, 488.554, 404.961);
        ((GeneralPath) shape).lineTo(488.554, 371.076);
        ((GeneralPath) shape).closePath();

        g.draw(shape);

        // _0_2_20
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(638.205, 362.199);
        ((GeneralPath) shape).curveTo(638.205, 360.031, 636.445, 358.271, 634.276, 358.271);
        ((GeneralPath) shape).lineTo(625.242, 358.271);
        ((GeneralPath) shape).curveTo(623.074, 358.271, 621.313, 360.031, 621.313, 362.199);
        ((GeneralPath) shape).lineTo(621.313, 396.084);
        ((GeneralPath) shape).curveTo(621.313, 398.252, 623.074, 400.013, 625.242, 400.013);
        ((GeneralPath) shape).lineTo(634.276, 400.013);
        ((GeneralPath) shape).curveTo(636.445, 400.013, 638.205, 398.252, 638.205, 396.084);
        ((GeneralPath) shape).lineTo(638.205, 362.199);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        g.setPaint(WHITE);
        g.draw(shape);

        // _0_2_21
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(335.358, 753.084);
        ((GeneralPath) shape).lineTo(83.208, 753.084);
        ((GeneralPath) shape).lineTo(83.208, 778.545);
        ((GeneralPath) shape).lineTo(79.208, 778.545);
        ((GeneralPath) shape).lineTo(79.208, 749.084);
        ((GeneralPath) shape).lineTo(335.358, 749.084);
        ((GeneralPath) shape).lineTo(335.358, 753.084);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);

        // _0_2_22
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(474.323, 584.554);
        ((GeneralPath) shape).lineTo(427.745, 584.554);
        ((GeneralPath) shape).lineTo(427.745, 641.913);
        ((GeneralPath) shape).lineTo(346.074, 641.913);
        ((GeneralPath) shape).lineTo(346.074, 637.913);
        ((GeneralPath) shape).lineTo(423.745, 637.913);
        ((GeneralPath) shape).lineTo(423.745, 580.554);
        ((GeneralPath) shape).lineTo(474.323, 580.554);
        ((GeneralPath) shape).lineTo(474.323, 584.554);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_23
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(111.208, 652.967);
        ((GeneralPath) shape).lineTo(249.916, 652.967);
        ((GeneralPath) shape).lineTo(249.916, 705.6);
        ((GeneralPath) shape).lineTo(245.916, 705.6);
        ((GeneralPath) shape).lineTo(245.916, 656.967);
        ((GeneralPath) shape).lineTo(107.208, 656.967);
        ((GeneralPath) shape).lineTo(107.208, 630.08);
        ((GeneralPath) shape).lineTo(111.208, 630.08);
        ((GeneralPath) shape).lineTo(111.208, 652.967);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_24
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(474.323, 622.554);
        ((GeneralPath) shape).lineTo(437.745, 622.554);
        ((GeneralPath) shape).lineTo(437.745, 718.842);
        ((GeneralPath) shape).lineTo(346.074, 718.842);
        ((GeneralPath) shape).lineTo(346.074, 714.842);
        ((GeneralPath) shape).lineTo(433.745, 714.842);
        ((GeneralPath) shape).lineTo(433.745, 618.554);
        ((GeneralPath) shape).lineTo(474.323, 618.554);
        ((GeneralPath) shape).lineTo(474.323, 622.554);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_25
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(335.358, 762.72);
        ((GeneralPath) shape).lineTo(215.231, 762.72);
        ((GeneralPath) shape).lineTo(215.231, 778.472);
        ((GeneralPath) shape).lineTo(211.231, 778.472);
        ((GeneralPath) shape).lineTo(211.231, 758.72);
        ((GeneralPath) shape).lineTo(335.358, 758.72);
        ((GeneralPath) shape).lineTo(335.358, 762.72);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_26
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(231.227, 640.952);
        ((GeneralPath) shape).lineTo(259.517, 640.952);
        ((GeneralPath) shape).lineTo(259.517, 705.6);
        ((GeneralPath) shape).lineTo(255.517, 705.6);
        ((GeneralPath) shape).lineTo(255.517, 644.952);
        ((GeneralPath) shape).lineTo(227.227, 644.952);
        ((GeneralPath) shape).lineTo(227.227, 630.08);
        ((GeneralPath) shape).lineTo(231.227, 630.08);
        ((GeneralPath) shape).lineTo(231.227, 640.952);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_27
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(474.323, 660.142);
        ((GeneralPath) shape).lineTo(447.347, 660.142);
        ((GeneralPath) shape).lineTo(447.347, 729.312);
        ((GeneralPath) shape).lineTo(346.074, 729.312);
        ((GeneralPath) shape).lineTo(346.074, 725.312);
        ((GeneralPath) shape).lineTo(443.347, 725.312);
        ((GeneralPath) shape).lineTo(443.347, 656.142);
        ((GeneralPath) shape).lineTo(474.323, 656.142);
        ((GeneralPath) shape).lineTo(474.323, 660.142);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_28
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(461.39, 783.585);
        ((GeneralPath) shape).lineTo(457.39, 783.585);
        ((GeneralPath) shape).lineTo(457.39, 758.123);
        ((GeneralPath) shape).lineTo(346.846, 758.123);
        ((GeneralPath) shape).lineTo(346.846, 754.123);
        ((GeneralPath) shape).lineTo(461.39, 754.123);
        ((GeneralPath) shape).lineTo(461.39, 783.585);
        ((GeneralPath) shape).closePath();

        g.setPaint(RED);
        g.fill(shape);

        // _0_2_29
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(469.379, 874.662);
        ((GeneralPath) shape).lineTo(465.379, 874.662);
        ((GeneralPath) shape).lineTo(465.379, 849.2);
        ((GeneralPath) shape).lineTo(346.846, 849.2);
        ((GeneralPath) shape).lineTo(346.846, 845.2);
        ((GeneralPath) shape).lineTo(469.379, 845.2);
        ((GeneralPath) shape).lineTo(469.379, 874.662);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 458.43f, 814.371f));

        // _0_2_30

        // _0_2_30_0

        // _0_2_30_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-25.861, -2.088);
        ((GeneralPath) shape).lineTo(-25.861, 0.243);
        ((GeneralPath) shape).lineTo(-39.384, 0.243);
        ((GeneralPath) shape).lineTo(-39.384, -18.694);
        ((GeneralPath) shape).lineTo(-36.932, -18.694);
        ((GeneralPath) shape).lineTo(-36.932, -2.088);
        ((GeneralPath) shape).lineTo(-25.861, -2.088);
        ((GeneralPath) shape).closePath();

        g.setPaint(RED);
        g.fill(shape);

        // _0_2_30_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-17.546, -16.751);
        ((GeneralPath) shape).lineTo(-21.637, -16.751);
        ((GeneralPath) shape).lineTo(-21.637, -18.694);
        ((GeneralPath) shape).lineTo(-10.967, -18.694);
        ((GeneralPath) shape).lineTo(-10.967, -16.751);
        ((GeneralPath) shape).lineTo(-15.094, -16.751);
        ((GeneralPath) shape).lineTo(-15.094, -1.869);
        ((GeneralPath) shape).lineTo(-10.967, -1.869);
        ((GeneralPath) shape).lineTo(-10.967, 0.243);
        ((GeneralPath) shape).lineTo(-21.637, 0.243);
        ((GeneralPath) shape).lineTo(-21.637, -1.869);
        ((GeneralPath) shape).lineTo(-17.546, -1.869);
        ((GeneralPath) shape).lineTo(-17.546, -16.751);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_30_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-6.355, -18.766);
        ((GeneralPath) shape).lineTo(2.944, -18.766);
        ((GeneralPath) shape).lineTo(6.403, -15.283);
        ((GeneralPath) shape).lineTo(6.403, -3.192);
        ((GeneralPath) shape).lineTo(2.944, 0.291);
        ((GeneralPath) shape).lineTo(-6.355, 0.291);
        ((GeneralPath) shape).lineTo(-6.355, -18.766);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-3.988, -16.448);
        ((GeneralPath) shape).lineTo(-3.988, -1.966);
        ((GeneralPath) shape).lineTo(1.851, -1.966);
        ((GeneralPath) shape).lineTo(3.927, -4.079);
        ((GeneralPath) shape).lineTo(3.927, -14.336);
        ((GeneralPath) shape).lineTo(1.851, -16.448);
        ((GeneralPath) shape).lineTo(-3.988, -16.448);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_30_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(16.454, -15.538);
        ((GeneralPath) shape).lineTo(11.683, -7.623);
        ((GeneralPath) shape).lineTo(20.945, -7.623);
        ((GeneralPath) shape).lineTo(16.454, -15.538);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(15.434, -18.694);
        ((GeneralPath) shape).lineTo(17.219, -18.694);
        ((GeneralPath) shape).lineTo(23.567, -7.259);
        ((GeneralPath) shape).lineTo(23.567, 0.243);
        ((GeneralPath) shape).lineTo(20.945, 0.243);
        ((GeneralPath) shape).lineTo(20.945, -5.292);
        ((GeneralPath) shape).lineTo(11.683, -5.292);
        ((GeneralPath) shape).lineTo(11.683, 0.243);
        ((GeneralPath) shape).lineTo(9.062, 0.243);
        ((GeneralPath) shape).lineTo(9.062, -7.198);
        ((GeneralPath) shape).lineTo(15.434, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_30_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(25.704, -18.694);
        ((GeneralPath) shape).lineTo(35.767, -18.694);
        ((GeneralPath) shape).lineTo(39.226, -15.234);
        ((GeneralPath) shape).lineTo(39.226, -11.52);
        ((GeneralPath) shape).lineTo(35.767, -8.06);
        ((GeneralPath) shape).lineTo(34.698, -8.06);
        ((GeneralPath) shape).lineTo(39.651, -0.607);
        ((GeneralPath) shape).lineTo(37.648, 0.716);
        ((GeneralPath) shape).lineTo(31.797, -8.06);
        ((GeneralPath) shape).lineTo(28.156, -8.06);
        ((GeneralPath) shape).lineTo(28.156, 0.243);
        ((GeneralPath) shape).lineTo(25.704, 0.243);
        ((GeneralPath) shape).lineTo(25.704, -18.694);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(28.156, -16.363);
        ((GeneralPath) shape).lineTo(28.156, -10.391);
        ((GeneralPath) shape).lineTo(34.698, -10.391);
        ((GeneralPath) shape).lineTo(36.774, -12.466);
        ((GeneralPath) shape).lineTo(36.774, -14.287);
        ((GeneralPath) shape).lineTo(34.698, -16.363);
        ((GeneralPath) shape).lineTo(28.156, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_30
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 228.096f, 617.786f));

        // _0_2_31

        // _0_2_31_0

        // _0_2_31_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.384, -18.694);
        ((GeneralPath) shape).lineTo(-29.321, -18.694);
        ((GeneralPath) shape).lineTo(-25.861, -15.234);
        ((GeneralPath) shape).lineTo(-25.861, -11.52);
        ((GeneralPath) shape).lineTo(-29.321, -8.06);
        ((GeneralPath) shape).lineTo(-36.932, -8.06);
        ((GeneralPath) shape).lineTo(-36.932, 0.243);
        ((GeneralPath) shape).lineTo(-39.384, 0.243);
        ((GeneralPath) shape).lineTo(-39.384, -18.694);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-36.932, -16.363);
        ((GeneralPath) shape).lineTo(-36.932, -10.391);
        ((GeneralPath) shape).lineTo(-30.389, -10.391);
        ((GeneralPath) shape).lineTo(-28.314, -12.466);
        ((GeneralPath) shape).lineTo(-28.314, -14.287);
        ((GeneralPath) shape).lineTo(-30.389, -16.363);
        ((GeneralPath) shape).lineTo(-36.932, -16.363);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_2_31_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-17.546, -16.751);
        ((GeneralPath) shape).lineTo(-21.637, -16.751);
        ((GeneralPath) shape).lineTo(-21.637, -18.694);
        ((GeneralPath) shape).lineTo(-10.967, -18.694);
        ((GeneralPath) shape).lineTo(-10.967, -16.751);
        ((GeneralPath) shape).lineTo(-15.094, -16.751);
        ((GeneralPath) shape).lineTo(-15.094, -1.869);
        ((GeneralPath) shape).lineTo(-10.967, -1.869);
        ((GeneralPath) shape).lineTo(-10.967, 0.243);
        ((GeneralPath) shape).lineTo(-21.637, 0.243);
        ((GeneralPath) shape).lineTo(-21.637, -1.869);
        ((GeneralPath) shape).lineTo(-17.546, -1.869);
        ((GeneralPath) shape).lineTo(-17.546, -16.751);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_31_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-1.196, 0.243);
        ((GeneralPath) shape).lineTo(-1.196, -16.363);
        ((GeneralPath) shape).lineTo(-6.731, -16.363);
        ((GeneralPath) shape).lineTo(-6.731, -18.694);
        ((GeneralPath) shape).lineTo(6.792, -18.694);
        ((GeneralPath) shape).lineTo(6.792, -16.363);
        ((GeneralPath) shape).lineTo(1.256, -16.363);
        ((GeneralPath) shape).lineTo(1.256, 0.243);
        ((GeneralPath) shape).lineTo(-1.196, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_31_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(23.118, -3.217);
        ((GeneralPath) shape).lineTo(19.659, 0.243);
        ((GeneralPath) shape).lineTo(13.055, 0.243);
        ((GeneralPath) shape).lineTo(9.596, -3.217);
        ((GeneralPath) shape).lineTo(9.596, -15.234);
        ((GeneralPath) shape).lineTo(13.055, -18.694);
        ((GeneralPath) shape).lineTo(19.659, -18.694);
        ((GeneralPath) shape).lineTo(23.118, -15.234);
        ((GeneralPath) shape).lineTo(23.118, -3.217);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(14.123, -16.363);
        ((GeneralPath) shape).lineTo(12.048, -14.287);
        ((GeneralPath) shape).lineTo(12.048, -4.164);
        ((GeneralPath) shape).lineTo(14.123, -2.088);
        ((GeneralPath) shape).lineTo(18.59, -2.088);
        ((GeneralPath) shape).lineTo(20.666, -4.164);
        ((GeneralPath) shape).lineTo(20.666, -14.287);
        ((GeneralPath) shape).lineTo(18.59, -16.363);
        ((GeneralPath) shape).lineTo(14.123, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_31_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(31.457, 0.243);
        ((GeneralPath) shape).lineTo(31.457, -16.363);
        ((GeneralPath) shape).lineTo(25.922, -16.363);
        ((GeneralPath) shape).lineTo(25.922, -18.694);
        ((GeneralPath) shape).lineTo(39.445, -18.694);
        ((GeneralPath) shape).lineTo(39.445, -16.363);
        ((GeneralPath) shape).lineTo(33.909, -16.363);
        ((GeneralPath) shape).lineTo(33.909, 0.243);
        ((GeneralPath) shape).lineTo(31.457, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_31
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 108.934f, 617.786f));

        // _0_2_32

        // _0_2_32_0

        // _0_2_32_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-44.088, 0.243);
        ((GeneralPath) shape).lineTo(-47.365, -3.035);
        ((GeneralPath) shape).lineTo(-45.666, -4.734);
        ((GeneralPath) shape).lineTo(-43.02, -2.088);
        ((GeneralPath) shape).lineTo(-38.552, -2.088);
        ((GeneralPath) shape).lineTo(-36.477, -4.164);
        ((GeneralPath) shape).lineTo(-36.477, -5.984);
        ((GeneralPath) shape).lineTo(-38.552, -8.06);
        ((GeneralPath) shape).lineTo(-44.088, -8.06);
        ((GeneralPath) shape).lineTo(-47.547, -11.52);
        ((GeneralPath) shape).lineTo(-47.547, -15.234);
        ((GeneralPath) shape).lineTo(-44.088, -18.694);
        ((GeneralPath) shape).lineTo(-37.484, -18.694);
        ((GeneralPath) shape).lineTo(-34.207, -15.416);
        ((GeneralPath) shape).lineTo(-35.906, -13.717);
        ((GeneralPath) shape).lineTo(-38.552, -16.363);
        ((GeneralPath) shape).lineTo(-43.02, -16.363);
        ((GeneralPath) shape).lineTo(-45.095, -14.287);
        ((GeneralPath) shape).lineTo(-45.095, -12.466);
        ((GeneralPath) shape).lineTo(-43.02, -10.391);
        ((GeneralPath) shape).lineTo(-37.484, -10.391);
        ((GeneralPath) shape).lineTo(-34.025, -6.931);
        ((GeneralPath) shape).lineTo(-34.025, -3.217);
        ((GeneralPath) shape).lineTo(-37.484, 0.243);
        ((GeneralPath) shape).lineTo(-44.088, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_32_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-25.685, 0.243);
        ((GeneralPath) shape).lineTo(-25.685, -16.363);
        ((GeneralPath) shape).lineTo(-31.221, -16.363);
        ((GeneralPath) shape).lineTo(-31.221, -18.694);
        ((GeneralPath) shape).lineTo(-17.698, -18.694);
        ((GeneralPath) shape).lineTo(-17.698, -16.363);
        ((GeneralPath) shape).lineTo(-23.233, -16.363);
        ((GeneralPath) shape).lineTo(-23.233, 0.243);
        ((GeneralPath) shape).lineTo(-25.685, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_32_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.036, -15.538);
        ((GeneralPath) shape).lineTo(-12.806, -7.623);
        ((GeneralPath) shape).lineTo(-3.544, -7.623);
        ((GeneralPath) shape).lineTo(-8.036, -15.538);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-9.055, -18.694);
        ((GeneralPath) shape).lineTo(-7.271, -18.694);
        ((GeneralPath) shape).lineTo(-0.923, -7.259);
        ((GeneralPath) shape).lineTo(-0.923, 0.243);
        ((GeneralPath) shape).lineTo(-3.544, 0.243);
        ((GeneralPath) shape).lineTo(-3.544, -5.292);
        ((GeneralPath) shape).lineTo(-12.806, -5.292);
        ((GeneralPath) shape).lineTo(-12.806, 0.243);
        ((GeneralPath) shape).lineTo(-15.428, 0.243);
        ((GeneralPath) shape).lineTo(-15.428, -7.198);
        ((GeneralPath) shape).lineTo(-9.055, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_32_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.968, 0.243);
        ((GeneralPath) shape).lineTo(6.968, -16.363);
        ((GeneralPath) shape).lineTo(1.432, -16.363);
        ((GeneralPath) shape).lineTo(1.432, -18.694);
        ((GeneralPath) shape).lineTo(14.955, -18.694);
        ((GeneralPath) shape).lineTo(14.955, -16.363);
        ((GeneralPath) shape).lineTo(9.42, -16.363);
        ((GeneralPath) shape).lineTo(9.42, 0.243);
        ((GeneralPath) shape).lineTo(6.968, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_32_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(23.27, -16.751);
        ((GeneralPath) shape).lineTo(19.179, -16.751);
        ((GeneralPath) shape).lineTo(19.179, -18.694);
        ((GeneralPath) shape).lineTo(29.849, -18.694);
        ((GeneralPath) shape).lineTo(29.849, -16.751);
        ((GeneralPath) shape).lineTo(25.722, -16.751);
        ((GeneralPath) shape).lineTo(25.722, -1.869);
        ((GeneralPath) shape).lineTo(29.849, -1.869);
        ((GeneralPath) shape).lineTo(29.849, 0.243);
        ((GeneralPath) shape).lineTo(19.179, 0.243);
        ((GeneralPath) shape).lineTo(19.179, -1.869);
        ((GeneralPath) shape).lineTo(23.27, -1.869);
        ((GeneralPath) shape).lineTo(23.27, -16.751);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_32_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(45.824, -13.717);
        ((GeneralPath) shape).lineTo(43.177, -16.363);
        ((GeneralPath) shape).lineTo(38.71, -16.363);
        ((GeneralPath) shape).lineTo(36.635, -14.287);
        ((GeneralPath) shape).lineTo(36.635, -4.164);
        ((GeneralPath) shape).lineTo(38.71, -2.088);
        ((GeneralPath) shape).lineTo(43.177, -2.088);
        ((GeneralPath) shape).lineTo(45.824, -4.734);
        ((GeneralPath) shape).lineTo(47.523, -3.035);
        ((GeneralPath) shape).lineTo(44.246, 0.243);
        ((GeneralPath) shape).lineTo(37.642, 0.243);
        ((GeneralPath) shape).lineTo(34.183, -3.217);
        ((GeneralPath) shape).lineTo(34.183, -15.234);
        ((GeneralPath) shape).lineTo(37.642, -18.694);
        ((GeneralPath) shape).lineTo(44.246, -18.694);
        ((GeneralPath) shape).lineTo(47.523, -15.416);
        ((GeneralPath) shape).lineTo(45.824, -13.717);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_32
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 516.998f, 666.702f));

        // _0_2_33

        // _0_2_33_0

        // _0_2_33_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-19.58, -13.717);
        ((GeneralPath) shape).lineTo(-22.226, -16.363);
        ((GeneralPath) shape).lineTo(-26.693, -16.363);
        ((GeneralPath) shape).lineTo(-28.769, -14.287);
        ((GeneralPath) shape).lineTo(-28.769, -4.164);
        ((GeneralPath) shape).lineTo(-26.693, -2.088);
        ((GeneralPath) shape).lineTo(-22.226, -2.088);
        ((GeneralPath) shape).lineTo(-20.15, -4.164);
        ((GeneralPath) shape).lineTo(-20.15, -8.06);
        ((GeneralPath) shape).lineTo(-25.685, -8.06);
        ((GeneralPath) shape).lineTo(-25.685, -10.391);
        ((GeneralPath) shape).lineTo(-17.698, -10.391);
        ((GeneralPath) shape).lineTo(-17.698, -3.217);
        ((GeneralPath) shape).lineTo(-21.158, 0.243);
        ((GeneralPath) shape).lineTo(-27.761, 0.243);
        ((GeneralPath) shape).lineTo(-31.221, -3.217);
        ((GeneralPath) shape).lineTo(-31.221, -15.234);
        ((GeneralPath) shape).lineTo(-27.761, -18.694);
        ((GeneralPath) shape).lineTo(-21.158, -18.694);
        ((GeneralPath) shape).lineTo(-17.88, -15.416);
        ((GeneralPath) shape).lineTo(-19.58, -13.717);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_33_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-4.904, -18.499);
        ((GeneralPath) shape).lineTo(-2.476, -18.499);
        ((GeneralPath) shape).lineTo(-2.476, -13.316);
        ((GeneralPath) shape).lineTo(-6.919, -8.873);
        ((GeneralPath) shape).lineTo(-6.919, 0.243);
        ((GeneralPath) shape).lineTo(-9.468, 0.243);
        ((GeneralPath) shape).lineTo(-9.468, -8.873);
        ((GeneralPath) shape).lineTo(-13.79, -13.316);
        ((GeneralPath) shape).lineTo(-13.79, -18.499);
        ((GeneralPath) shape).lineTo(-11.386, -18.499);
        ((GeneralPath) shape).lineTo(-11.386, -14.749);
        ((GeneralPath) shape).lineTo(-8.194, -11.41);
        ((GeneralPath) shape).lineTo(-4.904, -14.749);
        ((GeneralPath) shape).lineTo(-4.904, -18.499);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_33_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(1.214, -18.694);
        ((GeneralPath) shape).lineTo(11.277, -18.694);
        ((GeneralPath) shape).lineTo(14.736, -15.234);
        ((GeneralPath) shape).lineTo(14.736, -11.52);
        ((GeneralPath) shape).lineTo(11.277, -8.06);
        ((GeneralPath) shape).lineTo(10.209, -8.06);
        ((GeneralPath) shape).lineTo(15.161, -0.607);
        ((GeneralPath) shape).lineTo(13.158, 0.716);
        ((GeneralPath) shape).lineTo(7.307, -8.06);
        ((GeneralPath) shape).lineTo(3.666, -8.06);
        ((GeneralPath) shape).lineTo(3.666, 0.243);
        ((GeneralPath) shape).lineTo(1.214, 0.243);
        ((GeneralPath) shape).lineTo(1.214, -18.694);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(3.666, -16.363);
        ((GeneralPath) shape).lineTo(3.666, -10.391);
        ((GeneralPath) shape).lineTo(10.209, -10.391);
        ((GeneralPath) shape).lineTo(12.284, -12.466);
        ((GeneralPath) shape).lineTo(12.284, -14.287);
        ((GeneralPath) shape).lineTo(10.209, -16.363);
        ((GeneralPath) shape).lineTo(3.666, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_33_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(31.281, -3.217);
        ((GeneralPath) shape).lineTo(27.822, 0.243);
        ((GeneralPath) shape).lineTo(21.218, 0.243);
        ((GeneralPath) shape).lineTo(17.759, -3.217);
        ((GeneralPath) shape).lineTo(17.759, -15.234);
        ((GeneralPath) shape).lineTo(21.218, -18.694);
        ((GeneralPath) shape).lineTo(27.822, -18.694);
        ((GeneralPath) shape).lineTo(31.281, -15.234);
        ((GeneralPath) shape).lineTo(31.281, -3.217);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(22.287, -16.363);
        ((GeneralPath) shape).lineTo(20.211, -14.287);
        ((GeneralPath) shape).lineTo(20.211, -4.164);
        ((GeneralPath) shape).lineTo(22.287, -2.088);
        ((GeneralPath) shape).lineTo(26.754, -2.088);
        ((GeneralPath) shape).lineTo(28.829, -4.164);
        ((GeneralPath) shape).lineTo(28.829, -14.287);
        ((GeneralPath) shape).lineTo(26.754, -16.363);
        ((GeneralPath) shape).lineTo(22.287, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_33
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 508.426f, 628.77f));

        // _0_2_34

        // _0_2_34_0

        // _0_2_34_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-11.416, -13.717);
        ((GeneralPath) shape).lineTo(-14.063, -16.363);
        ((GeneralPath) shape).lineTo(-18.53, -16.363);
        ((GeneralPath) shape).lineTo(-20.605, -14.287);
        ((GeneralPath) shape).lineTo(-20.605, -4.164);
        ((GeneralPath) shape).lineTo(-18.53, -2.088);
        ((GeneralPath) shape).lineTo(-14.063, -2.088);
        ((GeneralPath) shape).lineTo(-11.987, -4.164);
        ((GeneralPath) shape).lineTo(-11.987, -8.06);
        ((GeneralPath) shape).lineTo(-17.522, -8.06);
        ((GeneralPath) shape).lineTo(-17.522, -10.391);
        ((GeneralPath) shape).lineTo(-9.535, -10.391);
        ((GeneralPath) shape).lineTo(-9.535, -3.217);
        ((GeneralPath) shape).lineTo(-12.994, 0.243);
        ((GeneralPath) shape).lineTo(-19.598, 0.243);
        ((GeneralPath) shape).lineTo(-23.057, -3.217);
        ((GeneralPath) shape).lineTo(-23.057, -15.234);
        ((GeneralPath) shape).lineTo(-19.598, -18.694);
        ((GeneralPath) shape).lineTo(-12.994, -18.694);
        ((GeneralPath) shape).lineTo(-9.717, -15.416);
        ((GeneralPath) shape).lineTo(-11.416, -13.717);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_34_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-6.731, -18.694);
        ((GeneralPath) shape).lineTo(3.332, -18.694);
        ((GeneralPath) shape).lineTo(6.792, -15.234);
        ((GeneralPath) shape).lineTo(6.792, -11.52);
        ((GeneralPath) shape).lineTo(3.332, -8.06);
        ((GeneralPath) shape).lineTo(-4.279, -8.06);
        ((GeneralPath) shape).lineTo(-4.279, 0.243);
        ((GeneralPath) shape).lineTo(-6.731, 0.243);
        ((GeneralPath) shape).lineTo(-6.731, -18.694);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-4.279, -16.363);
        ((GeneralPath) shape).lineTo(-4.279, -10.391);
        ((GeneralPath) shape).lineTo(2.264, -10.391);
        ((GeneralPath) shape).lineTo(4.34, -12.466);
        ((GeneralPath) shape).lineTo(4.34, -14.287);
        ((GeneralPath) shape).lineTo(2.264, -16.363);
        ((GeneralPath) shape).lineTo(-4.279, -16.363);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_34_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(13.055, 0.243);
        ((GeneralPath) shape).lineTo(9.778, -3.035);
        ((GeneralPath) shape).lineTo(11.477, -4.734);
        ((GeneralPath) shape).lineTo(14.123, -2.088);
        ((GeneralPath) shape).lineTo(18.59, -2.088);
        ((GeneralPath) shape).lineTo(20.666, -4.164);
        ((GeneralPath) shape).lineTo(20.666, -5.984);
        ((GeneralPath) shape).lineTo(18.59, -8.06);
        ((GeneralPath) shape).lineTo(13.055, -8.06);
        ((GeneralPath) shape).lineTo(9.596, -11.52);
        ((GeneralPath) shape).lineTo(9.596, -15.234);
        ((GeneralPath) shape).lineTo(13.055, -18.694);
        ((GeneralPath) shape).lineTo(19.659, -18.694);
        ((GeneralPath) shape).lineTo(22.936, -15.416);
        ((GeneralPath) shape).lineTo(21.237, -13.717);
        ((GeneralPath) shape).lineTo(18.59, -16.363);
        ((GeneralPath) shape).lineTo(14.123, -16.363);
        ((GeneralPath) shape).lineTo(12.048, -14.287);
        ((GeneralPath) shape).lineTo(12.048, -12.466);
        ((GeneralPath) shape).lineTo(14.123, -10.391);
        ((GeneralPath) shape).lineTo(19.659, -10.391);
        ((GeneralPath) shape).lineTo(23.118, -6.931);
        ((GeneralPath) shape).lineTo(23.118, -3.217);
        ((GeneralPath) shape).lineTo(19.659, 0.243);
        ((GeneralPath) shape).lineTo(13.055, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_34
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 467.276f, 904.591f));

        // _0_2_35

        // _0_2_35_0

        // _0_2_35_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-11.319, -13.717);
        ((GeneralPath) shape).lineTo(-13.966, -16.363);
        ((GeneralPath) shape).lineTo(-18.433, -16.363);
        ((GeneralPath) shape).lineTo(-20.508, -14.287);
        ((GeneralPath) shape).lineTo(-20.508, -4.164);
        ((GeneralPath) shape).lineTo(-18.433, -2.088);
        ((GeneralPath) shape).lineTo(-13.966, -2.088);
        ((GeneralPath) shape).lineTo(-11.319, -4.734);
        ((GeneralPath) shape).lineTo(-9.62, -3.035);
        ((GeneralPath) shape).lineTo(-12.897, 0.243);
        ((GeneralPath) shape).lineTo(-19.501, 0.243);
        ((GeneralPath) shape).lineTo(-22.96, -3.217);
        ((GeneralPath) shape).lineTo(-22.96, -15.234);
        ((GeneralPath) shape).lineTo(-19.501, -18.694);
        ((GeneralPath) shape).lineTo(-12.897, -18.694);
        ((GeneralPath) shape).lineTo(-9.62, -15.416);
        ((GeneralPath) shape).lineTo(-11.319, -13.717);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_35_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(0.127, -15.538);
        ((GeneralPath) shape).lineTo(-4.643, -7.623);
        ((GeneralPath) shape).lineTo(4.619, -7.623);
        ((GeneralPath) shape).lineTo(0.127, -15.538);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-0.892, -18.694);
        ((GeneralPath) shape).lineTo(0.892, -18.694);
        ((GeneralPath) shape).lineTo(7.241, -7.259);
        ((GeneralPath) shape).lineTo(7.241, 0.243);
        ((GeneralPath) shape).lineTo(4.619, 0.243);
        ((GeneralPath) shape).lineTo(4.619, -5.292);
        ((GeneralPath) shape).lineTo(-4.643, -5.292);
        ((GeneralPath) shape).lineTo(-4.643, 0.243);
        ((GeneralPath) shape).lineTo(-7.265, 0.243);
        ((GeneralPath) shape).lineTo(-7.265, -7.198);
        ((GeneralPath) shape).lineTo(-0.892, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_35_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(9.596, -18.694);
        ((GeneralPath) shape).lineTo(11.501, -18.694);
        ((GeneralPath) shape).lineTo(16.357, -11.386);
        ((GeneralPath) shape).lineTo(21.212, -18.694);
        ((GeneralPath) shape).lineTo(23.118, -18.694);
        ((GeneralPath) shape).lineTo(23.118, 0.243);
        ((GeneralPath) shape).lineTo(20.666, 0.243);
        ((GeneralPath) shape).lineTo(20.666, -13.51);
        ((GeneralPath) shape).lineTo(17.304, -8.473);
        ((GeneralPath) shape).lineTo(15.41, -8.473);
        ((GeneralPath) shape).lineTo(12.048, -13.51);
        ((GeneralPath) shape).lineTo(12.048, 0.243);
        ((GeneralPath) shape).lineTo(9.596, 0.243);
        ((GeneralPath) shape).lineTo(9.596, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_35
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 516.283f, 590.27f));

        // _0_2_36

        // _0_2_36_0

        // _0_2_36_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-25.685, 0.243);
        ((GeneralPath) shape).lineTo(-25.685, -16.363);
        ((GeneralPath) shape).lineTo(-31.221, -16.363);
        ((GeneralPath) shape).lineTo(-31.221, -18.694);
        ((GeneralPath) shape).lineTo(-17.698, -18.694);
        ((GeneralPath) shape).lineTo(-17.698, -16.363);
        ((GeneralPath) shape).lineTo(-23.233, -16.363);
        ((GeneralPath) shape).lineTo(-23.233, 0.243);
        ((GeneralPath) shape).lineTo(-25.685, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_36_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.036, -15.538);
        ((GeneralPath) shape).lineTo(-12.806, -7.623);
        ((GeneralPath) shape).lineTo(-3.544, -7.623);
        ((GeneralPath) shape).lineTo(-8.036, -15.538);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-9.055, -18.694);
        ((GeneralPath) shape).lineTo(-7.271, -18.694);
        ((GeneralPath) shape).lineTo(-0.923, -7.259);
        ((GeneralPath) shape).lineTo(-0.923, 0.243);
        ((GeneralPath) shape).lineTo(-3.544, 0.243);
        ((GeneralPath) shape).lineTo(-3.544, -5.292);
        ((GeneralPath) shape).lineTo(-12.806, -5.292);
        ((GeneralPath) shape).lineTo(-12.806, 0.243);
        ((GeneralPath) shape).lineTo(-15.428, 0.243);
        ((GeneralPath) shape).lineTo(-15.428, -7.198);
        ((GeneralPath) shape).lineTo(-9.055, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_36_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(13.17, -13.717);
        ((GeneralPath) shape).lineTo(10.524, -16.363);
        ((GeneralPath) shape).lineTo(6.057, -16.363);
        ((GeneralPath) shape).lineTo(3.981, -14.287);
        ((GeneralPath) shape).lineTo(3.981, -4.164);
        ((GeneralPath) shape).lineTo(6.057, -2.088);
        ((GeneralPath) shape).lineTo(10.524, -2.088);
        ((GeneralPath) shape).lineTo(13.17, -4.734);
        ((GeneralPath) shape).lineTo(14.87, -3.035);
        ((GeneralPath) shape).lineTo(11.592, 0.243);
        ((GeneralPath) shape).lineTo(4.989, 0.243);
        ((GeneralPath) shape).lineTo(1.529, -3.217);
        ((GeneralPath) shape).lineTo(1.529, -15.234);
        ((GeneralPath) shape).lineTo(4.989, -18.694);
        ((GeneralPath) shape).lineTo(11.592, -18.694);
        ((GeneralPath) shape).lineTo(14.87, -15.416);
        ((GeneralPath) shape).lineTo(13.17, -13.717);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_36_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.759, -18.694);
        ((GeneralPath) shape).lineTo(20.211, -18.694);
        ((GeneralPath) shape).lineTo(20.211, -10.391);
        ((GeneralPath) shape).lineTo(28.829, -10.391);
        ((GeneralPath) shape).lineTo(28.829, -18.694);
        ((GeneralPath) shape).lineTo(31.281, -18.694);
        ((GeneralPath) shape).lineTo(31.281, 0.243);
        ((GeneralPath) shape).lineTo(28.829, 0.243);
        ((GeneralPath) shape).lineTo(28.829, -8.06);
        ((GeneralPath) shape).lineTo(20.211, -8.06);
        ((GeneralPath) shape).lineTo(20.211, 0.243);
        ((GeneralPath) shape).lineTo(17.759, 0.243);
        ((GeneralPath) shape).lineTo(17.759, -18.694);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_36

        // _0_2_37
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(330.981, 543.406);
        ((GeneralPath) shape).curveTo(326.545, 552.216, 319.808, 561.149, 311.829, 569.149);

        g.setPaint(RED);
        g.draw(shape);

        // _0_2_38
        shape = new Rectangle2D.Double(745.8670043945312, 779.5880126953125, 172.28399658203125, 43.415000915527344);
        g.setPaint(new Color(0x00E100));
        g.fill(shape);

        // _0_2_39
        shape = new Rectangle2D.Double(745.8670043945312, 458.2430114746094, 172.28399658203125, 43.415000915527344);
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 832.342f, 489.006f));

        // _0_2_40

        // _0_2_40_0

        // _0_2_40_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-38.572, -14.729);
        ((GeneralPath) shape).lineTo(-43.095, -7.227);
        ((GeneralPath) shape).lineTo(-34.315, -7.227);
        ((GeneralPath) shape).lineTo(-38.572, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-39.539, -17.721);
        ((GeneralPath) shape).lineTo(-37.847, -17.721);
        ((GeneralPath) shape).lineTo(-31.829, -6.881);
        ((GeneralPath) shape).lineTo(-31.829, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, -6.824);
        ((GeneralPath) shape).lineTo(-39.539, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_2_40_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-18.469, -13.003);
        ((GeneralPath) shape).lineTo(-20.978, -15.512);
        ((GeneralPath) shape).lineTo(-25.212, -15.512);
        ((GeneralPath) shape).lineTo(-27.18, -13.544);
        ((GeneralPath) shape).lineTo(-27.18, -3.947);
        ((GeneralPath) shape).lineTo(-25.212, -1.979);
        ((GeneralPath) shape).lineTo(-20.978, -1.979);
        ((GeneralPath) shape).lineTo(-18.469, -4.488);
        ((GeneralPath) shape).lineTo(-16.858, -2.877);
        ((GeneralPath) shape).lineTo(-19.965, 0.23);
        ((GeneralPath) shape).lineTo(-26.225, 0.23);
        ((GeneralPath) shape).lineTo(-29.505, -3.049);
        ((GeneralPath) shape).lineTo(-29.505, -14.442);
        ((GeneralPath) shape).lineTo(-26.225, -17.721);
        ((GeneralPath) shape).lineTo(-19.965, -17.721);
        ((GeneralPath) shape).lineTo(-16.858, -14.614);
        ((GeneralPath) shape).lineTo(-18.469, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_40_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.872, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_40_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.582, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -1.772);
        ((GeneralPath) shape).lineTo(12.819, -1.772);
        ((GeneralPath) shape).lineTo(12.819, 0.23);
        ((GeneralPath) shape).lineTo(2.704, 0.23);
        ((GeneralPath) shape).lineTo(2.704, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_40_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.514, -10.0);
        ((GeneralPath) shape).lineTo(17.503, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -10.759);
        ((GeneralPath) shape).lineTo(23.176, -3.015);
        ((GeneralPath) shape).lineTo(26.72, -10.759);
        ((GeneralPath) shape).lineTo(26.72, -17.537);
        ((GeneralPath) shape).lineTo(28.975, -17.537);
        ((GeneralPath) shape).lineTo(28.964, -9.988);
        ((GeneralPath) shape).lineTo(23.993, 0.23);
        ((GeneralPath) shape).lineTo(22.359, 0.23);
        ((GeneralPath) shape).lineTo(17.514, -10.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_40_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(34.637, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -1.979);
        ((GeneralPath) shape).lineTo(45.131, -1.979);
        ((GeneralPath) shape).lineTo(45.131, 0.23);
        ((GeneralPath) shape).lineTo(32.312, 0.23);
        ((GeneralPath) shape).lineTo(32.312, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_40

        // _0_2_41
        shape = new Rectangle2D.Double(745.8670043945312, 522.5120239257812, 172.28399658203125, 43.415000915527344);
        g.setPaint(new Color(0xE1E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 832.342f, 553.275f));

        // _0_2_42

        // _0_2_42_0

        // _0_2_42_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-30.834, -14.729);
        ((GeneralPath) shape).lineTo(-35.356, -7.227);
        ((GeneralPath) shape).lineTo(-26.576, -7.227);
        ((GeneralPath) shape).lineTo(-30.834, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-31.8, -17.721);
        ((GeneralPath) shape).lineTo(-30.109, -17.721);
        ((GeneralPath) shape).lineTo(-24.09, -6.881);
        ((GeneralPath) shape).lineTo(-24.09, 0.23);
        ((GeneralPath) shape).lineTo(-26.576, 0.23);
        ((GeneralPath) shape).lineTo(-26.576, -5.017);
        ((GeneralPath) shape).lineTo(-35.356, -5.017);
        ((GeneralPath) shape).lineTo(-35.356, 0.23);
        ((GeneralPath) shape).lineTo(-37.842, 0.23);
        ((GeneralPath) shape).lineTo(-37.842, -6.824);
        ((GeneralPath) shape).lineTo(-31.8, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_2_42_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-22.065, -17.721);
        ((GeneralPath) shape).lineTo(-12.526, -17.721);
        ((GeneralPath) shape).lineTo(-9.246, -14.442);
        ((GeneralPath) shape).lineTo(-9.246, -10.92);
        ((GeneralPath) shape).lineTo(-12.526, -7.641);
        ((GeneralPath) shape).lineTo(-13.538, -7.641);
        ((GeneralPath) shape).lineTo(-8.843, -0.575);
        ((GeneralPath) shape).lineTo(-10.742, 0.679);
        ((GeneralPath) shape).lineTo(-16.288, -7.641);
        ((GeneralPath) shape).lineTo(-19.741, -7.641);
        ((GeneralPath) shape).lineTo(-19.741, 0.23);
        ((GeneralPath) shape).lineTo(-22.065, 0.23);
        ((GeneralPath) shape).lineTo(-22.065, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-19.741, -15.512);
        ((GeneralPath) shape).lineTo(-19.741, -9.85);
        ((GeneralPath) shape).lineTo(-13.538, -9.85);
        ((GeneralPath) shape).lineTo(-11.571, -11.818);
        ((GeneralPath) shape).lineTo(-11.571, -13.544);
        ((GeneralPath) shape).lineTo(-13.538, -15.512);
        ((GeneralPath) shape).lineTo(-19.741, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_42_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-6.381, -17.721);
        ((GeneralPath) shape).lineTo(-4.574, -17.721);
        ((GeneralPath) shape).lineTo(0.029, -10.794);
        ((GeneralPath) shape).lineTo(4.632, -17.721);
        ((GeneralPath) shape).lineTo(6.438, -17.721);
        ((GeneralPath) shape).lineTo(6.438, 0.23);
        ((GeneralPath) shape).lineTo(4.114, 0.23);
        ((GeneralPath) shape).lineTo(4.114, -12.808);
        ((GeneralPath) shape).lineTo(0.926, -8.032);
        ((GeneralPath) shape).lineTo(-0.869, -8.032);
        ((GeneralPath) shape).lineTo(-4.056, -12.808);
        ((GeneralPath) shape).lineTo(-4.056, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_42_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(11.421, -15.512);
        ((GeneralPath) shape).lineTo(11.421, -9.85);
        ((GeneralPath) shape).lineTo(19.292, -9.85);
        ((GeneralPath) shape).lineTo(19.292, -7.641);
        ((GeneralPath) shape).lineTo(11.421, -7.641);
        ((GeneralPath) shape).lineTo(11.421, -1.979);
        ((GeneralPath) shape).lineTo(21.916, -1.979);
        ((GeneralPath) shape).lineTo(21.916, 0.23);
        ((GeneralPath) shape).lineTo(9.096, 0.23);
        ((GeneralPath) shape).lineTo(9.096, -17.721);
        ((GeneralPath) shape).lineTo(21.916, -17.721);
        ((GeneralPath) shape).lineTo(21.916, -15.512);
        ((GeneralPath) shape).lineTo(11.421, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_42_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(24.93, -17.79);
        ((GeneralPath) shape).lineTo(33.745, -17.79);
        ((GeneralPath) shape).lineTo(37.025, -14.488);
        ((GeneralPath) shape).lineTo(37.025, -3.026);
        ((GeneralPath) shape).lineTo(33.745, 0.276);
        ((GeneralPath) shape).lineTo(24.93, 0.276);
        ((GeneralPath) shape).lineTo(24.93, -17.79);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(27.174, -15.592);
        ((GeneralPath) shape).lineTo(27.174, -1.864);
        ((GeneralPath) shape).lineTo(32.709, -1.864);
        ((GeneralPath) shape).lineTo(34.677, -3.866);
        ((GeneralPath) shape).lineTo(34.677, -13.59);
        ((GeneralPath) shape).lineTo(32.709, -15.592);
        ((GeneralPath) shape).lineTo(27.174, -15.592);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_42

        // _0_2_43
        shape = new Rectangle2D.Double(745.8670043945312, 716.47900390625, 172.28399658203125, 43.415000915527344);
        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 832.342f, 747.241f));

        // _0_2_44

        // _0_2_44_0

        // _0_2_44_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-38.572, -14.729);
        ((GeneralPath) shape).lineTo(-43.095, -7.227);
        ((GeneralPath) shape).lineTo(-34.315, -7.227);
        ((GeneralPath) shape).lineTo(-38.572, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-39.539, -17.721);
        ((GeneralPath) shape).lineTo(-37.847, -17.721);
        ((GeneralPath) shape).lineTo(-31.829, -6.881);
        ((GeneralPath) shape).lineTo(-31.829, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, -6.824);
        ((GeneralPath) shape).lineTo(-39.539, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_2_44_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-18.469, -13.003);
        ((GeneralPath) shape).lineTo(-20.978, -15.512);
        ((GeneralPath) shape).lineTo(-25.212, -15.512);
        ((GeneralPath) shape).lineTo(-27.18, -13.544);
        ((GeneralPath) shape).lineTo(-27.18, -3.947);
        ((GeneralPath) shape).lineTo(-25.212, -1.979);
        ((GeneralPath) shape).lineTo(-20.978, -1.979);
        ((GeneralPath) shape).lineTo(-18.469, -4.488);
        ((GeneralPath) shape).lineTo(-16.858, -2.877);
        ((GeneralPath) shape).lineTo(-19.965, 0.23);
        ((GeneralPath) shape).lineTo(-26.225, 0.23);
        ((GeneralPath) shape).lineTo(-29.505, -3.049);
        ((GeneralPath) shape).lineTo(-29.505, -14.442);
        ((GeneralPath) shape).lineTo(-26.225, -17.721);
        ((GeneralPath) shape).lineTo(-19.965, -17.721);
        ((GeneralPath) shape).lineTo(-16.858, -14.614);
        ((GeneralPath) shape).lineTo(-18.469, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_44_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.872, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_44_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.582, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -1.772);
        ((GeneralPath) shape).lineTo(12.819, -1.772);
        ((GeneralPath) shape).lineTo(12.819, 0.23);
        ((GeneralPath) shape).lineTo(2.704, 0.23);
        ((GeneralPath) shape).lineTo(2.704, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_44_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.514, -10.0);
        ((GeneralPath) shape).lineTo(17.503, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -10.759);
        ((GeneralPath) shape).lineTo(23.176, -3.015);
        ((GeneralPath) shape).lineTo(26.72, -10.759);
        ((GeneralPath) shape).lineTo(26.72, -17.537);
        ((GeneralPath) shape).lineTo(28.975, -17.537);
        ((GeneralPath) shape).lineTo(28.964, -9.988);
        ((GeneralPath) shape).lineTo(23.993, 0.23);
        ((GeneralPath) shape).lineTo(22.359, 0.23);
        ((GeneralPath) shape).lineTo(17.514, -10.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_44_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(34.637, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -1.979);
        ((GeneralPath) shape).lineTo(45.131, -1.979);
        ((GeneralPath) shape).lineTo(45.131, 0.23);
        ((GeneralPath) shape).lineTo(32.312, 0.23);
        ((GeneralPath) shape).lineTo(32.312, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_44

        // _0_2_45
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(918.634, 697.602);
        ((GeneralPath) shape).lineTo(745.385, 697.602);
        ((GeneralPath) shape).lineTo(745.385, 651.728);
        ((GeneralPath) shape).lineTo(918.634, 651.728);
        ((GeneralPath) shape).lineTo(918.634, 697.602);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(748.385, 654.728);
        ((GeneralPath) shape).lineTo(748.385, 694.602);
        ((GeneralPath) shape).lineTo(915.634, 694.602);
        ((GeneralPath) shape).lineTo(915.634, 654.728);
        ((GeneralPath) shape).lineTo(748.385, 654.728);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 832.342f, 683.719f));

        // _0_2_46

        // _0_2_46_0

        // _0_2_46_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-9.039, -3.049);
        ((GeneralPath) shape).lineTo(-12.318, 0.23);
        ((GeneralPath) shape).lineTo(-18.578, 0.23);
        ((GeneralPath) shape).lineTo(-21.858, -3.049);
        ((GeneralPath) shape).lineTo(-21.858, -14.442);
        ((GeneralPath) shape).lineTo(-18.578, -17.721);
        ((GeneralPath) shape).lineTo(-12.318, -17.721);
        ((GeneralPath) shape).lineTo(-9.039, -14.442);
        ((GeneralPath) shape).lineTo(-9.039, -3.049);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-17.566, -15.512);
        ((GeneralPath) shape).lineTo(-19.534, -13.544);
        ((GeneralPath) shape).lineTo(-19.534, -3.947);
        ((GeneralPath) shape).lineTo(-17.566, -1.979);
        ((GeneralPath) shape).lineTo(-13.331, -1.979);
        ((GeneralPath) shape).lineTo(-11.363, -3.947);
        ((GeneralPath) shape).lineTo(-11.363, -13.544);
        ((GeneralPath) shape).lineTo(-13.331, -15.512);
        ((GeneralPath) shape).lineTo(-17.566, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_46_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-4.056, -15.512);
        ((GeneralPath) shape).lineTo(-4.056, -9.85);
        ((GeneralPath) shape).lineTo(3.815, -9.85);
        ((GeneralPath) shape).lineTo(3.815, -7.641);
        ((GeneralPath) shape).lineTo(-4.056, -7.641);
        ((GeneralPath) shape).lineTo(-4.056, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, -17.721);
        ((GeneralPath) shape).lineTo(6.438, -17.721);
        ((GeneralPath) shape).lineTo(6.438, -15.512);
        ((GeneralPath) shape).lineTo(-4.056, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_46_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(11.421, -15.512);
        ((GeneralPath) shape).lineTo(11.421, -9.85);
        ((GeneralPath) shape).lineTo(19.292, -9.85);
        ((GeneralPath) shape).lineTo(19.292, -7.641);
        ((GeneralPath) shape).lineTo(11.421, -7.641);
        ((GeneralPath) shape).lineTo(11.421, 0.23);
        ((GeneralPath) shape).lineTo(9.096, 0.23);
        ((GeneralPath) shape).lineTo(9.096, -17.721);
        ((GeneralPath) shape).lineTo(21.916, -17.721);
        ((GeneralPath) shape).lineTo(21.916, -15.512);
        ((GeneralPath) shape).lineTo(11.421, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_46
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 832.342f, 810.351f));

        // _0_2_47

        // _0_2_47_0

        // _0_2_47_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-38.572, -14.729);
        ((GeneralPath) shape).lineTo(-43.095, -7.227);
        ((GeneralPath) shape).lineTo(-34.315, -7.227);
        ((GeneralPath) shape).lineTo(-38.572, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-39.539, -17.721);
        ((GeneralPath) shape).lineTo(-37.847, -17.721);
        ((GeneralPath) shape).lineTo(-31.829, -6.881);
        ((GeneralPath) shape).lineTo(-31.829, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, -6.824);
        ((GeneralPath) shape).lineTo(-39.539, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_2_47_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-18.469, -13.003);
        ((GeneralPath) shape).lineTo(-20.978, -15.512);
        ((GeneralPath) shape).lineTo(-25.212, -15.512);
        ((GeneralPath) shape).lineTo(-27.18, -13.544);
        ((GeneralPath) shape).lineTo(-27.18, -3.947);
        ((GeneralPath) shape).lineTo(-25.212, -1.979);
        ((GeneralPath) shape).lineTo(-20.978, -1.979);
        ((GeneralPath) shape).lineTo(-18.469, -4.488);
        ((GeneralPath) shape).lineTo(-16.858, -2.877);
        ((GeneralPath) shape).lineTo(-19.965, 0.23);
        ((GeneralPath) shape).lineTo(-26.225, 0.23);
        ((GeneralPath) shape).lineTo(-29.505, -3.049);
        ((GeneralPath) shape).lineTo(-29.505, -14.442);
        ((GeneralPath) shape).lineTo(-26.225, -17.721);
        ((GeneralPath) shape).lineTo(-19.965, -17.721);
        ((GeneralPath) shape).lineTo(-16.858, -14.614);
        ((GeneralPath) shape).lineTo(-18.469, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_47_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.872, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_47_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.582, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -1.772);
        ((GeneralPath) shape).lineTo(12.819, -1.772);
        ((GeneralPath) shape).lineTo(12.819, 0.23);
        ((GeneralPath) shape).lineTo(2.704, 0.23);
        ((GeneralPath) shape).lineTo(2.704, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_47_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.514, -10.0);
        ((GeneralPath) shape).lineTo(17.503, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -10.759);
        ((GeneralPath) shape).lineTo(23.176, -3.015);
        ((GeneralPath) shape).lineTo(26.72, -10.759);
        ((GeneralPath) shape).lineTo(26.72, -17.537);
        ((GeneralPath) shape).lineTo(28.975, -17.537);
        ((GeneralPath) shape).lineTo(28.964, -9.988);
        ((GeneralPath) shape).lineTo(23.993, 0.23);
        ((GeneralPath) shape).lineTo(22.359, 0.23);
        ((GeneralPath) shape).lineTo(17.514, -10.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_47_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(34.637, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -1.979);
        ((GeneralPath) shape).lineTo(45.131, -1.979);
        ((GeneralPath) shape).lineTo(45.131, 0.23);
        ((GeneralPath) shape).lineTo(32.312, 0.23);
        ((GeneralPath) shape).lineTo(32.312, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_47

        // _0_2_48
        shape = new Rectangle2D.Double(745.8670043945312, 843.8569946289062, 172.28399658203125, 43.415000915527344);
        g.setPaint(new Color(0x00E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 832.342f, 874.62f));

        // _0_2_49

        // _0_2_49_0

        // _0_2_49_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-38.572, -14.729);
        ((GeneralPath) shape).lineTo(-43.095, -7.227);
        ((GeneralPath) shape).lineTo(-34.315, -7.227);
        ((GeneralPath) shape).lineTo(-38.572, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-39.539, -17.721);
        ((GeneralPath) shape).lineTo(-37.847, -17.721);
        ((GeneralPath) shape).lineTo(-31.829, -6.881);
        ((GeneralPath) shape).lineTo(-31.829, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, 0.23);
        ((GeneralPath) shape).lineTo(-34.315, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, -5.017);
        ((GeneralPath) shape).lineTo(-43.095, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, 0.23);
        ((GeneralPath) shape).lineTo(-45.58, -6.824);
        ((GeneralPath) shape).lineTo(-39.539, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_2_49_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-18.469, -13.003);
        ((GeneralPath) shape).lineTo(-20.978, -15.512);
        ((GeneralPath) shape).lineTo(-25.212, -15.512);
        ((GeneralPath) shape).lineTo(-27.18, -13.544);
        ((GeneralPath) shape).lineTo(-27.18, -3.947);
        ((GeneralPath) shape).lineTo(-25.212, -1.979);
        ((GeneralPath) shape).lineTo(-20.978, -1.979);
        ((GeneralPath) shape).lineTo(-18.469, -4.488);
        ((GeneralPath) shape).lineTo(-16.858, -2.877);
        ((GeneralPath) shape).lineTo(-19.965, 0.23);
        ((GeneralPath) shape).lineTo(-26.225, 0.23);
        ((GeneralPath) shape).lineTo(-29.505, -3.049);
        ((GeneralPath) shape).lineTo(-29.505, -14.442);
        ((GeneralPath) shape).lineTo(-26.225, -17.721);
        ((GeneralPath) shape).lineTo(-19.965, -17.721);
        ((GeneralPath) shape).lineTo(-16.858, -14.614);
        ((GeneralPath) shape).lineTo(-18.469, -13.003);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_49_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-8.872, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -15.512);
        ((GeneralPath) shape).lineTo(-14.119, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -17.721);
        ((GeneralPath) shape).lineTo(-1.3, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, -15.512);
        ((GeneralPath) shape).lineTo(-6.548, 0.23);
        ((GeneralPath) shape).lineTo(-8.872, 0.23);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_49_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(6.582, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -15.88);
        ((GeneralPath) shape).lineTo(2.704, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -17.721);
        ((GeneralPath) shape).lineTo(12.819, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -15.88);
        ((GeneralPath) shape).lineTo(8.907, -1.772);
        ((GeneralPath) shape).lineTo(12.819, -1.772);
        ((GeneralPath) shape).lineTo(12.819, 0.23);
        ((GeneralPath) shape).lineTo(2.704, 0.23);
        ((GeneralPath) shape).lineTo(2.704, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -1.772);
        ((GeneralPath) shape).lineTo(6.582, -15.88);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_49_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.514, -10.0);
        ((GeneralPath) shape).lineTo(17.503, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -17.56);
        ((GeneralPath) shape).lineTo(19.666, -10.759);
        ((GeneralPath) shape).lineTo(23.176, -3.015);
        ((GeneralPath) shape).lineTo(26.72, -10.759);
        ((GeneralPath) shape).lineTo(26.72, -17.537);
        ((GeneralPath) shape).lineTo(28.975, -17.537);
        ((GeneralPath) shape).lineTo(28.964, -9.988);
        ((GeneralPath) shape).lineTo(23.993, 0.23);
        ((GeneralPath) shape).lineTo(22.359, 0.23);
        ((GeneralPath) shape).lineTo(17.514, -10.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_49_0_5
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(34.637, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -9.85);
        ((GeneralPath) shape).lineTo(42.508, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -7.641);
        ((GeneralPath) shape).lineTo(34.637, -1.979);
        ((GeneralPath) shape).lineTo(45.131, -1.979);
        ((GeneralPath) shape).lineTo(45.131, 0.23);
        ((GeneralPath) shape).lineTo(32.312, 0.23);
        ((GeneralPath) shape).lineTo(32.312, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -17.721);
        ((GeneralPath) shape).lineTo(45.131, -15.512);
        ((GeneralPath) shape).lineTo(34.637, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_49

        // _0_2_50
        shape = new Rectangle2D.Double(745.8670043945312, 586.781005859375, 172.28399658203125, 43.415000915527344);
        g.setPaint(new Color(0xE1E100));
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.00006f, 0, 0, 0.999935f, 832.342f, 617.544f));

        // _0_2_51

        // _0_2_51_0

        // _0_2_51_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-30.834, -14.729);
        ((GeneralPath) shape).lineTo(-35.356, -7.227);
        ((GeneralPath) shape).lineTo(-26.576, -7.227);
        ((GeneralPath) shape).lineTo(-30.834, -14.729);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-31.8, -17.721);
        ((GeneralPath) shape).lineTo(-30.109, -17.721);
        ((GeneralPath) shape).lineTo(-24.09, -6.881);
        ((GeneralPath) shape).lineTo(-24.09, 0.23);
        ((GeneralPath) shape).lineTo(-26.576, 0.23);
        ((GeneralPath) shape).lineTo(-26.576, -5.017);
        ((GeneralPath) shape).lineTo(-35.356, -5.017);
        ((GeneralPath) shape).lineTo(-35.356, 0.23);
        ((GeneralPath) shape).lineTo(-37.842, 0.23);
        ((GeneralPath) shape).lineTo(-37.842, -6.824);
        ((GeneralPath) shape).lineTo(-31.8, -17.721);
        ((GeneralPath) shape).closePath();

        g.setPaint(BLACK);
        g.fill(shape);

        // _0_2_51_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-22.065, -17.721);
        ((GeneralPath) shape).lineTo(-12.526, -17.721);
        ((GeneralPath) shape).lineTo(-9.246, -14.442);
        ((GeneralPath) shape).lineTo(-9.246, -10.92);
        ((GeneralPath) shape).lineTo(-12.526, -7.641);
        ((GeneralPath) shape).lineTo(-13.538, -7.641);
        ((GeneralPath) shape).lineTo(-8.843, -0.575);
        ((GeneralPath) shape).lineTo(-10.742, 0.679);
        ((GeneralPath) shape).lineTo(-16.288, -7.641);
        ((GeneralPath) shape).lineTo(-19.741, -7.641);
        ((GeneralPath) shape).lineTo(-19.741, 0.23);
        ((GeneralPath) shape).lineTo(-22.065, 0.23);
        ((GeneralPath) shape).lineTo(-22.065, -17.721);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-19.741, -15.512);
        ((GeneralPath) shape).lineTo(-19.741, -9.85);
        ((GeneralPath) shape).lineTo(-13.538, -9.85);
        ((GeneralPath) shape).lineTo(-11.571, -11.818);
        ((GeneralPath) shape).lineTo(-11.571, -13.544);
        ((GeneralPath) shape).lineTo(-13.538, -15.512);
        ((GeneralPath) shape).lineTo(-19.741, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_51_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-6.381, -17.721);
        ((GeneralPath) shape).lineTo(-4.574, -17.721);
        ((GeneralPath) shape).lineTo(0.029, -10.794);
        ((GeneralPath) shape).lineTo(4.632, -17.721);
        ((GeneralPath) shape).lineTo(6.438, -17.721);
        ((GeneralPath) shape).lineTo(6.438, 0.23);
        ((GeneralPath) shape).lineTo(4.114, 0.23);
        ((GeneralPath) shape).lineTo(4.114, -12.808);
        ((GeneralPath) shape).lineTo(0.926, -8.032);
        ((GeneralPath) shape).lineTo(-0.869, -8.032);
        ((GeneralPath) shape).lineTo(-4.056, -12.808);
        ((GeneralPath) shape).lineTo(-4.056, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, 0.23);
        ((GeneralPath) shape).lineTo(-6.381, -17.721);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_51_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(11.421, -15.512);
        ((GeneralPath) shape).lineTo(11.421, -9.85);
        ((GeneralPath) shape).lineTo(19.292, -9.85);
        ((GeneralPath) shape).lineTo(19.292, -7.641);
        ((GeneralPath) shape).lineTo(11.421, -7.641);
        ((GeneralPath) shape).lineTo(11.421, -1.979);
        ((GeneralPath) shape).lineTo(21.916, -1.979);
        ((GeneralPath) shape).lineTo(21.916, 0.23);
        ((GeneralPath) shape).lineTo(9.096, 0.23);
        ((GeneralPath) shape).lineTo(9.096, -17.721);
        ((GeneralPath) shape).lineTo(21.916, -17.721);
        ((GeneralPath) shape).lineTo(21.916, -15.512);
        ((GeneralPath) shape).lineTo(11.421, -15.512);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_51_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(24.93, -17.79);
        ((GeneralPath) shape).lineTo(33.745, -17.79);
        ((GeneralPath) shape).lineTo(37.025, -14.488);
        ((GeneralPath) shape).lineTo(37.025, -3.026);
        ((GeneralPath) shape).lineTo(33.745, 0.276);
        ((GeneralPath) shape).lineTo(24.93, 0.276);
        ((GeneralPath) shape).lineTo(24.93, -17.79);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(27.174, -15.592);
        ((GeneralPath) shape).lineTo(27.174, -1.864);
        ((GeneralPath) shape).lineTo(32.709, -1.864);
        ((GeneralPath) shape).lineTo(34.677, -3.866);
        ((GeneralPath) shape).lineTo(34.677, -13.59);
        ((GeneralPath) shape).lineTo(32.709, -15.592);
        ((GeneralPath) shape).lineTo(27.174, -15.592);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_51
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 80.9568f, 843.332f));

        // _0_2_52

        // _0_2_52_0

        // _0_2_52_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-27.646, 0.243);
        ((GeneralPath) shape).lineTo(-41.168, 0.243);
        ((GeneralPath) shape).lineTo(-41.168, -2.998);
        ((GeneralPath) shape).lineTo(-30.098, -12.466);
        ((GeneralPath) shape).lineTo(-30.098, -14.287);
        ((GeneralPath) shape).lineTo(-32.174, -16.363);
        ((GeneralPath) shape).lineTo(-36.641, -16.363);
        ((GeneralPath) shape).lineTo(-39.287, -13.717);
        ((GeneralPath) shape).lineTo(-40.986, -15.416);
        ((GeneralPath) shape).lineTo(-37.709, -18.694);
        ((GeneralPath) shape).lineTo(-31.105, -18.694);
        ((GeneralPath) shape).lineTo(-27.646, -15.234);
        ((GeneralPath) shape).lineTo(-27.646, -11.52);
        ((GeneralPath) shape).lineTo(-38.668, -2.088);
        ((GeneralPath) shape).lineTo(-27.646, -2.088);
        ((GeneralPath) shape).lineTo(-27.646, 0.243);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_2_52_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-20.472, -4.151);
        ((GeneralPath) shape).lineTo(-18.906, -4.151);
        ((GeneralPath) shape).lineTo(-16.842, -2.088);
        ((GeneralPath) shape).lineTo(-12.837, -2.088);
        ((GeneralPath) shape).lineTo(-10.773, -4.164);
        ((GeneralPath) shape).lineTo(-10.773, -8.084);
        ((GeneralPath) shape).lineTo(-18.396, -8.072);
        ((GeneralPath) shape).lineTo(-20.326, -9.99);
        ((GeneralPath) shape).lineTo(-20.326, -15.258);
        ((GeneralPath) shape).lineTo(-16.855, -18.718);
        ((GeneralPath) shape).lineTo(-11.781, -18.718);
        ((GeneralPath) shape).lineTo(-8.309, -15.258);
        ((GeneralPath) shape).lineTo(-8.309, -3.229);
        ((GeneralPath) shape).lineTo(-11.781, 0.243);
        ((GeneralPath) shape).lineTo(-18.287, 0.243);
        ((GeneralPath) shape).lineTo(-20.472, -1.845);
        ((GeneralPath) shape).lineTo(-20.472, -4.151);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-15.799, -16.387);
        ((GeneralPath) shape).lineTo(-17.886, -14.299);
        ((GeneralPath) shape).lineTo(-17.886, -10.391);
        ((GeneralPath) shape).lineTo(-10.773, -10.391);
        ((GeneralPath) shape).lineTo(-10.773, -14.299);
        ((GeneralPath) shape).lineTo(-12.837, -16.387);
        ((GeneralPath) shape).lineTo(-15.799, -16.387);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_52_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-2.361, 0.255);
        ((GeneralPath) shape).lineTo(-2.361, -4.37);
        ((GeneralPath) shape).lineTo(2.373, -4.37);
        ((GeneralPath) shape).lineTo(2.373, 0.255);
        ((GeneralPath) shape).lineTo(-2.361, 0.255);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-0.152, -2.27);
        ((GeneralPath) shape).lineTo(-0.152, -1.833);
        ((GeneralPath) shape).lineTo(0.164, -1.833);
        ((GeneralPath) shape).lineTo(0.164, -2.27);
        ((GeneralPath) shape).lineTo(-0.152, -2.27);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_52_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(8.212, -4.151);
        ((GeneralPath) shape).lineTo(9.778, -4.151);
        ((GeneralPath) shape).lineTo(11.841, -2.088);
        ((GeneralPath) shape).lineTo(15.847, -2.088);
        ((GeneralPath) shape).lineTo(17.911, -4.164);
        ((GeneralPath) shape).lineTo(17.911, -8.084);
        ((GeneralPath) shape).lineTo(10.288, -8.072);
        ((GeneralPath) shape).lineTo(8.357, -9.99);
        ((GeneralPath) shape).lineTo(8.357, -15.258);
        ((GeneralPath) shape).lineTo(11.829, -18.718);
        ((GeneralPath) shape).lineTo(16.903, -18.718);
        ((GeneralPath) shape).lineTo(20.375, -15.258);
        ((GeneralPath) shape).lineTo(20.375, -3.229);
        ((GeneralPath) shape).lineTo(16.903, 0.243);
        ((GeneralPath) shape).lineTo(10.397, 0.243);
        ((GeneralPath) shape).lineTo(8.212, -1.845);
        ((GeneralPath) shape).lineTo(8.212, -4.151);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(12.885, -16.387);
        ((GeneralPath) shape).lineTo(10.797, -14.299);
        ((GeneralPath) shape).lineTo(10.797, -10.391);
        ((GeneralPath) shape).lineTo(17.911, -10.391);
        ((GeneralPath) shape).lineTo(17.911, -14.299);
        ((GeneralPath) shape).lineTo(15.847, -16.387);
        ((GeneralPath) shape).lineTo(12.885, -16.387);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_52_0_4
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(41.071, 0.243);
        ((GeneralPath) shape).lineTo(27.549, 0.243);
        ((GeneralPath) shape).lineTo(27.549, -2.998);
        ((GeneralPath) shape).lineTo(38.619, -12.466);
        ((GeneralPath) shape).lineTo(38.619, -14.287);
        ((GeneralPath) shape).lineTo(36.544, -16.363);
        ((GeneralPath) shape).lineTo(32.077, -16.363);
        ((GeneralPath) shape).lineTo(29.43, -13.717);
        ((GeneralPath) shape).lineTo(27.731, -15.416);
        ((GeneralPath) shape).lineTo(31.008, -18.694);
        ((GeneralPath) shape).lineTo(37.612, -18.694);
        ((GeneralPath) shape).lineTo(41.071, -15.234);
        ((GeneralPath) shape).lineTo(41.071, -11.52);
        ((GeneralPath) shape).lineTo(30.049, -2.088);
        ((GeneralPath) shape).lineTo(41.071, -2.088);
        ((GeneralPath) shape).lineTo(41.071, 0.243);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_52
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.998987f, 0, 0, 1.00101f, 212.957f, 843.332f));

        // _0_2_53

        // _0_2_53_0

        // _0_2_53_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-5.948, -12.26);
        ((GeneralPath) shape).lineTo(-3.569, -9.869);
        ((GeneralPath) shape).lineTo(-3.569, -3.265);
        ((GeneralPath) shape).lineTo(-7.089, 0.243);
        ((GeneralPath) shape).lineTo(-11.872, 0.243);
        ((GeneralPath) shape).lineTo(-15.392, -3.277);
        ((GeneralPath) shape).lineTo(-15.392, -6.179);
        ((GeneralPath) shape).lineTo(-13.001, -6.179);
        ((GeneralPath) shape).lineTo(-13.001, -4.649);
        ((GeneralPath) shape).lineTo(-10.67, -2.294);
        ((GeneralPath) shape).lineTo(-8.291, -2.294);
        ((GeneralPath) shape).lineTo(-6.276, -4.309);
        ((GeneralPath) shape).lineTo(-6.276, -9.796);
        ((GeneralPath) shape).lineTo(-13.304, -9.796);
        ((GeneralPath) shape).lineTo(-15.392, -11.847);
        ((GeneralPath) shape).lineTo(-15.392, -18.924);
        ((GeneralPath) shape).lineTo(-3.484, -18.924);
        ((GeneralPath) shape).lineTo(-3.484, -16.387);
        ((GeneralPath) shape).lineTo(-12.685, -16.387);
        ((GeneralPath) shape).lineTo(-12.685, -12.26);
        ((GeneralPath) shape).lineTo(-5.948, -12.26);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_53_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(13.061, -12.26);
        ((GeneralPath) shape).lineTo(15.44, -9.869);
        ((GeneralPath) shape).lineTo(15.44, -3.265);
        ((GeneralPath) shape).lineTo(11.92, 0.243);
        ((GeneralPath) shape).lineTo(7.138, 0.243);
        ((GeneralPath) shape).lineTo(3.617, -3.277);
        ((GeneralPath) shape).lineTo(3.617, -6.179);
        ((GeneralPath) shape).lineTo(6.009, -6.179);
        ((GeneralPath) shape).lineTo(6.009, -4.649);
        ((GeneralPath) shape).lineTo(8.339, -2.294);
        ((GeneralPath) shape).lineTo(10.718, -2.294);
        ((GeneralPath) shape).lineTo(12.733, -4.309);
        ((GeneralPath) shape).lineTo(12.733, -9.796);
        ((GeneralPath) shape).lineTo(5.705, -9.796);
        ((GeneralPath) shape).lineTo(3.617, -11.847);
        ((GeneralPath) shape).lineTo(3.617, -18.924);
        ((GeneralPath) shape).lineTo(15.525, -18.924);
        ((GeneralPath) shape).lineTo(15.525, -16.387);
        ((GeneralPath) shape).lineTo(6.324, -16.387);
        ((GeneralPath) shape).lineTo(6.324, -12.26);
        ((GeneralPath) shape).lineTo(13.061, -12.26);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_53
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.03442f, 0, 0, 0.966729f, 181.827f, 913.216f));

        // _0_2_54

        // _0_2_54_0

        // _0_2_54_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(3.421, 0.0);
        ((GeneralPath) shape).lineTo(3.421, -1.644);
        ((GeneralPath) shape).lineTo(6.709, -1.644);
        ((GeneralPath) shape).lineTo(6.709, -14.618);
        ((GeneralPath) shape).lineTo(3.421, -13.796);
        ((GeneralPath) shape).lineTo(3.421, -15.485);
        ((GeneralPath) shape).lineTo(8.909, -16.851);
        ((GeneralPath) shape).lineTo(8.909, -1.644);
        ((GeneralPath) shape).lineTo(12.197, -1.644);
        ((GeneralPath) shape).lineTo(12.197, 0.0);
        ((GeneralPath) shape).lineTo(3.421, 0.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_54_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(17.806, 0.0);
        ((GeneralPath) shape).lineTo(17.806, -1.644);
        ((GeneralPath) shape).lineTo(21.094, -1.644);
        ((GeneralPath) shape).lineTo(21.094, -14.618);
        ((GeneralPath) shape).lineTo(17.806, -13.796);
        ((GeneralPath) shape).lineTo(17.806, -15.485);
        ((GeneralPath) shape).lineTo(23.293, -16.851);
        ((GeneralPath) shape).lineTo(23.293, -1.644);
        ((GeneralPath) shape).lineTo(26.581, -1.644);
        ((GeneralPath) shape).lineTo(26.581, 0.0);
        ((GeneralPath) shape).lineTo(17.806, 0.0);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_54_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(31.08, -0.122);
        ((GeneralPath) shape).lineTo(31.08, -2.166);
        ((GeneralPath) shape).curveTo(32.65, -1.544, 33.887, -1.233, 34.79, -1.233);
        ((GeneralPath) shape).curveTo(35.834, -1.233, 36.684, -1.538, 37.339, -2.149);
        ((GeneralPath) shape).curveTo(37.995, -2.76, 38.323, -3.551, 38.323, -4.521);
        ((GeneralPath) shape).curveTo(38.323, -6.831, 36.704, -7.987, 33.468, -7.987);
        ((GeneralPath) shape).lineTo(32.602, -7.987);
        ((GeneralPath) shape).lineTo(32.602, -9.464);
        ((GeneralPath) shape).lineTo(33.368, -9.475);
        ((GeneralPath) shape).curveTo(36.405, -9.475, 37.923, -10.545, 37.923, -12.685);
        ((GeneralPath) shape).curveTo(37.923, -14.366, 36.923, -15.207, 34.923, -15.207);
        ((GeneralPath) shape).curveTo(33.827, -15.207, 32.62, -14.899, 31.302, -14.285);
        ((GeneralPath) shape).lineTo(31.302, -16.195);
        ((GeneralPath) shape).curveTo(32.598, -16.632, 33.857, -16.851, 35.079, -16.851);
        ((GeneralPath) shape).curveTo(38.441, -16.851, 40.122, -15.581, 40.122, -13.041);
        ((GeneralPath) shape).curveTo(40.122, -11.108, 38.978, -9.731, 36.69, -8.909);
        ((GeneralPath) shape).curveTo(39.341, -8.294, 40.666, -6.843, 40.666, -4.554);
        ((GeneralPath) shape).curveTo(40.666, -3.007, 40.155, -1.792, 39.133, -0.911);
        ((GeneralPath) shape).curveTo(38.111, -0.03, 36.708, 0.411, 34.923, 0.411);
        ((GeneralPath) shape).curveTo(33.909, 0.411, 32.628, 0.233, 31.08, -0.122);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_54_0_3
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(51.719, 0.0);
        ((GeneralPath) shape).lineTo(51.719, -4.654);
        ((GeneralPath) shape).lineTo(44.321, -4.654);
        ((GeneralPath) shape).lineTo(44.321, -6.309);
        ((GeneralPath) shape).lineTo(51.719, -16.44);
        ((GeneralPath) shape).lineTo(53.774, -16.44);
        ((GeneralPath) shape).lineTo(53.774, -6.443);
        ((GeneralPath) shape).lineTo(55.973, -6.443);
        ((GeneralPath) shape).lineTo(55.973, -4.654);
        ((GeneralPath) shape).lineTo(53.774, -4.654);
        ((GeneralPath) shape).lineTo(53.774, 0.0);
        ((GeneralPath) shape).lineTo(51.719, 0.0);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(46.465, -6.443);
        ((GeneralPath) shape).lineTo(51.863, -6.443);
        ((GeneralPath) shape).lineTo(51.863, -13.752);
        ((GeneralPath) shape).lineTo(46.465, -6.443);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2_54

        // _0_2_55
        shape = new Rectangle2D.Double(50.08700180053711, 145.96499633789062, 3, 78.91200256347656);
        g.fill(shape);

        // _0_2_56
        shape = new Rectangle2D.Double(304.2049865722656, 159.81199645996094, 3, 79.31300354003906);
        g.fill(shape);

        // _0_2_57
        shape = new Rectangle2D.Double(440.47698974609375, 103.28199768066406, 79.48400115966797, 3);
        g.fill(shape);

        // _0_2_58
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(53.087, 259.448);
        ((GeneralPath) shape).lineTo(60.754, 259.448);
        ((GeneralPath) shape).lineTo(60.754, 262.448);
        ((GeneralPath) shape).lineTo(50.087, 262.448);
        ((GeneralPath) shape).lineTo(50.087, 224.277);
        ((GeneralPath) shape).lineTo(53.087, 224.277);
        ((GeneralPath) shape).lineTo(53.087, 259.448);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(60.754, 111.389);
        ((GeneralPath) shape).lineTo(53.087, 111.389);
        ((GeneralPath) shape).lineTo(53.087, 146.564);
        ((GeneralPath) shape).lineTo(50.087, 146.564);
        ((GeneralPath) shape).lineTo(50.087, 108.389);
        ((GeneralPath) shape).lineTo(60.754, 108.389);
        ((GeneralPath) shape).lineTo(60.754, 111.389);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x80FFFFFF, true));
        g.fill(shape);

        // _0_2_59
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(307.205, 273.495);
        ((GeneralPath) shape).lineTo(314.872, 273.495);
        ((GeneralPath) shape).lineTo(314.872, 276.495);
        ((GeneralPath) shape).lineTo(304.205, 276.495);
        ((GeneralPath) shape).lineTo(304.205, 238.325);
        ((GeneralPath) shape).lineTo(307.205, 238.325);
        ((GeneralPath) shape).lineTo(307.205, 273.495);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(314.872, 125.436);
        ((GeneralPath) shape).lineTo(307.205, 125.436);
        ((GeneralPath) shape).lineTo(307.205, 160.611);
        ((GeneralPath) shape).lineTo(304.205, 160.611);
        ((GeneralPath) shape).lineTo(304.205, 122.436);
        ((GeneralPath) shape).lineTo(314.872, 122.436);
        ((GeneralPath) shape).lineTo(314.872, 125.436);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_2_60
        shape = new Rectangle2D.Double(908.426025390625, 145.93499755859375, 3, 78.97200012207031);
        g.setPaint(WHITE);
        g.fill(shape);

        // _0_2_61
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(911.426, 262.448);
        ((GeneralPath) shape).lineTo(900.758, 262.448);
        ((GeneralPath) shape).lineTo(900.758, 259.448);
        ((GeneralPath) shape).lineTo(908.426, 259.448);
        ((GeneralPath) shape).lineTo(908.426, 224.277);
        ((GeneralPath) shape).lineTo(911.426, 224.277);
        ((GeneralPath) shape).lineTo(911.426, 262.448);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(911.426, 146.564);
        ((GeneralPath) shape).lineTo(908.426, 146.564);
        ((GeneralPath) shape).lineTo(908.426, 111.389);
        ((GeneralPath) shape).lineTo(900.758, 111.389);
        ((GeneralPath) shape).lineTo(900.758, 108.389);
        ((GeneralPath) shape).lineTo(911.426, 108.389);
        ((GeneralPath) shape).lineTo(911.426, 146.564);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x80FFFFFF, true));
        g.fill(shape);

        // _0_2_62
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(557.177, 113.959);
        ((GeneralPath) shape).lineTo(554.177, 113.959);
        ((GeneralPath) shape).lineTo(554.177, 106.282);
        ((GeneralPath) shape).lineTo(519.038, 106.282);
        ((GeneralPath) shape).lineTo(519.038, 103.282);
        ((GeneralPath) shape).lineTo(557.177, 103.282);
        ((GeneralPath) shape).lineTo(557.177, 113.959);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(441.4, 106.282);
        ((GeneralPath) shape).lineTo(406.265, 106.282);
        ((GeneralPath) shape).lineTo(406.265, 113.959);
        ((GeneralPath) shape).lineTo(403.265, 113.959);
        ((GeneralPath) shape).lineTo(403.265, 103.282);
        ((GeneralPath) shape).lineTo(441.4, 103.282);
        ((GeneralPath) shape).lineTo(441.4, 106.282);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_2

    }

    /**
     * Returns the X of the bounding box of the original SVG image.
     * 
     * @return The X of the bounding box of the original SVG image.
     */
    public static int getOrigX() {
        return 0;
    }

    /**
     * Returns the Y of the bounding box of the original SVG image.
     * 
     * @return The Y of the bounding box of the original SVG image.
     */
    public static int getOrigY() {
        return 0;
    }

    /**
     * Returns the width of the bounding box of the original SVG image.
     * 
     * @return The width of the bounding box of the original SVG image.
     */
    public static int getOrigWidth() {
        return 1;
    }

    /**
     * Returns the height of the bounding box of the original SVG image.
     * 
     * @return The height of the bounding box of the original SVG image.
     */
    public static int getOrigHeight() {
        return 1;
    }
}

