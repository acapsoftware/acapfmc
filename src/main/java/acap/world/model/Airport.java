/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import static processing.core.PApplet.loadStrings;

import acap.fmc.Constants;
import acap.world.utilities.Query;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import processing.core.PVector;

public class Airport extends BaseAeroObject {

  public String ident;
  public String country;
  public String state;
  public String city;
  public String name;
  public float alt;
  public float magvar;
  public PVector tower;
  public PVector location;
  public int longestRunway;
  public int usage = 1;
  public boolean removeMe = false;

  public HashMap<Integer, TaxiNode> taxiwayIndex = new HashMap<Integer, TaxiNode>();
  public HashMap<Integer, Parking> parkingIndex = new HashMap<Integer, Parking>();

  public ArrayList<Runway> runways = new ArrayList<Runway>();
  public ArrayList<TaxiNode> taxiNodes = new ArrayList<TaxiNode>();
  public ArrayList<TaxiPath> taxiPaths = new ArrayList<TaxiPath>();
  public ArrayList<Parking> parking = new ArrayList<Parking>();
  public ArrayList<Apron> aprons = new ArrayList<Apron>();
  public ArrayList<BoundaryFence> boundaryFences = new ArrayList<BoundaryFence>();

  public ArrayList<SID> SIDs = new ArrayList<SID>();
  public ArrayList<STAR> STARs = new ArrayList<STAR>();
  public ArrayList<Approach> approaches = new ArrayList<Approach>();

  public HashMap<String, ArrayList<String>> routes = new HashMap<String, ArrayList<String>>();

  public Query query;

  public Airport(String ident) {
    super(ident, ident, -1, -1);
    this.ident = ident;
    query = new Query(this);
  }

  public Airport(String ident, int usage) {
    super(ident, ident, -1, -1);
    this.ident = ident;
    this.usage = usage;
    query = new Query(this);
  }

  public void makeAirport(Query query) {

    query.parseInitialData();
    query.parseRunways();
    query.parseBoundaryFences();
    query.parseTaxiNodes();
    query.parseParking();
    query.parseAprons();
    query.parseTaxiPaths();

    location = new PVector((float) lat, (float) lon);

    longestRunway = 0;
    for (Runway runway : runways)
      if (runway.getLength() > longestRunway) longestRunway = (int) runway.getLength();

    loadApproaches();

    if (Constants.DEBUG)
      System.out.println(this.name + " (" + this.ident + ") was successfully created.");
  }

  public void loadApproaches() {
    String[] approachFile = new String[0];

    parseSIDS();
    int fileIndex;

    //LOAD THE STARS

    fileIndex = 0;

    while (fileIndex < approachFile.length
        && (!approachFile[fileIndex].split(" ")[0].equalsIgnoreCase("STARS")
            || approachFile[fileIndex].split(" ").length == 0)) fileIndex++;

    if (fileIndex >= approachFile.length) {
      if (Constants.DEBUG) System.out.println("File contained no STARSs for " + ident);
    } else {
      fileIndex++;

      //parse the STARS
      String starName = null;
      ArrayList<String> lines = new ArrayList();

      while (fileIndex < approachFile.length
          && approachFile[fileIndex].length() > 8
          && !approachFile[fileIndex].substring(0, 8).equalsIgnoreCase("ENDSTARS")) {

        while (fileIndex < approachFile.length
            && approachFile[fileIndex].length() > 4
            && !approachFile[fileIndex].substring(0, 4).equalsIgnoreCase("STAR")) {

          lines.add(approachFile[fileIndex]);
          fileIndex++;
        }

        //reset for new STAR
        if (starName != null) {
          STARs.add(new STAR(starName, lines));
          if (Constants.DEBUG) System.out.println("Added STAR " + starName + " to " + ident + ".");
        }

        if (fileIndex < approachFile.length) break;
        if (approachFile[fileIndex].split(" ").length > 1)
          starName = approachFile[fileIndex].split(" ")[1];
        lines = new ArrayList<String>();
        if (approachFile[fileIndex].length() > 13) lines.add(approachFile[fileIndex]);
        fileIndex++;
      }

      //check to see if there is still a STAR being processed
      if (lines.size() > 0)
        if (starName != null) {
          STARs.add(new STAR(starName, lines));
          if (Constants.DEBUG) System.out.println("Added STAR " + starName + " to " + ident + ".");
        }

      if (Constants.DEBUG) System.out.println("Reached end of STARs");
    }

    fileIndex = 0;

    while (fileIndex < approachFile.length
        && (!approachFile[fileIndex].split(" ")[0].equalsIgnoreCase("APPROACHES")
            || approachFile[fileIndex].split(" ").length == 0)) fileIndex++;

    if (fileIndex >= approachFile.length) {
      if (Constants.DEBUG) System.out.println("File contained no approaches for " + ident);
    } else {
      fileIndex++;

      //parse the APPROACHES
      String approachName = null;
      ArrayList<String> lines = new ArrayList();

      while (fileIndex < approachFile.length
          && approachFile[fileIndex].length() > 13
          && !approachFile[fileIndex].substring(0, 13).equalsIgnoreCase("ENDAPPROACHES")) {

        while (fileIndex < approachFile.length
            && approachFile[fileIndex].length() > 8
            && !approachFile[fileIndex].substring(0, 8).equalsIgnoreCase("APPROACH")) {

          lines.add(approachFile[fileIndex]);
          fileIndex++;
        }

        //reset for new APPROACH
        if (approachName != null) {
          STARs.add(new STAR(approachName, lines));
          if (Constants.DEBUG)
            System.out.println("Added Approach " + approachName + " to " + ident + ".");
          if (Constants.DEBUG) System.out.println("fileIndex " + fileIndex);
        }

        if (fileIndex < approachFile.length) break;
        if (approachFile[fileIndex].split(" ").length > 1)
          approachName = approachFile[fileIndex].split(" ")[1];
        lines = new ArrayList<String>();
        if (approachFile[fileIndex].length() > 13) lines.add(approachFile[fileIndex]);
        fileIndex++;
      }

      //check to see if there is still a APPROACH being processed
      if (lines.size() > 0)
        if (approachName != null) {
          approaches.add(new Approach(approachName, lines));
          if (Constants.DEBUG)
            System.out.println("Added Approach " + approachName + " to " + ident + ".");
          if (Constants.DEBUG) System.out.println("fileIndex " + fileIndex);
        }

      if (Constants.DEBUG) System.out.println("Reached end of Approaches");
    }

    if (Constants.DEBUG) System.out.println("Finished Loading Approaches for " + ident);
  }

  private String[] parseSIDS() {
    String[] approachFile = new String[0];
    File f = new File("./assets/DatabaseFiles/SIDSTARS/" + ident + ".txt");
    if (Constants.DEBUG)
      System.out.println("Searching for: " + "/DatabaseFiles/SIDSTARS/" + ident + ".txt");
    if (f.exists()) {
      if (Constants.DEBUG) System.out.println("Found an approach file for " + ident);

      approachFile = loadStrings(new File("./assets/DatabaseFiles/SIDSTARS/" + ident + ".txt"));

    } else {

      f = new File("./assets/DatabaseFiles/SIDSTARS/" + "K" + ident + ".txt");
      if (Constants.DEBUG)
        System.out.println(
            "Searching for: " + "assets/DatabaseFiles/SIDSTARS/" + "K" + ident + ".txt");
      if (f.exists()) {
        if (Constants.DEBUG) System.out.println("Found an approach file for " + ident);

        try {
          approachFile =
              Arrays.copyOf(
                  Files.readAllLines(f.toPath()).toArray(),
                  Files.readAllLines(f.toPath()).toArray().length,
                  String[].class);
        } catch (IOException e) {
          e.printStackTrace();
        }

      } else {
        if (Constants.DEBUG) System.out.println("No approach file for " + ident);
        return null;
      }
    }

    int fileIndex;

    fileIndex = 0;

    while (fileIndex < approachFile.length
        && (!approachFile[fileIndex].split(" ")[0].equalsIgnoreCase("SIDS")
            || approachFile[fileIndex].split(" ").length == 0)) fileIndex++;

    if (fileIndex >= approachFile.length) {
      if (Constants.DEBUG) System.out.println("File contained no SIDs for " + ident);
    } else {
      fileIndex++;

      //parse the SIDS
      String sidName = null;
      ArrayList<String> lines = new ArrayList();

      while (fileIndex < approachFile.length
          && approachFile[fileIndex].length() > 7
          && !approachFile[fileIndex].substring(0, 7).equalsIgnoreCase("ENDSIDS")) {

        while (fileIndex < approachFile.length
            && approachFile[fileIndex].length() > 3
            && !approachFile[fileIndex].substring(0, 3).equalsIgnoreCase("SID")) {

          lines.add(approachFile[fileIndex]);
          fileIndex++;
        }

        //reset for new SID
        if (sidName != null) {
          for (String line : lines) {
            if (!line.equalsIgnoreCase("ENDSIDS")) {
              SIDs.add(new SID(this, sidName, line));
              System.out.println(
                  "Added SID "
                      + SIDs.get(SIDs.size() - 1).name
                      + " to "
                      + ident
                      + " with "
                      + SIDs.get(SIDs.size() - 1).waypoints.size()
                      + " waypoints.");
            }
          }
        }

        if (fileIndex < approachFile.length) break;
        if (approachFile[fileIndex].split(" ").length > 1)
          sidName = approachFile[fileIndex].split(" ")[1];
        lines = new ArrayList<String>();
        if (approachFile[fileIndex].length() > 13) lines.add(approachFile[fileIndex]);
        fileIndex++;
      }

      //check to see if there is still a SID being processed
      if (lines.size() > 0)
        if (sidName != null) {
          for (String line : lines) {
            SIDs.add(new SID(this, sidName, line));
            System.out.println(
                "Added SID "
                    + SIDs.get(SIDs.size() - 1).name
                    + " to "
                    + ident
                    + " with "
                    + SIDs.get(SIDs.size() - 1).waypoints.size()
                    + "waypoints.");
          }
        }

      if (Constants.DEBUG) System.out.println("Reached end of SIDs");
    }
    return approachFile;
  }

  Runway getRunway(String designator) {

    Runway tempRunway = null;

    for (Runway runway : runways)
      if ((runway.getPrimaryNumber() + runway.getPrimaryDesignator()).equalsIgnoreCase(designator)
          || (runway.getSecondaryNumber() + runway.getSecondaryDesignator())
              .equalsIgnoreCase(designator)) tempRunway = runway;

    if (tempRunway == null)
      for (Runway runway : runways)
        if ((runway.getPrimaryNumber() + runway.getPrimaryDesignator())
                .equalsIgnoreCase(designator.substring(1, designator.length()))
            || (runway.getSecondaryNumber() + runway.getSecondaryDesignator())
                .equalsIgnoreCase(designator.substring(1, designator.length())))
          tempRunway = runway;

    return tempRunway;
  }

  Parking getFreeParking() {

    for (Parking parkingSpot : parking) if (!parkingSpot.occupied) return parkingSpot;
    return null;
  }

  Parking getFreeParking(float wingspan) {

    for (Parking parkingSpot : parking)
      if (!parkingSpot.occupied && parkingSpot.radius >= wingspan / 2) return parkingSpot;
    return null;
  }

  public Parking getNearestParking(float lat, float lon) {

    if (parking.size() != 0) {
      Parking closest = parking.get(0);

      for (Parking parkingSpot : parking)
        if (Math.sqrt(Math.pow(parkingSpot.lat - lat, 2) + Math.pow(parkingSpot.lon - lon, 2))
            < Math.sqrt(Math.pow(closest.lat - lat, 2) + Math.pow(closest.lon - lon, 2)))
          closest = parkingSpot;
      return closest;
    } else return null;
  }

  public Parking getNearestParking(double lat, double lon) {

    if (parking.size() != 0) {
      Parking closest = parking.get(0);

      for (Parking parkingSpot : parking)
        if (Math.sqrt(Math.pow(parkingSpot.lat - lat, 2) + Math.pow(parkingSpot.lon - lon, 2))
            < Math.sqrt(Math.pow(closest.lat - lat, 2) + Math.pow(closest.lon - lon, 2)))
          closest = parkingSpot;
      return closest;
    } else return null;
  }

  public String getIdent() {
    return ident;
  }

  public String getCountry() {
    return country;
  }

  public String getState() {
    return state;
  }

  public String getCity() {
    return city;
  }

  public String getName() {
    return name;
  }

  public float getLat() {
    return (float) lat;
  }

  public float getLon() {
    return (float) lon;
  }

  public float getAlt() {
    return alt;
  }

  public float getMagvar() {
    return magvar;
  }

  public PVector getTower() {
    return tower;
  }

  public PVector getLocation() {
    return location;
  }

  public HashMap<Integer, TaxiNode> getTaxiwayIndex() {
    return taxiwayIndex;
  }

  public HashMap<Integer, Parking> getParkingIndex() {
    return parkingIndex;
  }

  public ArrayList<Runway> getRunways() {
    return runways;
  }

  public ArrayList<TaxiNode> getTaxiNodes() {
    return taxiNodes;
  }

  public ArrayList<TaxiPath> getTaxiPaths() {
    return taxiPaths;
  }

  public ArrayList<Parking> getParking() {
    return parking;
  }

  public ArrayList<Apron> getAprons() {
    return aprons;
  }

  public ArrayList<BoundaryFence> getBoundaryFences() {
    return boundaryFences;
  }

  public ArrayList<SID> getSIDs() {
    return SIDs;
  }

  public ArrayList<STAR> getSTARs() {
    return STARs;
  }

  public ArrayList<Approach> getApproaches() {
    return approaches;
  }

  public HashMap<String, ArrayList<String>> getRoutes() {
    return routes;
  }

  public Query getQuery() {
    return query;
  }

  public Runway getRunwayByNumber(String number) {

    String designator = "";
    if (number.contains("L"))
      designator = "L";
    else if(number.contains("R"))
      designator = "R";


    if(number.replace(designator,"").length() == 1)
      number = "0" + number;

    if(!designator.isEmpty()){
      number = number.replace(designator,"");
      return getRunwayByNumberDesignator(number, designator);
    }
    else {
      String finalNumber = number;
      Runway[] primary =
          runways
              .stream()
              .filter(runway -> runway.getPrimaryNumber().equals(finalNumber))
              .toArray(Runway[]::new);
      if (primary.length > 0) {
        Runway r = new Runway(primary[0]);
        r.setHeading((float) r.getHeadingOfNumber(number));
        return r;
      }
      String finalNumber1 = number;
      Runway[] secondary =
          runways
              .stream()
              .filter(runway -> runway.getSecondaryNumber().equals(finalNumber1))
              .toArray(Runway[]::new);
      if (secondary.length > 0) {
        Runway r = new Runway(secondary[0]);
        r.setHeading((float) r.getHeadingOfNumber(number));
        return r;
      }
      return null;
    }
  }

  private Runway getRunwayByNumberDesignator(String number, String designator) {
    Runway[] primary =
        runways
            .stream()
            .filter(
                runway ->
                    runway.getPrimaryNumber().equals(number)
                        && runway.getPrimaryDesignator().equals(designator))
            .toArray(Runway[]::new);
    if (primary.length > 0){
      Runway r = new Runway(primary[0]);
      r.setHeading((float) r.getHeadingOfNumber(number));
      return r;
    }
    Runway[] secondary =
        runways
            .stream()
            .filter(
                runway ->
                    runway.getSecondaryNumber().equals(number)
                        && runway.getSecondaryDesignator().equals(designator))
            .toArray(Runway[]::new);
    if (secondary.length > 0){
      Runway r = new Runway(secondary[0]);
      r.setHeading((float) r.getHeadingOfNumber(number));
      return r;
    }
    return null;
  }
}
