/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import processing.core.PVector;

public class Navaid extends BaseAeroObject {

  String name = "";
  String ident = "";
  String type = "";
  float lat = 0;
  float lon = 0;
  PVector location;
  String frequency = "";
  boolean debug = false;

  public Navaid(String name, String ident, String type, String lat, String lon, String frequency) {
    super(ident, name, Double.parseDouble(lat), Double.parseDouble(lon));

    this.name = name;
    this.ident = ident;
    this.type = type;
    if (!lat.equalsIgnoreCase("")) this.lat = Float.parseFloat(lat);
    if (!lon.equalsIgnoreCase("")) this.lon = Float.parseFloat(lon);
    this.frequency = frequency;

    this.location = new PVector(this.lat, this.lon);

    if (debug)
      System.out.println(
          name + ", " + ident + ", " + type + ", " + lat + ", " + lon + ", " + frequency);
  }

  public float getLat() {
    return lat;
  }

  public float getLon() {
    return lon;
  }

  public String getIdent() {
    return ident;
  }

  public String getType() {
    return type;
  }

  public PVector getLocation() {
    return location;
  }

  public double lat() {
    return lat;
  }

  public double lon() {
    return lon;
  }
}
