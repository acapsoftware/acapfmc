/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import acap.fmc.routing.Waypoint;

/** Created by enadel on 12/13/16. */
public class TurnPoint extends Waypoint {

  public TurnPoint(float lon, float lat) {
    super(lon, lat);
  }

  public TurnPoint(float lon, float lat, float altitude) {
    super(lon, lat, altitude);
  }

  public TurnPoint(double lon, double lat, double altitude) {
    super(lon, lat, altitude);
  }

  public void drawWaypointInfo() {
    //
    //        drawer.stroke(255, identOpacity);
    //        drawer.fill(255, identOpacity);
    //        drawer.textAlign(CENTER);
    //        drawer.textSize(100);
    //        drawer.pushMatrix();
    //        drawer.translate(CalculationUtils.latToXCoord((float)this.latToXCoord(), (float)this.lonToYCoord()), CalculationUtils.lonToYCoord((float)this.lonToYCoord(), (float) this.latToXCoord()), this.getAltitude());
    //        drawer.line(0, 0, 0, 0, 0, 0.5f * zoom + 5000f);
    //        drawer.translate(0, 0, 0.5f * zoom + 6000f);
    //        drawer.rotateX(-PI / 2);
    //        drawer.rotateY(-radians(theta + 90));
    //        drawer.scale(0.5f * zoom + 1);
    //        drawer.text("", 0, 0);
    //        drawer.popMatrix();
  }
}
