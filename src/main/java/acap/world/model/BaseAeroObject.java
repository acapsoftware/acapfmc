/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import static acap.fmc.Constants.MAGVAR;

public abstract class BaseAeroObject implements AeroObject {

  protected final String id;
  protected final String name;

  public void setLat(double lat) {
    this.lat = lat;
  }

  public void setLon(double lon) {
    this.lon = lon;
  }

  public double lat;
  public double lon;

  public static final float EARTH_RADIUS_KM = 6371.01f;
  public static final float NM_TO_KM = 1.852f;
  public static final float KM_TO_NM = 1f / NM_TO_KM;

  public BaseAeroObject(String id, String name, double lat, double lon) {
    this.id = id;
    this.name = name;
    this.lat = lat;
    this.lon = lon;
  }

  @Override
  public String id() {
    return id;
  }

  @Override
  public String name() {
    return name;
  }

  @Override
  public double lat() {
    return lat;
  }

  @Override
  public double lon() {
    return lon;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof BaseAeroObject)) return false;

    BaseAeroObject baseAeroObject = (BaseAeroObject) o;

    return id.equals(baseAeroObject.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public float bearingTo(AeroObject other) {
    // haversine formula
    final double lat1 = Math.toRadians(lat());
    final double lat2 = Math.toRadians(other.lat());
    final double lng1 = Math.toRadians(lon());
    final double lng2 = Math.toRadians(other.lon());

    final double deltaLng = lng2 - lng1;
    final double y = Math.cos(lat2) * Math.sin(deltaLng);
    final double x =
        (Math.cos(lat1) * Math.sin(lat2)) - (Math.sin(lat1) * Math.cos(lat2) * Math.cos(deltaLng));

    final float raw = (float) Math.toDegrees(Math.atan2(y, x));
    final float normalized = (raw + 360) % 360;
    return (360 + normalized - MAGVAR) % 360;
  }

  @Override
  public float distanceTo(AeroObject other) {
    // haversine function, adapted from:
    //  http://www.movable-type.co.uk/scripts/latlong.html

    final double lat1 = lat();
    final double lat2 = other.lat();
    final double lng1 = lon();
    final double lng2 = other.lon();

    final double phi1 = Math.toRadians(lat1);
    final double phi2 = Math.toRadians(lat2);

    final double deltaPhi = Math.toRadians(lat2 - lat1);
    final double deltaLambda = Math.toRadians(lng2 - lng1);

    final double deltaPhi2sin = Math.sin(deltaPhi / 2.);
    final double deltaLam2sin = Math.sin(deltaLambda / 2.);

    final double a =
        deltaPhi2sin * deltaPhi2sin + Math.cos(phi1) * Math.cos(phi2) * deltaLam2sin * deltaLam2sin;
    final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return (float) (EARTH_RADIUS_KM * c * KM_TO_NM);
  }





}
