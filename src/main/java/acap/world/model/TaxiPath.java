/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import acap.utilities.Heading;

public class TaxiPath {

  String type;
  int start;
  int end;
  float taxiWidth;
  float weightLimit;
  boolean drawSurface;
  boolean drawDetail;
  String surface;
  String name;
  boolean centerLine;
  boolean centerLineLighted;
  String leftEdge;
  boolean leftEdgeLighted;
  String rightEdge;
  boolean rightEdgeLighted;
  String number;
  String designator;

  public TaxiPath(
      String type,
      int start,
      int end,
      float taxiWidth,
      float weightLimit,
      boolean drawSurface,
      boolean drawDetail,
      String surface,
      String name,
      boolean centerLine,
      boolean centerLineLighted,
      String leftEdge,
      boolean leftEdgeLighted,
      String rightEdge,
      boolean rightEdgeLighted,
      String number,
      String designator) {
    this.type = type;
    this.start = start;
    this.end = end;
    this.taxiWidth = taxiWidth;
    this.weightLimit = weightLimit;
    this.drawSurface = drawSurface;
    this.drawDetail = drawDetail;
    this.surface = surface;
    this.name = name;
    this.centerLine = centerLine;
    this.centerLineLighted = centerLineLighted;
    this.leftEdge = leftEdge;
    this.leftEdgeLighted = leftEdgeLighted;
    this.rightEdge = rightEdge;
    this.rightEdgeLighted = rightEdgeLighted;
    this.number = number;
    this.designator = designator;
  }

  public String getType() {
    return type;
  }

  public int getStart() {
    return start;
  }

  public int getEnd() {
    return end;
  }

  public float getTaxiWidth() {
    return taxiWidth;
  }

  public float getWeightLimit() {
    return weightLimit;
  }

  public boolean isDrawSurface() {
    return drawSurface;
  }

  public boolean isDrawDetail() {
    return drawDetail;
  }

  public String getSurface() {
    return surface;
  }

  private String getSecondaryNumber() {
    return Double.toString(Heading.Rotate(180, 10 * Double.valueOf(this.number)))
        .substring(0, 2);
  }

  public String getName() {
    if (this.getType().equals("RUNWAY")) {
      if (!this.designator.equals("NONE"))
        return this.number
            + this.designator.substring(0, 1)
            + "/"
            + getSecondaryNumber()
            + (this.designator.equals("LEFT") ? "R" : "L");
      else {
        if (getSecondaryNumber().equals("0.")) {
          return this.number + "/" + "36";
        }
        return this.number + "/" + getSecondaryNumber();
      }
    }
    if (this.getType().equals("PATH")) {
      return "apron";
    }
    if (this.type.equals("PARKING")) return "parking";
    if (this.type.equals("VEHICLE")) return "vehicle";

    if (this.type.equals("TAXI") && this.name.isEmpty()) return "taxi";

    return name;
  }

  public boolean isCenterLine() {
    return centerLine;
  }

  public boolean isCenterLineLighted() {
    return centerLineLighted;
  }

  public String getLeftEdge() {
    return leftEdge;
  }

  public boolean isLeftEdgeLighted() {
    return leftEdgeLighted;
  }

  public String getRightEdge() {
    return rightEdge;
  }

  public boolean isRightEdgeLighted() {
    return rightEdgeLighted;
  }

  public String getNumber() {
    return number;
  }

  public String getDesignator() {
    return designator;
  }
}
