/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

public class TaxiNode {

  int index;

  public int getIndex() {
    return index;
  }

  public String getType() {
    return type;
  }

  public boolean isOrientation() {
    return orientation;
  }

  public float getLat() {
    return lat;
  }

  public float getLon() {
    return lon;
  }

  String type;
  boolean orientation; //true is forward
  float lat;
  float lon;

  public TaxiNode(int index, String type, boolean orientation, float lat, float lon) {

    this.index = index;
    this.type = type;
    this.orientation = orientation; //true is forward
    this.lat = lat;
    this.lon = lon;
  }
}
