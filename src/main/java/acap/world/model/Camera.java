/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import acap.fmc.gui.processing.apps.GLWorld;
import acap.world.model.Aircraft;
import acap.world.model.Airport;

/** Created by enadel on 10/6/16. */
public class Camera {

  public enum CamMode {
    FREE,
    AIRPORT,
    AIRCRAFT,
    OVERVIEW
  }

  //state variables
  public static boolean dragLEFT = false;
  public static boolean dragRIGHT = false;
  public static boolean dragMIDDLE = false;
  public static float x = (GLWorld.getInstance().displayWidth / 2);
  public static float y = (GLWorld.getInstance().displayHeight / 2);
  public static float z = 0;
  public static CamMode cameraMode = CamMode.FREE;
  public static float camX = 0;
  public static float camY = 0;
  public static float camZ = 0;
  public static float saveX = 0;
  public static float saveY = 0;
  public static float zoom = 10;
  public static float saveZoom = 10;
  public static float theta = 0;
  public static float saveTheta = 0;
  public static float phi = 45;
  public static float savePhi = 0;
  public static float distanceToSelectedAirport = 1;
  public static Airport selectedAirport;
  public static int selectedAirportIndex = 0;
  public static Aircraft selectedAircraft;
  public static int selectedAircraftIndex = 0;
}
