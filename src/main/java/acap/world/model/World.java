/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import static acap.world.model.Camera.selectedAircraft;
import static acap.world.model.Camera.selectedAircraftIndex;

import acap.fmc.UserAircraft;
import java.util.ArrayList;

/** Created by enadel on 10/6/16. */
public class World {

  private ArrayList<Aircraft> aircraft = new ArrayList<>();
  public ArrayList<Airport> airports = new ArrayList<>();
  private static World ourInstance = new World();

  public static World getInstance() {
    return ourInstance;
  }

  private World() {}

  public void createUserAircraft() {
    aircraft.add(UserAircraft.getInstance());
    selectedAircraft = aircraft.get(selectedAircraftIndex);
  }

  public void addAircraft(Aircraft a) {
    aircraft.add(a);
  }

  public ArrayList<Aircraft> getAircraft() {
    return aircraft;
  }

  public ArrayList<Airport> getAirports() {
    return airports;
  }

  public Aircraft getAicraftByFlight(String flight){
    for(Aircraft a : this.aircraft){
      if(a.getFriendlyName().equals(flight))
        return a;
    }
    return null;
  }

  public void setAirports(ArrayList<Airport> airports) {
    this.airports = airports;
  }
}
