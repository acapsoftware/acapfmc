/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import static acap.world.utilities.CalculationUtils.getRadialIntersection;
import static processing.core.PConstants.PI;

import acap.fmc.gui.processing.apps.GLWorld;
import acap.fmc.routing.Waypoint;
import acap.world.utilities.CalculationUtils;
import acap.world.utilities.NavDB;
import java.util.ArrayList;
import java.util.Arrays;
import processing.core.PVector;

public class SID {

  String name = "";
  String line;
  Airport airport;
  NavDB nav = NavDB.getInstance();
  boolean debug = true;

  public String getName() {
    return name;
  }

  public String getLine() {
    return line;
  }

  public Airport getAirport() {
    return airport;
  }

  public boolean isDebug() {
    return debug;
  }

  public ArrayList<Waypoint> getWaypoints() {
    return waypoints;
  }

  ArrayList<Waypoint> waypoints = new ArrayList<Waypoint>();

  public SID(Airport airport, String name, String line) {

    this.airport = airport;
    this.name = name;
    this.line = line;

    ArrayList<String> instructions = new ArrayList<String>(Arrays.asList(line.split(" ")));
    System.out.println(
        "\n"
            + "There are "
            + instructions.size()
            + " instructions in the "
            + this.name
            + " SID at "
            + this.airport.ident
            + ", with content: "
            + line);
    if (debug) for (String instruction : instructions) System.out.println(instruction);
    int instructionPointer = 0;

    while (instructionPointer < instructions.size()) {

      //make waypoints
      if (instructions.get(instructionPointer).equalsIgnoreCase("RNW")) {
        this.name = instructions.get(instructionPointer + 1) + "." + this.name;
        try {
          waypoints.add(
              new Waypoint(this.airport.getRunway(instructions.get(instructionPointer + 1))));
        } catch (Exception e) {
          System.out.println(
              "ERROR: Departure runway "
                  + instructions.get(instructionPointer + 1)
                  + " not found at "
                  + this.airport.ident
                  + ".  SID removed.");
          return;
        }
        System.out.println(
            "Added waypoint "
                + waypoints.get(waypoints.size() - 1).getName()
                + " to the "
                + this.name
                + " SID at "
                + this.airport.ident
                + ".");
        instructionPointer += 2;
      } else if (instructions.get(instructionPointer).equalsIgnoreCase("HDG")) {

        PVector intersection;

        if (instructions.get(instructionPointer + 2).equalsIgnoreCase("INTERCEPT")) {

          if (instructions.get(instructionPointer + 7).length() == 3) {
            System.out.println("navaid intercept: " + instructions.get(instructionPointer + 7));

            intersection =
                getRadialIntersection(
                    waypoints.get(waypoints.size() - 1).getLocation(),
                    Float.parseFloat(instructions.get(instructionPointer + 1))
                        + this.airport.magvar,
                    nav.getNavaid(instructions.get(instructionPointer + 7)).getLocation(),
                    Float.parseFloat(instructions.get(instructionPointer + 4)));
          } else if (instructions.get(instructionPointer + 7).length() == 5) {
            System.out.println(
                "intersection intercept: " + instructions.get(instructionPointer + 7));

            //PVector origin = new PVector(waypoints.get(waypoints.size()-1).getLocation().x, waypoints.get(waypoints.size()-1).getLocation().y);

            System.out.println(waypoints.get(waypoints.size() - 1).getLocation());
            System.out.println(
                nav.getIntersections().get(instructions.get(instructionPointer + 7)));
            intersection =
                getRadialIntersection(
                    waypoints.get(waypoints.size() - 1).getLocation(),
                    Float.parseFloat(instructions.get(instructionPointer + 1))
                        + this.airport.magvar,
                    nav.getIntersection(instructions.get(instructionPointer + 7)),
                    Float.parseFloat(instructions.get(instructionPointer + 4)));
          } else {
            System.out.println(
                "ERROR: Intercept fix in "
                    + name
                    + " at "
                    + this.airport.ident
                    + " was not a navaid, or an intersection.");
            intersection = new PVector(0, 0);
          }

          waypoints.add(new Waypoint(intersection));
          System.out.println(
              "Added waypoint "
                  + waypoints.get(waypoints.size() - 1).getName()
                  + " to the "
                  + this.name
                  + " SID at "
                  + this.airport.ident
                  + ".");
          instructionPointer += 6;

        } else if (instructions.get(instructionPointer + 2).equalsIgnoreCase("UNTIL")) {

          if (instructions.get(instructionPointer + 4).equalsIgnoreCase("FROM")) {
            System.out.println("distance from a fix mode.");

            float distance =
                Float.parseFloat(instructions.get(instructionPointer + 3)); //STUFF IN HERE, BRO.
            String fixIdent = instructions.get(instructionPointer + 6);
            PVector origin;

            System.out.println(distance + " from " + fixIdent);

            if (fixIdent.substring(0, 1).equalsIgnoreCase("I") && fixIdent.length() == 4) {
              System.out.println("ILS mode");
              origin =
                  new PVector(
                      waypoints.get(waypoints.size() - 1).getLocation().x,
                      waypoints.get(waypoints.size() - 1).getLocation().y);
            } else if (fixIdent.length() == 3)
              origin =
                  new PVector(
                      nav.getNavaid(fixIdent).getLocation().x,
                      nav.getNavaid(fixIdent).getLocation().y);
            else if (fixIdent.length() == 5)
              origin =
                  new PVector(nav.getIntersection(fixIdent).x, nav.getIntersection(fixIdent).y);
            else {
              origin = new PVector(0, 0);
              System.out.println(
                  "ERROR: until fix: "
                      + fixIdent
                      + " not recognized in the "
                      + this.name
                      + " SID at "
                      + this.airport.ident
                      + ".");
              break;
            }

            System.out.println("The fix is located at: " + origin);

            PVector distanceAway = new PVector();
            System.out.println(
                "Heading: "
                    + (Float.parseFloat(instructions.get(instructionPointer + 1))
                        + this.airport.magvar));
            distanceAway =
                PVector.fromAngle(
                    GLWorld.radians(
                        Float.parseFloat(instructions.get(instructionPointer + 1))
                            + this.airport.magvar));

            distanceAway.set(
                distanceAway.x * GLWorld.cos(distanceAway.y * (PI / 180)),
                distanceAway.y); //YOOOO HOMIE

            distanceAway.set(
                CalculationUtils.miles2lon(
                    distanceAway.x * distance,
                    this.airport.getLocation().x,
                    this.airport.getLocation().y),
                CalculationUtils.miles2lat(
                    distanceAway.y * distance,
                    this.airport.getLocation().y,
                    this.airport.getLocation().x));

            distanceAway.add(origin);
            System.out.println("distance away:" + distanceAway);
            waypoints.add(new Waypoint(distanceAway));
            System.out.println(
                "Added waypoint "
                    + waypoints.get(waypoints.size() - 1).getName()
                    + " to the "
                    + this.name
                    + " SID at "
                    + this.airport.ident
                    + ".");
            instructionPointer += 7;

          } else {
            System.out.println("altitude mode.");

            float distance =
                1.66f * (Float.parseFloat(instructions.get(instructionPointer + 3)) / 500f);
            System.out.println("Distance: " + distance);
            PVector origin = waypoints.get(waypoints.size() - 1).getLocation();
            PVector altitudeAway;
            altitudeAway =
                PVector.fromAngle(
                    GLWorld.radians(
                        Float.parseFloat(instructions.get(instructionPointer + 1))
                            + this.airport.magvar));
            System.out.println(
                "Heading: " + Float.parseFloat(instructions.get(instructionPointer + 1)));
            System.out.println(
                "w/ magvar: "
                    + (Float.parseFloat(instructions.get(instructionPointer + 1))
                        + this.airport.magvar));
            altitudeAway.set(
                CalculationUtils.miles2lon(
                    altitudeAway.x * distance,
                    this.airport.getLocation().x,
                    this.airport.getLocation().y),
                CalculationUtils.miles2lat(
                    altitudeAway.y * distance,
                    this.airport.getLocation().y,
                    this.airport.getLocation().x));
            altitudeAway.add(origin);

            waypoints.add(new Waypoint(altitudeAway));
            System.out.println(
                "Added waypoint "
                    + waypoints.get(waypoints.size() - 1).getName()
                    + " to the "
                    + this.name
                    + " SID at "
                    + this.airport.ident
                    + ".");
            if (instructionPointer + 2 >= instructions.size()) break;
            else instructionPointer += 2;
          }
        } else if (instructions.get(instructionPointer + 2).equalsIgnoreCase("VECTORS")) {
          float distance = 3.0f;
          PVector origin = waypoints.get(waypoints.size() - 1).getLocation();
          PVector vectorsAway;
          vectorsAway =
              PVector.fromAngle(
                  GLWorld.radians(
                      Float.parseFloat(instructions.get(instructionPointer + 1))
                          + this.airport.magvar));
          vectorsAway.set(
              CalculationUtils.miles2lon(
                  vectorsAway.x * distance,
                  this.airport.getLocation().x,
                  this.airport.getLocation().y),
              CalculationUtils.miles2lat(
                  vectorsAway.y * distance,
                  this.airport.getLocation().y,
                  this.airport.getLocation().x));
          vectorsAway.add(origin);

          waypoints.add(new Waypoint(vectorsAway));
          System.out.println(
              "Added waypoint "
                  + waypoints.get(waypoints.size() - 1).getName()
                  + " to the "
                  + this.name
                  + " SID at "
                  + this.airport.ident
                  + ".");
          if (instructionPointer + 2 >= instructions.size()) break;
          else instructionPointer += 2;
        } else {
          System.out.println(
              "ERROR: Heading specified with no intercept or altitude in the "
                  + this.name
                  + " SID at "
                  + this.airport.ident
                  + ".");
          instructionPointer += 6;
        }
      } else if (instructions.get(instructionPointer).equalsIgnoreCase("TO")) {
        instructionPointer++;
      } else if (instructions.get(instructionPointer).equalsIgnoreCase("")) {
        instructionPointer++;
      } else if (instructions.get(instructionPointer).equalsIgnoreCase(" ")) {
        instructionPointer++;
      } else if (instructions.get(instructionPointer).equalsIgnoreCase("ENDSIDS")) {
        instructionPointer++;
      } else {
        //System.out.println("ERROR: Instruction: " + instructions.get(instructionPointer) + " not recognized in the " + this.name + " SID at " + this.airport.ident + ".");
        break;
      }
    }
  }
}

//RNW 15R HDG 151 INTERCEPT RADIAL 132 TO FIX FOXXX SPEED 210 FIX FOXXX FIX BRRRO SPEED 250 FIX JAITE FIX HEWMO SPEED 290 FIX KENWI FIX BLZZR
