/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import java.util.ArrayList;
import processing.core.PVector;

public class Apron {

  public ArrayList<PVector> getVerticies() {
    return verticies;
  }

  public String getSurface() {
    return surface;
  }

  ArrayList<PVector> verticies;
  String surface;

  public Apron(ArrayList<PVector> verticies, String suraface) {

    this.verticies = verticies;
    this.surface = surface;
  }
}
