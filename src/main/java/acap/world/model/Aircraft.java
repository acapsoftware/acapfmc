/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import static acap.world.view.WorldView.drawer;

import acap.fmc.Constants;
import acap.fmc.routing.AirportGraph;
import acap.fmc.routing.Waypoint;
import acap.world.utilities.DrawingUtils;
import processing.core.PShape;

public class Aircraft extends BaseAeroObject {

  //PERFORMANCE DATA with some defaults
  protected String type = "PA28";
  protected String tailNumber = "N#####";
  protected String friendlyName = "Piper Cherokee 140 Flite Liner";
  protected float cruiseSpeed = 110;
  protected float maxAltitude = 11000;
  protected float minRunway = 1200;
  protected float fuelCap = 50;
  protected boolean fuelType = false; //true = AVgas, false = Jet-A
  protected float cruisegph = 8;
  protected float cargo = 800; //lbs
  protected int wingspan = 30;
  protected float reserve = 1.0f; //hours of reserve fuel remaining 1.0 = 1hr
  protected float range = 0.0f;
  protected float optimalRange = 0.0f;

  //OPERATIONAL DATA with zeroed numbers
  protected Airport origin = null;
  protected Airport destination = null;
  protected float fuel = getFuelCap(); //topped-off
  protected double altitude = 0;
  protected float speed = 0;
  protected float gph = 0;
  protected float heading = 0.0f;
  protected float ETE = 0.0f;
  protected float distanceLast = 0.0f;

  protected double pitch = 0;
  protected double roll = 0;
  protected long lastUpdate = 0;
  protected long timeOff = 0;
  protected String wheelsUpTime = "";
  protected Parking originParking = null;
  protected Parking destinationParking = null;
  protected boolean enroute = false;

  protected PShape triShape;

  public Aircraft() {
    super("", "", 0, 0);
  }

  public Aircraft(Airport origin) {

    super("AIRCRAFT", "AIRCRAFT", origin.getLat(), origin.getLon());
    this.setOrigin(origin);

    this.originParking = this.getOrigin().getFreeParking();
    if (originParking != null) {
      setLon(originParking.lon);
      setLat(originParking.lat);
    } else {
      setLon(origin.getLon());
      setLat(origin.getLat());
    }
    setAltitude(this.getOrigin().alt);
    setHeading(originParking.heading);
    setLastUpdate(millis());
    setRange(((getFuel() / getGph()) - getReserve()) * getSpeed());
    setOptimalRange(((getRange() * getRange()) / 10000)); //8500 = range of a B777
  }

  public long millis() {
    return 0L;
  }

  public Aircraft(
      String friendlyName,
      String type,
      int cruiseSpeed,
      int maxAltitude,
      int minRunway,
      int fuelCap,
      String fuelType,
      int cruisegph,
      int cargo,
      int wingspan,
      int heightOffset,
      Airport origin) {

    super(friendlyName, friendlyName, origin.getLat(), origin.getLon());

    this.setFriendlyName(friendlyName);
    this.setType(type);
    this.setCruiseSpeed(cruiseSpeed);
    this.setMaxAltitude(maxAltitude);
    this.setMinRunway(minRunway);
    this.setFuelCap(fuelCap);
    this.setFuel(fuelCap);
    this.setWingspan(wingspan);
    this.setLastUpdate(millis());
    if (fuelType.equals('A')) this.setFuelType(false);
    else this.setFuelType(true);
    this.setCruisegph(getGph());
    this.setCargo(cargo);
    this.setOrigin(origin);

    setOriginParking(this.getOrigin().getFreeParking(wingspan));
    if (getOriginParking() != null) {
      setLon(getOriginParking().lon);
      setLat(getOriginParking().lat);
    } else {
      setLon(origin.getLon());
      setLat(origin.getLat());
    }
    setAltitude(this.getOrigin().alt);
    setHeading(getOriginParking().heading);
    this.setRange(((getFuel() / getGph()) - getReserve()) * getSpeed());
    this.setOptimalRange(
        ((getRange() * getRange())
            / 5500)); //5500 = range of a fully loaded B777 (serves as a maximum)
    this.setFuel(fuelCap);
  }

  public String getType() {
    return type;
  }

  public String getTailNumber() {
    return tailNumber;
  }

  public String getFriendlyName() {
    return friendlyName;
  }

  public float getCruiseSpeed() {
    return cruiseSpeed;
  }

  public float getMaxAltitude() {
    return maxAltitude;
  }

  /**
   * Get the minimum amount of runway to takeoff in nautical miles.
   * @return
   */
  public float getMinRunway() {
    return minRunway;
  }

  public float getFuelCap() {
    return fuelCap;
  }

  public boolean isFuelType() {
    return fuelType;
  }

  public float getCruisegph() {
    return cruisegph;
  }

  public float getCargo() {
    return cargo;
  }

  public int getWingspan() {
    return wingspan;
  }

  public float getReserve() {
    return reserve;
  }

  public float getRange() {
    return range;
  }

  public float getOptimalRange() {
    return optimalRange;
  }

  public Airport getOrigin() {
    return origin;
  }

  public Airport getDestination() {
    return destination;
  }

  public float getFuel() {
    return fuel;
  }

  public double getAltitude() {
    return altitude;
  }

  public float getSpeed() {
    return speed;
  }

  public float getGph() {
    return gph;
  }

  public float getHeading() {
    return heading;
  }

  public double getLon() {
    return lon;
  }

  public double getLat() {
    return lat;
  }

  public float getETE() {
    return ETE;
  }

  public float getDistanceLast() {
    return distanceLast;
  }

  public long getLastUpdate() {
    return lastUpdate;
  }

  public long getTimeOff() {
    return timeOff;
  }

  public String getWheelsUpTime() {
    return wheelsUpTime;
  }

  public Parking getOriginParking() {
    return originParking;
  }

  public Parking getDestinationParking() {
    return destinationParking;
  }

  public boolean isEnroute() {
    return enroute;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setTailNumber(String tailNumber) {
    this.tailNumber = tailNumber;
  }

  public void setFriendlyName(String friendlyName) {
    this.friendlyName = friendlyName;
  }

  public void setCruiseSpeed(float cruiseSpeed) {
    this.cruiseSpeed = cruiseSpeed;
  }

  public void setMaxAltitude(float maxAltitude) {
    this.maxAltitude = maxAltitude;
  }

  public void setMinRunway(float minRunway) {
    this.minRunway = minRunway;
  }

  public void setFuelCap(float fuelCap) {
    this.fuelCap = fuelCap;
  }

  public void setFuelType(boolean fuelType) {
    this.fuelType = fuelType;
  }

  public void setCruisegph(float cruisegph) {
    this.cruisegph = cruisegph;
  }

  public void setCargo(float cargo) {
    this.cargo = cargo;
  }

  public void setWingspan(int wingspan) {
    this.wingspan = wingspan;
  }

  public void setReserve(float reserve) {
    this.reserve = reserve;
  }

  public void setWaypoint(Waypoint p){
    this.lon = p.lon();
    this.lat = p.lat();
    this.altitude = p.getAltitude();
  }

  public void setRange(float range) {
    this.range = range;
  }

  public void setOptimalRange(float optimalRange) {
    this.optimalRange = optimalRange;
  }

  public void setOrigin(Airport origin) {
    this.origin = origin;
  }

  public void setDestination(Airport destination) {
    this.destination = destination;
  }

  public void setFuel(float fuel) {
    this.fuel = fuel;
  }

  public void setAltitude(float altitude) {
    this.altitude = altitude;
  }

  public void setSpeed(float speed) {
    this.speed = speed;
  }

  public void setGph(float gph) {
    this.gph = gph;
  }

  public void setHeading(float heading) {
    this.heading = heading;
  }

  public void setLon(float lon) {
    this.lon = lon;
  }

  public void setLon(double lon) {
    this.lon = lon;
  }

  public void setLat(float lat) {
    this.lat = lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public void setETE(float ETE) {
    this.ETE = ETE;
  }

  public void setDistanceLast(float distanceLast) {
    this.distanceLast = distanceLast;
  }

  public void setLastUpdate(long lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public void setTimeOff(long timeOff) {
    this.timeOff = timeOff;
  }

  public void setWheelsUpTime(String wheelsUpTime) {
    this.wheelsUpTime = wheelsUpTime;
  }

  public void setOriginParking(Parking originParking) {
    this.originParking = originParking;
  }

  public void setDestinationParking(Parking destinationParking) {
    this.destinationParking = destinationParking;
  }

  public void setEnroute(boolean enroute) {
    this.enroute = enroute;
  }

  public double getPitch() {
    return pitch;
  }

  public void setPitch(double pitch) {
    this.pitch = pitch;
  }

  public double getRoll() {
    return roll;
  }

  public void setRoll(double roll) {
    this.roll = roll;
  }

  public PShape getModel() {
    if(this.triShape == null){
      PShape userAircraftModel = drawer.loadShape(Constants.USER_AIRCRAFT_MODEL);
      this.triShape = DrawingUtils.createShapeTri(userAircraftModel);
    }
    return this.triShape;
  }
}
