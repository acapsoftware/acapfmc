/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

public class Parking {

  public int getIndex() {
    return index;
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getNumber() {
    return number;
  }

  public float getHeading() {
    return heading;
  }

  public float getRadius() {
    return radius;
  }

  public double getLat() {
    return lat;
  }

  public double getLon() {
    return lon;
  }

  public String getPushback() {
    return pushback;
  }

  public boolean isOccupied() {
    return occupied;
  }

  int index;
  String type;
  String name = "";
  String number = "";
  float heading;
  float radius;
  double lat;
  double lon;
  String pushback;
  boolean occupied;

  public Parking(
      int index,
      String type,
      String name,
      String number,
      float heading,
      float radius,
      float lat,
      float lon,
      String pushback) {

    this.index = index;
    this.type = type;
    this.name = name;
    this.number = number;
    this.heading = heading;
    this.radius = radius;
    this.lat = lat;
    this.lon = lon;
    this.pushback = pushback;
    this.occupied = false;
  }
}
