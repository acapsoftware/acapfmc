/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

import static processing.core.PApplet.str;

import acap.utilities.Heading;
import processing.core.PVector;

public class Runway {
  float lat;
  float lon;
  float alt;
  String surface;
  float heading;
  float recipricalHeading;
  float length;
  float width;
  String primaryNumber;
  String secondaryNumber;
  String primaryDesignator;
  String secondaryDesignator;
  float patternAltitude;
  PVector location;
  boolean primaryTakeoff;
  boolean primaryLanding;
  boolean primaryPattern; //true for left pattern
  boolean secondaryTakeoff;
  boolean secondaryLanding;
  boolean secondaryPattern; //true for left pattern
  //markings
  boolean threshold;
  boolean edges;
  boolean dashes;
  boolean leadingZeroIdent;
  boolean noThresholdEndArrows;
  boolean fixedDistance;
  boolean touchdown;
  boolean ident;
  boolean precision;
  boolean primaryClosed;
  boolean secondaryClosed;
  float blastpadPrimary = 0;
  float blastpadSecondary = 0;
  float offsetPrimary = 0;
  float offsetSecondary = 0;

  public Runway(
      String primaryNumber,
      String primaryDesignator,
      float lat,
      float lon,
      float alt,
      String surface,
      float heading,
      float length,
      float width,
      float patternAltitude,
      boolean primaryTakeoff,
      boolean primaryLanding,
      boolean primaryPattern,
      boolean secondaryTakeoff,
      boolean secondaryLanding,
      boolean secondaryPattern,
      boolean threshold,
      boolean edges,
      boolean dashes,
      boolean leadingZeroIdent,
      boolean noThresholdEndArrows,
      boolean fixedDistance,
      boolean touchdown,
      boolean ident,
      boolean precision,
      boolean primaryClosed,
      boolean secondaryClosed,
      boolean blastPad1end,
      float blastPad1length,
      boolean blastPad2end,
      float blastPad2length,
      boolean offset1end,
      float offset1length,
      boolean offset2end,
      float offset2length) {

    this.setPrimaryNumber(primaryNumber);
    //cardinal runway headings
    if (primaryNumber.equalsIgnoreCase("NORTH")) this.setSecondaryNumber("SOUTH");
    else if (primaryNumber.equalsIgnoreCase("NORTHEAST")) this.setSecondaryNumber("SOUTHWEST");
    else if (primaryNumber.equalsIgnoreCase("EAST")) this.setSecondaryNumber("WEST");
    else if (primaryNumber.equalsIgnoreCase("SOUTHEAST")) this.setSecondaryNumber("NORTHWEST");
    else if (primaryNumber.equalsIgnoreCase("SOUTH")) this.setSecondaryNumber("NORTH");
    else if (primaryNumber.equalsIgnoreCase("SOUTHWEST")) this.setSecondaryNumber("NORTHEAST");
    else if (primaryNumber.equalsIgnoreCase("WEST")) this.setSecondaryNumber("EAST");
    else if (primaryNumber.equalsIgnoreCase("NORTHWEST")) this.setSecondaryNumber("SOUTHEAST");
    else this.setSecondaryNumber(str(Integer.parseInt(primaryNumber) + 20 - 2));

    this.setLat(lat);
    this.setLon(lon);
    this.setAlt(alt);
    this.setSurface(surface);
    this.setHeading(heading);
    this.setLength(length);
    this.setWidth(width);
    this.setPatternAltitude(patternAltitude);
    this.setPrimaryTakeoff(primaryTakeoff);
    this.setPrimaryLanding(primaryLanding);
    this.setPrimaryPattern(primaryPattern);
    this.setSecondaryTakeoff(secondaryTakeoff);
    this.setSecondaryLanding(secondaryLanding);
    this.setSecondaryPattern(secondaryPattern);

    //markings
    this.setThreshold(threshold);
    this.setEdges(edges);
    this.setDashes(dashes);
    this.setLeadingZeroIdent(leadingZeroIdent);
    this.setNoThresholdEndArrows(noThresholdEndArrows);
    this.setFixedDistance(fixedDistance);
    this.setTouchdown(touchdown);
    this.setIdent(ident);
    this.setPrecision(precision);
    this.setPrimaryClosed(primaryClosed);
    this.setSecondaryClosed(secondaryClosed);

    //blastpads
    if (blastPad1end) setBlastpadPrimary(blastPad1length);
    else setBlastpadSecondary(blastPad1length);

    if (blastPad2end) setBlastpadPrimary(blastPad2length);
    else setBlastpadSecondary(blastPad2length);

    //offsets
    if (offset1end) setOffsetPrimary(offset1length);
    else setOffsetSecondary(offset1length);

    if (offset2end) setOffsetPrimary(offset2length);
    else setOffsetSecondary(offset2length);

    if (primaryDesignator.equalsIgnoreCase("NONE")) {
      this.setPrimaryDesignator("");
      this.setSecondaryDesignator("");
    } else this.setPrimaryDesignator(primaryDesignator.substring(0, 1));

    if (this.getPrimaryDesignator().equalsIgnoreCase("R")) this.setSecondaryDesignator("L");
    else if (this.getPrimaryDesignator().equalsIgnoreCase("L")) this.setSecondaryDesignator("R");
    else if (this.getPrimaryDesignator().equalsIgnoreCase("C")) this.setSecondaryDesignator("C");
    else this.setSecondaryDesignator("");

    if (!this.isLeadingZeroIdent() && this.getPrimaryNumber().substring(0, 1).equalsIgnoreCase("0"))
      this.setPrimaryNumber(this.getPrimaryNumber().substring(1, 2));

    if (!this.isLeadingZeroIdent()
        && this.getSecondaryNumber().substring(0, 1).equalsIgnoreCase("0"))
      this.setSecondaryNumber(this.getSecondaryNumber().substring(1, 2));

    this.setLocation(new PVector(this.getLat(), this.getLon()));
  }

  public Runway(Runway runway) {
    this.heading = runway.getHeading();
    this.length = runway.getLength();
    this.alt = runway.getAlt();
    this.patternAltitude = runway.getPatternAltitude();
    this.primaryDesignator = runway.getPrimaryDesignator();
    this.primaryNumber = runway.getPrimaryNumber();
    this.lat = runway.getLat();
    this.lon = runway.getLon();
    this.location = runway.getLocation();
    this.primaryNumber = runway.getPrimaryNumber();
    this.secondaryNumber = runway.getSecondaryNumber();
  }

  public float getLat() {
    return lat;
  }

  public float getLon() {
    return lon;
  }

  public float getAlt() {
    return alt;
  }

  public String getSurface() {
    return surface;
  }

  public float getHeading() {
    return heading;
  }

  public float getLength() {
    return length;
  }

  public float getWidth() {
    return width;
  }

  public double getHeadingOfNumber(String number) {
    if (primaryNumber.equals(number)) return heading;
    else return Heading.Rotate(180, heading);
  }

  public String getPrimaryNumber() {
    return primaryNumber;
  }

  public String getSecondaryNumber() {
    return secondaryNumber;
  }

  public String getPrimaryDesignator() {
    return primaryDesignator;
  }

  public String getSecondaryDesignator() {
    return secondaryDesignator;
  }

  public float getPatternAltitude() {
    return patternAltitude;
  }

  public PVector getLocation() {
    return location;
  }

  public boolean isPrimaryTakeoff() {
    return primaryTakeoff;
  }

  public boolean isPrimaryLanding() {
    return primaryLanding;
  }

  public boolean isPrimaryPattern() {
    return primaryPattern;
  }

  public boolean isSecondaryTakeoff() {
    return secondaryTakeoff;
  }

  public boolean isSecondaryLanding() {
    return secondaryLanding;
  }

  public boolean isSecondaryPattern() {
    return secondaryPattern;
  }

  public boolean isThreshold() {
    return threshold;
  }

  public boolean isEdges() {
    return edges;
  }

  public boolean isDashes() {
    return dashes;
  }

  public boolean isLeadingZeroIdent() {
    return leadingZeroIdent;
  }

  public boolean isNoThresholdEndArrows() {
    return noThresholdEndArrows;
  }

  public boolean isFixedDistance() {
    return fixedDistance;
  }

  public boolean isTouchdown() {
    return touchdown;
  }

  public boolean isIdent() {
    return ident;
  }

  public boolean isPrecision() {
    return precision;
  }

  public boolean isPrimaryClosed() {
    return primaryClosed;
  }

  public boolean isSecondaryClosed() {
    return secondaryClosed;
  }

  public float getBlastpadPrimary() {
    return blastpadPrimary;
  }

  public float getBlastpadSecondary() {
    return blastpadSecondary;
  }

  public float getOffsetPrimary() {
    return offsetPrimary;
  }

  public float getOffsetSecondary() {
    return offsetSecondary;
  }

  public void setLat(float lat) {
    this.lat = lat;
  }

  public void setLon(float lon) {
    this.lon = lon;
  }

  public void setAlt(float alt) {
    this.alt = alt;
  }

  public void setSurface(String surface) {
    this.surface = surface;
  }

  public void setHeading(float heading) {
    this.heading = heading;
  }

  public void setLength(float length) {
    this.length = length;
  }

  public void setWidth(float width) {
    this.width = width;
  }

  public void setPrimaryNumber(String primaryNumber) {
    this.primaryNumber = primaryNumber;
  }

  public void setSecondaryNumber(String secondaryNumber) {
    this.secondaryNumber = secondaryNumber;
  }

  public void setPrimaryDesignator(String primaryDesignator) {
    this.primaryDesignator = primaryDesignator;
  }

  public void setSecondaryDesignator(String secondaryDesignator) {
    this.secondaryDesignator = secondaryDesignator;
  }

  public void setPatternAltitude(float patternAltitude) {
    this.patternAltitude = patternAltitude;
  }

  public void setLocation(PVector location) {
    this.location = location;
  }

  public void setPrimaryTakeoff(boolean primaryTakeoff) {
    this.primaryTakeoff = primaryTakeoff;
  }

  public void setPrimaryLanding(boolean primaryLanding) {
    this.primaryLanding = primaryLanding;
  }

  public void setPrimaryPattern(boolean primaryPattern) {
    this.primaryPattern = primaryPattern;
  }

  public void setSecondaryTakeoff(boolean secondaryTakeoff) {
    this.secondaryTakeoff = secondaryTakeoff;
  }

  public void setSecondaryLanding(boolean secondaryLanding) {
    this.secondaryLanding = secondaryLanding;
  }

  public void setSecondaryPattern(boolean secondaryPattern) {
    this.secondaryPattern = secondaryPattern;
  }

  public void setThreshold(boolean threshold) {
    this.threshold = threshold;
  }

  public void setEdges(boolean edges) {
    this.edges = edges;
  }

  public void setDashes(boolean dashes) {
    this.dashes = dashes;
  }

  public void setLeadingZeroIdent(boolean leadingZeroIdent) {
    this.leadingZeroIdent = leadingZeroIdent;
  }

  public void setNoThresholdEndArrows(boolean noThresholdEndArrows) {
    this.noThresholdEndArrows = noThresholdEndArrows;
  }

  public void setFixedDistance(boolean fixedDistance) {
    this.fixedDistance = fixedDistance;
  }

  public void setTouchdown(boolean touchdown) {
    this.touchdown = touchdown;
  }

  public void setIdent(boolean ident) {
    this.ident = ident;
  }

  public void setPrecision(boolean precision) {
    this.precision = precision;
  }

  public void setPrimaryClosed(boolean primaryClosed) {
    this.primaryClosed = primaryClosed;
  }

  public void setSecondaryClosed(boolean secondaryClosed) {
    this.secondaryClosed = secondaryClosed;
  }

  public void setBlastpadPrimary(float blastpadPrimary) {
    this.blastpadPrimary = blastpadPrimary;
  }

  public void setBlastpadSecondary(float blastpadSecondary) {
    this.blastpadSecondary = blastpadSecondary;
  }

  public void setOffsetPrimary(float offsetPrimary) {
    this.offsetPrimary = offsetPrimary;
  }

  public void setOffsetSecondary(float offsetSecondary) {
    this.offsetSecondary = offsetSecondary;
  }
}
