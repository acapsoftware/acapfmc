/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.model;

public class RunwayBuilder {
  private String primaryNumber;
  private String primaryDesignator;
  private float lat;
  private float lon;
  private float alt;
  private String surface;
  private float heading;
  private float length;
  private float width;
  private float patternAltitude;
  private boolean primaryTakeoff;
  private boolean primaryLanding;
  private boolean primaryPattern;
  private boolean secondaryTakeoff;
  private boolean secondaryLanding;
  private boolean secondaryPattern;
  private boolean threshold;
  private boolean edges;
  private boolean dashes;
  private boolean leadingZeroIdent;
  private boolean noThresholdEndArrows;
  private boolean fixedDistance;
  private boolean touchdown;
  private boolean ident;
  private boolean precision;
  private boolean primaryClosed;
  private boolean secondaryClosed;
  private boolean blastPad1end;
  private float blastPad1length;
  private boolean blastPad2end;
  private float blastPad2length;
  private boolean offset1end;
  private float offset1length;
  private boolean offset2end;
  private float offset2length;

  public RunwayBuilder setPrimaryNumber(String primaryNumber) {
    this.primaryNumber = primaryNumber;
    return this;
  }

  public RunwayBuilder setPrimaryDesignator(String primaryDesignator) {
    this.primaryDesignator = primaryDesignator;
    return this;
  }

  public RunwayBuilder setLat(float lat) {
    this.lat = lat;
    return this;
  }

  public RunwayBuilder setLon(float lon) {
    this.lon = lon;
    return this;
  }

  public RunwayBuilder setAlt(float alt) {
    this.alt = alt;
    return this;
  }

  public RunwayBuilder setSurface(String surface) {
    this.surface = surface;
    return this;
  }

  public RunwayBuilder setHeading(float heading) {
    this.heading = heading;
    return this;
  }

  public RunwayBuilder setLength(float length) {
    this.length = length;
    return this;
  }

  public RunwayBuilder setWidth(float width) {
    this.width = width;
    return this;
  }

  public RunwayBuilder setPatternAltitude(float patternAltitude) {
    this.patternAltitude = patternAltitude;
    return this;
  }

  public RunwayBuilder setPrimaryTakeoff(boolean primaryTakeoff) {
    this.primaryTakeoff = primaryTakeoff;
    return this;
  }

  public RunwayBuilder setPrimaryLanding(boolean primaryLanding) {
    this.primaryLanding = primaryLanding;
    return this;
  }

  public RunwayBuilder setPrimaryPattern(boolean primaryPattern) {
    this.primaryPattern = primaryPattern;
    return this;
  }

  public RunwayBuilder setSecondaryTakeoff(boolean secondaryTakeoff) {
    this.secondaryTakeoff = secondaryTakeoff;
    return this;
  }

  public RunwayBuilder setSecondaryLanding(boolean secondaryLanding) {
    this.secondaryLanding = secondaryLanding;
    return this;
  }

  public RunwayBuilder setSecondaryPattern(boolean secondaryPattern) {
    this.secondaryPattern = secondaryPattern;
    return this;
  }

  public RunwayBuilder setThreshold(boolean threshold) {
    this.threshold = threshold;
    return this;
  }

  public RunwayBuilder setEdges(boolean edges) {
    this.edges = edges;
    return this;
  }

  public RunwayBuilder setDashes(boolean dashes) {
    this.dashes = dashes;
    return this;
  }

  public RunwayBuilder setLeadingZeroIdent(boolean leadingZeroIdent) {
    this.leadingZeroIdent = leadingZeroIdent;
    return this;
  }

  public RunwayBuilder setNoThresholdEndArrows(boolean noThresholdEndArrows) {
    this.noThresholdEndArrows = noThresholdEndArrows;
    return this;
  }

  public RunwayBuilder setFixedDistance(boolean fixedDistance) {
    this.fixedDistance = fixedDistance;
    return this;
  }

  public RunwayBuilder setTouchdown(boolean touchdown) {
    this.touchdown = touchdown;
    return this;
  }

  public RunwayBuilder setIdent(boolean ident) {
    this.ident = ident;
    return this;
  }

  public RunwayBuilder setPrecision(boolean precision) {
    this.precision = precision;
    return this;
  }

  public RunwayBuilder setPrimaryClosed(boolean primaryClosed) {
    this.primaryClosed = primaryClosed;
    return this;
  }

  public RunwayBuilder setSecondaryClosed(boolean secondaryClosed) {
    this.secondaryClosed = secondaryClosed;
    return this;
  }

  public RunwayBuilder setBlastPad1end(boolean blastPad1end) {
    this.blastPad1end = blastPad1end;
    return this;
  }

  public RunwayBuilder setBlastPad1length(float blastPad1length) {
    this.blastPad1length = blastPad1length;
    return this;
  }

  public RunwayBuilder setBlastPad2end(boolean blastPad2end) {
    this.blastPad2end = blastPad2end;
    return this;
  }

  public RunwayBuilder setBlastPad2length(float blastPad2length) {
    this.blastPad2length = blastPad2length;
    return this;
  }

  public RunwayBuilder setOffset1end(boolean offset1end) {
    this.offset1end = offset1end;
    return this;
  }

  public RunwayBuilder setOffset1length(float offset1length) {
    this.offset1length = offset1length;
    return this;
  }

  public RunwayBuilder setOffset2end(boolean offset2end) {
    this.offset2end = offset2end;
    return this;
  }

  public RunwayBuilder setOffset2length(float offset2length) {
    this.offset2length = offset2length;
    return this;
  }

  public Runway createRunway() {
    return new Runway(
        primaryNumber,
        primaryDesignator,
        lat,
        lon,
        alt,
        surface,
        heading,
        length,
        width,
        patternAltitude,
        primaryTakeoff,
        primaryLanding,
        primaryPattern,
        secondaryTakeoff,
        secondaryLanding,
        secondaryPattern,
        threshold,
        edges,
        dashes,
        leadingZeroIdent,
        noThresholdEndArrows,
        fixedDistance,
        touchdown,
        ident,
        precision,
        primaryClosed,
        secondaryClosed,
        blastPad1end,
        blastPad1length,
        blastPad2end,
        blastPad2length,
        offset1end,
        offset1length,
        offset2end,
        offset2length);
  }
}
