/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.view;

import static acap.world.utilities.CalculationUtils.latToXCoord;
import static acap.world.utilities.CalculationUtils.lonToYCoord;
import static acap.world.view.WorldView.drawer;
import static com.jogamp.opengl.math.FloatUtil.sin;
import static processing.core.PApplet.cos;
import static processing.core.PApplet.radians;
import static processing.core.PConstants.ENABLE_DEPTH_TEST;

import acap.world.model.Aircraft;
import acap.world.model.World;

public class AircraftView {

  public AircraftView() {}

  void rotateXYZ(float xx, float yy, float zz) {
    float cx, cy, cz, sx, sy, sz;

    cx = cos(xx);
    cy = cos(yy);
    cz = cos(zz);
    sx = sin(xx);
    sy = sin(yy);
    sz = sin(zz);

    drawer.applyMatrix(
        cy * cz,
        (cz * sx * sy) - (cx * sz),
        (cx * cz * sy) + (sx * sz),
        0.0f,
        cy * sz,
        (cx * cz) + (sx * sy * sz),
        (-cz * sx) + (cx * sy * sz),
        0.0f,
        -sy,
        cy * sx,
        cx * cy,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        1.0f);
  }

  public void drawUserAircraft() {
    for (Aircraft aircraft : World.getInstance().getAircraft()) {
      drawer.hint(ENABLE_DEPTH_TEST);
      drawer.pushMatrix();
      drawer.translate(
          latToXCoord(aircraft.getLat()),
          lonToYCoord(aircraft.getLon()),
          (float) aircraft.getAltitude() + 2);

      rotateXYZ(
          radians((float) aircraft.getPitch()),
          radians((float) aircraft.getRoll()),
          radians(aircraft.getHeading() + 90));

      drawer.shape(aircraft.getModel());
      drawer.popMatrix();
    }
  }
}
