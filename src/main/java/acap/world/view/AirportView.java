/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.view;

import static acap.fmc.Constants.*;
import static acap.world.model.Camera.theta;
import static acap.world.model.Camera.zoom;
import static processing.core.PApplet.pow;
import static processing.core.PApplet.radians;
import static processing.core.PConstants.*;

import acap.world.utilities.CalculationUtils;
import acap.world.utilities.DrawingUtils;
import acap.world.model.*;
import java.util.concurrent.ConcurrentHashMap;
import processing.core.PVector;

/** Created by enadel on 10/6/16. */
public class AirportView {

  Airport mAirport;
  public static float identOpacity = 255;
  public static ConcurrentHashMap<PVector, Float> headingMap = new ConcurrentHashMap<>();

  public AirportView(Airport a) {
    mAirport = a;
  }

  public void drawMarker() {
    if (zoom > 400 && zoom <= (400 + pow((mAirport.longestRunway / 1000), 7))) {
      WorldView.drawer.fill(255);
      WorldView.drawer.noStroke();
      WorldView.drawer.textAlign(CENTER);
      WorldView.drawer.pushMatrix();
      WorldView.drawer.translate(
          CalculationUtils.latToXCoord((float) mAirport.lat),
          CalculationUtils.lonToYCoord((float) mAirport.lon),
          mAirport.alt);
      WorldView.drawer.ellipse(0, 0, 3 * (zoom - 400) + 8000, 3 * (zoom - 400) + 8000);
      WorldView.drawer.rotateZ(PI / 2);
      WorldView.drawer.translate(0, 20 * (zoom - 400) + 20000);
      WorldView.drawer.scale(1.5f * zoom + 500);
      WorldView.drawer.textSize(12);
      WorldView.drawer.text(mAirport.ident, 0, 0);
      WorldView.drawer.popMatrix();
    }
  }

  public void drawIdent() {
    if (zoom < 400) {

      WorldView.drawer.stroke(255, identOpacity);
      WorldView.drawer.fill(255, identOpacity);
      WorldView.drawer.textAlign(CENTER);
      WorldView.drawer.textSize(100);
      WorldView.drawer.pushMatrix();
      WorldView.drawer.translate(
          CalculationUtils.latToXCoord(mAirport.getLat()),
          CalculationUtils.lonToYCoord(mAirport.getLon()),
          mAirport.getAlt());
      WorldView.drawer.line(0, 0, 0, 0, 0, 0.5f * zoom + 5000f);
      WorldView.drawer.translate(0, 0, 0.5f * zoom + 6000f);
      WorldView.drawer.rotateX(-PI / 2);
      WorldView.drawer.rotateY(-radians(theta + 90));
      WorldView.drawer.scale(0.5f * zoom + 1);
      WorldView.drawer.text(mAirport.getIdent(), 0, 0);
      WorldView.drawer.popMatrix();
    }
  }

  public void drawTaxiNodes() {

    if (!renderMode) {

      WorldView.drawer.textSize(20);
      WorldView.drawer.noStroke();
      WorldView.drawer.rectMode(CENTER);

      for (TaxiNode taxiNode : mAirport.getTaxiNodes()) {

        if (taxiNode.getType().equals("HOLD_SHORT")) WorldView.drawer.fill(255);
        else if (taxiNode.getType().equals("ILS_HOLD_SHORT")) WorldView.drawer.fill(255);
        else if (taxiNode.getType().equals("ILS_HOLD_SHORT_NO_DRAW"))
          WorldView.drawer.fill(0, 255, 255);
        else if (taxiNode.getType().equals("HOLD_SHORT_NO_DRAW"))
          WorldView.drawer.fill(0, 255, 255);
        else if (taxiNode.getType().equals("NORMAL")) WorldView.drawer.fill(50, 50, 255);
        else WorldView.drawer.fill(255);

        WorldView.drawer.pushMatrix();
        WorldView.drawer.translate(
            CalculationUtils.latToXCoord(taxiNode.getLat()),
            CalculationUtils.lonToYCoord(taxiNode.getLon()),
            mAirport.alt + 2);
        WorldView.drawer.ellipse(0, 0, 50, 50);
        WorldView.drawer.rotateZ(PI / 2);
        WorldView.drawer.text(taxiNode.getIndex(), 30, 30);
        WorldView.drawer.popMatrix();
      }
    }
  }

  public void drawTaxiPaths() {

    WorldView.drawer.textSize(40);
    WorldView.drawer.strokeWeight(1);
    WorldView.drawer.fill(255);
    WorldView.drawer.rectMode(CENTER);

    for (TaxiPath taxiPath : mAirport.getTaxiPaths()) {

      if (taxiPath.getType().equalsIgnoreCase("VEHICLE")) WorldView.drawer.stroke(255, 0, 255);
      else if (taxiPath.getType().equalsIgnoreCase("TAXI")) WorldView.drawer.stroke(0, 0, 255);
      else if (taxiPath.getType().equalsIgnoreCase("PARKING")) WorldView.drawer.stroke(80, 255, 80);
      else if (taxiPath.getType().equalsIgnoreCase("RUNWAY")) WorldView.drawer.stroke(0, 0, 80);
      else if (taxiPath.getType().equalsIgnoreCase("CLOSED")) WorldView.drawer.stroke(255, 80, 80);
      else if (taxiPath.getType().equalsIgnoreCase("PATH")) WorldView.drawer.stroke(255, 255, 255);
      else if (WorldView.drawer.frameCount % 2 == 0) WorldView.drawer.fill(255);
      else WorldView.drawer.fill(255, 0, 0);

      WorldView.drawer.pushMatrix();

      float startLocationLat =
          CalculationUtils.latToXCoord(
              mAirport.getTaxiwayIndex().get(taxiPath.getStart()).getLat());
      float startLocationLon =
          CalculationUtils.lonToYCoord(
              mAirport.getTaxiwayIndex().get(taxiPath.getStart()).getLon());

      WorldView.drawer.translate(startLocationLat, startLocationLon, mAirport.getAlt() - 1);

      //centerlines
      if (taxiPath.getType().equalsIgnoreCase("PARKING")) {
        if (renderMode) WorldView.drawer.stroke(255, 0, 255);
        if (zoom < taxiwayCenterlineDrawDistance)
          WorldView.drawer.line(
              0,
              0,
              CalculationUtils.latToXCoord(
                      mAirport.getParkingIndex().get(taxiPath.getEnd()).getLat())
                  - startLocationLat,
              CalculationUtils.lonToYCoord(
                      mAirport.getParkingIndex().get(taxiPath.getEnd()).getLon())
                  - startLocationLon);
      } else if (taxiPath.getType().equalsIgnoreCase("RUNWAY")) {
        if (renderMode) WorldView.drawer.noStroke();
        if (zoom < taxiwayCenterlineDrawDistance)
          WorldView.drawer.line(
              0,
              0,
              CalculationUtils.latToXCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLat())
                  - startLocationLat,
              CalculationUtils.lonToYCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLon())
                  - startLocationLon);
      } else if (taxiPath.getType().equalsIgnoreCase("PATH")) {
        if (renderMode) WorldView.drawer.stroke(255, 0, 255);
        if (zoom < taxiwayCenterlineDrawDistance)
          WorldView.drawer.line(
              0,
              0,
              CalculationUtils.latToXCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLat())
                  - startLocationLat,
              CalculationUtils.lonToYCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLon())
                  - startLocationLon);
      } else if (taxiPath.getType().equalsIgnoreCase("VEHICLE")) {
        if (renderMode) WorldView.drawer.stroke(255, 255, 255);
        if (zoom < taxiwayCenterlineDrawDistance)
          WorldView.drawer.line(
              0,
              0,
              CalculationUtils.latToXCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLat())
                  - startLocationLat,
              CalculationUtils.lonToYCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLon())
                  - startLocationLon);
      } else {
        if (renderMode) WorldView.drawer.stroke(255, 0, 255);
        if (zoom < taxiwayCenterlineDrawDistance)
          WorldView.drawer.line(
              0,
              0,
              CalculationUtils.latToXCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLat())
                  - startLocationLat,
              CalculationUtils.lonToYCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLon())
                  - startLocationLon);

        WorldView.drawer.noStroke();
        WorldView.drawer.fill(50, 50, 50);
        WorldView.drawer.ellipse(0, 0, taxiPath.getTaxiWidth(), taxiPath.getTaxiWidth());

        WorldView.drawer.translate(
            (CalculationUtils.latToXCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLat())
                    - startLocationLat)
                * 0.5f,
            (CalculationUtils.lonToYCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLon())
                    - startLocationLon)
                * 0.5f,
            -1);

        PVector pathVector =
            new PVector(
                CalculationUtils.latToXCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLat())
                    - startLocationLat,
                CalculationUtils.lonToYCoord(mAirport.taxiwayIndex.get(taxiPath.getEnd()).getLon())
                    - startLocationLon);

        WorldView.drawer.ellipse(
            pathVector.x / 2f, pathVector.y / 2f, taxiPath.getTaxiWidth(), taxiPath.getTaxiWidth());

        WorldView.drawer.rotateZ(headingMap.computeIfAbsent(pathVector, PVector::heading));

        WorldView.drawer.rect(0, 0, pathVector.mag(), taxiPath.getTaxiWidth());

        if (zoom < taxiwayCenterlineDrawDistance) {
          WorldView.drawer.rotateZ(-pathVector.heading());
          WorldView.drawer.rotateZ(-headingMap.computeIfAbsent(pathVector, PVector::heading));
          WorldView.drawer.rotateZ(PI / 2 + radians(theta));
          WorldView.drawer.fill(255, 255, 255);
          WorldView.drawer.text(taxiPath.getName(), -5, -5, 5);
        }
      }
      WorldView.drawer.popMatrix();
    }
  }

  public void drawParking() {

    WorldView.drawer.textSize(20);
    WorldView.drawer.textAlign(LEFT);
    WorldView.drawer.rectMode(CENTER);
    WorldView.drawer.noFill();

    zoom = zoom;

    WorldView.drawer.stroke(255, 0, 0);

    if (zoom < taxiwayCenterlineDrawDistance)
      for (Parking parkingSpot : mAirport.getParking()) {

        WorldView.drawer.stroke(50, 255, 50);
        WorldView.drawer.pushMatrix();
        WorldView.drawer.translate(
            CalculationUtils.latToXCoord(parkingSpot.getLat()),
            CalculationUtils.lonToYCoord(parkingSpot.getLon()),
            mAirport.getAlt() - 1);
        WorldView.drawer.ellipse(0, 0, parkingSpot.getRadius() * 2f, parkingSpot.getRadius() * 2f);
        WorldView.drawer.rotate(radians(parkingSpot.getHeading() - 90));
        WorldView.drawer.stroke(255, 255, 0);
        WorldView.drawer.line(
            -parkingSpot.getRadius() * 0.5f, 0, parkingSpot.getRadius() * 0.5f, 0);

        //used to draw aircraft at parking stands
        //                drawer.translate(0,0,6);
        //                drawer.shape(triShape);
        //                drawer.translate(0,0,-6);

        if (showParkingNames) {
          WorldView.drawer.stroke(50, 255, 50);
          WorldView.drawer.rotateZ(PI);
          WorldView.drawer.text(
              parkingSpot.getName() + " " + parkingSpot.getNumber(),
              -parkingSpot.getRadius() * 1.2f,
              -parkingSpot.getRadius() * 1.2f);
        }
        WorldView.drawer.popMatrix();
      }
  }

  public void drawBoundaryFence() {

    WorldView.drawer.stroke(255, lineOpacity);
    WorldView.drawer.noFill();

    for (BoundaryFence boundaryFence : mAirport.getBoundaryFences()) {
      WorldView.drawer.beginShape();
      for (PVector vertex : boundaryFence.getVerticies())
        WorldView.drawer.vertex(
            CalculationUtils.latToXCoord(vertex.x),
            CalculationUtils.lonToYCoord(vertex.y),
            mAirport.getAlt());
      WorldView.drawer.endShape();
    }
  }

  public void drawAprons() {

    WorldView.drawer.noStroke();
    WorldView.drawer.fill(80);

    for (Apron apron : mAirport.getAprons()) {
      WorldView.drawer.beginShape();
      for (PVector vertex : apron.getVerticies())
        WorldView.drawer.vertex(
            CalculationUtils.latToXCoord(vertex.x),
            CalculationUtils.lonToYCoord(vertex.y),
            mAirport.getAlt() - 4);
      WorldView.drawer.endShape(CLOSE);
    }
  }

  public void drawTower() {
    WorldView.drawer.stroke(255, lineOpacity);
    WorldView.drawer.noFill();
    int floor = (int) (mAirport.longestRunway * 0.025);
    int top = (int) (mAirport.longestRunway * 0.025 + mAirport.longestRunway * 0.003);
    WorldView.drawer.pushMatrix();
    WorldView.drawer.translate(
        CalculationUtils.latToXCoord(mAirport.getTower().x),
        CalculationUtils.lonToYCoord(mAirport.getTower().y),
        mAirport.getAlt());
    WorldView.drawer.line(-10, -10, 0, -10, -10, floor);
    WorldView.drawer.line(10, -10, 0, 10, -10, floor);
    WorldView.drawer.line(-10, 10, 0, -10, 10, floor);
    WorldView.drawer.line(10, 10, 0, 10, 10, floor);
    DrawingUtils.polygon(0, 0, floor, 20, 6);
    DrawingUtils.polygon(0, 0, top, 30, 6);
    WorldView.drawer.line(20, 0, floor, 30, 0, top);
    for (int i = 0; i < 5; i++) {
      WorldView.drawer.rotate(PI / 3);
      WorldView.drawer.line(20, 0, floor, 30, 0, top);
    }
    WorldView.drawer.popMatrix();
  }

  public void drawSIDS() {

    WorldView.drawer.fill(0);
    WorldView.drawer.stroke(255, 0, 255);
    WorldView.drawer.strokeWeight(1);

    for (SID sid : mAirport.getSIDs())
      if (sid.getName().contains("13L.CISRO8"))
        for (int i = 1; i < sid.getWaypoints().size(); i++) {
          WorldView.drawer.line(
              CalculationUtils.latToXCoord(sid.getWaypoints().get(i - 1).getLocation().x),
              CalculationUtils.lonToYCoord(sid.getWaypoints().get(i - 1).getLocation().y),
              mAirport.alt,
              CalculationUtils.latToXCoord(sid.getWaypoints().get(i).getLocation().x),
              CalculationUtils.lonToYCoord(sid.getWaypoints().get(i).getLocation().y),
              mAirport.alt);
        }

    WorldView.drawer.strokeWeight(1);
  }
}
