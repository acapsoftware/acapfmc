/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.view;

import static acap.fmc.Constants.*;
import static acap.fmc.Constants.MILE;
import static acap.world.utilities.CalculationUtils.latToXCoord;
import static acap.world.utilities.CalculationUtils.lonToYCoord;
import static acap.world.model.Camera.*;
import static processing.core.PConstants.CENTER;
import static processing.core.PConstants.CORNERS;

import acap.fmc.gui.processing.apps.GLWorld;
import acap.world.utilities.NavDB;
import acap.world.model.Airport;
import acap.world.model.World;
import processing.core.PApplet;

/** Created by enadel on 10/6/16. */
public class WorldView {
  public static PApplet drawer = GLWorld.getInstance();

  public WorldView() {
    setCameraToStartingAirport();
  }

  private void setCameraToStartingAirport() {
    selectedAirport = NavDB.getInstance().getAirport(PLANE_START);
    cameraMode = CamMode.AIRPORT;
    x = latToXCoord((float) selectedAirport.lat);
    y = lonToYCoord((float) selectedAirport.lon);
    z = selectedAirport.alt;
  }

  public void drawAirports() {

    drawer.rectMode(CENTER);

    lineOpacity = -0.00900f * distanceToSelectedAirport + 255;
    identOpacity = -0.005f * camZ + 2000;
    drawer.textFont(GLWorld.groundFont);

    for (Airport airport : World.getInstance().getAirports()) {
      AirportView aView = new AirportView(airport);
      RunwayView rView = new RunwayView(airport.getRunways());

      if (zoom < 400) {
        aView.drawIdent();
        aView.drawBoundaryFence();
        rView.drawRunways();
        if (distanceToSelectedAirport < 5 * MILE) {
          aView.drawTaxiNodes();
          aView.drawParking();
          aView.drawAprons();
          aView.drawTaxiPaths();
          aView.drawTower();
          aView.drawSIDS();
        }
      } else {
        aView.drawMarker();
      }
    }
  }

  public void drawSky() {
    if (drawSky) {
      drawer.background(0, 50, 0);
      drawer.noStroke();
      drawer.fill(135, 206, 235);
      drawer.rectMode(CORNERS);
      drawer.rect(
          -100,
          0,
          drawer.displayWidth,
          drawer.displayHeight - 680 - (drawer.displayHeight / 2) * (phi / 30));
      drawer.rectMode(CENTER);

    } else drawer.background(0);
  }
}
