/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.view;

import static acap.world.model.Camera.*;
import static acap.world.model.Camera.cameraMode;
import static acap.world.view.WorldView.drawer;
import static controlP5.ControlP5Constants.LEFT;
import static processing.core.PConstants.CORNERS;
import static processing.core.PConstants.DISABLE_DEPTH_TEST;

import acap.fmc.Constants;
import acap.world.utilities.NavDB;
import acap.world.model.World;

/** Created by erikn on 2/12/2017. */
public class WorldOverlay {

  public static final String COPYRIGHT_TEXT =
      "Copyright 2016 Nicholas C. Cyganski  -  THE AUTONOMOUS CARGO AIRCRAFT PROJECT (ACAP)";

  public void drawOverlay() {

    initializeOverlay();

    drawer.fill(255);

    displayCameraPosition();
    drawer.text("Airports: " + NavDB.getInstance().getNumAirports(), 10, 65 + drawer.height - 125);
    drawer.text(
        "Aircraft: " + World.getInstance().getAircraft().size(), 10, 80 + drawer.height - 125);

    drawer.fill(255);
    drawer.text(COPYRIGHT_TEXT + Constants.version, 10, drawer.height - 10);

    drawer.textSize(18);
    drawer.fill(255);

    displayCameraState();
  }

  private void initializeOverlay() {
    drawer.hint(DISABLE_DEPTH_TEST);
    drawer.noLights();
    drawer.textAlign(LEFT);
    drawer.textSize(12);
    drawer.stroke(255);
    drawer.rectMode(CORNERS);
  }

  private void displayCameraPosition() {
    drawer.text("zoom/1000: " + zoom, 10, 20 + drawer.height - 125);
    drawer.text("Latitude: " + x, 10, 35 + drawer.height - 125);
    drawer.text("Longitude: " + y, 10, 50 + drawer.height - 125);
  }

  private void displayCameraState() {
    drawer.text("Camera: ", drawer.width - 400, drawer.height - 45);
    if (cameraMode == CamMode.FREE) {
      drawer.fill(80, 80, 255);
      drawer.text("Free", drawer.width - 330, drawer.height - 45);
    }
    if (cameraMode == CamMode.AIRPORT) {
      drawer.fill(80, 255, 80);
      drawer.text("Airport", drawer.width - 330, drawer.height - 45);
    }
    if (cameraMode == CamMode.AIRCRAFT) {
      drawer.fill(255, 80, 80);
      drawer.text("Aircraft", drawer.width - 330, drawer.height - 45);
    }
    if (cameraMode == CamMode.OVERVIEW) {
      drawer.fill(255, 80, 80);
      drawer.text("Overview", drawer.width - 330, drawer.height - 45);
    }
  }
}
