/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.view;

import static acap.fmc.Constants.lineOpacity;
import static acap.world.view.WorldView.drawer;
import static processing.core.PApplet.radians;
import static processing.core.PConstants.*;

import acap.world.utilities.CalculationUtils;
import acap.world.model.Runway;
import java.util.ArrayList;

/** Created by enadel on 10/6/16. */
public class RunwayView {

  ArrayList<Runway> runways;

  RunwayView(ArrayList<Runway> r) {
    this.runways = r;
  }

  public void drawRunways() {

    drawer.textAlign(CENTER);
    drawer.rectMode(CENTER);
    for (Runway runway : this.runways) {

      if (runway.getWidth() >= 50) drawer.textSize(60);
      else drawer.textSize(30);
      drawer.noStroke();
      drawer.fill(50, 50, 50);
      drawer.pushMatrix();
      drawer.translate(
          CalculationUtils.latToXCoord(runway.getLat()),
          CalculationUtils.lonToYCoord(runway.getLon()),
          runway.getAlt() - 2);
      drawer.rotate(radians(runway.getHeading() - 90));
      drawer.rect(0, 0, runway.getWidth(), runway.getLength());

      drawer.translate(0, -runway.getLength() / 2, 1);

      drawer.fill(255, 255, 255);
      drawer.stroke(255, 255, 255);
      drawer.strokeWeight(1);

      if (runway.isIdent()) {

        //secondarySide
        if (runway.getSecondaryDesignator().equalsIgnoreCase(""))
          drawer.text(
              runway.getSecondaryNumber(),
              0,
              runway.getLength() - 130 - runway.getOffsetSecondary(),
              2);
        else {
          drawer.text(
              runway.getSecondaryNumber(),
              0,
              runway.getLength() - 200 - runway.getOffsetSecondary(),
              2);
          drawer.text(
              runway.getSecondaryDesignator(),
              0,
              runway.getLength() - 130 - runway.getOffsetSecondary(),
              2);
        }

        drawer.pushMatrix();
        drawer.translate(0, runway.getLength(), 0);
        drawer.rotate(PI);

        //primarySide
        if (runway.getPrimaryDesignator().equalsIgnoreCase(""))
          drawer.text(
              runway.getPrimaryNumber(),
              0,
              runway.getLength() - 130 - runway.getOffsetPrimary(),
              5);
        else {
          drawer.text(
              runway.getPrimaryNumber(),
              0,
              runway.getLength() - 200 - runway.getOffsetPrimary(),
              5);
          drawer.text(
              runway.getPrimaryDesignator(),
              0,
              runway.getLength() - 130 - runway.getOffsetPrimary(),
              5);
        }
        drawer.popMatrix();
      }

      if (runway.isDashes()) {
        drawer.stroke(255, lineOpacity);

        if (runway.getPrimaryDesignator().equalsIgnoreCase("")) {
          int numDash =
              (int)
                      ((runway.getLength()
                              - 100
                              - runway.getOffsetPrimary()
                              - runway.getOffsetSecondary())
                          / 200)
                  - 1;

          for (int i = 0; i < numDash; i++) {
            drawer.line(
                0,
                runway.getOffsetPrimary() + 200 + i * 200,
                0,
                runway.getOffsetPrimary() + 200 + ((i + 1) * 200) - 80);
          }
        } else {
          int numDash =
              (int)
                      ((runway.getLength()
                              - 200
                              - runway.getOffsetPrimary()
                              - runway.getOffsetSecondary())
                          / 200)
                  - 1;

          for (int i = 0; i < numDash; i++) {
            drawer.line(
                0,
                runway.getOffsetPrimary() + 300 + i * 200,
                0,
                runway.getOffsetPrimary() + 300 + ((i + 1) * 200) - 80);
          }
        }
      }

      if (runway.isTouchdown()) {
        drawer.noStroke();
        drawer.fill(255, lineOpacity);

        if (runway.getWidth() < 60) {
          drawer.rect(
              -runway.getWidth() / 4, runway.getOffsetPrimary() + 1075, runway.getWidth() / 4, 150);
          drawer.rect(
              runway.getWidth() / 4, runway.getOffsetPrimary() + 1075, runway.getWidth() / 4, 150);
          drawer.rect(
              -runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 1075,
              runway.getWidth() / 4,
              150);
          drawer.rect(
              runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 1075,
              runway.getWidth() / 4,
              150);
        } else {
          drawer.rect(-runway.getWidth() / 4, runway.getOffsetPrimary() + 1075, 20, 150);
          drawer.rect(runway.getWidth() / 4, runway.getOffsetPrimary() + 1075, 20, 150);
          drawer.rect(
              -runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 1075,
              20,
              150);
          drawer.rect(
              runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 1075,
              20,
              150);
        }
      }

      if (runway.isFixedDistance()) {
        drawer.noStroke();
        drawer.fill(255, lineOpacity);

        if (runway.getLength() - runway.getOffsetPrimary() - runway.getOffsetSecondary() >= 7000) {

          drawer.rect(-runway.getWidth() / 4 - 8, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(-runway.getWidth() / 4, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(-runway.getWidth() / 4 + 8, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(runway.getWidth() / 4 - 8, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(runway.getWidth() / 4, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(runway.getWidth() / 4 + 8, runway.getOffsetPrimary() + 537, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4 - 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              -runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              -runway.getWidth() / 4 + 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 - 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4, runway.getLength() - runway.getOffsetSecondary() - 537, 4, 75);
          drawer.rect(
              runway.getWidth() / 4 + 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);

          drawer.rect(-runway.getWidth() / 4 - 4, runway.getOffsetPrimary() + 1537, 4, 75);
          drawer.rect(-runway.getWidth() / 4 + 4, runway.getOffsetPrimary() + 1537, 4, 75);
          drawer.rect(runway.getWidth() / 4 - 4, runway.getOffsetPrimary() + 1537, 4, 75);
          drawer.rect(runway.getWidth() / 4 + 4, runway.getOffsetPrimary() + 1537, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4 - 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);
          drawer.rect(
              -runway.getWidth() / 4 + 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 - 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 + 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);

          drawer.rect(-runway.getWidth() / 4 - 4, runway.getOffsetPrimary() + 2037, 4, 75);
          drawer.rect(-runway.getWidth() / 4 + 4, runway.getOffsetPrimary() + 2037, 4, 75);
          drawer.rect(runway.getWidth() / 4 - 4, runway.getOffsetPrimary() + 2037, 4, 75);
          drawer.rect(runway.getWidth() / 4 + 4, runway.getOffsetPrimary() + 2037, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4 - 4,
              runway.getLength() - runway.getOffsetSecondary() - 2037,
              4,
              75);
          drawer.rect(
              -runway.getWidth() / 4 + 4,
              runway.getLength() - runway.getOffsetSecondary() - 2037,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 - 4,
              runway.getLength() - runway.getOffsetSecondary() - 2037,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 + 4,
              runway.getLength() - runway.getOffsetSecondary() - 2037,
              4,
              75);

          drawer.rect(-runway.getWidth() / 4, runway.getOffsetPrimary() + 2537, 4, 75);
          drawer.rect(runway.getWidth() / 4, runway.getOffsetPrimary() + 2537, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 2537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 2537,
              4,
              75);

          drawer.rect(-runway.getWidth() / 4, runway.getOffsetPrimary() + 3037, 4, 75);
          drawer.rect(runway.getWidth() / 4, runway.getOffsetPrimary() + 3037, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 3037,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 3037,
              4,
              75);

        } else {

          drawer.rect(-runway.getWidth() / 4 - 8, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(-runway.getWidth() / 4, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(-runway.getWidth() / 4 + 8, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(runway.getWidth() / 4 - 8, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(runway.getWidth() / 4, runway.getOffsetPrimary() + 537, 4, 75);
          drawer.rect(runway.getWidth() / 4 + 8, runway.getOffsetPrimary() + 537, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4 - 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              -runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              -runway.getWidth() / 4 + 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 - 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4, runway.getLength() - runway.getOffsetSecondary() - 537, 4, 75);
          drawer.rect(
              runway.getWidth() / 4 + 8,
              runway.getLength() - runway.getOffsetSecondary() - 537,
              4,
              75);

          drawer.rect(-runway.getWidth() / 4 - 4, runway.getOffsetPrimary() + 1537, 4, 75);
          drawer.rect(-runway.getWidth() / 4 + 4, runway.getOffsetPrimary() + 1537, 4, 75);
          drawer.rect(runway.getWidth() / 4 - 4, runway.getOffsetPrimary() + 1537, 4, 75);
          drawer.rect(runway.getWidth() / 4 + 4, runway.getOffsetPrimary() + 1537, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4 - 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);
          drawer.rect(
              -runway.getWidth() / 4 + 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 - 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4 + 4,
              runway.getLength() - runway.getOffsetSecondary() - 1537,
              4,
              75);

          drawer.rect(-runway.getWidth() / 4, runway.getOffsetPrimary() + 2037, 4, 75);
          drawer.rect(runway.getWidth() / 4, runway.getOffsetPrimary() + 2037, 4, 75);

          drawer.rect(
              -runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 2037,
              4,
              75);
          drawer.rect(
              runway.getWidth() / 4,
              runway.getLength() - runway.getOffsetSecondary() - 2037,
              4,
              75);
        }
      }

      if (runway.isEdges()) {
        drawer.noFill();
        drawer.stroke(255, lineOpacity);
        drawer.rectMode(CORNERS);
        drawer.rect(-runway.getWidth() * 0.46f, 0, runway.getWidth() * 0.46f, runway.getLength());
        drawer.fill(255, lineOpacity);
        drawer.rectMode(CENTER);
        drawer.rect(0, runway.getOffsetPrimary(), runway.getWidth() * 0.92f, 3);
      }

      if (runway.isThreshold()) {
        drawer.fill(255, lineOpacity);
        drawer.noStroke();
        drawer.translate(0, 0, 1);
        drawer.rectMode(CENTER);

        int numStripes = ((((int) (runway.getWidth() - 50)) / 50) * 2) + 2;

        for (int i = 0; i < numStripes; i++) {
          //primary end
          drawer.rect(
              -((runway.getWidth() / numStripes / 75) + 10) * i - 6,
              runway.getOffsetPrimary() + 65,
              4,
              100);
          drawer.rect(
              ((runway.getWidth() / numStripes / 75) + 10) * i + 6,
              runway.getOffsetPrimary() + 65,
              4,
              100);
          //secondary end
          drawer.rect(
              -((runway.getWidth() / numStripes / 75) + 10) * i - 6,
              runway.getLength() - runway.getOffsetSecondary() - 65,
              4,
              100);
          drawer.rect(
              ((runway.getWidth() / numStripes / 75) + 10) * i + 6,
              runway.getLength() - runway.getOffsetSecondary() - 65,
              4,
              100);
        }
      }

      if (runway.getBlastpadPrimary() > 0) {
        drawer.noStroke();
        drawer.rectMode(CENTER);
        drawer.translate(0, 0, -1);
        drawer.fill(50, lineOpacity);
        drawer.rect(
            0, -runway.getBlastpadPrimary() / 2, runway.getWidth(), runway.getBlastpadPrimary());
        drawer.translate(0, 0, 2);
        drawer.fill(140, lineOpacity);
        drawer.rect(
            0,
            -runway.getBlastpadPrimary() / 2,
            runway.getWidth() - 30,
            runway.getBlastpadPrimary() - 40);

        int numChevron = (int) (runway.getBlastpadPrimary() - 40) / 100;
        drawer.stroke(200, 200, 0, lineOpacity);
        drawer.strokeWeight(2);

        drawer.line(
            -(runway.getWidth() - 30) / 4,
            -20,
            -(runway.getWidth() - 30) / 2,
            -(runway.getWidth() - 30) / 2);
        drawer.line(
            (runway.getWidth() - 30) / 4,
            -20,
            (runway.getWidth() - 30) / 2,
            -(runway.getWidth() - 30) / 2);

        for (int i = 0; i < numChevron; i++) {
          drawer.line(
              0,
              -70 - (100 * i),
              -(runway.getWidth() - 30) / 2,
              -30 - (runway.getWidth() - 30) - (100 * i));
          drawer.line(
              0,
              -70 - (100 * i),
              (runway.getWidth() - 30) / 2,
              -30 - (runway.getWidth() - 30) - (100 * i));
        }
      }

      if (runway.getOffsetPrimary() > 0) {
        drawer.stroke(255, lineOpacity);
        drawer.strokeWeight(1);
        drawer.fill(255, lineOpacity);

        if (runway.getWidth() < 40) {
          drawer.rect(0, runway.getOffsetPrimary(), runway.getWidth() * 0.92f, 3);
        } else if (runway.getWidth() <= 120) {
          drawer.line(
              runway.getWidth() / 4,
              runway.getOffsetPrimary(),
              runway.getWidth() / 2f * 0.75f - 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              runway.getWidth() / 4,
              runway.getOffsetPrimary(),
              runway.getWidth() / 2f * 0.75f + 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              -runway.getWidth() / 4,
              runway.getOffsetPrimary(),
              -runway.getWidth() / 2 * 0.25f - 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              -runway.getWidth() / 4,
              runway.getOffsetPrimary(),
              -runway.getWidth() / 2 * 0.25f + 8,
              runway.getOffsetPrimary() - 45);
          drawer.rect(0, runway.getOffsetPrimary(), runway.getWidth() * 0.92f, 3);
        } else {
          drawer.line(
              runway.getWidth() / 2 * 0.75f,
              runway.getOffsetPrimary(),
              runway.getWidth() / 2 * 0.75f - 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              runway.getWidth() / 2 * 0.75f,
              runway.getOffsetPrimary(),
              runway.getWidth() / 2 * 0.75f + 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              runway.getWidth() / 2 * 0.25f,
              runway.getOffsetPrimary(),
              runway.getWidth() / 2 * 0.25f - 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              runway.getWidth() / 2 * 0.25f,
              runway.getOffsetPrimary(),
              runway.getWidth() / 2 * 0.25f + 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              -runway.getWidth() / 2 * 0.25f,
              runway.getOffsetPrimary(),
              -runway.getWidth() / 2 * 0.25f - 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              -runway.getWidth() / 2 * 0.25f,
              runway.getOffsetPrimary(),
              -runway.getWidth() / 2 * 0.25f + 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              -runway.getWidth() / 2 * 0.75f,
              runway.getOffsetPrimary(),
              -runway.getWidth() / 2 * 0.75f - 8,
              runway.getOffsetPrimary() - 45);
          drawer.line(
              -runway.getWidth() / 2 * 0.75f,
              runway.getOffsetPrimary(),
              -runway.getWidth() / 2 * 0.75f + 8,
              runway.getOffsetPrimary() - 45);
          drawer.rect(0, runway.getOffsetPrimary(), runway.getWidth() * 0.92f, 3);
        }

        int numArrow = (int) (runway.getOffsetPrimary()) / 200;

        for (int i = 0; i < numArrow; i++) {
          drawer.line(
              0,
              runway.getOffsetPrimary() - 80 - (200 * i),
              -8,
              runway.getOffsetPrimary() - 125 - (200 * i));
          drawer.line(
              0,
              runway.getOffsetPrimary() - 80 - (200 * i),
              8,
              runway.getOffsetPrimary() - 125 - (200 * i));
          drawer.line(
              0,
              runway.getOffsetPrimary() - 120 - (200 * i),
              0,
              runway.getOffsetPrimary() - 200 - (200 * i));
          drawer.rect(0, runway.getOffsetPrimary(), runway.getWidth() * 0.92f, 3);
        }
      }

      //switch to other side
      drawer.translate(0, runway.getLength(), -1);
      drawer.rotate(PI);

      if (runway.getBlastpadSecondary() > 0) {
        drawer.noStroke();
        drawer.rectMode(CENTER);
        drawer.translate(0, 0, -1);
        drawer.fill(50, lineOpacity);
        drawer.rect(
            0,
            -runway.getBlastpadSecondary() / 2,
            runway.getWidth(),
            runway.getBlastpadSecondary());
        drawer.translate(0, 0, 2);
        drawer.fill(140, lineOpacity);
        drawer.rect(
            0,
            -runway.getBlastpadSecondary() / 2,
            runway.getWidth() - 30,
            runway.getBlastpadSecondary() - 40);

        int numChevron = (int) (runway.getBlastpadSecondary() - 40) / 100;
        drawer.stroke(200, 200, 0, lineOpacity);
        drawer.strokeWeight(2);

        drawer.line(
            -(runway.getWidth() - 30) / 4,
            -20,
            -(runway.getWidth() - 30) / 2,
            -(runway.getWidth() - 30) / 2);
        drawer.line(
            (runway.getWidth() - 30) / 4,
            -20,
            (runway.getWidth() - 30) / 2,
            -(runway.getWidth() - 30) / 2);

        for (int i = 0; i < numChevron; i++) {
          drawer.line(
              0,
              -70 - (100 * i),
              -(runway.getWidth() - 30) / 2,
              -30 - (runway.getWidth() - 30) - (100 * i));
          drawer.line(
              0,
              -70 - (100 * i),
              (runway.getWidth() - 30) / 2,
              -30 - (runway.getWidth() - 30) - (100 * i));
        }
      }

      if (runway.getOffsetSecondary() > 0) {
        drawer.stroke(255, lineOpacity);
        drawer.strokeWeight(1);
        drawer.fill(255, lineOpacity);

        if (!runway.isNoThresholdEndArrows()) {

          if (runway.getWidth() < 40) {
            drawer.rect(0, runway.getOffsetSecondary(), runway.getWidth() * 0.92f, 3);
          } else if (runway.getWidth() <= 120) {
            drawer.line(
                runway.getWidth() / 4,
                runway.getOffsetSecondary(),
                runway.getWidth() / 2 * 0.75f - 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                runway.getWidth() / 4,
                runway.getOffsetSecondary(),
                runway.getWidth() / 2 * 0.75f + 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                -runway.getWidth() / 4,
                runway.getOffsetSecondary(),
                -runway.getWidth() / 2 * 0.25f - 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                -runway.getWidth() / 4,
                runway.getOffsetSecondary(),
                -runway.getWidth() / 2 * 0.25f + 8,
                runway.getOffsetSecondary() - 45);
            drawer.rect(0, runway.getOffsetSecondary(), runway.getWidth() * 0.92f, 3);
          } else {
            drawer.line(
                runway.getWidth() / 2 * 0.75f,
                runway.getOffsetSecondary(),
                runway.getWidth() / 2 * 0.75f - 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                runway.getWidth() / 2 * 0.75f,
                runway.getOffsetSecondary(),
                runway.getWidth() / 2 * 0.75f + 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                runway.getWidth() / 2 * 0.25f,
                runway.getOffsetSecondary(),
                runway.getWidth() / 2 * 0.25f - 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                runway.getWidth() / 2 * 0.25f,
                runway.getOffsetSecondary(),
                runway.getWidth() / 2 * 0.25f + 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                -runway.getWidth() / 2 * 0.25f,
                runway.getOffsetSecondary(),
                -runway.getWidth() / 2 * 0.25f - 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                -runway.getWidth() / 2 * 0.25f,
                runway.getOffsetSecondary(),
                -runway.getWidth() / 2 * 0.25f + 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                -runway.getWidth() / 2 * 0.75f,
                runway.getOffsetSecondary(),
                -runway.getWidth() / 2 * 0.75f - 8,
                runway.getOffsetSecondary() - 45);
            drawer.line(
                -runway.getWidth() / 2 * 0.75f,
                runway.getOffsetSecondary(),
                -runway.getWidth() / 2 * 0.75f + 8,
                runway.getOffsetSecondary() - 45);
            drawer.rect(0, runway.getOffsetSecondary(), runway.getWidth() * 0.92f, 3);
          }

          int numArrow = (int) (runway.getOffsetSecondary()) / 200;

          for (int i = 0; i < numArrow; i++) {
            drawer.line(
                0,
                runway.getOffsetSecondary() - 80 - (200 * i),
                -8,
                runway.getOffsetSecondary() - 125 - (200 * i));
            drawer.line(
                0,
                runway.getOffsetSecondary() - 80 - (200 * i),
                8,
                runway.getOffsetSecondary() - 125 - (200 * i));
            drawer.line(
                0,
                runway.getOffsetSecondary() - 120 - (200 * i),
                0,
                runway.getOffsetSecondary() - 200 - (200 * i));
          }
        }
      }

      drawer.popMatrix();
    }
  }
}
