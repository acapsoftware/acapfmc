/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.controller;

import static acap.world.utilities.CalculationUtils.latToXCoord;
import static acap.world.utilities.CalculationUtils.lonToYCoord;
import static acap.world.model.Camera.*;
import static acap.world.model.Camera.selectedAircraft;
import static acap.world.view.WorldView.drawer;
import static processing.core.PApplet.*;

/** Created by erikn on 2/12/2017. */
public class CameraController {

  public void updateCamera() {

    if (dragLEFT) {}

    if (dragRIGHT) {
      int zoomSpd = 65;
      x =
          x
              + (((zoom * 1000f) / zoomSpd)
                  * ((-0.1f * (drawer.pmouseX - drawer.mouseX)) * cos(radians(theta - 90f))
                      + (0.1f * (drawer.pmouseY - drawer.mouseY)) * sin(radians(theta - 90))));
      y =
          y
              + (((zoom * 1000f) / zoomSpd)
                  * ((-0.1f * (drawer.pmouseY - drawer.mouseY)) * cos(radians(theta - 90f))
                      - (0.1f * (drawer.pmouseX - drawer.mouseX)) * sin(radians(theta - 90))));
    }

    if (dragMIDDLE) {
      theta = theta - 0.3f * (drawer.pmouseX - drawer.mouseX);
      phi = phi - 0.3f * (drawer.pmouseY - drawer.mouseY);
      phi = constrain(phi, 0, 89);
    }

    switch (cameraMode) {
      case FREE:
        camX = x - zoom * 1000 * cos(radians(phi)) * cos(radians(theta));
        camY = y - zoom * 1000 * cos(radians(phi)) * sin(radians(theta));
        camZ = zoom * 1000 * sin(radians(phi));
        drawer.camera(camX, camY, camZ, x, y, z, 0, 0, -1);
        break;
      case AIRPORT:
        camX = x - zoom * 1000 * cos(radians(phi)) * cos(radians(theta));
        camY = y - zoom * 1000 * cos(radians(phi)) * sin(radians(theta));
        camZ = zoom * 1000 * sin(radians(phi)) + selectedAirport.alt;

        drawer.camera(camX, camY, camZ, x, y, selectedAirport.alt, 0, 0, -1);
        break;
      case AIRCRAFT:
        camX =
            latToXCoord(selectedAircraft.getLat())
                - zoom * 1000 * cos(radians(phi)) * cos(radians(theta));
        camY =
            lonToYCoord(selectedAircraft.getLon())
                - zoom * 1000 * cos(radians(phi)) * sin(radians(theta));
        camZ = zoom * 1000 * sin(radians(phi)) + (float) selectedAircraft.getAltitude();

        drawer.camera(
            camX,
            camY,
            camZ,
            latToXCoord(selectedAircraft.getLat()),
            lonToYCoord(selectedAircraft.getLon()),
            (float) selectedAircraft.getAltitude(),
            0,
            0,
            -1);
        break;
    }
  }
}
