/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.controller;

import static acap.world.model.Camera.*;
import static acap.world.model.Camera.selectedAirport;
import static acap.world.model.Camera.z;

import acap.world.utilities.CalculationUtils;
import acap.world.utilities.NavDB;
import acap.world.model.Camera;
import acap.world.model.Airport;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.*;

/** Created by enadel on 10/30/16. */
public class AirportListController implements IListController {

  NavDB navDB;

  public AirportListController() {
    this.navDB = NavDB.getInstance();
  }

  public void execute(JComboBox comboBox) {
    ArrayList<Airport> airports = this.navDB.getAirports();

    for (Airport airport : airports) {
      comboBox.addItem(airport.getName() + " (" + airport.getIdent() + ")");
    }

    comboBox.addItemListener(
        new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            selectedAirport = NavDB.getInstance().getAirport(comboBox.getSelectedIndex());
            cameraMode = Camera.CamMode.AIRPORT;
            x = CalculationUtils.latToXCoord((float) selectedAirport.lat);
            y = CalculationUtils.lonToYCoord((float) selectedAirport.lon);
            z = selectedAirport.alt;
          }
        });
  }
}
