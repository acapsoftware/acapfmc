/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.utilities;

import static processing.core.PApplet.*;
import static processing.core.PConstants.PI;

import acap.fmc.UserAircraft;
import processing.core.PVector;

/** Created by enadel on 10/7/16. */
public class CalculationUtils {

  private static final float CENTRAL_LAT = 42.293387532f;
  private static final double CENTRAL_LAT_RADIANS = Math.toRadians(CENTRAL_LAT);
  private static final float FEET_PER_DEGREE = 365228.16f;
  private static final double RADIANS_PER_FEET_DEGREE =
      (Math.cos(CENTRAL_LAT_RADIANS) * FEET_PER_DEGREE);

  public static float miles2lon(float miles, float longitude, float latitude) {

    float goodlat = radians(CENTRAL_LAT);

    return miles / (cos(goodlat)) / 69.172f;
  }

  public static float miles2lat(float miles, float lat, float lon) {

    return miles / 69.172f;
  }

  public static float lonToYCoord(float longitude) {
    return (float) ((longitude - UserAircraft.getInstance().lon()) * RADIANS_PER_FEET_DEGREE);
  }

  public static float lonToYCoord(double longitude) {
    return (float) ((longitude - UserAircraft.getInstance().lon()) * RADIANS_PER_FEET_DEGREE);
  }

  public static PVector getRadialIntersection(
      PVector firstLocation, float firstRadial, PVector secondLocation, float secondRadial) {

    float lon1 = firstLocation.y;
    float lat1 = firstLocation.x;
    float lon2 = secondLocation.y;
    float lat2 = secondLocation.x;

    float d2theta = PI / 180;

    //distort the geometry so that angles are true
    float lon1a = lon1 * cos(lat1 * d2theta);
    float lon2a = lon2 * cos(lat2 * d2theta);

    float t1 = firstRadial * d2theta;
    float t2 = secondRadial * d2theta;

    //lets shorten the names of the latToXCoord and lonToYCoord variables for the trig computations

    //Solve for the lengths of the radials at the intersection
    float r1 =
        -(sin(t2) * (lon1a - lon2a) + cos(t2) * lat2 - cos(t2) * lat1)
            / (cos(t1) * sin(t2) - sin(t1) * cos(t2));
    float r2 =
        -(sin(t1) * (lon1a - lon2a) + cos(t1) * lat2 - cos(t1) * lat1)
            / (cos(t1) * sin(t2) - sin(t1) * cos(t2));

    //Find the point of intersection using either of the above solutions and its
    //base point

    float lon3a = lon1a + r1;
    //Following line will fail at north and south pole and be very off if pole inside solution triangle
    lon3a = lon1a + r1 * sin(t1);
    float lon3 = lon3a / cos(lat1 * d2theta);
    float lat3 = lat1 + r1 * cos(t1);

    println("Intersection at " + lat3 + ", " + lon3);

    return new PVector(lat3, lon3);
  }

  public static float latToXCoord(float lat) {
    return (float) (((double) lat - UserAircraft.getInstance().lat()) * FEET_PER_DEGREE);
  }

  public static float latToXCoord(double lat) {
    return (float) ((lat - UserAircraft.getInstance().lat()) * FEET_PER_DEGREE);
  }
}
