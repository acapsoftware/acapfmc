/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.utilities;

import static processing.core.PApplet.*;

import acap.fmc.Constants;
import acap.world.model.Airport;
import acap.world.model.Navaid;
import acap.world.model.World;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import processing.core.PVector;

public class NavDB {

  private static NavDB instance = null;
  ArrayList<Airport> airports = new ArrayList<>();
  ArrayList<Navaid> navaids = new ArrayList<>();
  HashMap<String, PVector> intersections = new HashMap<>();
  HashMap<String, ArrayList<String>> airways = new HashMap<>();

  protected NavDB() {
    loadNavaidsInBulk();
    loadIntersections();
    loadAirways();
    loadPreferredRoutes();
    loadAirports();
    World.getInstance().setAirports(getAirports());
  }

  public static NavDB getInstance() {
    if (instance == null) {
      instance = new NavDB();
    }
    return instance;
  }

  private void loadNavaidsInBulk() {
    String[] navaidScope = loadStrings(new File("assets/DatabaseFiles/NAVDATA/wpNavAID.txt"));
    int navaidScopeSize = navaidScope.length;
    println(
        "Loading "
            + navaidScopeSize
            + " navigational aids...  (estimated time: "
            + (int) (navaidScopeSize * 0.0002)
            + " seconds)");

    for (String line : navaidScope) {
      if (line.length() >= 61)
        this.navaids.add(
            new Navaid(
                line.substring(0, 24).trim(),
                line.substring(24, 29).trim(),
                line.substring(29, 33).trim(),
                line.substring(33, 43).trim(),
                line.substring(43, 54).trim(),
                line.substring(54, line.length()).trim()));
    }
  }

  private void loadIntersections() {
    String[] intersectionScope = loadStrings(new File("assets/DatabaseFiles/NAVDATA/wpNavFIX.txt"));
    int intersectionScopeSize = intersectionScope.length;
    println("Loading " + intersectionScopeSize + " intersections/GPS waypoints...");

    for (String line : intersectionScope) {
      if (line.length() >= 50)
        intersections.put(
            line.substring(0, 5).trim(),
            new PVector(
                Float.parseFloat(line.substring(29, 39).trim()),
                Float.parseFloat(line.substring(39, 49).trim())));
    }
  }

  private void loadAirways() {
    String[] airwayScope = loadStrings(new File("assets/DatabaseFiles/NAVDATA/wpNavRTE.txt"));
    int airwayScopeSize = airwayScope.length;
    println("Loading " + airwayScopeSize + " airways/flight corridors...");

    String tempName = split(airwayScope[0], " ")[0];
    ArrayList<String> tempAirway = new ArrayList<String>();
    tempAirway.add(split(airwayScope[0], " ")[2]);

    for (int i = 1; i < airwayScopeSize; i++) {

      if (split(airwayScope[i], " ")[0].equalsIgnoreCase(tempName))
        tempAirway.add(split(airwayScope[i], " ")[2]);
      else {
        if (Constants.DEBUG) {
          print("\nCompleting airway " + tempName + ", including: ");
          for (String string : tempAirway) print(string + " ");
        }
        airways.put(tempName, tempAirway);
        tempName = split(airwayScope[i], " ")[0];
        tempAirway.clear();
        tempAirway.add(split(airwayScope[i], " ")[2]);
      }
    }
    if (Constants.DEBUG) {
      print("\nCompleting airway " + tempName + ", including: ");
      for (String string : tempAirway) print(string + " ");
    }
    airways.put(tempName, tempAirway);
    tempAirway.clear();
  }

  private void loadAirports() {
    String[] airportScope = loadStrings(new File("assets/DatabaseFiles/unitedStatesAirports.txt"));
    int airportScopeSize = airportScope.length;
    println(
        "Loading "
            + airportScopeSize
            + " airports...  (estimated time: "
            + (int) (airportScopeSize * 0.25)
            + " seconds)");

    for (String line : airportScope) {
      String[] elements = split(line, ", ");
      // Temp fix until correct default is loaded
      if(!elements[0].equals("TEST_LOL")){
        airports.add(new Airport(elements[0], Integer.parseInt(elements[1])));

        if (airports.get(airports.size() - 1).removeMe) {
          println("WARNING: Removing " + airports.get(airports.size() - 1).ident);
          airports.remove(airports.size() - 1);
        }
      }
    }
  }

  private void loadPreferredRoutes() {
    //Orig,Route String,Dest,Hours1,Hours2,Hours3,Type,Area,Altitude,Aircraft,Direction,Seq,DCNTR,ACNTR
    String[] prefRoutes = loadStrings(new File("assets/DatabaseFiles/NAVDATA/prefroutes_db.txt"));
    println("Loading " + prefRoutes.length + " FAA IFR preferred routes...");

    for (String line : prefRoutes) {
      String[] elements = split(line, ",");
      String origin = elements[0];
      String destination = elements[2];
      ArrayList<String> route = new ArrayList<String>();

      Airport originAirport = getAirport("K" + origin);

      if (originAirport != null) {
        originAirport.routes.put(destination + elements[11], route);
        if (Constants.DEBUG)
          println(
              "Added route from "
                  + origin
                  + " to "
                  + destination
                  + ", by name: "
                  + (destination + elements[11]));
      }
    }
  }

  public Airport getAirport(String ident) {

    Airport foundAirport = null;

    for (Airport airport : airports)
      if (airport.ident.equalsIgnoreCase(ident)) foundAirport = airport;

    return foundAirport;
  }

  public ArrayList<Airport> getAirports() {
    return airports;
  }

  public Airport getAirport(int airportIndex) {
    return this.airports.get(airportIndex);
  }

  public int getNumAirports() {
    return this.airports.size();
  }

  public ArrayList<Navaid> getNavaids() {
    return navaids;
  }

  public HashMap<String, PVector> getIntersections() {
    return intersections;
  }

  public PVector getIntersection(String intersection) {
    return intersections.get(intersection);
  }

  public HashMap<String, ArrayList<String>> getAirways() {
    return airways;
  }

  public Navaid getNavaid(String ident) {

    Navaid foundNavaid = null;

    for (Navaid navaid : navaids)
      if (navaid.getIdent().equalsIgnoreCase(ident)) foundNavaid = navaid;

    return foundNavaid;
  }

  public Airport getNearestAirport(float x, float y) {

    if (this.getNumAirports() != 0) {
      Airport nearest = this.getAirport(0);

      for (Airport airport : airports)
        if (sqrt(
                pow(CalculationUtils.latToXCoord(airport.getLat()) - x, 2)
                    + pow(CalculationUtils.lonToYCoord(airport.getLon()) - y, 2))
            < sqrt(
                pow(CalculationUtils.latToXCoord(nearest.getLat()) - x, 2)
                    + pow(CalculationUtils.lonToYCoord(nearest.getLon()) - y, 2)))
          nearest = airport;
      return nearest;
    } else return null;
  }
}
