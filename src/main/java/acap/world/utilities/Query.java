/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.utilities;

import acap.world.model.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmldb.api.base.*;
import processing.core.PVector;

public class Query {

  private static final String collectionPath = "assets/DatabaseFiles/AirportDataXML/";
  //final static String resourceName = "AirportData.xml";

  private boolean debug = false;
  DocumentBuilderFactory dbf;
  DocumentBuilder db;
  Document dom;
  Airport airport;

 public Query(Airport airport) {

    File dir = new File(collectionPath + "Airports/");
    File[] xmlFiles = new File[0];
    xmlFiles = dir.listFiles();

    File found = null;
    for (File f : xmlFiles) {
      if (f.getName().equals(airport.getIdent() + ".xml")) {
        found = f;
        break;
      }
    }
    if ( found != null ){
      try {
        dbf = DocumentBuilderFactory.newInstance();
        db = dbf.newDocumentBuilder();
        dom = db.parse(found);
        this.airport = airport;
        this.airport.makeAirport(
            this); //There has to be an airport here, but the parseinitaldata functions acts like there is not.
      } catch (IOException | ParserConfigurationException | SAXException e) {
        if (found == null) {
          System.err.println("Airport: " + airport.getIdent() + " not found in database");
        } else {
          e.printStackTrace();
        }
      }
    }
  }

  public void parseInitialData() {

    try {

      Element line = (Element) dom.getElementsByTagName("Airport").item(0);
      Element tower = (Element) line.getElementsByTagName("Tower").item(0);

      airport.country = line.getAttribute("country");
      airport.state = line.getAttribute("state");
      airport.city = line.getAttribute("city");
      airport.name = line.getAttribute("name");
      airport.lat = Float.parseFloat(line.getAttribute("lat"));
      airport.lon = Float.parseFloat(line.getAttribute("lon"));
      airport.alt = meters2feet(line.getAttribute("alt"));
      try {
        airport.tower =
            new PVector(
                Float.parseFloat(tower.getAttribute("lat")),
                Float.parseFloat(tower.getAttribute("lon")));
      } catch (NumberFormatException e) {
        System.out.println("No tower found");
      }

      if (line.hasAttribute(
          "magvar")) {; //magnetic variations are not reported for some airports in the US.
      }
      try {
        airport.magvar = -Float.parseFloat(line.getAttribute("magvar"));
      } catch (Exception NumberFormatException) {
        if (debug) {
          System.out.println("WARNING: Empty String in Magnetic Variation.  Substituting 0.");
        }
        airport.magvar = 0;
      }

    } catch (Exception e) {
      System.out.println("ERROR PARSING INITIAL DATA");
      e.printStackTrace();
    }
  }

  public void parseRunways() {

    try {

      NodeList nodes = dom.getElementsByTagName("Runway");

      for (int j = 0; j < nodes.getLength(); j++) {
        Element runway = (Element) nodes.item(j);

        Element markings = (Element) runway.getElementsByTagName("Markings").item(0);

        Element lights = (Element) runway.getElementsByTagName("Lights").item(0);

        NodeList blastPadList = runway.getElementsByTagName("BlastPad");
        boolean blastPad1end = true;
        boolean blastPad2end = false;
        float blastPad1length = 0;
        float blastPad2length = 0;
        boolean offset1end = true;
        boolean offset2end = false;
        float offset1length = 0;
        float offset2length = 0;

        if (blastPadList.getLength() > 0) {
          blastPad1end =
              ((Element) blastPadList.item(0))
                  .getAttribute("end")
                  .equalsIgnoreCase("PRIMARY"); //true = primary
          blastPad1length = meters2feet(((Element) blastPadList.item(0)).getAttribute("length"));
        }
        if (blastPadList.getLength() > 1) {
          blastPad2end =
              ((Element) blastPadList.item(1))
                  .getAttribute("end")
                  .equalsIgnoreCase("PRIMARY"); //true = primary
          blastPad2length = meters2feet(((Element) blastPadList.item(1)).getAttribute("length"));
        }

        NodeList offsetList = runway.getElementsByTagName("OffsetThreshold");
        if (offsetList.getLength() > 0) {
          offset1end =
              ((Element) offsetList.item(0))
                  .getAttribute("end")
                  .equalsIgnoreCase("PRIMARY"); //true = primary
          offset1length = meters2feet(((Element) offsetList.item(0)).getAttribute("length"));
        }
        if (offsetList.getLength() > 1) {
          offset2end =
              ((Element) offsetList.item(1))
                  .getAttribute("end")
                  .equalsIgnoreCase("PRIMARY"); //true = primary
          offset2length = meters2feet(((Element) offsetList.item(1)).getAttribute("length"));
        }

        airport.runways.add(
            new RunwayBuilder()
                .setPrimaryNumber(runway.getAttribute("number"))
                .setPrimaryDesignator(runway.getAttribute("designator"))
                .setLat(Float.parseFloat(runway.getAttribute("lat")))
                .setLon(Float.parseFloat(runway.getAttribute("lon")))
                .setAlt(meters2feet(runway.getAttribute("alt")))
                .setSurface(runway.getAttribute("surface"))
                .setHeading(Float.parseFloat(runway.getAttribute("heading")))
                .setLength(meters2feet(runway.getAttribute("length")))
                .setWidth(meters2feet(runway.getAttribute("width")))
                .setPatternAltitude(meters2feet(runway.getAttribute("patternAltitude")))
                .setPrimaryTakeoff(runway.getAttribute("primaryTakeoff").equalsIgnoreCase("Yes"))
                .setPrimaryLanding(runway.getAttribute("primaryLanding").equalsIgnoreCase("Yes"))
                .setPrimaryPattern(runway.getAttribute("primaryPattern").equalsIgnoreCase("Left"))
                .setSecondaryTakeoff(
                    runway.getAttribute("secondaryTakeoff").equalsIgnoreCase("Yes"))
                .setSecondaryLanding(
                    runway.getAttribute("secondaryLanding").equalsIgnoreCase("Yes"))
                .setSecondaryPattern(
                    runway.getAttribute("secondaryPattern").equalsIgnoreCase("Left"))
                .setThreshold(markings.getAttribute("threshold").equalsIgnoreCase("TRUE"))
                .setEdges(markings.getAttribute("edges").equalsIgnoreCase("TRUE"))
                .setDashes(markings.getAttribute("dashes").equalsIgnoreCase("TRUE"))
                .setLeadingZeroIdent(
                    markings.getAttribute("leadingZeroIdent").equalsIgnoreCase("TRUE"))
                .setNoThresholdEndArrows(
                    markings.getAttribute("noThresholdEndArrows").equalsIgnoreCase("TRUE"))
                .setFixedDistance(markings.getAttribute("fixedDistance").equalsIgnoreCase("TRUE"))
                .setTouchdown(markings.getAttribute("touchdown").equalsIgnoreCase("TRUE"))
                .setIdent(markings.getAttribute("ident").equalsIgnoreCase("TRUE"))
                .setPrecision(markings.getAttribute("precision").equalsIgnoreCase("TRUE"))
                .setPrimaryClosed(markings.getAttribute("primaryClosed").equalsIgnoreCase("TRUE"))
                .setSecondaryClosed(
                    markings.getAttribute("secondaryClosed").equalsIgnoreCase("TRUE"))
                .setBlastPad1end(blastPad1end)
                .setBlastPad1length(blastPad1length)
                .setBlastPad2end(blastPad2end)
                .setBlastPad2length(blastPad2length)
                .setOffset1end(offset1end)
                .setOffset1length(offset1length)
                .setOffset2end(offset2end)
                .setOffset2length(offset2length)
                .createRunway());
      }

      if (debug) {
        System.out.println("Found " + nodes.getLength() + " runways.");
      }

    } catch (Exception e) {
      System.out.println("ERROR PARSING RUNWAYS");
      e.printStackTrace();
    }
  }

  public void parseTaxiNodes() {

    NodeList nodes = dom.getElementsByTagName("TaxiwayPoint");

    for (int j = 0; j < nodes.getLength(); j++) {
      Element line = (Element) nodes.item(j);

      try {

        airport.taxiNodes.add(
            new TaxiNode(
                Integer.parseInt(line.getAttribute("index")),
                line.getAttribute("type"),
                line.getAttribute("orientation").equalsIgnoreCase("FORWARD"),
                Float.parseFloat(line.getAttribute("lat")),
                Float.parseFloat(line.getAttribute("lon"))));

        airport.taxiwayIndex.put(
            Integer.parseInt(line.getAttribute("index")),
            airport.taxiNodes.get(airport.taxiNodes.size() - 1));

      } catch (Exception e) {
        e.printStackTrace();
        System.out.println("ERROR PARSING TAXI NODES");
      }
    }

    if (debug) {
      System.out.println("Found " + nodes.getLength() + " taxiway nodes.");
    }
  }

  public void parseTaxiPaths() {

    NodeList nodes = dom.getElementsByTagName("TaxiName");

    HashMap<Integer, String> taxiNames = new HashMap<Integer, String>();

    for (int j = 0; j < nodes.getLength(); j++) {
      Element line = (Element) nodes.item(j);

      try {

        taxiNames.put(Integer.parseInt(line.getAttribute("index")), line.getAttribute("name"));

      } catch (Exception e) {
        System.out.println("ERROR PARSING TAXI PATHS");
        e.printStackTrace();
      }
    }

    nodes = dom.getElementsByTagName("TaxiwayPath");

    for (int j = 0; j < nodes.getLength(); j++) {
      Element line = (Element) nodes.item(j);

      try {

        airport.taxiPaths.add(
            new TaxiPath(
                line.getAttribute("type"),
                Integer.parseInt(line.getAttribute("start")),
                Integer.parseInt(line.getAttribute("end")),
                meters2feet(line.getAttribute("width")),
                Float.parseFloat(line.getAttribute("weightLimit")),
                line.getAttribute("drawSurface").equalsIgnoreCase("TRUE"),
                line.getAttribute("drawDetail").equalsIgnoreCase("TRUE"),
                line.getAttribute("surface"),
                getTaxiwayName(line.getAttribute("name"), taxiNames),
                line.getAttribute("centerLine").equalsIgnoreCase("TRUE"),
                line.getAttribute("centerLineLighted").equalsIgnoreCase("TRUE"),
                line.getAttribute("leftEdge"),
                line.getAttribute("leftEdgeLighted").equalsIgnoreCase("TRUE"),
                line.getAttribute("rightEdge"),
                line.getAttribute("rightEdgeLighted").equalsIgnoreCase("TRUE"),
                line.getAttribute("number"),
                line.getAttribute("designator")));

      } catch (Exception e) {
        e.printStackTrace();
        System.out.println("ERROR PARSING TAXI PATHS");
      }
    }

    if (debug) {
      System.out.println("Found " + nodes.getLength() + " taxiway paths.");
    }
  }

  private String getTaxiwayName(String index, HashMap<Integer, String> taxiwayNames) {

    if (index.equalsIgnoreCase("") || index == null) {
      return "";
    } else {
      return taxiwayNames.get(Integer.parseInt(index));
    }
  }

  public void parseParking() {

    NodeList nodes = dom.getElementsByTagName("TaxiwayParking");

    for (int j = 0; j < nodes.getLength(); j++) {
      Element line = (Element) nodes.item(j);

      try {

        airport.parking.add(
            new Parking(
                Integer.parseInt(line.getAttribute("index")),
                line.getAttribute("type"),
                line.getAttribute("name"),
                line.getAttribute("number"),
                Float.parseFloat(line.getAttribute("heading")),
                meters2feet(line.getAttribute("radius")),
                Float.parseFloat(line.getAttribute("lat")),
                Float.parseFloat(line.getAttribute("lon")),
                line.getAttribute("pushBack")));

        airport.parkingIndex.put(
            Integer.parseInt(line.getAttribute("index")),
            airport.parking.get(airport.parking.size() - 1));

      } catch (Exception e) {
        System.out.println("ERROR PARSING PARKING");
        e.printStackTrace();
      }
    }

    if (debug) {
      System.out.println("Found " + nodes.getLength() + " parking stands.");
    }
  }

  public void parseBoundaryFences() {

    ArrayList<BoundaryFence> boundaryFences = new ArrayList<BoundaryFence>();

    try {
      NodeList nodes = dom.getElementsByTagName("BoundaryFence");

      for (int i = 0; i < nodes.getLength(); i++) {

        ArrayList<PVector> verticies = new ArrayList<PVector>();

        Element element = (Element) nodes.item(i);

        NodeList name = element.getElementsByTagName("Vertex");
        Element line = (Element) name.item(0);

        NodeList names = element.getElementsByTagName("Vertex");

        for (int j = 0; j < names.getLength(); j++) {
          line = (Element) names.item(j);

          verticies.add(
              new PVector(
                  Float.parseFloat(line.getAttribute("lat")),
                  Float.parseFloat(line.getAttribute("lon"))));
        }
        boundaryFences.add(new BoundaryFence(verticies));
      }

    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("ERROR PARSING BOUNDARY FENCES");
    }

    if (debug) {
      System.out.println("Found " + boundaryFences.size() + " boundary fences.");
    }
    airport.boundaryFences = boundaryFences;
  }

  public void parseAprons() {

    ArrayList<Apron> aprons = new ArrayList<Apron>();

    try {
      NodeList nodes = dom.getElementsByTagName("Apron");

      for (int i = 0; i < nodes.getLength(); i++) {

        ArrayList<PVector> verticies = new ArrayList<PVector>();

        Element element = (Element) nodes.item(i);

        NodeList name = element.getElementsByTagName("Vertex");
        Element line = (Element) name.item(0);

        NodeList names = element.getElementsByTagName("Vertex");

        for (int j = 0; j < names.getLength(); j++) {
          line = (Element) names.item(j);

          verticies.add(
              new PVector(
                  Float.parseFloat(line.getAttribute("lat")),
                  Float.parseFloat(line.getAttribute("lon"))));
        }
        aprons.add(new Apron(verticies, line.getAttribute("surface")));
      }

    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("ERROR PARSING APRONS");
    }

    if (debug) {
      System.out.println("Found " + aprons.size() + " apron/ramp areas.");
    }
    airport.aprons = aprons;
  }

  private float meters2feet(String alt) {

    return Float.parseFloat(alt.substring(0, alt.length() - 1)) * 3.28084f;
  }

  // private ArrayList<Object> getResult(){

  //     ArrayList<Object> results = new ArrayList<Object>();

  //     try{
  //         ResourceSet result = service.query(xQuery);
  //         ResourceIterator i = result.getIterator();
  //             while (i.hasMoreResources()) {
  //                 Resource r = i.nextResource();
  //                 if(debug) System.out.println((String) r.getContent());
  //                 results.add(r.getContent());
  //             }
  //         return results;
  //     }
  //     catch(Exception e){
  //         System.out.println("Failed to serialize the query.  Error:");
  //         System.out.println(e);
  //         return results;
  //     }
  // }

  // public float parseMeter(String inMeters){

  //     return Float.parseFloat((String)inMeters.substring(0, inMeters.length()-1))*3.28084;
  // }

  //--------------------------------------------------ACCESSORS TO THE DATABASE--------------------------------------------------//

  // public ArrayList<Float> getAllRunwayHeadingsMagnetic(String ident){

  //                 xQuery = "for $x in collection('" + collectionPath + "')" +
  //                     "//Airport[contains(@ident, \"" + ident + "\")]/Runway/@heading"
  //                 + " return data($x)";

  //                 ArrayList<Object> data = getResult();
  //                 ArrayList<Float> runwayHeadings = new ArrayList<Float>();

  //                  for(Object heading : data){
  //                      runwayHeadings.add(Float.parseFloat((String)heading));
  //                      runwayHeadings.add(Float.parseFloat((String)heading) + 180);
  //                  }

  //                 return runwayHeadings;
  // }

}
