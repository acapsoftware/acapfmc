/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.utilities;

import static acap.world.view.WorldView.drawer;
import static processing.core.PApplet.cos;
import static processing.core.PApplet.sin;
import static processing.core.PConstants.CLOSE;
import static processing.core.PConstants.TRIANGLES;
import static processing.core.PConstants.TWO_PI;
import static processing.opengl.PShapeOpenGL.NORMAL;

import org.graphstream.graph.Edge;
import processing.core.PImage;
import processing.core.PShape;
import processing.core.PVector;

public class DrawingUtils {

  public static void polygon(float x, float y, float z, float radius, int npoints) {
    float angle = TWO_PI / npoints;
    drawer.pushMatrix();
    drawer.translate(x, y, z);
    drawer.beginShape();
    for (float a = 0; a < TWO_PI; a += angle) {
      float sx = x + cos(a) * radius;
      float sy = y + sin(a) * radius;
      drawer.vertex(sx, sy);
    }
    drawer.endShape(CLOSE);
    drawer.popMatrix();
  }

  public static void drawLatLonLine(
      float lat1,
      float lon1,
      float altitude1,
      float lat2,
      float lon2,
      float altitude2,
      int r,
      int g,
      int b) {
    drawer.stroke(r, g, b);
    drawer.line(
        CalculationUtils.latToXCoord(lat1),
        CalculationUtils.lonToYCoord(lon1),
        altitude1,
        CalculationUtils.latToXCoord(lat2),
        CalculationUtils.lonToYCoord(lon2),
        altitude2);
  }

  public static void drawLatLonLine(
      float lat1, float lon1, float altitude1, float lat2, float lon2, float altitude2) {
    drawLatLonLine(lat1, lon1, altitude1, lat2, lon2, altitude2, 255, 0, 255);
  }

  public static PShape createShapeTri(PShape r) {
    PImage tex = drawer.loadImage("assets/Aircraft/pa28_180_t.jpg");
    PShape s = drawer.createShape();
    s.beginShape(TRIANGLES);
    s.noStroke();
    s.texture(tex);
    s.textureMode(NORMAL);
    for (int i = 100; i < r.getChildCount(); i++) {
      if (r.getChild(i).getVertexCount() == 3) {
        for (int j = 0; j < r.getChild(i).getVertexCount(); j++) {
          PVector p = r.getChild(i).getVertex(j);
          PVector n = r.getChild(i).getNormal(j);
          float u = r.getChild(i).getTextureU(j);
          float v = r.getChild(i).getTextureV(j);
          s.normal(-n.x * 3, n.y * 3, n.z * 3);
          s.vertex(-p.x * 3, p.y * 3, p.z * 3, u, v);
        }
      }
    }
    s.endShape();
    return s;
  }

  public static void drawEdge(Edge e, float altitude) {
    float lat1 = ((Double) e.getNode0().getAttribute("lat")).floatValue();
    float lon1 = ((Double) e.getNode0().getAttribute("lon")).floatValue();
    float lat2 = ((Double) e.getNode1().getAttribute("lat")).floatValue();
    float lon2 = ((Double) e.getNode1().getAttribute("lon")).floatValue();
    drawLatLonLine(lat1, lon1, altitude, lat2, lon2, altitude, 0, 255, 43);
  }
}
