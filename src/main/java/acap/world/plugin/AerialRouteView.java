/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.plugin;

import acap.fmc.routing.Waypoint;
import java.util.ArrayList;

/** Created by erikn on 2/14/2017. */
public class AerialRouteView implements Runnable {

  ArrayList<Waypoint> route;

  public AerialRouteView() {
    int CLIMBPERF = 700;
    int IAS = 70;

    //ArrayList<Waypoint> route = RoutingHelper.generateRoute("KORH GDM KORE", 4000f,700f,70f);
  }

  @Override
  public void run() {}
}
