/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.plugin;

import acap.fmc.UserAircraft;
import acap.fmc.routing.AirportGraph;
import acap.fmc.routing.Waypoint;
import acap.fmc.routing.air.AirRoute;
import acap.fmc.state.ModeControls;
import acap.utilities.Heading;
import acap.world.utilities.NavDB;
import acap.world.model.Parking;
import acap.world.model.Runway;
import java.util.ArrayList;
import java.util.List;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Created by erikn on 2/14/2017. */
public class RouteView implements Runnable {

  static List<Edge> pathEdges;
  Path SBOEdge;
  private static UserAircraft uCraft = UserAircraft.getInstance();
  protected final Logger log = LoggerFactory.getLogger(getClass());

  public RouteView() {}

  @Override
  public void run() {
    Waypoint.drawWaypointList(uCraft.getRoute(), 255, 255, 0);
  }

  public static void refreshRoute() {
    uCraft.clearRoute();

    Path p = new Path();
    AirportGraph dest;

    String originid = ModeControls.getInstance().getControl(ModeControls.Origin);
    String destid = ModeControls.getInstance().getControl(ModeControls.Destination);
    String taxiin = ModeControls.getInstance().getControl(ModeControls.TaxiIn);
    String taxiout = ModeControls.getInstance().getControl(ModeControls.TaxiOut);
    String route = ModeControls.getInstance().getControl(ModeControls.Route);
    String course = ModeControls.getInstance().getControl(ModeControls.CRS);
    String heading = ModeControls.getInstance().getControl(ModeControls.HDG);


    uCraft.setOrigin(NavDB.getInstance().getAirport(originid));
    uCraft.addToRoute(calculateTaxiRoute(originid, taxiout, heading));

    // optional //
    int cruisingAlt = Integer.valueOf(ModeControls.getInstance().getControl(ModeControls.ALT));
    int cruisingSpeed = Integer.valueOf(ModeControls.getInstance().getControl(ModeControls.SPD));
    uCraft.setCruiseSpeed(cruisingSpeed);
    uCraft.setCruiseAlt(cruisingAlt);

    AirRoute air = new AirRoute(route);
    air.setStartingWaypoint(uCraft.getLastWaypointofRoute());
    air.setLandingRunway(course);
    air.setTakeOffRunway(heading);
    air.setCruiseSpeed(cruisingSpeed);
    air.setCruisingAltitude(cruisingAlt);

    uCraft.addToRoute(air.calculateRoute());

    Runway landing = NavDB.getInstance().getAirport(destid).getRunwayByNumber(course);

    Waypoint endofCurrent = uCraft.getLastWaypointofRoute();

    uCraft.setDestination(NavDB.getInstance().getAirport(destid));
    dest = new AirportGraph(destid);

    Node[] runwayr =
        dest.getRunwayByHeading(
            landing.getPrimaryNumber() + "/" + landing.getSecondaryNumber(), course);
    Node closestNodeInHeading = null;
    for (int i = 0; i < runwayr.length; i++) {
      Node n = runwayr[i];
      if (Math.abs(
          Heading.headingDiff(
              NavDB.getInstance().getAirport(destid).getRunwayByNumber(course).getHeading(),
              AirportGraph.headingToNode(endofCurrent.lat(), endofCurrent.lon(), n)))
          < 5) {
        closestNodeInHeading = n;
        break;
      }
    }

    Waypoint taxiExit = new Waypoint(closestNodeInHeading, NavDB.getInstance().getAirport(destid).getAlt() + 30);
    UserAircraft.getInstance().setTaxiExitNode(taxiExit);

    uCraft.addToRoute(
        new Waypoint(closestNodeInHeading, NavDB.getInstance().getAirport(destid).getAlt() + 30));

    if (taxiin.contains("!")) {
      String instruction = taxiin.substring(0, taxiin.indexOf('!'));
      Parking parkingSpot =
          NavDB.getInstance().getAirport(destid).getParking().get(Integer.valueOf(instruction) - 1);
      Node park = dest.getNodeNearLonLat(parkingSpot.getLon(), parkingSpot.getLat());

      Waypoint parkspot = new Waypoint(park, NavDB.getInstance().getAirport(destid).getAlt() + 30);
      UserAircraft.getInstance().setDestionationParkingSpot(parkspot);

      dest.getShortestPathToNode(closestNodeInHeading, park)
          .getNodeSet()
          .forEach(
              e ->
                  uCraft.addToRoute(
                      new Waypoint(e, NavDB.getInstance().getAirport(destid).getAlt() + 30)));

    }
  }

  /**
   * Return the taxi route based off of the input from the mode control panel. The variables used in this from the
   * mode control panel include : origin, taxiout instructions, and heading.
   * @return Waypoints that make up the taxi route
   * @param originid
   * @param taxiout
   * @param heading
   */
  private static ArrayList<Waypoint> calculateTaxiRoute(String originid, String taxiout,
      String heading) {
    ArrayList<Waypoint> taxiRoute = new ArrayList<>();
    Node target;
    String runway;
    Path p;
    AirportGraph origin = new AirportGraph(originid);

    Node starting = origin.getNodeNearLonLat(uCraft.getLon(), uCraft.getLat());

    if (taxiout.contains("!")) {
      runway = taxiout.substring(0, taxiout.indexOf('!'));
      target = origin.getRunwayByHeading(runway, heading)[0];
      p = origin.getShortestPathToNode(starting, target);
    } else {
      p = origin.parsePathFromString(taxiout, starting);
    }

    p.getNodeSet()
        .forEach(
            e -> {
              taxiRoute.add(new Waypoint(e, NavDB.getInstance().getAirport(originid).getAlt() + 5));
            });
    return taxiRoute;
  }

}
