package acap.world.plugin;

import acap.fmc.UserAircraft;
import acap.fmc.gui.utilities.PopupManager;
import acap.fmc.procedure.FakeTask;
import acap.fmc.procedure.TaskGroup;
import acap.fmc.procedure.TaskQueue;
import acap.fmc.routing.Waypoint;
import java.util.ArrayList;

public class FSMDemo implements Runnable{


  public FSMDemo() {


    TaskGroup tg = new TaskGroup("FSM DEMO",1);
    tg.addTask(new FakeTask("Taxi Clearance", () ->
    {
      PopupManager pm = new PopupManager();
      pm.showYesNoDialog("Clear to Taxi?");
      if(pm.getLastButton().equals("Yes"))
        UserAircraft.getInstance().setTaxiClearanceStatus(true);
    }
    ));

    TaskQueue.getInstance().addTaskGroup(tg);
  }

  private void traverseWaypoints(ArrayList<Waypoint> restofWay) {
    for(Waypoint w : restofWay) {
      UserAircraft.getInstance().setWaypoint(w);
      UserAircraft.getInstance().setHeading(w.bearingTo(restofWay.get(restofWay.indexOf(w)+1)));
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void run() {

  }
}
