/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.world.plugin;

import static acap.utilities.CSVReader.parseWaypoints;

import acap.fmc.routing.Waypoint;
import acap.utilities.KalmanFilter;
import java.util.ArrayList;

/** Created by erikn on 2/16/2017. */
public class CSVPoints implements Runnable {
  ArrayList<Waypoint> testPoints;
  ArrayList<Waypoint> filteredPoints;

  private static final double EARTH_RADIUS_IN_METERS = 6371009;
  KalmanFilter f;

  public CSVPoints() {
    testPoints = parseWaypoints("assets/gpsvalues.csv", 1, 0, 2);
    KalmanFilter f = new KalmanFilter(4, 2);
    KalmanFilter alt = new KalmanFilter(2, 1);

    f.getState_transition().set_identity_matrix();
    alt.getState_transition().set_matrix(1.0, 1.0, 0.0, 1.0);

    set_seconds_per_timestep(f, 1.0);

    f.getObservation_model().set_matrix(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0);
    alt.getObservation_model().set_matrix(1.0, 0.0);

    double pos = 0.000001;
    f.getProcess_noise_covariance()
        .set_matrix(pos, 0.0, 0.0, 0.0, 0.0, pos, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    //alt.getProcess_noise_covariance().set_identity_matrix();
    alt.getProcess_noise_covariance().set_matrix(2.0, 0.0, 2.0, 0.0);

    f.getObservation_noise_covariance().set_matrix(pos * 1.0, 0.0, 0.0, pos * 1.0);
    alt.getObservation_noise_covariance().set_matrix(180.0, 0.0, 180.0, 0.0);

    f.state_estimate.set_matrix(0.0, 0.0, 0.0, 0.0);
    f.estimate_covariance.set_identity_matrix();
    f.estimate_covariance.scale_matrix(1000.0 * 1000.0 * 1000.0 * 1000.0);

    alt.state_estimate.set_matrix(10000.0, 0, 0, 0);
    alt.estimate_covariance.set_identity_matrix();
    alt.estimate_covariance.scale_matrix(10000.0 * 10000.0);

    filteredPoints = new ArrayList<>();

    for (int i = 0; i < testPoints.size() - 1; i++) {
      update_velocity2d(testPoints.get(i).lat(), testPoints.get(i).lon(), 1.0, f);
      alt.observation.set_matrix(testPoints.get(i).getAltitude());
      alt.update();
      double lat = get_lat_long(f)[0];
      double lon = get_lat_long(f)[1];

      double altitude = alt.state_estimate.data[0][0];
      int velocity = (int) get_speed(f, altitude);
      Waypoint p = new Waypoint(lon, lat, altitude);
      p.setSpeed(velocity);
      filteredPoints.add(p);
    }
  }

  /* Set the seconds per timestep in the velocity2d model. */
  /*
   * The position units are in thousandths of latitude and longitude. The
   * velocity units are in thousandths of position units per second.
   *
   * So if there is one second per timestep, a velocity of 1 will change the
   * lat or long by 1 after a million timesteps.
   *
   * Thus a typical position is hundreds of thousands of units. A typical
   * velocity is maybe ten.
   */
  void set_seconds_per_timestep(KalmanFilter f, double seconds_per_timestep) {
    /*
     * unit_scaler accounts for the relation between position and velocity
     * units
     */
    double unit_scaler = 0.001;
    f.getState_transition().data[0][2] = unit_scaler * seconds_per_timestep;
    f.getState_transition().data[1][3] = unit_scaler * seconds_per_timestep;
  }

  /* Update the velocity2d model with new gps data. */
  void update_velocity2d(
      double lat, double lon, double seconds_since_last_timestep, KalmanFilter f) {
    set_seconds_per_timestep(f, seconds_since_last_timestep);
    f.observation.set_matrix(lat * 1000.0, lon * 1000.0);
    f.update();
  }

  /* Extract a lat long from a velocity2d Kalman filter. */
  double[] get_lat_long(KalmanFilter f) {
    double[] latlon = new double[2];
    latlon[0] = f.state_estimate.data[0][0] / 1000.0;
    latlon[1] = f.state_estimate.data[1][0] / 1000.0;
    return latlon;
  }

  double[] get_velocity(KalmanFilter f) {
    double[] delta_latlon = new double[2];
    delta_latlon[0] = f.state_estimate.data[2][0] / (1000.0 * 1000.0);
    delta_latlon[1] = f.state_estimate.data[3][0] / (1000.0 * 1000.0);
    return delta_latlon;
  }

  /* Extract speed in meters per second from a velocity2d Kalman filter. */
  double get_speed(KalmanFilter f, double altitude) {
    double[] latlon = get_lat_long(f);
    double[] delta_latlon = get_velocity(f);
    /*
     * First, let's calculate a unit-independent measurement - the radii of
     * the earth traveled in each second. (Presumably this will be a very
     * small number.)
     */

    /* Convert to radians */
    latlon[0] = Math.toRadians(latlon[0]);
    latlon[1] = Math.toRadians(latlon[1]);
    delta_latlon[0] = Math.toRadians(delta_latlon[0]);
    delta_latlon[1] = Math.toRadians(delta_latlon[1]);

    /* Haversine formula */
    double lat1 = latlon[0] - delta_latlon[0];
    double sin_half_dlat = Math.sin(delta_latlon[0] / 2.0);
    double sin_half_dlon = Math.sin(delta_latlon[1] / 2.0);
    double a =
        sin_half_dlat * sin_half_dlat
            + Math.cos(lat1) * Math.cos(latlon[0]) * sin_half_dlon * sin_half_dlon;
    double radians_per_second = 2 * Math.atan2(1000.0 * Math.sqrt(a), 1000.0 * Math.sqrt(1.0 - a));

    /* Convert units */
    double meters_per_second = radians_per_second * (EARTH_RADIUS_IN_METERS + altitude);
    return meters_per_second;
  }

  @Override
  public void run() {
//    for (int i = 0; i < testPoints.size() - 1; i++) {
//      testPoints.get(i).drawTo(testPoints.get(i + 1));
//    }
//
//    for (int i = 0; i < filteredPoints.size() - 1; i++) {
//      if (i % 30 == 0) filteredPoints.get(i).drawWaypointSpeed();
//      filteredPoints.get(i).drawTo(filteredPoints.get(i + 1), 255, 0, 0);
//    }
  }
}
