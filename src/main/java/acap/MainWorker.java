/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap;

import acap.fmc.gui.MainWindow;
import acap.fmc.gui.processing.apps.GLWorld;
import com.jogamp.newt.awt.NewtCanvasAWT;
import java.util.HashMap;
import javax.swing.*;

/** Created by tjnickerson on 11/15/16. */
public class MainWorker extends SwingWorker<Object, Object> {

  HashMap<Integer, String> stringMap;
  HashMap<Integer, String> localStringMap;

  public MainWorker(HashMap<Integer, String> stringMap) {
    this.stringMap = stringMap;
  }

  public HashMap<Integer, String> getStringMap() {
    return this.stringMap;
  }

  private Integer getLowestValue(HashMap<Integer, String> hashMap) {
    Integer lowest = null;
    for (Integer key : hashMap.keySet()) {
      if (lowest == null || key < lowest) {
        lowest = key;
      }
    }
    hashMap.remove(lowest);
    return lowest;
  }

  private void updateProgress() throws Exception {
    if (this.localStringMap == null) {
      this.localStringMap = new HashMap<Integer, String>(this.stringMap);
    }
    Integer lowest = this.getLowestValue(this.localStringMap);
    Thread.sleep(500);
    setProgress(lowest);
  }

  @Override
  protected Object doInBackground() throws Exception {

    updateProgress();
    try {
      UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
    } catch (ClassNotFoundException
        | InstantiationException
        | IllegalAccessException
        | UnsupportedLookAndFeelException ex) {
      ex.printStackTrace();
    }

    updateProgress();
    GLWorld glWorld = GLWorld.getInstance();
    NewtCanvasAWT canvas = glWorld.getCanvas();

    // Create the windows, start the canvas and show it
    updateProgress();
    new MainWindow("ACAP FMC v2.0", canvas).show();

    return null;
  }
}
