/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap;

import acap.fmc.gui.MainSplash;
import acap.world.utilities.NavDB;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Created by tjnickerson on 11/15/16. */
public class Main{

  public static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    new Thread(NavDB::getInstance).start();

    EventQueue.invokeLater(
        () -> {
          MainSplash splash = MainSplash.getInstance();
          splash.addComponentListener(new SplashListener());
          splash.show();
        });
  }
}

class SplashListener implements ComponentListener {

  @Override
  public void componentResized(ComponentEvent e) {}

  @Override
  public void componentMoved(ComponentEvent e) {}

  @Override
  public void componentShown(ComponentEvent e) {
    HashMap<Integer, String> stringMap = new HashMap();

    stringMap.put(25, "Setting look and feel...");
    stringMap.put(50, "Initializing OpenGL applet...");
    stringMap.put(100, "Displaying windows...");

    MainSplash.getInstance().doWork(new MainWorker(stringMap));
  }

  @Override
  public void componentHidden(ComponentEvent e) {}
}
