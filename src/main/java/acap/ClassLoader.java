/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap;

import java.util.*;
import java.util.stream.Collectors;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

/** Created by erikn on 2/13/2017. */
public class ClassLoader {

  public static String[] getClasses(String pluginpackage) {
    List<java.lang.ClassLoader> classLoadersList = new LinkedList<java.lang.ClassLoader>();
    classLoadersList.add(ClasspathHelper.contextClassLoader());
    classLoadersList.add(ClasspathHelper.staticClassLoader());

    Reflections reflections =
        new Reflections(
            new ConfigurationBuilder()
                .setScanners(
                    new SubTypesScanner(false /* don't exclude Object.class */),
                    new ResourcesScanner())
                .setUrls(
                    ClasspathHelper.forClassLoader(
                        classLoadersList.toArray(new java.lang.ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(pluginpackage))));

    Set<Class<? extends Object>> allClasses = reflections.getSubTypesOf(Object.class);

    ArrayList<String> packageClasses = new ArrayList<>();
    allClasses =
        allClasses.stream().filter(e -> !e.getName().contains("$")).collect(Collectors.toSet());
    allClasses.forEach(e -> packageClasses.add(e.getName()));
    return packageClasses.toArray(new String[packageClasses.size()]);
  }

  public static void main(String[] args) {
    try {

      Arrays.stream(getClasses("acap.drawer.plugin")).forEach(System.out::println);

      java.lang.ClassLoader myClassLoader = java.lang.ClassLoader.getSystemClassLoader();

      for (String cname : getClasses("acap.drawer.plugin")) {
        Class myClass = myClassLoader.loadClass(cname);
        Runnable whatInstance = (Runnable) myClass.newInstance();
        whatInstance.run();
      }

    } catch (SecurityException e) {
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }
}
