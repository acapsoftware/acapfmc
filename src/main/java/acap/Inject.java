/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap;

import java.util.ArrayList;

/** Created by erikn on 2/10/2017. */
public class Inject {
  private static Inject ourInstance = new Inject();
  private ArrayList<Runnable> startupTasks;
  private ArrayList<Runnable> taskThreads;
  private ArrayList<Runnable> drawLoop;

  public static Inject getInstance() {
    return ourInstance;
  }

  private Inject() {
    startupTasks = new ArrayList<>();
    taskThreads = new ArrayList<>();
    drawLoop = new ArrayList<>();
  }

  public void startThreads() {

    for (Runnable r : taskThreads) {
      new Thread(r).start();
    }
  }

  public void runStartupTasks() {
    for (int i = 0; i < startupTasks.size(); i++) {
      startupTasks.get(i).run();
    }
  }

  public void runDrawLoopTasks() {
    for (Runnable r : drawLoop) {
      r.run();
    }
  }

  public void addToStartup(Runnable r) {
    this.startupTasks.add(r);
  }

  public void addThread(Runnable r) {
    this.taskThreads.add(r);
  }

  public void addToDrawLoop(Runnable r) {
    drawLoop.add(r);
  }

  public ArrayList<Runnable> getStartupTasks() {
    return startupTasks;
  }

  public ArrayList<Runnable> getTaskThreads() {
    return taskThreads;
  }

  public ArrayList<Runnable> getDrawLoopTasks() {
    return drawLoop;
  }
}
