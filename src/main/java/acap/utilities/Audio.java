package acap.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineUnavailableException;

/**
 * Created by erikn on 3/29/2017.
 */
public class Audio implements Runnable {

  private static final long AUDIO_COOLDOWN = 1000;
  private static Audio instance;
  private boolean canPlay;
  private static AudioComparator comparator = new AudioComparator();
  private static PriorityQueue<AudioInputStream> filesToPlay = new PriorityQueue<>(5,comparator);
  private static HashMap<AudioInputStream,Integer> clipPriorityMap = new HashMap<>();
  private static int numPlayed = 0;

  private static final String AUDIO_LOC = "data/";
  private static final String AUDIO_EXT = ".wav";
  public static final String TEN = AUDIO_LOC + "10" + AUDIO_EXT;
  public static final String TWENTY = AUDIO_LOC + "20" + AUDIO_EXT;
  public static final String THIRTY = AUDIO_LOC + "30" + AUDIO_EXT;
  public static final String FOURTY = AUDIO_LOC + "40" + AUDIO_EXT;
  public static final String FIFTY = AUDIO_LOC + "50" + AUDIO_EXT;
  public static final String SIXTY = AUDIO_LOC + "60" + AUDIO_EXT;
  public static final String ONEHUNDRED = AUDIO_LOC + "100" + AUDIO_EXT;
  public static final String TWOHUNDRED = AUDIO_LOC + "200" + AUDIO_EXT;
  public static final String THREEHUNDRED = AUDIO_LOC + "300" + AUDIO_EXT;
  public static final String FOURHUNDRED = AUDIO_LOC + "400" + AUDIO_EXT;
  public static final String FIVEHUNDRED = AUDIO_LOC + "500" + AUDIO_EXT;
  public static final String ONETHOUSAND = AUDIO_LOC + "1000" + AUDIO_EXT;
  public static final String TWENTYFIVEHUNDRED = AUDIO_LOC + "2500" + AUDIO_EXT;
  public static final String ADJUST_V_SPEED = AUDIO_LOC + "AdjustVspeed" + AUDIO_EXT;
  public static final String AP_DISCONNECT = AUDIO_LOC + "APDisconnect" + AUDIO_EXT;
  public static final String AURAL_WARNING_TEST_A = AUDIO_LOC + "aural_warning_tests_A" + AUDIO_EXT;
  public static final String AURAL_WARNING_TEST_B = AUDIO_LOC + "aural_warning_tests_B" + AUDIO_EXT;
  public static final String AUTOPILOT = AUDIO_LOC + "autopilot" + AUDIO_EXT;
  public static final String AVIONICS = AUDIO_LOC + "avionics" + AUDIO_EXT;
  public static final String BANKANGLE = AUDIO_LOC + "BankAngle" + AUDIO_EXT;
  public static final String BELL = AUDIO_LOC + "Bell" + AUDIO_EXT;
  public static final String CAUTION = AUDIO_LOC + "caution" + AUDIO_EXT;
  public static final String CAUTIONTERRAIN = AUDIO_LOC + "CautionTerrain" + AUDIO_EXT;
  public static final String CHIME = AUDIO_LOC + "Chime" + AUDIO_EXT;
  public static final String CLEARCONFLICT = AUDIO_LOC + "ClearConflict" + AUDIO_EXT;
  public static final String CLIMB = AUDIO_LOC + "Climb" + AUDIO_EXT;
  public static final String CLIMBNOW = AUDIO_LOC + "ClimbNow" + AUDIO_EXT;
  public static final String CROSSINGCLIMB = AUDIO_LOC + "CrossingClimb" + AUDIO_EXT;
  public static final String CROSSINGDESCEND = AUDIO_LOC + "CrossingDescend" + AUDIO_EXT;
  public static final String DESCEND = AUDIO_LOC + "Descend" + AUDIO_EXT;
  public static final String DESCENDNOW = AUDIO_LOC + "DescendNow" + AUDIO_EXT;
  public static final String DONTSINK = AUDIO_LOC + "DontSink" + AUDIO_EXT;
  public static final String ELECTRICDISCONNECT = AUDIO_LOC + "electricDisconnect" + AUDIO_EXT;
  public static final String GLIDESLOPE = AUDIO_LOC + "GlideSlop" + AUDIO_EXT;
  public static final String INCREASECLIMB = AUDIO_LOC + "IncreaseClimb" + AUDIO_EXT;
  public static final String INCREASEDESCENT = AUDIO_LOC + "IncreaseDescent" + AUDIO_EXT;
  public static final String MAINTAIN = AUDIO_LOC + "Maintain" + AUDIO_EXT;
  public static final String MAINTAINCROSS = AUDIO_LOC + "MaintainCross" + AUDIO_EXT;
  public static final String MONITOR_V_SPEED = AUDIO_LOC + "MonitorVSpeed" + AUDIO_EXT;
  public static final String PAXSIGN = AUDIO_LOC + "paxsign" + AUDIO_EXT;
  public static final String SINKRATE = AUDIO_LOC + "SinkRate" + AUDIO_EXT;
  public static final String SINKRATEPULLUP = AUDIO_LOC + "SinkRatePullUP" + AUDIO_EXT;
  public static final String TAKEOFFB_RAKE = AUDIO_LOC + "takeoff brake" + AUDIO_EXT;
  public static final String TAKEOFF_FLAPS = AUDIO_LOC + "takeoff flaps" + AUDIO_EXT;
  public static final String TAKEOFFCONFIG = AUDIO_LOC + "TakeOffConfig" + AUDIO_EXT;
  public static final String TCAS_TEST_FAIL = AUDIO_LOC + "TCAStestFail" + AUDIO_EXT;
  public static final String TCAS_TEST_OK = AUDIO_LOC + "TCAStestOK" + AUDIO_EXT;
  public static final String TERRAIN = AUDIO_LOC + "Terrain" + AUDIO_EXT;
  public static final String TERRAIN_PULL_UP = AUDIO_LOC + "TerrainPullUP" + AUDIO_EXT;
  public static final String THROTTLE = AUDIO_LOC + "throttle" + AUDIO_EXT;
  public static final String TRAFFIC = AUDIO_LOC + "Traffic" + AUDIO_EXT;
  public static final String WARNING = AUDIO_LOC + "Warning" + AUDIO_EXT;
  public static final String WINDSHEAR = AUDIO_LOC + "Windshear" + AUDIO_EXT;

  private String[] testclips = {AURAL_WARNING_TEST_A, AURAL_WARNING_TEST_B, CHIME};

  @Override
  public void run() {
    while (true) {
      if (canPlay && !filesToPlay.isEmpty()) {
        AudioInputStream a = filesToPlay.poll();
        try {
          Clip clip = (Clip) AudioSystem.getLine(new Line.Info(Clip.class));
          addStopListener(clip);

          clip.open(a);
          clip.start();

          numPlayed++;
          canPlay = false;
        } catch (LineUnavailableException | IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void addStopListener(Clip clip) {
    clip.addLineListener(event -> {
      if (event.getType() == LineEvent.Type.STOP) {
        clip.close();
        try {
          Thread.sleep(AUDIO_COOLDOWN);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        Audio.getInstance().cooldownReset();
      }
    });
  }

  static class AudioComparator implements Comparator<AudioInputStream> {

    public int compare(AudioInputStream a, AudioInputStream b) {

      return clipPriorityMap.get(a).compareTo(clipPriorityMap.get(b));
    }
  }

  private Audio(){
    canPlay = true;
  }

  public static Audio getInstance() {
    if (instance == null) {
      instance = new Audio();
    }
    return instance;
  }

  /**
   * Select an audio file to play - along with the priority that the audio file is heard
   * @param audiofile - file to play
   * @param priority - priority of the audio clip (higher is higher priority)
   */
  public void playAudio(String audiofile, int priority){
    try
    {
      File f = new File(audiofile);
      AudioInputStream ais =AudioSystem.getAudioInputStream(f);
      clipPriorityMap.put(ais,priority);
      filesToPlay.add(ais);
    }
    catch (Exception exc)
    {
      exc.printStackTrace(System.out);
    }
  }

  /**
   * Add an audio stream to play with a set priority
   * @param ais - audio input stream
   * @param priority - priority of the audio stream
   */
  public void playAudio(AudioInputStream ais, int priority){
    try
    {
      clipPriorityMap.put(ais,priority);
      filesToPlay.add(ais);
    }
    catch (Exception exc)
    {
      exc.printStackTrace(System.out);
    }
  }

  public void runTestClips(){
    Arrays.stream(this.testclips).forEach(clip -> playAudio(clip,0));
  }

  private void cooldownReset(){
    canPlay = true;
  }

  public int getNumPlayed(){
    return numPlayed;
  }

}


