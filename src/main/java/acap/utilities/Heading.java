package acap.utilities;

public class Heading {

  /**
   * Rotate a heading a specific amount of degrees
   * @param degrees - degrees to rotate
   * @param currentHeading - starting heading
   * @return - the rotated heading
   */
  public static double Rotate(double degrees, double currentHeading) {
    double orientation = (currentHeading + degrees) % 360;
    if (orientation < 0) {
      return orientation += 360;
    } else return orientation;
  }

  /**
   * Calculate the difference in inital and final heading (in degrees)
   * @param initial - Initial heading
   * @param ending - Final heading
   * @return the difference between the initial and ending heading
   */
  public static double headingDiff(double initial, double ending) {
    double diff = ending - initial;
    double absDiff = Math.abs(diff);

    if (absDiff <= 180) {
      return absDiff == 180 ? absDiff : diff;
    } else if (ending > initial) {
      return absDiff - 360.0;
    } else {
      return 360.0 - absDiff;
    }
  }

  /**
   * Get the turn rate in degrees
   *
   * @param airspeed
   * @param bankAngle
   * @return turn rate in degrees
   */
  public static double calculateTurnRate(double airspeed, double bankAngle) {
    return (1096 * Math.tan(Math.toRadians(bankAngle))) / airspeed;
  }

}
