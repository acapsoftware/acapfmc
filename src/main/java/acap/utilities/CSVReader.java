/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.utilities;

import acap.fmc.routing.Waypoint;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/** Created by enadel on 12/10/16. */
public class CSVReader {

  public static void main(String[] args) {
    parseWaypoints("assets/gpsvalues.csv", 0, 1, 2);
  }

  public static ArrayList<Waypoint> parseWaypoints(
      String file, int lonIndex, int latIndex, int altIndex) {
    String csvFile = file;
    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    ArrayList<Waypoint> points = new ArrayList<>();

    try {

      br = new BufferedReader(new FileReader(csvFile));
      while ((line = br.readLine()) != null) {

        // use comma as separator
        String[] readingLine = line.split(cvsSplitBy);

        //System.out.println("Longitude" + readingLine[7] + " , Latitude: " + readingLine[8]);

        points.add(
            new Waypoint(
                (float) Double.parseDouble(readingLine[lonIndex]),
                (float) Double.parseDouble(readingLine[latIndex]),
                (float) Double.parseDouble(readingLine[altIndex]) * 3.28084));
      }

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return points;
  }
}
