/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.utilities;

public class Vector3 {

  public double X;
  public double Y;
  public double Z;

  public Vector3(double x, double y, double z) {
    this.X = x;
    this.Y = y;
    this.Z = z;
  }

  //Static Vector Operations
  public static Vector3 CrossProduct(Vector3 A, Vector3 B) {
    Vector3 cross = new Vector3(0, 0, 0);
    cross.X = A.Y * B.Z - A.Z * B.Y;
    cross.Y = A.Z * B.X - A.X * B.Z;
    cross.Z = A.X * B.Y - A.Y * B.X;
    return cross;
  }

  public static double DotProduct(Vector3 A, Vector3 B) {
    return (A.X * B.X + A.Y * B.Y + A.Z * B.Z);
  }

  public static Vector3 Multiply(double A, Vector3 B) {
    return (new Vector3(B.X * A, B.Y * A, B.Z * A));
  }

  public static Vector3 Subtract(Vector3 A, Vector3 B) {
    Vector3 diff = new Vector3(0, 0, 0);
    diff.X = A.X - B.X;
    diff.Y = A.Y - B.Y;
    diff.Z = A.Z - B.Z;
    return diff;
  }

  public static double Length(Vector3 A) {
    return Math.sqrt(A.X * A.X + A.Y * A.Y + A.Z * A.Z);
  }

  public static Vector3 Normalize(Vector3 A) {
    Vector3 ret = new Vector3(0, 0, 0);
    double length = Vector3.Length(A);
    ret.X = A.X / length;
    ret.Y = A.Y / length;
    ret.Z = A.Z / length;
    return ret;
  }
}
