package acap.utilities;


import javax.sound.sampled.AudioInputStream;
import marytts.LocalMaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;

public class TTS {
  LocalMaryInterface mary = null;
  public TTS(){
    try {
      mary = new LocalMaryInterface();
    } catch (MaryConfigurationException e) {
      System.err.println("Could not initialize MaryTTS interface: " + e.getMessage());
    }
  }

  public void speak(String text){
    AudioInputStream audio = null;
    try {
      audio = mary.generateAudio(text);
    } catch (SynthesisException e) {
      e.printStackTrace();
    }

    Audio.getInstance().playAudio(audio,0);

  }
  }
