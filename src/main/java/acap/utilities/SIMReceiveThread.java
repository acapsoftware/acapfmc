/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.utilities;

import static acap.world.view.WorldView.drawer;

import acap.fmc.UserAircraft;
import acap.fmc.procedure.SubscribeThread;
import acap.fmc.routing.Waypoint;
import com.flightsim.fsuipc.FSUIPC;
import com.flightsim.fsuipc.fsuipc_wrapper;
import java.util.ArrayList;

// create seperate program for running this and listening to messages
/** Created by enadel on 11/13/16. */
public class SIMReceiveThread implements Runnable {

  public static ArrayList<Waypoint> points = new ArrayList<>();
  static FSUIPC fsuipc;
  private short flapDetents = 4;

  public SIMReceiveThread() {
    int ret = fsuipc_wrapper.Open(fsuipc_wrapper.SIM_ANY);
    fsuipc = new FSUIPC();
  }

  public static void main(String[] args) {

    (new Thread(new SubscribeThread())).start();
    (new Thread(new SIMReceiveThread())).start();

    while (true) {

      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void run() {
    System.out.println("SIM THREAD ACTIVAQTED");
    while (true) {
      try {
        if (points.size() < 500)
          points.add(
              new Waypoint((float) getLatitude(), (float) getLongitude(), (float) getAltitude()));
        else points.clear();

        UserAircraft.getInstance().setLon(getLongitude());
        UserAircraft.getInstance().setLat(getLatitude());
        UserAircraft.getInstance().setAltitude(getAltitude());
        UserAircraft.getInstance().setHeading((float) getHeading() - 180);
        drawer.redraw();

        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public double getLatitude() {
    double d = (double) fsuipc.getLong(0x0560);

    d = 90.0 * d / (10001750.0 * 65536.0 * 65536.0);
    return d;
  }

  public double getLongitude() {
    double d = (double) fsuipc.getLong(0x0568);

    d = 360.0 * d / (65536.0 * 65536.0 * 65536.0 * 65536.0);
    return d;
  }

  public int getAltitude() {
    return (int) (3.28084 * fsuipc.getInt(0x0574));
  }

  int getVerticalSpeed() {
    return (int) (fsuipc.getInt(0x02c8) * 60 * 3.28084 / 256);
  }

  short getOAT() {
    return (short) (fsuipc.getShort(0x0E8C) / 256);
  }

  int getIAS() {
    return fsuipc.getInt(0x02bc) / 128;
  }

  public static double getRoll() {
    return fsuipc.getDouble(0x2f78);
  }

  public static double getPitch() {
    return fsuipc.getDouble(0x2f70);
  }

  int getHeading() {
    return (int) ((fsuipc.getInt(0x0580) * 360.0 / (65536.0 * 65536.0)) + 360) % 360;
  }

  short getFlapPosition() {
    return fsuipc.getShort(0x0BDC);
  }

  short getFlapDetent() {
    return (short) (fsuipc.getShort(0x0BDC) / (16383 / flapDetents));
  }

  short getThrottle() {
    return fsuipc.getShort(0x088C);
  }

  short getMixture() {
    return fsuipc.getShort(0x0890);
  }

  short getStarterPosition() {
    return fsuipc.getShort(0x0892); //off,L,R,B,S
  }

  short getRPM() {
    return (short) (fsuipc.getShort(0x0898) * fsuipc.getShort(0x08C8) / 65536);
  }

  short getElevator() {
    return fsuipc.getShort(0x0BB4);
  }

  short getRudder() {
    return fsuipc.getShort(0x0BBC);
  }

  short getAileron() {
    return fsuipc.getShort(0x0BB8);
  }

  Float getRudderTrim() {
    return fsuipc.getFloat(0x2EC0);
  }

  short getElevatorTrim() {
    return fsuipc.getShort(0x0BC2);
  }

  Double getXAccel() {
    return fsuipc.getDouble(0x3060);
  }

  Double getYAccel() {
    return fsuipc.getDouble(0x3068);
  }

  Double getZAccel() {
    return fsuipc.getDouble(0x3070);
  }

  public static double getPitchAccel() {
    return fsuipc.getDouble(0x3078);
  }

  public static double getRollAccel() {
    return fsuipc.getDouble(0x3080);
  }

  public static double getYawAccel() {
    return fsuipc.getDouble(0x3088);
  }

  short getFuelSelector() {
    return fsuipc.getShort(0x0AF8);
  }

  short getRadarAltitude() {
    return fsuipc.getShort(0x0B4C);
  }

  short getLeftBrake() {
    return fsuipc.getShort(0x0BC4);
  }

  short getRightBrake() {
    return fsuipc.getShort(0x0BC6);
  }

  boolean getGroundSwitches(){ //true = on ground,  false = in air

    if((short)fsuipc.getShort(0x0366) == 0)
      return false;
    else
      return true;

  }
}
