/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.utilities;

/** Created by erikn on 2/26/2017. */
public class Kalman1D {

  private KalmanFilter filt;

  public Kalman1D(double measurementNoise) {
    filt = new KalmanFilter(2, 1);
    filt.getState_transition().set_matrix(1.0, 1.0, 0.0, 1.0);
    filt.getObservation_model().set_matrix(1.0, 0.0);

    filt.getProcess_noise_covariance().set_identity_matrix();
    filt.getObservation_noise_covariance().set_matrix(measurementNoise);

    /* We set the covariance to indicate that the initial value is extremely deviated from a true value*/
    double deviation = 1000.0;
    filt.state_estimate.set_matrix(10 * 1000.0, 0.0);
    filt.estimate_covariance.set_identity_matrix();
    filt.estimate_covariance.scale_matrix(deviation * deviation);
  }

  public void addObservation(double observation_point) {
    filt.observation.set_matrix(observation_point);
    filt.update();
  }

  public double getEstimatedPosition() {
    return filt.state_estimate.data[0][0];
  }

  public double getEstimatedChangeRate() {
    return filt.state_estimate.data[1][0];
  }
}
