/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.controls;

import acap.fmc.controls.SerialMessage.ControlMessage.component;
import com.google.protobuf.Message;

/**
 * Class for controlling individual components based on percentage
 */
public class ComponentController {

  private static double smoothness;
  private static double velocity;
  private double position;
  private boolean setup = false;
  private component targetComponent;

  /**
   * Create a component controller with the intended target
   * @param targetComponent - the component to set a position for (use constants found in SerialMessage class)
   */
  public ComponentController(component targetComponent) {
    this.targetComponent = targetComponent;
  }

  /**
   * Set the desired position
   * @param position - percentage
   */
  public void setPosition(double position) {
    this.position = position;
  }

  /**
   * Sets the smoothness of the motors
   * @param smooth
   */
  public void setSmoothness(double smooth) {
    smoothness = smooth;
  }

  /**
   * Sets the velocity of the motors
   * @param vel
   */
  public void setVelocity(double vel) {
    velocity = vel;
  }

  /**
   * Indiciate that this component control message will contain a desired set smoothness/velocity.
   * THIS MUST BE TURNED ON TO USE THE VELOCITY AND SMOOTHNESS
   */
  public void turnOnSetup() {
    setup = true;
  }

  /**
   * Generate a message that can be sent using the MsgPublisher. This message is interpreted by
   * the ECE side into an actual movement.
   *
   * @return
   */
  public Message serialize() {
    SerialMessage.ControlMessage.Builder msg = SerialMessage.ControlMessage.newBuilder();
    msg.setSourceComponent(SerialMessage.ControlMessage.component.FLIGHT_COMPUTER);
    msg.setTargetComponent(this.targetComponent);
    msg.setPosition(this.position);
    msg.setIsSetup(this.setup);
    msg.setMinPosition(Devices.getInstance().getControllerMin(targetComponent));
    msg.setMaxPosition(Devices.getInstance().getControllerMax(targetComponent));
    msg.setVelocity(velocity);
    msg.setSmoothness(smoothness);

    return msg.build();
  }
}
