/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.controls;

public final class SerialMessage {
  private SerialMessage() {}

  public static void registerAllExtensions(com.google.protobuf.ExtensionRegistryLite registry) {}

  public static void registerAllExtensions(com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions((com.google.protobuf.ExtensionRegistryLite) registry);
  }

  public interface ControlMessageOrBuilder
      extends
      // @@protoc_insertion_point(interface_extends:ACAP.ControlMessage)
      com.google.protobuf.MessageOrBuilder {

    /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
    int getTargetComponentValue();
    /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
    SerialMessage.ControlMessage.component getTargetComponent();

    /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
    int getSourceComponentValue();
    /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
    SerialMessage.ControlMessage.component getSourceComponent();

    /** <code>optional double position = 3;</code> */
    double getPosition();

    /** <code>optional bool isSetup = 4;</code> */
    boolean getIsSetup();

    /** <code>optional double maxPosition = 5;</code> */
    double getMaxPosition();

    /** <code>optional double minPosition = 6;</code> */
    double getMinPosition();

    /** <code>optional double velocity = 7;</code> */
    double getVelocity();

    /** <code>optional double smoothness = 8;</code> */
    double getSmoothness();
  }
  /** Protobuf type {@code ACAP.ControlMessage} */
  public static final class ControlMessage extends com.google.protobuf.GeneratedMessageV3
      implements
      // @@protoc_insertion_point(message_implements:ACAP.ControlMessage)
      ControlMessageOrBuilder {
    // Use ControlMessage.newBuilder() to construct.
    private ControlMessage(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }

    private ControlMessage() {
      targetComponent_ = 0;
      sourceComponent_ = 0;
      position_ = 0D;
      isSetup_ = false;
      maxPosition_ = 0D;
      minPosition_ = 0D;
      velocity_ = 0D;
      smoothness_ = 0D;
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }

    private ControlMessage(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default:
              {
                if (!input.skipField(tag)) {
                  done = true;
                }
                break;
              }
            case 8:
              {
                int rawValue = input.readEnum();

                targetComponent_ = rawValue;
                break;
              }
            case 16:
              {
                int rawValue = input.readEnum();

                sourceComponent_ = rawValue;
                break;
              }
            case 25:
              {
                position_ = input.readDouble();
                break;
              }
            case 32:
              {
                isSetup_ = input.readBool();
                break;
              }
            case 41:
              {
                maxPosition_ = input.readDouble();
                break;
              }
            case 49:
              {
                minPosition_ = input.readDouble();
                break;
              }
            case 57:
              {
                velocity_ = input.readDouble();
                break;
              }
            case 65:
              {
                smoothness_ = input.readDouble();
                break;
              }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }

    public static final com.google.protobuf.Descriptors.Descriptor getDescriptor() {
      return SerialMessage.internal_static_ACAP_ControlMessage_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return SerialMessage.internal_static_ACAP_ControlMessage_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              SerialMessage.ControlMessage.class, SerialMessage.ControlMessage.Builder.class);
    }

    /** Protobuf enum {@code ACAP.ControlMessage.component} */
    public enum component implements com.google.protobuf.ProtocolMessageEnum {
      /** <code>ALL = 0;</code> */
      ALL(0),
      /** <code>STABILIATOR = 1;</code> */
      STABILIATOR(1),
      /** <code>RUDDER = 2;</code> */
      RUDDER(2),
      /** <code>THROTTLE = 3;</code> */
      THROTTLE(3),
      /** <code>MIXTURE = 4;</code> */
      MIXTURE(4),
      /** <code>CARB_HEAT = 5;</code> */
      CARB_HEAT(5),
      /** <code>BRAKES = 6;</code> */
      BRAKES(6),
      /** <code>PITCH_TRIM = 7;</code> */
      PITCH_TRIM(7),
      /** <code>RUDDER_TRIM = 8;</code> */
      RUDDER_TRIM(8),
      /** <code>FLAPS = 9;</code> */
      FLAPS(9),
      /** <code>LEFT_MAGNETO = 10;</code> */
      LEFT_MAGNETO(10),
      /** <code>RIGHT_MAGNETO = 11;</code> */
      RIGHT_MAGNETO(11),
      /** <code>STARTER = 12;</code> */
      STARTER(12),
      /** <code>LANDING_LIGHT = 13;</code> */
      LANDING_LIGHT(13),
      /** <code>ROTATING_BEACON = 14;</code> */
      ROTATING_BEACON(14),
      /** <code>FUEL_PUMP = 15;</code> */
      FUEL_PUMP(15),
      /** <code>AILERON = 16;</code> */
      AILERON(16),
      /** <code>FLIGHT_COMPUTER = 17;</code> */
      FLIGHT_COMPUTER(17),
      /** <code>STABILATOR_TRIM = 18;</code> */
      STABILATOR_TRIM(18),
      /** <code>NAVIGATION_LIGHT = 19;</code> */
      NAVIGATION_LIGHT(19),
      UNRECOGNIZED(-1),
      ;

      /** <code>ALL = 0;</code> */
      public static final int ALL_VALUE = 0;
      /** <code>STABILIATOR = 1;</code> */
      public static final int STABILIATOR_VALUE = 1;
      /** <code>RUDDER = 2;</code> */
      public static final int RUDDER_VALUE = 2;
      /** <code>THROTTLE = 3;</code> */
      public static final int THROTTLE_VALUE = 3;
      /** <code>MIXTURE = 4;</code> */
      public static final int MIXTURE_VALUE = 4;
      /** <code>CARB_HEAT = 5;</code> */
      public static final int CARB_HEAT_VALUE = 5;
      /** <code>BRAKES = 6;</code> */
      public static final int BRAKES_VALUE = 6;
      /** <code>PITCH_TRIM = 7;</code> */
      public static final int PITCH_TRIM_VALUE = 7;
      /** <code>RUDDER_TRIM = 8;</code> */
      public static final int RUDDER_TRIM_VALUE = 8;
      /** <code>FLAPS = 9;</code> */
      public static final int FLAPS_VALUE = 9;
      /** <code>LEFT_MAGNETO = 10;</code> */
      public static final int LEFT_MAGNETO_VALUE = 10;
      /** <code>RIGHT_MAGNETO = 11;</code> */
      public static final int RIGHT_MAGNETO_VALUE = 11;
      /** <code>STARTER = 12;</code> */
      public static final int STARTER_VALUE = 12;
      /** <code>LANDING_LIGHT = 13;</code> */
      public static final int LANDING_LIGHT_VALUE = 13;
      /** <code>ROTATING_BEACON = 14;</code> */
      public static final int ROTATING_BEACON_VALUE = 14;
      /** <code>FUEL_PUMP = 15;</code> */
      public static final int FUEL_PUMP_VALUE = 15;
      /** <code>AILERON = 16;</code> */
      public static final int AILERON_VALUE = 16;
      /** <code>FLIGHT_COMPUTER = 17;</code> */
      public static final int FLIGHT_COMPUTER_VALUE = 17;
      /** <code>STABILATOR_TRIM = 18;</code> */
      public static final int STABILATOR_TRIM_VALUE = 18;
      /** <code>NAVIGATION_LIGHT = 19;</code> */
      public static final int NAVIGATION_LIGHT_VALUE = 19;

      public final int getNumber() {
        if (this == UNRECOGNIZED) {
          throw new java.lang.IllegalArgumentException(
              "Can't get the number of an unknown enum value.");
        }
        return value;
      }

      /** @deprecated Use {@link #forNumber(int)} instead. */
      @java.lang.Deprecated
      public static component valueOf(int value) {
        return forNumber(value);
      }

      public static component forNumber(int value) {
        switch (value) {
          case 0:
            return ALL;
          case 1:
            return STABILIATOR;
          case 2:
            return RUDDER;
          case 3:
            return THROTTLE;
          case 4:
            return MIXTURE;
          case 5:
            return CARB_HEAT;
          case 6:
            return BRAKES;
          case 7:
            return PITCH_TRIM;
          case 8:
            return RUDDER_TRIM;
          case 9:
            return FLAPS;
          case 10:
            return LEFT_MAGNETO;
          case 11:
            return RIGHT_MAGNETO;
          case 12:
            return STARTER;
          case 13:
            return LANDING_LIGHT;
          case 14:
            return ROTATING_BEACON;
          case 15:
            return FUEL_PUMP;
          case 16:
            return AILERON;
          case 17:
            return FLIGHT_COMPUTER;
          case 18:
            return STABILATOR_TRIM;
          case 19:
            return NAVIGATION_LIGHT;
          default:
            return null;
        }
      }

      public static com.google.protobuf.Internal.EnumLiteMap<component> internalGetValueMap() {
        return internalValueMap;
      }

      private static final com.google.protobuf.Internal.EnumLiteMap<component> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<component>() {
            public component findValueByNumber(int number) {
              return component.forNumber(number);
            }
          };

      public final com.google.protobuf.Descriptors.EnumValueDescriptor getValueDescriptor() {
        return getDescriptor().getValues().get(ordinal());
      }

      public final com.google.protobuf.Descriptors.EnumDescriptor getDescriptorForType() {
        return getDescriptor();
      }

      public static final com.google.protobuf.Descriptors.EnumDescriptor getDescriptor() {
        return SerialMessage.ControlMessage.getDescriptor().getEnumTypes().get(0);
      }

      private static final component[] VALUES = values();

      public static component valueOf(com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
        if (desc.getType() != getDescriptor()) {
          throw new java.lang.IllegalArgumentException("EnumValueDescriptor is not for this type.");
        }
        if (desc.getIndex() == -1) {
          return UNRECOGNIZED;
        }
        return VALUES[desc.getIndex()];
      }

      private final int value;

      private component(int value) {
        this.value = value;
      }

      // @@protoc_insertion_point(enum_scope:ACAP.ControlMessage.component)
    }

    public static final int TARGETCOMPONENT_FIELD_NUMBER = 1;
    private int targetComponent_;
    /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
    public int getTargetComponentValue() {
      return targetComponent_;
    }
    /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
    public SerialMessage.ControlMessage.component getTargetComponent() {
      SerialMessage.ControlMessage.component result =
          SerialMessage.ControlMessage.component.valueOf(targetComponent_);
      return result == null ? SerialMessage.ControlMessage.component.UNRECOGNIZED : result;
    }

    public static final int SOURCECOMPONENT_FIELD_NUMBER = 2;
    private int sourceComponent_;
    /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
    public int getSourceComponentValue() {
      return sourceComponent_;
    }
    /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
    public SerialMessage.ControlMessage.component getSourceComponent() {
      SerialMessage.ControlMessage.component result =
          SerialMessage.ControlMessage.component.valueOf(sourceComponent_);
      return result == null ? SerialMessage.ControlMessage.component.UNRECOGNIZED : result;
    }

    public static final int POSITION_FIELD_NUMBER = 3;
    private double position_;
    /** <code>optional double position = 3;</code> */
    public double getPosition() {
      return position_;
    }

    public static final int ISSETUP_FIELD_NUMBER = 4;
    private boolean isSetup_;
    /** <code>optional bool isSetup = 4;</code> */
    public boolean getIsSetup() {
      return isSetup_;
    }

    public static final int MAXPOSITION_FIELD_NUMBER = 5;
    private double maxPosition_;
    /** <code>optional double maxPosition = 5;</code> */
    public double getMaxPosition() {
      return maxPosition_;
    }

    public static final int MINPOSITION_FIELD_NUMBER = 6;
    private double minPosition_;
    /** <code>optional double minPosition = 6;</code> */
    public double getMinPosition() {
      return minPosition_;
    }

    public static final int VELOCITY_FIELD_NUMBER = 7;
    private double velocity_;
    /** <code>optional double velocity = 7;</code> */
    public double getVelocity() {
      return velocity_;
    }

    public static final int SMOOTHNESS_FIELD_NUMBER = 8;
    private double smoothness_;
    /** <code>optional double smoothness = 8;</code> */
    public double getSmoothness() {
      return smoothness_;
    }

    private byte memoizedIsInitialized = -1;

    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output) throws java.io.IOException {
      if (targetComponent_ != SerialMessage.ControlMessage.component.ALL.getNumber()) {
        output.writeEnum(1, targetComponent_);
      }
      if (sourceComponent_ != SerialMessage.ControlMessage.component.ALL.getNumber()) {
        output.writeEnum(2, sourceComponent_);
      }
      if (position_ != 0D) {
        output.writeDouble(3, position_);
      }
      if (isSetup_ != false) {
        output.writeBool(4, isSetup_);
      }
      if (maxPosition_ != 0D) {
        output.writeDouble(5, maxPosition_);
      }
      if (minPosition_ != 0D) {
        output.writeDouble(6, minPosition_);
      }
      if (velocity_ != 0D) {
        output.writeDouble(7, velocity_);
      }
      if (smoothness_ != 0D) {
        output.writeDouble(8, smoothness_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (targetComponent_ != SerialMessage.ControlMessage.component.ALL.getNumber()) {
        size += com.google.protobuf.CodedOutputStream.computeEnumSize(1, targetComponent_);
      }
      if (sourceComponent_ != SerialMessage.ControlMessage.component.ALL.getNumber()) {
        size += com.google.protobuf.CodedOutputStream.computeEnumSize(2, sourceComponent_);
      }
      if (position_ != 0D) {
        size += com.google.protobuf.CodedOutputStream.computeDoubleSize(3, position_);
      }
      if (isSetup_ != false) {
        size += com.google.protobuf.CodedOutputStream.computeBoolSize(4, isSetup_);
      }
      if (maxPosition_ != 0D) {
        size += com.google.protobuf.CodedOutputStream.computeDoubleSize(5, maxPosition_);
      }
      if (minPosition_ != 0D) {
        size += com.google.protobuf.CodedOutputStream.computeDoubleSize(6, minPosition_);
      }
      if (velocity_ != 0D) {
        size += com.google.protobuf.CodedOutputStream.computeDoubleSize(7, velocity_);
      }
      if (smoothness_ != 0D) {
        size += com.google.protobuf.CodedOutputStream.computeDoubleSize(8, smoothness_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
        return true;
      }
      if (!(obj instanceof SerialMessage.ControlMessage)) {
        return super.equals(obj);
      }
      SerialMessage.ControlMessage other = (SerialMessage.ControlMessage) obj;

      boolean result = true;
      result = result && targetComponent_ == other.targetComponent_;
      result = result && sourceComponent_ == other.sourceComponent_;
      result =
          result
              && (java.lang.Double.doubleToLongBits(getPosition())
                  == java.lang.Double.doubleToLongBits(other.getPosition()));
      result = result && (getIsSetup() == other.getIsSetup());
      result =
          result
              && (java.lang.Double.doubleToLongBits(getMaxPosition())
                  == java.lang.Double.doubleToLongBits(other.getMaxPosition()));
      result =
          result
              && (java.lang.Double.doubleToLongBits(getMinPosition())
                  == java.lang.Double.doubleToLongBits(other.getMinPosition()));
      result =
          result
              && (java.lang.Double.doubleToLongBits(getVelocity())
                  == java.lang.Double.doubleToLongBits(other.getVelocity()));
      result =
          result
              && (java.lang.Double.doubleToLongBits(getSmoothness())
                  == java.lang.Double.doubleToLongBits(other.getSmoothness()));
      return result;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptorForType().hashCode();
      hash = (37 * hash) + TARGETCOMPONENT_FIELD_NUMBER;
      hash = (53 * hash) + targetComponent_;
      hash = (37 * hash) + SOURCECOMPONENT_FIELD_NUMBER;
      hash = (53 * hash) + sourceComponent_;
      hash = (37 * hash) + POSITION_FIELD_NUMBER;
      hash =
          (53 * hash)
              + com.google.protobuf.Internal.hashLong(
                  java.lang.Double.doubleToLongBits(getPosition()));
      hash = (37 * hash) + ISSETUP_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashBoolean(getIsSetup());
      hash = (37 * hash) + MAXPOSITION_FIELD_NUMBER;
      hash =
          (53 * hash)
              + com.google.protobuf.Internal.hashLong(
                  java.lang.Double.doubleToLongBits(getMaxPosition()));
      hash = (37 * hash) + MINPOSITION_FIELD_NUMBER;
      hash =
          (53 * hash)
              + com.google.protobuf.Internal.hashLong(
                  java.lang.Double.doubleToLongBits(getMinPosition()));
      hash = (37 * hash) + VELOCITY_FIELD_NUMBER;
      hash =
          (53 * hash)
              + com.google.protobuf.Internal.hashLong(
                  java.lang.Double.doubleToLongBits(getVelocity()));
      hash = (37 * hash) + SMOOTHNESS_FIELD_NUMBER;
      hash =
          (53 * hash)
              + com.google.protobuf.Internal.hashLong(
                  java.lang.Double.doubleToLongBits(getSmoothness()));
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static SerialMessage.ControlMessage parseFrom(com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }

    public static SerialMessage.ControlMessage parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }

    public static SerialMessage.ControlMessage parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }

    public static SerialMessage.ControlMessage parseFrom(
        byte[] data, com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }

    public static SerialMessage.ControlMessage parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3.parseWithIOException(PARSER, input);
    }

    public static SerialMessage.ControlMessage parseFrom(
        java.io.InputStream input, com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3.parseWithIOException(
          PARSER, input, extensionRegistry);
    }

    public static SerialMessage.ControlMessage parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input);
    }

    public static SerialMessage.ControlMessage parseDelimitedFrom(
        java.io.InputStream input, com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3.parseDelimitedWithIOException(
          PARSER, input, extensionRegistry);
    }

    public static SerialMessage.ControlMessage parseFrom(com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3.parseWithIOException(PARSER, input);
    }

    public static SerialMessage.ControlMessage parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3.parseWithIOException(
          PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() {
      return newBuilder();
    }

    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(SerialMessage.ControlMessage prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }

    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /** Protobuf type {@code ACAP.ControlMessage} */
    public static final class Builder
        extends com.google.protobuf.GeneratedMessageV3.Builder<Builder>
        implements
        // @@protoc_insertion_point(builder_implements:ACAP.ControlMessage)
        SerialMessage.ControlMessageOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor getDescriptor() {
        return SerialMessage.internal_static_ACAP_ControlMessage_descriptor;
      }

      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return SerialMessage.internal_static_ACAP_ControlMessage_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                SerialMessage.ControlMessage.class, SerialMessage.ControlMessage.Builder.class);
      }

      // Construct using acap.model.SerialMessage.ControlMessage.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }

      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders) {}
      }

      public Builder clear() {
        super.clear();
        targetComponent_ = 0;

        sourceComponent_ = 0;

        position_ = 0D;

        isSetup_ = false;

        maxPosition_ = 0D;

        minPosition_ = 0D;

        velocity_ = 0D;

        smoothness_ = 0D;

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor getDescriptorForType() {
        return SerialMessage.internal_static_ACAP_ControlMessage_descriptor;
      }

      public SerialMessage.ControlMessage getDefaultInstanceForType() {
        return SerialMessage.ControlMessage.getDefaultInstance();
      }

      public SerialMessage.ControlMessage build() {
        SerialMessage.ControlMessage result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public SerialMessage.ControlMessage buildPartial() {
        SerialMessage.ControlMessage result = new SerialMessage.ControlMessage(this);
        result.targetComponent_ = targetComponent_;
        result.sourceComponent_ = sourceComponent_;
        result.position_ = position_;
        result.isSetup_ = isSetup_;
        result.maxPosition_ = maxPosition_;
        result.minPosition_ = minPosition_;
        result.velocity_ = velocity_;
        result.smoothness_ = smoothness_;
        onBuilt();
        return result;
      }

      public Builder clone() {
        return (Builder) super.clone();
      }

      public Builder setField(com.google.protobuf.Descriptors.FieldDescriptor field, Object value) {
        return (Builder) super.setField(field, value);
      }

      public Builder clearField(com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }

      public Builder clearOneof(com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }

      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field, int index, Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }

      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field, Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof SerialMessage.ControlMessage) {
          return mergeFrom((SerialMessage.ControlMessage) other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(SerialMessage.ControlMessage other) {
        if (other == SerialMessage.ControlMessage.getDefaultInstance()) return this;
        if (other.targetComponent_ != 0) {
          setTargetComponentValue(other.getTargetComponentValue());
        }
        if (other.sourceComponent_ != 0) {
          setSourceComponentValue(other.getSourceComponentValue());
        }
        if (other.getPosition() != 0D) {
          setPosition(other.getPosition());
        }
        if (other.getIsSetup() != false) {
          setIsSetup(other.getIsSetup());
        }
        if (other.getMaxPosition() != 0D) {
          setMaxPosition(other.getMaxPosition());
        }
        if (other.getMinPosition() != 0D) {
          setMinPosition(other.getMinPosition());
        }
        if (other.getVelocity() != 0D) {
          setVelocity(other.getVelocity());
        }
        if (other.getSmoothness() != 0D) {
          setSmoothness(other.getSmoothness());
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        SerialMessage.ControlMessage parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (SerialMessage.ControlMessage) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int targetComponent_ = 0;
      /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
      public int getTargetComponentValue() {
        return targetComponent_;
      }
      /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
      public Builder setTargetComponentValue(int value) {
        targetComponent_ = value;
        onChanged();
        return this;
      }
      /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
      public SerialMessage.ControlMessage.component getTargetComponent() {
        SerialMessage.ControlMessage.component result =
            SerialMessage.ControlMessage.component.valueOf(targetComponent_);
        return result == null ? SerialMessage.ControlMessage.component.UNRECOGNIZED : result;
      }
      /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
      public Builder setTargetComponent(SerialMessage.ControlMessage.component value) {
        if (value == null) {
          throw new NullPointerException();
        }

        targetComponent_ = value.getNumber();
        onChanged();
        return this;
      }
      /** <code>optional .ACAP.ControlMessage.component TargetComponent = 1;</code> */
      public Builder clearTargetComponent() {

        targetComponent_ = 0;
        onChanged();
        return this;
      }

      private int sourceComponent_ = 0;
      /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
      public int getSourceComponentValue() {
        return sourceComponent_;
      }
      /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
      public Builder setSourceComponentValue(int value) {
        sourceComponent_ = value;
        onChanged();
        return this;
      }
      /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
      public SerialMessage.ControlMessage.component getSourceComponent() {
        SerialMessage.ControlMessage.component result =
            SerialMessage.ControlMessage.component.valueOf(sourceComponent_);
        return result == null ? SerialMessage.ControlMessage.component.UNRECOGNIZED : result;
      }
      /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
      public Builder setSourceComponent(SerialMessage.ControlMessage.component value) {
        if (value == null) {
          throw new NullPointerException();
        }

        sourceComponent_ = value.getNumber();
        onChanged();
        return this;
      }
      /** <code>optional .ACAP.ControlMessage.component SourceComponent = 2;</code> */
      public Builder clearSourceComponent() {

        sourceComponent_ = 0;
        onChanged();
        return this;
      }

      private double position_;
      /** <code>optional double position = 3;</code> */
      public double getPosition() {
        return position_;
      }
      /** <code>optional double position = 3;</code> */
      public Builder setPosition(double value) {

        position_ = value;
        onChanged();
        return this;
      }
      /** <code>optional double position = 3;</code> */
      public Builder clearPosition() {

        position_ = 0D;
        onChanged();
        return this;
      }

      private boolean isSetup_;
      /** <code>optional bool isSetup = 4;</code> */
      public boolean getIsSetup() {
        return isSetup_;
      }
      /** <code>optional bool isSetup = 4;</code> */
      public Builder setIsSetup(boolean value) {

        isSetup_ = value;
        onChanged();
        return this;
      }
      /** <code>optional bool isSetup = 4;</code> */
      public Builder clearIsSetup() {

        isSetup_ = false;
        onChanged();
        return this;
      }

      private double maxPosition_;
      /** <code>optional double maxPosition = 5;</code> */
      public double getMaxPosition() {
        return maxPosition_;
      }
      /** <code>optional double maxPosition = 5;</code> */
      public Builder setMaxPosition(double value) {

        maxPosition_ = value;
        onChanged();
        return this;
      }
      /** <code>optional double maxPosition = 5;</code> */
      public Builder clearMaxPosition() {

        maxPosition_ = 0D;
        onChanged();
        return this;
      }

      private double minPosition_;
      /** <code>optional double minPosition = 6;</code> */
      public double getMinPosition() {
        return minPosition_;
      }
      /** <code>optional double minPosition = 6;</code> */
      public Builder setMinPosition(double value) {

        minPosition_ = value;
        onChanged();
        return this;
      }
      /** <code>optional double minPosition = 6;</code> */
      public Builder clearMinPosition() {

        minPosition_ = 0D;
        onChanged();
        return this;
      }

      private double velocity_;
      /** <code>optional double velocity = 7;</code> */
      public double getVelocity() {
        return velocity_;
      }
      /** <code>optional double velocity = 7;</code> */
      public Builder setVelocity(double value) {

        velocity_ = value;
        onChanged();
        return this;
      }
      /** <code>optional double velocity = 7;</code> */
      public Builder clearVelocity() {

        velocity_ = 0D;
        onChanged();
        return this;
      }

      private double smoothness_;
      /** <code>optional double smoothness = 8;</code> */
      public double getSmoothness() {
        return smoothness_;
      }
      /** <code>optional double smoothness = 8;</code> */
      public Builder setSmoothness(double value) {

        smoothness_ = value;
        onChanged();
        return this;
      }
      /** <code>optional double smoothness = 8;</code> */
      public Builder clearSmoothness() {

        smoothness_ = 0D;
        onChanged();
        return this;
      }

      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      // @@protoc_insertion_point(builder_scope:ACAP.ControlMessage)
    }

    // @@protoc_insertion_point(class_scope:ACAP.ControlMessage)
    private static final SerialMessage.ControlMessage DEFAULT_INSTANCE;

    static {
      DEFAULT_INSTANCE = new SerialMessage.ControlMessage();
    }

    public static SerialMessage.ControlMessage getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<ControlMessage> PARSER =
        new com.google.protobuf.AbstractParser<ControlMessage>() {
          public ControlMessage parsePartialFrom(
              com.google.protobuf.CodedInputStream input,
              com.google.protobuf.ExtensionRegistryLite extensionRegistry)
              throws com.google.protobuf.InvalidProtocolBufferException {
            return new ControlMessage(input, extensionRegistry);
          }
        };

    public static com.google.protobuf.Parser<ControlMessage> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<ControlMessage> getParserForType() {
      return PARSER;
    }

    public SerialMessage.ControlMessage getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }
  }

  private static final com.google.protobuf.Descriptors.Descriptor
      internal_static_ACAP_ControlMessage_descriptor;
  private static final com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ACAP_ControlMessage_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor getDescriptor() {
    return descriptor;
  }

  private static com.google.protobuf.Descriptors.FileDescriptor descriptor;

  static {
    java.lang.String[] descriptorData = {
      "\n\023SerialMessage.proto\022\004ACAP\"\301\004\n\016ControlM"
          + "essage\0227\n\017TargetComponent\030\001 \001(\0162\036.ACAP.C"
          + "ontrolMessage.component\0227\n\017SourceCompone"
          + "nt\030\002 \001(\0162\036.ACAP.ControlMessage.component"
          + "\022\020\n\010position\030\003 \001(\001\022\017\n\007isSetup\030\004 \001(\010\022\023\n\013m"
          + "axPosition\030\005 \001(\001\022\023\n\013minPosition\030\006 \001(\001\022\020\n"
          + "\010velocity\030\007 \001(\001\022\022\n\nsmoothness\030\010 \001(\001\"\311\002\n\t"
          + "component\022\007\n\003ALL\020\000\022\017\n\013STABILIATOR\020\001\022\n\n\006R"
          + "UDDER\020\002\022\014\n\010THROTTLE\020\003\022\013\n\007MIXTURE\020\004\022\r\n\tCA"
          + "RB_HEAT\020\005\022\n\n\006BRAKES\020\006\022\016\n\nPITCH_TRIM\020\007\022\017\n",
      "\013RUDDER_TRIM\020\010\022\t\n\005FLAPS\020\t\022\020\n\014LEFT_MAGNET"
          + "O\020\n\022\021\n\rRIGHT_MAGNETO\020\013\022\013\n\007STARTER\020\014\022\021\n\rL"
          + "ANDING_LIGHT\020\r\022\023\n\017ROTATING_BEACON\020\016\022\r\n\tF"
          + "UEL_PUMP\020\017\022\013\n\007AILERON\020\020\022\023\n\017FLIGHT_COMPUT"
          + "ER\020\021\022\023\n\017STABILATOR_TRIM\020\022\022\024\n\020NAVIGATION_"
          + "LIGHT\020\023B\014\n\nacap.modelb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor.internalBuildGeneratedFileFrom(
        descriptorData, new com.google.protobuf.Descriptors.FileDescriptor[] {}, assigner);
    internal_static_ACAP_ControlMessage_descriptor = getDescriptor().getMessageTypes().get(0);
    internal_static_ACAP_ControlMessage_fieldAccessorTable =
        new com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
            internal_static_ACAP_ControlMessage_descriptor,
            new java.lang.String[] {
              "TargetComponent",
              "SourceComponent",
              "Position",
              "IsSetup",
              "MaxPosition",
              "MinPosition",
              "Velocity",
              "Smoothness",
            });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
