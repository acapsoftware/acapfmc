package acap.fmc.controls;

public class Autoflight {

  private int desiredAltitude = 0;
  private float desiredPitch = 0;
  private float desiredRoll = 0;
  private float desiredSpeed = 0;
  private boolean autopilotMaster = true;
  private boolean autoPilotDisconnectSwitch = false;
  private int commandMode = 0;
  private boolean lnav = true;
  private boolean vnav = false;
  private boolean autothrottle = true;
  private boolean headingHold = false;
  private boolean verticalSpeedHold = false;
  private boolean speedHold = true;
  private boolean yawDamper = true;
  private boolean altitudeHold = false;
  private boolean toga = false;
  private int phaseOfFlight = 4;
  private int desiredHeading = 250;
  private int desiredVerticalSpeed = 700;
  public Autoflight() {
  }

  public float getDesiredSpeed() {
    return desiredSpeed;
  }

  public void setDesiredSpeed(float desiredSpeed) {
    this.desiredSpeed = desiredSpeed;
  }

  public boolean isAutoPilotDisconnectSwitch() {
    return autoPilotDisconnectSwitch;
  }

  public void setAutoPilotDisconnectSwitch(boolean autoPilotDisconnectSwitch) {
    this.autoPilotDisconnectSwitch = autoPilotDisconnectSwitch;
  }

  public boolean isAutopilotMaster() {
    return autopilotMaster;
  }

  public void setAutopilotMaster(boolean autopilotMaster) {
    this.autopilotMaster = autopilotMaster;
  }

  public int getCommandMode() {
    return commandMode;
  }

  public void setCommandMode(int commandMode) {
    this.commandMode = commandMode;
  }

  public boolean isLnav() {
    return lnav;
  }

  public void setLnav(boolean lnav) {
    this.lnav = lnav;
  }

  public boolean isVnav() {
    return vnav;
  }

  public void setVnav(boolean vnav) {
    this.vnav = vnav;
  }

  public boolean isAutothrottle() {
    return autothrottle;
  }

  public void setAutothrottle(boolean autothrottle) {
    this.autothrottle = autothrottle;
  }

  public boolean isHeadingHold() {
    return headingHold;
  }

  public void setHeadingHold(boolean headingHold) {
    this.headingHold = headingHold;
  }

  public boolean isVerticalSpeedHold() {
    return verticalSpeedHold;
  }

  public void setVerticalSpeedHold(boolean verticalSpeedHold) {
    this.verticalSpeedHold = verticalSpeedHold;
  }

  public boolean isSpeedHold() {
    return speedHold;
  }

  public void setSpeedHold(boolean speedHold) {
    this.speedHold = speedHold;
  }

  public boolean isYawDamper() {
    return yawDamper;
  }

  public void setYawDamper(boolean yawDamper) {
    this.yawDamper = yawDamper;
  }

  public boolean isAltitudeHold() {
    return altitudeHold;
  }

  public void setAltitudeHold(boolean altitudeHold) {
    this.altitudeHold = altitudeHold;
  }

  public boolean isToga() {
    return toga;
  }

  public void setToga(boolean toga) {
    this.toga = toga;
  }

  public int getPhaseOfFlight() {
    return phaseOfFlight;
  }


  public int getDesiredHeading() {
    return desiredHeading;
  }

  public void setDesiredHeading(int desiredHeading) {
    this.desiredHeading = desiredHeading;
  }

  public int getDesiredVerticalSpeed() {
    return desiredVerticalSpeed;
  }

  public void setDesiredVerticalSpeed(int desiredVerticalSpeed) {
    this.desiredVerticalSpeed = desiredVerticalSpeed;
  }

  public int getDesiredAltitude() {
    return desiredAltitude;
  }

  public void setDesiredAltitude(int desiredAltitude) {
    this.desiredAltitude = desiredAltitude;
  }

  public float getDesiredPitch() {
    return desiredPitch;
  }

  public void setDesiredPitch(float desiredPitch) {
    this.desiredPitch = desiredPitch;
  }

  public float getDesiredRoll() {
    return desiredRoll;
  }

  public void setDesiredRoll(float desiredRoll) {
    this.desiredRoll = desiredRoll;
  }
}