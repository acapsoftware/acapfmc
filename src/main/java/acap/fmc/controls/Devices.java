/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.controls;

import acap.fmc.controls.SerialMessage.ControlMessage.component;
import acap.fmc.routing.FlightSimInterface;
import java.util.HashMap;

/** Created by tjnickerson on 12/6/16. */
public class Devices {
  public static final String ALL = "ALL";
  public static final String AILERON = "AILERON";
  public static final String BRAKES = "BRAKES";
  public static final String CARB_HEAT = "CARBHEAT";
  public static final String FLAPS = "FLAPS";
  public static final String MAG_LEFT = "LEFTMAG";
  public static final String MAG_RIGHT = "RIGHTMAG";
  public static final String MIXTURE = "MIXTURE";
  public static final String PITCH_TRIM = "PITCHTRIM";
  public static final String RUDDER = "RUDDER";
  public static final String RUDDERTRIM = "RUDDERTRIM";
  public static final String STABILATOR = "STABILATOR";
  public static final String STARTER = "STARTER";
  public static final String THROTTLE = "THROTTLE";
  public static final String LANDING_LIGHT = "LANDING_LIGHT";
  public static final String ROTATING_BEACON = "ROTATING_BEACON";
  public static final String FUEL_PUMP = "FUEL_PUMP";
  private static final int MAX = 1;
  private static final int MIN = 0;
  private static final int POSITION = 2;
  private static final int LAST_REQUESTED_VALUE = 3;
  private static final int FSINTERFACE = 4;
  private static final Double DEFAULT_MIN = 0D;
  private static final Double DEFAULT_MAX = 1024D;
  private static final Double DEFAULT_POSITION = 0D;


  private static Devices instance = new Devices();

  private HashMap<component, Object[]> map;
  private HashMap<String, component> smap;

  public static Devices getInstance() {
    return instance;
  }

  public Devices() {
    this.smap = new HashMap<>();
    this.map = new HashMap<>();

    smap.put(ALL, component.ALL);
    smap.put(AILERON, component.AILERON);
    smap.put(BRAKES, component.BRAKES);
    smap.put(CARB_HEAT, component.CARB_HEAT);
    smap.put(FLAPS, component.FLAPS);
    smap.put(MAG_LEFT, component.LEFT_MAGNETO);
    smap.put(MAG_RIGHT, component.RIGHT_MAGNETO);
    smap.put(MIXTURE, component.MIXTURE);
    smap.put(PITCH_TRIM, component.PITCH_TRIM);
    smap.put(RUDDER, component.RUDDER);
    smap.put(RUDDERTRIM, component.RUDDER_TRIM);
    smap.put(STABILATOR, component.STABILIATOR);
    smap.put(THROTTLE, component.THROTTLE);
    smap.put(LANDING_LIGHT, component.LANDING_LIGHT);
    smap.put(ROTATING_BEACON, component.ROTATING_BEACON);
    smap.put(FUEL_PUMP, component.FUEL_PUMP);

    for (component c : SerialMessage.ControlMessage.component.values()) {
      map.put(
          c, new Object[] {DEFAULT_MIN, DEFAULT_MAX, DEFAULT_POSITION, DEFAULT_POSITION, new FlightSimInterface(c)});
    }
  }

  public component getComponentByString(String c) {
    if (smap.containsKey(c)) return smap.get(c);
    else return component.UNRECOGNIZED;
  }

  public void setControllerMax(component c, double value) {
    map.get(c)[MAX] = value;
  }

  public void setControllerMin(component c, double value) {
    map.get(c)[MIN] = value;
  }

  public void setControllerReading(component c, double value) {
    map.get(c)[POSITION] = value;
  }

  public void setControlValueRequested(component c, double value){
    map.get(c)[LAST_REQUESTED_VALUE] = value;
  }

  public double getControllerMax(component c) {
    return (double) map.get(c)[MAX];
  }

  public double getControllerMin(component c) {
    return (double) map.get(c)[MIN];
  }

  public double getControllerReading(component c) {
    return (double) map.get(c)[POSITION];
  }

  public double getLastControllerValueSet(component c) { return (double) map.get(c)[LAST_REQUESTED_VALUE];}

  public FlightSimInterface getFSInterface(component c) {
    return (FlightSimInterface) map.get(c)[FSINTERFACE];
  }
}
