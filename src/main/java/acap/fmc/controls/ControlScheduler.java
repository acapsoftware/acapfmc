/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.controls;

import acap.fmc.Constants;
import acap.fmc.gui.components.ProgressBar;
import acap.fmc.gui.windows.TaskPane;
import acap.fmc.procedure.MsgPublisher;
import acap.fmc.procedure.Task;
import acap.fmc.procedure.TaskGroup;
import acap.fmc.procedure.TaskQueue;

import java.util.Map;

/**
 * The control scheduler is a thread that monitors the task queue and runs tasks that are waiting
 * to be completed. It is a singleton and is accessed by calling ControlScheduler.getInstance()
 */
public class ControlScheduler implements Runnable {

  public static boolean PAUSED = Constants.DEBUG;
  TaskQueue queue;
  TaskGroup currentTaskGroup = null;

  private static ControlScheduler instance;

  private ControlScheduler() {
    this.queue = TaskQueue.getInstance();
  }

  /**
   * Get an instance of the control scheduler
   * @return
   */
  public static ControlScheduler getInstance() {
    if (instance == null)
      instance = new ControlScheduler();
    return instance;
  }

  /**
   * Retrieve the current task group that is being run
   * @return
   */
  public synchronized TaskGroup getCurrentTaskGroup() {
    return this.currentTaskGroup;
  }

  /**
   * Get the next task group in the task queue
   */
  public synchronized void getNextTaskGroup() {
    if(this.currentTaskGroup == null) {
      this.currentTaskGroup = this.queue.getNextTaskGroup();
    }
    else{
      if(!this.currentTaskGroup.hasNextTask())
        this.currentTaskGroup = this.queue.hasNextTaskGroup()? this.queue.getNextTaskGroup() : null;
    }
  }

  /**
   * When an object implementing interface <code>Runnable</code> is used to create a thread,
   * starting the thread causes the object's <code>run</code> method to be called in that separately
   * executing thread.
   *
   * <p>The general contract of the method <code>run</code> is that it may take any action
   * whatsoever.
   *
   * @see Thread#run()
   */
  @Override
  public void run() {
    while (true) {
      if (!PAUSED) {
        if (this.queue.hasNextTaskGroup()) {
          currentTaskGroup = this.queue.getNextTaskGroup();
          System.out.println("EXECUTING TASK");
          while (currentTaskGroup.hasNextTask()) {
            Task task = currentTaskGroup.getNextTask();
            try {
              TaskPane tp = TaskPane.getInstance();
              Map<Task, ProgressBar> map = tp.getTaskProgressMap();
              task.run();
              if(map.containsKey(task))
                map.get(task).start();
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        }
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
