/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc;

import static acap.fmc.Constants.PLANE_START;
import static acap.world.view.WorldView.drawer;

import acap.fmc.controls.Autoflight;
import acap.fmc.controls.Devices;
import acap.fmc.controls.SerialMessage.ControlMessage.component;
import acap.fmc.procedure.Instruction;
import acap.fmc.procedure.fsm.StateMachine;
import acap.fmc.routing.AirportGraph;
import acap.fmc.routing.Waypoint;
import acap.fmc.sensors.AircraftSensors;
import acap.fmc.sensors.EngineSensors;
import acap.world.utilities.DrawingUtils;
import acap.world.utilities.NavDB;
import acap.world.model.Aircraft;
import java.util.ArrayList;
import java.util.List;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import processing.core.PShape;

public class UserAircraft extends Aircraft {
  private static UserAircraft instance;
  private final AircraftSensors aircraftSensors = new AircraftSensors();
  private final EngineSensors engineSensors = new EngineSensors();
  private final Autoflight autoflight = new Autoflight();

  //new parameters
  String equipmentCode = "/G";
  int grossWeight = 2150;
  int emptyWeight = 1290;
  int minLanding = 1080;
  int minTakeoff = 1700;
  int maxHeadwindComponent = 50;
  int maxCrosswindComponent = 50;
  float angleOfAttack = 12.4f;
  float comFrequency = 120.50f;
  float navFrequency = 110.90f;
  float lateralForce = 12.0f;
  float radarAltimeter = 65.2f;
  boolean flightDirector = true;
  float criticalAOA = 15.0f;
  float barometerSetting = 29.92f;
  float localizerDeflection = -5; //percentage
  float glideslopeDeflection = 30; //percentage
  float track = 0.0f;
  int serviceCeiling = 14900;
  int oxygenAltitude = 12500;
  float speedTrend = 10;
  float rotationSpeed = 60;
  float flaps1speed = 97;
  float flaps2speed = 88;
  float flaps3speed = 79;
  float landingSpeed = 58;
  float vref = 1.3f*landingSpeed;
  float maximumManeuveringSpeed = 102;
  float minimumManeuveringSpeed = 61;
  float stallSpeed = 54;
  //V-Speeds in kts
  int Vy = 0;
  int Vx = 0;
  int Vne = 0;
  int Vg = 0;
  int Vno = 0;
  //derived variables
  float groundspeed = 0;
  //switches
  boolean rightSquatSwitch = false;
  boolean leftSquatSwitch = false;
  boolean noseSquatSwitch = false;
  ArrayList<Instruction> instructions = new ArrayList<Instruction>();
  ArrayList<Waypoint> taxiIn = new ArrayList<>();
  private int verticalSpeed;
  private float maneuveringSpeed;
  private String altimeterSetting;
  private boolean flapsInTransit = false;
  private int rollStatus = 2;
  private int pitchStatus = 1;
  private int yawStatus = 1;
  private int rudderTrimStatus = 0;
  private int pitchTrimStatus = 2;
  private int flapsStatus = 2;
  private int brakesStatus = 2;
  private boolean staticStatus = true;
  private boolean pitotStatus = true;
  private boolean tachStatus = true;
  private boolean gpsStatus = true;
  private boolean gyroStatus = true;
  private boolean lidarStatus = false;
  private boolean cameraStatus = true;
  private boolean temperatureStatus = true;
  private boolean barometerStatus = true;
  //Routes
  private ArrayList<Waypoint> route = new ArrayList<>();
  private ArrayList<Waypoint> taxiOut = new ArrayList<>();
  private double gyroRate_Yaw = 0;
  private double gyroRate_Pitch = 0;
  private double gyroRate_Roll = 0;
  private boolean taxiClearanceStatus;
  private int cruiseAlt;
  private boolean holdingShort;
  private AirportGraph originGraph = null;
  private Waypoint topOfDescent;
  private Waypoint initialApproach;
  private Waypoint finalApproach;
  private Waypoint taxiExitNode;
  private Waypoint destinationParkingSpot;
  private Waypoint expectedTouchdown;


  private UserAircraft() {

    this.friendlyName = "Piper PA-28 Cherokee 140 \"Flite Liner\"";
    this.type = "PA28";
    this.cruiseSpeed = 105;
    this.maxAltitude = 11000;
    this.minRunway = 535; //the minimum groundroll
    this.fuelCap = 50; //gal
    this.fuel = fuelCap; //top it off
    this.wingspan = 30;
    this.lastUpdate = millis();
    this.fuelType = false;
    this.cruisegph = 7.5f;
    this.cargo = 800;
    this.origin = NavDB.getInstance().getAirport(PLANE_START);
    this.originGraph = new AirportGraph(PLANE_START);
    this.tailNumber = "N570FL";

    if(drawer == null) {
      PShape userAircraftModel = drawer.loadShape(Constants.USER_AIRCRAFT_MODEL);
      triShape = DrawingUtils.createShapeTri(userAircraftModel);
    }

    originParking = this.origin.getNearestParking(lat, lon);
    if (originParking != null) {
      lon = originParking.getLon();
      lat = originParking.getLat();
    } else {
      lon = origin.getLon();
      lat = origin.getLat();
    }
    altitude = this.origin.alt;
    heading = 30;
    this.range = ((fuel / gph) - reserve) * speed;
    this.optimalRange =
        ((range * range) / 5500); //5500 = range of a fully loaded B777 (serves as a maximum)
  }

  public static synchronized UserAircraft getInstance() {
    if (instance == null) instance = new UserAircraft();
    return instance;
  }

  public float getAngleOfAttack() {
    return angleOfAttack;
  }

  public void setAngleOfAttack(float angleOfAttack) {
    this.angleOfAttack = angleOfAttack;
  }

  public float getComFrequency() {
    return comFrequency;
  }

  public void setComFrequency(float comFrequency) {
    this.comFrequency = comFrequency;
  }

  public float getNavFrequency() {
    return navFrequency;
  }

  public void setNavFrequency(float navFrequency) {
    this.navFrequency = navFrequency;
  }

  public float getLateralForce() {
    return lateralForce;
  }

  public void setLateralForce(float lateralForce) {
    this.lateralForce = lateralForce;
  }

  public float getRadarAltimeter() {
    return radarAltimeter;
  }

  public void setRadarAltimeter(float radarAltimeter) {
    this.radarAltimeter = radarAltimeter;
  }

  public boolean isFlightDirector() {
    return flightDirector;
  }

  public void setFlightDirector(boolean flightDirector) {
    this.flightDirector = flightDirector;
  }

  public float getCriticalAOA() {
    return criticalAOA;
  }

  public void setCriticalAOA(float criticalAOA) {
    this.criticalAOA = criticalAOA;
  }

  public float getBarometerSetting() {
    return barometerSetting;
  }

  public void setBarometerSetting(float barometerSetting) {
    this.barometerSetting = barometerSetting;
  }

  public float getLocalizerDeflection() {
    return localizerDeflection;
  }

  public void setLocalizerDeflection(float localizerDeflection) {
    this.localizerDeflection = localizerDeflection;
  }

  public float getGlideslopeDeflection() {
    return glideslopeDeflection;
  }

  public void setGlideslopeDeflection(float glideslopeDeflection) {
    this.glideslopeDeflection = glideslopeDeflection;
  }

  public float getTrack() {
    return track;
  }

  public void setTrack(float track) {
    this.track = track;
  }

  public int getServiceCeiling() {
    return serviceCeiling;
  }

  public void setServiceCeiling(int serviceCeiling) {
    this.serviceCeiling = serviceCeiling;
  }

  public int getOxygenAltitude() {
    return oxygenAltitude;
  }

  public void setOxygenAltitude(int oxygenAltitude) {
    this.oxygenAltitude = oxygenAltitude;
  }

  public float getSpeedTrend() {
    return speedTrend;
  }

  public void setSpeedTrend(float speedTrend) {
    this.speedTrend = speedTrend;
  }

  public float getRotationSpeed() {
    return rotationSpeed;
  }

  public void setRotationSpeed(float rotationSpeed) {
    this.rotationSpeed = rotationSpeed;
  }

  public float getFlaps1speed() {
    return flaps1speed;
  }

  public void setFlaps1speed(float flaps1speed) {
    this.flaps1speed = flaps1speed;
  }

  public float getFlaps2speed() {
    return flaps2speed;
  }

  public void setFlaps2speed(float flaps2speed) {
    this.flaps2speed = flaps2speed;
  }

  public float getFlaps3speed() {
    return flaps3speed;
  }

  public void setFlaps3speed(float flaps3speed) {
    this.flaps3speed = flaps3speed;
  }

  public float getLandingSpeed() {
    return landingSpeed;
  }

  public void setLandingSpeed(float landingSpeed) {
    this.landingSpeed = landingSpeed;
  }

  public float getVref() {
    return vref;
  }

  public void setVref(float vref) {
    this.vref = vref;
  }

  public float getMaximumManeuveringSpeed() {
    return maximumManeuveringSpeed;
  }

  public void setMaximumManeuveringSpeed(float maximumManeuveringSpeed) {
    this.maximumManeuveringSpeed = maximumManeuveringSpeed;
  }

  public float getMinimumManeuveringSpeed() {
    return minimumManeuveringSpeed;
  }

  public void setMinimumManeuveringSpeed(float minimumManeuveringSpeed) {
    this.minimumManeuveringSpeed = minimumManeuveringSpeed;
  }

  public float getStallSpeed() {
    return stallSpeed;
  }

  public void setStallSpeed(float stallSpeed) {
    this.stallSpeed = stallSpeed;
  }

  public Autoflight getAutoFlightSettings() {
    return autoflight;
  }

  public boolean isFlapsInTransit() {
    return flapsInTransit;
  }

  public void setFlapsInTransit(boolean flapsInTransit) {
    this.flapsInTransit = flapsInTransit;
  }

  public int getRollStatus() {
    return rollStatus;
  }

  public void setRollStatus(int rollStatus) {
    this.rollStatus = rollStatus;
  }

  public int getPitchStatus() {
    return pitchStatus;
  }

  public void setPitchStatus(int pitchStatus) {
    this.pitchStatus = pitchStatus;
  }

  public int getYawStatus() {
    return yawStatus;
  }

  public void setYawStatus(int yawStatus) {
    this.yawStatus = yawStatus;
  }

  public int getRudderTrimStatus() {
    return rudderTrimStatus;
  }

  public void setRudderTrimStatus(int rudderTrimStatus) {
    this.rudderTrimStatus = rudderTrimStatus;
  }

  public int getPitchTrimStatus() {
    return pitchTrimStatus;
  }

  public void setPitchTrimStatus(int pitchTrimStatus) {
    this.pitchTrimStatus = pitchTrimStatus;
  }

  public int getFlapsStatus() {
    return flapsStatus;
  }

  public void setFlapsStatus(int flapsStatus) {
    this.flapsStatus = flapsStatus;
  }

  public int getBrakesStatus() {
    return brakesStatus;
  }

  public void setBrakesStatus(int brakesStatus) {
    this.brakesStatus = brakesStatus;
  }

  public boolean isStaticStatus() {
    return staticStatus;
  }

  public void setStaticStatus(boolean staticStatus) {
    this.staticStatus = staticStatus;
  }

  public boolean isPitotStatus() {
    return pitotStatus;
  }

  public void setPitotStatus(boolean pitotStatus) {
    this.pitotStatus = pitotStatus;
  }

  public boolean isTachStatus() {
    return tachStatus;
  }

  public void setTachStatus(boolean tachStatus) {
    this.tachStatus = tachStatus;
  }

  public boolean isGpsStatus() {
    return gpsStatus;
  }

  public void setGpsStatus(boolean gpsStatus) {
    this.gpsStatus = gpsStatus;
  }

  public boolean isGyroStatus() {
    return gyroStatus;
  }

  public void setGyroStatus(boolean gyroStatus) {
    this.gyroStatus = gyroStatus;
  }

  public boolean isLidarStatus() {
    return lidarStatus;
  }

  public void setLidarStatus(boolean lidarStatus) {
    this.lidarStatus = lidarStatus;
  }

  public boolean isCameraStatus() {
    return cameraStatus;
  }

  public void setCameraStatus(boolean cameraStatus) {
    this.cameraStatus = cameraStatus;
  }

  public boolean isTemperatureStatus() {
    return temperatureStatus;
  }

  public void setTemperatureStatus(boolean temperatureStatus) {
    this.temperatureStatus = temperatureStatus;
  }

  public boolean isBarometerStatus() {
    return barometerStatus;
  }

  public void setBarometerStatus(boolean barometerStatus) {
    this.barometerStatus = barometerStatus;
  }

  public int getVy() {
    return Vy;
  }

  public void setVy(int vy) {
    Vy = vy;
  }

  public int getVx() {
    return Vx;
  }

  public void setVx(int vx) {
    Vx = vx;
  }

  public int getVne() {
    return Vne;
  }

  public void setVne(int vne) {
    Vne = vne;
  }

  public int getVg() {
    return Vg;
  }

  public void setVg(int vg) {
    Vg = vg;
  }

  public int getVno() {
    return Vno;
  }

  public void setVno(int vno) {
    Vno = vno;
  }

  public float getGroundspeed() {
    return groundspeed;
  }

  public void setGroundspeed(float groundspeed) {
    this.groundspeed = groundspeed;
  }

  public float getAirspeedSensor() {
    return aircraftSensors.getAirspeedSensor();
  }

  public void setAirspeedSensor(float airspeedSensor) {
    this.aircraftSensors.setAirspeedSensor(airspeedSensor);
  }

  public float getFlapSensor() {
    return (float) Devices.getInstance().getControllerReading(component.FLAPS);
  }

  public int getPhaseOfFlight() {
    return StateMachine.getInstance().getCurrentPhaseNumber().ordinal();
  }

  public float getDestinationAltitude() {
    if (this.destination != null) {
      return this.destination.getAlt();
    } else {
      return 1000;
    }
  }

  public Waypoint getExpectedTouchdown() {
    return expectedTouchdown;
  }

  public void setExpectedTouchdown(Waypoint expectedTouchdown) {
    this.expectedTouchdown = expectedTouchdown;
  }

  public Waypoint getTopOfDescent(){
    return this.topOfDescent;
  }

  public void setTopOfDescent(Waypoint topOfDescent) {
    this.topOfDescent = topOfDescent;
  }

  public Waypoint getInitialApproach(){
    return this.initialApproach;
  }

  public void setInitialApproach(Waypoint initialApproach) {
    this.initialApproach = initialApproach;
  }

  public void updateRoute(String newRoute) {

    //convert string to route
    //setroute

  }

  public void updateTaxiOut(String taxiout) {
    ArrayList<Waypoint> taxiRoute = new ArrayList<>();
    Node target;
    String runway;
    Path p;
    AirportGraph origin = new AirportGraph(this.origin.id());

    Node starting = origin.getNodeNearLonLat(lon, lat);

    if (taxiout.contains("!")) {
      runway = taxiout.substring(0, taxiout.indexOf('!'));
      target = origin.getRunwayByHeading(runway)[0];
      p = origin.getShortestPathToNode(starting, target);
    } else {
      p = origin.parsePathFromString(taxiout, starting);
    }

    p.getNodeSet()
        .forEach(
            e -> {
              taxiRoute.add(new Waypoint(e, NavDB.getInstance().getAirport(this.origin.id()).getAlt() + 5));
            });
    this.taxiOut = taxiRoute;
  }

  public void updateTaxiIn(String newTaxiIn) {

    //convert string to route
    //setroute

  }

  public ArrayList<Instruction> getInstructions() {
    return instructions;
  }

  public void addInstructions(Instruction i) {
    instructions.add(i);
  }

//  public float getHeading() {
//    return (float) gyroRate_Yaw;
//  }

  public double getGyroRate_Yaw() {
    return gyroRate_Yaw;
  }

  public void setGyroRate_Yaw(double gyroRate_Yaw) {
    this.gyroRate_Yaw = gyroRate_Yaw;
  }

  public double getGyroRate_Pitch() {
    return gyroRate_Pitch;
  }

  public void setGyroRate_Pitch(double gyroRate_Pitch) {
    this.gyroRate_Pitch = gyroRate_Pitch;
  }

  public double getGyroRate_Roll() {
    return gyroRate_Roll;
  }

  public void setGyroRate_Roll(double gyroRate_Roll) {
    this.gyroRate_Roll = gyroRate_Roll;
  }

  public int getMaxHeadwindComponent() {
    return maxHeadwindComponent;
  }

  public void setMaxHeadwindComponent(int maxHeadwindComponent) {
    this.maxHeadwindComponent = maxHeadwindComponent;
  }

  public int getMaxCrosswindComponent() {
    return maxCrosswindComponent;
  }

  public void setMaxCrosswindComponent(int maxCrosswindComponent) {
    this.maxCrosswindComponent = maxCrosswindComponent;
  }

  public double getInertialRate_X() {
    return aircraftSensors.getInertialRate_X();
  }

  public void setInertialRate_X(double inertialRate_X) {
    this.aircraftSensors.setInertialRate_X(inertialRate_X);
  }

  public double getInertialRate_Y() {
    return aircraftSensors.getInertialRate_Y();
  }

  public void setInertialRate_Y(double inertialRate_Y) {
    this.aircraftSensors.setInertialRate_Y(inertialRate_Y);
  }

  public double getInertialRate_Z() {
    return aircraftSensors.getInertialRate_Z();
  }

  public void setInertialRate_Z(double inertialRate_Z) {
    this.aircraftSensors.setInertialRate_Z(inertialRate_Z);
  }

  public ArrayList<Waypoint> getRoute() {
    return route;
  }

  public void addToRoute(Waypoint p) {
    if(this.route.isEmpty())
      this.route.add(p);
    else{
      List<Waypoint> between = this.route.get(this.route.size()-1).getPointsTo(p);
      this.route.addAll(between);
      this.route.add(p);
    }
  }

  public void addToRoute(List<Waypoint> p) {
    for(Waypoint adding : p){
      this.addToRoute(adding);
    }
  }

  public void clearRoute() {
    this.route = new ArrayList<>();
  }

  public int getMinLanding() {
    return minLanding;
  }

  public boolean isRightSquatSwitch() {
    return rightSquatSwitch;
  }

  public void setRightSquatSwitch(boolean rightSquatSwitch) {
    this.rightSquatSwitch = rightSquatSwitch;
  }

  public boolean isLeftSquatSwitch() {
    return leftSquatSwitch;
  }

  public void setLeftSquatSwitch(boolean leftSquatSwitch) {
    this.leftSquatSwitch = leftSquatSwitch;
  }

  public boolean isNoseSquatSwitch() {
    return noseSquatSwitch;
  }

  public void setNoseSquatSwitch(boolean noseSquatSwitch) {
    this.noseSquatSwitch = noseSquatSwitch;
  }

  public int getRudderSensePosition() {
    return (int) Devices.getInstance().getControllerReading(component.RUDDER);
  }

  public int getRudderSetPosition() {
    return (int) Devices.getInstance().getLastControllerValueSet(component.RUDDER);
  }

  public int getAileronSensePosition() {
    return (int) Devices.getInstance().getControllerReading(component.AILERON);
  }

  public int getAileronSetPosition() {
    return (int) Devices.getInstance().getLastControllerValueSet(component.AILERON);
  }

  public boolean getTaxiClearanceStatus() {
    return taxiClearanceStatus;
  }
  
  public void setTaxiClearanceStatus(boolean status){
    this.taxiClearanceStatus = status;
  }

  public void setCruiseAlt(int cruiseAlt) {
    this.cruiseAlt = cruiseAlt;
  }

  public double getCruiseAltitude() {
    return this.cruiseAlt;
  }

  public void reachedHoldShort() {
    this.holdingShort = true;
  }

  public boolean isHoldingShort(){
    return new Waypoint(this.originGraph.getNodeNearLonLat(lon,lat), this.origin.getAlt() + 5).isHoldshort();
  }

  public Waypoint getFinalApproach(){
    return this.finalApproach;
  }

  public void setFinalApproach(Waypoint finalApproach) {
    this.finalApproach = finalApproach;
  }

  public Waypoint getTaxiExitNode() {
    return this.taxiExitNode;
  }

  public void setTaxiExitNode(Waypoint taxiExitNode) {
    this.taxiExitNode = taxiExitNode;
  }

  public void setDestionationParkingSpot(Waypoint p) {
    this.destinationParkingSpot = p;
  }

  public Waypoint getDestinationParkingSpot() {
    return this.destinationParkingSpot;
  }

  public Waypoint getLastWaypointofRoute(){
    return this.route.get(this.route.size() - 1);
  }

  public float getFuelPressureSensor() {
    return aircraftSensors.getFuelPressureSensor();
  }

  public void setFuelPressureSensor(float fuelPressureSensor) {
    this.aircraftSensors.setFuelPressureSensor(fuelPressureSensor);
  }

  public float getOilPressureSensor() {
    return aircraftSensors.getOilPressureSensor();
  }

  public void setOilPressureSensor(float oilPressureSensor) {
    this.aircraftSensors.setOilPressureSensor(oilPressureSensor);
  }

  public int getOilTemperatureSensor() {
    return aircraftSensors.getOilTemperatureSensor();
  }

  public void setOilTemperatureSensor(int oilTemperatureSensor) {
    this.aircraftSensors.setOilTemperatureSensor(oilTemperatureSensor);
  }

  public float getLeftFuelSensor() {
    return aircraftSensors.getLeftFuelSensor();
  }

  public void setLeftFuelSensor(float leftFuelSensor) {
    this.aircraftSensors.setLeftFuelSensor(leftFuelSensor);
  }

  public float getRightFuelSensor() {
    return aircraftSensors.getRightFuelSensor();
  }

  public void setRightFuelSensor(float rightFuelSensor) {
    this.aircraftSensors.setRightFuelSensor(rightFuelSensor);
  }

  public int getRpm() {
    return engineSensors.getRpm();
  }

  public int getPowerSetting() {
    return engineSensors.getPowerSetting();
  }

  public float getFuelFlow() {
    return engineSensors.getFuelFlow();
  }

  public int getExaustGasTempurature() {
    return engineSensors.getExaustGasTempurature();
  }

  public int getCylinderHeadTempurature() {
    return engineSensors.getCylinderHeadTempurature();
  }

  public float getHobbsTime() {
    return engineSensors.getHobbsTime();
  }

  public float getTachTime() {
    return engineSensors.getTachTime();
  }

  public boolean isCarbHeat() {
    return engineSensors.isCarbHeat();
  }

  public void setCarbHeat(boolean carbHeat) {
    this.engineSensors.setCarbHeat(carbHeat);
  }

  public int getIcingPowerLoss() {
    return engineSensors.getIcingPowerLoss();
  }

  public int getCarbIceBuildUp() {
    return engineSensors.getCarbIceBuildUp();
  }

  public boolean isStarter() {
    return engineSensors.isStarter();
  }

  public void setStarter(boolean starter) {
    this.engineSensors.setStarter(starter);
  }

  public boolean isLeftMagneto() {
    return engineSensors.isLeftMagneto();
  }

  public void setLeftMagneto(boolean leftMagneto) {
    this.engineSensors.setLeftMagneto(leftMagneto);
  }

  public boolean isRightMagneto() {
    return engineSensors.isRightMagneto();
  }

  public void setRightMagneto(boolean rightMagneto) {
    this.engineSensors.setRightMagneto(rightMagneto);
  }

  public boolean isStarterSafetyKey() {
    return engineSensors.isStarterSafetyKey();
  }

  public boolean isFuelPump() {
    return engineSensors.isFuelPump();
  }

  public int getFuelSelector() {
    return engineSensors.getFuelSelector();
  }

  public void setFuelSelector(int fuelSelector) {
    this.engineSensors.setFuelSelector(fuelSelector);
  }

  public int getMixtureSetting() {
    return engineSensors.getMixtureSetting();
  }

  public int getMixturePower() {
    return engineSensors.getMixturePower();
  }

  public int getMixtureEconomy() {
    return engineSensors.getMixtureEconomy();
  }

  public int getMixtureTaxi() {
    return engineSensors.getMixtureTaxi();
  }

  public int getMixtureStop() {
    return engineSensors.getMixtureStop();
  }

  public int getReserveMinutes() {
    return engineSensors.getReserveMinutes();
  }

  public float getFuelAtStart() {
    return engineSensors.getFuelAtStart();
  }

  public float getEndurance() {
    return (UserAircraft.getInstance().getLeftFuelSensor() + UserAircraft.getInstance().getRightFuelSensor())
        / engineSensors.getFuelFlow();
  }

  public float getFuelWeight() {
    return (UserAircraft.getInstance().getLeftFuelSensor() + UserAircraft.getInstance().getRightFuelSensor())
        * 6.01f;
  }

  public boolean isLowFuelEmergency() {
    return engineSensors.isLowFuelEmergency();
  }

  public int getFlapSetPosition() {
    return (int) Devices.getInstance().getLastControllerValueSet(component.FLAPS);
  }

  public float getStabilatorSensePosition() {
    return (float) Devices.getInstance().getControllerReading(component.STABILIATOR);
  }

  public float getStabilatorSetPosition() {
    return (float) Devices.getInstance().getLastControllerValueSet(component.STABILIATOR);
  }

  public float getStabilatorTrimSensePosition() {
    return (float) Devices.getInstance().getControllerReading(component.STABILATOR_TRIM);
  }

  public float getStabilatorTrimSetPosition() {
    return (float) Devices.getInstance().getLastControllerValueSet(component.STABILATOR_TRIM);
  }

  public int getVerticalSpeed() {
    return verticalSpeed;
  }

  public void setVerticalSpeed(int verticalSpeed) {
    this.verticalSpeed = verticalSpeed;
  }

  public float getBrakeSensePosition() {
    return (float) Devices.getInstance().getControllerReading(component.BRAKES);
  }

  public float getBrakeSetPosition() {
    return (float) Devices.getInstance().getLastControllerValueSet(component.BRAKES);
  }

  public float getRudderTrimSensePosition() {
    return (float) Devices.getInstance().getControllerReading(component.RUDDER_TRIM);
  }

  public float getRudderTrimSetPosition() {
    return (float) Devices.getInstance().getLastControllerValueSet(component.RUDDER_TRIM);
  }

  public float getSafeFlapSetting() {
    return Math.max(Math.min(46-(this.getAirspeedSensor()-75)*1.6f, 46), -1);
  }

  public int getOutsideAirTemp() {
    // TODO : grab data from weather thread
    return 99;
  }

  public int getDensityAltitude() {
    // TODO: grab data from weather thread
    return 1231;
  }

  public String getAltimeterSetting() {
    return altimeterSetting;
  }

  public void setAltimeterSetting(String altimeterSetting) {
    this.altimeterSetting = altimeterSetting;
  }

  public float getManeuveringSpeed() {
    return maneuveringSpeed;
  }

  public void setManeuveringSpeed(float maneuveringSpeed) {
    this.maneuveringSpeed = maneuveringSpeed;
  }
}

