/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc;

/** Created by enadel on 10/8/16. */
public class Constants {

  public static final int MILE = 5280;
  public static final String version = "2.1.1b";
  public static final String COPYRIGHTPROMPT =
      "Copyright 2016 ACAP\nTHE AUTONOMOUS CARGO AIRCRAFT PROJECT (ACAP)\n" + version;
  public static final boolean DEBUG = true;
  public static final String ZMQSUBPROXY = "tcp://127.0.0.1:5555";
  public static final String ZMQPUBPROXY = "tcp://127.0.0.1:5560";
  public static final String USER_AIRCRAFT_MODEL = "assets/Aircraft/cherokee.obj";
  public static final String PLANE_START = "KORH";
  public static final int MAGVAR = 0;
  public static final boolean drawSky = false;
  public static final double KNOTS_TO_FPM = 101.269;
  public static final int ALTITUDE_ABOVE_PATTERN = 1000;
  public static final boolean READ_DATA_FROM_SIM = false;
  public static final boolean ENABLE_JOYSTICK = false;
  public static final double POINTS_BETWEEN_EACH_WAYPOINT = 120.0;

  //view options
  public static float lineOpacity = 255;
  public static float identOpacity = 255;
  public static boolean renderMode = true;
  public static boolean showParkingNames = false;
  public static int taxiwayCenterlineDrawDistance = 5;
}
