package acap.fmc.procedure.aural;


import acap.fmc.UserAircraft;
import acap.utilities.Audio;

/**
 * This Example cue class shows the basic implementation of an audio cue.
 *
 * When the checkCondition method returns true, the audioclip returned is played. The priority of
 * the cue determines what order it will be played if several audio clips are waiting to be played.
 *
 * In this example, the chime audio file is played when the user aircraft reaches cruising altitude
 *
 * NOTE: You do not need to instantiate this cue class anywhere. The AuralHandler will automatically
 * use any class that is in the fmc.procedure.aural package that implements IAudioCue
 */
public class ExampleCue implements IAudioCue{

  @Override
  public boolean checkCondition() {
    return UserAircraft.getInstance().getAltitude() == UserAircraft.getInstance().getCruiseAltitude();
  }

  @Override
  public String audioclip() {
    return Audio.CHIME;
  }

  @Override
  public int getPriority() {
    return 0;
  }
}
