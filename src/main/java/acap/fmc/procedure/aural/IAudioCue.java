package acap.fmc.procedure.aural;

/**
 * THe interface for any audio cue that is processed by the AuralHandler
 */
public interface IAudioCue {

   boolean checkCondition();

   String audioclip();

  int getPriority();

}
