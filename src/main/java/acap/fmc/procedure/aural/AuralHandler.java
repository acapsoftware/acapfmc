package acap.fmc.procedure.aural;

import static acap.ClassLoader.getClasses;

import acap.utilities.Audio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The aural handler class is a thread that runs checks to play specific audio cues. In order
 * for the aural handler to see an audio cue, a class implementing IAudioCue must be added to the
 * acap.fmc.procedure.aural package.
 */
public class AuralHandler implements Runnable {

  private String[] handleClasses;
  private java.lang.ClassLoader myClassLoader;
  private final Logger log = LoggerFactory.getLogger(getClass());

  public AuralHandler() {
    handleClasses = getClasses("acap.fmc.procedure.aural");
    myClassLoader = java.lang.ClassLoader.getSystemClassLoader();
  }

  /**
   * Load the classes found in the aural package and check if the condition is valid for each one. If
   * the checkCondition method returns true for any IAudioCue, play the corresponding audio clip.
   */
  public void run() {
    while (true) {
      for (String cname : handleClasses) {
        try {
          Class myClass = myClassLoader.loadClass(cname);
          IAudioCue audioCueHandler = (IAudioCue) myClass.newInstance();
          if(audioCueHandler.checkCondition()){
            Audio.getInstance().playAudio(audioCueHandler.audioclip(),audioCueHandler.getPriority());
          }

        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException ignored) {
        }
      }
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
