/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure;

import java.util.PriorityQueue;
import java.util.function.Function;
import javax.swing.*;

/** Created by enadel on 11/8/16. */
public class TaskQueue {

  static TaskQueue instance;

  PriorityQueue<TaskGroup> tasks;
  TaskGroup lastAdded;
  Function addCallback; // called when a new task group is added

  public TaskQueue() {
    tasks = new PriorityQueue<>();
  }

  public static TaskQueue getInstance() {
    if (instance == null) {
      instance = new TaskQueue();
    }
    return instance;
  }

  public synchronized void addTaskGroup(TaskGroup tg) {
    this.tasks.add(tg);
    this.lastAdded = tg;

    if (this.addCallback != null) {
      try {
        this.addCallback.apply(tg);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public synchronized void setAddedCallback(Function callback) {
    this.addCallback = callback;
  }

  public synchronized TaskGroup getNextTaskGroup() {
    return tasks.poll();
  }

  public synchronized boolean hasNextTaskGroup() {
    return !tasks.isEmpty();
  }

  public synchronized PriorityQueue<TaskGroup> getTasks() {
    return this.tasks;
  }

  public synchronized TaskGroup getLastAdded() {
    return this.lastAdded;
  }
}
