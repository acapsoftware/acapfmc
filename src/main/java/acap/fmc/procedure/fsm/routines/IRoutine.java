package acap.fmc.procedure.fsm.routines;

/**
 * Routine that is run during the getRelevantState. Any routine that needs to be added
 * must implement this class.
 */
public interface IRoutine extends Runnable {

  String getRelevantState();

}
