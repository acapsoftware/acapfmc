package acap.fmc.procedure.fsm.routines;

import static acap.ClassLoader.getClasses;

import acap.fmc.procedure.aural.IAudioCue;
import acap.fmc.procedure.fsm.StateMachine;
import acap.utilities.Audio;
import java.util.ArrayList;
import java.util.HashMap;
import org.codehaus.groovy.util.HashCodeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that loads routines into their respective mappings. This class takes IRoutines found in the routines
 * package and maps them to the state indicated in getRelevantState(). Each set of routines is executed
 * at the beginning of the associated state.
 */
public class RoutineHandler {

  private String[] handleClasses;
  private java.lang.ClassLoader myClassLoader;
  private final Logger log = LoggerFactory.getLogger(getClass());
  private HashMap<String, ArrayList<IRoutine>> routineMap;

  public RoutineHandler() {
    handleClasses = getClasses("acap.fmc.procedure.fsm.routines");
    myClassLoader = java.lang.ClassLoader.getSystemClassLoader();
    routineMap = new HashMap<>();

    routineMap.put(StateMachine.STARTUP,new ArrayList<>());
    routineMap.put(StateMachine.TAXI,new ArrayList<>());
    routineMap.put(StateMachine.TAKEOFF,new ArrayList<>());
    routineMap.put(StateMachine.INITIAL_CLIMB,new ArrayList<>());
    routineMap.put(StateMachine.CLIMB,new ArrayList<>());
    routineMap.put(StateMachine.CRUISE,new ArrayList<>());
    routineMap.put(StateMachine.DESCENT,new ArrayList<>());
    routineMap.put(StateMachine.INITIAL_APPROACH,new ArrayList<>());
    routineMap.put(StateMachine.FINAL_APPROACH,new ArrayList<>());
    routineMap.put(StateMachine.LANDING,new ArrayList<>());
    routineMap.put(StateMachine.ROLLOUT,new ArrayList<>());
    routineMap.put(StateMachine.TAXIPARK,new ArrayList<>());
    routineMap.put(StateMachine.SHUTDOWN,new ArrayList<>());

  }

  /**
   * Load routines into their respective state
   */
  public void loadRoutines() {
      for (String cname : handleClasses) {
        try {
          Class myClass = myClassLoader.loadClass(cname);
          IRoutine routine = (IRoutine) myClass.newInstance();
          routineMap.get(routine.getRelevantState()).add(routine);
        } catch (ClassCastException | IllegalAccessException | InstantiationException | ClassNotFoundException ignored) {
        }
      }
    }

  /**
   * Retrieve the set of routines for a relevant state
   * @param state
   * @return
   */
  public ArrayList<IRoutine> getRoutines(String state){
    log.info("Loading routines for " + state);
    return routineMap.get(state);
  }
}
