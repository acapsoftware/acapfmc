package acap.fmc.procedure.fsm.routines;

import acap.fmc.procedure.fsm.StateMachine;
import acap.utilities.Audio;

/**
 * Audio startup routine that runs ONCE at the beginning of the startup phase
 */
public class AudioStartup implements IRoutine{

  @Override
  public String getRelevantState() {
    return StateMachine.STARTUP;
  }

  @Override
  public void run() {
    Audio.getInstance().runTestClips();
  }
}
