/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure.fsm;

import acap.fmc.UserAircraft;
import acap.fmc.state.ModeControls;
import org.statefulj.persistence.annotations.State;

/** Created by erikn on 2/20/2017. */
public class Phase {

  /**
   * 0. Startup Taxi Clearance acquired, before taxi checks complete.
   * 1. Taxi Before takeoff check
   * complete, holding short takeoff runway
   * 2. Takeoff 50ft above the ground
   * 3. Initial Climb 1000ft
   * off the ground
   * 4. Climb reach cruising altitude
   * 5. Cruise Reached calculated T/D waypoint
   * 6.
   * Descent Reached initial approach fix, or 3nm on entry to airport
   * 7. Initial Approach Reached
   * final approach fix, or on final leg of pattern
   * 8. Final Approach 200ft above ground
   * 9. Landing Something specific about the squat switches
   * 10. Rollout Reached exit taxi node
   * 11. Taxi Reached
   * parking spot 12. Shutdown
   *
   * @param args
   */

  @State String state;

  private boolean holdshort;

  public String getState() {
    return state;
  }

  public boolean hasTaxiClearance() {
    return UserAircraft.getInstance().getTaxiClearanceStatus();
  }

  public boolean reachedTakeOffHoldShort() {
    return UserAircraft.getInstance().isHoldingShort();
  }

  public boolean initialClimbReached() {
    return UserAircraft.getInstance().getAltitude() > UserAircraft.getInstance().getOrigin().getAlt() + 50;
  }

  public Boolean reachedCruiseAltitude() {

    return UserAircraft.getInstance().getAltitude() >= UserAircraft.getInstance().getCruiseAltitude();

  }

  public Boolean reachedDescentPoint() {
    return false;
  }

  public Boolean onFinal() {
    return UserAircraft.getInstance().distanceTo(UserAircraft.getInstance().getFinalApproach()) < 0.1;
  }

  public Boolean reachedTDWaypoint() {
    return UserAircraft.getInstance().distanceTo(UserAircraft.getInstance().getTopOfDescent()) < 0.1;
  }

  public Boolean reachedInitialApproach() {
    return UserAircraft.getInstance().distanceTo(UserAircraft.getInstance().getInitialApproach()) < 0.1;
  }

  public Boolean isLanding() {
    return UserAircraft.getInstance().getAltitude() <= UserAircraft.getInstance().getDestination().getAlt() + 200;
  }

  public Boolean squatSwitchesOn() {
//    UserAircraft aircraft = UserAircraft.getInstance();
//
//    return aircraft.isLeftSquatSwitch() && aircraft.isRightSquatSwitch()
//        && aircraft.isNoseSquatSwitch();

    return UserAircraft.getInstance().distanceTo(UserAircraft.getInstance().getExpectedTouchdown()) < 0.01;
  }

  public Boolean reachedExitTaxiNode() {
    return UserAircraft.getInstance().distanceTo(UserAircraft.getInstance().getTaxiExitNode()) < 0.01;
  }

  public Boolean reachedParkingSpot() {
    return UserAircraft.getInstance().distanceTo(UserAircraft.getInstance().getDestinationParkingSpot()) < 0.01;
  }
}
