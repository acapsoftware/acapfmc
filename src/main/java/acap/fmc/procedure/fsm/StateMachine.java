/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure.fsm;

import static acap.fmc.gui.windows.DisplaysPane.setDisplayText;
import static acap.fmc.gui.windows.TaskPane.setStateDisplayText;

import acap.fmc.gui.windows.DisplaysPane;
import acap.fmc.gui.windows.TaskPane;
import acap.fmc.procedure.fsm.routines.IRoutine;
import acap.fmc.procedure.fsm.routines.RoutineHandler;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import org.statefulj.fsm.FSM;
import org.statefulj.fsm.TooBusyException;
import org.statefulj.fsm.model.State;
import org.statefulj.fsm.model.impl.StateActionPairImpl;
import org.statefulj.fsm.model.impl.StateImpl;
import org.statefulj.persistence.memory.MemoryPersisterImpl;


/**
 * 0. Startup Taxi Clearance acquired, before taxi checks complete.
 * 1. Taxi Before takeoff check
 * complete, holding short takeoff runway
 * 2. Takeoff 50ft above the ground
 * 3. Initial Climb 1000ft
 * off the ground
 * 4. Climb reach cruising altitude
 * 5. Cruise Reached calculated T/D waypoint
 * 6. Descent Reached initial approach fix, or 3nm on entry to airport
 * 7. Initial Approach Reached final approach fix, or on final leg of pattern
 * 8. Final Approach 200ft above ground
 * 9. Landing Something specific about the squat switches
 * 10. Rollout Reached exit taxi node
 * 11. Taxi Reached parking spot
 * 12. Shutdown
 */
public class StateMachine implements Runnable{

  public static final String CHECK_PHASE_CHANGE = "check";
  private static StateMachine instance;

  public enum PHASE{
    STARTUP,
    TAXI,
    TAKEOFF,
    INITIAL_CLIMB,
    CLIMB,
    CRUISE,
    DESCENT,
    INITIAL_APPROACH,
    FINAL_APPROACH,
    LANDING,
    ROLLOUT,
    TAXIPARK,
    SHUTDOWN
  }

  public static final String STARTUP = "STARTUP";
  public static final String TAXI = "TAXI";
  public static final String TAKEOFF = "TAKEOFF";
  public static final String INITIAL_CLIMB = "INITIAL_CLIMB";
  public static final String CLIMB = "CLIMB";
  public static final String CRUISE = "CRUISE";
  public static final String DESCENT = "DESCENT";
  public static final String INITIAL_APPROACH = "INITIAL_APPROACH";
  public static final String FINAL_APPROACH = "FINAL_APPROACH";
  public static final String LANDING = "LANDING";
  public static final String ROLLOUT = "ROLLOUT";
  public static final String TAXIPARK = "TAXIPARK";
  public static final String SHUTDOWN = "SHUTDOWN";

  private RoutineHandler routineHandler;

  private State<Phase> Startup = new StateImpl<>(STARTUP);
  private State<Phase> Taxi = new StateImpl<>(TAXI);
  private State<Phase> Take_Off = new StateImpl<>(TAKEOFF);
  private State<Phase> Initial_Climb = new StateImpl<>(INITIAL_CLIMB);
  private State<Phase> Climb = new StateImpl<>(CLIMB);
  private State<Phase> Cruise = new StateImpl<>(CRUISE);
  private State<Phase> Descent = new StateImpl<>(DESCENT);
  private State<Phase> Initial_Approach = new StateImpl<>(INITIAL_APPROACH);
  private State<Phase> Final_Approach = new StateImpl<>(FINAL_APPROACH);
  private State<Phase> Landing = new StateImpl<>(LANDING);
  private State<Phase> Rollout = new StateImpl<>(ROLLOUT);
  private State<Phase> Taxi_Parking = new StateImpl<>(TAXIPARK);
  private State<Phase> Shutdown = new StateImpl<>(SHUTDOWN, true);

  private FSM<Phase> fsm;
  private MemoryPersisterImpl<Phase> persister;

  Phase p;

  public static synchronized StateMachine getInstance() {
    if (instance == null) instance = new StateMachine();
    return instance;
  }

  private StateMachine() {
    setupTransitions();

    routineHandler = new RoutineHandler();
    routineHandler.loadRoutines();

    List<State<Phase>> states = generateStateList();

    persister = new MemoryPersisterImpl<>(states, Startup);

    fsm = new FSM<>("AircraftFSM", persister);
    p = new Phase();

    for(IRoutine r :routineHandler.getRoutines(STARTUP))
      new Thread(r).start();
  }

  private List<State<Phase>> generateStateList() {
    List<State<Phase>> states = new LinkedList<>();
    states.add(Startup);
    states.add(Taxi);
    states.add(Take_Off);
    states.add(Initial_Climb);
    states.add(Climb);
    states.add(Cruise);
    states.add(Descent);
    states.add(Initial_Approach);
    states.add(Final_Approach);
    states.add(Landing);
    states.add(Rollout);
    states.add(Taxi_Parking);
    states.add(Shutdown);
    return states;
  }

  public String getCurrentPhase() {
    return fsm.getCurrentState(p).getName();
  }

  public PHASE getCurrentPhaseNumber() {
    return PHASE.valueOf(fsm.getCurrentState(p).getName());
  }

  private void setupTransitions() {
    setTransitionCheck(Startup, Taxi, Phase::hasTaxiClearance);
    setTransitionCheck(Taxi, Take_Off, Phase::reachedTakeOffHoldShort);
    setTransitionCheck(Take_Off, Initial_Climb, Phase::initialClimbReached);
    setTransitionCheck(Initial_Climb, Climb, Phase::hasTaxiClearance);
    setTransitionCheck(Climb, Cruise, Phase::reachedCruiseAltitude);
    setTransitionCheck(Cruise, Descent, Phase::reachedTDWaypoint);
    setTransitionCheck(Descent, Initial_Approach, Phase::reachedInitialApproach);
    setTransitionCheck(Initial_Approach, Final_Approach, Phase::onFinal);
    setTransitionCheck(Final_Approach, Landing, Phase::isLanding);
    setTransitionCheck(Landing, Rollout, Phase::squatSwitchesOn);
    setTransitionCheck(Rollout, Taxi_Parking, Phase::reachedExitTaxiNode);
    setTransitionCheck(Taxi_Parking, Shutdown, Phase::reachedParkingSpot);
  }


  /**
   * Adds the transition check to a relevant state.
   * @param currentState - starting state
   * @param nextState - state to transition to
   * @param checkFunction - boolean function required to make transition occur (what conditions to checK)
   */
  private void setTransitionCheck(
      State<Phase> currentState, State<Phase> nextState, Function<Phase, Boolean> checkFunction) {

    currentState.addTransition(
        CHECK_PHASE_CHANGE,
        ((stateful, event, args1) -> {
          State<Phase> next = null;
          if (checkFunction.apply(stateful)) {
            next = nextState;
            for(IRoutine r :routineHandler.getRoutines(nextState.getName()))
              new Thread(r).start();

          }
          else next = currentState;

          return new StateActionPairImpl<>(next, null);
        }));
  }

  @Override
  public void run() {
    while(true) {
      try {
        fsm.onEvent(p, CHECK_PHASE_CHANGE);
        DisplaysPane.setDisplayText(DisplaysPane.STATE_DISPLAY, getCurrentPhase());
        TaskPane.setStateDisplayText(getCurrentPhase());
        Thread.sleep(100);
      } catch (TooBusyException | InterruptedException e) {
        e.printStackTrace();
      }

    }
  }
}
