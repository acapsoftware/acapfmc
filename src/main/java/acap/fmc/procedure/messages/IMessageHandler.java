package acap.fmc.procedure.messages;

public interface IMessageHandler {

  /**
   * Handle the interpretation of a message received by external services
   * @param b
   */
  void interpret(byte[] b);
}
