package acap.fmc.procedure.messages;

import acap.fmc.procedure.messages.proto.LaneMessageProtocol.LaneMessage;
import com.google.protobuf.InvalidProtocolBufferException;


public class LaneMessageHandler implements IMessageHandler {

  @Override
  public void interpret(byte[] b) {
    // TODO: combine this data with ground navigation (lane offset/orientation)
    try {
      LaneMessage msg = LaneMessage.parseFrom(b);
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
  }
}
