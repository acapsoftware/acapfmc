package acap.fmc.procedure.messages;

import static acap.fmc.Constants.READ_DATA_FROM_SIM;

import acap.fmc.controls.Devices;
import acap.fmc.controls.SerialMessage.ControlMessage;
import acap.fmc.routing.FlightSimInterface;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * Message handler for control messages received by the sensors. This class takes in readings
 * that are SerialMessage types and updates the device map with the updated reading.
 */
public class ControlMessageHandler implements IMessageHandler{

  public void interpret(byte[] b) {

    try {
      ControlMessage msg = ControlMessage.parseFrom(b);
      if (msg.getTargetComponent() == ControlMessage.component.FLIGHT_COMPUTER) updateModel(msg);
      if (msg.getIsSetup()) updateRestrictions(msg);
    } catch (InvalidProtocolBufferException ignored) {
    }

  }

  /**
   * Update min/max position of the source component
   * @param msg
   */
  private void updateRestrictions(ControlMessage msg) {

    Devices.getInstance().setControllerMin(msg.getSourceComponent(), msg.getMinPosition());
    Devices.getInstance().setControllerMin(msg.getSourceComponent(), msg.getMaxPosition());
  }

  /**
   * Update the Device sensor reading in the flight management computer based on the source component
   * @param msg
   */
  public void updateModel(ControlMessage msg) {
    Devices.getInstance().setControllerReading(msg.getSourceComponent(), msg.getPosition());
    if (READ_DATA_FROM_SIM)
      if (FlightSimInterface.isToggleControl(msg.getSourceComponent()))
        Devices.getInstance()
            .getFSInterface(msg.getSourceComponent())
            .setSimControlValue(msg.getPosition() == 1);
      else
        Devices.getInstance()
            .getFSInterface(msg.getSourceComponent())
            .setSimControlValue((int) msg.getPosition());
  }
}
