package acap.fmc.procedure.messages;


import acap.fmc.procedure.messages.proto.AirplaneProto.Airplane;
import acap.world.model.Aircraft;
import acap.world.model.World;
import com.google.protobuf.InvalidProtocolBufferException;

public class TrafficMessageHandler implements IMessageHandler{

  @Override
  public void interpret(byte[] b) {
    try {
      Airplane msg = Airplane.parseFrom(b);
      Aircraft traffic = World.getInstance().getAicraftByFlight(msg.getFlight());
      if(traffic != null){
        traffic.setAltitude(msg.getAltitude());
        traffic.setLat(msg.getLat());
        traffic.setLon(msg.getLat());
        traffic.setSpeed((float) msg.getSpeed());
      }
      else{
        traffic = new Aircraft();
        traffic.setAltitude(msg.getAltitude());
        traffic.setLat(msg.getLat());
        traffic.setLon(msg.getLat());
        traffic.setFriendlyName(msg.getFlight());
        traffic.setSpeed((float) msg.getSpeed());
        World.getInstance().getAircraft().add(traffic);
      }

    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }

  }
}
