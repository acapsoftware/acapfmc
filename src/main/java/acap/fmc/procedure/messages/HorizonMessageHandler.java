package acap.fmc.procedure.messages;


import acap.fmc.UserAircraft;
import acap.fmc.procedure.messages.proto.HorizonMessageProtocol;
import acap.fmc.procedure.messages.proto.HorizonMessageProtocol.HorizonMessage;
import com.google.protobuf.InvalidProtocolBufferException;

public class HorizonMessageHandler implements IMessageHandler{

  @Override
  public void interpret(byte[] b) {
    try {
      // Todo : figure out what to do with OpenCV found pitch/bank (its already from gyro)
      HorizonMessage msg = HorizonMessage.parseFrom(b);
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
  }



}
