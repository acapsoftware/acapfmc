/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure;

import static acap.ClassLoader.getClasses;
import static acap.fmc.Constants.ZMQPUBPROXY;

import acap.fmc.procedure.messages.IMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;
public class SubscribeThread implements Runnable {

  private ZMQ.Socket subscriber;
  private String[] handleClasses;
  private java.lang.ClassLoader myClassLoader;
  private final Logger log = LoggerFactory.getLogger(getClass());

  public SubscribeThread() {
    ZMQ.Context context = ZMQ.context(1);
    subscriber = context.socket(ZMQ.SUB);
    subscriber.connect(ZMQPUBPROXY);
    subscriber.subscribe("".getBytes());

    handleClasses = getClasses("acap.fmc.procedure.messages");
    myClassLoader = java.lang.ClassLoader.getSystemClassLoader();
  }

  @Override
  public void run() {
    log.info("Subscriber thread started...");
    while (true) {
      byte[] b = subscriber.recv(0);
      for (String cname : handleClasses) {
        Class myClass = null;
        try {
          myClass = myClassLoader.loadClass(cname);
          IMessageHandler messageHandle = (IMessageHandler) myClass.newInstance();
          messageHandle.interpret(b);
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException ignored) {

        }
      }

      try {
        Thread.sleep(100);
      } catch (InterruptedException ignored) {

      }
    }
  }
}
