/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure;

import static acap.fmc.Constants.ZMQSUBPROXY;

import acap.fmc.UserAircraft;
import acap.fmc.controls.Devices;
import acap.fmc.controls.SerialMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

public class MsgPublisher {
  ZMQ.Context context;
  ZMQ.Socket pubSocket;
  private static MsgPublisher ourInstance = new MsgPublisher();
  protected final Logger log = LoggerFactory.getLogger(getClass());

  public static MsgPublisher getInstance() {
    return ourInstance;
  }

  private MsgPublisher() {
    context = ZMQ.context(1);
    pubSocket = context.socket(ZMQ.PUB);
    pubSocket.connect(ZMQSUBPROXY);
  }

  public void sendMsg(Message m) {
    if (m != null) {
      try {
        SerialMessage.ControlMessage sm = SerialMessage.ControlMessage.parseFrom(m.toByteArray());
        setLastSentControlValue(sm);
      } catch (InvalidProtocolBufferException e) {
        e.printStackTrace();
      }

      pubSocket.send(m.toByteArray());
      log.info("MESSAGE SENT!");
    }
  }

  private void setLastSentControlValue(SerialMessage.ControlMessage cm) {
    Devices.getInstance().setControlValueRequested(cm.getTargetComponent(),cm.getPosition());
  }
}
