package acap.fmc.procedure;

import acap.fmc.controls.ComponentController;

/**
 * Created by erikn on 3/20/2017.
 */
public class FakeTask extends Task {

  private Runnable toRun;
  private int completePercent = 0;
  boolean complete = false;

  public FakeTask(String title, Runnable r) {
    super(title, null, 1);
    toRun = r;
  }

  public void run(){
    new Thread(toRun).start();
    completePercent = 100;
    complete = true;
  }

  public int percentComplete(){
    return completePercent;
  }

  public boolean isComplete(){
    return complete;
  }

}
