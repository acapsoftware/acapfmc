/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure;

import acap.fmc.routing.Waypoint;
import java.util.ArrayList;

public class Instruction {

  int type = 0;
  String displayName = null;
  ArrayList<Waypoint> waypoints = new ArrayList<Waypoint>();
  ArrayList<String> displayNames = new ArrayList<String>();

  public Instruction(int type, ArrayList waypoints) {

    displayNames.add("Standby");
    displayNames.add("Evaluation");
    displayNames.add("Before Start");
    displayNames.add("Engine Start");
    displayNames.add("Before Taxi");
    displayNames.add("Taxi Out");
    displayNames.add("Systems Check");
    displayNames.add("Line Up & Wait");
    displayNames.add("Takeoff");
    displayNames.add("Climb");
    displayNames.add("Enroute");
    displayNames.add("Descent");
    displayNames.add("Approach");
    displayNames.add("Final");
    displayNames.add("Flare");
    displayNames.add("Rollout");
    displayNames.add("Taxi In");
    displayNames.add("Shutdown");

    this.type = type;
    this.waypoints = waypoints;
    displayName = displayNames.get(type);
  }

  public Instruction(int type) {

    displayNames.add("Standby");
    displayNames.add("Evaluation");
    displayNames.add("Before Start");
    displayNames.add("Engine Start");
    displayNames.add("Before Taxi");
    displayNames.add("Taxi Out");
    displayNames.add("Systems Check");
    displayNames.add("Line Up & Wait");
    displayNames.add("Takeoff");
    displayNames.add("Climb");
    displayNames.add("Enroute");
    displayNames.add("Descent");
    displayNames.add("Approach");
    displayNames.add("Final");
    displayNames.add("Flare");
    displayNames.add("Rollout");
    displayNames.add("Taxi In");
    displayNames.add("Shutdown");

    this.type = type;
    displayName = displayNames.get(type);
  }

  public void execute() {

    switch (type) {
      case 0:
        break;
    }
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public ArrayList<Waypoint> getWaypoints() {
    return waypoints;
  }

  public void setWaypoints(ArrayList<Waypoint> waypoints) {
    this.waypoints = waypoints;
  }

  public ArrayList<String> getDisplayNames() {
    return displayNames;
  }

  public void setDisplayNames(ArrayList<String> displayNames) {
    this.displayNames = displayNames;
  }
}
