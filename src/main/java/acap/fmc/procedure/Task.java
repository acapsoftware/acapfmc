/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure;

import acap.fmc.controls.ComponentController;
import acap.fmc.controls.Devices;
import acap.fmc.controls.SerialMessage.ControlMessage.component;
import com.google.protobuf.Message;
import java.util.Arrays;
import java.util.concurrent.Callable;

/** Created by enadel on 11/8/16. */
public class Task implements Comparable<Task>, Runnable, ITask{

  ComponentController c;
  int priority;
  String title;
  boolean sent;

  public Task(String title, ComponentController c, int priority) {
    this.c = c;
    this.priority = priority;
    this.title = title;
    this.sent = false;
  }

  /**
   * Create a task object from a string that can be added to the FMC task queue
   * @param input
   * @return
   */
  public static Task constructFromString(String input){
    String[] separated = input.split(" ");
    ComponentController c = new ComponentController(Devices.getInstance().getComponentByString(separated[0]));
    c.setPosition(Double.valueOf(separated[1]));
    return new Task("Set " + separated[0] + " to " + separated[1] + " Percent", c, 1);
  }

  public synchronized String getTitle() {
    return this.title;
  }


  @Override
  public int compareTo(Task o) {
    if (o.priority > this.priority) return -1;
    if (o.priority == this.priority) return 0;
    if (o.priority < this.priority) return 1;
    else return 0;
  }

  @Override
  public void run() {
    MsgPublisher.getInstance().sendMsg(this.c.serialize());
    this.sent = true;
  }


  @Override
  public boolean isComplete() {
    return this.sent;
  }


  @Override
  public Object checkStatus() {
    return sent ? 100 : 0;
  }

  @Override
  public int percentComplete() {
    return sent ? 100 : 0;
  }

  @Override
  public String printStatus() {
    return null;
  }
}
