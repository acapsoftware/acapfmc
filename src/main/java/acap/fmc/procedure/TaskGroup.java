/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.procedure;

import java.util.PriorityQueue;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * A queue of tasks that are associated with a common goal.
 */
public class TaskGroup implements Comparable<TaskGroup> {

  LinkedBlockingDeque<Task> tasks;
  String id;
  int priority;
  String title;

  public TaskGroup(String title, int priority) {
    this.tasks = new LinkedBlockingDeque<Task>();
    this.id = UUID.randomUUID().toString();
    this.priority = priority;
    this.title = title;
  }

  public synchronized String getTitle() {
    return this.title;
  }

  /**
   * Add a task to the task group
   * @param t
   */
  public synchronized void addTask(Task t) {
    try {
      tasks.put(t);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  /**
   * Removes the next task inside the task group and returns it
   * @return
   */
  public synchronized Task getNextTask() {
    try {
      return tasks.take();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Checks if the taskgroup has any more tasks
   * @return
   */
  public synchronized boolean hasNextTask() {
    return !tasks.isEmpty();
  }

  /**
   * Retrieve the underlying datastructure of tasks
   * @return
   */
  public synchronized LinkedBlockingDeque<Task> getTasks() {
    return this.tasks;
  }

  protected synchronized String getUUID() {
    return this.id;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    else if (obj == null) return false;
    else if (getClass() != obj.getClass()) return false;
    else if (((TaskGroup) obj).getUUID() == this.getUUID()) return true;
    return false;
  }

  @Override
  public int compareTo(TaskGroup o) {
    if (o.priority > this.priority) return -1;
    if (o.priority == this.priority) return 0;
    if (o.priority < this.priority) return 1;
    else return 0;
  }
}
