/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.hid;

import static acap.fmc.Constants.READ_DATA_FROM_SIM;
import static acap.Main.logger;

import acap.fmc.controls.ComponentController;
import acap.fmc.controls.Devices;
import acap.fmc.controls.SerialMessage.ControlMessage.component;
import acap.fmc.gui.processing.apps.GLWorld;
import acap.fmc.gui.windows.helpers.DebugPaneHelper;
import acap.fmc.procedure.MsgPublisher;
import com.google.protobuf.Message;

import net.java.games.input.*;
import org.apache.logging.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Created by enadel on 11/13/16. */
public class joystickinput implements Runnable {

  /**
   * Joystick Mappings. Left Lever Arm: Throttle Right LEver Arm: Brake HAT-up: Elevator Trim
   * Nose-Down HAT-down: Elevator Trim Nose Up HAT-right: Rudder Trim Nose Right HAT-left: Rudder
   * Trim Nose Left Trigger: Autopilot Disconnect Lighted Red Button: Starter Toggle Center Thumb
   * Button: EMPTY Right Thumb Button: EMPTY T1: Retract Flaps by 1 notch T2: Extend Flaps by 1
   * notch T3: Left Magneto Toggle T4: Right Magneto Toggle T5: Fuel Pump Toggle T6: Landing Light
   * Toggle T7: Rotating Beacon Toggle T8: Navigation Light Toggle
   */
  public static final String AILERON = "x";

  public static final String ELEVATOR = "y";
  public static final String RUDDER = "rz";
  public static final String THROTTLE = "z";
  public static final String BRAKE = "slider";
  public static final String STARTER = "Thumb";
  public static final String RETRACT_FLAPS = "Top 2";
  public static final String EXTEND_FLAPS = "Pinkie";
  public static final String LEFT_MAGNETO = "Base";
  public static final String RIGHT_MAGNETO = "Base 2";
  public static final String FUEL_PUMP = "Base 3";
  public static final String LANDING_LIGHT = "Base 4";
  public static final String ROTATING_BEACON = "Base 5";
  public static final String NAVIGATION_LIGHT = "Base 6";

  protected final Logger log = LoggerFactory.getLogger(getClass());
  //
  //    public static final String AILERON = "X Axis";
  //    public static final String ELEVATOR = "Y Axis";
  //    public static final String RUDDER = "Y Rotation";
  //    public static final String THROTTLE = "X Rotation";
  //    public static final String BRAKE = "Button 9";
  //    public static final String STARTER = "Button 8";
  //    public static final String RETRACT_FLAPS = "Button 4";
  //    public static final String EXTEND_FLAPS = "Button 5";
  //    public static final String LEFT_MAGNETO = "Button 6";
  //    public static final String RIGHT_MAGNETO = "Button 7";
  //    public static final String FUEL_PUMP = "Button 0";
  //    public static final String LANDING_LIGHT = "Button 2";
  //    public static final String ROTATING_BEACON = "Button 3";
  //    public static final String NAVIGATION_LIGHT = "Button 1";

  private static joystickinput instance = new joystickinput();

  public joystickinput() {}

  public static joystickinput getInstance() {
    return instance;
  }

  public static void main(String[] args) {
    Controller[] ca = ControllerEnvironment.getDefaultEnvironment().getControllers();

    for (int i = 0; i < ca.length; i++) {
      /* Get the name of the controller */
      System.out.println(ca[i].getName());
    }

    /* Get this controllers components (buttons and axis) */
    Component[] components = ca[0].getComponents();
    System.out.println("Component Count: " + components.length);
    for (int j = 0; j < components.length; j++) {

      /* Get the components name */
      System.out.println("Component " + j + ": " + components[j].getName());

      System.out.println("    Identifier: " + components[j].getIdentifier().getName());
      System.out.print("    ComponentType: ");
      if (components[j].isRelative()) {
        System.out.print("Relative");
      } else {
        System.out.print("Absolute");
      }
      if (components[j].isAnalog()) {
        System.out.print(" Analog");
      } else {
        System.out.print(" Digital");
      }
    }

    while (true) {
      Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
      if (controllers.length == 0) {
        System.out.println("Found no controllers.");
        System.exit(0);
      }

      Keyboard b = null;
      int controllerNum = 0;

      for (int i = 0; i < controllers.length; i++)
        if (controllers[i].getType() == Controller.Type.KEYBOARD) b = (Keyboard) controllers[i];

      if (b.isKeyDown(net.java.games.input.Component.Identifier.Key.A)) {
        System.out.println("I'm pressed");
      }

      for (int i = 0; i < controllers.length; i++) {
        if (controllers[i].getName().contains("360")) controllerNum = i;
      }

      for (int i = 0; i < controllers.length; i++) {
        controllers[controllerNum].poll();

        EventQueue queue = controllers[controllerNum].getEventQueue();

        Event event = new Event();

        while (queue.getNextEvent(event)) {
          StringBuffer buffer = new StringBuffer(controllers[controllerNum].getName());
          buffer.append(" at ");
          buffer.append(event.getNanos()).append(", ");
          Component comp = event.getComponent();
          buffer.append(comp.getName()).append(" changed to ");
          float value = event.getValue();
          if (comp.isAnalog()) {
            buffer.append(value);
          } else {
            if (value == 1.0f) {
              buffer.append("On");
            } else {
              buffer.append("Off");
            }
          }
          System.out.println(comp.getName());
          System.out.println(buffer.toString());
        }
      }

      try {
        Thread.sleep(20);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  @Override
  public void run() {
    int controllerNum = 0;
    Controller[] controllers;
    try {
      controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
    } catch (UnsatisfiedLinkError e) {
      return;
    }
    if (controllers.length == 0) {
      log.warn("Found no controllers.");
      return;
    }

    for (int i = 0; i < controllers.length; i++) {
      if (controllers[i].getName().equals("Saitek AV8R Joystick")) controllerNum = i;
      if (controllers[i].getName().contains("XBOX 360 For Windows")) controllerNum = i;
    }
    while (true) {
      if (DebugPaneHelper.enableJoystick) {
        controllers[controllerNum].poll();

        EventQueue queue = controllers[controllerNum].getEventQueue();

        Event event = new Event();

        while (queue.getNextEvent(event)) {
          StringBuffer buffer = new StringBuffer();
          Component comp = event.getComponent();

          float value = event.getValue();
          if (comp.isAnalog()) {
            buffer.append(value);
          } else {
            if (value == 1.0f) {
              buffer.append("On");
            } else {
              buffer.append("Off");
            }
          }
          if (comp.getName().equals(AILERON)) {
            //System.out.println("AILERON: " + buffer.toString());
            double mValue = event.getValue() * 100.0;
            ComponentController aCont = new ComponentController(component.AILERON);
            aCont.setPosition(mValue);
            DebugPaneHelper.getSetterMap().get(Devices.AILERON).setValue((int) mValue);
            MsgPublisher.getInstance().sendMsg((Message) aCont.serialize());
          }
          if (comp.getName().equals(ELEVATOR)) {
            //System.out.println("ELEVATOR: " + buffer.toString());
            double mValue = event.getValue() * 100.0;
            ComponentController sCont = new ComponentController(component.STABILIATOR);
            sCont.setPosition(mValue);
            DebugPaneHelper.getSetterMap().get(Devices.STABILATOR).setValue((int) mValue);
            MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
          } else if (comp.getName().equals(RUDDER)) {
            //System.out.println("RUDDER: " + buffer.toString());
            double mValue = event.getValue() * 100.0;
            ComponentController rCont = new ComponentController(component.RUDDER);
            rCont.setPosition(mValue);
            DebugPaneHelper.getSetterMap().get(Devices.RUDDER).setValue((int) mValue);
            MsgPublisher.getInstance().sendMsg((Message) rCont.serialize());

          } else if (comp.getName().equals(THROTTLE)) {
            //System.out.println("THROTTLE: " + buffer.toString());
            float mValue = GLWorld.map((int) (event.getValue() * 100), -100, 100, 100, 0);
            ComponentController tCont = new ComponentController(component.THROTTLE);
            tCont.setPosition(mValue);
            DebugPaneHelper.getSetterMap().get(Devices.THROTTLE).setValue((int) mValue);
            MsgPublisher.getInstance().sendMsg((Message) tCont.serialize());

          } else if (comp.getName().equals(BRAKE)) {
            //System.out.println("BRAKE: " + buffer.toString() );
            ComponentController bCont = new ComponentController(component.BRAKES);
            float mValue = GLWorld.map((int) (event.getValue() * 100), -100, 100, 100, 0);
            if (READ_DATA_FROM_SIM)
              Devices.getInstance()
                  .getFSInterface(component.BRAKES)
                  .setSimControlValue((int) mValue);
            bCont.setPosition(mValue);
            DebugPaneHelper.getSetterMap().get(Devices.BRAKES).setValue((int) mValue);
            MsgPublisher.getInstance().sendMsg((Message) bCont.serialize());
          } else if (comp.getName().equals(STARTER)) {
            //System.out.println("STARTER: " + buffer.toString() );
            int mValue =
                !(Devices.getInstance().getControllerReading(component.STARTER) == 1) ? 1 : 0;
            ComponentController sCont = new ComponentController(component.STARTER);
            sCont.setPosition(mValue);
            DebugPaneHelper.getToggleMap().get(Devices.STARTER).setActive(mValue != 0);
            MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
          } else if (comp.getName().equals(RETRACT_FLAPS)) {
            //System.out.println("FLAPS: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              double mValue = Devices.getInstance().getControllerReading(component.FLAPS) - 1;
              ComponentController sCont = new ComponentController(component.FLAPS);
              sCont.setPosition(mValue);
              DebugPaneHelper.getSetterMap().get(Devices.FLAPS).setValue((int) mValue);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          } else if (comp.getName().equals(EXTEND_FLAPS)) {
            //System.out.println("FLAPS: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              double mValue = Devices.getInstance().getControllerReading(component.FLAPS) + 1;
              ComponentController sCont = new ComponentController(component.FLAPS);
              sCont.setPosition(mValue);
              DebugPaneHelper.getSetterMap().get(Devices.FLAPS).setValue((int) mValue);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          } else if (comp.getName().equals(LEFT_MAGNETO)) {
            //System.out.println("LEFTMAG: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              int mValue =
                  !(Devices.getInstance().getControllerReading(component.LEFT_MAGNETO) == 1)
                      ? 1
                      : 0;
              ComponentController sCont = new ComponentController(component.LEFT_MAGNETO);
              Devices.getInstance().setControllerReading(component.LEFT_MAGNETO, mValue);
              sCont.setPosition(mValue);
              DebugPaneHelper.getToggleMap().get(Devices.MAG_LEFT).setActive(mValue != 0);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          } else if (comp.getName().equals(RIGHT_MAGNETO)) {
            //System.out.println("RIGHTMAG: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              int mValue =
                  !(Devices.getInstance().getControllerReading(component.RIGHT_MAGNETO) == 1)
                      ? 1
                      : 0;
              ComponentController sCont = new ComponentController(component.RIGHT_MAGNETO);
              Devices.getInstance().setControllerReading(component.RIGHT_MAGNETO, mValue);
              sCont.setPosition(mValue);
              DebugPaneHelper.getToggleMap().get(Devices.MAG_RIGHT).setActive(mValue != 0);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          } else if (comp.getName().equals(FUEL_PUMP)) {
            //System.out.println("PUMP: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              int mValue =
                  !(Devices.getInstance().getControllerReading(component.FUEL_PUMP) == 1) ? 1 : 0;
              ComponentController sCont = new ComponentController(component.FUEL_PUMP);
              sCont.setPosition(mValue);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          } else if (comp.getName().equals(LANDING_LIGHT)) {
            //System.out.println("LIGHT: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              int mValue =
                  !(Devices.getInstance().getControllerReading(component.LANDING_LIGHT) == 1)
                      ? 1
                      : 0;
              ComponentController sCont = new ComponentController(component.LANDING_LIGHT);
              sCont.setPosition(mValue);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          } else if (comp.getName().equals(ROTATING_BEACON)) {
            //System.out.println("ROTATINGBEACON: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              int mValue =
                  !(Devices.getInstance().getControllerReading(component.ROTATING_BEACON) == 1)
                      ? 1
                      : 0;
              ComponentController sCont = new ComponentController(component.ROTATING_BEACON);
              sCont.setPosition(mValue);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          } else if (comp.getName().equals(NAVIGATION_LIGHT)) {
            //System.out.println("NAVLIGHT: " + buffer.toString() );
            if (buffer.toString().equals("On")) {
              int mValue =
                  !(Devices.getInstance().getControllerReading(component.NAVIGATION_LIGHT) == 1)
                      ? 1
                      : 0;
              Devices.getInstance().setControllerReading(component.NAVIGATION_LIGHT, mValue);
              ComponentController sCont = new ComponentController(component.NAVIGATION_LIGHT);
              sCont.setPosition(mValue);
              MsgPublisher.getInstance().sendMsg((Message) sCont.serialize());
            }
          }
        }

        try {
          Thread.sleep(20);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
  }
}
