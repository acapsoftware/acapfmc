/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.sensors;

/** Created by erikn on 2/4/2017. */
/*
 * Copyright 2011 Phidgets Inc.  All rights reserved.
 */

import acap.fmc.UserAircraft;
import acap.utilities.KalmanFilter;
import com.phidgets.GPSPhidget;
import com.phidgets.PhidgetException;
import com.phidgets.event.ErrorEvent;
import com.phidgets.event.ErrorListener;
import com.phidgets.event.GPSPositionChangeEvent;
import com.phidgets.event.GPSPositionChangeListener;
import com.phidgets.event.GPSPositionFixStatusChangeEvent;
import com.phidgets.event.GPSPositionFixStatusChangeListener;
import java.io.IOException;

public class GPS {

  public static final int GPS_UPDATE_INTERVAL = 1;
  private static double longitude;
  private static double latitude;
  private static double heading;
  private static double velocity;
  private GPSPhidget gps;

  private static final double EARTH_RADIUS_IN_METERS = 6371009;

  KalmanFilter filter = new KalmanFilter(4, 2);

  public GPS() {
    initKalmanFilter();
    init();
  }

  private void initKalmanFilter() {
    filter.getState_transition().set_identity_matrix();

    set_seconds_per_timestep(filter, 1.0);

    filter.getObservation_model().set_matrix(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0);

    double pos = 0.000001;
    filter
        .getProcess_noise_covariance()
        .set_matrix(pos, 0.0, 0.0, 0.0, 0.0, pos, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0);

    filter.getObservation_noise_covariance().set_matrix(pos * 1.0, 0.0, 0.0, pos * 1.0);

    filter.state_estimate.set_matrix(0.0, 0.0, 0.0, 0.0);
    filter.estimate_covariance.set_identity_matrix();
    filter.estimate_covariance.scale_matrix(1000.0 * 1000.0 * 1000.0 * 1000.0);
  }

  /* Set the seconds per timestep in the velocity2d model. */
  /*
   * The position units are in thousandths of latitude and longitude. The
   * velocity units are in thousandths of position units per second.
   *
   * So if there is one second per timestep, a velocity of 1 will change the
   * lat or long by 1 after a million timesteps.
   *
   * Thus a typical position is hundreds of thousands of units. A typical
   * velocity is maybe ten.
   */
  void set_seconds_per_timestep(KalmanFilter f, double seconds_per_timestep) {
    /*
     * unit_scaler accounts for the relation between position and velocity
     * units
     */
    double unit_scaler = 0.001;
    f.getState_transition().data[0][2] = unit_scaler * seconds_per_timestep;
    f.getState_transition().data[1][3] = unit_scaler * seconds_per_timestep;
  }

  /* Update the velocity2d model with new gps data. */
  void update_velocity2d(
      double lat, double lon, double seconds_since_last_timestep, KalmanFilter f) {
    set_seconds_per_timestep(f, seconds_since_last_timestep);
    f.observation.set_matrix(lat * 1000.0, lon * 1000.0);
    f.update();
  }

  /* Extract a lat long from a velocity2d Kalman filter. */
  double[] get_lat_long(KalmanFilter f) {
    double[] latlon = new double[2];
    latlon[0] = f.state_estimate.data[0][0] / 1000.0;
    latlon[1] = f.state_estimate.data[1][0] / 1000.0;
    return latlon;
  }

  double[] get_velocity(KalmanFilter f) {
    double[] delta_latlon = new double[2];
    delta_latlon[0] = f.state_estimate.data[2][0] / (1000.0 * 1000.0);
    delta_latlon[1] = f.state_estimate.data[3][0] / (1000.0 * 1000.0);
    return delta_latlon;
  }

  /* Extract speed in meters per second from a velocity2d Kalman filter. */
  double get_speed(KalmanFilter f, double altitude) {
    double[] latlon = get_lat_long(f);
    double[] delta_latlon = get_velocity(f);
    /*
     * First, let's calculate a unit-independent measurement - the radii of
     * the earth traveled in each second. (Presumably this will be a very
     * small number.)
     */

    /* Convert to radians */
    latlon[0] = Math.toRadians(latlon[0]);
    latlon[1] = Math.toRadians(latlon[1]);
    delta_latlon[0] = Math.toRadians(delta_latlon[0]);
    delta_latlon[1] = Math.toRadians(delta_latlon[1]);

    /* Haversine formula */
    double lat1 = latlon[0] - delta_latlon[0];
    double sin_half_dlat = Math.sin(delta_latlon[0] / 2.0);
    double sin_half_dlon = Math.sin(delta_latlon[1] / 2.0);
    double a =
        sin_half_dlat * sin_half_dlat
            + Math.cos(lat1) * Math.cos(latlon[0]) * sin_half_dlon * sin_half_dlon;
    double radians_per_second = 2 * Math.atan2(1000.0 * Math.sqrt(a), 1000.0 * Math.sqrt(1.0 - a));

    /* Convert units */
    double meters_per_second = radians_per_second * (EARTH_RADIUS_IN_METERS + altitude);
    return meters_per_second;
  }

  private void init() {
    try {
      gps = new GPSPhidget();

      gps.openAny();
      System.out.println("Waiting for the Phidget GPS to be attached...");
      gps.waitForAttachment();

      gps.addErrorListener(
          new ErrorListener() {

            public void error(ErrorEvent ex) {
              System.out.println("\n--->Error: " + ex.getException());
            }
          });

      gps.addGPSPositionFixStatusChangeListener(
          new GPSPositionFixStatusChangeListener() {

            public void gpsPositionFixStatusChanged(GPSPositionFixStatusChangeEvent g) {
              System.out.println(g.toString());
            }
          });

      gps.addGPSPositionChangeListener(
          new GPSPositionChangeListener() {

            public void gpsPositionChanged(GPSPositionChangeEvent gpspce) {
              try {
                update_velocity2d(
                    gps.getLatitude(), gps.getLongitude(), GPS_UPDATE_INTERVAL, filter);

                longitude = get_lat_long(filter)[1];
                latitude = get_lat_long(filter)[0];

                UserAircraft.getInstance().setLat(latitude);
                UserAircraft.getInstance().setLon(longitude);

                heading = gps.getHeading();
                velocity = gps.getVelocity();

                UserAircraft.getInstance().setHeading((float) heading);
                UserAircraft.getInstance().setSpeed((float) velocity);
              } catch (PhidgetException ex) {
                System.out.println("\n--->Error: " + ex.getDescription());
              }
            }
          });

      System.out.println("Phidget Information");
      System.out.println("====================================");
      System.out.println("Version: " + gps.getDeviceVersion());
      System.out.println("Name: " + gps.getDeviceName());
      System.out.println("Serial #: " + gps.getSerialNumber());

      System.in.read();

    } catch (PhidgetException ex) {
      System.out.println("Exception: " + "Phidget Error: " + ex.getDescription());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
