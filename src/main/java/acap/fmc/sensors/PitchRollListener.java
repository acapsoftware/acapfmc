/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.sensors;

import acap.fmc.UserAircraft;
import acap.utilities.Kalman1D;
import acap.utilities.Matrix3x3;
import acap.utilities.Vector3;
import com.phidgets.PhidgetException;
import com.phidgets.SpatialEventData;
import com.phidgets.SpatialPhidget;
import com.phidgets.event.SpatialDataEvent;
import com.phidgets.event.SpatialDataListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PitchRollListener implements SpatialDataListener {

  public static final double RADIANS_TO_DEGREES = 180.0 / Math.PI;
  private Double[] gyroHeading;
  private Double lastTime;
  private Kalman1D filterX;
  private Kalman1D filterY;
  private Kalman1D filterZ;

  private ArrayList<Double[]> compassBearingFilter;

  private double compassBearing;

  private final char DEGREESYMBOL = '\u00b0';

  public PitchRollListener(
      Double[] gyroHeading, Double lastTime, ArrayList<Double[]> compassBearingFilter) {

    this.gyroHeading = gyroHeading;
    this.lastTime = lastTime;
    this.compassBearingFilter = compassBearingFilter;
    compassBearing = 0.0;
    this.filterX = new Kalman1D(0.59);
    this.filterY = new Kalman1D(0.59);
    this.filterZ = new Kalman1D(0.59);
  }

  public Double getLastTime() {
    return this.lastTime;
  }

  public void data(SpatialDataEvent sde) {

    SpatialPhidget spatial = (SpatialPhidget) sde.getSource();

    try {
      if (spatial.getGyroAxisCount() > 0) {

        calculateGyroHeading(sde.getData());

        this.filterX.addObservation(roundDouble(gyroHeading[0], 3));
        this.filterY.addObservation(roundDouble(gyroHeading[1], 3));
        this.filterZ.addObservation(roundDouble(gyroHeading[2], 3));

        UserAircraft.getInstance().setGyroRate_Pitch(this.filterX.getEstimatedPosition());
        UserAircraft.getInstance().setGyroRate_Roll(this.filterY.getEstimatedPosition());
        UserAircraft.getInstance().setGyroRate_Yaw(this.filterZ.getEstimatedPosition());
      }

    } catch (PhidgetException ex) {
    }

    try {
      //Even when there is a compass chip, sometimes there won't be valid data in the event.
      if ((spatial.getCompassAxisCount() > 0) && (sde.getData()[0].getMagneticField().length > 0)) {
        try {
          calculateCompassBearing(sde.getData());
        } catch (Exception ex) {
        }
      }
    } catch (PhidgetException ex) {
      Logger.getLogger(PitchRollListener.class.getName()).log(Level.SEVERE, null, ex);
    }

    getAccelerometerData(sde.getData());
  }

  private void calculateGyroHeading(SpatialEventData[] data) {
    int i;

    Double time = data[0].getTime() * 1000.0;
    if (lastTime == 0) {
      lastTime = time;
    }

    for (i = 0; i < 3; i++) {
      gyroHeading[i] += data[0].getAngularRate()[i] * (((time) - lastTime) / 1000.0);
    }

    lastTime = time;
  }
  //This finds a magnetic north bearing, correcting for board tilt and roll as measured by the accelerometer
  //This doesn't account for dynamic acceleration - ie accelerations other then gravity will throw off the calculation
  double lastBearing = 0;

  public void getAccelerometerData(SpatialEventData[] spatialData) {
    Vector3 gravity =
        Vector3.Normalize(
            new Vector3(
                spatialData[0].getAcceleration()[0],
                spatialData[0].getAcceleration()[2],
                spatialData[0].getAcceleration()[1]));

    UserAircraft.getInstance().setInertialRate_X(gravity.X);
    UserAircraft.getInstance().setInertialRate_Y(gravity.Y);
    UserAircraft.getInstance().setInertialRate_Z(gravity.Z);
  }

  public void calculateCompassBearing(SpatialEventData[] spatialData) {

    double Xh = 0;
    double Yh = 0;

    //find the tilt of the board with respect to gravity
    Vector3 gravity =
        Vector3.Normalize(
            new Vector3(
                spatialData[0].getAcceleration()[0],
                spatialData[0].getAcceleration()[2],
                spatialData[0].getAcceleration()[1]));

    double pitchAngle = Math.asin(gravity.X);
    double rollAngle = Math.asin(gravity.Z);

    //The board is up-side down
    if (gravity.Y < 0) {
      pitchAngle = -pitchAngle;
      rollAngle = -rollAngle;
    }

    //Construct a rotation matrix for rotating vectors measured in the body frame, into the earth frame
    //this is done by using the angles between the board and the gravity vector.
    Matrix3x3 xRotMatrix = new Matrix3x3();

    xRotMatrix.matrix[0][0] = Math.cos(pitchAngle);
    xRotMatrix.matrix[1][0] = -Math.sin(pitchAngle);
    xRotMatrix.matrix[2][0] = 0;
    xRotMatrix.matrix[0][1] = Math.sin(pitchAngle);
    xRotMatrix.matrix[1][1] = Math.cos(pitchAngle);
    xRotMatrix.matrix[2][1] = 0;
    xRotMatrix.matrix[0][2] = 0;
    xRotMatrix.matrix[1][2] = 0;
    xRotMatrix.matrix[2][2] = 1;

    Matrix3x3 zRotMatrix = new Matrix3x3();
    zRotMatrix.matrix[0][0] = 1;
    zRotMatrix.matrix[1][0] = 0;
    zRotMatrix.matrix[2][0] = 0;
    zRotMatrix.matrix[0][1] = 0;
    zRotMatrix.matrix[1][1] = Math.cos(rollAngle);
    zRotMatrix.matrix[2][1] = -Math.sin(rollAngle);
    zRotMatrix.matrix[0][2] = 0;
    zRotMatrix.matrix[1][2] = Math.sin(rollAngle);
    zRotMatrix.matrix[2][2] = Math.cos(rollAngle);

    Matrix3x3 rotMatrix = Matrix3x3.Multiply(xRotMatrix, zRotMatrix);

    Vector3 data =
        new Vector3(
            spatialData[0].getMagneticField()[0],
            spatialData[0].getMagneticField()[2],
            -spatialData[0].getMagneticField()[1]);

    Vector3 correctedData = Matrix3x3.Multiply(data, rotMatrix);

    //These represent the x and y components of the magnetic field vector in the earth frame
    Xh = -correctedData.Z;
    Yh = -correctedData.X;

    //we use the computed X-Y to find a magnetic North bearing in the earth frame
    try {
      double bearing = 0.0;
      double _360inRads = (360.0 * Math.PI / 180.0);
      if (Xh < 0.0) {

        bearing = Math.PI - Math.atan(Yh / Xh);
      } else if ((Xh > 0.0) && (Yh < 0.0)) {
        bearing = -Math.atan(Yh / Xh);
      } else if ((Xh > 0.0) && (Yh > 0.0)) {
        bearing = Math.PI * 2.0 - (Math.atan(Yh / Xh));
      } else if ((Xh == 0.0) && (Yh < 0.0)) {
        bearing = Math.PI / 2.0;
      } else if ((Xh == 0.0) && (Yh > 0.0)) {
        bearing = Math.PI * 1.5;
      }

      //The board is up-side down
      if (gravity.Y < 0) {
        bearing = Math.abs(bearing - _360inRads);
      }

      //passing the 0 <-> 360 point, need to make sure the filter never contains both values near 0 and values near 360 at the same time.
      if (Math.abs(bearing - lastBearing) > 2) //2 radians == ~115 degrees
      {
        if (bearing > lastBearing) {
          for (Double[] stuff : compassBearingFilter) {
            stuff[0] += _360inRads;
          }
        } else {
          for (Double[] stuff : compassBearingFilter) {
            stuff[0] -= _360inRads;
          }
        }
      }

      Double[] temp = {bearing, pitchAngle, rollAngle};
      int compassBearingFilterSize = 10;
      compassBearingFilter.add(temp);
      if (compassBearingFilter.size() > compassBearingFilterSize) {
        compassBearingFilter.remove(0);
      }

      bearing = 0;
      pitchAngle = 0;
      rollAngle = 0;

      for (Double[] stuff : compassBearingFilter) {
        bearing += stuff[0];
        pitchAngle += stuff[1];
        rollAngle += stuff[2];
      }

      bearing /= compassBearingFilter.size();
      pitchAngle /= compassBearingFilter.size();
      rollAngle /= compassBearingFilter.size();

      compassBearing = bearing * RADIANS_TO_DEGREES;
      lastBearing = bearing;

      UserAircraft.getInstance().setRoll(roundDouble(rollAngle * RADIANS_TO_DEGREES, 1));
      UserAircraft.getInstance().setPitch(roundDouble(pitchAngle * RADIANS_TO_DEGREES, 1));

    } catch (Exception ex) {
    }
  }

  private double roundDouble(Double value, int decimalPlaces) {
    BigDecimal bd = new BigDecimal(value).setScale(decimalPlaces, RoundingMode.HALF_EVEN);
    return (bd.doubleValue());
  }
}
