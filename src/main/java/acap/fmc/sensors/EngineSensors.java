package acap.fmc.sensors;

public class EngineSensors {//Engine
  int rpm = 1453;
  int powerSetting = 35;
  float fuelFlow = 5.1f;
  int exaustGasTempurature = 885;
  int cylinderHeadTempurature = 513;
  float hobbsTime = 8425.15f;
  float tachTime = 1851.35f;
  boolean carbHeat = false;
  int icingPowerLoss = 110;
  int carbIceBuildUp = 5;
  boolean starter = false;
  boolean leftMagneto = false;
  boolean rightMagneto = true;
  boolean starterSafetyKey = true;
  boolean fuelPump = false;
  int fuelSelector = 1;
  int mixtureSetting = 90;
  int mixturePower = 95;
  int mixtureEconomy = 85;
  int mixtureTaxi = 60;
  int mixtureStop = 15;
  int reserveMinutes = 30;
  float fuelAtStart = 35;
  float endurance;
  float fuelWeight;
  boolean lowFuelEmergency = false;

  public EngineSensors() {
  }

  public int getRpm() {
    return rpm;
  }

  public int getPowerSetting() {
    return powerSetting;
  }

  public float getFuelFlow() {
    return fuelFlow;
  }

  public int getExaustGasTempurature() {
    return exaustGasTempurature;
  }

  public int getCylinderHeadTempurature() {
    return cylinderHeadTempurature;
  }

  public float getHobbsTime() {
    return hobbsTime;
  }

  public float getTachTime() {
    return tachTime;
  }

  public boolean isCarbHeat() {
    return carbHeat;
  }

  public void setCarbHeat(boolean carbHeat) {
    this.carbHeat = carbHeat;
  }

  public int getIcingPowerLoss() {
    return icingPowerLoss;
  }

  public int getCarbIceBuildUp() {
    return carbIceBuildUp;
  }

  public boolean isStarter() {
    return starter;
  }

  public void setStarter(boolean starter) {
    this.starter = starter;
  }

  public boolean isLeftMagneto() {
    return leftMagneto;
  }

  public void setLeftMagneto(boolean leftMagneto) {
    this.leftMagneto = leftMagneto;
  }

  public boolean isRightMagneto() {
    return rightMagneto;
  }

  public void setRightMagneto(boolean rightMagneto) {
    this.rightMagneto = rightMagneto;
  }

  public boolean isStarterSafetyKey() {
    return starterSafetyKey;
  }

  public boolean isFuelPump() {
    return fuelPump;
  }

  public int getFuelSelector() {
  return fuelSelector;
}

  public void setFuelSelector(int fuelSelector) {
    this.fuelSelector = fuelSelector;
  }

  public int getMixtureSetting() {
    return mixtureSetting;
  }

  public int getMixturePower() {
    return mixturePower;
  }

  public int getMixtureEconomy() {
    return mixtureEconomy;
  }

  public int getMixtureTaxi() {
    return mixtureTaxi;
  }

  public int getMixtureStop() {
    return mixtureStop;
  }

  public int getReserveMinutes() {
    return reserveMinutes;
  }

  public float getFuelAtStart() {
    return fuelAtStart;
  }

  public boolean isLowFuelEmergency() {
    return lowFuelEmergency;
  }
}