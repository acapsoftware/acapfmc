package acap.fmc.sensors;

public class AircraftSensors {

  float barometricSensor = 0;
  float airspeedSensor = 0;
  double inertialRate_X = 0;
  double inertialRate_Y = 0;
  double inertialRate_Z = 0;
  float bellyLaserSensor = 0;
  float wingtipLaserSensor = 0;
  float ammeterSensor = 0;
  float tachometerSensor = 0;
  float egtSensor = 0;
  float oatSensor = 0;
  int oilTemperatureSensor = 0;
  float fuelPressureSensor = 0;
  float oilPressureSensor = 0;
  float leftFuelSensor = 0;
  float rightFuelSensor = 0;
  float aircraftBatterySensor = 0;
  float avionicsBatterySensor = 0;
  float brakeSensor = 0;

  public AircraftSensors() {
  }

  public float getAirspeedSensor() {
    return airspeedSensor;
  }

  public void setAirspeedSensor(float airspeedSensor) {
    this.airspeedSensor = airspeedSensor;
  }

  public double getInertialRate_X() {
    return inertialRate_X;
  }

  public void setInertialRate_X(double inertialRate_X) {
    this.inertialRate_X = inertialRate_X;
  }

  public double getInertialRate_Y() {
    return inertialRate_Y;
  }

  public void setInertialRate_Y(double inertialRate_Y) {
    this.inertialRate_Y = inertialRate_Y;
  }

  public double getInertialRate_Z() {
    return inertialRate_Z;
  }

  public void setInertialRate_Z(double inertialRate_Z) {
    this.inertialRate_Z = inertialRate_Z;
  }

  public int getOilTemperatureSensor() {
    return oilTemperatureSensor;
  }

  public void setOilTemperatureSensor(int oilTemperatureSensor) {
    this.oilTemperatureSensor = oilTemperatureSensor;
  }

  public float getFuelPressureSensor() {
    return fuelPressureSensor;
  }

  public void setFuelPressureSensor(float fuelPressureSensor) {
    this.fuelPressureSensor = fuelPressureSensor;
  }

  public float getOilPressureSensor() {
    return oilPressureSensor;
  }

  public void setOilPressureSensor(float oilPressureSensor) {
    this.oilPressureSensor = oilPressureSensor;
  }

  public float getLeftFuelSensor() {
    return leftFuelSensor;
  }

  public void setLeftFuelSensor(float leftFuelSensor) {
    this.leftFuelSensor = leftFuelSensor;
  }

  public float getRightFuelSensor() {
    return rightFuelSensor;
  }

  public void setRightFuelSensor(float rightFuelSensor) {
    this.rightFuelSensor = rightFuelSensor;
  }
}