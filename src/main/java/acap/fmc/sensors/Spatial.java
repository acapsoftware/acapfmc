/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.sensors;


import com.phidgets.PhidgetException;
import com.phidgets.SpatialPhidget;
import com.phidgets.event.*;
import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Spatial implements Runnable {

  private static String runArgs[];
  private static SpatialPhidget spatial;

  private static Double[] gyroHeading = {0.0, 0.0, 0.0}; //degrees
  private Double lastTime;
  private ArrayList<Double[]> compassBearingFilter = new ArrayList<Double[]>();

  protected final Logger log = LoggerFactory.getLogger(getClass());

  public Spatial() {}

  public static void resetGyro() {
    try {
      spatial.zeroGyro();
    } catch (PhidgetException e) {
      e.printStackTrace();
    }
    gyroHeading = new Double[] {0.0, 0.0, 0.0};
  }

  private void initialize() {
    try {

      spatial = new SpatialPhidget();

      gyroHeading[0] = 0.0;
      gyroHeading[1] = 0.0;
      gyroHeading[2] = 0.0;
      lastTime = 0.0;

      spatial.addAttachListener(
          ae -> {
            System.out.println("attachment of " + ae);
            try {
              ((SpatialPhidget) ae.getSource()).setDataRate(32); //set data rate to 496ms
            } catch (PhidgetException pe) {
              System.out.println("Problem setting data rate!");
            }
          });

      spatial.addDetachListener(
          ae -> System.out.println("detachment of " + ae));
      spatial.addErrorListener(
          ee -> System.out.println("error event for " + ee));

      SpatialDataListener spatialData_listener =
          new PitchRollListener(this.gyroHeading, this.lastTime, this.compassBearingFilter);

      spatial.addSpatialDataListener(spatialData_listener);
      spatial.openAny();
      System.in.read();

    } catch (PhidgetException ex) {

    } catch (IOException e) {
      e.printStackTrace();
    } catch (ExceptionInInitializerError e) {
      log.warn("Phidget drivers not found.");
    }
  }

  /** @param args the command line arguments */
  public static void main(String args[]) {
    new Spatial().initialize();
  }

  @Override
  public void run() {
    initialize();
  }
}
