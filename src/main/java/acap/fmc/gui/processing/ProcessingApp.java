/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.processing;

import com.jogamp.newt.awt.NewtCanvasAWT;
import com.jogamp.newt.opengl.GLWindow;
import java.awt.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.swing.*;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PSurface;

/** Created by tjnickerson on 1/22/17. */
public abstract class ProcessingApp extends PApplet {

  protected PSurface surface;
  protected NewtCanvasAWT canvas;
  protected float scaling;
  int w, h;
  String renderer;

  public ProcessingApp(String renderer) {
    this(renderer, 1.0f);
  }

  public ProcessingApp(String renderer, float scaling) {
    this.renderer = renderer;
    this.scaling = scaling;
  }

  @Override
  public void size(int width, int height) {
    this.w = width;
    this.h = height;
  }

  protected PSurface getAppletSurface() {
    // Sets global system properties that are (maybe) important
    System.setProperty("sun.awt.noerasebackground", "true");
    System.setProperty("javafx.animation.fullspeed", "true");
    System.setProperty("java.net.useSystemProxies", "true");

    // Ensures components resize at the same time as the windows
    try {
      Toolkit.getDefaultToolkit().setDynamicLayout(true);
    } catch (HeadlessException var21) {
      System.err.println("Cannot run sketch without a display. Read this for possible solutions:");
      System.err.println("https://github.com/processing/processing/wiki/Running-without-a-Display");
      System.exit(1);
    }

    // Initializes the main applet
    this.pixelDensity = this.displayDensity();
    this.displayWidth = getDisplayMode().getWidth();
    this.displayHeight = getDisplayMode().getHeight();
    this.setSize(this.w, this.h);
    this.sketchPath(calcSketchPath());

    // Handle OSX-specific display settings
    if (this.platform == MACOSX) {
      // Initialize the OSX class used for icon handling
      Class think = null;
      try {
        think =
            Thread.currentThread()
                .getContextClassLoader()
                .loadClass("processing.core.ThinkDifferent");
        Method method = think.getMethod("init", new Class[] {PApplet.class});
        method.invoke(null, new Object[] {this});
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    // Extract the PSurface from the applet
    this.surface = this.initSurface();
    this.surface.setSize(this.sketchWidth(), this.sketchHeight());
    this.surface.setLocation(0, 0);
    this.surface.placeWindow(null, null);

    return this.surface;
  }

  public NewtCanvasAWT getCanvas() {
    // Convert the underlying PSurface to an AWT Canvas
    if (this.surface != null) {
      GLWindow win = (GLWindow) surface.getNative();

      this.canvas = new NewtCanvasAWT(win);
      this.canvas.setSize(new Dimension(win.getWidth(), win.getHeight()));

      return this.canvas;
    }

    this.surface = this.getAppletSurface();
    this.surface.startThread();

    return this.getCanvas();
  }

  public JPanel getPanel() {
    NewtCanvasAWT canvas = this.getCanvas();
    JPanel wrapper = new JPanel(new BorderLayout());

    wrapper.setPreferredSize(
        new Dimension(
            (int) (canvas.getWidth() * this.scaling + 10),
            (int) (canvas.getHeight() * this.scaling + 10)));

    wrapper.add(canvas, BorderLayout.CENTER);

    return wrapper;
  }

  @Override
  protected PGraphics makeGraphics(int w, int h, String renderer, String path, boolean primary) {
    String msg;
    try {
      Class e = Thread.currentThread().getContextClassLoader().loadClass(this.renderer);
      Constructor msg1 = e.getConstructor(new Class[0]);
      PGraphics target1 = (PGraphics) msg1.newInstance(new Object[0]);
      target1.setParent(this);
      target1.setPrimary(primary);
      if (path != null) {
        target1.setPath(this.savePath(path));
      }

      target1.setSize(w, h);
      return target1;
    } catch (InvocationTargetException var9) {
      msg = var9.getTargetException().getMessage();
      if (msg != null && msg.indexOf("no jogl in java.library.path") != -1) {
        throw new RuntimeException(
            "The jogl library folder needs to be specified with -Djava.library.path=/path/to/jogl");
      } else {
        this.printStackTrace(var9.getTargetException());
        Throwable target = var9.getTargetException();
        throw new RuntimeException(target.getMessage());
      }
    } catch (Exception var11) {
      if (!(var11 instanceof IllegalArgumentException)
          && !(var11 instanceof NoSuchMethodException)
          && !(var11 instanceof IllegalAccessException)) {
        this.printStackTrace(var11);
        throw new RuntimeException(var11.getMessage());
      } else if (var11.getMessage().contains("cannot be <= 0")) {
        throw new RuntimeException(var11);
      } else {
        this.printStackTrace(var11);
        msg = renderer + " needs to be updated " + "for the current release of Processing.";
        throw new RuntimeException(msg);
      }
    }
  }

  private DisplayMode getDisplayMode() {
    return GraphicsEnvironment.getLocalGraphicsEnvironment()
        .getDefaultScreenDevice()
        .getDisplayMode();
  }

  public void draw() {
    scale(this.scaling);
  }
}
