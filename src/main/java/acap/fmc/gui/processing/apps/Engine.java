package acap.fmc.gui.processing.apps;

import acap.fmc.UserAircraft;
import acap.fmc.gui.processing.ProcessingApp;

public class Engine extends ProcessingApp {

    UserAircraft aircraft = UserAircraft.getInstance();
    public Engine() {
        super(P2D, 0.69f);
        size(960, 825);
    }

    public void draw() {
        super.draw();

        background(0);
        noFill();

        textAlign(CENTER, CENTER);
        textSize(18);
        rectMode(CENTER);
        textLeading(25);
        strokeWeight(1);
        stroke(255);
        strokeCap(SQUARE);
        ellipseMode(CENTER);



        noFill();
        strokeWeight(3);
        textSize(10);
        //tabs
        text("TAB", 745, 465);
        text("TAB", 355, 465);
        textSize(12);


        //fuel
        //ellipse(390, 125, 180, 180);
        strokeWeight(1);
        stroke(255);
        fill(255);
        pushMatrix();
        translate(390, 125);
        for(int i=0; i<8; i++){
            rotate(radians(20));
            line(0, 75, 0, 90);
        }
        rotate(radians(20));
        for(int i=0; i<8; i++){
            rotate(radians(20));
            line(0, 75, 0, 90);
        }
        //resetMatrix();
        //translate(390, 125);
        rotate(radians(130));
        for(int i=0; i<4; i++){
            pushMatrix();
            translate(67,0);
            rotate(radians(-110-i*40));
            text(i*4, 0, 0);
            popMatrix();
            rotate(radians(40));
        }
        //resetMatrix();
        //translate(390, 125);
        rotate(radians(160));
        for(int i=0; i<3; i++){
            pushMatrix();
            translate(67,0);
            rotate(radians(-70+i*60));
            text(i*5, 0, 0);
            popMatrix();
            rotate(radians(-60));
        }
        strokeWeight(4);
        popMatrix();
        pushMatrix();
        //resetMatrix();
        translate(390, 125);
        rotate(radians(20 + aircraft.getFuelFlow() /0.1f));
        line(0, 31, 0, 72);
        popMatrix();
        pushMatrix();
        //resetMatrix();
        translate(390, 125);
        rotate(radians(-20 - aircraft.getFuelPressureSensor()/(0.083333f)));
        line(0, 31, 0, 72);
        popMatrix();

        strokeWeight(3);
        noFill();
        stroke(225,225,0);
        arc(390, 125, 180, 180, radians(110), radians(120));
        arc(390, 125, 180, 180, radians(210), radians(230));
        stroke(0,225,0);
        arc(390, 125, 180, 180, radians(120), radians(210));
        arc(390, 125, 180, 180, radians(330), radians(410));
        stroke(255,0,0);
        arc(390, 125, 180, 180, radians(230), radians(250));
        arc(390, 125, 180, 180, radians(290), radians(330));
        arc(390, 125, 180, 180, radians(410), radians(430));

        //labels
        fill(255);
        text("GPH", 355, 155);
        text("PSI", 425, 155);
        text("FUEL\nFLOW", 305, 35);
        text("FUEL\nPRESS", 475, 35);



        //oil
        //ellipse(620, 125, 180, 180);
        strokeWeight(1);
        stroke(255);
        fill(255);
        pushMatrix();
        translate(620, 125);
        for(int i=0; i<8; i++){
            rotate(radians(20));
            line(0, 75, 0, 90);
        }
        rotate(radians(20));
        for(int i=0; i<8; i++){
            rotate(radians(20));
            line(0, 75, 0, 90);
        }
        //resetMatrix();
        //translate(620, 125);
        rotate(radians(130));
        for(int i=0; i<4; i++){
            pushMatrix();
            translate(62,0);
            rotate(radians(-110-i*40));
            text(i*50+50, 0, 0);
            popMatrix();
            rotate(radians(40));
        }
        //resetMatrix();
        //translate(620, 125);
        rotate(radians(160));
        for(int i=0; i<3; i++){
            pushMatrix();
            translate(62,0);
            rotate(radians(-70+i*60));
            text(i*30+20, 0, 0);
            popMatrix();
            rotate(radians(-60));
        }
        strokeWeight(4);
        popMatrix();
        pushMatrix();
        //resetMatrix();
        translate(620, 125);
        rotate(radians(20 + (aircraft.getOilTemperatureSensor()-50)/1.25f));
        line(0, 31, 0, 72);
        popMatrix();
        pushMatrix();
        //resetMatrix();
        translate(620, 125);
        rotate(radians(-20 - (aircraft.getOilPressureSensor()-20)/0.5f));
        line(0, 31, 0, 72);
        popMatrix();

        strokeWeight(3);
        noFill();
        stroke(225,225,0);
        arc(620, 125, 180, 180, radians(110), radians(120));
        arc(620, 125, 180, 180, radians(350), radians(420));
        stroke(0,225,0);
        arc(620, 125, 180, 180, radians(120), radians(230));
        arc(620, 125, 180, 180, radians(297), radians(350));
        stroke(255,0,0);
        arc(620, 125, 180, 180, radians(230), radians(250));
        arc(620, 125, 180, 180, radians(290), radians(297));
        arc(620, 125, 180, 180, radians(230), radians(250));
        arc(620, 125, 180, 180, radians(420), radians(430));

        //labels
        fill(255);
        text("\u00B0F", 593, 155);
        text("PSI", 655, 155);
        text("OIL\nTEMP", 535, 35);
        text("OIL\nPRESS", 705, 35);



        //temp
        //ellipse(850, 125, 180, 180);
        strokeWeight(1);
        stroke(255);
        fill(255);
        pushMatrix();
        translate(850, 125);
        for(int i=0; i<8; i++){
            rotate(radians(20));
            line(0, 75, 0, 90);
        }
        rotate(radians(20));
        for(int i=0; i<8; i++){
            rotate(radians(20));
            line(0, 75, 0, 90);
        }
        //resetMatrix();
        //translate(850, 125);
        rotate(radians(130));
        for(int i=0; i<4; i++){
            pushMatrix();
            translate(67,0);
            rotate(radians(-110-i*40));
            text(i*2+6, 0, 0);
            popMatrix();
            rotate(radians(40));
        }
        //resetMatrix();
        //translate(850, 125);
        rotate(radians(160));
        for(int i=0; i<4; i++){
            pushMatrix();
            translate(67,0);
            rotate(radians(-70+i*40));
            text(i*2+2, 0, 0);
            popMatrix();
            rotate(radians(-40));
        }
        popMatrix();
        strokeWeight(4);
        //resetMatrix();
        pushMatrix();
        translate(850, 125);
        rotate(radians(20 + (aircraft.getExaustGasTempurature() -600)/5));
        line(0, 31, 0, 72);
        popMatrix();

        pushMatrix();
        //resetMatrix();
        translate(850, 125);
        rotate(radians(-20 - (aircraft.getCylinderHeadTempurature() -200)/5));
        line(0, 31, 0, 72);
        popMatrix();


        strokeWeight(3);
        noFill();
        stroke(225,225,0);
        arc(850, 125, 180, 180, radians(110), radians(120));
        arc(850, 125, 180, 180, radians(210), radians(230));
        arc(850, 125, 180, 180, radians(310), radians(330));
        arc(850, 125, 180, 180, radians(420), radians(430));
        stroke(0,225,0);
        arc(850, 125, 180, 180, radians(120), radians(210));
        arc(850, 125, 180, 180, radians(330), radians(420));
        stroke(255,0,0);
        arc(850, 125, 180, 180, radians(230), radians(250));
        arc(850, 125, 180, 180, radians(290), radians(310));

        //labels
        fill(255);
        text("\u00B0F x 100", 850, 145);
        text("CHT", 930, 40);
        text("EGT", 770, 40);



        //power
        //ellipse(150, 220, 250, 250);

        strokeWeight(2);
        stroke(255);
        pushMatrix();
        translate(150, 220);
        rotate(radians(40));
        for(int i=0; i<15; i++){
            line(0, 105, 0, 125);
            rotate(radians(20));
        }
        //resetMatrix();
        //translate(150, 220);
        rotate(radians(-210));
        fill(255);
        textSize(16);
        for(int i=0; i<8; i++){
            pushMatrix();
            translate(90,0);
            rotate(radians(-130-i*40));
            text(i*5, 0, 0);
            popMatrix();
            rotate(radians(40));
        }
        //resetMatrix();
        //translate(150, 220);
        rotate(radians(310 + aircraft.getRpm() /12.5f));
        strokeWeight(5);
        if((aircraft.getRpm() >= 2750) && (millis() % 500 >= 250))
            stroke(255, 0, 0);
        line(0, 0, 0, 100);
        popMatrix();

        strokeWeight(3);
        noFill();
        stroke(255);
        arc(150, 220, 250, 250, radians(129.5f), radians(170));
        stroke(0,225,0);
        arc(150, 220, 250, 250, radians(170), radians(330));
        stroke(225,225,0);
        arc(150, 220, 250, 250, radians(330), radians(350));
        stroke(255,0,0);
        arc(150, 220, 250, 250, radians(350), radians(410.5f));
        fill(225);
        noStroke();
        if((aircraft.getRpm() >= 2750) && (millis() % 500 >= 250))
            fill(255, 0, 0);
        ellipse(150, 220, 10, 10);

        //power labels
        textSize(24);
        text("RPM\n" + aircraft.getRpm() + "\nPOWER " + aircraft.getPowerSetting() + "%\nFREQ " + aircraft
            .getRpm() /15 + "Hz", 150, 335);


        //mags/starters
        fill(255);
        text("DC 12", 150, 675);
        text("L", 455, 475);
        text("R", 645, 475);
        stroke(0,225,0);
        shapeMode(OPEN);
        noFill();
        line(150, 655, 150, 615);
        if(aircraft.isStarterSafetyKey() && aircraft.isLeftMagneto()){
            stroke(0,225,0);
            strokeWeight(4);
            beginShape();
            vertex(137, 565);
            vertex(137, 525);
            vertex(50, 525);
            vertex(50, 505);
            endShape();
            stroke(255);
            strokeWeight(2);
            fill(0,225,0);
            rect(50, 457, 50, 44);
            ellipse(50, 479, 50, 42);
            arc(50, 437, 50, 42, PI, 2*PI);
            fill(0);
            text("L", 50, 437);
        }
        else{
            stroke(255);
            strokeWeight(4);
            beginShape();
            vertex(137, 565);
            vertex(137, 525);
            vertex(50, 525);
            vertex(50, 505);
            endShape();
            strokeWeight(2);
            fill(0);
            rect(50, 457, 50, 44);
            ellipse(50, 479, 50, 42);
            arc(50, 437, 50, 42, PI, 2*PI);
            fill(255);
            text("L", 50, 437);
        }
        noFill();
        if(aircraft.isStarterSafetyKey() && aircraft.isRightMagneto()){
            stroke(0,225,0);
            strokeWeight(4);
            beginShape();
            vertex(163, 565);
            vertex(163, 525);
            vertex(250, 525);
            vertex(250, 505);
            endShape();
            stroke(255);
            strokeWeight(2);
            fill(0,225,0);
            rect(250, 457, 50, 44);
            ellipse(250, 479, 50, 42);
            arc(250, 437, 50, 42, PI, 2*PI);
            fill(0);
            text("R", 250, 437);
        }
        else{
            stroke(255);
            strokeWeight(4);
            beginShape();
            vertex(163, 565);
            vertex(163, 525);
            vertex(250, 525);
            vertex(250, 505);
            endShape();
            strokeWeight(2);
            fill(0);
            rect(250, 457, 50, 44);
            ellipse(250, 479, 50, 42);
            arc(250, 437, 50, 42, PI, 2*PI);
            fill(255);
            text("R", 250, 437);
        }


        if(aircraft.isStarterSafetyKey() && aircraft.isStarter()){
            stroke(0,225,0);
            strokeWeight(4);
            line(150, 565, 150, 505);
            stroke(255);
            strokeWeight(5);
            line(150, 445, 150, 432);
            line(150, 442, 158, 435);
            line(150, 442, 142, 435);
            line(139, 442, 161, 442);
            strokeWeight(2);
            fill(0,225,0);
            rect(150, 467, 26, 40);
            ellipse(150, 490, 26, 20);
            arc(150, 449, 26, 20, PI, 2*PI);
            fill(0);
            text("S", 150, 460);
        }
        else{
            stroke(255);
            strokeWeight(4);
            line(150, 565, 150, 505);
            strokeWeight(5);
            line(150, 445, 150, 432);
            line(150, 442, 158, 435);
            line(150, 442, 142, 435);
            line(139, 442, 161, 442);
            strokeWeight(2);
            fill(0);
            rect(150, 467, 26, 40);
            ellipse(150, 490, 26, 20);
            arc(150, 449, 26, 20, PI, 2*PI);
            fill(255);
            text("S", 150, 460);

        }


        strokeWeight(2);
        stroke(255);
        if(aircraft.isStarterSafetyKey())
            fill(0,225,0);
        else
            noFill();

        rect(150, 591, 45, 45, 5);

        if(aircraft.isStarterSafetyKey())
            stroke(0);
        else
            stroke(255);

        noFill();
        pushMatrix();
        translate(150, 591);
        rotate(QUARTER_PI);
        ellipse(0,-15,3,3);
        arc(0,-12,17,17,radians(-242), radians(75));
        beginShape();
        vertex(-4,-5);
        vertex(-4,17);
        vertex(-1,21);
        vertex(2,17);
        vertex(1,13);
        vertex(1,11);
        vertex(2,9);
        vertex(1,6);
        vertex(2,3);
        vertex(3,1);
        vertex(2,-1);
        vertex(4,-3);
        vertex(4,-5);
        endShape();
        popMatrix();

        fill(255);
        textSize(18);
        text(String.format("%1$.1f", aircraft.getFuelFlow()), 330, 220);
        text(String.format("%1$.1f",aircraft.getFuelPressureSensor()), 450, 220);
        text(aircraft.getOilTemperatureSensor(), 560, 220);
        text(aircraft.getOilPressureSensor(), 680, 220);
        text(aircraft.getExaustGasTempurature(), 790, 220);
        text(aircraft.getCylinderHeadTempurature(), 910, 220);
        textAlign(RIGHT, CENTER);
        text("ENG PUMP", 530, 278);
        text("ELEC BOOST", 530, 331);
        textAlign(LEFT, CENTER);
        text("ENG", 687, 278);
        textAlign(CENTER, CENTER);
        text("FOB:\n" + String.format("%1$.1f",(aircraft.getLeftFuelSensor()/50+aircraft.getRightFuelSensor()/50)*100) + "%\n" + String.format("%1$.1f",aircraft.getLeftFuelSensor()+aircraft.getRightFuelSensor()) +
                " GAL\n" + String.format("%1$.1f", aircraft.getFuelWeight()) + " LBS\n" + String.format("%1$.1f",
            aircraft.getEndurance()) + " HRS", 550, 610);
        if(aircraft.isLowFuelEmergency())
            fill(255,0,0);
        else
            fill(255);
        text(aircraft.getReserveMinutes() + " MIN", 850, 470);

        //timers
        stroke(255);
        strokeWeight(2);
        fill(255);
        rect(90, 738, 80, 32);
        rect(90, 771, 80, 32);
        text(String.format("%1$.2f", aircraft.getHobbsTime()), 200, 736);
        text(String.format("%1$.2f", aircraft.getTachTime()), 200, 770);
        noFill();
        rect(200, 738, 140, 32);
        rect(200, 771, 140, 32);
        fill(0);
        text("HOBBS", 90, 736);
        text("TACH", 90, 770);

        //carbHeat
        if(aircraft.isCarbHeat()){
            fill(255);
            rect(380, 670, 100, 60);
            fill(0,180,255);
            rect(380, 744, 100, 87);
            fill(0);
            text("CARB\nHEAT", 380, 668);
            textSize(14);
            text("POWER LOSS\n" + aircraft.getIcingPowerLoss() + " RPM", 380, 725);
            textSize(18);
            text(aircraft.getCarbIceBuildUp() + "% ICE", 380, 765);
        }
        else{
            noFill();
            rect(380, 670, 100, 60);
            rect(380, 744, 100, 87);
            fill(255);
            text("CARB\nHEAT", 380, 668);
            textSize(14);
            text("POWER LOSS\n" + aircraft.getIcingPowerLoss() + " RPM", 380, 725);
            textSize(18);
            fill(0,180,255);
            text(aircraft.getCarbIceBuildUp() + "% ICE", 380, 765);
        }


        //mixture
        rectMode(CORNERS);
        fill(127,0,0);
        noStroke();
        rect(470,764,470+ aircraft.getMixtureSetting() *4.5f, 788);
        rectMode(CENTER);

        noFill();
        stroke(255);
        rect(695, 776, 450, 24);
        fill(225);
        text("MIXTURE", 695, 805);
        //setpoints
        strokeWeight(1);
        textSize(24);
        line(470+ aircraft.getMixtureStop() *4.5f, 788, 470+ aircraft.getMixtureStop() *4.5f, 750);
        text("S", 470+ aircraft.getMixtureStop() *4.5f, 728);
        line(470+ aircraft.getMixtureTaxi() *4.5f, 788, 470+ aircraft.getMixtureTaxi() *4.5f, 750);
        text("T", 470+ aircraft.getMixtureTaxi() *4.5f, 728);
        line(470+ aircraft.getMixtureEconomy() *4.5f, 788, 470+
            aircraft.getMixtureEconomy() *4.5f, 750);
        text("E", 470+ aircraft.getMixtureEconomy() *4.5f, 728);
        line(470+ aircraft.getMixturePower() *4.5f, 788, 470+ aircraft.getMixturePower()
            *4.5f, 750);
        text("P", 470+ aircraft.getMixturePower() *4.5f, 728);

        if( (aircraft.getFuelSelector() == 1 && aircraft.getLeftFuelSensor() < aircraft.getRightFuelSensor()/2) ||  (
            aircraft.getFuelSelector() == 2 && aircraft.getRightFuelSensor() < aircraft.getLeftFuelSensor()/2) ){
            fill(255, 175, 0);
            text("SWITCH TANKS", 770, 384);
            stroke(255, 175, 0);
        }
        else{
            fill(40);
            text("SWITCH TANKS", 770, 384);
            stroke(40);
        }

        strokeWeight(2);
        noFill();
        rect(770, 387, 220, 45);
        fill(255);

        textSize(16);
        text("LEAN", 895, 800);
        text("RICH", 495, 800);

        if(aircraft.isLowFuelEmergency())
            fill(255,0,0);
        else
            fill(255);

        text("RESERVE:", 850, 435);
        text("UNTIL\nLOW FUEL\nEMERGENCY", 850, 555);

        fill(255,0,0);
        text(String.format("%1$.2f", aircraft.getEndurance() - 0.5) + " HRS", 850, 500);

        if(aircraft.getLeftFuelSensor() <= 2)
            fill(255,0,0);
        else
            fill(255);

        text(String.format("%1$.1f", aircraft.getLeftFuelSensor()/50*100) + "%", 380, 434);
        text(String.format("%1$.1f", aircraft.getLeftFuelSensor()) + " GAL\n" + String.format("%1$.1f", aircraft.getLeftFuelSensor()*6.01) + " LBS\n" + String.format("%1$.2f", aircraft.getLeftFuelSensor()/ aircraft
            .getFuelFlow()) + " HRS", 380, 555);

        if(aircraft.getRightFuelSensor() <= 2)
            fill(255,0,0);
        else
            fill(255);

        text(String.format("%1$.1f", aircraft.getRightFuelSensor()/50*100) + "%", 720, 434);
        text(String.format("%1$.1f", aircraft.getRightFuelSensor()) + " GAL\n" + String.format("%1$.1f", aircraft.getRightFuelSensor()*6.01) + " LBS\n" + String.format("%1$.2f", aircraft.getRightFuelSensor()/ aircraft
            .getFuelFlow()) + " HRS", 720, 555);

        text("FUEL USED: \n" + String.format("%1$.1f", (aircraft.getFuelAtStart() - aircraft.getRightFuelSensor()) - aircraft.getLeftFuelSensor()) + " GAL\n" + String.format("%1$.1f",
            aircraft.getFuelAtStart() *6.01 - aircraft.getRightFuelSensor()*6.01 - aircraft.getLeftFuelSensor()*6.01) + " LBS", 780, 655);

        text(String.format("%1$.2f", aircraft.getFuelFlow()) + " GPH\n" + String.format("%1$.2f",
            aircraft.getFuelFlow() *6.01) + " LBS/MIN", 705, 320);



        //fuel
        stroke(255);
        noFill();
        line(525, 410, 510, 410);
        line(575, 410, 590, 410);
        textSize(10);
        text("OFF", 495, 410);
        text("OFF", 605, 410);
        strokeWeight(2);
        noStroke();
        pushMatrix();
        translate(550,410);
        if(aircraft.getFuelSelector() == 0){
            fill(255);
            rotate(HALF_PI);
        }
        else if(aircraft.getFuelSelector() == 1){
            fill(0,225,0);
            rotate(radians(25));
        }
        else if(aircraft.getFuelSelector() == 2){
            fill(0,255,0);
            rotate(radians(-25));
        }
        beginShape();
        vertex(-5,24);
        vertex(-2,25);
        vertex(2,25);
        vertex(5,24);
        vertex(5,-24);
        vertex(2,-25);
        vertex(-2,-25);
        vertex(-5,-24);
        vertex(-5,24);
        endShape();
        popMatrix();

        //fuel level
        noStroke();
        fill(0, 180, 255);
        rectMode(CORNERS);
        rect(320, 510, 440, 510-2.4f*aircraft.getLeftFuelSensor());
        rect(660, 510, 780, 510-2.4f*aircraft.getRightFuelSensor());
        rectMode(CENTER);
        //tabs
        stroke(255);
        line(780, 466.8f, 760, 466.8f);
        line(320, 466.8f, 340, 466.8f);

        strokeWeight(1);
        stroke(255,0,0);
        noFill();
        beginShape();
        vertex(792, 508);
        vertex(782, 503);
        vertex(792, 498);
        endShape();
        beginShape();
        vertex(308, 508);
        vertex(318, 503);
        vertex(308, 498);
        endShape();

        strokeWeight(2);
        noFill();
        stroke(255);
        rect(380, 480, 120, 60);
        rect(720, 480, 120, 60);

        strokeWeight(4);
        if(aircraft.getFuelSelector() == 1)
            stroke(0,225,0);
        else
            stroke(255);
        beginShape();
        vertex(440, 500);
        vertex(540, 500);
        vertex(540, 433);
        endShape();

        if(aircraft.getFuelSelector() == 2)
            stroke(0,225,0);
        else
            stroke(255);
        beginShape();
        vertex(660, 500);
        vertex(560, 500);
        vertex(560, 433);
        endShape();



        if(aircraft.getFuelSelector() > 0)
            stroke(0,225,0);
        else
            stroke(255);
        line(550, 385, 550, 345);
        line(550, 320, 550, 293);
        line(562, 280, 680, 280);


        noStroke();
        if(aircraft.isFuelPump())
            fill(0, 225, 0);
        else
            fill(255);

        pushMatrix();
        translate(550, 333);
        for(int i=0; i<4; i++){
            rotate(HALF_PI);
            beginShape();
            vertex(0,0);
            vertex(-2,-2);
            vertex(-4,-3);
            vertex(-6,-3.5f);
            vertex(-8,-3);
            vertex(-10,-2);
            vertex(-12,-1);
            vertex(-12,1);
            vertex(-11,4);//
            vertex(-10,3);
            vertex(-8,2);
            vertex(-4,0.5f);
            vertex(-2,0);
            vertex(0,0);
            endShape();
        }
        popMatrix();

        if(aircraft.getRpm() > 500)
            fill(0, 225, 0);
        else
            fill(255);

        pushMatrix();
        translate(550, 280);
        for(int i=0; i<4; i++){
            rotate(HALF_PI);
            beginShape();
            vertex(0,0);
            vertex(-2,-2);
            vertex(-4,-3);
            vertex(-6,-3.5f);
            vertex(-8,-3);
            vertex(-10,-2);
            vertex(-12,-1);
            vertex(-12,1);
            vertex(-11,4);//
            vertex(-10,3);
            vertex(-8,2);
            vertex(-4,0.5f);
            vertex(-2,0);
            vertex(0,0);
            endShape();
        }
        popMatrix();

        stroke(255);
        noFill();
        strokeWeight(2);
        //selector
        ellipse(550, 410, 50, 50);
        //boost pump
        ellipse(550, 333, 24, 24);
        //engine pump
        ellipse(550, 280, 24, 24);
    }

    public void mousePressed() {
        if ((640 < mouseX && mouseX < 960) && (830 < mouseY && mouseY < 950))
            aircraft.getAutoFlightSettings().setAutoPilotDisconnectSwitch(!aircraft.getAutoFlightSettings().isAutoPilotDisconnectSwitch());

        if ((330< mouseX && mouseX < 430) && (640 < mouseY && mouseY < 788))
            aircraft.setCarbHeat(!aircraft.isCarbHeat());

        if ((0 < mouseX && mouseX < 640) && (830 < mouseY && mouseY < 950))
            println("switch to mode control panel");

        if ((0 < mouseX && mouseX < 100) && (400 < mouseY && mouseY < 550))
            aircraft.setLeftMagneto(!aircraft.isLeftMagneto());

        if ((200 < mouseX && mouseX < 300) && (400 < mouseY && mouseY < 550))
            aircraft.setRightMagneto(!aircraft.isRightMagneto());

        if ((100 < mouseX && mouseX < 200) && (400 < mouseY && mouseY < 550))
            aircraft.setStarter(!aircraft.isStarter());

        if ((500 < mouseX && mouseX < 600) && (360 < mouseY && mouseY < 460))
            if(aircraft.getFuelSelector() == 1)
                aircraft.setFuelSelector(2);
            else
                aircraft.setFuelSelector(1);
    }

}