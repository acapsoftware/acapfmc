package acap.fmc.gui.processing.apps;

import acap.fmc.UserAircraft;
import acap.fmc.gui.processing.ProcessingApp;
import processing.core.PShape;

/**
 * Load and Display a Shape.
 * Illustration by George Brower.
 *
 * The loadShape() command is used to read simple SVG (Scalable Vector Graphics)
 * files and OBJ (Object) files into a Processing sketch. This example loads an
 * SVG file of a monster robot face and displays it to the screen.
 */
public class FlightControls extends ProcessingApp {
    private PShape aircraft;

    private UserAircraft UAircraft = UserAircraft.getInstance();

    public FlightControls() {
        super(P2D, 0.69f);
        size(960, 825);
    }

    public void setup() {
        // The file "bot1.svg" must be in the data folder
        // of the current sketch to load successfully
        aircraft = loadShape("aircraft.svg");
    }

    public void draw() {
        super.draw();

        background(0);
        noFill();

        shapeMode(CORNER);
        textAlign(CENTER, CENTER);
        textSize(18);
        strokeCap(ROUND);
        rectMode(CENTER);
        textLeading(25);
        strokeWeight(1);
        stroke(255);

        shape(aircraft, 0, 0);

        //rudder
        pushMatrix();
        translate(480, 45);
        strokeWeight(2);
        noFill();
        rect(0, 4, 10, 8);


        if(UAircraft.getAirspeedSensor() >= UAircraft.getManeuveringSpeed())
            stroke(127);

        line(-80, 0,  80, 0);
        line(-80, 0, -80, 8);
        line( 80, 0,  80, 8);

        stroke(255);

        line(-40, 0,  40, 0);
        line(-40, 0, -40, 8);
        line( 40, 0,  40, 8);

        fill(0,225,0);
        noStroke();

        pushMatrix();
        translate(UAircraft.getRudderSensePosition()*0.8f, -13);

        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, 12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getRudderSetPosition()*0.8f, 21);
        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();

        stroke(255);
        fill(255);
        text("RUD", 0, -30);
        popMatrix();




        //Left Aileron
        pushMatrix();
        translate(110,112);
        rotate(-HALF_PI);
        strokeWeight(2);
        noFill();
        rect(0, 4, 10, 8);


        if(UAircraft.getAirspeedSensor() >= UAircraft.getManeuveringSpeed())
            stroke(127);

        line(-80, 0,  80, 0);
        line(-80, 0, -80, 8);
        line( 80, 0,  80, 8);

        stroke(255);

        line(-40, 0,  40, 0);
        line(-40, 0, -40, 8);
        line( 40, 0,  40, 8);

        fill(0,225,0);
        noStroke();

        pushMatrix();
        translate(UAircraft.getAileronSensePosition()*0.8f, -13);

        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, 12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getAileronSetPosition()*0.8f, 21);

        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();
        popMatrix();

        stroke(255);
        fill(255);
        text("L\nAIL", 480-410, 80);



        //Right Aileron
        pushMatrix();
        translate(360+480,112);
        rotate(HALF_PI);
        strokeWeight(2);
        noFill();
        rect(0, 4, 10, 8);


        if(UAircraft.getAirspeedSensor() >= UAircraft.getManeuveringSpeed())
            stroke(127);

        line(-80, 0,  80, 0);
        line(-80, 0, -80, 8);
        line( 80, 0,  80, 8);

        stroke(255);

        line(-40, 0,  40, 0);
        line(-40, 0, -40, 8);
        line( 40, 0,  40, 8);

        fill(0,225,0);
        noStroke();

        pushMatrix();
        translate(UAircraft.getAileronSensePosition()*0.8f, -13);
        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, 12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getAileronSetPosition()*0.8f, 21);
        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();
        popMatrix();

        stroke(255);
        fill(255);
        text("R\nAIL", 480+410, 80);


        //Stabilator
        pushMatrix();
        translate(320,140);
        rotate(-HALF_PI);
        strokeWeight(2);
        noFill();
        rect(0, 4, 10, 8);


        if(UAircraft.getAirspeedSensor() >= UAircraft.getManeuveringSpeed())
            stroke(127);

        line(-80, 0,  80, 0);
        line(-80, 0, -80, 8);
        line( 80, 0,  80, 8);

        stroke(255);

        line(-40, 0,  40, 0);
        line(-40, 0, -40, 8);
        line( 40, 0,  40, 8);

        fill(0,225,0);
        noStroke();

        pushMatrix();
        translate(UAircraft.getStabilatorSensePosition()*0.8f, -13);

        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, 12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getStabilatorSetPosition()*0.8f, 21);

        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();
        popMatrix();

        stroke(255);
        fill(255);
        text("STAB", 480-100, 140);




        //Stabilator Trim
        pushMatrix();
        translate(620,140);
        rotate(-HALF_PI);
        strokeWeight(2);
        noFill();
        rect(0, 4, 15, 8);

        line(-50, 0,  50, 0);
        line(-50, -10, -50, 10);
        line( 50, -10,  50, 10);

        noStroke();
        fill(0,180,255);

        pushMatrix();
        translate(UAircraft.getStabilatorTrimSensePosition()*0.5f, -13);

        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, 12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getStabilatorTrimSetPosition()*0.5f, 21);

        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();
        popMatrix();

        stroke(255);
        fill(255);
        text("PITCH\nTRIM", 480+80, 140);


//Rudder Trim
        pushMatrix();
        translate(480,375);
        strokeWeight(2);
        noFill();
        rect(0, 4, 15, 8);

        line(-50, 0,  50, 0);
        line(-50, -10, -50, 10);
        line( 50, -10,  50, 10);

        noStroke();
        fill(0,180,255);

        pushMatrix();
        translate(UAircraft.getRudderTrimSensePosition()*0.5f, -13);

        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, 12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getRudderTrimSetPosition()*0.5f, 21);
        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();
        popMatrix();

        stroke(255);
        fill(255);
        text("RUD\nTRIM", 480, 435);


        //Left Brake
        pushMatrix();
        translate(290,330);
        rotate(-HALF_PI);
        strokeWeight(2);
        fill(255);

        line(-35, 0,  35, 0);
        line(-35, -5, -35, 5);
        beginShape();
        vertex(35, 3);
        vertex(40, 0);
        vertex(35, -3);
        vertex(35, 3);
        endShape();

        noStroke();
        fill(0,225,0);

        pushMatrix();
        translate(UAircraft.getBrakeSensePosition()*0.35f, -13);

        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, 12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getBrakeSetPosition()*0.35f, 13);
        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();
        popMatrix();

        stroke(255);
        fill(255);
        text("L\nBRK", 480-230, 300);


        //Right Brake
        pushMatrix();
        translate(670,330);
        rotate(-HALF_PI);
        strokeWeight(2);
        fill(255);

        line(-35, 0,  35, 0);
        line(-35, -5, -35, 5);
        beginShape();
        vertex(35, 3);
        vertex(40, 0);
        vertex(35, -3);
        vertex(35, 3);
        endShape();

        noStroke();
        fill(0,225,0);

        pushMatrix();
        translate(UAircraft.getBrakeSensePosition()*0.35f, 13);
        beginShape();
        vertex(-8, 0);
        vertex(8, 0);
        vertex(0, -12);
        vertex(-8, 0);
        endShape();
        popMatrix();

        stroke(255, 0, 255);
        noFill();
        pushMatrix();
        translate(UAircraft.getBrakeSetPosition()*0.35f, -13);
        rotate(QUARTER_PI);
        rect(0,0,9,9);
        popMatrix();
        popMatrix();

        stroke(255);
        fill(255);
        text("R\nBRK", 480+230, 300);

        text("GND      SW", 480-7, 300);
        text("FLAPS", 480-335, 375);


        //ground switches
        strokeWeight(2);
        stroke(255);

        if(UAircraft.isLeftSquatSwitch())
            fill(0,225,0);
        else
            noFill();
        rect(480-143, 319, 16, 40, 5);

        if(UAircraft.isRightSquatSwitch())
            fill(0,225,0);
        else
            noFill();
        rect(480+143, 319, 16, 40, 5);

        if(UAircraft.isNoseSquatSwitch())
            fill(0,225,0);
        else
            noFill();
        rect(480, 327, 16, 40, 5);


        //flaps
        ellipseMode(CENTER);
        strokeWeight(2);
        stroke(255);

        if(!UAircraft.isFlapsInTransit() && UAircraft.getFlapSetPosition() > 0)
            fill(0,225,0);
        else if(UAircraft.getFlapSetPosition() == 0)
            noFill();
        else
            fill(225,225,0);


        //flap depiction
        pushMatrix();
        translate(480-238, 422);
        rotate(radians(UAircraft.getFlapSensor() * 0.01f * 40));
        beginShape();
        vertex(2, 9);
        vertex(60, 0);
        vertex(2, -9);
        endShape();
        arc(0, 0 , 18, 18, radians(70), radians(290), OPEN);
        popMatrix();

        //arc
        noFill();
        arc(480-238, 421, 185, 185, radians(0), radians(45), OPEN);
        pushMatrix();
        translate(480-238, 421);
        line(85, 0, 92.5f, 0);
        rotate(radians(10));
        line(88, 0, 92.5f, 0);
        rotate(radians(15));
        line(88, 0, 92.5f, 0);
        rotate(radians(15));
        line(88, 0, 92.5f, 0);
        rotate(radians(5));
        line(85, 0, 92.5f, 0);
        popMatrix();
        fill(255);
        text("0\u00B0", 480-125, 421);
        text("10\u00B0", 480-123, 441);
        text("25\u00B0", 480-129, 465);
        text("40\u00B0", 480-146, 489);

        strokeCap(SQUARE);
        //no flap range
        noFill();
        stroke(255,0,0);
        strokeWeight(6);
        arc(480-238, 421, 195, 195, radians(UAircraft.getSafeFlapSetting()), radians(46), OPEN);

        strokeCap(ROUND);

        //position selected
        noFill();
        strokeWeight(2);
        stroke(255,0,255);
        pushMatrix();
        translate(480-238, 421);
        if(UAircraft.getFlapSensor() == 1)
            rotate(radians(10));
        if(UAircraft.getFlapSensor() == 2)
            rotate(radians(25));
        if(UAircraft.getFlapSensor() == 3)
            rotate(radians(40));
        translate(77,0);
        rotate(QUARTER_PI);
        rect(0, 0, 8, 8);
        popMatrix();


        //statuses
        textSize(22);
        textAlign(RIGHT, CENTER);
        text("ROLL", 710, 400);
        text("PITCH", 710, 465);
        text("YAW", 710, 530);
        text("RUD TRIM", 710, 595);
        text("PITCH TRIM", 710, 660);
        text("FLAPS", 710, 725);
        text("BRAKES", 710, 790);

        textAlign(CENTER, CENTER);

        if(UAircraft.getRollStatus() == 0){
            stroke(255);
            noFill();
            rect(820, 402, 180, 50);
            fill(255);
            text("OFF", 820, 400);
        }
        else if(UAircraft.getRollStatus() == 1){
            stroke(225, 225, 0);
            fill(225, 225, 0);
            rect(820, 402, 180, 50);
            fill(0);
            text("ARMED", 820, 400);
        }
        else if(UAircraft.getRollStatus() == 2){
            stroke(0, 225, 0);
            fill(0, 225, 0);
            rect(820, 402, 180, 50);
            fill(0);
            text("ACTIVE", 820, 400);
        }

        if(UAircraft.getPitchStatus() == 0){
            stroke(255);
            noFill();
            rect(820, 467, 180, 50);
            fill(255);
            text("OFF", 820, 465);
        }
        else if(UAircraft.getPitchStatus() == 1){
            stroke(225, 225, 0);
            fill(225, 225, 0);
            rect(820, 467, 180, 50);
            fill(0);
            text("ARMED", 820, 465);
        }
        else if(UAircraft.getPitchStatus() == 2){
            stroke(0, 225, 0);
            fill(0, 225, 0);
            rect(820, 467, 180, 50);
            fill(0);
            text("ACTIVE", 820, 465);
        }


        if(UAircraft.getYawStatus() == 0){
            stroke(255);
            noFill();
            rect(820, 532, 180, 50);
            fill(255);
            text("OFF", 820, 530);
        }
        else if(UAircraft.getYawStatus() == 1){
            stroke(225, 225, 0);
            fill(225, 225, 0);
            rect(820, 532, 180, 50);
            fill(0);
            text("ARMED", 820, 530);
        }
        else if(UAircraft.getYawStatus() == 2){
            stroke(0, 225, 0);
            fill(0, 225, 0);
            rect(820, 532, 180, 50);
            fill(0);
            text("ACTIVE", 820, 530);
        }


        if(UAircraft.getRudderTrimStatus() == 0){
            stroke(255);
            noFill();
            rect(820, 597, 180, 50);
            fill(255);
            text("OFF", 820, 595);
        }
        else if(UAircraft.getRudderTrimStatus() == 1){
            stroke(225, 225, 0);
            fill(225, 225, 0);
            rect(820, 597, 180, 50);
            fill(0);
            text("ARMED", 820, 595);
        }
        else if(UAircraft.getRudderTrimStatus() == 2){
            stroke(0, 225, 0);
            fill(0, 225, 0);
            rect(820, 597, 180, 50);
            fill(0);
            text("ACTIVE", 820, 595);
        }


        if(UAircraft.getPitchTrimStatus() == 0){
            stroke(255);
            noFill();
            rect(820, 662, 180, 50);
            fill(255);
            text("OFF", 820, 660);
        }
        else if(UAircraft.getPitchTrimStatus() == 1){
            stroke(225, 225, 0);
            fill(225, 225, 0);
            rect(820, 662, 180, 50);
            fill(0);
            text("ARMED", 820, 660);
        }
        else if(UAircraft.getPitchTrimStatus() == 2){
            stroke(0, 225, 0);
            fill(0, 225, 0);
            rect(820, 662, 180, 50);
            fill(0);
            text("ACTIVE", 820, 660);
        }


        if(UAircraft.getFlapsStatus() == 0){
            stroke(255);
            noFill();
            rect(820, 727, 180, 50);
            fill(255);
            text("OFF", 820, 725);
        }
        else if(UAircraft.getFlapsStatus() == 1){
            stroke(225, 225, 0);
            fill(225, 225, 0);
            rect(820, 727, 180, 50);
            fill(0);
            text("ARMED", 820, 725);
        }
        else if(UAircraft.getFlapsStatus() == 2){
            stroke(0, 225, 0);
            fill(0, 225, 0);
            rect(820, 727, 180, 50);
            fill(0);
            text("ACTIVE", 820, 725);
        }


        if(UAircraft.getBrakesStatus() == 0){
            stroke(255);
            noFill();
            rect(820, 792, 180, 50);
            fill(255);
            text("OFF", 820, 790);
        }
        else if(UAircraft.getBrakesStatus() == 1){
            stroke(225, 225, 0);
            fill(225, 225, 0);
            rect(820, 792, 180, 50);
            fill(0);
            text("ARMED", 820, 790);
        }
        else if(UAircraft.getBrakesStatus() == 2){
            stroke(0, 225, 0);
            fill(0, 225, 0);
            rect(820, 792, 180, 50);
            fill(0);
            text("ACTIVE", 820, 790);
        }


        //sensors
        textAlign(LEFT, CENTER);
        textSize(20);
        strokeCap(SQUARE);
        strokeWeight(4);

        if(UAircraft.isTachStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("TACH", 480, 500);
        noFill();
        beginShape();
        vertex(355, 565);
        vertex(405, 565);
        vertex(405, 503);
        vertex(475, 503);
        endShape();

        if(UAircraft.isGpsStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("GPS", 480, 540);
        noFill();
        beginShape();
        vertex(355, 640);
        vertex(415, 640);
        vertex(415, 543);
        vertex(475, 543);
        endShape();

        if(UAircraft.isGyroStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("GYRO", 480, 580);
        noFill();
        beginShape();
        vertex(355, 650);
        vertex(425, 650);
        vertex(425, 583);
        vertex(475, 583);
        endShape();

        textAlign(CENTER, CENTER);

        if(UAircraft.isPitotStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("PITOT", 250, 530);
        noFill();
        beginShape();
        vertex(290, 630);
        vertex(290, 565);
        vertex(250, 565);
        vertex(250, 550);
        endShape();

        if(UAircraft.isStaticStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("STATIC", 100, 530);
        noFill();
        beginShape();
        vertex(280, 630);
        vertex(280, 575);
        vertex(100, 575);
        vertex(100, 550);
        endShape();

        if(UAircraft.isCameraStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("CAM", 450, 800);
        noFill();
        beginShape();
        vertex(355, 765);
        vertex(450, 765);
        vertex(450, 780);
        endShape();

        if(UAircraft.isLidarStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("LIDAR", 450, 730);
        noFill();
        beginShape();
        vertex(355, 695);
        vertex(450, 695);
        vertex(450, 710);
        endShape();

        if(UAircraft.isBarometerStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("BARO", 90, 708);
        noFill();
        beginShape();
        vertex(345, 675);
        vertex(90, 675);
        vertex(90, 695);
        endShape();

        if(UAircraft.isTemperatureStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("TEMP", 215, 708);
        noFill();
        beginShape();
        vertex(345, 685);
        vertex(215, 685);
        vertex(215, 695);
        endShape();

        textSize(14);
        if(UAircraft.isBarometerStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("CABIN INHG", 90, 763);
        if(UAircraft.isTemperatureStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("OAT \u00B0"+"F", 215, 763);
        textSize(22);
        if(UAircraft.isBarometerStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text(String.format("%1$.2f", UAircraft.getAltimeterSetting()), 90, 735);

        if(UAircraft.isTemperatureStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text(UAircraft.getOutsideAirTemp(), 215, 735);
        textSize(20);
        textAlign(LEFT, CENTER);
        if(UAircraft.isBarometerStatus()){
            stroke(0,225,0);
            fill(255);
        }
        else{
            stroke(255,0,0);
            fill(255,0,0);
        }
        text("DENSITY ALT: " + UAircraft.getDensityAltitude(), 50, 800);

        noFill();
        strokeWeight(2);
        if(UAircraft.isBarometerStatus()){
            stroke(0,225,0);
            stroke(255);
        }
        else{
            stroke(255,0,0);
        }
        rect(90, 737, 100, 80);
        if(UAircraft.isTemperatureStatus()){
            stroke(0,225,0);
            stroke(255);
        }
        else{
            stroke(255,0,0);
        }
        rect(215, 737, 70, 80);
    }

    public void mousePressed() {
        if ((700 < mouseX && mouseX < 940) && (372 < mouseY && mouseY < 432)){
            if(UAircraft.getRollStatus() >= 1)
                UAircraft.setRollStatus(0);
            else
                UAircraft.setRollStatus(1);
        }

        if ((700 < mouseX && mouseX < 940) && (437 < mouseY && mouseY < 497)){
            if(UAircraft.getPitchStatus() >= 1)
                UAircraft.setPitchStatus(0);
            else
                UAircraft.setPitchStatus(1);
        }

        if ((700 < mouseX && mouseX < 940) && (502 < mouseY && mouseY < 562)){
            if(UAircraft.getYawStatus() >= 1)
                UAircraft.setYawStatus(0);
            else
                UAircraft.setYawStatus(1);
        }

        if ((700 < mouseX && mouseX < 940) && (567 < mouseY && mouseY < 627)){
            if(UAircraft.getRudderTrimStatus() >= 1)
                UAircraft.setRudderTrimStatus(0);
            else
                UAircraft.setRudderTrimStatus(1);
        }

        if ((700 < mouseX && mouseX < 940) && (632 < mouseY && mouseY < 692)){
            if(UAircraft.getPitchTrimStatus() >= 1)
                UAircraft.setPitchTrimStatus(0);
            else
                UAircraft.setPitchTrimStatus(1);
        }

        if ((700 < mouseX && mouseX < 940) && (697 < mouseY && mouseY < 757)){
            if(UAircraft.getFlapsStatus() >= 1)
                UAircraft.setFlapsStatus(0);
            else
                UAircraft.setFlapsStatus(1);
        }

        if ((700 < mouseX && mouseX < 940) && (762 < mouseY && mouseY < 822)){
            if(UAircraft.getBrakesStatus() >= 1)
                UAircraft.setBrakesStatus(0);
            else
                UAircraft.setBrakesStatus(1);
        }
    }
}
