package acap.fmc.gui.processing.apps;

import acap.fmc.UserAircraft;
import acap.fmc.controls.Autoflight;
import acap.fmc.gui.processing.ProcessingApp;

import processing.core.PGraphics;

public class PrimaryFlightDisplay extends ProcessingApp {
    private static final float SCALE_FACTOR = 0.69f;
    UserAircraft aircraft = UserAircraft.getInstance();
    Autoflight autoflight = aircraft.getAutoFlightSettings();

    PGraphics speedTape;
    PGraphics speedPointer;
    PGraphics altitudeTape;
    PGraphics altitudePointer;
    PGraphics artificialHorizon;
    PGraphics compass;
    PGraphics verticalSpeedIndicator;

    public PrimaryFlightDisplay() {
        super(P2D, SCALE_FACTOR);
        size(960, 825);
    }

    public void setup() {
        speedTape = createGraphics(142, 650);
        speedPointer = createGraphics(120, 60);
        altitudeTape = createGraphics(130, 650);
        altitudePointer = createGraphics(150, 66);
        artificialHorizon = createGraphics(430,430);
        compass = createGraphics(480, 120);
        verticalSpeedIndicator = createGraphics(90, 500);
    }

    public void draw(){
        super.draw();

        background(0);
        noFill();

        textAlign(CENTER, CENTER);
        textSize(28);
        strokeCap(SQUARE);
        rectMode(CENTER);
        textLeading(25);
        strokeWeight(1);
        stroke(255);
        imageMode(CENTER);

        //VERTICAL SPEED
        verticalSpeedIndicator.beginDraw();
        verticalSpeedIndicator.background(0);
        verticalSpeedIndicator.noStroke();
        verticalSpeedIndicator.fill(127);
        verticalSpeedIndicator.textSize(15);
        verticalSpeedIndicator.strokeCap(SQUARE);
        verticalSpeedIndicator.textAlign(CENTER, CENTER);

        verticalSpeedIndicator.translate(0,250);
        verticalSpeedIndicator.beginShape();
        verticalSpeedIndicator.vertex(90,-75);
        verticalSpeedIndicator.vertex(90,75);
        verticalSpeedIndicator.vertex(40,250);
        verticalSpeedIndicator.vertex(5,250);
        verticalSpeedIndicator.vertex(5,50);
        verticalSpeedIndicator.vertex(25,40);
        verticalSpeedIndicator.vertex(25,-40);
        verticalSpeedIndicator.vertex(5,-50);
        verticalSpeedIndicator.vertex(5,-250);
        verticalSpeedIndicator.vertex(40,-250);
        verticalSpeedIndicator.vertex(90,-75);
        verticalSpeedIndicator.endShape();

        verticalSpeedIndicator.fill(255);
        verticalSpeedIndicator.stroke(255);
        verticalSpeedIndicator.strokeWeight(3);
        verticalSpeedIndicator.line(25,0,40,0);

        verticalSpeedIndicator.strokeWeight(1);
        verticalSpeedIndicator.line(25,30,30,30);
        verticalSpeedIndicator.line(25,-30,30,-30);
        verticalSpeedIndicator.line(25,80,30,80);
        verticalSpeedIndicator.line(25,-80,30,-80);
        verticalSpeedIndicator.line(25,180,30,180);
        verticalSpeedIndicator.line(25,-180,30,-180);
        verticalSpeedIndicator.text(".5", 15, 58);
        verticalSpeedIndicator.text(".5", 15, -62);
        verticalSpeedIndicator.text("1", 15, 118);
        verticalSpeedIndicator.text("1", 15, -122);
        verticalSpeedIndicator.text("2", 15, 238);
        verticalSpeedIndicator.text("2", 15, -242);

        verticalSpeedIndicator.strokeWeight(2);
        verticalSpeedIndicator.line(25,60,35,60);
        verticalSpeedIndicator.line(25,-60,35,-60);
        verticalSpeedIndicator.line(25,120,35,120);
        verticalSpeedIndicator.line(25,-120,35,-120);
        verticalSpeedIndicator.line(25,240,35,240);
        verticalSpeedIndicator.line(25,-240,35,-240);

        verticalSpeedIndicator.strokeWeight(7);
        verticalSpeedIndicator.line(95, 0, 38, constrain(-aircraft.getVerticalSpeed()/8.333f, -240, 240));


        verticalSpeedIndicator.strokeWeight(5);
        verticalSpeedIndicator.stroke(255,50,255);
        verticalSpeedIndicator.line(0, constrain(-autoflight.getDesiredVerticalSpeed()/8.333f, -240, 240), 30, constrain(-autoflight.getDesiredVerticalSpeed()/8.333f, -240, 240));

        verticalSpeedIndicator.endDraw();

        image(verticalSpeedIndicator, 910, 450);

        textSize(22);
        fill(255,50,255);
        text(autoflight.getDesiredVerticalSpeed(), 905, 720);

        //COMPASS
        compass.beginDraw();
        compass.background(0);
        compass.noStroke();
        compass.fill(127);
        compass.ellipse(compass.width/2, 320, 600, 600);
        compass.fill(255);
        compass.textSize(28);
        compass.stroke(255);
        compass.strokeWeight(2);
        compass.textAlign(CENTER, CENTER);
        compass.strokeCap(SQUARE);

        compass.noFill();
        compass.beginShape();
        compass.vertex(compass.width/2, 15);
        compass.vertex(compass.width/2+12, 0);
        compass.vertex(compass.width/2-12, 0);
        compass.vertex(compass.width/2, 15);
        compass.endShape();

        compass.pushMatrix();
        compass.translate(compass.width/2, 320);
        compass.rotate(radians(-aircraft.getHeading() + 10));

        for(int i=2; i<74; i++){
            if((i+3)%2==0){
                compass.strokeWeight(1);
                compass.line(0,-290,0,-300);
            }
            else if((i+3)%3==0){
                compass.strokeWeight(3);
                compass.textSize(28);
                compass.line(0,-280,0,-300);
                compass.text(i/2, -1, -266);
            }
            else{
                compass.strokeWeight(2);
                compass.textSize(15);
                compass.line(0,280,0,300);
                compass.text(i/2, -1, -270);
            }

            compass.rotate(radians(5));
        }
        compass.popMatrix();

        compass.fill(255,50,255);
        compass.textSize(24);
        compass.text(autoflight.getDesiredHeading(), 175, 100);
        compass.fill(0,200,0);
        compass.textSize(18);
        compass.text("MAG",325 , 105);

        //crab angle/track
        compass.pushMatrix();
        compass.translate(compass.width/2, 320);
        compass.strokeWeight(2);
        compass.rotate(radians(aircraft.getHeading()-aircraft.getTrack()));
        compass.line(0,0,0,-300);
        compass.line(-10,-238,10,-238);
        compass.popMatrix();


        //heading bug
        compass.pushMatrix();
        compass.translate(compass.width/2, 320);
        compass.strokeWeight(2);
        compass.stroke(255,50,255);
        float headingSetConstrained = constrain((-aircraft.getHeading()+autoflight.getDesiredHeading()-180)%360, -225, -135);

        compass.rotate(radians(headingSetConstrained));
        compass.translate(0,295);
        compass.noFill();
        compass.beginShape();
        compass.vertex(-20, 0);
        compass.vertex(20, 0);
        compass.vertex(20, 20);
        compass.vertex(15, 20);
        compass.vertex(0, 5);
        compass.vertex(-15, 20);
        compass.vertex(-20, 20);
        compass.vertex(-20, 0);
        compass.endShape();
        compass.popMatrix();

        compass.endDraw();

        image(compass,415,770);



        //SPEED TAPE
        speedTape.beginDraw();
        speedTape.background(0);
        speedTape.noStroke();
        speedTape.fill(127);
        speedTape.rectMode(CORNERS);
        speedTape.rect(0, 0, 100, 650);
        speedTape.fill(255);
        speedTape.textSize(28);
        speedTape.stroke(255);
        speedTape.strokeWeight(3);
        speedTape.textAlign(RIGHT, CENTER);
        speedTape.pushMatrix();
        speedTape.translate(0, 325.0f+aircraft.getAirspeedSensor()*10.0f);

        for(int i=0; i<51; i++){
            if(i%2==0)
                speedTape.text(i*5, 73, -i*50);
            speedTape.line(83,3-i*50 , 98, 3-i*50);
        }

        speedTape.strokeCap(SQUARE);

        if(aircraft.getAirspeedSensor()>30){
            speedTape.stroke(255,0,0);
            speedTape.strokeWeight(14);
            speedTape.line(102, -aircraft.getStallSpeed()*10.0f, 102, -aircraft.getStallSpeed()*10.0f+1000);
            speedTape.line(102, -aircraft.getVne()*10.0f, 102, -aircraft.getVne()*10.0f-1000);

            speedTape.stroke(255,255,0);
            speedTape.strokeWeight(6);
            speedTape.line(106, -aircraft.getVno()*10.0f, 106, -aircraft.getVne()*10.0f);

            speedTape.stroke(255,255,0);
            speedTape.strokeWeight(2);
            speedTape.line(107, -aircraft.getMinimumManeuveringSpeed()*10.0f, 107, -aircraft.getStallSpeed()*10.0f);
            speedTape.line(99, -aircraft.getMinimumManeuveringSpeed()*10.0f, 107, -aircraft.getMinimumManeuveringSpeed()*10.0f);
            speedTape.line(99, -aircraft.getMaximumManeuveringSpeed()*10.0f, 107, -aircraft.getMaximumManeuveringSpeed()*10.0f);
            speedTape.line(107, -aircraft.getMaximumManeuveringSpeed()*10.0f, 107, -aircraft.getVno()*10.0f);

            //vspeeds
            speedTape.stroke(0,255,0);
            speedTape.fill(0,255,0);
            speedTape.strokeWeight(1);
            speedTape.textSize(14);
            speedTape.textAlign(LEFT, CENTER);

            if(aircraft.getPhaseOfFlight() > 5){
                speedTape.line(80, -aircraft.getVref()*10, 110, -aircraft.getVref()*10);
                speedTape.text("REF", 112, -aircraft.getVref()*10-3);

                speedTape.line(90, -aircraft.getFlaps1speed()*10, 110, -aircraft.getFlaps1speed()*10);
                speedTape.text("1", 112, -aircraft.getFlaps1speed()*10-3);

                speedTape.line(90, -aircraft.getFlaps2speed()*10, 110, -aircraft.getFlaps2speed()*10);
                speedTape.text("2", 112, -aircraft.getFlaps2speed()*10-3);

                speedTape.line(90, -aircraft.getFlaps3speed()*10, 110, -aircraft.getFlaps3speed()*10);
                speedTape.text("3", 112, -aircraft.getFlaps3speed()*10-3);
            }

            if(aircraft.getPhaseOfFlight() < 5){

                speedTape.line(90, -aircraft.getVx()*10, 110, -aircraft.getVx()*10);
                speedTape.text("X", 112, -aircraft.getVx()*10-3);

                speedTape.line(90, -aircraft.getVy()*10, 110, -aircraft.getVy()*10);
                speedTape.text("Y", 112, -aircraft.getVy()*10-3);

                speedTape.line(90, -aircraft.getVy()+5*10, 110, -aircraft.getVy()+5*10);
                speedTape.text("UP", 112, -aircraft.getVy()+5*10-3);

                speedTape.line(80, -aircraft.getRotationSpeed()*10, 110, -aircraft.getRotationSpeed()*10);
                speedTape.text("ROT", 112, -aircraft.getRotationSpeed()*10-3);
            }

            speedTape.line(90, -aircraft.getVg()*10, 110, -aircraft.getVg()*10);
            speedTape.text("G", 112, -aircraft.getVg()*10-3);


        }

        //Autopilot speed setting pointer
        speedTape.noFill();
        speedTape.stroke(255,50,255);
        speedTape.strokeWeight(3);
        speedTape.beginShape();
        float speedSetConstrained = constrain(-autoflight.getDesiredSpeed()*10.0f, -328.0f-aircraft.getAirspeedSensor()*10.0f, 322.0f-aircraft.getAirspeedSensor()*10.0f);
        speedTape.vertex(85,speedSetConstrained+3);
        speedTape.vertex(97,speedSetConstrained+15);
        speedTape.vertex(135,speedSetConstrained+15);
        speedTape.vertex(135,speedSetConstrained-9);
        speedTape.vertex(97,speedSetConstrained-9);
        speedTape.vertex(85,speedSetConstrained+3);
        speedTape.endShape();

        speedTape.popMatrix();


        //speedtrend
        speedTape.stroke(0,255,0);
        speedTape.strokeWeight(4);
        speedTape.line(75 ,325, 75, 325-aircraft.getSpeedTrend()*10);




        speedTape.endDraw();

        image(speedTape, 112, 450);


        fill(255);
        text("GS " + String.format("%1$.0f",aircraft.getGroundspeed()), 92, 798);

        fill(255,50,255);
        text(autoflight.getDesiredSpeed(), 92, 98);




        //SPEED POINTER
        speedPointer.beginDraw();
        speedPointer.strokeWeight(2);
        speedPointer.stroke(255);
        speedPointer.fill(0);
        speedPointer.beginShape();
        speedPointer.vertex(1, 1);
        speedPointer.vertex(75, 1);
        speedPointer.vertex(75, 20);
        speedPointer.vertex(85, 30);
        speedPointer.vertex(75, 40);
        speedPointer.vertex(75, 59);
        speedPointer.vertex(1, 59);
        speedPointer.vertex(1, 1);
        speedPointer.endShape();


        speedPointer.fill(255);
        speedPointer.textAlign(CENTER, BOTTOM);
        speedPointer.textSize(30);
        speedPointer.textLeading(30);
        speedPointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 58, (aircraft.getAirspeedSensor()%10)*30+78);
        speedPointer.textLeading(50);
        if((int)(aircraft.getAirspeedSensor()%10)==9)
            speedPointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 38, (int)((aircraft.getAirspeedSensor()/10)%10)*50 + ((aircraft.getAirspeedSensor()*10%10)*5 +98));
        else
            speedPointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 38, (int)((aircraft.getAirspeedSensor()/10)%10)*50+98);

        if((int)(aircraft.getAirspeedSensor()%100)==99)
            speedPointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 18, (int)((aircraft.getAirspeedSensor()/100)%10)*50 + ((aircraft.getAirspeedSensor()*10%10)*5 +98));
        else
            speedPointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 18, (int)((aircraft.getAirspeedSensor()/100)%10)*50+98);


        speedPointer.endDraw();
        image(speedPointer, 90, 450);



        //ALTITUDE TAPE
        altitudeTape.beginDraw();
        altitudeTape.background(0);
        altitudeTape.noStroke();
        altitudeTape.fill(127);
        altitudeTape.rectMode(CORNERS);
        altitudeTape.rect(10, 0, 130, 650);
        altitudeTape.fill(255);
        altitudeTape.textSize(22);
        altitudeTape.stroke(255);
        altitudeTape.strokeWeight(3);
        altitudeTape.strokeCap(SQUARE);
        altitudeTape.textAlign(RIGHT, CENTER);
        altitudeTape.pushMatrix();

        altitudeTape.translate(0, (float) (325.0f+aircraft.getAltitude()*0.5f));

        for(int i=-20; i<151; i++){
            if(i%2==0)
                altitudeTape.text(i*100, 120, -i*50);

            if(i%5==0)
                altitudeTape.strokeWeight(5);
            else
                altitudeTape.strokeWeight(2);
            altitudeTape.line(10,3-i*50 , 30, 3-i*50);

            if(i%10==0){
                altitudeTape.strokeWeight(1);
                altitudeTape.line(40,20-i*50 , 130, 20-i*50);
                altitudeTape.line(40,-14-i*50 , 130, -14-i*50);
            }
        }

        //Autopilot altitude setting pointer
        altitudeTape.noFill();
        altitudeTape.stroke(255,50,255);
        altitudeTape.strokeWeight(3);
        altitudeTape.beginShape();
        float altitudeSetConstrained = constrain(-autoflight.getDesiredAltitude()*0.5f, -328.0f-(float)aircraft.getAltitude()*0.5f, 322.0f-(float)aircraft.getAltitude()*0.5f);
        altitudeTape.vertex(2,altitudeSetConstrained+28);
        altitudeTape.vertex(40,altitudeSetConstrained+28);
        altitudeTape.vertex(40,altitudeSetConstrained+13);
        altitudeTape.vertex(30,altitudeSetConstrained+3);
        altitudeTape.vertex(40,altitudeSetConstrained-7);
        altitudeTape.vertex(40,altitudeSetConstrained-22);
        altitudeTape.vertex(2,altitudeSetConstrained-22);
        altitudeTape.vertex(2,altitudeSetConstrained+28);
        altitudeTape.endShape();

        altitudeTape.stroke(255,255,0);
        altitudeTape.rectMode(CORNERS);
        altitudeTape.rect(11, (-aircraft.getDestinationAltitude()+6)*0.5f, 128, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.line(11, (-aircraft.getDestinationAltitude()+6+60)*0.5f, 20, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.line(11, (-aircraft.getDestinationAltitude()+6)*0.5f, 40, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.line(30, (-aircraft.getDestinationAltitude()+6)*0.5f, 60, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.line(50, (-aircraft.getDestinationAltitude()+6)*0.5f, 80, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.line(70, (-aircraft.getDestinationAltitude()+6)*0.5f, 100, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.line(90, (-aircraft.getDestinationAltitude()+6)*0.5f, 120, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.line(110, (-aircraft.getDestinationAltitude()+6)*0.5f, 140, (-aircraft.getDestinationAltitude()+100)*0.5F);
        altitudeTape.strokeWeight(6);
        altitudeTape.line(13, (-aircraft.getDestinationAltitude()+100)*0.5f, 13, (-aircraft.getDestinationAltitude()+1000)*0.5f);

        altitudeTape.line(13, (-aircraft.getOxygenAltitude()+6)*0.5f, 14, (-aircraft.getServiceCeiling()+6)*0.5f);
        altitudeTape.stroke(255,0,0);
        altitudeTape.line(13, (-aircraft.getServiceCeiling()+6)*0.5f, 14, (-aircraft.getServiceCeiling()-5000)*0.5f);



        altitudeTape.popMatrix();
        altitudeTape.endDraw();

        image(altitudeTape, 785, 450);

        textSize(22);
        fill(0,200,0);
        text(String.format("%1$.2f",aircraft.getBarometerSetting()) + " inhg", 790, 798);

        textSize(28);
        fill(255,50,255);
        text(autoflight.getDesiredAltitude(), 790, 98);




        //ALTITUDE POINTER
        altitudePointer.beginDraw();
        altitudePointer.strokeWeight(2);
        altitudePointer.stroke(255);
        altitudePointer.fill(0);
        altitudePointer.beginShape();
        altitudePointer.vertex(10, 1);
        altitudePointer.vertex(110, 1);
        altitudePointer.vertex(110, 65);
        altitudePointer.vertex(10, 65);
        altitudePointer.vertex(10, 43);
        altitudePointer.vertex(1, 33);
        altitudePointer.vertex(10, 23);
        altitudePointer.vertex(10, 1);
        altitudePointer.endShape();


        altitudePointer.fill(255);
        altitudePointer.textAlign(CENTER, BOTTOM);
        altitudePointer.textSize(22);
        altitudePointer.textLeading(30);
        altitudePointer.text("10\n00\n90\n80\n70\n60\n50\n40\n30\n20\n10\n00\n90", 92,
            (float) ((aircraft.getAltitude()/10%10)*30+79));
        altitudePointer.textLeading(50);
        if((int)((aircraft.getAltitude()%100)/10) == 9)
            altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 70,
                (float) ((int)((aircraft.getAltitude()/100)%10)*50 + (aircraft.getAltitude()*10%100)/2 +99));
        else
            altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 70, (int)((aircraft.getAltitude()/100)%10)*50+99);

        altitudePointer.textSize(32);
        altitudePointer.textLeading(50);

        if((int)((aircraft.getAltitude()%1000)/10) == 99)
            altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 50,
                (float) ((int)((aircraft.getAltitude()/1000)%10)*50 + (aircraft.getAltitude()*10%100)/2 +101));
        else
            altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 50, (int)((aircraft.getAltitude()/1000)%10)*50+101);

        if(aircraft.getAltitude() >= 9990){
            if((int)((aircraft.getAltitude()%10000)/10) == 999)
                altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 31,
                    (float) ((int)((aircraft.getAltitude()/10000)%10)*50 + (aircraft.getAltitude()*10%100)/2 +101));
            else
                altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n0\n9", 31, (int)((aircraft.getAltitude()/10000)%10)*50+101);

        }
        else{
            if((int)((aircraft.getAltitude()%10000)/10) == 999)
                altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n \n ", 31,
                    (float) ((int)((aircraft.getAltitude()/10000)%10)*50 + (aircraft.getAltitude()*10%100)/2 +101));
            else
                altitudePointer.text("1\n0\n9\n8\n7\n6\n5\n4\n3\n2\n1\n \n ", 31, (int)((aircraft.getAltitude()/10000)%10)*50+101);

        }

        altitudePointer.endDraw();
        image(altitudePointer, 835, 450);









        artificialHorizon.beginDraw();
        artificialHorizon.translate(artificialHorizon.width/2, artificialHorizon.height/2);
        artificialHorizon.scale(1.1f);
        artificialHorizon.translate(-artificialHorizon.width/2, -artificialHorizon.height/2);

        artificialHorizon.rectMode(CORNERS);

        artificialHorizon.pushMatrix();
        artificialHorizon.translate(artificialHorizon.width/2, artificialHorizon.height/2);

        artificialHorizon.rotate(radians((float) -aircraft.getRoll()));
        artificialHorizon.translate(0, (float) ((float)aircraft.getPitch()*8.0f));

        artificialHorizon.noStroke();
        artificialHorizon.strokeWeight(4);

        artificialHorizon.fill(0,112,235);
        artificialHorizon.rect(-500, 0, 500, -1000);
        artificialHorizon.fill(100,40,0);
        artificialHorizon.rect(-500, 0, 500, 1000);

        artificialHorizon.stroke(255);
        artificialHorizon.line(-500, 0, 500, 0);

        artificialHorizon.strokeWeight(2);
        artificialHorizon.fill(255);
        artificialHorizon.textSize(16);
        artificialHorizon.textAlign(CENTER, CENTER);

        for(int i= -14+(int)(aircraft.getPitch()/2.5); i< (8+(int)(aircraft.getPitch()/2.5)); i++){
            if(i%4 == 0 && i/4 != 0){
                artificialHorizon.line(-100, -i*20, 100, -i*20);
                artificialHorizon.text((i/4)*10, -120, -i*20-3);
                artificialHorizon.text((i/4)*10,  118, -i*20-3);

            }
            else if(i%2 == 0)
                artificialHorizon.line(-50, -i*20, 50, -i*20);
            else
                artificialHorizon.line(-20, -i*20, 20, -i*20);
        }

        artificialHorizon.stroke(255);
        artificialHorizon.noFill();
        artificialHorizon.strokeWeight(2);
        artificialHorizon.translate(0, (float) (-aircraft.getPitch()*8.0f));

        artificialHorizon.beginShape();
        artificialHorizon.vertex(0,-172);
        artificialHorizon.vertex(-10,-157);
        artificialHorizon.vertex(10,-157);
        artificialHorizon.vertex(0,-172);
        artificialHorizon.endShape();

        artificialHorizon.rectMode(CENTER);
        artificialHorizon.rect(-aircraft.getLateralForce(),-153, 20, 5);


        artificialHorizon.popMatrix();

        artificialHorizon.pushMatrix();

        artificialHorizon.translate(artificialHorizon.width/2, artificialHorizon.height/2);
        artificialHorizon.fill(0);
        artificialHorizon.strokeWeight(2);
        artificialHorizon.rectMode(CENTER);

        artificialHorizon.rect(0,0,8,8);

        artificialHorizon.beginShape();
        artificialHorizon.vertex(-60,-5);
        artificialHorizon.vertex(-60,20);
        artificialHorizon.vertex(-68,20);
        artificialHorizon.vertex(-68,3);
        artificialHorizon.vertex(-140,3);
        artificialHorizon.vertex(-140,-5);
        artificialHorizon.vertex(-60,-5);
        artificialHorizon.endShape();

        artificialHorizon.beginShape();
        artificialHorizon.vertex(60,-5);
        artificialHorizon.vertex(60,20);
        artificialHorizon.vertex(68,20);
        artificialHorizon.vertex(68,3);
        artificialHorizon.vertex(140,3);
        artificialHorizon.vertex(140,-5);
        artificialHorizon.vertex(60,-5);
        artificialHorizon.endShape();

        artificialHorizon.fill(255);
        artificialHorizon.noStroke();

        artificialHorizon.beginShape();
        artificialHorizon.vertex(0,-175);
        artificialHorizon.vertex(-10,-190);
        artificialHorizon.vertex(10,-190);
        artificialHorizon.vertex(0,-175);
        artificialHorizon.endShape();

        if(aircraft.isFlightDirector()){
            artificialHorizon.noStroke();
            artificialHorizon.fill(255,50,255);
            artificialHorizon.rect(0, constrain(-(autoflight.getDesiredPitch()-(float)aircraft.getPitch())*8, -150, 150), 200, 5);
            artificialHorizon.rect(constrain(
                (float) ((autoflight.getDesiredRoll()-aircraft.getRoll())*2.5f), -150, 150), 0, 5, 200);
        }

        if(aircraft.getFlapSensor() > 5 && (aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8 <= 130){
            artificialHorizon.noFill();
            artificialHorizon.stroke(255,255,50);
            artificialHorizon.strokeWeight(3);
            artificialHorizon.line(60, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8+3, 60, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5);
            artificialHorizon.line(60, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, 90, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5);
            artificialHorizon.line(70, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, 75, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-20);
            artificialHorizon.line(78, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, 83, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-20);
            artificialHorizon.line(86, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, 91, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-20);

            artificialHorizon.line(-60, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8+3, -60, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5);
            artificialHorizon.line(-60, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, -90, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5);
            artificialHorizon.line(-70, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, -75, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-20);
            artificialHorizon.line(-78, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, -83, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-20);
            artificialHorizon.line(-86, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-5, -91, -(aircraft.getCriticalAOA()-(float)aircraft.getPitch())*8-20);
        }

        artificialHorizon.stroke(255);

        artificialHorizon.rotate(radians(-240));
        artificialHorizon.line(0,180,0,190);
        artificialHorizon.rotate(radians(15));
        artificialHorizon.line(0,180,0,190);
        artificialHorizon.rotate(radians(15));
        artificialHorizon.line(0,180,0,197);
        artificialHorizon.rotate(radians(10));
        artificialHorizon.line(0,180,0,190);
        artificialHorizon.rotate(radians(10));
        artificialHorizon.line(0,180,0,190);
        artificialHorizon.rotate(radians(20));
        artificialHorizon.line(0,180,0,190);
        artificialHorizon.rotate(radians(10));
        artificialHorizon.line(0,180,0,190);
        artificialHorizon.rotate(radians(10));
        artificialHorizon.line(0,180,0,197);
        artificialHorizon.rotate(radians(15));
        artificialHorizon.line(0,180,0,190);
        artificialHorizon.rotate(radians(15));
        artificialHorizon.line(0,180,0,190);

        artificialHorizon.popMatrix();



        artificialHorizon.noFill();
        artificialHorizon.stroke(0);
        artificialHorizon.strokeWeight(40);
        artificialHorizon.strokeCap(SQUARE);
        artificialHorizon.arc(50,50, 100, 100, PI, PI+HALF_PI);
        artificialHorizon.arc(380,50, 100, 100, PI+HALF_PI, PI*2);
        artificialHorizon.arc(380,380, 100, 100, 0, HALF_PI);
        artificialHorizon.arc(50,380, 100, 100, HALF_PI, PI);


        artificialHorizon.endDraw();

        image(artificialHorizon, 415, 420);











        //localizer/glideslope
        strokeWeight(1);
        noFill();
        stroke(255);

        line(415, 658, 415, 682);
        ellipse(415+60,670,12,12);
        ellipse(415+120,670,12,12);
        ellipse(415-60,670,12,12);
        ellipse(415-120,670,12,12);

        line(668, 420, 692, 420);
        ellipse(680, 420+60, 12, 12);
        ellipse(680, 420+120, 12, 12);
        ellipse(680, 420-60, 12, 12);
        ellipse(680, 420-120, 12, 12);

        noStroke();
        fill(255,50,255);

        //localizer
        pushMatrix();
        translate(415+aircraft.getLocalizerDeflection()*1.2f,670);
        rotate(radians(45));
        rect(0,0,15,15);
        popMatrix();

        //glideslope
        pushMatrix();
        translate(680,420+aircraft.getGlideslopeDeflection()*1.2f);
        rotate(radians(45));
        rect(0,0,15,15);
        popMatrix();

    }


}
