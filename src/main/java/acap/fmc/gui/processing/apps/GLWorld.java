/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.processing.apps;

import static acap.ClassLoader.getClasses;
import static acap.fmc.Constants.COPYRIGHTPROMPT;
import static acap.world.utilities.CalculationUtils.latToXCoord;
import static acap.world.utilities.CalculationUtils.lonToYCoord;
import static acap.world.model.Camera.*;

import acap.Inject;
import acap.fmc.Constants;
import acap.fmc.controls.ControlScheduler;
import acap.fmc.gui.processing.ProcessingApp;
import acap.fmc.hid.joystickinput;
import acap.fmc.procedure.SubscribeThread;
import acap.fmc.procedure.fsm.StateMachine;
import acap.fmc.routing.Waypoint;
import acap.fmc.sensors.Spatial;
import acap.utilities.Audio;
import acap.utilities.SIMReceiveThread;
import acap.world.utilities.NavDB;
import acap.world.controller.CameraController;
import acap.world.model.World;
import acap.world.view.AircraftView;
import acap.world.view.WorldOverlay;
import acap.world.view.WorldView;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import org.graphstream.graph.Edge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import processing.core.PFont;
import processing.core.PShape;
import processing.event.MouseEvent;
import processing.sound.SoundFile;

@SuppressWarnings("unused")
public class GLWorld extends ProcessingApp {

  public static final float FOVY = PI / 3.0f;
  private static GLWorld w = new GLWorld();

  public static GLWorld getInstance() {
    return w;
  }

  //interface
  private static PFont mediumFont;
  public static PFont groundFont;
  private TextField scratchPad;

  //AI Aircraft
  int totalAircraft = 0;

  private static final Logger log = LoggerFactory.getLogger(GLWorld.class);

  public ArrayList<Waypoint> route = new ArrayList<>();
  ArrayList<Edge> pathEdges;

  private NavDB navdb;

  public PShape triShape;

  public GLWorld() {
    super(P3D);
  }

  public void loadPluginClasses() {
    try {

      Arrays.stream(getClasses("acap.world.plugin")).forEach(System.out::println);

      ClassLoader myClassLoader = ClassLoader.getSystemClassLoader();

      for (String cname : getClasses("acap.world.plugin")) {
        Class myClass = myClassLoader.loadClass(cname);
        Runnable whatInstance = (Runnable) myClass.newInstance();
        Inject.getInstance().addToDrawLoop(whatInstance);
      }

    } catch (SecurityException e) {
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  public void setup() {

    loadFonts();

    background(color(0, 0, 0));

    textFont(groundFont);
    textMode(MODEL);

    textAlign(CENTER);
    text("Loading...", width / 2, height / 2);

    smooth();
    navdb = NavDB.getInstance();
    new WorldView();
    Inject.getInstance().addToStartup(World.getInstance()::createUserAircraft);
    Inject.getInstance().addToDrawLoop(new CameraController()::updateCamera);
    Inject.getInstance().addToDrawLoop(new WorldView()::drawSky);
    Inject.getInstance().addToDrawLoop(new AircraftView()::drawUserAircraft);
    Inject.getInstance().addToDrawLoop(new WorldView()::drawAirports);
    loadPluginClasses();
    Inject.getInstance().addThread(ControlScheduler.getInstance());
    Inject.getInstance().addThread(new SubscribeThread());
    Inject.getInstance().addThread(new Spatial());
    Inject.getInstance().addThread(Audio.getInstance());
    Inject.getInstance().addThread(StateMachine.getInstance());
    if (Constants.ENABLE_JOYSTICK) Inject.getInstance().addThread(joystickinput.getInstance());
    if (Constants.READ_DATA_FROM_SIM) Inject.getInstance().addThread(new SIMReceiveThread());

    Inject.getInstance().startThreads();
    initialize();
  }

  @SuppressWarnings("unused")
  public void initialize() {

    Inject.getInstance().runStartupTasks();
    println(COPYRIGHTPROMPT);
    loadFonts();
    delay(1000);
    loop();
  }

  private void loadFonts() {
    textMode(MODEL);
    PFont smallFont = loadFont("Calibri-12.vlw");
    mediumFont = loadFont("Calibri-16.vlw");
    groundFont = loadFont("Calibri-80.vlw");
    textFont(groundFont);
  }

  public void draw() {
    distanceToSelectedAirport =
        sqrt(
            pow(latToXCoord(selectedAirport.getLat()) - camX, 2)
                + pow(lonToYCoord(selectedAirport.getLon()) - camY, 2)
                + pow(selectedAirport.alt - camZ, 2));
    Inject.getInstance().runDrawLoopTasks();
    camera();
    boolean fullscreenMode = false;
    if (fullscreenMode)
      perspective(FOVY, (float) displayWidth / (float) displayHeight, 1, 1000000000);
    else perspective(FOVY, (float) width / (float) height, 1, 1000000000);

    new WorldOverlay().drawOverlay();
  }

  public void mousePressed() {
    if (mouseButton == LEFT) {

      dragLEFT = true;
    }
    if (mouseButton == CENTER) {

      dragMIDDLE = true;
    }
    if (mouseButton == RIGHT) {

      dragRIGHT = true;
    }
  }

  public void mouseReleased() {
    if (mouseButton == LEFT) {

      dragLEFT = false;
    }
    if (mouseButton == CENTER) {

      dragMIDDLE = false;
    }
    if (mouseButton == RIGHT) {
      selectedAirport = navdb.getNearestAirport(x, y);
      dragRIGHT = false;
    }
  }

  private void zoom(int delta) {

    if (delta > 0) {
      zoom = (zoom + zoom * 0.1f); // + 1;
    } else {
      zoom = (zoom - zoom * 0.1f); // + 1;
    }
  }

  public void keyReleased() {

    if ((key == 'z') && (dragMIDDLE)) dragMIDDLE = false;
  }

  public void keyPressed() {
    if (key == 'y') {
      Spatial.resetGyro();
    }

    switch (cameraMode) {
      case FREE:
        if (keyCode == RIGHT) {
          if (selectedAirportIndex < navdb.getNumAirports() - 1) selectedAirportIndex++;
          else selectedAirportIndex = 0;

          selectedAirport = navdb.getAirport(selectedAirportIndex);
          x = latToXCoord(selectedAirport.getLat());
          y = lonToYCoord(selectedAirport.getLon());
          z = selectedAirport.alt;
        }
        if (keyCode == LEFT) {
          if (selectedAirportIndex > 0) selectedAirportIndex--;
          else selectedAirportIndex = navdb.getNumAirports() - 1;

          selectedAirport = navdb.getAirport(selectedAirportIndex);
          x = latToXCoord(selectedAirport.getLat());
          y = lonToYCoord(selectedAirport.getLon());
          z = selectedAirport.alt;
        }
        break;
      case AIRPORT:
        if (keyCode == RIGHT) {
          if (selectedAircraftIndex < World.getInstance().getAircraft().size() - 1)
            selectedAircraftIndex++;
          else selectedAircraftIndex = 0;

          selectedAircraft = World.getInstance().getAircraft().get(selectedAircraftIndex);
          x = latToXCoord(selectedAircraft.getLat());
          y = lonToYCoord(selectedAircraft.getLon());
          z = (float) selectedAircraft.getAltitude();
        }
        if (keyCode == LEFT) {
          if (selectedAircraftIndex > 0) selectedAircraftIndex--;
          else selectedAircraftIndex = World.getInstance().getAircraft().size() - 1;

          selectedAircraft = World.getInstance().getAircraft().get(selectedAircraftIndex);
          x = latToXCoord(selectedAircraft.getLat());
          y = lonToYCoord(selectedAircraft.getLon());
          z = (float) selectedAircraft.getAltitude();
        }
        break;
    }
    if ((key == 'c')) {
      if (cameraMode == CamMode.FREE) {
        cameraMode = CamMode.AIRPORT;
        saveX = x;
        saveY = y;
        saveZoom = zoom;
        saveTheta = theta;
        savePhi = phi;
        x = latToXCoord(selectedAirport.getLat());
        y = lonToYCoord(selectedAirport.getLon());
        z = selectedAirport.alt;
      } else if (cameraMode == CamMode.AIRPORT) {
        cameraMode = CamMode.AIRCRAFT;
        zoom = saveZoom;
        theta = saveTheta;
        phi = savePhi;
        x = latToXCoord(selectedAircraft.getLat());
        y = lonToYCoord(selectedAircraft.getLon());
        z = (float) selectedAircraft.getAltitude();
        selectedAirport = navdb.getNearestAirport(x, y);
      } else if (cameraMode == CamMode.AIRCRAFT) {
        cameraMode = CamMode.FREE;
        x = saveX;
        y = saveY;
        z = 0;
        zoom = saveZoom;
        theta = saveTheta;
        phi = savePhi;
      }
    }

    if ((key == 'z') && (!dragMIDDLE)) {
      dragMIDDLE = true;
    }
  }

  public void mouseWheel(MouseEvent event) {
    zoom(event.getCount());
  }
}
