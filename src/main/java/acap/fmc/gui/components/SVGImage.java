/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.svg.SVGDocumentLoaderEvent;
import org.apache.batik.swing.svg.SVGDocumentLoaderListener;

/** Created by tjnickerson on 1/20/17. */
public class SVGImage extends JSVGCanvas {

  private SVGImage self;

  public SVGImage(String url) {
    super();

    //        this.setSize(new Dimension(WIDTH, HEIGHT));
    this.self = this;

    this.addSVGDocumentLoaderListener(
        new SVGDocumentLoaderListener() {
          @Override
          public void documentLoadingStarted(SVGDocumentLoaderEvent e) {}

          @Override
          public void documentLoadingCompleted(SVGDocumentLoaderEvent e) {
            e.getSVGDocument().getRootElement().setAttributeNS(null, "width", "50px");
          }

          @Override
          public void documentLoadingCancelled(SVGDocumentLoaderEvent e) {}

          @Override
          public void documentLoadingFailed(SVGDocumentLoaderEvent e) {}
        });

    this.setURI(ClassLoader.getSystemClassLoader().getResource(url).getPath());
  }
}
