/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.DocumentFilter;

/** Created by tjnickerson on 1/17/17. */
public class TextArea extends JTextArea {

  private int maxLength;

  public TextArea(int rows, int columns, int maxLength) {
    super(rows, columns);

    this.maxLength = maxLength;

    this.setup();
  }

  public void setup() {
    DefaultStyledDocument doc = new DefaultStyledDocument();
    doc.setDocumentFilter(new DocumentSizeFilter(this.maxLength));
    doc.addDocumentListener(
        new DocumentListener() {
          @Override
          public void changedUpdate(DocumentEvent e) {}

          @Override
          public void insertUpdate(DocumentEvent e) {}

          @Override
          public void removeUpdate(DocumentEvent e) {}
        });

    this.setDocument(doc);
  }

  /**
   * DocumentSizeFilter from
   * http://docs.oracle.com/javase/tutorial/uiswing/examples/components/TextComponentDemoProject/src/components/DocumentSizeFilter.java
   */
  class DocumentSizeFilter extends DocumentFilter {
    int maxCharacters;
    boolean DEBUG = false;

    public DocumentSizeFilter(int maxChars) {
      maxCharacters = maxChars;
    }

    public void insertString(FilterBypass fb, int offs, String str, AttributeSet a)
        throws BadLocationException {
      if (DEBUG) {
        System.out.println("in DocumentSizeFilter's insertString method");
      }

      //This rejects the entire insertion if it would make
      //the contents too long. Another option would be
      //to truncate the inserted string so the contents
      //would be exactly maxCharacters in length.
      if ((fb.getDocument().getLength() + str.length()) <= maxCharacters)
        super.insertString(fb, offs, str, a);
      //            else
      //                Toolkit.getDefaultToolkit().beep();
    }

    public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
        throws BadLocationException {
      if (DEBUG) {
        System.out.println("in DocumentSizeFilter's replace method");
      }
      //This rejects the entire replacement if it would make
      //the contents too long. Another option would be
      //to truncate the replacement string so the contents
      //would be exactly maxCharacters in length.
      if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters)
        super.replace(fb, offs, length, str, a);
      //            else
      //                Toolkit.getDefaultToolkit().beep();
    }
  }
}
