/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.ui.tabs.ITabBarUI;
import acap.fmc.gui.ui.tabs.TabBarPrimaryUI;
import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/18/16. */
public class TabBar extends JPanel implements ActionListener {

  JPanel cards;
  ArrayList<FlatButton> tabs;
  JPanel tabsPanel;

  ITabBarUI ui;

  public TabBar() {
    this(new TabBarPrimaryUI());
  }

  public TabBar(ITabBarUI ui) {
    this.tabs = new ArrayList<>();
    this.ui = ui;

    this.setup();
  }

  private void setup() {
    this.setLayout(new BorderLayout());
    this.setBackground(Palette.GRAY_1);
    this.setOpaque(true);

    this.tabsPanel = new JPanel();
    this.tabsPanel.setBackground(this.ui.tabBackground());
    this.tabsPanel.setLayout(new BoxLayout(tabsPanel, BoxLayout.X_AXIS));

    this.cards = new JPanel(new CardLayout());
    this.cards.setOpaque(false);

    this.addComponents();
  }

  private void deactivateButton(FlatButton button) {
    button.setActive(false);
  }

  private void addComponents() {
    this.add(this.tabsPanel, BorderLayout.NORTH);
    this.add(this.cards, BorderLayout.CENTER);
  }

  private boolean containerContainsTabs(Container container) {
    for (Component c : container.getComponents()) {
      if (c instanceof TabBar) {
        return true;
      } else if (c instanceof Container) {
        return containerContainsTabs((Container) c);
      }
    }
    return false;
  }

  public void addTab(String text, Component component) {
    this.addTab(text, component, false);
  }

  public void addTab(String text, Component component, boolean noPadding) {
    // If this is the first tab added, it should be active
    FlatButton newTab =
        new FlatButton(text, this.ui.tabFontSize(), this.ui.tabSelected(), tabs.size() == 0);
    newTab.setBackground(this.ui.tabBackground());
    newTab.setBorder(this.ui.tabPadding());
    newTab.addActionListener(this);
    newTab.disableEffects(true);

    if (component instanceof JComponent && !containerContainsTabs((JComponent) component) && !noPadding) {
      ((JComponent) component).setBorder(new EmptyBorder(0, 25, 0, 25));
    }

    this.cards.add(component, text);
    this.tabs.add(newTab);
    this.tabsPanel.add(newTab);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String button_text = ((JButton) e.getSource()).getActionCommand();
    CardLayout cl = (CardLayout) (cards.getLayout());

    this.tabs.forEach(this::deactivateButton);
    ((FlatButton) e.getSource()).setActive(true);

    cl.show(cards, button_text);
  }
}
