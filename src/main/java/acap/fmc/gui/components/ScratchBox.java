/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.MainWindow;
import acap.fmc.gui.utilities.FontLoader;
import acap.fmc.gui.utilities.Palette;
import acap.fmc.state.ModeControls;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

/** Created by tjnickerson on 1/6/17. */
public class ScratchBox extends JPanel {
  private static final Color BASE_COLOR = Palette.GRAY_4;

  private boolean active;
  private float alignment;
  private boolean enabled;
  private int maxLength;
  private String currentText;
  private String control;
  private JLabel label;
  private TextArea displayArea;

  public ScratchBox(String label, float alignment, int maxLength) {
    this(label, false, alignment, maxLength, true);
  }

  public ScratchBox(String control, int maxLength) {
    this(control, Component.LEFT_ALIGNMENT, maxLength);
  }

  public ScratchBox(String control) {
    this(control, false, Component.LEFT_ALIGNMENT, 5000, true);
  }

  public ScratchBox(
      String control, boolean isActive, float alignment, int maxLength, boolean enabled) {
    super(new BorderLayout());

    this.active = isActive;
    this.alignment = alignment;
    this.enabled = enabled;
    this.control = control;
    this.maxLength = maxLength;

    this.setup();
    this.addComponents();
    this.setEnabled(this.enabled);

    if (this.enabled) this.setActive(this.active);
  }

  public boolean isActive() {
    return this.active;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;

    if (this.enabled) {
      this.label.setForeground(Color.white);
    } else {
      this.label.setForeground(Palette.GRAY_5);
    }

    this.displayArea.setEnabled(this.enabled);
  }

  public void setActive(boolean active) {
    Color highlight = this.BASE_COLOR;

    if (active) {
      highlight = Palette.RED;
    }

    this.setBorder(new LineBorder(highlight, ((LineBorder) this.getBorder()).getThickness()));

    this.active = active;
    this.label.setBackground(highlight);
    this.label.setBorder(
        new CompoundBorder(
            new MatteBorder(1, 0, 0, 0, highlight), new EmptyBorder(10, 10, 10, 10)));
  }

  private void setup() {
    this.setBorder(new LineBorder(this.BASE_COLOR, 1));
    this.setOpaque(false);

    this.label = new JLabel();
    this.displayArea = new TextArea(50, 2, this.maxLength);

    this.displayArea.setBorder(new EmptyBorder(10, 10, 10, 10));
    this.displayArea.setWrapStyleWord(true);
    this.displayArea.setLineWrap(true);
    this.displayArea.setForeground(Color.white);
    this.displayArea.setCaretColor(Color.white);
    this.displayArea.setAlignmentX(this.alignment);
    this.displayArea.setFont(FontLoader.getFont(FontLoader.NOTO_MONO, 16f));
    this.displayArea.setOpaque(false);
    this.displayArea.addFocusListener(
        new FocusListener() {
          @Override
          public void focusGained(FocusEvent e) {
            setActive(true);
            currentText = ((JTextArea) e.getSource()).getText();
          }

          @Override
          public void focusLost(FocusEvent e) {
            setActive(false);
            ((JTextArea) e.getSource()).setText(currentText);
          }
        });

    this.displayArea.addKeyListener(
        new KeyListener() {
          @Override
          public void keyTyped(KeyEvent e) {}

          @Override
          public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
              e.consume();
              currentText = ((JTextArea) e.getSource()).getText().trim();
              ModeControls.getInstance().updateControl(control, currentText);
              MainWindow.getInstance().getFrame().requestFocusInWindow();
            }
          }

          @Override
          public void keyReleased(KeyEvent e) {}
        });

    this.label.setBackground(this.BASE_COLOR);
    this.label.setCursor(new Cursor(Cursor.HAND_CURSOR));
    this.label.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_BOLD, 16f));
    this.label.setForeground(Color.white);
    this.label.setOpaque(true);
    this.label.setText(this.control);
    this.label.addMouseListener(
        new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            setEnabled(!enabled);
          }
        });
  }

  private void addComponents() {
    this.add(this.displayArea, BorderLayout.CENTER);
    this.add(this.label, BorderLayout.SOUTH);
  }
}
