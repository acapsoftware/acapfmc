/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.utilities.FontSizes;
import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/18/16. */
public class ProgressCollapsible extends JPanel {

  private static final int GRID_VGAP = 10;

  private int rows;
  private String title;
  private JPanel panel;

  private ArrayList<ProgressBar> progressBars;

  public ProgressCollapsible(String title) {
    this.title = title;

    this.panel = new JPanel(new GridLayout(0, 1, 0, GRID_VGAP));
    this.progressBars = new ArrayList<>();

    this.setup();
  }

  private void setup() {
    this.setLayout(new BorderLayout());
    this.setOpaque(false);

    this.panel.setBackground(Palette.GRAY_2);
    this.panel.setBorder(new EmptyBorder(15, 15, 15, 15));
    this.panel.setVisible(false);

    this.addComponents();
  }

  private void addComponents() {
    CollapsibleTitle titleBar = new CollapsibleTitle(this.title);
    titleBar.setProgress(50);
    titleBar.addMouseListener(
        new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            if (panel.isVisible()) {
              panel.setVisible(false);
              titleBar.setExpanded(false);
            } else {
              panel.setVisible(true);
              titleBar.setExpanded(true);
            }
          }
        });

    this.add(titleBar, BorderLayout.NORTH);
    this.add(this.panel, BorderLayout.CENTER);
  }

  public void addProgressBar(ProgressBar pb) {
    this.progressBars.add(pb);
    this.rows++;
    this.panel.setLayout(new GridLayout(this.rows, 1, 0, GRID_VGAP));
    this.panel.add(pb);
  }

  class CollapsibleTitle extends ProgressBar {

    private boolean expanded = false;

    public CollapsibleTitle(String title) {
      super(
          title,
          ProgressBar.PADDING_LARGE,
          FontSizes.MEDIUM,
          new SwingWorker<Object, Object>() {
            @Override
            protected Object doInBackground() throws Exception {
              return null;
            }
          });

      this.setBackground(Palette.GRAY_3);
      this.setCursor(new Cursor(Cursor.HAND_CURSOR));
      this.setHorizontalAlignment(SwingConstants.LEFT);
    }

    public void paintTriangle(
        Graphics g, int x, int y, int size, int direction, boolean isEnabled) {
      Color oldColor = g.getColor();
      int mid, i, j;

      j = 0;
      size = Math.max(size, 2);
      mid = (size / 2) - 1;

      g.translate(x, y);

      g.setColor(Color.white);

      switch (direction) {
        case NORTH:
          for (i = 0; i < size; i++) {
            g.drawLine(mid - i, i, mid + i, i);
          }
          if (!isEnabled) {
            g.setColor(Color.white);
            g.drawLine(mid - i + 2, i, mid + i, i);
          }
          break;
        case SOUTH:
          if (!isEnabled) {
            g.translate(1, 1);
            g.setColor(Color.white);
            for (i = size - 1; i >= 0; i--) {
              g.drawLine(mid - i, j, mid + i, j);
              j++;
            }
            g.translate(-1, -1);
            g.setColor(Color.white);
          }

          j = 0;
          for (i = size - 1; i >= 0; i--) {
            g.drawLine(mid - i, j, mid + i, j);
            j++;
          }
          break;
      }
      g.translate(-x, -y);
      g.setColor(oldColor);
    }

    @Override
    public void paintComponent(Graphics g) {
      super.paintComponent(g);

      int direction = SwingConstants.SOUTH;

      // Reverses direction if collapsible is expanded
      if (expanded) {
        direction = SwingConstants.NORTH;
      }

      // Paint the dropdown triangle
      paintTriangle(g, this.getWidth() - 20, (this.getHeight() / 2) - 1, 5, direction, true);
    }

    public void setExpanded(boolean expanded) {
      this.expanded = expanded;
      this.revalidate();
      this.repaint();
    }
  }
}
