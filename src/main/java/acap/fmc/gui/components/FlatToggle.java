/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** Created by tjnickerson on 12/3/16. */
public class FlatToggle extends FlatButton implements ActionListener {

  public FlatToggle(String name, float fontSize, Color activeColor, boolean isActive) {
    super(name, fontSize, activeColor, isActive);
  }

  public FlatToggle(String title, boolean isActive) {
    super(title, isActive);
  }

  public FlatToggle(String title) {
    super(title);
  }

  @Override
  protected void setup() {
    super.setup();

    this.disableEffects(true);
    this.addActionListener(this);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    this.setActive(!this.isActive());
  }
}
