/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.ui.CustomCombobxUI;
import acap.fmc.gui.utilities.FontLoader;
import acap.world.controller.IListController;
import java.awt.*;
import javax.swing.*;

/** Created by tjnickerson on 11/16/16. */
public class FlatCombo extends JComboBox {

  public FlatCombo(IListController controller) {
    super();

    controller.execute(this);

    setup();
  }

  public void setup() {
    Font font = FontLoader.getFont(FontLoader.NOTO_SANS_REGULAR, 14f);

    if (font != null) {
      this.setFont(font);
    }

    this.setUI(new CustomCombobxUI());
  }
}
