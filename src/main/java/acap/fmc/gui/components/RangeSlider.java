/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.ui.RangeSliderUI;
import javax.swing.*;

/**
 * Created by tjnickerson on 12/1/16. Class based on code found at
 * https://github.com/ernieyu/Swing-range-slider
 */
public class RangeSlider extends JSlider {

  public RangeSlider(int min, int max) {
    super(min, max);
    initSlider();
  }

  private void initSlider() {
    this.setOrientation(HORIZONTAL);
  }

  @Override
  public void updateUI() {
    setUI(new RangeSliderUI(this));
    updateLabelUIs();
  }

  /** Returns the lower value in the range. */
  @Override
  public int getValue() {
    return super.getValue();
  }

  /** Sets the lower value in the range. */
  @Override
  public void setValue(int value) {
    int oldValue = getValue();
    if (oldValue == value) {
      return;
    }

    // Compute new value and extent to maintain upper value.
    int oldExtent = getExtent();
    int newValue = Math.min(Math.max(getMinimum(), value), oldValue + oldExtent);
    int newExtent = oldExtent + oldValue - newValue;

    // Set new value and extent, and fire a single change event.
    getModel()
        .setRangeProperties(newValue, newExtent, getMinimum(), getMaximum(), getValueIsAdjusting());
  }

  /** Returns the upper value in the range. */
  public int getUpperValue() {
    return getValue() + getExtent();
  }

  /** Sets the upper value in the range. */
  public void setUpperValue(int value) {
    // Compute new extent.
    int lowerValue = getValue();
    int newExtent = Math.min(Math.max(0, value - lowerValue), getMaximum() - lowerValue);

    // Set extent to set upper value.
    setExtent(newExtent);
  }
}
