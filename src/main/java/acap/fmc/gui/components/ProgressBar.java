/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.utilities.FontLoader;
import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/20/16. */
public class ProgressBar extends JLabel {

  public static final EmptyBorder PADDING_SMALL = new EmptyBorder(10, 15, 10, 15);
  public static final EmptyBorder PADDING_LARGE = new EmptyBorder(15, 15, 15, 15);

  private int progress = 0;
  private String completedText;

  private SwingWorker<Object, Object> worker;

  public ProgressBar(
      String text, EmptyBorder padding, Float fontSize, SwingWorker<Object, Object> worker) {
    this(text, text, padding, fontSize, worker);
  }

  public ProgressBar(
      String text,
      String completedText,
      EmptyBorder padding,
      Float fontSize,
      SwingWorker<Object, Object> worker) {
    super(text);

    this.completedText = completedText;
    this.worker = worker;

    this.setBackground(Palette.GRAY_4);
    this.setBorder(padding);
    this.setForeground(Color.white);
    this.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_BOLD, fontSize));

    this.worker.addPropertyChangeListener(
            evt -> {
              if ("progress".equals(evt.getPropertyName())) {
                int progress = new Integer((Integer) evt.getNewValue());
                if (progress == 100) {
                  setText(completedText);
                }
                setProgress(progress);
              }
            });
  }

  public void start() {
    this.worker.execute();
  }

  @Override
  public void paintComponent(Graphics g) {
    int progressWidth = (int) (this.getWidth() * ((float) (this.progress) / 100));

    // Fills in the main background
    g.setColor(this.getBackground());
    g.fillRect(0, 0, this.getWidth(), this.getHeight());

    // Paints the background progress
    if (progressWidth == this.getWidth()) {
      g.setColor(Palette.GREEN);
    } else {
      g.setColor(Palette.GRAY_5);
    }

    g.fillRect(0, 0, progressWidth, this.getHeight());

    super.paintComponent(g);
  }

  public void setProgress(int progress) {

    if (progress > 100) {
      progress = 100;
    } else if (progress < 0) {
      progress = 0;
    }

    this.progress = progress;
    this.revalidate();
    this.repaint();
  }
}
