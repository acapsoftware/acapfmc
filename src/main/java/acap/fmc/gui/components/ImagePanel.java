/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import java.awt.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/** Created by tjnickerson on 11/17/16. */
public class ImagePanel extends JPanel {

  Image image;
  int w;
  int h;

  public ImagePanel(String filename, int width, int height) {

    try {
      this.image =
          ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/" + filename));
    } catch (IOException e) {
      e.printStackTrace();
    }

    this.w = width;
    this.h = height;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    Toolkit t = Toolkit.getDefaultToolkit();
    g.drawImage(this.image, 0, 0, this);
  }
}
