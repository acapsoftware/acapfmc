/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.utilities.FontLoader;
import acap.fmc.gui.utilities.FontSizes;
import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/16/16. */
public class FlatButton extends JButton {

  private Color activeColor;
  private Color hoverColor;
  private Color pressedColor;

  private boolean isActive;
  private boolean disableEffects; // Disables effects like hover/depressed colors
  private float fontSize;

  public FlatButton(String name, boolean isActive) {
    this(name, FontSizes.MEDIUM, Palette.RED, isActive);
  }

  public FlatButton(String name) {
    this(name, false);
  }

  public FlatButton(Icon icon) {
    super(icon);
    super.setContentAreaFilled(false);
    this.disableEffects = false;

    this.setup();
  }

  public FlatButton(String name, float fontSize, Color activeColor, boolean isActive) {
    super(name);
    super.setContentAreaFilled(false);

    this.activeColor = activeColor;
    this.disableEffects = false;
    this.fontSize = fontSize;

    this.setActive(isActive);
    this.setup();
  }

  public void disableEffects(boolean disable) {
    this.disableEffects = disable;
    this.updateSecondaryColors();
  }

  protected void setup() {
    Font font = FontLoader.getFont(FontLoader.NOTO_SANS_BOLD, fontSize);

    if (font != null) {
      this.setFont(font);
    }

    this.setBackground(Palette.GRAY_4);
    this.setBorder(new EmptyBorder(15, 15, 15, 15));
    this.setBorderPainted(false);
    this.setForeground(Color.WHITE);
    this.setFocusPainted(false);
    this.setHorizontalAlignment(SwingConstants.CENTER);
    this.setCursor(new Cursor(Cursor.HAND_CURSOR));
  }

  public boolean isActive() {
    return this.isActive;
  }

  public void setActive(boolean active) {
    this.isActive = active;
    this.repaint();
  }

  private void updateSecondaryColors() {
    if (this.disableEffects) {
      this.hoverColor = this.getBackground();
      this.pressedColor = this.getBackground();
      ;
    } else {
      this.hoverColor = this.getBackground().brighter();
      this.pressedColor = this.getBackground().darker();
    }
  }

  @Override
  public void setBackground(Color color) {
    super.setBackground(color);
    this.updateSecondaryColors();
  }

  public Color getPressedColor() {
    return this.pressedColor;
  }

  public Color getHoverColor() {
    return this.hoverColor;
  }

  public Color getActiveColor() {
    return this.activeColor;
  }

  @Override
  protected void paintComponent(Graphics g) {
    if (this.isActive) {
      g.setColor(this.getActiveColor());
      g.fillRect(0, 0, getWidth(), getHeight());
    } else {
      if (getModel().isPressed()) {
        g.setColor(this.getPressedColor());
        g.fillRect(0, 0, getWidth(), getHeight());
      } else if (getModel().isRollover()) {
        g.setColor(this.getHoverColor());
        g.fillRect(0, 0, getWidth(), getHeight());
      } else {
        g.setColor(this.getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());
      }
    }

    super.paintComponent(g);
  }

  @Override
  public void setContentAreaFilled(boolean b) {}

  @Override
  public void setOpaque(boolean isOpaque) {}
}
