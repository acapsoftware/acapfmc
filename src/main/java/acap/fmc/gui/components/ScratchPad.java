/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.components;

import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 1/6/17. */
public class ScratchPad extends JTextField {

  private static ScratchPad instance;

  public ScratchPad() {
    super(50);

    this.setup();
  }

  public static ScratchPad getInstance() {
    if (instance == null) {
      instance = new ScratchPad();
    }

    return instance;
  }

  private void setup() {
    this.setBackground(Palette.GRAY_3);
    this.setBorder(new EmptyBorder(10, 10, 10, 10));
    //        this.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_REGULAR, 14f));
    this.setForeground(Color.white);
    this.setCaretColor(Palette.GRAY_5);
  }
}
