/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.windows;

import static javax.swing.BorderFactory.createEmptyBorder;

import acap.fmc.Constants;
import acap.fmc.controls.ControlScheduler;
import acap.fmc.gui.components.FlatButton;
import acap.fmc.gui.components.ProgressBar;
import acap.fmc.gui.components.ProgressCollapsible;
import acap.fmc.gui.containers.StackPanel;
import acap.fmc.gui.ui.CustomScrollBarUI;
import acap.fmc.gui.utilities.FontLoader;
import acap.fmc.gui.utilities.FontSizes;
import acap.fmc.gui.utilities.Palette;
import acap.fmc.gui.utilities.PopupManager;
import acap.fmc.procedure.MsgPublisher;
import acap.fmc.procedure.Task;
import acap.fmc.procedure.TaskGroup;
import acap.fmc.procedure.TaskQueue;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tjnickerson on 11/20/16.
 */
public class TaskPane extends JPanel {

    private static final int ICON_WIDTH = 15;
    private static final int ICON_HEIGHT = 15;
    private static JLabel stateLabel;

    protected final Logger log = LoggerFactory.getLogger(getClass());
    private static TaskPane instance;

    boolean emptyQueue = true;
    StackPanel stack;

    private static Map<Task, ProgressBar> taskProgressBarMap;

    public TaskPane() {
        super();

        this.setup();
        this.addComponents();

        taskProgressBarMap = new HashMap<>();

        TaskQueue.getInstance()
                .setAddedCallback(
                        o -> {
                            if (o.getClass() == TaskGroup.class) {
                                addTaskGroup((TaskGroup) o);
                            }
                            return null;
                        });
    }

    public static TaskPane getInstance() {
        if (instance == null)
            instance = new TaskPane();
        return instance;
    }

    public static Map<Task, ProgressBar> getTaskProgressMap() {
        return taskProgressBarMap;
    }

    private void setup() {
        this.setLayout(new GridLayout(1, 1));
        this.setOpaque(false);

        stateLabel = new JLabel();
        stateLabel.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_BOLD, 16));
        stateLabel.setHorizontalAlignment(SwingConstants.CENTER);
        stateLabel.setForeground(Color.white);
        stateLabel.setText("No Current State");
        stateLabel.setOpaque(true);
        stateLabel.setBorder(new EmptyBorder(25, 25, 25, 25));
        stateLabel.setBackground(Palette.GRAY_1);
    }

    private FlatButton makeButton(Icon icon, MouseListener listener) {
        FlatButton button = new FlatButton(icon);
        button.setBorder(new EmptyBorder(20, 20, 20, 20));
        button.addMouseListener(listener);
        return button;
    }

    public static void setStateDisplayText(String stateText) {
        if (stateLabel != null)
            stateLabel.setText("Current State: " + stateText);
    }

    public void addComponents() {
        JPanel panel = new JPanel(new BorderLayout());

        this.stack = new StackPanel();

        JScrollPane scrollPane = new JScrollPane(this.stack);
        scrollPane.setBorder(createEmptyBorder());
        scrollPane.setViewportView(this.stack);
        scrollPane.getViewport().setOpaque(false);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.getVerticalScrollBar().setUI(new CustomScrollBarUI(Palette.GRAY_1));

        panel.add(stateLabel, BorderLayout.NORTH);
        panel.add(scrollPane, BorderLayout.CENTER);

        JPanel controlPanel = new JPanel(new GridLayout(1, 3));
        controlPanel.setBackground(Palette.GRAY_1);

        controlPanel.add(this.makeButton(new PlayIcon(), new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ControlScheduler.PAUSED = false;
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        }));

        controlPanel.add(this.makeButton(new PauseIcon(), new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ControlScheduler.getInstance().PAUSED = true;
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        }));

        controlPanel.add(this.makeButton(new StepIcon(), new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ControlScheduler scheduler = ControlScheduler.getInstance();
                if (scheduler.getCurrentTaskGroup() == null) {
                    scheduler.getNextTaskGroup();
                }
                if (scheduler.getCurrentTaskGroup() != null) {
                    if (scheduler.getCurrentTaskGroup().hasNextTask()) {
                        Task task = scheduler.getCurrentTaskGroup().getNextTask();
                        try {
                            System.out.println(taskProgressBarMap.get(task));
                            if (taskProgressBarMap.containsKey(task))
                                taskProgressBarMap.get(task).start();
                            task.run();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        }));

        controlPanel.add(this.makeButton(new AddIcon(), new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PopupManager pm = new PopupManager();
                pm.showInputDialog("Enter a new task command:", () -> {
                    TaskGroup g = new TaskGroup("Custom Input",1);
                    g.addTask(Task.constructFromString(pm.getLastValue()));
                    TaskQueue.getInstance().addTaskGroup(g);
                });
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        }));

        panel.add(controlPanel, BorderLayout.SOUTH);

        this.add(panel);
    }

    private void addTaskGroup(TaskGroup tg) {
        ProgressCollapsible c = new ProgressCollapsible(tg.getTitle());
        for (Task t : tg.getTasks()) {
            ProgressBar newPB = new ProgressBar(
                    t.getTitle(),
                    "Completed!",
                    ProgressBar.PADDING_SMALL,
                    FontSizes.SMALL,
                    new SwingWorker<Object, Object>() {
                        @Override
                        protected Object doInBackground() throws Exception {
                            log.info("SETTING PROGRESS...");
                            System.out.println(t.percentComplete());
                            setProgress(t.percentComplete());
                            while (!t.isComplete()) {
                                setProgress(t.percentComplete());
                                Thread.sleep(100);
                            }
                            return null;
                        }
                    });
            this.taskProgressBarMap.put(t, newPB);
            c.addProgressBar(newPB);
        }

        c.setBorder(new EmptyBorder(emptyQueue ? 15 : 0, 15, 15, 15));
        stack.addComponent(c);
        emptyQueue = false;
    }

    abstract class ControlIcon implements Icon {
        protected int width;
        protected int height;

        ControlIcon() {
            this.width = ICON_WIDTH;
            this.height = ICON_HEIGHT;
        }

        @Override
        public int getIconWidth() {
            return this.width;
        }

        @Override
        public int getIconHeight() {
            return this.height;
        }
    }

    class AddIcon extends ControlIcon {

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            int barWidth = 4;

            Graphics2D g2d = (Graphics2D) g;

            g2d.setColor(Color.WHITE);

            g2d.fillRect(x + ((this.width / 2) - (barWidth / 2)), y, barWidth, this.height);
            g2d.fillRect(x, y + ((this.width / 2) - (barWidth / 2)), this.height, barWidth);
        }
    }

    class StepIcon extends ControlIcon {

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.WHITE);

            int padding = 4;
            int barWidth = (this.width - padding) / 3;

            int triangleX = x + barWidth + padding;
            Point point1 = new Point(triangleX, y);
            Point point2 = new Point(triangleX, y + this.height);
            Point point3 = new Point(triangleX + ((2 * this.width) / 3), y + (this.height / 2));

            int[] xa = {point1.x, point2.x, point3.x};
            int[] ya = {point1.y, point2.y, point3.y};

            g2d.fillRect(x, y, barWidth, this.height);
            g2d.fillPolygon(xa, ya, 3);
        }
    }

    class PauseIcon extends ControlIcon {

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            int padding = 4;
            int barWidth = (this.width - padding) / 2;

            Graphics2D g2d = (Graphics2D) g;

            g2d.setColor(Color.WHITE);

            g2d.fillRect(x, y, barWidth, this.height);
            g2d.fillRect(x + barWidth + padding, y, barWidth, this.height);
        }

    }

    class PlayIcon extends ControlIcon {

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Graphics2D g2d = (Graphics2D) g;

            g2d.setColor(Color.WHITE);

            Point point2 = new Point(x, y + this.height);
            Point point3 = new Point(x + this.width, y + (this.height / 2));

            int[] xa = {x, point2.x, point3.x};
            int[] ya = {y, point2.y, point3.y};

            g2d.fillPolygon(xa, ya, 3);
        }

    }
}
