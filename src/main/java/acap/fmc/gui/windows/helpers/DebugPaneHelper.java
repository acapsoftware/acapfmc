/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.windows.helpers;

import acap.fmc.controls.ComponentController;
import acap.fmc.controls.Devices;
import acap.fmc.controls.SerialMessage;
import acap.fmc.controls.SerialMessage.ControlMessage.component;
import acap.fmc.gui.components.FlatButton;
import acap.fmc.gui.components.FlatToggle;
import acap.fmc.gui.components.RangeSlider;
import acap.fmc.gui.containers.ComponentGroup;
import acap.fmc.gui.utilities.Palette;
import acap.fmc.gui.wrappers.types.RangeSliderWrapper;
import acap.fmc.gui.wrappers.types.SliderWrapper;
import acap.fmc.procedure.MsgPublisher;
import acap.fmc.procedure.Task;
import acap.fmc.procedure.TaskGroup;
import acap.fmc.procedure.TaskQueue;
import java.awt.*;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 12/1/16. */
public class DebugPaneHelper {

  public static volatile boolean enableJoystick = true;

  private static HashMap<String, SliderWrapper> setterMap = new HashMap<>();
  private static HashMap<String, FlatToggle> toggleMap = new HashMap<>();

  private void startSenseUpdater(String device, SliderWrapper slider) {
    (new Thread(new SenseUpdater(device, slider))).start();
  }

  public static HashMap<String, SliderWrapper> getSetterMap() {
    return setterMap;
  }

  public static HashMap<String, FlatToggle> getToggleMap() {
    return toggleMap;
  }

  public static ComponentGroup getGlobalControls() {
    ComponentGroup group = new CGBuilder("Global Setters", null).build();

    SliderWrapper velocitySlider = ComponentHelpers.makeSetSlider("Motor Velocity", Devices.ALL);
    SliderWrapper smoothnessSlider =
        ComponentHelpers.makeSetSlider("Motor Smoothness", Devices.FLAPS);
    FlatToggle joystickToggle = new FlatToggle("Joystick Input");

    FlatButton testButton = new FlatButton("Add Dummy Task");

    testButton.addActionListener(
        e -> {
          Task dummyTask =
              new Task(
                  "Test Task",
                  new ComponentController(
                      Devices.getInstance().getComponentByString(Devices.AILERON)),
                  1);
          TaskGroup parent = new TaskGroup("Test Subtask", 1);
          parent.addTask(dummyTask);
          TaskQueue.getInstance().addTaskGroup(parent);
        });

    joystickToggle.addActionListener(
        e -> {
          FlatToggle source = (FlatToggle) e.getSource();
          enableJoystick =
              !source
                  .isActive(); // we do the not (~) of it because the new isActive boolean hasn't been set yet
        });

    group.addComponent(velocitySlider);
    group.addComponent(smoothnessSlider);
    group.addComponent(joystickToggle);
    group.addComponent(testButton);

    return group;
  }

  public static ComponentGroup getFlapControls() {
    CGBuilder builder = new CGBuilder("Flaps", Devices.FLAPS);
    builder.addSenseSlider();
    builder.addSetSlider();
    builder.addEngageToggle();
    builder.addFlapReleaseToggle();
    return builder.build();
  }

  public static ComponentGroup getRudderControls() {
    CGBuilder builder = new CGBuilder("Rudder", Devices.RUDDER);
    builder.addSenseSlider(-100, 100);
    builder.addRangeSlider(-100, 100);
    builder.addSetSlider(-100, 100);
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  public static ComponentGroup getStabliatorControls() {
    CGBuilder builder = new CGBuilder("Stabilator", Devices.STABILATOR);
    builder.addSenseSlider(-100, 100);
    builder.addRangeSlider(-100, 100);
    builder.addSetSlider(-100, 100);
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  public static ComponentGroup getAileronControls() {
    CGBuilder builder = new CGBuilder("Aileron", Devices.AILERON);
    builder.addSenseSlider(-100, 100);
    builder.addRangeSlider(-100, 100);
    builder.addSetSlider(-100, 100);
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  public static JPanel getPowerToggles() {
    JPanel togglePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 25, 0));
    FlatToggle leftMagToggle = ComponentHelpers.makeToggle("Left Mag", Devices.MAG_LEFT);
    FlatToggle rightMagToggle = ComponentHelpers.makeToggle("Right Mag", Devices.MAG_RIGHT);
    FlatToggle starterToggle = ComponentHelpers.makeToggle("Starter", Devices.STARTER);

    togglePanel.setOpaque(false);
    togglePanel.setBorder(new EmptyBorder(25, 0, 0, 0));

    togglePanel.add(leftMagToggle);
    togglePanel.add(rightMagToggle);
    togglePanel.add(starterToggle);

    toggleMap.put(Devices.MAG_LEFT, leftMagToggle);
    toggleMap.put(Devices.MAG_RIGHT, rightMagToggle);
    toggleMap.put(Devices.STARTER, starterToggle);

    return togglePanel;
  }

  public static ComponentGroup getBrakesControls() {
    CGBuilder builder = new CGBuilder("Brakes", Devices.BRAKES);
    builder.addSenseSlider();
    builder.addRangeSlider();
    builder.addSetSlider();
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  public static ComponentGroup getCarbHeatControls() {
    CGBuilder builder = new CGBuilder("Carb Heat", Devices.CARB_HEAT);
    builder.addSetSlider();
    builder.addRangeSlider();
    return builder.build();
  }

  public static ComponentGroup getThrottleControls() {
    CGBuilder builder = new CGBuilder("Throttle", Devices.THROTTLE);
    builder.addRangeSlider();
    builder.addSetSlider();
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  public static ComponentGroup getMixtureControls() {
    CGBuilder builder = new CGBuilder("Mixture", Devices.MIXTURE);
    builder.addRangeSlider();
    builder.addSetSlider();
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  public static ComponentGroup getPitchTrimControls() {
    CGBuilder builder = new CGBuilder("Pitch Trim", Devices.PITCH_TRIM);
    builder.addSenseSlider(-100, 100);
    builder.addRangeSlider(-100, 100);
    builder.addSetSlider(-100, 100);
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  public static ComponentGroup getRudderTrimControls() {
    CGBuilder builder = new CGBuilder("Rudder Trim", Devices.RUDDERTRIM);
    builder.addSenseSlider(-100, 100);
    builder.addRangeSlider(-100, 100);
    builder.addSetSlider(-100, 100);
    builder.addSetMinButton();
    builder.addEngageToggle();
    builder.addSetMaxButton();
    return builder.build();
  }

  // Component Group Builder
  static class CGBuilder {
    private String deviceKey;
    private ComponentGroup cg;

    private SliderWrapper senseSlider;
    private SliderWrapper setSlider;
    private RangeSliderWrapper rangeSlider;

    private boolean addedSenseSlider;
    private boolean addedSetSlider;
    private boolean addedRangeSlider;
    private boolean addedEngageToggle;
    private boolean addedSetMaxButton;
    private boolean addedSetMinButton;
    private boolean addedFlapReleaseToggle;

    private JPanel buttonsPanel;
    private GridBagConstraints buttonsPanelConstraints;

    public CGBuilder(String title, String deviceKey) {
      this.deviceKey = deviceKey;
      this.setSlider = null;
      this.cg = new ComponentGroup(title);
    }

    public void createButtonPanel() {
      this.buttonsPanel = new JPanel(new GridBagLayout());
      this.buttonsPanel.setOpaque(false);
      this.buttonsPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
      this.buttonsPanelConstraints = new GridBagConstraints();
      this.buttonsPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
      this.buttonsPanelConstraints.gridy = 0;
      this.buttonsPanelConstraints.weighty = 1;
      this.buttonsPanelConstraints.weightx = 1;
      this.buttonsPanelConstraints.insets = new Insets(0, 4, 0, 4);
    }

    public FlatToggle addFlapReleaseToggle() {
      if (!addedFlapReleaseToggle) {
        FlatToggle releaseToggle = new FlatToggle("Flap Release");
        this.buttonsPanelConstraints.gridx = this.buttonsPanel.getComponentCount();
        this.buttonsPanel.add(releaseToggle, this.buttonsPanelConstraints);
        return releaseToggle;
      }

      return null;
    }

    public FlatButton addSetMinButton() {
      if (buttonsPanel == null) this.createButtonPanel();

      if (!addedSetMinButton) {
        FlatButton minButton = new FlatButton("Set Min");
        this.buttonsPanelConstraints.gridx = this.buttonsPanel.getComponentCount();

        if (this.setSlider != null) {
          minButton.addActionListener(
              e -> {
                SerialMessage.ControlMessage.component comp =
                    Devices.getInstance().getComponentByString(deviceKey);
                if (comp != SerialMessage.ControlMessage.component.UNRECOGNIZED) {
                  Devices.getInstance().setControllerMin(comp, setSlider.getValue());
                  if (rangeSlider != null) rangeSlider.setValue(setSlider.getValue());
                }
              });
        }

        this.buttonsPanel.add(minButton, this.buttonsPanelConstraints);
        return minButton;
      }

      return null;
    }

    public FlatButton addSetMaxButton() {
      if (buttonsPanel == null) this.createButtonPanel();

      if (!addedSetMaxButton) {
        FlatButton maxButton = new FlatButton("Set Max");
        this.buttonsPanelConstraints.gridx = this.buttonsPanel.getComponentCount();

        if (this.setSlider != null) {
          maxButton.addActionListener(
              e -> {
                SerialMessage.ControlMessage.component comp =
                    Devices.getInstance().getComponentByString(deviceKey);
                if (comp != SerialMessage.ControlMessage.component.UNRECOGNIZED) {
                  Devices.getInstance().setControllerMax(comp, setSlider.getValue());
                  if (rangeSlider != null) rangeSlider.setUpperValue(setSlider.getValue());
                }
              });
        }

        this.buttonsPanel.add(maxButton, this.buttonsPanelConstraints);
        return maxButton;
      }

      return null;
    }

    public FlatToggle addEngageToggle() {
      if (buttonsPanel == null) this.createButtonPanel();

      if (!addedEngageToggle) {
        FlatToggle engageToggle = new FlatToggle("Engage", false);
        this.buttonsPanelConstraints.gridx = this.buttonsPanel.getComponentCount();
        this.buttonsPanel.add(engageToggle, this.buttonsPanelConstraints);
        return engageToggle;
      }

      return null;
    }

    public SliderWrapper addSenseSlider() {
      return addSenseSlider(0, 100);
    }

    public SliderWrapper addSenseSlider(int sliderMin, int sliderMax) {
      if (!addedSenseSlider) {
        DebugPaneHelper instance = new DebugPaneHelper();
        SliderWrapper senseSlider =
            ComponentHelpers.makeSenseSlider(this.deviceKey, sliderMin, sliderMax);

        this.cg.addComponent(senseSlider);

        if (this.rangeSlider != null) {
          senseSlider.padLeft(true);
        }

        this.addedSenseSlider = true;
        this.senseSlider = senseSlider;

        instance.startSenseUpdater(this.deviceKey, this.senseSlider);
        return this.senseSlider;
      }

      return null;
    }

    public SliderWrapper addSetSlider() {
      return addSetSlider(0, 100);
    }

    public SliderWrapper addSetSlider(int sliderMin, int sliderMax) {
      if (!addedSetSlider) {
        SliderWrapper setSlider =
            ComponentHelpers.makeSetSlider(this.deviceKey, sliderMin, sliderMax);

        if (this.rangeSlider != null) {
          setSlider.padLeft(true);
          this.rangeSlider.addChangeListener(
              e -> {
                RangeSlider source = (RangeSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                  this.setSlider.setMinimum(source.getValue());
                  this.setSlider.setMaximum(source.getUpperValue());
                }
              });
        }

        this.cg.addComponent(setSlider);
        this.setSlider = setSlider;
        this.addedSetSlider = true;

        setterMap.put(deviceKey, this.setSlider);

        return this.setSlider;
      }

      return null;
    }

    public RangeSliderWrapper addRangeSlider() {
      return addRangeSlider(0, 100);
    }

    public RangeSliderWrapper addRangeSlider(int sliderMin, int sliderMax) {
      if (!addedRangeSlider) {
        int min = 0;
        int max = 100;
        RangeSliderWrapper rangeSlider =
            ComponentHelpers.makeRangeSlider(this.deviceKey, min, max, sliderMin, sliderMax);

        if (this.setSlider != null) {
          this.setSlider.padLeft(true);
        }

        if (this.senseSlider != null) {
          this.senseSlider.padLeft(true);
        }

        this.cg.addComponent(rangeSlider);
        this.rangeSlider = rangeSlider;
        this.addedRangeSlider = true;

        return this.rangeSlider;
      }

      return null;
    }

    public ComponentGroup build() {
      this.cg.setBorder(
          BorderFactory.createCompoundBorder(
              new EmptyBorder(15, 0, 0, 0),
              BorderFactory.createMatteBorder(0, 0, 1, 0, Palette.GRAY_2)));

      if (buttonsPanel != null) this.cg.addComponent(buttonsPanel);

      return this.cg;
    }
  }

  static class ComponentHelpers {

    private static int getDefaultValueForDevice(String device) {
      SerialMessage.ControlMessage.component comp =
          Devices.getInstance().getComponentByString(device);
      return (int) (Devices.getInstance().getControllerReading(comp));
    }

    private static boolean getDefaultBooleanForDevice(String device) {
      return false;
    }

    public static FlatToggle makeToggle(String title, String device) {
      return new FlatToggle(title, getDefaultBooleanForDevice(device));
    }

    public static SliderWrapper makeSenseSlider(String device) {
      return makeSenseSlider(device, 0, 100);
    }

    public static SliderWrapper makeSenseSlider(String device, int sliderMin, int sliderMax) {
      return new SliderWrapper(
          "Position Sense", sliderMin, sliderMax, getDefaultValueForDevice(device), false);
    }

    public static SliderWrapper makeSetSlider(String device) {
      return makeSetSlider("Position Set", device, 0, 100);
    }

    public static SliderWrapper makeSetSlider(String device, int sliderMin, int sliderMax) {
      return makeSetSlider("Position Set", device, sliderMin, sliderMax);
    }

    public static SliderWrapper makeSetSlider(String name, String device) {
      return makeSetSlider(name, device, 0, 100);
    }

    public static SliderWrapper makeSetSlider(
        String name, String device, int sliderMin, int sliderMax) {
      return new SliderWrapper(
          name,
          sliderMin,
          sliderMax,
          getDefaultValueForDevice(device),
          e -> {
            JSlider source = (JSlider) e.getSource();
            if (!source.getValueIsAdjusting()) {
              ComponentController control = null;
              try {
                control =
                    new ComponentController(Devices.getInstance().getComponentByString(device));
              } catch (Exception e1) {
                e1.printStackTrace();
              }
              control.setPosition(source.getValue());
              MsgPublisher.getInstance().sendMsg(control.serialize());
            }
          });
    }

    public static RangeSliderWrapper makeRangeSlider(String device, int min, int max) {
      return makeRangeSlider(device, min, max, 0, 100);
    }

    public static RangeSliderWrapper makeRangeSlider(
        String device, int min, int max, int sliderMin, int sliderMax) {
      RangeSliderWrapper rsw =
          new RangeSliderWrapper("Min/Max Limit", sliderMin, sliderMax, min, max);
      rsw.addChangeListener(
          e -> {
            component c = Devices.getInstance().getComponentByString(device);
            RangeSlider source = (RangeSlider) e.getSource();
            if (source.getValueIsAdjusting()) {
              ComponentController control = null;
              try {
                control =
                    new ComponentController(Devices.getInstance().getComponentByString(device));
              } catch (Exception e1) {
                e1.printStackTrace();
              }
              Devices.getInstance().setControllerMax(c, source.getValue());
              Devices.getInstance().setControllerMin(c, source.getUpperValue());
              control.turnOnSetup();
              MsgPublisher.getInstance().sendMsg(control.serialize());
            }
          });
      return rsw;
    }
  }

  class SenseUpdater implements Runnable {

    private String device;
    private SliderWrapper sliderWrapper;

    SenseUpdater(String device, SliderWrapper slider) {
      this.device = device;
      this.sliderWrapper = slider;
    }

    @Override
    public void run() {
      while (true) {
        int value = DebugPaneHelper.ComponentHelpers.getDefaultValueForDevice(device);
        if (value != sliderWrapper.getValue()) {
          sliderWrapper.setValue(value);
        }
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
