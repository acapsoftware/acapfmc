/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.windows;

import acap.fmc.gui.components.FlatToggle;
import acap.fmc.gui.components.ScratchBox;
import acap.fmc.gui.containers.GridPanel;
import java.awt.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/20/16. */
public class ModeControlPanel extends GridPanel {

  public ModeControlPanel() {
    super(8, 5, 5);

    this.setup();
    this.addComponents();
  }

  private void setup() {
    this.setBorder(new EmptyBorder(25, 25, 25, 25));
    this.setOpaque(false);
  }

  public void addComponents() {
    this.add(new FlatToggle("HDG"));
    this.add(new FlatToggle("VS"));
    this.add(new FlatToggle("ALT"));
    this.add(new FlatToggle("ALT"));
    this.add(new FlatToggle("ALT"));

    this.add(new ScratchBox("CRS", Component.CENTER_ALIGNMENT, 3));
    this.add(new ScratchBox("SPD", Component.CENTER_ALIGNMENT, 3));
    this.add(new ScratchBox("HDG", Component.CENTER_ALIGNMENT, 3));
    this.add(new ScratchBox("ALT", Component.CENTER_ALIGNMENT, 5));
    this.add(new ScratchBox("V/S", Component.CENTER_ALIGNMENT, 5));

    GridBagConstraints gbc = new GridBagConstraints();
    gbc.ipady = 25;

    this.setConstraints(gbc);

    this.add(new ScratchBox("Origin"), 2);
    this.add(new ScratchBox("Taxi-Out"), 3);
    this.add(new ScratchBox("Destination"), 2);
    this.add(new ScratchBox("Taxi-In"), 3);
    this.add(new ScratchBox("Route"), 5);
  }
}
