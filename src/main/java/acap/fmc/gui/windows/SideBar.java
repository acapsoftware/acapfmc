/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.windows;

import acap.fmc.gui.components.TabBar;
import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;

/** Created by tjnickerson on 11/14/16. */
public class SideBar extends JPanel {

  public SideBar() {
    super(new BorderLayout());

    this.setup();
  }

  public void setup() {
    this.setBackground(Palette.GRAY_1);

    this.addComponents();
  }

  public void addComponents() {
    TabBar tbar = new TabBar();

    tbar.addTab("Task Queue", TaskPane.getInstance(), true);
    tbar.addTab("Mode Control", new ModeControlPanel());
    tbar.addTab("Displays", DisplaysPane.getInstance());
    tbar.addTab("Debug", new DebugPane());

    this.add(tbar, BorderLayout.CENTER);
  }
}
