/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.windows;

import acap.fmc.gui.components.TabBar;
import acap.fmc.gui.containers.ComponentGroup;
import acap.fmc.gui.containers.StackPanel;
import acap.fmc.gui.ui.CustomScrollBarUI;
import acap.fmc.gui.ui.tabs.TabBarSecondaryUI;
import acap.fmc.gui.utilities.Palette;
import acap.fmc.gui.windows.helpers.DebugPaneHelper;
import acap.fmc.gui.wrappers.types.ComboWrapper;
import acap.world.controller.AirportListController;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/20/16. */
public class DebugPane extends JPanel {

  private static final Color BACKGROUND = Palette.GRAY_1;

  public DebugPane() {
    super(new BorderLayout());

    this.setup();
  }

  private void setup() {
    this.setBackground(BACKGROUND);
    this.addComponents();
  }

  private JScrollPane makeScrollable(JPanel panel) {
    JScrollPane scrollPane = new JScrollPane(panel);
    scrollPane.setViewportView(panel);
    scrollPane.getViewport().setOpaque(false);
    scrollPane.setOpaque(false);
    scrollPane.getVerticalScrollBar().setUnitIncrement(16);
    scrollPane.getVerticalScrollBar().setUI(new CustomScrollBarUI(Palette.GRAY_1));
    scrollPane.getHorizontalScrollBar().setUI(new CustomScrollBarUI(Palette.GRAY_1));

    return scrollPane;
  }

  private JComponent makeFltCtrlTab() {
    StackPanel fltCtrlPanel = new StackPanel();
    fltCtrlPanel.setOpaque(false);
    fltCtrlPanel.setBorder(new EmptyBorder(0, 25, 25, 25));

    fltCtrlPanel.addComponent(DebugPaneHelper.getAileronControls());
    fltCtrlPanel.addComponent(DebugPaneHelper.getStabliatorControls());
    fltCtrlPanel.addComponent(DebugPaneHelper.getRudderControls());
    fltCtrlPanel.addComponent(DebugPaneHelper.getFlapControls());
    fltCtrlPanel.addComponent(DebugPaneHelper.getPitchTrimControls());
    fltCtrlPanel.addComponent(DebugPaneHelper.getRudderTrimControls());

    return this.makeScrollable(fltCtrlPanel);
  }

  private JComponent makeGlobalTab() {
    StackPanel globalPanel = new StackPanel();
    globalPanel.setOpaque(false);
    globalPanel.setBorder(new EmptyBorder(0, 25, 25, 25));

    globalPanel.addComponent(DebugPaneHelper.getGlobalControls());

    return this.makeScrollable(globalPanel);
  }

  private JComponent makePowerTab() {
    StackPanel powerPanel = new StackPanel();
    powerPanel.setOpaque(false);
    powerPanel.setBorder(new EmptyBorder(0, 25, 25, 25));

    powerPanel.addComponent(DebugPaneHelper.getThrottleControls());
    powerPanel.addComponent(DebugPaneHelper.getMixtureControls());
    powerPanel.addComponent(DebugPaneHelper.getCarbHeatControls());
    powerPanel.addComponent(DebugPaneHelper.getBrakesControls());
    powerPanel.addComponent(DebugPaneHelper.getPowerToggles());

    return this.makeScrollable(powerPanel);
  }

  private JComponent makeProgramTab() {
    ComponentGroup cg = new ComponentGroup("Display Settings");
    StackPanel programPanel = new StackPanel();

    cg.addComponent(new ComboWrapper("Display Airport", new AirportListController()));
    cg.setBorder(new EmptyBorder(15, 0, 0, 0));

    programPanel.setBorder(new EmptyBorder(0, 25, 25, 25));
    programPanel.addComponent(cg);
    programPanel.setOpaque(false);

    return this.makeScrollable(programPanel);
  }

  public void addComponents() {
    TabBar tbar = new TabBar(new TabBarSecondaryUI());
    tbar.addTab("Global Controls", this.makeGlobalTab());
    tbar.addTab("Flight Controls", this.makeFltCtrlTab());
    tbar.addTab("Power", this.makePowerTab());
    tbar.addTab("Program", this.makeProgramTab());
    this.add(tbar);
  }
}
