/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.windows;

import acap.fmc.gui.components.FlatToggle;
import acap.fmc.gui.components.TabBar;
import acap.fmc.gui.containers.GridPanel;
import acap.fmc.gui.processing.apps.Engine;
import acap.fmc.gui.processing.apps.FlightControls;
import acap.fmc.gui.processing.apps.PrimaryFlightDisplay;
import acap.fmc.gui.ui.tabs.TabBarSecondaryUI;
import acap.fmc.gui.utilities.FontSizes;
import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class DisplaysPane extends JPanel {

  public static final String AP_DISCONNECT = "!AP_DISCONNECT";
  public static final String AP_MASTER = "!AP_MASTER";
  public static final String LNAV = "!LNAV";
  public static final String VNAV = "!VNAV";
  public static final String AT = "!A/T";
  public static final String CMD = "!CMD";
  public static final String HDG = "HDG";
  public static final String VS = "V/S";
  public static final String SPD = "SPD";
  public static final String STATE_DISPLAY = "!STATE_DISPLAY";
  public static final String YD = "!YD";
  public static final String ALT = "ALT";
  public static final String TOGA = "!TO/GA";

  private static FlatToggle apDisconnect;
  private static FlatToggle stateDisplay;
  private static FlatToggle apMaster;
  private static FlatToggle lnav;
  private static FlatToggle vnav;
  private static FlatToggle at;
  private static FlatToggle cmd;
  private static FlatToggle hdg;
  private static FlatToggle vs;
  private static FlatToggle spd;
  private static FlatToggle yd;
  private static FlatToggle alt;
  private static FlatToggle toga;

  private static HashMap<String, FlatToggle> toggleMap;
  private static DisplaysPane instance;

  public DisplaysPane() {
    super(new BorderLayout());

    this.setup();
    this.addComponents();

    toggleMap = new HashMap<>();
    toggleMap.put(AP_MASTER, apMaster);
    toggleMap.put(AP_DISCONNECT, apDisconnect);
    toggleMap.put(LNAV, lnav);
    toggleMap.put(VNAV, vnav);
    toggleMap.put(AT, at);
    toggleMap.put(CMD, cmd);
    toggleMap.put(HDG, hdg);
    toggleMap.put(VS, vs);
    toggleMap.put(SPD, spd);
    toggleMap.put(STATE_DISPLAY, stateDisplay);
    toggleMap.put(YD, yd);
    toggleMap.put(ALT, alt);
    toggleMap.put(TOGA, toga);
  }

  public void setup() {
    this.setBackground(Color.blue);
  }

  public static DisplaysPane getInstance() {
    if (instance == null)
      instance = new DisplaysPane();
    return instance;
  }

  public static void setDisplayText(String key, String value) {
    FlatToggle toggle = toggleMap.get(key);
    if (toggle != null) {
      if (key.charAt(0) != '!')
        toggle.setText(key + " " + value);
      else
        toggle.setText(value);
    }
  }

  private JPanel formatPanel(JPanel panel) {
    JPanel wrapper = new JPanel();
    wrapper.setLayout(new GridBagLayout());
    wrapper.setBackground(Color.black);
    wrapper.add(panel, new GridBagConstraints());
    return wrapper;
  }

  public FlatToggle makeToggle(String title, boolean large, MouseListener listener) {
    FlatToggle toggle;
    EmptyBorder padding;

    if (large) {
      toggle = new FlatToggle(title, FontSizes.LARGE, Palette.RED, false);
      padding = new EmptyBorder(10, 20, 10, 20);
    } else {
      toggle = new FlatToggle(title, FontSizes.MEDIUM, Palette.RED, false);
      padding = new EmptyBorder(10, 20, 10, 20);
    }

    toggle.setBorder(new CompoundBorder(padding, new LineBorder(Color.white, 1)));
    toggle.setBackground(Color.black);

    if (listener != null)
      toggle.addMouseListener(listener);

    return toggle;
  }

  public FlatToggle makeToggle(String title, boolean large) {
    return this.makeToggle(title, large, null);
  }

  public FlatToggle makeToggle(String title) {
    return this.makeToggle(title, false);
  }

  public void addComponents() {
    TabBar tbar = new TabBar(new TabBarSecondaryUI());

    tbar.addTab(
        "Primary Flight Display (PDF)", this.formatPanel(new PrimaryFlightDisplay().getPanel()));
    tbar.addTab("Flight Controls", this.formatPanel(new FlightControls().getPanel()));
    tbar.addTab("Electrics", this.formatPanel(new Engine().getPanel()));
    tbar.addTab("Procedures", new JPanel());

    GridPanel togglesPanel = new GridPanel(3, 4,1);

    apDisconnect = this.makeToggle("", true);
    apMaster = this.makeToggle("");
    lnav = this.makeToggle("");
    vnav = this.makeToggle("");
    at = this.makeToggle("");
    cmd = this.makeToggle("");
    hdg = this.makeToggle("");
    vs = this.makeToggle("");
    spd = this.makeToggle("");
    stateDisplay = this.makeToggle("");
    yd = this.makeToggle("");
    alt = this.makeToggle("");
    toga = this.makeToggle("");

    togglesPanel.add(apDisconnect);
    togglesPanel.add(apMaster);
    togglesPanel.add(lnav);
    togglesPanel.add(vnav);
    togglesPanel.add(at);
    togglesPanel.add(cmd);
    togglesPanel.add(hdg);
    togglesPanel.add(vs);
    togglesPanel.add(spd);
    togglesPanel.add(stateDisplay);
    togglesPanel.add(yd);
    togglesPanel.add(alt);
    togglesPanel.add(toga);

    JPanel bottomBar = new JPanel(new BorderLayout(0, 0));
    bottomBar.setBorder(new LineBorder(Color.white, 1));
    bottomBar.add(apDisconnect, BorderLayout.EAST);
    bottomBar.add(togglesPanel, BorderLayout.CENTER);
    bottomBar.setOpaque(false);

    this.add(tbar, BorderLayout.CENTER);
    this.add(bottomBar, BorderLayout.SOUTH);
  }
}
