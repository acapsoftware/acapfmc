/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui;

import acap.MainWorker;
import acap.fmc.gui.components.ImagePanel;
import acap.fmc.gui.utilities.FontLoader;
import java.awt.*;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/17/16. */
public class MainSplash {

  private JFrame frame;
  private static MainSplash instance = null;
  private JLabel statusLabel;

  public MainSplash() {
    this.statusLabel = this.getStatusLabel();
    setup();
  }

  public void updateStatus(String update) {
    statusLabel.setText(update);
  }

  public void addComponentListener(ComponentListener listener) {
    this.frame.addComponentListener(listener);
  }

  private void setup() {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    this.frame = new JFrame();
    this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.frame.setLocation(screenSize.width / 2 - 500, screenSize.height / 2 - 239);
    this.frame.setSize(1000, 478);
    this.frame.setUndecorated(true);
    this.frame.getContentPane().setLayout(new BorderLayout());

    ImagePanel ip = new ImagePanel("splash.png", 1000, 478);
    ip.setLayout(new BorderLayout());

    ip.add(this.statusLabel, BorderLayout.CENTER);
    ip.add(this.getCreditsPanel(), BorderLayout.SOUTH);

    this.frame.add(ip, BorderLayout.CENTER);
  }

  private JPanel getCreditsPanel() {
    JPanel creditsPanel = new JPanel(new GridLayout(1, 2));

    creditsPanel.setBorder(new EmptyBorder(15, 25, 15, 25));
    creditsPanel.setOpaque(false);

    creditsPanel.add(
        this.makeCreditLabel(
            "<html><body>ACAP Flight Management Computer<br>Version 2.0</body></html>",
            SwingConstants.LEFT));

    creditsPanel.add(
        this.makeCreditLabel(
            "Copyright 2017 Autonomous Cargo Aircraft Project", SwingConstants.RIGHT));

    return creditsPanel;
  }

  private JLabel getStatusLabel() {
    JLabel statusLabel = new JLabel("Loading system...");

    Font font = FontLoader.getFont(FontLoader.NOTO_SANS_REGULAR, 20f);

    if (font != null) {
      statusLabel.setFont(font);
    }

    statusLabel.setForeground(Color.white);
    statusLabel.setVerticalAlignment(SwingConstants.BOTTOM);
    statusLabel.setHorizontalAlignment(SwingConstants.CENTER);

    statusLabel.setBorder(new EmptyBorder(15, 25, 25, 25));

    return statusLabel;
  }

  private JLabel makeCreditLabel(String text, int alignment) {
    JLabel creditLabel = new JLabel(text);

    Font font = FontLoader.getFont(FontLoader.NOTO_SANS_REGULAR, 14f);

    if (font != null) {
      creditLabel.setFont(font);
    }

    creditLabel.setHorizontalAlignment(alignment);
    creditLabel.setVerticalAlignment(SwingConstants.BOTTOM);
    creditLabel.setForeground(Color.white);
    creditLabel.setOpaque(false);

    return creditLabel;
  }

  public static MainSplash getInstance() {
    if (instance == null) {
      instance = new MainSplash();
    }
    return instance;
  }

  public void show() {
    frame.setVisible(true);
  }

  public void close() {
    frame.setVisible(false);
    frame.dispose();
  }

  /**
   * Runs a worker that updates the JLabel as work is done
   *
   * @param worker
   */
  public void doWork(MainWorker worker) {

    worker.addPropertyChangeListener(
        evt -> {
          HashMap<Integer, String> stringMap = worker.getStringMap();

          if ("progress".equals(evt.getPropertyName())) {
            Integer progress = new Integer((Integer) evt.getNewValue());
            String updateString = stringMap.get(progress);
            if (updateString != null) {
              updateStatus(updateString);
            }
          }
        });

    worker.execute();
  }
}
