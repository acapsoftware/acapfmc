/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.wrappers;

import acap.fmc.gui.utilities.FontLoader;
import java.awt.*;
import javax.swing.*;

/** Created by tjnickerson on 12/1/16. */
public abstract class ComponentWrapper extends JPanel {

  protected String title;
  protected JLabel label;

  public ComponentWrapper(String title) {
    super(new BorderLayout(0, 10));

    this.title = title;
  }

  public void init() {
    this.setup();
    this.addComponents();
  }

  protected void setup() {
    this.setOpaque(false);

    this.label = new JLabel(this.title);
    this.label.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_BOLD, 13f));
    this.label.setForeground(Color.white);
    this.label.setOpaque(false);
  }

  protected void addComponents() {
    this.add(this.label, BorderLayout.NORTH);
    this.add(this.getMainComponent(), BorderLayout.CENTER);
  }

  protected abstract Component getMainComponent();
}
