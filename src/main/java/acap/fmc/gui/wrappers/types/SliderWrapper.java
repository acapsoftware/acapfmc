/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.wrappers.types;

import acap.fmc.gui.ui.CustomSliderUI;
import acap.fmc.gui.utilities.FontLoader;
import acap.fmc.gui.wrappers.ComponentWrapper;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;

/** Created by tjnickerson on 12/1/16. */
public class SliderWrapper extends ComponentWrapper {

  public static final int LABEL_WIDTH = 50;

  private int initial;
  private int min;
  private int max;
  private boolean enabled;
  private ChangeListener changeListener;
  private JSlider slider;
  private JLabel valueLabel;
  private boolean padLeft;

  private SliderWrapper(
      String title,
      int min,
      int max,
      int initialValue,
      boolean enabled,
      ChangeListener changeListener) {
    super(title);

    this.initial = initialValue;
    this.min = min;
    this.max = max;
    this.enabled = enabled;
    this.changeListener = changeListener;
    this.padLeft = false;

    this.init();
  }

  public void padLeft(boolean padLeft) {
    this.padLeft = padLeft;
    this.addComponents();
  }

  public SliderWrapper(String title, int min, int max, int initialValue) {
    this(title, min, max, initialValue, true, null);
  }

  public SliderWrapper(
      String title, int min, int max, int initialValue, ChangeListener changeListener) {
    this(title, min, max, initialValue, true, changeListener);
  }

  public SliderWrapper(String title, int min, int max, int initialValue, boolean enabled) {
    this(title, min, max, initialValue, enabled, null);
  }

  public SliderWrapper(String title, int min, int max) {
    this(title, min, max, 0);
  }

  @Override
  protected void setup() {
    super.setup();

    this.valueLabel = this.createLabel(Integer.toString(this.initial));

    this.slider = new JSlider(this.min, this.max, this.initial);
    this.slider.setOpaque(false);
    this.slider.setEnabled(this.enabled);
    this.slider.setOrientation(SwingConstants.HORIZONTAL);
    this.slider.setUI(new CustomSliderUI(slider));

    this.slider.addChangeListener(
        e -> {
          JSlider source = (JSlider) e.getSource();
          valueLabel.setText(Integer.toString(source.getValue()));
        });

    if (this.changeListener != null) {
      this.slider.addChangeListener(this.changeListener);
    }
  }

  public int getMinimum() {
    return this.min;
  }

  public int getMaximum() {
    return this.max;
  }

  public void setMinimum(int value) {
    this.slider.setMinimum(value);
  }

  public int getValue() {
    return this.slider.getValue();
  }

  public void setValue(int value) {
    this.slider.setValue(value);
  }

  public void setMaximum(int value) {
    this.slider.setMaximum(value);
  }

  protected JLabel createLabel(String text) {
    JLabel lbl = new JLabel();
    lbl.setBorder(new EmptyBorder(0, 10, 0, 10));
    lbl.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_BOLD, 12f));
    lbl.setForeground(Color.white);
    lbl.setHorizontalAlignment(SwingConstants.CENTER);
    lbl.setPreferredSize(new Dimension(50, lbl.getPreferredSize().height));
    lbl.setOpaque(false);
    lbl.setText(text);
    lbl.setVerticalAlignment(SwingConstants.BOTTOM);
    return lbl;
  }

  @Override
  protected Component getMainComponent() {
    JPanel container = new JPanel(new BorderLayout());
    container.setOpaque(false);

    container.add(this.slider, BorderLayout.CENTER);
    container.add(this.valueLabel, BorderLayout.EAST);

    return container;
  }

  @Override
  protected void addComponents() {
    JPanel childPanel = new JPanel(new BorderLayout(0, 15));
    childPanel.setOpaque(false);

    childPanel.add(this.label, BorderLayout.NORTH);
    childPanel.add(this.slider, BorderLayout.CENTER);

    if (this.padLeft) {
      JPanel fillerPanel = new JPanel();
      fillerPanel.setPreferredSize(
          new Dimension(LABEL_WIDTH, fillerPanel.getPreferredSize().height));
      fillerPanel.setOpaque(false);
      this.add(fillerPanel, BorderLayout.WEST);
    }

    this.add(this.valueLabel, BorderLayout.EAST);
    this.add(childPanel, BorderLayout.CENTER);
  }
}
