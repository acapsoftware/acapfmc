/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.wrappers.types;

import acap.fmc.gui.components.FlatCombo;
import acap.fmc.gui.wrappers.ComponentWrapper;
import acap.world.controller.IListController;
import java.awt.*;

/** Created by tjnickerson on 12/6/16. */
public class ComboWrapper extends ComponentWrapper {

  private FlatCombo combo;
  private IListController controller;

  public ComboWrapper(String title, IListController listController) {
    super(title);

    this.controller = listController;

    this.init();
  }

  @Override
  public void setup() {
    super.setup();
    this.combo = new FlatCombo(this.controller);
    this.combo.setPreferredSize(new Dimension((int) this.combo.getPreferredSize().getWidth(), 45));
  }

  @Override
  protected Component getMainComponent() {
    return this.combo;
  }
}
