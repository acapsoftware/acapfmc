/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.wrappers.types;

import acap.fmc.gui.components.RangeSlider;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.ChangeListener;

/** Created by tjnickerson on 12/1/16. */
public class RangeSliderWrapper extends SliderWrapper {

  private int initialLower;
  private int initialUpper;
  private boolean isEnabled;

  private RangeSlider slider;
  private JLabel lowerValueLabel;
  private JLabel upperValueLabel;

  private RangeSliderWrapper(
      String title, int min, int max, int initialLower, int initialUpper, boolean enabled) {
    super(title, min, max);

    this.initialLower = initialLower;
    this.initialUpper = initialUpper;
    this.isEnabled = enabled;

    this.init();
  }

  public RangeSliderWrapper(String title, int min, int max) {
    this(title, min, max, min, max, true);
  }

  public RangeSliderWrapper(String title, int min, int max, int initialLower, int initialUpper) {
    this(title, min, max, initialLower, initialUpper, true);
  }

  @Override
  public void setup() {
    super.setup();

    this.lowerValueLabel = this.createLabel(Integer.toString(this.initialLower));
    this.upperValueLabel = this.createLabel(Integer.toString(this.initialUpper));

    this.slider = new RangeSlider(this.getMinimum(), this.getMaximum());
    this.slider.setOpaque(false);
    this.slider.setUpperValue(this.initialUpper);
    this.slider.setValue(this.initialLower);
    this.slider.setEnabled(this.isEnabled);

    this.slider.addChangeListener(
        e -> {
          RangeSlider slider = (RangeSlider) e.getSource();
          this.lowerValueLabel.setText(String.valueOf(this.slider.getValue()));
          this.upperValueLabel.setText(String.valueOf(this.slider.getUpperValue()));
        });
  }

  @Override
  public void setValue(int value) {
    this.slider.setValue(value);
  }

  public void setUpperValue(int value) {
    this.slider.setUpperValue(value);
  }

  public void addChangeListener(ChangeListener listener) {
    this.slider.addChangeListener(listener);
  }

  @Override
  protected void addComponents() {
    JPanel childPanel = new JPanel(new BorderLayout(0, 15));
    childPanel.setOpaque(false);

    childPanel.add(this.label, BorderLayout.NORTH);
    childPanel.add(this.slider, BorderLayout.CENTER);

    this.add(this.upperValueLabel, BorderLayout.EAST);
    this.add(this.lowerValueLabel, BorderLayout.WEST);
    this.add(childPanel, BorderLayout.CENTER);
  }
}
