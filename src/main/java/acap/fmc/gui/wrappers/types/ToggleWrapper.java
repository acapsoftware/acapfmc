/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.wrappers.types;

import acap.fmc.gui.components.FlatToggle;
import acap.fmc.gui.wrappers.ComponentWrapper;
import java.awt.*;
import javax.swing.*;

/** Created by tjnickerson on 12/3/16. */
public class ToggleWrapper extends ComponentWrapper {

  private boolean isActive;

  public ToggleWrapper(String title) {
    this(title, false);
  }

  public ToggleWrapper(String title, boolean isActive) {
    super(title);

    this.isActive = isActive;

    this.init();
  }

  @Override
  protected void setup() {
    super.setup();
    this.label.setHorizontalAlignment(SwingConstants.CENTER);
  }

  @Override
  protected Component getMainComponent() {
    JPanel container = new JPanel(new FlowLayout(FlowLayout.CENTER));
    FlatToggle toggle = new FlatToggle(this.title, this.isActive);

    toggle.setAlignmentX(Component.CENTER_ALIGNMENT);

    container.setOpaque(false);
    container.add(toggle);

    return container;
  }
}
