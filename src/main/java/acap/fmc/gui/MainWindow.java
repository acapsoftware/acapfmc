/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui;

import acap.fmc.gui.components.ScratchPad;
import acap.fmc.gui.utilities.Palette;
import acap.fmc.gui.utilities.PopupManager;
import acap.fmc.gui.windows.SideBar;
import com.jogamp.newt.awt.NewtCanvasAWT;

import javax.swing.*;
import java.awt.*;

/** Created by tjnickerson on 11/10/16. */
public class MainWindow {

  private static final int HEIGHT = 500;
  private static final int WIDTH = 1000;

  static MainWindow instance;
  NewtCanvasAWT canvas;
  JFrame frame;
  String title;

  public MainWindow(String title, NewtCanvasAWT canvas) {
    this.canvas = canvas;
    this.frame = new JFrame(this.title);
    this.title = title;
    this.instance = this;

    initWindow();
  }

  public static MainWindow getInstance() {
    return instance;
  }

  public JFrame getFrame() {
    return this.frame;
  }

  private void initWindow() {
    this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.frame.setSize(WIDTH, HEIGHT);
    this.frame.setBackground(Palette.GRAY_1);
    this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    this.frame.setUndecorated(true);
    this.frame.getContentPane().setLayout(new GridLayout(1, 2));
  }

  public static int getHeight() {
    return HEIGHT;
  }

  public static int getWidth() {
    return WIDTH;
  }

  public static int getSidebarWidth() {
    return (int) (WIDTH * 0.5);
  }

  private void initSidebars() {
    ScratchPad scratchPad = ScratchPad.getInstance();
    JPanel leftSide = new JPanel(new BorderLayout());
    JPanel rightSide = new SideBar();

    leftSide.add(this.canvas, BorderLayout.CENTER);
    leftSide.add(scratchPad, BorderLayout.SOUTH);

    this.add(this.canvas, BorderLayout.CENTER);
    this.add(rightSide, BorderLayout.EAST);
  }

  public void add(Component component) {
    this.frame.getContentPane().add(component);
  }

  public void add(Component component, Object constraint) {
    this.frame.getContentPane().add(component, constraint);
  }

  public void show() {
    this.preShow();
    frame.setVisible(true);
    MainSplash.getInstance().close();
  }

  private void preShow() {
    this.addControls();
    this.frame.pack();
    this.frame.setLocationRelativeTo(null);
  }

  private void addControls() {
    initSidebars();
  }
}
