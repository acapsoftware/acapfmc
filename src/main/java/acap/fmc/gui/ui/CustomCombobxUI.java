/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.ui;

import static javax.swing.BorderFactory.createEmptyBorder;

import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import java.io.Serializable;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;

/** Created by tjnickerson on 12/12/16. */
public class CustomCombobxUI extends BasicComboBoxUI {

  @Override
  public void installUI(JComponent c) {
    super.installUI(c);

    LookAndFeel.uninstallBorder(comboBox);
    LookAndFeel.uninstallBorder(listBox);

    comboBox.setForeground(Color.white);
    comboBox.setBackground(Palette.GRAY_3);
    listBox.setSelectionBackground(Palette.GRAY_3);
    comboBox.setRenderer(new CustomListCellRenderer());
  }

  @Override
  public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus) {
    Color t = g.getColor();
    g.setColor(Palette.GRAY_3);
    g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
    g.setColor(t);
  }

  @Override
  protected ComboPopup createPopup() {
    return new CustomComboPopup(comboBox);
  }

  @Override
  protected JButton createArrowButton() {
    return null;
  }

  class CustomListCellRenderer extends JLabel implements ListCellRenderer<Object>, Serializable {

    public CustomListCellRenderer() {
      super();
      this.setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
        JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
      JLabel label = new JLabel();
      setComponentOrientation(list.getComponentOrientation());

      list.setSelectionForeground(Color.white);
      list.setBackground(Palette.GRAY_2);
      list.setBorder(createEmptyBorder());

      JList.DropLocation dropLocation = list.getDropLocation();
      if (dropLocation != null && !dropLocation.isInsert() && dropLocation.getIndex() == index) {
        isSelected = true;
      }

      if (isSelected) {
        setBackground(Palette.GRAY_4);
      } else {
        setBackground(Palette.GRAY_3);
      }

      setForeground(Color.white);

      setIcon(null);
      setText((value == null) ? "" : value.toString());
      setBorder(new EmptyBorder(10, 10, 10, 10));

      setEnabled(list.isEnabled());
      setFont(list.getFont());

      return this;
    }
  }

  class CustomComboPopup extends BasicComboPopup {

    public CustomComboPopup(JComboBox combo) {
      super(combo);
    }

    @Override
    protected void configurePopup() {
      super.configurePopup();
      setBorder(createEmptyBorder());
    }

    @Override
    protected JScrollPane createScroller() {
      JScrollPane sp = super.createScroller();
      sp.getVerticalScrollBar().setUI(new CustomScrollBarUI(Palette.GRAY_3));
      return sp;
    }
  }
}
