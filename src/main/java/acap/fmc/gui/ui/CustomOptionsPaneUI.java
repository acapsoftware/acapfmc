package acap.fmc.gui.ui;

import acap.fmc.gui.utilities.FontLoader;
import acap.fmc.gui.utilities.Palette;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by tjnickerson on 3/24/17.
 */
public class CustomOptionsPaneUI extends BasicOptionPaneUI {
    public CustomOptionsPaneUI() {
        super();

        UIManager uim = new UIManager();
        uim.put("OptionPane.background", Palette.GRAY_3);
        uim.put("Panel.background", Palette.GRAY_3);
    }

    @Override
    protected Object getMessage() {
        Object obj = super.getMessage();

        if (this.inputComponent instanceof JTextField) {
            this.inputComponent.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_REGULAR, 14));
            this.inputComponent.setBorder(new CompoundBorder(
                    new LineBorder(Palette.GRAY_4, 1),
                    new EmptyBorder(10, 10, 10, 10))
            );
            this.inputComponent.setForeground(Color.white);
            this.inputComponent.setBackground(Palette.GRAY_2);
        }

        return obj;
    }

    @Override
    protected Container createButtonArea() {
        JPanel area = (JPanel) super.createButtonArea();
        area.setBorder(new EmptyBorder(20, 0, 0, 0));
        return area;
    }

    @Override
    protected void addButtonComponents(Container container, Object[] buttons,
                                       int initialIndex) {
        super.addButtonComponents(container, buttons, initialIndex);
        int numButtons = buttons.length;

        for (int counter = 0; counter < numButtons; counter++) {
            Object button = buttons[counter];
            if (button instanceof JButton) {
                ActionListener buttonListener = createButtonActionListener(counter);
                if (buttonListener != null) {
                    ((JButton)button).addActionListener(buttonListener);
                }
            }
        }
    }
}
