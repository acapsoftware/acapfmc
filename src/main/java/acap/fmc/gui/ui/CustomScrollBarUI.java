/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.ui;

import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;

/** Created by tjnickerson on 11/20/16. */
public class CustomScrollBarUI extends BasicScrollBarUI {

  private static final int H_OFFSET = 0;
  private static final int V_OFFSET = 10;

  private final Dimension d = new Dimension();
  private Color background;

  public CustomScrollBarUI(Color background) {
    this.background = background;
  }

  @Override
  protected JButton createDecreaseButton(int orientation) {
    return new JButton() {
      @Override
      public Dimension getPreferredSize() {
        return d;
      }
    };
  }

  @Override
  protected JButton createIncreaseButton(int orientation) {
    return new JButton() {
      @Override
      public Dimension getPreferredSize() {
        return d;
      }
    };
  }

  @Override
  protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
    Graphics2D g2 = (Graphics2D) g.create();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setPaint(this.background);
    g2.fillRect(r.x, r.y, r.width, r.height);
    g2.dispose();
  }

  @Override
  protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
    Graphics2D g2 = (Graphics2D) g.create();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    Color color;
    JScrollBar sb = (JScrollBar) c;
    if (!sb.isEnabled()) {
      return;
    } else if (isDragging) {
      color = Palette.GRAY_2.darker();
    } else if (isThumbRollover()) {
      color = Palette.GRAY_2.brighter();
    } else {
      color = Palette.GRAY_2;
    }
    g2.setPaint(color);

    if (scrollbar.getOrientation() == JScrollBar.HORIZONTAL) {
      g2.fillRoundRect(r.x + H_OFFSET, r.y, r.width - H_OFFSET * 2, r.height, 10, 10);
    } else {
      g2.fillRoundRect(r.x, r.y + V_OFFSET, r.width, r.height - V_OFFSET * 2, 10, 10);
    }
    g2.dispose();
  }

  @Override
  protected void setThumbBounds(int x, int y, int width, int height) {
    super.setThumbBounds(x, y, width, height);
    scrollbar.repaint();
  }

  @Override
  protected void installDefaults() {
    super.installDefaults();
    scrollBarWidth = 10;
  }
}
