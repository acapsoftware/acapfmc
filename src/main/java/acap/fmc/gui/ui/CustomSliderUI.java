/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.ui;

import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.BasicSliderUI;

/** Created by tjnickerson on 11/30/16. */
public class CustomSliderUI extends BasicSliderUI {

  private static final int THUMB_WIDTH = 8;
  private static final int TRACK_HEIGHT = 25;

  public CustomSliderUI(JSlider b) {
    super(b);
  }

  @Override
  public void paint(Graphics g, JComponent c) {
    Graphics2D g2d = (Graphics2D) g;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    super.paint(g, c);
  }

  @Override
  protected Dimension getThumbSize() {
    return new Dimension(THUMB_WIDTH, TRACK_HEIGHT);
  }

  @Override
  public void paintFocus(Graphics g) {}

  @Override
  public void paintTrack(Graphics g) {
    Graphics2D g2d = (Graphics2D) g;
    Stroke old = g2d.getStroke();
    g2d.setPaint(Palette.GRAY_3);
    g2d.fillRect(trackRect.x, trackRect.y, trackRect.width, trackRect.height);
    g2d.setPaint(Palette.RED);
    g2d.fillRect(trackRect.x, trackRect.y, thumbRect.x, trackRect.height);
    g2d.setStroke(old);
  }

  @Override
  public void paintThumb(Graphics g) {
    if (slider.isEnabled()) {
      Graphics2D g2d = (Graphics2D) g;
      Stroke old = g2d.getStroke();
      g2d.setPaint(Color.white);
      g2d.fillRect(thumbRect.x, thumbRect.y, thumbRect.width, thumbRect.height);
      g2d.setStroke(old);
    }
  }
}
