/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.ui.tabs;

import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/20/16. */
public class TabBarPrimaryUI implements ITabBarUI {

  @Override
  public Color tabSelected() {
    return Palette.RED;
  }

  @Override
  public Color tabBackground() {
    return Palette.GRAY_3;
  }

  @Override
  public EmptyBorder tabPadding() {
    return new EmptyBorder(20, 25, 20, 25);
  }

  @Override
  public float tabFontSize() {
    return 14f;
  }
}
