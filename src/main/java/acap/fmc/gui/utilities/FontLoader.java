/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.utilities;

import java.awt.*;
import java.io.IOException;

/** Created by tjnickerson on 11/16/16. */
public class FontLoader {

  public static final String NOTO_MONO = "NotoMono-Regular.ttf";
  public static final String NOTO_SANS_BOLD = "NotoSansUI-Bold.ttf";
  public static final String NOTO_SANS_REGULAR = "NotoSansUI-Regular.ttf";

  public static Font getFont(String filename, float size) {
    return getFont(filename, size, Font.PLAIN);
  }

  public static Font getFont(String filename, float size, int style) {
    Font font = null;

    try {
      font =
          Font.createFont(
              Font.TRUETYPE_FONT,
              ClassLoader.getSystemClassLoader().getResource("fonts/" + filename).openStream());

      GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
      genv.registerFont(font);

      font = font.deriveFont(style, size);
    } catch (FontFormatException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return font;
  }
}
