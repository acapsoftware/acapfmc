package acap.fmc.gui.utilities;

import acap.fmc.gui.components.FlatButton;
import acap.fmc.gui.ui.CustomOptionsPaneUI;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PopupManager {

    private Object lastValue;
    private Object lastButton;

    private static final Color BACKGROUND_COLOR = Palette.GRAY_3;
    private static final Color FOREGROUND_COLOR = Color.white;

    private JLabel makeLabel(String text) {
        String htmlWrap = "<html>" + text.replace("\n", "<br/>") + "</html>";
        JLabel customLabel = new JLabel(htmlWrap);
        customLabel.setForeground(FOREGROUND_COLOR);
        customLabel.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_REGULAR, 14));
        customLabel.setBorder(new EmptyBorder(10,0,10,0));
        customLabel.setOpaque(false);
        return customLabel;
    }

    private FlatButton makeButton(String text, Runnable clickEvent) {
        FlatButton customBtn = new FlatButton(text);
        customBtn.setBorder(new EmptyBorder(10,25,10,25));
        customBtn.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (clickEvent != null)
                    clickEvent.run();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        return customBtn;
    }

    private JDialog makeDialog() {
        JDialog dialog = new JDialog((Frame)null, "", true);
        dialog.setUndecorated(true);
        dialog.setBackground(BACKGROUND_COLOR);
        return dialog;
    }

    private void packageAndShowDialog(JDialog dialog, Container contentPane) {
        dialog.setContentPane(contentPane);
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    private JOptionPane makeOptionPane(String message, String acceptButton, String rejectButton, JDialog dialog, Runnable callback) {
        final JOptionPane optionPane = new JOptionPane(
                makeLabel(message),
                JOptionPane.PLAIN_MESSAGE,
                JOptionPane.YES_NO_OPTION,
                null);

        Runnable closeCB = () -> {
            if (dialog != null) {
                dialog.setVisible(false);
                dialog.dispose();
            }
        };

        Runnable confirmCB = () -> {
            this.lastValue = optionPane.getInputValue();
            this.lastButton = optionPane.getValue();
            closeCB.run();
            if (callback != null)
                callback.run();
        };

        FlatButton[] a = {
                makeButton(acceptButton, confirmCB),
                makeButton(rejectButton, closeCB)
        };

        optionPane.setOptions(a);
        optionPane.setUI(new CustomOptionsPaneUI());
        optionPane.setBorder(new EmptyBorder(25,25,25,25));

        return optionPane;
    }

    public void showYesNoDialog(String message) {
        showYesNoDialog(message, null);
    }

    public void showYesNoDialog(String message, Runnable callback) {
        showConfirmationDialog(message, callback, "Yes", "No");
    }

    public void showOkayCancelDialog(String message) {
        showOkayCancelDialog(message, null);
    }

    public void showOkayCancelDialog(String message, Runnable callback) {
        showConfirmationDialog(message, callback, "Okay", "Cancel");
    }

    public void showConfirmationDialog(String message, String acceptButton, String rejectButton) {
        showConfirmationDialog(message, null, acceptButton, rejectButton);
    }

    public void showConfirmationDialog(String message, Runnable callback, String acceptButton, String rejectButton) {
        final JDialog dialog = makeDialog();
        final JOptionPane optionPane = makeOptionPane(
                message,
                acceptButton,
                rejectButton,
                dialog,
                callback
        );

        packageAndShowDialog(dialog, optionPane);
    }

    public void showInputDialog(String message) {
        showInputDialog(message, null);
    }

    public void showInputDialog(String message, Runnable callback) {
        showInputDialog(message, callback,"Submit", "Cancel");
    }

    public void showInputDialog(String message, Runnable callback, String acceptButton, String rejectButton) {
        final JDialog dialog = makeDialog();

        final JOptionPane optionPane = makeOptionPane(
                message,
                acceptButton,
                rejectButton,
                dialog,
                callback
        );

        optionPane.setWantsInput(true);

        packageAndShowDialog(dialog, optionPane);
    }

    public String getLastValue() {
        return (String)this.lastValue;
    }

    public String getLastButton() {
        return ((FlatButton) this.lastButton).getText();
    }
}
