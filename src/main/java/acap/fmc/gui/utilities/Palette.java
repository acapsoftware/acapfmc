/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.utilities;

import java.awt.*;

/** Created by tjnickerson on 11/15/16. */
public class Palette {

  public static final Color TRANSPARENT = new Color(0, 0, 0, 0);

  public static final Color GRAY_1 = Color.decode("#151515");
  public static final Color GRAY_2 = Color.decode("#202020");
  public static final Color GRAY_3 = Color.decode("#303030");
  public static final Color GRAY_4 = Color.decode("#3F3F3F");
  public static final Color GRAY_5 = Color.decode("#767676");

  public static final Color RED = Color.decode("#B3001B");
  public static final Color GREEN = Color.decode("#077B24");
}
