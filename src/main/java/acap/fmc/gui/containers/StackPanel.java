/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.containers;

import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;

/** Created by tjnickerson on 11/29/16. */
public class StackPanel extends JPanel {

  private static final Color BACKGROUND = Palette.GRAY_1;

  private JPanel innerPanel;
  private GridBagConstraints gbc;
  private int componentCount;
  private int vgap;

  public StackPanel() {
    this(0);
  }

  public StackPanel(int vgap) {
    super(new BorderLayout());

    this.vgap = vgap;

    this.setup();
  }

  private void setup() {
    this.componentCount = 0;
    this.gbc = new GridBagConstraints();

    this.innerPanel = new JPanel(new GridBagLayout());
    this.innerPanel.setOpaque(false);

    gbc.anchor = GridBagConstraints.NORTHWEST;
    gbc.weightx = 1;
    gbc.weighty = 1;
    gbc.gridx = 1;
    gbc.insets = new Insets(0, 0, this.vgap, 0);
    gbc.fill = GridBagConstraints.HORIZONTAL;

    this.setBackground(BACKGROUND);
    this.addComponents();
  }

  private void addComponents() {
    this.add(this.innerPanel, BorderLayout.NORTH);
  }

  public void addComponent(Component component) {
    componentCount++;

    gbc.gridy = componentCount;

    this.innerPanel.add(component, gbc);
  }
}
