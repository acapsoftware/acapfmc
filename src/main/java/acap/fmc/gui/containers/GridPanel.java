/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.containers;

import java.awt.*;
import javax.swing.*;

/** Created by tjnickerson on 1/8/17. */
public class GridPanel extends JPanel {

  private GridBagConstraints constraints;
  private int rows;
  private int columns;
  private int spacing;
  private int current_row;
  private int current_column;

  public GridPanel(int rows, int columns) {
    this(rows, columns, 0);
  }

  public GridPanel(int rows, int columns, int spacing) {
    super(new GridBagLayout());

    this.current_column = 0;
    this.current_row = 0;
    this.rows = rows;
    this.columns = columns;
    this.spacing = spacing;

    this.constraints = new GridBagConstraints();
    this.setup();
  }

  private void setup() {
    this.constraints.insets = new Insets(this.spacing, this.spacing, this.spacing, this.spacing);
    this.constraints.fill = GridBagConstraints.BOTH;
  }

  public void setConstraints(GridBagConstraints constraints) {
    this.constraints = constraints;
    this.setup();
  }

  @Override
  public Component add(Component component, int columnSpan) {
    this.constraints.gridx = this.current_column;
    this.constraints.gridy = this.current_row;

    if (this.current_column == 0 || (this.current_row == this.rows - 1))
      this.constraints.weightx = 1D;
    else this.constraints.weightx = 0D;

    this.constraints.gridwidth = columnSpan;

    if (this.current_row <= this.rows) {
      this.add(component, this.constraints);
    }

    if (this.current_column + columnSpan >= this.columns) {
      this.current_column = 0;
      this.current_row++;
    } else {
      this.current_column += columnSpan;
    }

    return component;
  }

  @Override
  public Component add(Component component) {
    return this.add(component, 1);
  }
}
