/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.gui.containers;

import acap.fmc.gui.utilities.FontLoader;
import acap.fmc.gui.utilities.Palette;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** Created by tjnickerson on 11/29/16. */
public class ComponentGroup extends JPanel {

  private String title;
  private StackPanel panel;

  public ComponentGroup(String title) {
    this.panel = new StackPanel(20);
    this.title = title;

    this.setup();
  }

  private void setup() {
    this.setLayout(new BorderLayout());
    this.setOpaque(false);

    this.panel.setOpaque(false);
    this.panel.setBorder(new EmptyBorder(15, 0, 0, 0));

    this.addComponents();
  }

  private void addComponents() {
    JLabel title = new JLabel(this.title);

    title.setForeground(Palette.GRAY_5);
    title.setFont(FontLoader.getFont(FontLoader.NOTO_SANS_REGULAR, 24f, Font.ITALIC));

    this.add(title, BorderLayout.NORTH);
    this.add(this.panel, BorderLayout.CENTER);
  }

  public void addComponent(Component component) {
    this.panel.addComponent(component);
  }
}
