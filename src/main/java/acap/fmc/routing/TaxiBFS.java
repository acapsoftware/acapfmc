/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.routing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import org.graphstream.graph.BreadthFirstIterator;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

/** Created by erikn on 1/31/2017. */
public class TaxiBFS<T extends Node> extends BreadthFirstIterator {

  private String[] taxiways;
  private ArrayList<Edge> traversal;
  private String stopping = "";
  private boolean stop = false;
  private boolean traversedAtLeastOne = false;
  private TaxiDijkstra.IEdgeEval test = null;
  private Node lastNode = null;

  public TaxiBFS(Node startNode, boolean directed) {
    super(startNode, directed);
  }

  public TaxiBFS(Node startNode, String[] includedTaxiWays) {
    super(startNode);
    taxiways = includedTaxiWays;
    traversal = new ArrayList<>();
  }

  public TaxiBFS(Node startNode, String[] includedTaxiWays, String stop) {
    super(startNode);
    taxiways = includedTaxiWays;
    traversal = new ArrayList<>();
    stopping = stop;
  }

  public TaxiBFS(Node startNode, String start, String stop) {
    super(startNode);
    taxiways = new String[] {start};
    traversal = new ArrayList<>();
    stopping = stop;
  }

  public TaxiBFS(Node startNode, String stop) {
    super(startNode);
    taxiways = null;
    traversal = new ArrayList<>();
    stopping = stop;
  }

  public TaxiBFS(Node startNode, String stop, TaxiDijkstra.IEdgeEval tester) {
    super(startNode);
    taxiways = null;
    traversal = new ArrayList<>();
    test = tester;
    stopping = stop;
  }

  public boolean hasNext() {

    return (!stop || !traversedAtLeastOne) && qHead < qTail;
  }

  @SuppressWarnings("unchecked")
  public T next() {
    if (qHead >= qTail) throw new NoSuchElementException();
    Node current = queue[qHead++];
    int level = depth[current.getIndex()] + 1;
    Iterable<Edge> edges = directed ? current.getEachLeavingEdge() : current.getEachEdge();

    for (Edge e : edges) {
      if (e.getAttribute("taxipath").toString().contains(stopping)) {
        stop = true;
      }
    }
    //
    for (Edge e : traversal) {
      if (taxiways != null && e.getAttribute("taxipath").toString().contains(taxiways[0]))
        traversedAtLeastOne = true;
    }

    if (!stop || !traversedAtLeastOne) {
      for (Edge e : edges) {
        Node node = e.getOpposite(current);
        int j = node.getIndex();

        if (test == null) {
          if (depth[j] == -1
              && (taxiways == null
                  || e.getAttribute("taxipath").toString().contains(taxiways[0]))) {
            queue[qTail++] = node;
            depth[j] = level;
            traversal.add(e);
          }
        } else {
          if (depth[j] == -1 && test.test(e)) {
            queue[qTail++] = node;
            depth[j] = level;
            traversal.add(e);
          }
        }
      }
    }
    return (T) current;
  }
}
