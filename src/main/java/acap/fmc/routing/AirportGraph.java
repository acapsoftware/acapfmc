/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.routing;

import static acap.world.model.BaseAeroObject.EARTH_RADIUS_KM;
import static acap.world.model.BaseAeroObject.KM_TO_NM;
import static org.exist.storage.ElementValue.type;

import acap.world.utilities.NavDB;
import acap.world.model.*;
import java.io.IOException;
import java.util.*;
import org.apache.commons.lang3.ArrayUtils;
import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.stream.file.FileSinkImages.CustomResolution;
import org.graphstream.stream.file.FileSinkImages.LayoutPolicy;
import org.graphstream.stream.file.FileSinkImages.OutputPolicy;
import org.graphstream.stream.file.FileSinkImages.OutputType;
import org.graphstream.stream.file.FileSinkImages.Resolutions;
import org.graphstream.stream.file.FileSourceGraphML;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

public class AirportGraph {

  public static final String TAXIPATH = "taxipath";
  public static final String TYPE = "type";
  public static final String LENGTH = "length";
  public static final String HOLDSHORT = "holdshort";
  public static final String TAXIWAY = "taxiway";
  public static final String NAME = "name";
  private Graph airport;
  private Airport airp;

  public AirportGraph(String identifier) {
    airport = getAirportGraph(identifier);
    airp = NavDB.getInstance().getAirport(identifier);
  }

  private double distanceBetweenNodes(Node n1, Node n2) {
    return distanceToNode(n1, n2.getAttribute("lon"), n2.getAttribute("lat"));
  }

  private double headingToNode(Node start, Node end) {
    final double lat1 = Math.toRadians(start.getAttribute("lat"));
    final double lat2 = Math.toRadians(end.getAttribute("lat"));
    final double lng1 = Math.toRadians(start.getAttribute("lon"));
    final double lng2 = Math.toRadians(end.getAttribute("lon"));

    final double deltaLng = lng2 - lng1;
    final double y = Math.cos(lat2) * Math.sin(deltaLng);
    final double x =
        (Math.cos(lat1) * Math.sin(lat2)) - (Math.sin(lat1) * Math.cos(lat2) * Math.cos(deltaLng));

    final float raw = (float) Math.toDegrees(Math.atan2(y, x));
    final float normalized = (raw + 360) % 360;
    return (360 + normalized - 0) % 360;
  }

  public static double headingToNode(double lat, double lon, Node end) {
    final double lat1 = Math.toRadians(lat);
    final double lat2 = Math.toRadians(end.getAttribute("lat"));
    final double lng1 = Math.toRadians(lon);
    final double lng2 = Math.toRadians(end.getAttribute("lon"));

    final double deltaLng = lng2 - lng1;
    final double y = Math.cos(lat2) * Math.sin(deltaLng);
    final double x =
        (Math.cos(lat1) * Math.sin(lat2)) - (Math.sin(lat1) * Math.cos(lat2) * Math.cos(deltaLng));

    final float raw = (float) Math.toDegrees(Math.atan2(y, x));
    final float normalized = (raw + 360) % 360;
    return (360 + normalized - 0) % 360;
  }

  private double distanceToNode(Node n1, double lon, double lat) {
    // haversine function, adapted from:
    //  http://www.movable-type.co.uk/scripts/latlong.html

    final double lat1 = n1.getAttribute("lat");
    final double lat2 = lat;
    final double lng1 = n1.getAttribute("lon");
    final double lng2 = lon;

    final double phi1 = Math.toRadians(lat1);
    final double phi2 = Math.toRadians(lat2);

    final double deltaPhi = Math.toRadians(lat2 - lat1);
    final double deltaLambda = Math.toRadians(lng2 - lng1);

    final double deltaPhi2sin = Math.sin(deltaPhi / 2.);
    final double deltaLam2sin = Math.sin(deltaLambda / 2.);

    final double a =
        deltaPhi2sin * deltaPhi2sin + Math.cos(phi1) * Math.cos(phi2) * deltaLam2sin * deltaLam2sin;
    final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return (EARTH_RADIUS_KM * c * KM_TO_NM);
  }

  public Node getNodeNearLonLat(double lon, double lat) {

    double minDist = Double.MAX_VALUE;
    Node closest = null;

    for (Node n : airport.getNodeSet()) {
      if (distanceToNode(n, lon, lat) < minDist) {
        minDist = distanceToNode(n, lon, lat);
        closest = n;
      }
    }

    return closest;
  }

  public Path findPathToTaxiWay(Node start, String taxiway) {
    return findPathToTaxiWay(start, taxiway, e -> true);
  }

  public Path getShortestPathToNode(Node start, Node finish) {
    Dijkstra dijkstra = new Dijkstra(Dijkstra.Element.EDGE, null, LENGTH);
    dijkstra.init(this.airport);
    dijkstra.setSource(start);
    dijkstra.compute();

    Path tax = dijkstra.getPath(finish);

    return tax;
  }

  /**
   * Retrieve a set of graph nodes corresponding to a specific runway
   * @param runway - The name of the runway pair i.e (03/21)
   * @param name - The runway desired
   * @return
   */
  public Node[] getRunwayByHeading(String runway, String name) {
    Edge[] runwayEdges =
        airport
            .getEdgeSet()
            .stream()
            .filter(e -> e.getAttribute(TAXIPATH).equals(runway))
            .toArray(Edge[]::new);
    Node[] runwayNodes = Arrays.stream(runwayEdges).map(Edge::getSourceNode).toArray(Node[]::new);

    double heading = (headingToNode(runwayNodes[1], runwayNodes[0]));
    if (Math.abs(airp.getRunwayByNumber(name).getHeading() - heading) < 1) {
      ArrayUtils.reverse(runwayNodes);
      return runwayNodes;
    } else return runwayNodes;
  }

  /**
   * Retrieve a set of graph nodes corresponding to a specific runway
   * @param heading - The runway desired along with designator
   * @return
   */
  public Node[] getRunwayByHeading(String heading) {
    Edge[] runwayEdges =
        airport
            .getEdgeSet()
            .stream()
            .filter(e -> e.getAttribute(TAXIPATH).toString().contains(heading))
            .toArray(Edge[]::new);
    Node[] runwayNodes = Arrays.stream(runwayEdges).map(Edge::getSourceNode).toArray(Node[]::new);

    double rheading = (headingToNode(runwayNodes[1], runwayNodes[0]));
    if (Math.abs(airp.getRunwayByNumber(heading).getHeading() - rheading) < 1) {
      ArrayUtils.reverse(runwayNodes);
      return runwayNodes;
    } else return runwayNodes;
  }

  public Path findPathToTaxiWay(Node start, String taxiway, TaxiDijkstra.IEdgeEval restriction) {
    TaxiBFS<Node> bf = new TaxiBFS<Node>(start, taxiway, restriction);
    ArrayList<Edge> p = new ArrayList<>();
    Node end = null;
    boolean stop = false;

    while (bf.hasNext() && !stop) {
      end = bf.next();
      for (Edge e : end.getEdgeSet()) {
        if (e.getAttribute(TAXIPATH).equals(taxiway)) {
          stop = true;
        }
      }
    }

    if (end != null) end.setAttribute("ui.class", "important");

    TaxiDijkstra dijkstra = new TaxiDijkstra(Dijkstra.Element.EDGE, null, LENGTH, restriction);
    dijkstra.init(this.airport);
    dijkstra.setSource(start);
    dijkstra.compute();

    Path tax = dijkstra.getPath(end);

    return tax;
  }

  public Path findPathToTaxiWay(Node start, String taxiway, String stayOn) {
    TaxiBFS<Node> bf = new TaxiBFS<Node>(start, stayOn, taxiway);
    ArrayList<Edge> p = new ArrayList<>();
    Node end = null;

    while (bf.hasNext()) {
      end = bf.next();
    }

    if (end != null) end.setAttribute("ui.class", "important");

    TaxiDijkstra dijkstra =
        new TaxiDijkstra(
            Dijkstra.Element.EDGE, null, LENGTH, e -> e.getAttribute(TAXIPATH).toString().contains(stayOn));

    dijkstra.init(this.airport);
    dijkstra.setSource(start);
    dijkstra.compute();

    Path tax = dijkstra.getPath(end);

    for (Edge e : tax.getEdgeSet()) {
      e.setAttribute("ui.class", "path");
    }

    return tax;
  }

  public Path parsePathFromString(String taxiInstruction, Node startingNode) {
    ArrayList<Edge> pathedges = new ArrayList<>();
    Path p = new Path();
    Node next = startingNode;
    List<String> stringpath = Arrays.asList(taxiInstruction.split(" "));

    if (!startingNode.getAttribute(TYPE).equals("TAXI")) {
      Path startpath =
          findPathToTaxiWay(
              startingNode, stringpath.get(0), e -> !e.getAttribute(TYPE).equals("TAXI"));
      next = startpath.getNodePath().get(startpath.size() - 1);
      for (Edge e : startpath.getEdgeSet()) pathedges.add(e);
    }

    for (int i = 0; i < stringpath.size() - 1; i++) {
      Path test = findPathToTaxiWay(next, stringpath.get(i + 1), stringpath.get(i));
      for (Edge e : test.getEdgeSet()) {
        pathedges.add(e);
      }
      next = test.getNodePath().get(test.size() - 1);
    }

    Node cur = startingNode;
    for (Edge e : pathedges) {
      p.add(cur, e);
      cur = e.getOpposite(cur);
    }

    return p;
  }

  public List<Edge> getTaxiEdges(String taxiwayName) {
    List<Edge> es = new ArrayList<>();
    for (Edge e : airport.getEdgeSet()) {
      if (e.getAttribute(TAXIPATH).equals(taxiwayName)) {
        es.add(e);
      }
    }
    return es;
  }

  public Graph getGraph() {
    return airport;
  }

  public Graph getAirportGraph(String identifier) {
    Graph graph = new MultiGraph(identifier);

    String styleSheet =
              "node {"
            + " fill-color: white;"
            + " size: 15px;"
            + " stroke-mode: plain;"
            + " stroke-color: white;"
            + " stroke-width: 15px;"
            + "}"
            + "node.important {"
            + " fill-color: #B3001B;"
            + " size: 30px;"
            + "}"
            + "edge.runway {"
            + " shape: line;"
            + " fill-color: red;"
            + " stroke-width: 30px;"
            + " arrow-size: 30px, 18px;"
            + " text-color: #767676;"
            + "}"
            + "edge.path {"
            + " shape: line;"
            + " size: 22px;"
            + " stroke-width: 30px;"
            + " fill-color: #077B24;"
                  + " text-alignment: above;"
            + " arrow-size: 15px, 13px;"
            + " text-size: 70;"
            + " text-color: #f442d7;"
            + "}"
            + "edge {"
            + " shape: line;"
            + " fill-color: black;"
            + " arrow-size: 30px, 18px;"
            + " size: 12px;"
            + " text-alignment: above;"
            + " text-color: #767676;"
            + " text-background-mode: rounded-box; "
            + " text-background-color: yellow; "
            + " text-size: 50;"
            + "}";

    graph.addAttribute("ui.stylesheet", styleSheet);

    NavDB db = NavDB.getInstance();

    ArrayList<TaxiNode> testNodes = db.getAirport(identifier).getTaxiNodes();
    ArrayList<TaxiPath> testPaths = db.getAirport(identifier).getTaxiPaths();
    ArrayList<Parking> parking = db.getAirport(identifier).getParking();
    HashMap<Integer, Parking> pkIndex = db.getAirport(identifier).getParkingIndex();

    for (TaxiPath n : testPaths) {
      if (!n.getName().isEmpty()) {
        String node_name =
            String.valueOf(testNodes.get(n.getStart()).getLon())
                + String.valueOf(testNodes.get(n.getStart()).getLat());
        if (graph.getNode(node_name) == null && !n.getType().equals("PARKING")) {
          graph.addNode(node_name);
          graph
              .getNode(node_name)
              .setAttribute(
                  "xy", testNodes.get(n.getStart()).getLon(), testNodes.get(n.getStart()).getLat());
          graph.getNode(node_name).setAttribute(TAXIWAY, n.getName());
          graph.getNode(node_name).setAttribute(NAME, n.getName());
          graph.getNode(node_name).setAttribute(TYPE, n.getType());
          graph.getNode(node_name).setAttribute(HOLDSHORT, false);
          graph
              .getNode(node_name)
              .setAttribute("lon", (double) testNodes.get(n.getStart()).getLon());
          graph
              .getNode(node_name)
              .setAttribute("lat", (double) testNodes.get(n.getStart()).getLat());
        }
      }
    }

    for (Parking p : parking) {
      String node_name = String.valueOf(p.getLon()) + String.valueOf(p.getLat());
      if (graph.getNode(node_name) == null) {
        graph.addNode(node_name);
        graph.getNode(node_name).setAttribute("xy", p.getLon(), p.getLat());
        graph.getNode(node_name).setAttribute("parking", p.getName());
        graph.getNode(node_name).setAttribute(NAME, p.getName());
        graph.getNode(node_name).setAttribute("lon", (double) p.getLon());
        graph.getNode(node_name).setAttribute("lat", (double) p.getLat());
        graph.getNode(node_name).setAttribute(TYPE, p.getType());
        graph.getNode(node_name).setAttribute(HOLDSHORT, false);
      }
    }

    for (TaxiNode n : testNodes) {
      String node_name = String.valueOf(n.getLon()) + String.valueOf(n.getLat());
      if(n.getType().equals("HOLD_SHORT")){
        if(graph.getNode(node_name) != null){
          graph.getNode(node_name).setAttribute(HOLDSHORT, true);
        }
      }
      if (graph.getNode(node_name) == null) {
        graph.addNode(node_name);
        graph.getNode(node_name).setAttribute("xy", n.getLon(), n.getLat());
        graph.getNode(node_name).setAttribute(TYPE, n.getType());
        graph.getNode(node_name).setAttribute("lon", (double) n.getLon());
        graph.getNode(node_name).setAttribute("lat", (double) n.getLat());
      }
    }

    int increment;

    for (TaxiPath n : testPaths) {
      if (!n.getName().isEmpty()) {
        increment = 0;
        String start_name =
            String.valueOf(testNodes.get(n.getStart()).getLon())
                + String.valueOf(testNodes.get(n.getStart()).getLat());
        String end_name =
            String.valueOf(testNodes.get(n.getEnd()).getLon())
                + String.valueOf(testNodes.get(n.getEnd()).getLat());

        if (n.getType().equals("PARKING")) {
          end_name =
              String.valueOf(pkIndex.get(n.getEnd()).getLon())
                  + String.valueOf(pkIndex.get(n.getEnd()).getLat());
        }

        if (graph.getNode(start_name) != null && graph.getNode(end_name) != null) {
          while (graph.getEdge(n.getName() + increment) != null) increment++;
          graph.addEdge(n.getName() + increment, start_name, end_name);
//          if (n.getType().equals("TAXI") || n.getType().equals("RUNWAY"))
//            graph
//                .getEdge(n.getName() + increment)
//                .setAttribute("ui.label", n.getName());
          graph.getEdge(n.getName() + increment).setAttribute(TYPE, n.getType());
          graph.getEdge(n.getName() + increment).setAttribute(TAXIPATH, n.getName());
          graph
              .getEdge(n.getName() + increment)
              .setAttribute(
                  LENGTH, distanceBetweenNodes(graph.getNode(start_name), graph.getNode(end_name)));
        }
      }
    }
    return graph;
  }

  public static void main(String args[]) {

    AirportGraph g = new AirportGraph("KBOS");

    Node test = g.getNodeNearLonLat(-71.0176773071289, 42.35686492919922);

    Path p = g.parsePathFromString("K K3 E K F 15R G D D1 09", test);

    p.getEdgeSet()
        .forEach(
            (Edge e) -> {
              e.setAttribute("ui.class","path");
              if(e.getAttribute(TYPE).toString().equals("TAXI") ||e.getAttribute(TYPE).toString().equals("RUNWAY"))
                e.setAttribute("ui.label",e.getAttribute(TAXIPATH).toString());
            });


    Viewer v = g.getGraph().display(false);
    View vv = v.getDefaultView();
    vv.getCamera().setViewPercent(0.5);

    FileSinkImages pic = new FileSinkImages(OutputType.PNG, new CustomResolution(6000,6000));

    pic.setLayoutPolicy(LayoutPolicy.NO_LAYOUT);
    try {
      pic.writeAll(g.getGraph(), "sample.png");
    } catch (IOException e) {
      e.printStackTrace();
    }


  }
}
