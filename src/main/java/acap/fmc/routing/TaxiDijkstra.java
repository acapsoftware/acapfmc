/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.routing;

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.algorithm.util.FibonacciHeap;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

/** Created by erikn on 2/5/2017. */
public class TaxiDijkstra extends Dijkstra {

  interface IEdgeEval {
    boolean test(Edge e);
  }

  private IEdgeEval pathRestriction;

  protected static class Data {
    FibonacciHeap<Double, Node>.Node fn;
    Edge edgeFromParent;
    double distance;
  }

  public TaxiDijkstra(Element element, String resultAttribute, String lengthAttribute) {
    super(element, resultAttribute, lengthAttribute, null, null, null);
    pathRestriction = null;
  }

  public TaxiDijkstra(
      Element element, String resultAttribute, String lengthAttribute, IEdgeEval pathrestrict) {
    super(element, resultAttribute, lengthAttribute, null, null, null);
    pathRestriction = pathrestrict;
  }

  public double getPathLength(Node target) {
    return target.<Data>getAttribute(resultAttribute).distance;
  }

  public <T extends Edge> T getEdgeFromParent(Node target) {
    return (T) target.<Data>getAttribute(resultAttribute).edgeFromParent;
  }

  @Override
  protected void makeTree() {
    // initialization
    FibonacciHeap<Double, Node> heap = new FibonacciHeap<Double, Node>();
    for (Node node : graph) {
      Data data = new Data();

      double v = node == source ? getSourceLength() : Double.POSITIVE_INFINITY;

      data.fn = heap.add(v, node);
      data.edgeFromParent = null;
      node.addAttribute(resultAttribute, data);
    }

    // main loop
    while (!heap.isEmpty()) {
      Node u = heap.extractMin();
      Data dataU = u.getAttribute(resultAttribute);
      dataU.distance = dataU.fn.getKey();
      dataU.fn = null;
      if (dataU.edgeFromParent != null) edgeOn(dataU.edgeFromParent);
      for (Edge e : u.getEachLeavingEdge()) {
        Node v = e.getOpposite(u);
        Data dataV = v.getAttribute(resultAttribute);
        if (dataV.fn == null) continue;
        double tryDist = dataU.distance + getLength(e, v);
        if (tryDist < dataV.fn.getKey() && (pathRestriction != null && pathRestriction.test(e))) {
          dataV.edgeFromParent = e;
          heap.decreaseKey(dataV.fn, tryDist);
        }
      }
    }
  }
}
