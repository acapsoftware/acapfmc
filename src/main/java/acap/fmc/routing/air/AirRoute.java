package acap.fmc.routing.air;

import acap.fmc.Constants;
import acap.fmc.UserAircraft;
import acap.fmc.routing.Waypoint;
import acap.world.utilities.NavDB;
import acap.world.model.Airport;
import acap.world.model.Runway;
import java.util.ArrayList;

public class AirRoute {


  public static final float NM_TO_FEET = 6076.12f;
  public static final float FEET_PER_NAUTICAL_MILES = NM_TO_FEET;

  public void setCruisingAltitude(float cruisingAltitude) {
    this.cruisingAltitude = cruisingAltitude;
  }

  private float cruisingAltitude = -1;
  private int desiredVerticalSpeed = -1;

  public void setCruiseSpeed(float cruiseSpeed) {
    this.cruiseSpeed = cruiseSpeed;
  }

  private float cruiseSpeed;
  private Runway takeOffRunway;
  private Runway landingRunway;
  private double takeOffHeading;
  private Waypoint start;
  private static UserAircraft aircraft = UserAircraft.getInstance();
  private static NavDB nav = NavDB.getInstance();
  private Airport origin;
  private Airport dest;

  private ArrayList<Waypoint> landmarkWaypoints;
  private ArrayList<Waypoint> routeWaypoints;

  public AirRoute(String route){
    try {
      this.landmarkWaypoints = parseRouteString(route, aircraft.getCruiseAltitude());
      this.origin = nav.getAirport(this.landmarkWaypoints.get(0).getName());
      this.dest = nav.getAirport(this.landmarkWaypoints.get(this.landmarkWaypoints.size() - 1).getName());
      this.routeWaypoints = new ArrayList<>();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public ArrayList<Waypoint> calculateRoute() {

    Waypoint lift = getMinLiftPoint();
    this.routeWaypoints.add(lift);

    Waypoint endOfInitialTakeOff = getEndOfInitialTakeOff(lift);
    this.routeWaypoints.add(endOfInitialTakeOff);

    this.cruisingAltitude =
        cruisingAltitude < 0 ? (float) aircraft.getCruiseAltitude() : this.cruisingAltitude;
    Waypoint cruiseAlt = climbToTargetAltitude(endOfInitialTakeOff, cruisingAltitude,endOfInitialTakeOff.bearingTo(dest), this.desiredVerticalSpeed, this.cruiseSpeed);
    this.routeWaypoints.add(cruiseAlt);


    for (int x = 1; x < this.landmarkWaypoints.size() - 1; x++) this.routeWaypoints.add(this.landmarkWaypoints.get(x));

    Waypoint descentStart =
        topOfDescent(
            getDownwind45EntryPoint(landingRunway),
            this.routeWaypoints.get(this.routeWaypoints.size() - 1).getAltitude(),
            this.routeWaypoints
                .get(this.routeWaypoints.size() - 1)
                .bearingTo(getDownwind45EntryPoint(landingRunway)),
            this.desiredVerticalSpeed,
            cruiseSpeed);

    aircraft.setTopOfDescent(descentStart);
    this.routeWaypoints.add(descentStart);

    this.routeWaypoints.addAll(getLandingWaypoints());

    return this.routeWaypoints;
  }


  /**
   * Find the take off point at which the aircraft has reached pattern altitude
   * @param lift
   * @return
   */
  private Waypoint getEndOfInitialTakeOff(Waypoint lift) {
    this.desiredVerticalSpeed = desiredVerticalSpeed < 0 ? aircraft.getAutoFlightSettings().getDesiredVerticalSpeed() : this.desiredVerticalSpeed;
    float cruiseSpeed = this.cruiseSpeed <= 0 ? aircraft.getCruiseSpeed() : this.cruiseSpeed;
    return getTakeoffWaypoint(lift, (float) this.takeOffHeading,origin.getAlt() + Constants.ALTITUDE_ABOVE_PATTERN, this.desiredVerticalSpeed, cruiseSpeed);

  }

  /**
   * Retrieve the waypoint that the user aircraft can achieve the start of lift.
   * @return
   */
  public Waypoint getMinLiftPoint() {

    return this.start.getWaypointFromBearingAndDistance((float)this.takeOffHeading,aircraft.getMinRunway() / NM_TO_FEET);
  }


  /**
   * Sets the take off runway that will be used by the calculated route.
   * @param takeOffRunway
   */
  public void setTakeOffRunway(String takeOffRunway){
    this.takeOffRunway = this.origin.getRunwayByNumber(takeOffRunway);
    this.takeOffHeading = this.takeOffRunway.getHeading();
  }

  /**
   * Sets the runway that will be landed on.
   * @param landingRunway
   */
  public void setLandingRunway(String landingRunway){
    this.landingRunway = this.dest.getRunwayByNumber(landingRunway);
  }

  public void setStartingWaypoint(Waypoint start){
    this.start = start;
  }


  /**
   * Translates a string route of NAVAIDS/AIRPORTS to waypoints
   *
   * @param route string ("KORH KBOS")
   * @return
   * @throws Exception
   */
  private ArrayList<Waypoint> parseRouteString(String route, double cruiseAltitude)
      throws Exception {
    String[] pieces = route.split("\\s+");
    ArrayList<Waypoint> waypoints = new ArrayList<>();

    for (String piece : pieces) {

      if (NavDB.getInstance().getAirport(piece) != null)
        waypoints.add(new Waypoint(nav.getAirport(piece)));
      else if (NavDB.getInstance().getNavaid(piece) != null) {
        waypoints.add(new Waypoint(nav.getNavaid(piece), (float) aircraft.getCruiseAltitude()));
      } else throw new Exception("ENTERED INVALID NAVAID/AIRPORT");
    }
    return waypoints;
  }


  /**
   * @param start - starting waypoint
   * @param targetAltitude - IN FEET
   * @param climbrate - IN FEET PER MINUTE
   * @param ias - Indicated air speed (Knots)
   * @return
   */
  public  Waypoint getTakeoffWaypoint(
      Waypoint start, float heading, float targetAltitude, float climbrate, float ias) {
    float climbAngle = calculateClimbAngle(climbrate, ias);
    float timeToClimb = targetAltitude / (climbrate / 60.0f);
    float cruiseSpeed_x = (float) (ias * (Math.cos(climbAngle)));
    float cruiseSpeed_y = (float) (ias * (Math.sin(climbAngle)));
    float distanceTraveled_NM = timeToClimb * (cruiseSpeed_x / 60.f / 60.f);
    float distanceClimbed = targetAltitude - start.getAltitude();

    return start.getWaypointFromBearingAndDistance(heading,distanceTraveled_NM, distanceClimbed);
  }


  private float calculateClimbAngle(float climbrate, float ias) {
    return (float) Math.asin(climbrate / (ias * Constants.KNOTS_TO_FPM));
  }



  /**
   * Calculates waypoint after reaching target altitude from origin given performance variables
   *
   * @param targetAltitude - Target altitude to reach (feet)
   * @param heading - Heading being traveled (degrees)
   * @param climbrate - Climb rate in (ft/m)
   * @param ias - Indicated air speed (knots)
   * @return
   */
  public Waypoint climbToTargetAltitude(
      Waypoint origin, float targetAltitude, float heading, float climbrate, float ias) {
    float climbAngle = calculateClimbAngle(climbrate, ias);
    float timeToClimb = targetAltitude / (climbrate / 60.0f);
    float cruiseSpeed_x = (float) (ias * (Math.cos(climbAngle)));
    float cruiseSpeed_y = (float) (ias * (Math.sin(climbAngle)));
    float distanceTraveled_NM = timeToClimb * (cruiseSpeed_x / 60.f / 60.f);
    float distanceClimbed = targetAltitude - origin.getAltitude();

    return origin.getWaypointFromBearingAndDistance(heading,distanceTraveled_NM, distanceClimbed);
  }

  private Waypoint topOfDescent(
      Waypoint endpoint, float startingAltitude, float heading, float descentrate, float ias) {
    float climbAngle = calculateClimbAngle(descentrate, ias);
    float targetAltitude = endpoint.getAltitude();
    float timeToDescend = (startingAltitude - targetAltitude) / (descentrate / 60.0f); // seconds
    float cruiseSpeed_x = (float) (ias * (Math.cos(climbAngle)));
    float cruiseSpeed_y = (float) (ias * (Math.sin(climbAngle)));
    float distanceTraveled_NM = timeToDescend * (cruiseSpeed_x / 60.f / 60.f);
    float distanceTraveled_vertical =  startingAltitude - endpoint.getAltitude();

    return endpoint.getWaypointFromBearingAndDistance(heading - 180, distanceTraveled_NM,distanceTraveled_vertical);
  }

  public Waypoint getDownwind45EntryPoint(Runway r) {

    Waypoint downwind =
        new Waypoint(r).getWaypointFromBearingAndDistance(r.getHeading() - 90, 1);

    return downwind.getWaypointFromBearingAndDistance(r.getHeading() - 45, 1, Constants.ALTITUDE_ABOVE_PATTERN);


  }

  public Waypoint getDownwindHalfwayPoint(Runway r) {
    return new Waypoint(r).getWaypointFromBearingAndDistance(r.getHeading() - 90, 1, Constants.ALTITUDE_ABOVE_PATTERN);
  }

  public Waypoint getBaseStart(Runway r, Waypoint finalApproach) {

    return finalApproach.getWaypointFromBearingAndDistance(r.getHeading() - 90, 1, 0);
  }


  private Waypoint getEndofRunway(Runway r) {

    return new Waypoint(r).getWaypointFromBearingAndDistance(r.getHeading(),
        (float) (r.getLength() / FEET_PER_NAUTICAL_MILES / 2.0));
  }

  private Waypoint getStartOfRunway(Runway r) {
    return new Waypoint(r).getWaypointFromBearingAndDistance(r.getHeading() - 180,
        (float) (r.getLength() / FEET_PER_NAUTICAL_MILES / 2.0));
  }

  public Waypoint getFinalStart(Runway r, float verticalspeed, float descentspeed) {
    float timeToDescend = Constants.ALTITUDE_ABOVE_PATTERN / (verticalspeed / 60.f);
    float distanceTraveled_NM = timeToDescend * (descentspeed / 60.f / 60.f);

    Waypoint touchdown = getStartOfRunway(r).getWaypointFromBearingAndDistance(r.getHeading(),aircraft.getMinLanding() / FEET_PER_NAUTICAL_MILES);
    return touchdown.getWaypointFromBearingAndDistance(r.getHeading() - 180, distanceTraveled_NM, Constants.ALTITUDE_ABOVE_PATTERN);

  }

  public ArrayList<Waypoint> getLandingWaypoints() {

    Waypoint downwind45Entry = getDownwind45EntryPoint(this.landingRunway);
    Waypoint downwind = getDownwindHalfwayPoint(this.landingRunway);

    Waypoint finalApp = getFinalStart(this.landingRunway, this.desiredVerticalSpeed, this.cruiseSpeed);
    Waypoint base = getBaseStart(this.landingRunway, finalApp);

    ArrayList<Waypoint> w = new ArrayList<>();

    w.add(downwind45Entry);
    UserAircraft.getInstance().setInitialApproach(downwind45Entry);
    w.add(downwind);

    w.add(base);
    w.add(finalApp);
    UserAircraft.getInstance().setFinalApproach(finalApp);

    Waypoint touchdown = getStartOfRunway(this.landingRunway).getWaypointFromBearingAndDistance(this.landingRunway.getHeading(),aircraft.getMinLanding() / FEET_PER_NAUTICAL_MILES);
    w.add(touchdown);
    UserAircraft.getInstance().setExpectedTouchdown(touchdown);

    return w;
  }

}
