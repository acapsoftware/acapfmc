/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.routing;

import static acap.fmc.Constants.identOpacity;
import static acap.fmc.routing.AirportGraph.HOLDSHORT;
import static acap.world.model.Camera.theta;
import static acap.world.model.Camera.zoom;
import static acap.world.view.WorldView.drawer;
import static processing.core.PApplet.radians;
import static processing.core.PConstants.CENTER;
import static processing.core.PConstants.PI;

import acap.world.utilities.CalculationUtils;
import acap.world.utilities.DrawingUtils;
import acap.world.utilities.NavDB;
import acap.world.model.Airport;
import acap.world.model.BaseAeroObject;
import acap.world.model.Navaid;
import acap.world.model.Runway;
import java.util.ArrayList;
import java.util.List;
import org.graphstream.graph.Node;
import processing.core.PVector;

public class Waypoint extends BaseAeroObject {

  PVector location = new PVector(0, 0); //encoded as lonToYCoord, latToXCoord

  public void setAltitude(int altitude) {
    this.altitude = altitude;
  }



  int altitude = 0;
  String name = "";
  int speed = 0;
  Navaid navaid = null;
  String runwayNumber = "0";
  boolean primary = true;

  public boolean isHoldshort() {
    return holdshort;
  }

  boolean holdshort = false;

  public Waypoint(float lon, float lat) {
    this(new PVector(lon, lat));
  }

  public Waypoint(float lon, float lat, float altitude) {
    this(new PVector(lon, lat), altitude);
  }

  public Waypoint(double lon, double lat, double altitude) {
    this(new PVector((float) lon, (float) lat), (float) altitude);
  }

  public Waypoint(PVector location) {
    super("", "", location.y, location.x);
    this.location = location;
    this.name = "GPS";
  }

  public Waypoint(PVector location, int speed) {
    this(location);
    this.speed = speed;
  }

  public Waypoint(PVector location, float altitude) {
    this(location);
    this.altitude = (int) altitude;
  }

  public Waypoint(PVector location, float altitude, int speed) {
    this(location, altitude);
    this.speed = speed;
  }

  public Waypoint(Navaid navaid) {
    super(navaid.getIdent(), navaid.getIdent(), navaid.getLat(), navaid.getLon());
    this.navaid = navaid;
    this.location = new PVector(navaid.getLon(), navaid.getLat());
    this.name = navaid.getIdent();
  }

  public Waypoint(Navaid navaid, int speed) {
    this(navaid);
    this.speed = speed;
  }

  public Waypoint(Navaid navaid, float altitude) {
    this(navaid);
    this.altitude = (int) altitude;
  }

  public Waypoint(Navaid navaid, float altitude, int speed) {
    this(navaid, altitude);
    this.speed = speed;
  }

  public Waypoint(Node n, double altitude) {
    this(n.getAttribute("lon"), n.getAttribute("lat"), altitude);
    this.holdshort = n.getAttribute(HOLDSHORT);
  }

  public Waypoint(Airport airport) {
    super(airport.name, airport.ident, airport.lat, airport.lon);
    this.location = new PVector((float) airport.lat, (float) airport.lon);
    this.name = airport.ident;
    this.altitude = (int) airport.getAlt();
  }

  public Waypoint(Airport airport, int speed) {
    this(airport);
    this.speed = speed;
  }

  public Waypoint(Airport airport, float altitude) {
    this(airport);
    this.altitude = (int) altitude;
  }

  public Waypoint(Airport airport, float altitude, int speed) {
    this(airport, altitude);
    this.speed = speed;
  }

  public Waypoint(String intersection) {
    super(
        intersection,
        intersection,
        NavDB.getInstance().getIntersections().get(intersection).x,
        NavDB.getInstance().getIntersections().get(intersection).y);
    this.location = NavDB.getInstance().getIntersections().get(intersection);
    this.name = intersection;
  }

  public Waypoint(String intersection, int speed) {
    this(intersection);
    this.speed = speed;
  }

  public Waypoint(String intersection, float altitude) {
    this(intersection);
    this.altitude = (int) altitude;
  }

  public Waypoint(String intersection, float altitude, int speed) {
    this(intersection, altitude);
    this.speed = speed;
  }

  public Waypoint(Runway runway, boolean primary) {
    super(
        String.valueOf(runway.getHeading()),
        String.valueOf(runway.getHeading()),
        runway.getLat(),
        runway.getLon());
    this.location = new PVector(runway.getLon(), runway.getLat());
    this.primary = primary;
    this.altitude = (int) runway.getAlt();
    if (primary) this.runwayNumber = runway.getPrimaryNumber() + runway.getPrimaryDesignator();
    else this.runwayNumber = runway.getSecondaryNumber() + runway.getSecondaryDesignator();

    this.name = runwayNumber;
  }

  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Waypoint))return false;
    Waypoint otherMyClass = (Waypoint) other;
    return this.lat == otherMyClass.lat && this.lon == otherMyClass.lon && this.altitude == otherMyClass.altitude;
  }

  public Waypoint(Runway runway) {
    this(runway, true);
  }

  public String toString(){
    return "Lon: " + lon + " lat: " + lat + " alt: " + altitude;
  }

  public PVector getLocation() {
    return location;
  }

  public float getAltitude() {
    return altitude;
  }

  public String getName() {
    return name;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public int getSpeed() {
    return speed;
  }

  public Navaid getNavaid() {
    return navaid;
  }

  public String getRunwayNumber() {
    return runwayNumber;
  }

  public boolean isPrimary() {
    return primary;
  }

  @Override
  public double lat() {
    return location.y;
  }

  @Override
  public double lon() {
    return location.x;
  }

  public void drawTo(Waypoint b) {
    DrawingUtils.drawLatLonLine(
        location.y, location.x, altitude, b.getLocation().y, b.getLocation().x, b.getAltitude());
  }

  public List<Waypoint> getPointsTo(Waypoint to){
    ArrayList<Waypoint> between = new ArrayList<>();

    double altBetween = to.altitude - altitude;

    double pointsBetweenWaypoint = 15.0;

    if(this.distanceTo(to) > 1)
      pointsBetweenWaypoint = 200.0;

    for(int i = 1; i <= pointsBetweenWaypoint; i++) {
      Waypoint temp = this.getWaypointFromBearingAndDistance(this.bearingTo(to),
          (float) ((this.distanceTo(to) / pointsBetweenWaypoint) * i),
          (float) ((altBetween / pointsBetweenWaypoint) * i));
      between.add(temp);
    }
    return between;
  }

  public void drawTo(Waypoint b, int r, int g, int blue) {
    DrawingUtils.drawLatLonLine(
        location.y,
        location.x,
        altitude,
        b.getLocation().y,
        b.getLocation().x,
        b.getAltitude(),
        r,
        g,
        blue);
  }

  public double calculateTurnAngleToNext(Waypoint inbound, Waypoint outbound) {
    return inbound.bearingTo(outbound) - bearingTo(inbound);
  }

  public static void drawWaypointList(ArrayList<Waypoint> waypoints) {
    for (int x = 0; x < waypoints.size() - 1; x++) waypoints.get(x).drawTo(waypoints.get(x + 1));
  }

  public Waypoint getWaypointFromBearingAndDistance(
       float bearing, float distance) {

    return getWaypointFromBearingAndDistance(bearing,distance,0);
  }

  public static void drawWaypointList(ArrayList<Waypoint> waypoints, int r, int g, int b) {
    drawWaypointList(waypoints, r, g, b, false);
  }

  public static void drawWaypointList(
      ArrayList<Waypoint> waypoints, int r, int g, int b, boolean drawAlt) {
    for (int x = 0; x < waypoints.size() - 1; x++) {
      waypoints.get(x).drawTo(waypoints.get(x + 1), r, g, b);
      if (drawAlt) {
        waypoints.get(x).drawWaypointInfo();
      }
    }
  }

  public void drawWaypointInfo() {

    drawer.stroke(255, identOpacity);
    drawer.fill(255, identOpacity);
    drawer.textAlign(CENTER);
    drawer.textSize(100);
    drawer.pushMatrix();
    drawer.translate(
        CalculationUtils.latToXCoord((float) this.lat()),
        CalculationUtils.lonToYCoord((float) this.lon()),
        this.getAltitude());
    drawer.line(0, 0, 0, 0, 0, 0.5f * zoom + 5000f);
    drawer.translate(0, 0, 0.5f * zoom + 6000f);
    drawer.rotateX(-PI / 2);
    drawer.rotateY(-radians(theta + 90));
    drawer.scale(0.5f * zoom + 1);
    drawer.text(this.getAltitude(), 0, 0);
    drawer.popMatrix();
  }

  public void drawWaypointSpeed() {

    drawer.stroke(255, identOpacity);
    drawer.fill(255, identOpacity);
    drawer.textAlign(CENTER);
    drawer.textSize(100);
    drawer.pushMatrix();
    drawer.translate(
        CalculationUtils.latToXCoord((float) this.lat()),
        CalculationUtils.lonToYCoord((float) this.lon()),
        this.getAltitude());
    drawer.line(0, 0, 0, 0, 0, 0.5f * zoom + 3000f);
    drawer.translate(0, 0, 0.5f * zoom + 3000f);
    drawer.rotateX(-PI / 2);
    drawer.rotateY(-radians(theta + 90));
    drawer.scale(0.5f * zoom);
    drawer.text(this.speed, 0, 0);
    drawer.popMatrix();
  }

  public Waypoint getWaypointFromBearingAndDistance(float bearing, float horizontalDistance, float verticalDistance) {

    final double lat1 = Math.toRadians(lat);
    final double lng1 = Math.toRadians(lon);

    final double rBearing = Math.toRadians(bearing);

    float d = BaseAeroObject.NM_TO_KM * horizontalDistance;

    double lat2 =
        Math.asin(
            Math.sin(lat1) * Math.cos(d / BaseAeroObject.EARTH_RADIUS_KM)
                + Math.cos(lat1)
                * Math.sin(d / BaseAeroObject.EARTH_RADIUS_KM)
                * Math.cos(rBearing));
    double lon2 =
        lng1
            + Math.atan2(
            Math.sin(rBearing) * Math.sin(d / BaseAeroObject.EARTH_RADIUS_KM) * Math.cos(lat1),
            Math.cos(d / BaseAeroObject.EARTH_RADIUS_KM) - Math.sin(lat1) * Math.sin(lat1));

    return new Waypoint(new PVector((float) Math.toDegrees(lon2), (float) Math.toDegrees(lat2)), this.altitude + verticalDistance);
  }
}
