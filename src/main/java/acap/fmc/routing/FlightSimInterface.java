/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.routing;

import acap.fmc.controls.SerialMessage.ControlMessage.component;
import com.flightsim.fsuipc.fsuipc_wrapper;
import java.util.HashMap;

/** Created by erikn on 1/19/2017. */
public class FlightSimInterface {

  private component comp;
  private short flapDetents = 4;
  private static int[] toggleBitLocation = new int[20];

  static {
    toggleBitLocation[component.LANDING_LIGHT_VALUE] = (char) 4;
    toggleBitLocation[component.ROTATING_BEACON_VALUE] = (char) 2;
    toggleBitLocation[component.NAVIGATION_LIGHT_VALUE] = (char) 1;
  }

  private static HashMap<component, Integer> controlValues = new HashMap<>();

  static {
    controlValues.put(component.STABILIATOR, 0x0BB2);
    controlValues.put(component.AILERON, 0x0BB6);
    controlValues.put(component.RUDDER, 0x0BBA);
    controlValues.put(component.BRAKES, 0x0BC4);
    controlValues.put(component.FLAPS, 0x0BDC);
    controlValues.put(component.THROTTLE, 0x088C);
    controlValues.put(component.MIXTURE, 0x0890);
    controlValues.put(component.STARTER, 0x0892);
    controlValues.put(component.STABILATOR_TRIM, 0x0BC0);
    controlValues.put(component.FUEL_PUMP, 0x3104);
    controlValues.put(component.LANDING_LIGHT, 0x0D0C);
    controlValues.put(component.ROTATING_BEACON, 0x0D0C);
    controlValues.put(component.NAVIGATION_LIGHT, 0x0D0C);
  }

  private static HashMap<component, Boolean> toggleValues = new HashMap<>();

  static {
    for (component c : component.values()) toggleValues.put(c, false);

    toggleValues.put(component.STARTER, true);
    toggleValues.put(component.FUEL_PUMP, true);
    toggleValues.put(component.LANDING_LIGHT, true);
    toggleValues.put(component.ROTATING_BEACON, true);
  }

  public FlightSimInterface(component c) {
    this.comp = c;
  }

  public void setSimControlValue(int value) {

    if (comp == component.STARTER) {
      byte[] data = new byte[2];

      data[0] = (byte) (value & 0xFF);
      data[1] = (byte) ((value >> 8) & 0xFF);
      fsuipc_wrapper.WriteData(controlValues.get(comp), 2, data);
      return;
    }

    short aValue = (short) (value * 164);
    if (comp == component.FLAPS) {
      int notch = (16383 / flapDetents) * value;
      aValue = (short) notch;
    }

    byte[] data = new byte[2];
    data[0] = (byte) (aValue & 0xFF);
    data[1] = (byte) ((aValue >> 8) & 0xFF);
    fsuipc_wrapper.WriteData(controlValues.get(comp), 2, data);

    // Set the other brake value as well (we dont have 2 brakes in the protocol)
    if (comp == component.BRAKES) fsuipc_wrapper.WriteData(0x0BC6, 2, data);
  }

  public void setSimControlValue(boolean value) {

    if (comp == component.FUEL_PUMP) {
      byte[] data = new byte[1];
      if (value) data[0] = 1;
      else data[0] = 0;
      fsuipc_wrapper.WriteData(controlValues.get(comp), 1, data);
    } else {
      byte[] data = new byte[1];
      byte[] setdata = new byte[1];
      setdata[0] = (byte) toggleBitLocation[comp.getNumber()];
      fsuipc_wrapper.ReadData(controlValues.get(comp), 1, data);
      if (value) data[0] = (byte) (data[0] | setdata[0]);
      else data[0] = (byte) (data[0] & ~setdata[0]);

      fsuipc_wrapper.WriteData(controlValues.get(comp), 1, data);
    }
  }

  public static boolean isToggleControl(component c) {
    return toggleValues.get(c);
  }
}
