/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package acap.fmc.state;

import acap.world.plugin.RouteView;
import java.util.HashMap;

public class ModeControls {

  public static final String CRS = "CRS";
  public static final String SPD = "SPD";
  public static final String HDG = "HDG";
  public static final String ALT = "ALT";
  public static final String VS = "VS";
  public static final String Origin = "Origin";
  public static final String Destination = "Destination";
  public static final String Route = "Route";
  public static final String TaxiOut = "Taxi-Out";
  public static final String TaxiIn = "Taxi-In";

  private static ModeControls instance;

  private HashMap<String, String> map;

  public ModeControls() {
    this.map = new HashMap<>();

    this.map.put(CRS, null);
    this.map.put(SPD, null);
    this.map.put(HDG, null);
    this.map.put(ALT, null);
    this.map.put(VS, null);
    this.map.put(Origin, null);
    this.map.put(Destination, null);
    this.map.put(Route, null);
    this.map.put(TaxiOut, null);
    this.map.put(TaxiIn, null);
  }

  public static ModeControls getInstance() {
    if (instance == null) instance = new ModeControls();
    return instance;
  }

  public String getControl(String control) {
    if (!this.map.containsKey(control))
      throw new Error("Attempted to get a mode control that doesn't exist.");
    else return this.map.get(control);
  }

  public void updateControl(String control, String value) {
    if (!this.map.containsKey(control))
      throw new Error("Attempted to assign a value to a mode control that doesn't exist.");
    else {
      this.map.put(control, value);
      RouteView.refreshRoute();
    }
  }
}
