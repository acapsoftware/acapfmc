/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package metar.core.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;

/**
 * Java class for anonymous complex types.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element ref="{}METAR"/>
 *       &lt;/sequence>
 *       &lt;attribute name="num_results" types="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
  name = "",
  propOrder = {"metar"}
)
@XmlRootElement(name = "data")
public class Data {

  @XmlElement(name = "METAR")
  protected List<METAR> metar;

  @XmlAttribute(name = "num_results")
  protected Integer numResults;

  /**
   * Gets the value of the metar property.
   *
   * <p>This accessor method returns a reference to the live list, not a snapshot. Therefore any
   * modification you make to the returned list will be present inside the JAXB object. This is why
   * there is not a <CODE>set</CODE> method for the metar property.
   *
   * <p>For example, to add a new item, do as follows:
   *
   * <pre>
   *    getMETAR().add(newItem);
   * </pre>
   *
   * <p>Objects of the following types(s) are allowed in the list {@link METAR }
   */
  public List<METAR> getMETAR() {
    if (metar == null) {
      metar = new ArrayList<METAR>();
    }
    return this.metar;
  }

  /**
   * Gets the value of the numResults property.
   *
   * @return possible object is {@link Integer }
   */
  public Integer getNumResults() {
    return numResults;
  }

  /**
   * Sets the value of the numResults property.
   *
   * @param value allowed object is {@link Integer }
   */
  public void setNumResults(Integer value) {
    this.numResults = value;
  }
}
