/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package metar.core.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Java class for anonymous complex types.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="sky_cover" types="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="cloud_base_ft_agl" types="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "sky_condition")
public class SkyCondition {

  @XmlAttribute(name = "sky_cover")
  protected String skyCover;

  @XmlAttribute(name = "cloud_base_ft_agl")
  protected Integer cloudBaseFtAgl;

  /**
   * Gets the value of the skyCover property.
   *
   * @return possible object is {@link String }
   */
  public String getSkyCover() {
    return skyCover;
  }

  /**
   * Sets the value of the skyCover property.
   *
   * @param value allowed object is {@link String }
   */
  public void setSkyCover(String value) {
    this.skyCover = value;
  }

  /**
   * Gets the value of the cloudBaseFtAgl property.
   *
   * @return possible object is {@link Integer }
   */
  public Integer getCloudBaseFtAgl() {
    return cloudBaseFtAgl;
  }

  /**
   * Sets the value of the cloudBaseFtAgl property.
   *
   * @param value allowed object is {@link Integer }
   */
  public void setCloudBaseFtAgl(Integer value) {
    this.cloudBaseFtAgl = value;
  }
}
