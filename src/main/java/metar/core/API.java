/*
 * Copyright 2016 DiffPlug
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package metar.core;

import acap.world.model.Airport;
import acap.world.model.Runway;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import metar.core.xml.METAR;
import metar.core.xml.Response;

public class API {

  static String mainURL =
      "http://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml";

  public static METAR grabLatestMETAR(String airport) {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
      HttpResponse<String> returnedXML =
          Unirest.get(mainURL)
              .queryString("stationString", airport)
              .queryString("hoursBeforeNow", "1")
              .asString();

      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      Response dataResponse = (Response) jaxbUnmarshaller.unmarshal(returnedXML.getRawBody());
      METAR receivedMetar = dataResponse.getData().getMETAR().get(0);

      return receivedMetar;

    } catch (JAXBException e) {
      e.printStackTrace();
    } catch (UnirestException e) {
      e.printStackTrace();
    } catch (IndexOutOfBoundsException e) {
      System.err.println("NO RESPONSE FROM AVIATION WEATHER.GOV - TRY AGAIN IN A FEW MINUTES");
      e.printStackTrace();
    }
    return new METAR();
  }

  public static METAR grabNearestMETAR(float lon, float lat, int radius) {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
      HttpResponse<String> returnedXML =
          Unirest.get(mainURL)
              .queryString(
                  "radialDistance",
                  Integer.toString(radius) + ";" + Float.toString(lon) + "," + Float.toString(lat))
              .queryString("hoursBeforeNow", "3")
              .asString();

      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      Response dataResponse = (Response) jaxbUnmarshaller.unmarshal(returnedXML.getRawBody());
      METAR receivedMetar = dataResponse.getData().getMETAR().get(0);

      return receivedMetar;

    } catch (JAXBException e) {
      e.printStackTrace();
    } catch (UnirestException e) {
      e.printStackTrace();
    }
    return new METAR();
  }

  /**
   * * Returns the density altitude in Feet given the current metar data
   *
   * @param m Metar data
   * @return
   */
  public static float calculateDensityAltitude(METAR m) {
    float elevationInFeet = (m.getElevationM() * 3.28084f);

    float pressureAltitude = elevationInFeet + (1000 * (29.92f - m.getAltimInHg()));
    float densityAltitude =
        pressureAltitude + (120 * (m.getTempC() - (((elevationInFeet * 2) / 1000) - 15) * -1));
    return densityAltitude;
  }

  public static Runway pickTakeOffRunway(
      METAR weather,
      Airport p,
      float minRunwayNeeded,
      double maxCrosswindComponent,
      double maxHeadwindComponent) {

    float smallestHeadingDiff = Float.MAX_VALUE;
    Runway prefRunway = null;

    Integer winddirection = weather.getWindDirDegrees();
    Integer windspeed = weather.getWindSpeedKt();

    winddirection = winddirection / 10;

    if (isNegligibleWinds(windspeed)) {

      for (Runway r : p.getRunways()) {
        if (r.getLength() >= minRunwayNeeded) {
          float headingDiff;
          headingDiff = getHeadingDiff(winddirection, r);

          if (headingDiff < smallestHeadingDiff) {
            smallestHeadingDiff = headingDiff;
            prefRunway = r;
          }
        }
      }
    } else {
      for (Runway r : p.getRunways()) {
        if (r.getLength() >= minRunwayNeeded) {
          float diff;
          double crosswindComponent = windspeed * Math.sin(winddirection - r.getHeading());
          double headwindComponent = windspeed * Math.cos(winddirection - r.getHeading());

          diff = getHeadingDiff(winddirection, r);

          if (diff < smallestHeadingDiff
              && crosswindComponent < maxCrosswindComponent
              && headwindComponent < maxHeadwindComponent) {
            smallestHeadingDiff = diff;
            prefRunway = r;
          }
        }
      }
    }
    return prefRunway;
  }

  private static float getHeadingDiff(Integer winddirection, Runway r) {
    float headingDiff;
    if (winddirection > r.getHeading()) headingDiff = winddirection - r.getHeading();
    else headingDiff = r.getHeading() - winddirection;
    return headingDiff;
  }

  private static boolean isNegligibleWinds(float windspeed) {
    return windspeed <= 5;
  }
}
